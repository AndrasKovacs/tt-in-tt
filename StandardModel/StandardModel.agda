{-# OPTIONS --no-eta #-}

-- note : we rely on eta for Σ (in library.agda, eta is not turned on)

module StandardModel.StandardModel (⟦U⟧ : Set)(⟦El⟧ : ⟦U⟧ → Set) where

open import lib

-- set-theoretic interpretation

open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.Func

d : Decl
d = record
  { Con   =             Set
  ; Ty    = λ ⟦Γ⟧     → ⟦Γ⟧ → Set
  ; Tms   = λ ⟦Γ⟧ ⟦Δ⟧ → ⟦Γ⟧ → ⟦Δ⟧
  ; Tm    = λ ⟦Γ⟧ ⟦A⟧ → (γ : ⟦Γ⟧) → (⟦A⟧ γ)
  }

c : Core d
c = record
  { •     =             ⊤
  ; _,_   = λ ⟦Γ⟧ ⟦A⟧ → Σ ⟦Γ⟧ ⟦A⟧
  ; _[_]T = λ ⟦A⟧ ⟦δ⟧ γ → ⟦A⟧ (⟦δ⟧ γ)
  ; id    = λ         γ → γ
  ; _∘_   = λ ⟦δ⟧ ⟦σ⟧ γ → ⟦δ⟧ (⟦σ⟧ γ)
  ; ε     = λ         _ → tt
  ; _,s_  = λ ⟦δ⟧ ⟦t⟧ γ → ⟦δ⟧ γ ,Σ ⟦t⟧ γ
  ; π₁    = λ ⟦δ⟧     γ → (proj₁ (⟦δ⟧ γ))
  ; _[_]t = λ ⟦t⟧ ⟦δ⟧ γ → ⟦t⟧ (⟦δ⟧ γ)
  ; π₂    = λ ⟦δ⟧     γ → (proj₂ (⟦δ⟧ γ))
  ; [id]T = refl
  ; [][]T = refl
  ; idl   = refl
  ; idr   = refl
  ; ass   = refl
  ; ,∘    = refl
  ; π₁β   = refl
  ; πη    = refl
  ; εη    = refl
  ; π₂β   = refl
  }

b : Base c
b = record
  { U     = λ         _ → ⟦U⟧
  ; U[]   = refl
  ; El    = λ ⟦Â⟧     γ → ⟦El⟧ (⟦Â⟧ γ)
  ; El[]  = refl
  }

f : Func c
f = record
  { Π     = λ ⟦A⟧ ⟦B⟧ γ → (x : ⟦A⟧ γ) → ⟦B⟧ (γ ,Σ x)
  ; Π[]   = refl
  ; lam   = λ ⟦t⟧     γ → λ a → ⟦t⟧ (γ ,Σ a)
  ; app   = λ ⟦t⟧     γ → ⟦t⟧ (proj₁ γ) (proj₂ γ)
  ; lam[] = refl
  ; Πβ    = refl
  ; Πη    = refl
  }

open import TT.Syntax
open import TT.Rec

open rec d c b f

⟦_⟧C : Con → Set
⟦_⟧T : ∀{Γ} → Ty Γ → ⟦ Γ ⟧C → Set
⟦_⟧s : ∀{Γ Δ} → Tms Γ Δ → ⟦ Γ ⟧C → ⟦ Δ ⟧C
⟦_⟧t : ∀{Γ}{A : Ty Γ} → (t : Tm Γ A) → (γ : ⟦ Γ ⟧C) → ⟦ A ⟧T γ

⟦_⟧C = RecCon
⟦_⟧T = RecTy
⟦_⟧s = RecTms
⟦_⟧t = RecTm
