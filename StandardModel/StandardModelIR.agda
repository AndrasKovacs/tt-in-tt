{-# OPTIONS --no-eta #-}
-- note : we rely on eta for Σ (in library.agda, eta is not turned on)

module StandardModel.StandardModelIR (⟦U⟧ : Set)(⟦El⟧ : ⟦U⟧ → Set) where

open import lib

-- an inductive-recursive universe

data UU : Set
EL : UU → Set

data UU where
  ′Π′  : (A : UU) → (EL A → UU) → UU -- \'
  ′Σ′  : (A : UU) → (EL A → UU) → UU
  ′⊤′  : UU
  ′U′  : UU
  ′El′ : EL ′U′ → UU

EL (′Π′ A B) =   (x : EL A) → EL (B x)
EL (′Σ′ A B) = Σ (EL A) λ x → EL (B x)
EL ′⊤′       = ⊤
EL ′U′       = ⟦U⟧
EL (′El′ Â)  = ⟦El⟧ Â

-- set-theoretic interpretation

open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.Func

d : Decl
d = record
  { Con   =             UU
  ; Ty    = λ ⟦Γ⟧     → EL ⟦Γ⟧ → UU
  ; Tms   = λ ⟦Γ⟧ ⟦Δ⟧ → EL ⟦Γ⟧ → EL ⟦Δ⟧
  ; Tm    = λ ⟦Γ⟧ ⟦A⟧ → (γ : EL ⟦Γ⟧) → EL (⟦A⟧ γ)
  }

c : Core d
c = record
  { •     =             ′⊤′
  ; _,_   = λ ⟦Γ⟧ ⟦A⟧ → ′Σ′ ⟦Γ⟧ ⟦A⟧
  ; _[_]T = λ ⟦A⟧ ⟦δ⟧ γ → ⟦A⟧ (⟦δ⟧ γ)
  ; id    = λ         γ → γ
  ; _∘_   = λ ⟦δ⟧ ⟦σ⟧ γ → ⟦δ⟧ (⟦σ⟧ γ)
  ; ε     = λ         _ → tt
  ; _,s_  = λ ⟦δ⟧ ⟦t⟧ γ → ⟦δ⟧ γ ,Σ ⟦t⟧ γ
  ; π₁    = λ ⟦δ⟧     γ → (proj₁ (⟦δ⟧ γ))
  ; _[_]t = λ ⟦t⟧ ⟦δ⟧ γ → ⟦t⟧ (⟦δ⟧ γ)
  ; π₂    = λ ⟦δ⟧     γ → (proj₂ (⟦δ⟧ γ))
  ; [id]T = refl
  ; [][]T = refl
  ; idl   = refl
  ; idr   = refl
  ; ass   = refl
  ; ,∘    = refl
  ; π₁β   = refl
  ; πη    = refl
  ; εη    = refl
  ; π₂β   = refl
  }

b : Base c
b = record
  { U     = λ         _ → ′U′
  ; U[]   = refl
  ; El    = λ ⟦Â⟧     γ → ′El′ (⟦Â⟧ γ)
  ; El[]  = refl
  }

f : Func c
f = record
  { Π     = λ ⟦A⟧ ⟦B⟧ γ → ′Π′ (⟦A⟧ γ) (λ x → ⟦B⟧ (γ ,Σ x))
  ; Π[]   = refl
  ; lam   = λ ⟦t⟧     γ → λ a → ⟦t⟧ (γ ,Σ a)
  ; app   = λ ⟦t⟧     γ → ⟦t⟧ (proj₁ γ) (proj₂ γ)
  ; lam[] = refl
  ; Πβ    = refl
  ; Πη    = refl
  }

open import TT.Syntax
open import TT.Rec

open rec d c b f

⟦_⟧C : Con → UU
⟦_⟧T : ∀{Γ} → Ty Γ → EL (⟦ Γ ⟧C) → UU
⟦_⟧s : ∀{Γ Δ} → Tms Γ Δ → EL (⟦ Γ ⟧C) → EL (⟦ Δ ⟧C)
⟦_⟧t : ∀{Γ}{A : Ty Γ} → (t : Tm Γ A) → (γ : EL (⟦ Γ ⟧C)) → EL (⟦ A ⟧T γ)

⟦_⟧C = RecCon
⟦_⟧T = RecTy
⟦_⟧s = RecTms
⟦_⟧t = RecTm
