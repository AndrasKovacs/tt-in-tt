{-# OPTIONS --rewriting #-}

module StandardModel.Cons where

open import lib
open import TT.Syntax
open import StandardModel.StandardModel ⊥ (λ _ → ⊤)

-- consistency of our theory using the standard model

cons : Tm • U → ⊥
cons t = ⟦ t ⟧t tt
