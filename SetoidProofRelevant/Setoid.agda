{-# OPTIONS --without-K #-}

module Setoid.Setoid where

open import lib

open import NBE.Cheat  

-- setoid

record Setoid : Set₁ where
  field
    ∣_∣ : Set
    _∶_~_ : ∣_∣ → ∣_∣ → Set
    ref : (γ : ∣_∣) → _∶_~_ γ γ

  infix 4 ∣_∣
  infix 5 _∶_~_

∶~T=
  : {∣∣₀ ∣∣₁ : Set}(∣∣₂ : ∣∣₀ ≡ ∣∣₁)
    {_~₀_ : ∣∣₀ → ∣∣₀ → Set}{_~₁_ : ∣∣₁ → ∣∣₁ → Set}
  → (∣∣₀ → ∣∣₀ → Set)
  ≡ (∣∣₁ → ∣∣₁ → Set)
∶~T= refl = refl

refT=
  : {∣∣₀ ∣∣₁ : Set}(∣∣₂ : ∣∣₀ ≡ ∣∣₁)
    {_~₀_ : ∣∣₀ → ∣∣₀ → Set}{_~₁_ : ∣∣₁ → ∣∣₁ → Set}
    (~₂ : _~₀_ ≡[ ∶~T= ∣∣₂ {_~₀_}{_~₁_} ]≡ _~₁_)
    {ref₀ : (γ : ∣∣₀) → γ ~₀ γ}{ref₁ : (γ : ∣∣₁) → γ ~₁ γ}
  → ((γ : ∣∣₀) → _~₀_ γ γ)
  ≡ ((γ : ∣∣₁) → _~₁_ γ γ)
refT= refl refl = refl

mkSetoid=
  : {∣∣₀ ∣∣₁ : Set}(∣∣₂ : ∣∣₀ ≡ ∣∣₁)
    {_~₀_ : ∣∣₀ → ∣∣₀ → Set}{_~₁_ : ∣∣₁ → ∣∣₁ → Set}
    (~₂ : _~₀_ ≡[ ∶~T= ∣∣₂ {_~₀_}{_~₁_} ]≡ _~₁_)
    {ref₀ : (γ : ∣∣₀) → γ ~₀ γ}{ref₁ : (γ : ∣∣₁) → γ ~₁ γ}
    (ref₂ : ref₀ ≡[ refT= ∣∣₂ ~₂ {ref₀}{ref₁} ]≡ ref₁)
  → _≡_ {A = Setoid}
        (record { ∣_∣ = ∣∣₀ ; _∶_~_ = _~₀_ ; ref = ref₀ })
        (record { ∣_∣ = ∣∣₁ ; _∶_~_ = _~₁_ ; ref = ref₁ })
mkSetoid= refl refl refl = refl

open Setoid public

∣∣= : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
   → ∣ Γ₀ ∣ ≡ ∣ Γ₁ ∣
∣∣= refl = refl

∶~=
  : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
    {γ₀ : ∣ Γ₀ ∣}{γ₁ : ∣ Γ₁ ∣}(γ₂ : γ₀ ≡[ ∣∣= Γ₂ ]≡ γ₁)
    {δ₀ : ∣ Γ₀ ∣}{δ₁ : ∣ Γ₁ ∣}(δ₂ : δ₀ ≡[ ∣∣= Γ₂ ]≡ δ₁)
  → Γ₀ ∶ γ₀ ~ δ₀ ≡ Γ₁ ∶ γ₁ ~ δ₁
∶~= refl refl refl = refl
    
ref=
  : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
    {γ₀ : ∣ Γ₀ ∣}{γ₁ : ∣ Γ₁ ∣}(γ₂ : γ₀ ≡[ ∣∣= Γ₂ ]≡ γ₁)
  → ref Γ₀ γ₀ ≡[ ∶~= Γ₂ γ₂ γ₂ ]≡ ref Γ₁ γ₁
ref= refl refl = refl

-- setoid morphism

record _→S_ (Γ Δ : Setoid) : Set where
  field
    map  : ∣ Γ ∣ → ∣ Δ ∣
    resp : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map γ) ~ (map δ)
    nat : (γ : ∣ Γ ∣) → ref Δ (map γ) ≡ resp (ref Γ γ)

infix 3 _→S_

respT=
  : {Γ Δ : Setoid}
    {map₀ map₁ : ∣ Γ ∣ → ∣ Δ ∣}(map₂ : map₀ ≡ map₁)
  → ({γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₀ γ) ~ (map₀ δ))
  ≡ ({γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₁ γ) ~ (map₁ δ))
respT= refl = refl

natT=
  : {Γ Δ : Setoid}
    {map₀ map₁ : ∣ Γ ∣ → ∣ Δ ∣}(map₂ : map₀ ≡ map₁)
    {resp₀ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₀ γ) ~ (map₀ δ)}
    {resp₁ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₁ γ) ~ (map₁ δ)}
    (resp₂ : (λ {γ}{δ} p → resp₀ {γ}{δ} p) ≡[ respT= {Γ}{Δ} map₂ ]≡ (λ {γ}{δ} p → resp₁ {γ}{δ} p))
  → ((γ : ∣ Γ ∣) → ref Δ (map₀ γ) ≡ resp₀ (ref Γ γ))
  ≡ ((γ : ∣ Γ ∣) → ref Δ (map₁ γ) ≡ resp₁ (ref Γ γ))
natT= refl refl = refl

mk→S=
  : {Γ Δ : Setoid}
    {map₀ map₁ : ∣ Γ ∣ → ∣ Δ ∣}(map₂ : map₀ ≡ map₁)
    {resp₀ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₀ γ) ~ (map₀ δ)}
    {resp₁ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₁ γ) ~ (map₁ δ)}
    (resp₂ : (λ {γ}{δ} p → resp₀ {γ}{δ} p) ≡[ respT= {Γ}{Δ} map₂ ]≡ (λ {γ}{δ} p → resp₁ {γ}{δ} p))
    {nat₀ : (γ : ∣ Γ ∣) → ref Δ (map₀ γ) ≡ resp₀ (ref Γ γ)}
    {nat₁ : (γ : ∣ Γ ∣) → ref Δ (map₁ γ) ≡ resp₁ (ref Γ γ)}
    (nat₂ : nat₀ ≡[ natT= map₂ resp₂ ]≡ nat₁)
  → _≡_ {A = Γ →S Δ}
        (record { map = map₀ ; resp = resp₀ ; nat = nat₀ })
        (record { map = map₁ ; resp = resp₁ ; nat = nat₁ })
mk→S= refl refl refl = refl

open _→S_ public

map=
  : {Γ Δ : Setoid}
    {σ₀ σ₁ : Γ →S Δ}(σ₂ : σ₀ ≡ σ₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → map σ₀ γ₀ ≡ map σ₁ γ₁
map= refl refl = refl

resp= 
  : {Γ Δ : Setoid}
    {σ₀ σ₁ : Γ →S Δ}(σ₂ : σ₀ ≡ σ₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
    {δ₀ δ₁ : ∣ Γ ∣}(δ₂ : δ₀ ≡ δ₁)
    {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≡[ ∶~= {Γ} refl γ₂ δ₂ ]≡ p₁)
  → resp σ₀ p₀ ≡[ ∶~= {Δ} refl (map= σ₂ γ₂) (map= σ₂ δ₂) ]≡ resp σ₁ p₁
resp= refl refl refl refl = refl

nat=
  : {Γ Δ : Setoid}
    {σ₀ σ₁ : Γ →S Δ}(σ₂ : σ₀ ≡ σ₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → nat σ₀ γ₀ ≡[ ≡= (∶~= {Δ} refl (map= σ₂ γ₂) (map= σ₂ γ₂)) (ref= {Δ} refl (map= σ₂ γ₂)) (resp= σ₂ γ₂ γ₂ (ref= {Γ} refl γ₂)) ]≡ nat σ₁ γ₁
nat= refl refl = refl

-- family of setoids

record FamSetoid (Γ : Setoid) : Set₁ where
  field
    ∣_∣F_ : ∣ Γ ∣ → Set
    _∶_F_~_ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣_∣F_ γ → ∣_∣F_ δ → Set
    refF : {γ : ∣ Γ ∣}(a : ∣_∣F_ γ) → _∶_F_~_ (ref Γ γ) a a
    
  infix 5 ∣_∣F_
  infix 5 _∶_F_~_

FamSetoid=
  : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
  → FamSetoid Γ₀ ≡ FamSetoid Γ₁
FamSetoid= refl = refl

∶F~T=
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
  → ({γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set)
  ≡ ({γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set)
∶F~T= refl = refl

refFT=
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
    (∶F~₂ : (λ {γ}{δ} → ∶F~₀ {γ}{δ}) ≡[ ∶F~T= {Γ} ∣∣F₂ {∶F~₀}{∶F~₁} ]≡ (λ {γ}{δ} → ∶F~₁ {γ}{δ}))
    {refF₀ : {γ : ∣ Γ ∣}(a : ∣∣F₀ γ) → ∶F~₀ (ref Γ γ) a a}
    {refF₁ : {γ : ∣ Γ ∣}(a : ∣∣F₁ γ) → ∶F~₁ (ref Γ γ) a a}
  → ({γ : ∣ Γ ∣}(a : ∣∣F₀ γ) → ∶F~₀ (ref Γ γ) a a)
  ≡ ({γ : ∣ Γ ∣}(a : ∣∣F₁ γ) → ∶F~₁ (ref Γ γ) a a)
refFT= refl refl = refl

mkFamSetoid=
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
    (∶F~₂ : (λ {γ}{δ} → ∶F~₀ {γ}{δ}) ≡[ ∶F~T= {Γ} ∣∣F₂ {∶F~₀}{∶F~₁} ]≡ (λ {γ}{δ} → ∶F~₁ {γ}{δ}))
    {refF₀ : {γ : ∣ Γ ∣}(a : ∣∣F₀ γ) → ∶F~₀ (ref Γ γ) a a}
    {refF₁ : {γ : ∣ Γ ∣}(a : ∣∣F₁ γ) → ∶F~₁ (ref Γ γ) a a}
    (refF₂ : (λ {γ} → refF₀ {γ}) ≡[ refFT= ∣∣F₂ ∶F~₂ {refF₀}{refF₁} ]≡ (λ {γ} → refF₁ {γ}))
  → _≡_ {A = FamSetoid Γ}
        (record { ∣_∣F_ = ∣∣F₀ ; _∶_F_~_ = ∶F~₀ ; refF = refF₀ })
        (record { ∣_∣F_ = ∣∣F₁ ; _∶_F_~_ = ∶F~₁ ; refF = refF₁ })
mkFamSetoid= refl refl refl = refl

open FamSetoid public

∣∣F=
  : {Γ : Setoid}
    {A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → ∣ A₀ ∣F γ₀ ≡ ∣ A₁ ∣F γ₁
∣∣F= refl refl = refl

∶F~=
  : {Γ : Setoid}
    {A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
    {δ₀ δ₁ : ∣ Γ ∣}(δ₂ : δ₀ ≡ δ₁)
    {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≡[ ∶~= {Γ} refl γ₂ δ₂ ]≡ p₁)
    {a₀ : ∣ A₀ ∣F γ₀}{a₁ : ∣ A₁ ∣F γ₁}(a₂ : a₀ ≡[ ∣∣F= A₂ γ₂ ]≡ a₁)
    {b₀ : ∣ A₀ ∣F δ₀}{b₁ : ∣ A₁ ∣F δ₁}(b₂ : b₀ ≡[ ∣∣F= A₂ δ₂ ]≡ b₁)
  → A₀ ∶ p₀ F a₀ ~ b₀ ≡ A₁ ∶ p₁ F a₁ ~ b₁
∶F~= refl refl refl refl refl refl = refl

refF=
  : {Γ : Setoid}
    {A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
    {a₀ : ∣ A₀ ∣F γ₀}{a₁ : ∣ A₁ ∣F γ₁}(a₂ : a₀ ≡[ ∣∣F= A₂ γ₂ ]≡ a₁)
  → refF A₀ a₀ ≡[ ∶F~= {Γ} A₂ γ₂ γ₂ (ref= {Γ} refl γ₂) a₂ a₂ ]≡ refF A₁ a₁
refF= refl refl refl = refl

∣∣F='
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → ∣∣F₀ γ₀ ≡ ∣∣F₁ γ₁
∣∣F=' refl refl = refl

∣∣F=mkFamSetoid
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
    (∶F~₂ : (λ {γ}{δ} → ∶F~₀ {γ}{δ}) ≡[ ∶F~T= {Γ} ∣∣F₂ {∶F~₀}{∶F~₁} ]≡ (λ {γ}{δ} → ∶F~₁ {γ}{δ}))
    {refF₀ : {γ : ∣ Γ ∣}(a : ∣∣F₀ γ) → ∶F~₀ (ref Γ γ) a a}
    {refF₁ : {γ : ∣ Γ ∣}(a : ∣∣F₁ γ) → ∶F~₁ (ref Γ γ) a a}
    (refF₂ : (λ {γ} → refF₀ {γ}) ≡[ refFT= ∣∣F₂ ∶F~₂ {refF₀}{refF₁} ]≡ (λ {γ} → refF₁ {γ}))
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → _≡_ {A = ∣∣F₀ γ₀ ≡ ∣∣F₁ γ₁}
        (∣∣F= (mkFamSetoid= ∣∣F₂ ∶F~₂ refF₂) γ₂)
        (∣∣F=' {Γ} ∣∣F₂ γ₂)
∣∣F=mkFamSetoid refl refl refl refl = refl

-- section of family of setoids

record _→F_ (Γ : Setoid)(A : FamSetoid Γ) : Set where
  field
    mapF  : (γ : ∣ Γ ∣) → ∣ A ∣F γ
    respF : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF γ) ~ (mapF δ)
    natF : (γ : ∣ Γ ∣) → refF A (mapF γ) ≡ respF (ref Γ γ)

infix 3 _→F_

Γ→F= : {Γ : Setoid}{A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
     → (Γ →F A₀) ≡ (Γ →F A₁)
Γ→F= {Γ} = ap (Γ →F_)

respFT=
  : {Γ : Setoid}{A : FamSetoid Γ}
    {mapF₀ mapF₁ : (γ : ∣ Γ ∣) → ∣ A ∣F γ}(mapF₂ : mapF₀ ≡ mapF₁)
    {respF₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₀ γ) ~ (mapF₀ δ)}
    {respF₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₁ γ) ~ (mapF₁ δ)}
  → ({γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₀ γ) ~ (mapF₀ δ))
  ≡ ({γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₁ γ) ~ (mapF₁ δ))
respFT= refl = refl

natFT=
  : {Γ : Setoid}{A : FamSetoid Γ}
    {mapF₀ mapF₁ : (γ : ∣ Γ ∣) → ∣ A ∣F γ}(mapF₂ : mapF₀ ≡ mapF₁)
    {respF₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₀ γ) ~ (mapF₀ δ)}
    {respF₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₁ γ) ~ (mapF₁ δ)}
    (respF₂ : (λ {γ}{δ} → respF₀ {γ}{δ}) ≡[ respFT= {Γ}{A} mapF₂ {respF₀}{respF₁} ]≡ (λ {γ}{δ} → respF₁ {γ}{δ}))
    {natF₀ : (γ : ∣ Γ ∣) → refF A (mapF₀ γ) ≡ respF₀ (ref Γ γ)}
    {natF₁ : (γ : ∣ Γ ∣) → refF A (mapF₁ γ) ≡ respF₁ (ref Γ γ)}
  → ((γ : ∣ Γ ∣) → refF A (mapF₀ γ) ≡ respF₀ (ref Γ γ))
  ≡ ((γ : ∣ Γ ∣) → refF A (mapF₁ γ) ≡ respF₁ (ref Γ γ))
natFT= refl refl = refl

mk→F=
  : {Γ : Setoid}{A : FamSetoid Γ}
    {mapF₀ mapF₁ : (γ : ∣ Γ ∣) → ∣ A ∣F γ}(mapF₂ : mapF₀ ≡ mapF₁)
    {respF₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₀ γ) ~ (mapF₀ δ)}
    {respF₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₁ γ) ~ (mapF₁ δ)}
    (respF₂ : (λ {γ}{δ} → respF₀ {γ}{δ}) ≡[ respFT= {Γ}{A} mapF₂ {respF₀}{respF₁} ]≡ (λ {γ}{δ} → respF₁ {γ}{δ}))
    {natF₀ : (γ : ∣ Γ ∣) → refF A (mapF₀ γ) ≡ respF₀ (ref Γ γ)}
    {natF₁ : (γ : ∣ Γ ∣) → refF A (mapF₁ γ) ≡ respF₁ (ref Γ γ)}
    (natF₂ : natF₀ ≡[ natFT= {Γ}{A} mapF₂ respF₂ {natF₀}{natF₁} ]≡ natF₁)
  → _≡_ {A = Γ →F A}
        (record { mapF = mapF₀ ; respF = respF₀ ; natF = natF₀ })
        (record { mapF = mapF₁ ; respF = respF₁ ; natF = natF₁ })
mk→F= refl refl refl = refl

open _→F_ public

mapFcoe
  : {Γ : Setoid}{A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {t : Γ →F A₀}{γ : ∣ Γ ∣}
  → mapF (coe (Γ→F= A₂) t) γ
  ≡ coe (∣∣F= A₂ refl) (mapF t γ)
mapFcoe refl = refl

respFcoe
  : {Γ : Setoid}{A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {t : Γ →F A₀}{γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)
  → respF (coe (Γ→F= A₂) t) p
  ≡ coe (∶F~= {Γ} A₂ {γ} refl {δ} refl {p} refl {mapF t γ}{mapF (coe (Γ→F= A₂) t) γ} (mapFcoe A₂ ⁻¹) {mapF t δ}{mapF (coe (Γ→F= A₂) t) δ} (mapFcoe A₂ ⁻¹))
        (respF t p)
respFcoe refl p = refl

-- fixities

infixl 5 _,S_
infixl 7 _[_]TS
infixl 5 _,sS_
infix  6 _∘S_
infixl 8 _[_]tS

-- trivial setoid

⊤S : Setoid
⊤S = record
  { ∣_∣   = ⊤
  ; _∶_~_ = λ _ _ → ⊤
  ; ref   = λ _ → tt
  }

-- setoid comprehension

_,S_ : (Γ : Setoid) → FamSetoid Γ → Setoid
Γ ,S A = record
  { ∣_∣   = Σ ∣ Γ ∣ ∣ A ∣F_
  ; _∶_~_ = λ { (γ₀ ,Σ a₀) (γ₁ ,Σ a₁) → Σ (Γ ∶ γ₀ ~ γ₁) λ γ₂ → A ∶ γ₂ F a₀ ~ a₁ }
  ; ref   = λ { (γ ,Σ a) → (ref Γ γ) ,Σ refF A a }
  }

,S=
  : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
    {A₀ : FamSetoid Γ₀}{A₁ : FamSetoid Γ₁}(A₂ : A₀ ≡[ FamSetoid= Γ₂ ]≡ A₁)
  → Γ₀ ,S A₀ ≡ Γ₁ ,S A₁
,S= refl refl = refl

-- substitution of families of setoids

_[_]TS : {Γ : Setoid}{Θ : Setoid}(A : FamSetoid Γ) → Θ →S Γ → FamSetoid Θ
A [ σ ]TS = record
  { ∣_∣F_   = λ θ → ∣ A ∣F map σ θ
  ; _∶_F_~_ = λ θ₂ a₀ a₁ → A ∶ resp σ θ₂ F a₀ ~ a₁
  ; refF    = λ {γ} a → coe (ap (λ z → A ∶ z F a ~ a) (nat σ γ)) (refF A a)
  }

-- identity setoid morphism

idS : {Γ : Setoid} → Γ →S Γ
idS {Γ} = record
  { map  = λ γ → γ
  ; resp = λ γ₂ → γ₂
  ; nat  = λ _ → refl
  }

-- setoid morphism composition

_∘S_ : {Γ Δ Σ : Setoid} → Δ →S Σ → Γ →S Δ → Γ →S Σ
_∘S_ {Γ}{Δ}{Σ} σ ν = record
  { map  = λ γ → map σ (map ν γ)
  ; resp = λ γ₂ → resp σ (resp ν γ₂)
  ; nat  = λ γ → nat σ (map ν γ) ◾ ap (resp σ) (nat ν γ)
  }
  
-- 

εS : {Γ : Setoid} → Γ →S ⊤S
εS {Γ} = record { map = λ _ → tt ; resp = λ _ → tt ; nat = λ _ → refl }

-- setoid morphism extension

_,sS_ : {Γ Δ : Setoid}(σ : Γ →S Δ){A : FamSetoid Δ}
      → Γ →F (A [ σ ]TS) → Γ →S (Δ ,S A)
_,sS_ {Γ}{Δ} σ {A} t = record
  { map  = λ γ → (map σ γ) ,Σ (mapF t γ)
  ; resp = λ γ₂ → (resp σ γ₂) ,Σ respF t γ₂
  ; nat  = λ γ → ,Σ= (nat σ γ) (natF t γ)
  }

-- first projection

π₁S : {Γ Δ : Setoid}{A : FamSetoid Δ}(σ : Γ →S (Δ ,S A))
    → Γ →S Δ
π₁S σ = record
  { map  = λ γ → proj₁ (map σ γ)
  ; resp = λ γ₂ → proj₁ (resp σ γ₂)
  ; nat  = λ γ → ,Σ=0 (nat σ γ)
  }

-- substitution of sections

_[_]tS : {Γ Δ : Setoid}{A : FamSetoid Δ}
         (t : Δ →F A)(σ : Γ →S Δ) → Γ →F (A [ σ ]TS)
_[_]tS {Γ}{Δ}{A} t σ = record
  { mapF  = λ γ → mapF t (map σ γ)
  ; respF = λ γ₂ → respF t (resp σ γ₂)
  ; natF  = λ γ → ap (λ z → coe (ap (λ z → A ∶ z F mapF t (map σ γ) ~ mapF t (map σ γ)) (nat σ γ)) z)
                     (natF t (map σ γ))
                ◾ apd (respF t) (nat σ γ)
  }

-- second projection

π₂S : {Γ Δ : Setoid}{A : FamSetoid Δ}(σ : Γ →S (Δ ,S A))
    → Γ →F (A [ π₁S σ ]TS)
π₂S {Γ}{Δ}{A} σ = record
  { mapF  = λ γ → proj₂ (map σ γ)
  ; respF = λ γ₂ → proj₂ (resp σ γ₂)
  ; natF  = λ γ → ,Σ=1 (nat σ γ)
  }

-- equalities

abstract
  [id]TS : {Γ : Setoid}{A : FamSetoid Γ} → A [ idS ]TS ≡ A
  [id]TS = refl

module [][]TS
  {Γ Δ Σ : Setoid}(A : FamSetoid Σ)(σ : Γ →S Δ)(δ : Δ →S Σ)
  where
    pnatF
      : (λ {γ} a → coe (ap (λ z → A ∶ resp δ z F a ~ a) (nat σ γ)) (coe (ap (λ z → A ∶ z F a ~ a) (nat δ (map σ γ))) (refF A a)))
      ≡ (λ {γ} a → coe (ap (λ z → A ∶ z F a ~ a) (nat δ (map σ γ) ◾ ap (resp δ) (nat σ γ))) (refF A a))
    pnatF
      = funexti λ γ → funext λ a
      → coecoe (ap (λ z → A ∶ z F a ~ a) (nat δ (map σ γ)))
               (ap (λ z → A ∶ resp δ z F a ~ a) (nat σ γ))
      ◾ ap (λ z → coe z (refF A a))
           {ap (λ z → A ∶ z F a ~ a) (nat δ (map σ γ)) ◾ ap (λ z → A ∶ resp δ z F a ~ a) (nat σ γ)}
           {ap (λ z → A ∶ z F a ~ a) (nat (δ ∘S σ) γ)}
           ( ap (λ z → ap (λ z → A ∶ z F a ~ a) (nat δ (map σ γ)) ◾ z)
                (apap (nat σ γ))
           ◾ ap◾ (λ z → A ∶ z F a ~ a) (nat δ (map σ γ)) (ap (resp δ) (nat σ γ)) ⁻¹ )

    ret : A [ δ ]TS [ σ ]TS ≡ A [ δ ∘S σ ]TS
    ret = mkFamSetoid=
        (refl {x = λ θ → ∣ A ∣F map δ (map σ θ)})
        (refl {x = λ {_}{_} θ₂ → _∶_F_~_ A (resp δ (resp σ θ₂))})
        pnatF

abstract
  idlS : {Γ Δ : Setoid}{δ : Γ →S Δ} → idS ∘S δ ≡ δ
  idlS {Γ}{Δ}{δ}
    = mk→S=
        refl
        refl
        (funext λ γ → ◾lid (ap (λ γ₂ → γ₂) (nat δ γ)) ◾ apid (nat δ γ))


abstract
  idrS : {Γ Δ : Setoid}{δ : Γ →S Δ} → δ ∘S idS ≡ δ
  idrS {Γ}{Δ}{δ}
    = mk→S= refl refl
        (funext λ γ → ◾rid (nat δ γ))

abstract
  assS
    : {Δ Γ Σ Ω : Setoid}{σ : Σ →S Ω}{δ : Γ →S Σ}{ν : Δ →S Γ}
    → (σ ∘S δ) ∘S ν ≡ σ ∘S (δ ∘S ν)
  assS {Δ}{Γ}{Σ}{Ω}{σ}{δ}{ν}
    = mk→S= refl refl
        (funext λ γ → ◾ass _ _ _ ⁻¹
                    ◾ ap (λ z → (nat σ (map δ (map ν _)) ◾ z))
                         ( ap (λ z → ap (resp σ) (nat δ (map ν _)) ◾ z)
                              (apap (nat ν _))
                         ◾ ap◾ (resp σ) (nat δ (map ν _)) (ap (resp δ) (nat ν _)) ⁻¹))

coerespT=
  : {Γ Δ : Setoid}
    {map₀ map₁ : ∣ Γ ∣ → ∣ Δ ∣}(map₂ : map₀ ≡ map₁)
    {resp' : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₀ γ) ~ (map₀ δ)}
    {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)
  → coe (respT= {Γ}{Δ} map₂) resp' p
  ≡ coe (∶~= {Δ} refl (ap (λ z → z γ) map₂) (ap (λ z → z δ) map₂)) (resp' p)
coerespT= refl p = refl

module ,∘S
  {Γ Δ Σ : Setoid}{δ : Γ →S Δ}{σ : Σ →S Γ}{A : FamSetoid Δ}{a : Γ →F (A [ δ ]TS)}
  where
    abstract
      map₂ : (λ γ → map (_,sS_ δ {A} a) (map σ γ))
           ≡ (λ γ → map (δ ∘S σ) γ ,Σ mapF (coe (Γ→F= ([][]TS.ret A σ δ)) (a [ σ ]tS)) γ)
      map₂
        = funext λ γ →
            ,Σ= refl
                ( ap
                     (λ z → coe z (mapF a (map σ γ)))
                     {refl {x = ∣ A ∣F map δ (map σ γ)}}
                     {∣∣F= {Σ}{A [ δ ]TS [ σ ]TS}{A [ δ ∘S σ ]TS}
                           (mkFamSetoid= refl refl ([][]TS.pnatF A σ δ)) refl}
                     (∣∣F=mkFamSetoid {Σ}{∣ A [ δ ∘S σ ]TS ∣F_} refl refl ([][]TS.pnatF A σ δ) refl ⁻¹)
                ◾ mapFcoe ([][]TS.ret A σ δ) {a [ σ ]tS}{γ} ⁻¹)

    abstract
      resp₂
        : (λ {_}{_} γ₂ → resp (_,sS_ δ {A} a) (resp {Σ}{Γ} σ γ₂))
        ≡[ respT= {Σ}{Δ ,S A} map₂ ]≡
          (λ {_}{_} γ₂ → resp δ (resp σ γ₂) ,Σ respF (coe (Γ→F= (mkFamSetoid= refl refl ([][]TS.pnatF A σ δ))) (a [ σ ]tS)) γ₂)
      resp₂
        = funexti λ γ → funexti λ δ → funext λ p →
              coerespT= map₂ p
            ◾ cheat

    abstract
      nat₂
        : (λ γ → nat (_,sS_ δ {A} a) (map σ γ) ◾ ap (resp (_,sS_ δ {A} a)) (nat σ γ))
        ≡[ natT= {Σ}{Δ ,S A} map₂ resp₂ ]≡
          (λ γ → ,Σ= (nat (δ ∘S σ) γ) (natF (coe (Γ→F= ([][]TS.ret A σ δ)) (a [ σ ]tS)) γ))
      nat₂
        = funext λ γ →
            cheat

    ret : (_,sS_ δ {A} a) ∘S σ ≡ (δ ∘S σ) ,sS coe (Γ→F= ([][]TS.ret A σ δ)) (a [ σ ]tS)
    ret = mk→S= map₂ resp₂ nat₂

abstract
  π₁βS
    : {Γ Δ : Setoid}{A : FamSetoid Δ}{δ : Γ →S Δ}{a : Γ →F (A [ δ ]TS)}
    → π₁S (_,sS_ δ {A} a) ≡ δ
  π₁βS {Γ}{Δ}{A}{δ}{a}
    = mk→S= refl refl
        (funext λ γ → ,Σ=β0 (nat δ γ) (natF a γ))

abstract
  πηS
    : {Γ Δ : Setoid}{A : FamSetoid Δ}{δ : Γ →S (Δ ,S A)}
    → π₁S δ ,sS π₂S δ ≡ δ
  πηS {Γ}{Δ}{A}{δ}
    = mk→S= refl refl
        (funext λ γ → ,Σ=η (nat δ γ))

abstract
  εη : {Γ : Setoid}{σ : Γ →S ⊤S} → σ ≡ εS
  εη {Γ}{σ}
    = mk→S= refl refl
        (funext λ γ → set⊤ (nat σ γ) refl)

map='
  : {Γ Δ : Setoid}
    {map₀ map₁ : ∣ Γ ∣ → ∣ Δ ∣}(map₂ : map₀ ≡ map₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → map₀ γ₀ ≡ map₁ γ₁
map=' refl refl = refl

∣∣F=mk→S
  : {Γ Δ : Setoid}
    {map₀ map₁ : ∣ Γ ∣ → ∣ Δ ∣}(map₂ : map₀ ≡ map₁)
    {resp₀ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₀ γ) ~ (map₀ δ)}
    {resp₁ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₁ γ) ~ (map₁ δ)}
    (resp₂ : (λ {γ}{δ} p → resp₀ {γ}{δ} p) ≡[ respT= {Γ}{Δ} map₂ ]≡ (λ {γ}{δ} p → resp₁ {γ}{δ} p))
    {nat₀ : (γ : ∣ Γ ∣) → ref Δ (map₀ γ) ≡ resp₀ (ref Γ γ)}
    {nat₁ : (γ : ∣ Γ ∣) → ref Δ (map₁ γ) ≡ resp₁ (ref Γ γ)}
    (nat₂ : nat₀ ≡[ natT= map₂ resp₂ ]≡ nat₁)
    {A : FamSetoid Δ}
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → ∣∣F= (ap (_[_]TS A) (mk→S= {Γ}{Δ} map₂ resp₂ nat₂)) γ₂
  ≡ ap (∣ A ∣F_) (map=' {Γ}{Δ} map₂ γ₂)
∣∣F=mk→S refl refl refl refl = refl

module π₂βS
  {Γ Δ : Setoid}{A : FamSetoid Δ}{δ : Γ →S Δ}{a : Γ →F (A [ δ ]TS)}
  where
    abstract
      mapF₂
        : mapF (coe (Γ→F= (ap (_[_]TS A) (π₁βS {Γ}{Δ}{A}{δ}{a}))) (π₂S (_,sS_ δ {A} a)))
        ≡ mapF a
      mapF₂
        = funext λ γ →
              mapFcoe (ap (_[_]TS A) (π₁βS {Γ}{Δ}{A}{δ}{a})){(π₂S (_,sS_ δ {A} a))}{γ}
            ◾ ap (λ z → coe z (mapF a γ))
                 {∣∣F= (ap (_[_]TS A) (π₁βS {Γ}{Δ}{A}{δ}{a})) refl}
                 {refl {x = ∣ A ∣F map δ γ}}
                 (∣∣F=mk→S
                    {Γ}{Δ}
                    (refl {x = map δ})
                    (refl {x = λ {_}{_} z → resp δ z})
                    (funext λ γ → ,Σ=β0 (nat δ γ) (natF a γ))
                    {A}
                    refl)
{-
    abstract
      resp₂
        : {!!}
      resp₂
        = {!!}
-}
    ret : π₂S (_,sS_ δ {A} a) ≡[ Γ→F= (ap (_[_]TS A) (π₁βS {Γ}{Δ}{A}{δ}{a})) ]≡ a
    ret = mk→F= mapF₂ cheat cheat
