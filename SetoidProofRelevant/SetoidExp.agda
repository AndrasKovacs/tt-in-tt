{-# OPTIONS --without-K #-}

module Setoid.SetoidExp where

open import lib

open import Setoid.Setoid

-- setoid exponential

_^S_ : {Γ Δ : Setoid}(σ : Γ →S Δ)(A : FamSetoid Δ) → (Γ ,S A [ σ ]TS) →S (Δ ,S A)
σ ^S A = (σ ∘S π₁S idS) ,sS coe (Γ→F= ([][]TS.ret A (π₁S idS) σ)) (π₂S idS)

infixl 5 _^S_

SetoidExp : {Γ : Setoid}(A : FamSetoid Γ)(B : FamSetoid (Γ ,S A))(γ : ∣ Γ ∣) → Set
SetoidExp {Γ} A B γ
  = Σ ((x : ∣ A ∣F γ) → ∣ B ∣F (γ ,Σ x)) λ mapE
  → Σ ({x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE x ~ mapE y) λ respE
  → ((x : ∣ A ∣F γ) → refF B (mapE x) ≡ respE (refF A x))

mapE
  : {Γ : Setoid}{A : FamSetoid Γ}(B : FamSetoid (Γ ,S A)){γ : ∣ Γ ∣}
    (w : SetoidExp A B γ) → ((x : ∣ A ∣F γ) → ∣ B ∣F (γ ,Σ x))
mapE _ = proj₁
respE
  : {Γ : Setoid}{A : FamSetoid Γ}(B : FamSetoid (Γ ,S A)){γ : ∣ Γ ∣}
   (w : SetoidExp A B γ) → ({x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE B w x ~ mapE B w y)
respE _ w = proj₁ (proj₂ w)
natE
  : {Γ : Setoid}{A : FamSetoid Γ}(B : FamSetoid (Γ ,S A)){γ : ∣ Γ ∣}
    (w : SetoidExp A B γ) → ((x : ∣ A ∣F γ) → refF B (mapE B w x) ≡ respE B w (refF A x))
natE _ w = proj₂ (proj₂ w)

respET=
  : {Γ : Setoid}{A : FamSetoid Γ}(B : FamSetoid (Γ ,S A)){γ : ∣ Γ ∣}
    {mapE₀ mapE₁ : (x : ∣ A ∣F γ) → ∣ B ∣F (γ ,Σ x)}(mapE₂ : mapE₀ ≡ mapE₁)
  → ({x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE₀ x ~ mapE₀ y)
  ≡ ({x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE₁ x ~ mapE₁ y)
respET= _ refl = refl

natET=
  : {Γ : Setoid}{A : FamSetoid Γ}(B : FamSetoid (Γ ,S A)){γ : ∣ Γ ∣}
    {mapE₀ mapE₁ : (x : ∣ A ∣F γ) → ∣ B ∣F (γ ,Σ x)}(mapE₂ : mapE₀ ≡ mapE₁)
    {respE₀ : {x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE₀ x ~ mapE₀ y}
    {respE₁ : {x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE₁ x ~ mapE₁ y}
    (respE₂ : (λ {x}{y} p → respE₀ {x}{y} p) ≡[ respET= B mapE₂ ]≡ (λ {x}{y} p → respE₁ {x}{y} p))
  → ((x : ∣ A ∣F γ) → refF B (mapE₀ x) ≡ respE₀ (refF A x))
  ≡ ((x : ∣ A ∣F γ) → refF B (mapE₁ x) ≡ respE₁ (refF A x))
natET= _ refl refl = refl

mkSetoidExp=
  : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,S A)}{γ : ∣ Γ ∣}
    {mapE₀ mapE₁ : (x : ∣ A ∣F γ) → ∣ B ∣F (γ ,Σ x)}(mapE₂ : mapE₀ ≡ mapE₁)
    {respE₀ : {x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE₀ x ~ mapE₀ y}
    {respE₁ : {x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE₁ x ~ mapE₁ y}
    (respE₂ : (λ {x}{y} p → respE₀ {x}{y} p) ≡[ respET= B mapE₂ ]≡ (λ {x}{y} p → respE₁ {x}{y} p))
    {natE₀ : (x : ∣ A ∣F γ) → refF B (mapE₀ x) ≡ respE₀ (refF A x)}
    {natE₁ : (x : ∣ A ∣F γ) → refF B (mapE₁ x) ≡ respE₁ (refF A x)}
    (natE₂ : natE₀ ≡[ natET= B mapE₂ respE₂ ]≡ natE₁)
  → _≡_ {A = SetoidExp A B γ}
        (mapE₀ ,Σ (respE₀ ,Σ natE₀))
        (mapE₁ ,Σ (respE₁ ,Σ natE₁))
mkSetoidExp= refl refl refl = refl

ΠS : {Γ : Setoid}(A : FamSetoid Γ)(B : FamSetoid (Γ ,S A)) → FamSetoid Γ
ΠS {Γ} A B
  = record
      { ∣_∣F_   = SetoidExp A B
      ; _∶_F_~_ = λ {γ}{δ} p f g → {x : ∣ A ∣F γ}{y : ∣ A ∣F δ}(q : A ∶ p F x ~ y) → B ∶ p ,Σ q F mapE B f x ~ mapE B g y
      ; refF    = respE B
      }

Π[]S : {Γ Δ : Setoid}{σ : Γ →S Δ}{A : FamSetoid Δ}{B : FamSetoid (Δ ,S A)}
     → ΠS A B [ σ ]TS ≡ ΠS (A [ σ ]TS) (B [ σ ^S A ]TS)
Π[]S {Γ}{Δ}{σ}{A}{B}
  = mkFamSetoid=
      (funext λ γ → Σ= (→= refl (λ { {_} refl → ∣∣F= {A₀ = B} refl (,Σ= refl ({!!} ◾ mapFcoe ([][]TS.ret A (π₁S idS) σ){π₂S idS} ⁻¹)) }))
                       {!!})
      {!!}
      {!!}

lamS : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,S A)} → (Γ ,S A) →F B → Γ →F ΠS A B
lamS {Γ}{A}{B} t
  = record
      { mapF
          = λ γ →  (λ x → mapF t (γ ,Σ x))
                ,Σ ((λ {x}{y} p → respF t (ref Γ γ ,Σ p))
                ,Σ (λ x → natF t (γ ,Σ x)))
      ; respF = λ p q → respF t (p ,Σ q)
      ; natF  = λ γ → funexti λ x → funexti λ y → funext λ p → refl
      }

appS : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,S A)} → Γ →F ΠS A B → (Γ ,S A) →F B
appS {Γ}{A}{B} f
  = record
      { mapF  = λ { (γ ,Σ x) → mapE B (mapF f γ) x }
      ; respF = λ { (p ,Σ q) → respF f p q }
      ; natF  = λ { (γ ,Σ x) → natE B (mapF f γ) x ◾ ap (λ z → z (refF A x)) (natF f γ) }
      }

lam[] : {Γ Δ : Setoid}{σ : Γ →S Δ}{A : FamSetoid Δ}{B : FamSetoid (Δ ,S A)}{t :(Δ ,S A) →F B}
      → lamS t [ σ ]tS ≡[ Γ→F= (Π[]S {Γ}{Δ}{σ}{A}{B}) ]≡ lamS (t [ σ ^S A ]tS)
lam[] = {!!}

ΠβS : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,S A)}{t : (Γ ,S A) →F B}
    → appS (lamS t) ≡ t
ΠβS {Γ}{A}{B}{t}
  = mk→F=
      (funext λ { (γ ,Σ x) → refl })
      {!!}
      {!!}

ΠηS : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,S A)}{f : Γ →F ΠS A B}
    → lamS (appS {B = B} f) ≡ f
ΠηS {Γ}{A}{B}{f}
  = mk→F=
      (funext λ γ → {!!})
      {!!}
      {!!}
