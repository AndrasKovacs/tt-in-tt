{-# OPTIONS --without-K #-}

module Setoid.Model where

open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.Func

open import Setoid.Setoid

d : Decl
d = record
  { Con = Setoid
  ; Ty  = FamSetoid
  ; Tms = _→S_
  ; Tm  = _→F_
  }

c : Core d
c = record
  { •     = ⊤S
  ; _,_   = _,S_
  ; _[_]T = _[_]TS
  ; id    = idS
  ; _∘_   = _∘S_
  ; ε     = εS
  ; _,s_  = _,sS_
  ; π₁    = π₁S
  ; _[_]t = _[_]tS
  ; π₂    = π₂S
  ; [id]T = [id]TS
  ; [][]T = λ {_}{_}{_}{A}{σ}{δ} → [][]TS.ret A σ δ
  ; idl   = idlS
  ; idr   = idrS
  ; ass   = λ {_}{_}{_}{_}{σ}{δ}{ν} → assS {_}{_}{_}{_}{σ}{δ}{ν}
  ; ,∘    = λ {_}{_}{_}{δ}{σ}{A}{a} → ,∘S.ret {_}{_}{_}{δ}{σ}{A}{a}
  ; π₁β   = λ {_}{_}{A}{δ}{a} → π₁βS {_}{_}{A}{δ}{a}
  ; πη    = πηS
  ; εη    = εη
  ; π₂β   = π₂βS.ret
  }

f : Func d c
f = record
  { Π     = {!!}
  ; Π[]   = {!!}
  ; lam   = {!!}
  ; app   = {!!}
  ; lam[] = {!!}
  ; Πβ    = {!!}
  ; Πη    = {!!}
  }
