{-# OPTIONS --without-K #-}

module Setoid.Setoid1 where

open import lib

-- setoid

record Setoid : Set₁ where
  field
    ∣_∣ : Set
    _∶_~_ : ∣_∣ → ∣_∣ → Set
    ref : (γ : ∣_∣) → _∶_~_ γ γ

  infix 4 ∣_∣
  infix 5 _∶_~_

postulate
  mkSetoid=
    : {∣∣₀ ∣∣₁ : Set}(∣∣₂ : ∣∣₀ ≡ ∣∣₁)
      {_~₀_ : ∣∣₀ → ∣∣₀ → Set}{_~₁_ : ∣∣₁ → ∣∣₁ → Set}
      (~₂ : {γ₀ : ∣∣₀}{γ₁ : ∣∣₁}(γ₂ : γ₀ ≡[ ∣∣₂ ]≡ γ₁)
            {δ₀ : ∣∣₀}{δ₁ : ∣∣₁}(δ₂ : δ₀ ≡[ ∣∣₂ ]≡ δ₁)
          → (γ₀ ~₀ δ₀) ≡ (γ₁ ~₁ δ₁))
      {ref₀ : (γ : ∣∣₀) → γ ~₀ γ}{ref₁ : (γ : ∣∣₁) → γ ~₁ γ}
      (ref₂ : {γ₀ : ∣∣₀}{γ₁ : ∣∣₁}(γ₂ : γ₀ ≡[ ∣∣₂ ]≡ γ₁)
            → ref₀ γ₀ ≡[ ~₂ γ₂ γ₂ ]≡ ref₁ γ₁)
    → _≡_ {A = Setoid}
          (record { ∣_∣ = ∣∣₀ ; _∶_~_ = _~₀_ ; ref = ref₀ })
          (record { ∣_∣ = ∣∣₁ ; _∶_~_ = _~₁_ ; ref = ref₁ })

open Setoid

∣∣= : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
   → ∣ Γ₀ ∣ ≡ ∣ Γ₁ ∣
∣∣= refl = refl

∶~=
  : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
    {γ₀ : ∣ Γ₀ ∣}{γ₁ : ∣ Γ₁ ∣}(γ₂ : γ₀ ≡[ ∣∣= Γ₂ ]≡ γ₁)
    {δ₀ : ∣ Γ₀ ∣}{δ₁ : ∣ Γ₁ ∣}(δ₂ : δ₀ ≡[ ∣∣= Γ₂ ]≡ δ₁)
  → Γ₀ ∶ γ₀ ~ δ₀ ≡ Γ₁ ∶ γ₁ ~ δ₁
∶~= refl refl refl = refl
    
ref=
  : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
    {γ₀ : ∣ Γ₀ ∣}{γ₁ : ∣ Γ₁ ∣}(γ₂ : γ₀ ≡[ ∣∣= Γ₂ ]≡ γ₁)
  → ref Γ₀ γ₀ ≡[ ∶~= Γ₂ γ₂ γ₂ ]≡ ref Γ₁ γ₁
ref= refl refl = refl

-- setoid morphism

record _→S_ (Γ Δ : Setoid) : Set where
  field
    map  : ∣ Γ ∣ → ∣ Δ ∣
    resp : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map γ) ~ (map δ)
    nat : (γ : ∣ Γ ∣) → ref Δ (map γ) ≡ resp (ref Γ γ)

infix 3 _→S_

postulate
  mk→S=
    : {Γ Δ : Setoid}
      {map₀ map₁ : ∣ Γ ∣ → ∣ Δ ∣}(map₂ : {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁) → map₀ γ₀ ≡ map₁ γ₁)
      {resp₀ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₀ γ) ~ (map₀ δ)}
      {resp₁ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₁ γ) ~ (map₁ δ)}
      (resp₂ : {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁){δ₀ δ₁ : ∣ Γ ∣}(δ₂ : δ₀ ≡ δ₁)
               {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≡[ ∶~= {Γ} refl γ₂ δ₂ ]≡ p₁)
             → resp₀ p₀ ≡[ ∶~= {Δ} refl (map₂ γ₂) (map₂ δ₂) ]≡ resp₁ p₁)
      {nat₀ : (γ : ∣ Γ ∣) → ref Δ (map₀ γ) ≡ resp₀ (ref Γ γ)}
      {nat₁ : (γ : ∣ Γ ∣) → ref Δ (map₁ γ) ≡ resp₁ (ref Γ γ)}
      (nat₂ : {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
            → nat₀ γ₀ ≡[ ≡= (∶~= {Δ} refl (map₂ γ₂) (map₂ γ₂)) (ref= {Δ} refl (map₂ γ₂)) (resp₂ γ₂ γ₂ (ref= {Γ} refl γ₂)) ]≡ nat₁ γ₁)
    → _≡_ {A = Γ →S Δ}
          (record { map = map₀ ; resp = resp₀ ; nat = nat₀ })
          (record { map = map₁ ; resp = resp₁ ; nat = nat₁ })

open _→S_

map=
  : {Γ Δ : Setoid}
    {σ₀ σ₁ : Γ →S Δ}(σ₂ : σ₀ ≡ σ₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → map σ₀ γ₀ ≡ map σ₁ γ₁
map= refl refl = refl

resp= 
  : {Γ Δ : Setoid}
    {σ₀ σ₁ : Γ →S Δ}(σ₂ : σ₀ ≡ σ₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
    {δ₀ δ₁ : ∣ Γ ∣}(δ₂ : δ₀ ≡ δ₁)
    {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≡[ ∶~= {Γ} refl γ₂ δ₂ ]≡ p₁)
  → resp σ₀ p₀ ≡[ ∶~= {Δ} refl (map= σ₂ γ₂) (map= σ₂ δ₂) ]≡ resp σ₁ p₁
resp= refl refl refl refl = refl

nat=
  : {Γ Δ : Setoid}
    {σ₀ σ₁ : Γ →S Δ}(σ₂ : σ₀ ≡ σ₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → nat σ₀ γ₀ ≡[ ≡= (∶~= {Δ} refl (map= σ₂ γ₂) (map= σ₂ γ₂)) (ref= {Δ} refl (map= σ₂ γ₂)) (resp= σ₂ γ₂ γ₂ (ref= {Γ} refl γ₂)) ]≡ nat σ₁ γ₁
nat= refl refl = refl

-- family of setoids

record FamSetoid (Γ : Setoid) : Set₁ where
  field
    ∣_∣F_ : ∣ Γ ∣ → Set
    _∶_F_~_ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣_∣F_ γ → ∣_∣F_ δ → Set
    refF : {γ : ∣ Γ ∣}(a : ∣_∣F_ γ) → _∶_F_~_ (ref Γ γ) a a
    
  infix 5 ∣_∣F_
  infix 5 _∶_F_~_

postulate
  mkFamSetoid=
    : {Γ : Setoid}
      {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁) → ∣∣F₀ γ₀ ≡ ∣∣F₁ γ₁)
      {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
      {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
      (∶F~₂ : {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
              {δ₀ δ₁ : ∣ Γ ∣}(δ₂ : δ₀ ≡ δ₁)
              {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≡[ ∶~= {Γ} refl γ₂ δ₂ ]≡ p₁)
              {a₀ : ∣∣F₀ γ₀}{a₁ : ∣∣F₁ γ₁}(a₂ : a₀ ≡[ ∣∣F₂ γ₂ ]≡ a₁)
              {b₀ : ∣∣F₀ δ₀}{b₁ : ∣∣F₁ δ₁}(b₂ : b₀ ≡[ ∣∣F₂ δ₂ ]≡ b₁)
            → ∶F~₀ p₀ a₀ b₀ ≡ ∶F~₁ p₁ a₁ b₁)
      {refF₀ : {γ : ∣ Γ ∣}(a : ∣∣F₀ γ) → ∶F~₀ (ref Γ γ) a a}
      {refF₁ : {γ : ∣ Γ ∣}(a : ∣∣F₁ γ) → ∶F~₁ (ref Γ γ) a a}
      (refF₂ : {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
               {a₀ : ∣∣F₀ γ₀}{a₁ : ∣∣F₁ γ₁}(a₂ : a₀ ≡[ ∣∣F₂ γ₂ ]≡ a₁)
             → refF₀ a₀ ≡[ ∶F~₂ _ _ (ref= {Γ} refl γ₂) a₂ a₂ ]≡ refF₁ a₁)
    → _≡_ {A = FamSetoid Γ}
          (record { ∣_∣F_ = ∣∣F₀ ; _∶_F_~_ = ∶F~₀ ; refF = refF₀ })
          (record { ∣_∣F_ = ∣∣F₁ ; _∶_F_~_ = ∶F~₁ ; refF = refF₁ })

open FamSetoid

∣∣F=
  : {Γ : Setoid}
    {A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → ∣ A₀ ∣F γ₀ ≡ ∣ A₁ ∣F γ₁
∣∣F= refl refl = refl

∶F~=
  : {Γ : Setoid}
    {A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
    {δ₀ δ₁ : ∣ Γ ∣}(δ₂ : δ₀ ≡ δ₁)
    {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≡[ ∶~= {Γ} refl γ₂ δ₂ ]≡ p₁)
    {a₀ : ∣ A₀ ∣F γ₀}{a₁ : ∣ A₁ ∣F γ₁}(a₂ : a₀ ≡[ ∣∣F= A₂ γ₂ ]≡ a₁)
    {b₀ : ∣ A₀ ∣F δ₀}{b₁ : ∣ A₁ ∣F δ₁}(b₂ : b₀ ≡[ ∣∣F= A₂ δ₂ ]≡ b₁)
  → A₀ ∶ p₀ F a₀ ~ b₀ ≡ A₁ ∶ p₁ F a₁ ~ b₁
∶F~= refl refl refl refl refl refl = refl

refF=
  : {Γ : Setoid}
    {A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
    {a₀ : ∣ A₀ ∣F γ₀}{a₁ : ∣ A₁ ∣F γ₁}(a₂ : a₀ ≡[ ∣∣F= A₂ γ₂ ]≡ a₁)
  → refF A₀ a₀ ≡[ ∶F~= {Γ} A₂ γ₂ γ₂ (ref= {Γ} refl γ₂) a₂ a₂ ]≡ refF A₁ a₁
refF= refl refl refl = refl

abstract
  ∣∣F=mkFamSetoid=
    : {Γ : Setoid}
      {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁) → ∣∣F₀ γ₀ ≡ ∣∣F₁ γ₁)
      {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
      {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
      (∶F~₂ : {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
              {δ₀ δ₁ : ∣ Γ ∣}(δ₂ : δ₀ ≡ δ₁)
              {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≡[ ∶~= {Γ} refl γ₂ δ₂ ]≡ p₁)
              {a₀ : ∣∣F₀ γ₀}{a₁ : ∣∣F₁ γ₁}(a₂ : a₀ ≡[ ∣∣F₂ γ₂ ]≡ a₁)
              {b₀ : ∣∣F₀ δ₀}{b₁ : ∣∣F₁ δ₁}(b₂ : b₀ ≡[ ∣∣F₂ δ₂ ]≡ b₁)
            → ∶F~₀ p₀ a₀ b₀ ≡ ∶F~₁ p₁ a₁ b₁)
      {refF₀ : {γ : ∣ Γ ∣}(a : ∣∣F₀ γ) → ∶F~₀ (ref Γ γ) a a}
      {refF₁ : {γ : ∣ Γ ∣}(a : ∣∣F₁ γ) → ∶F~₁ (ref Γ γ) a a}
      (refF₂ : {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
               {a₀ : ∣∣F₀ γ₀}{a₁ : ∣∣F₁ γ₁}(a₂ : a₀ ≡[ ∣∣F₂ γ₂ ]≡ a₁)
             → refF₀ a₀ ≡[ ∶F~₂ _ _ (ref= {Γ} refl γ₂) a₂ a₂ ]≡ refF₁ a₁)
    → _≡_ {A = {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁) → ∣∣F₀ γ₀ ≡ ∣∣F₁ γ₁}
          (∣∣F= (mkFamSetoid= ∣∣F₂ ∶F~₂ refF₂))
          ∣∣F₂
  ∣∣F=mkFamSetoid= = {!!}


-- section of family of setoids

record _→F_ (Γ : Setoid)(A : FamSetoid Γ) : Set where
  field
    mapF  : (γ : ∣ Γ ∣) → ∣ A ∣F γ
    respF : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF γ) ~ (mapF δ)
    natF : (γ : ∣ Γ ∣) → refF A (mapF γ) ≡ respF (ref Γ γ)

infix 3 _→F_

Γ→F= : {Γ : Setoid}{A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
     → (Γ →F A₀) ≡ (Γ →F A₁)
Γ→F= {Γ} = ap (Γ →F_)

postulate
  mk→F=
    : {Γ : Setoid}{A : FamSetoid Γ}
      {mapF₀ mapF₁ : (γ : ∣ Γ ∣) → ∣ A ∣F γ}
      (mapF₂ : {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁) → mapF₀ γ₀ ≡[ ∣∣F= {Γ}{A} refl γ₂ ]≡ mapF₁ γ₁)
      {respF₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₀ γ) ~ (mapF₀ δ)}
      {respF₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₁ γ) ~ (mapF₁ δ)}
      (respF₂ : {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
                {δ₀ δ₁ : ∣ Γ ∣}(δ₂ : δ₀ ≡ δ₁)
                {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≡[ ∶~= {Γ} refl γ₂ δ₂ ]≡ p₁)
              → respF₀ p₀ ≡[ ∶F~=  {Γ}{A} refl γ₂ δ₂ p₂ (mapF₂ γ₂) (mapF₂ δ₂) ]≡ respF₁ p₁)
      {natF₀ : (γ : ∣ Γ ∣) → refF A (mapF₀ γ) ≡ respF₀ (ref Γ γ)}
      {natF₁ : (γ : ∣ Γ ∣) → refF A (mapF₁ γ) ≡ respF₁ (ref Γ γ)}
      (natF₂ : {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁) → natF₀ γ₀ ≡[ ≡= (∶F~= {Γ}{A} refl γ₂ γ₂ (ref= {Γ} refl γ₂) (mapF₂ γ₂) (mapF₂ γ₂)) (refF= {Γ}{A} refl γ₂ (mapF₂ γ₂)) (respF₂ γ₂ γ₂ (ref= {Γ} refl γ₂) ) ]≡ natF₁ γ₁)
    → _≡_ {A = Γ →F A}
          (record { mapF = mapF₀ ; respF = respF₀ ; natF = natF₀ })
          (record { mapF = mapF₁ ; respF = respF₁ ; natF = natF₁ })

open _→F_

abstract
  mapFcoe
    : {Γ : Setoid}{A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
      {t : Γ →F A₀}{γ : ∣ Γ ∣}
    → mapF (coe (Γ→F= A₂) t) γ
    ≡ coe (∣∣F= A₂ refl) (mapF t γ)
  mapFcoe refl = refl

-- fixities

infixl 5 _,S_
infixl 7 _[_]TS
infixl 5 _,sS_
infix  6 _∘S_
infixl 8 _[_]tS

-- trivial setoid

⊤S : Setoid
⊤S = record
  { ∣_∣   = ⊤
  ; _∶_~_ = λ _ _ → ⊤
  ; ref   = λ _ → tt
  }

-- setoid comprehension

_,S_ : (Γ : Setoid) → FamSetoid Γ → Setoid
Γ ,S A = record
  { ∣_∣   = Σ ∣ Γ ∣ ∣ A ∣F_
  ; _∶_~_ = λ { (γ₀ ,Σ a₀) (γ₁ ,Σ a₁) → Σ (Γ ∶ γ₀ ~ γ₁) λ γ₂ → A ∶ γ₂ F a₀ ~ a₁ }
  ; ref   = λ { (γ ,Σ a) → (ref Γ γ) ,Σ refF A a }
  }

-- substitution of families of setoids

_[_]TS : {Γ : Setoid}{Θ : Setoid}(A : FamSetoid Γ) → Θ →S Γ → FamSetoid Θ
A [ σ ]TS = record
  { ∣_∣F_   = λ θ → ∣ A ∣F map σ θ
  ; _∶_F_~_ = λ θ₂ a₀ a₁ → A ∶ resp σ θ₂ F a₀ ~ a₁
  ; refF    = λ {γ} a → coe (ap (λ z → A ∶ z F a ~ a) (nat σ γ)) (refF A a)
  }

-- identity setoid morphism

idS : {Γ : Setoid} → Γ →S Γ
idS {Γ} = record
  { map  = λ γ → γ
  ; resp = λ γ₂ → γ₂
  ; nat  = λ _ → refl
  }

-- setoid morphism composition

_∘S_ : {Γ Δ Σ : Setoid} → Δ →S Σ → Γ →S Δ → Γ →S Σ
_∘S_ {Γ}{Δ}{Σ} σ ν = record
  { map  = λ γ → map σ (map ν γ)
  ; resp = λ γ₂ → resp σ (resp ν γ₂)
  ; nat  = λ γ → nat σ (map ν γ) ◾ ap (resp σ) (nat ν γ)
  }
  
-- 

εS : {Γ : Setoid} → Γ →S ⊤S
εS {Γ} = record { map = λ _ → tt ; resp = λ _ → tt ; nat = λ _ → refl }

-- setoid morphism extension

_,sS_ : {Γ Δ : Setoid}(σ : Γ →S Δ){A : FamSetoid Δ}
      → Γ →F (A [ σ ]TS) → Γ →S (Δ ,S A)
_,sS_ {Γ}{Δ} σ {A} t = record
  { map  = λ γ → (map σ γ) ,Σ (mapF t γ)
  ; resp = λ γ₂ → (resp σ γ₂) ,Σ respF t γ₂
  ; nat  = λ γ → ,Σ= (nat σ γ) (natF t γ)
  }

-- first projection

π₁S : {Γ Δ : Setoid}{A : FamSetoid Δ}(σ : Γ →S (Δ ,S A))
    → Γ →S Δ
π₁S σ = record
  { map  = λ γ → proj₁ (map σ γ)
  ; resp = λ γ₂ → proj₁ (resp σ γ₂)
  ; nat  = λ γ → ,Σ=0 (nat σ γ)
  }

-- substitution of sections

_[_]tS : {Γ Δ : Setoid}{A : FamSetoid Δ}
         (t : Δ →F A)(σ : Γ →S Δ) → Γ →F (A [ σ ]TS)
_[_]tS {Γ}{Δ}{A} t σ = record
  { mapF  = λ γ → mapF t (map σ γ)
  ; respF = λ γ₂ → respF t (resp σ γ₂)
  ; natF  = λ γ → ap (λ z → coe (ap (λ z → A ∶ z F mapF t (map σ γ) ~ mapF t (map σ γ)) (nat σ γ)) z)
                     (natF t (map σ γ))
                ◾ apd (respF t) (nat σ γ)
  }

-- second projection

π₂S : {Γ Δ : Setoid}{A : FamSetoid Δ}(σ : Γ →S (Δ ,S A))
    → Γ →F (A [ π₁S σ ]TS)
π₂S {Γ}{Δ}{A} σ = record
  { mapF  = λ γ → proj₂ (map σ γ)
  ; respF = λ γ₂ → proj₂ (resp σ γ₂)
  ; natF  = λ γ → ,Σ=1 (nat σ γ)
  }

-- equalities

abstract
  [id]TS : {Γ : Setoid}{A : FamSetoid Γ} → A [ idS ]TS ≡ A
  [id]TS = refl

abstract
  [][]TS
    : {Γ Δ Σ : Setoid}{A : FamSetoid Σ}{σ : Γ →S Δ}{δ : Δ →S Σ}
    → A [ δ ]TS [ σ ]TS ≡ A [ δ ∘S σ ]TS
  [][]TS {Γ}{Δ}{Σ}{A}{σ}{δ}
    = mkFamSetoid=
        (λ { {_} refl → refl })
        (λ { {_} refl refl refl refl refl → refl })
        (λ { {γ} refl {a} refl → coecoe (ap (λ z → A ∶ z F _ ~ _) (nat δ (map σ _))) (ap (λ z → A ∶ resp δ z F _ ~ _) (nat σ _))
                               ◾ ap (λ z → coe z (refF A a))
                                    {ap (λ z → A ∶ z F a ~ a) (nat δ (map σ γ)) ◾ ap (λ z → A ∶ resp δ z F a ~ a) (nat σ γ)}
                                    {ap (λ z → A ∶ z F a ~ a) (nat (δ ∘S σ) γ)}
                                    ( ap (λ z → ap (λ z → A ∶ z F a ~ a) (nat δ (map σ γ)) ◾ z)
                                         (apap (nat σ γ))
                                    ◾ ap◾ (λ z → A ∶ z F a ~ a) (nat δ (map σ γ)) (ap (resp δ) (nat σ γ)) ⁻¹) })

abstract
  idlS : {Γ Δ : Setoid}{δ : Γ →S Δ} → idS ∘S δ ≡ δ
  idlS {Γ}{Δ}{δ}
    = mk→S=
        (λ { {_} refl → refl })
        (λ { {_} refl refl refl → refl })
        (λ { {_} refl → ◾lid (ap (λ γ₂ → γ₂) (nat δ _)) ◾ apid (nat δ _) })

abstract
  idrS : {Γ Δ : Setoid}{δ : Γ →S Δ} → δ ∘S idS ≡ δ
  idrS {Γ}{Δ}{δ}
    = mk→S=
        (λ { {_} refl → refl })
        (λ { {_} refl refl refl → refl })
        (λ { {_} refl → ◾rid (nat δ _) })

abstract
  assS
    : {Δ Γ Σ Ω : Setoid}{σ : Σ →S Ω}{δ : Γ →S Σ}{ν : Δ →S Γ}
    → (σ ∘S δ) ∘S ν ≡ σ ∘S (δ ∘S ν)
  assS {Δ}{Γ}{Σ}{Ω}{σ}{δ}{ν}
    = mk→S=
      (λ { {_} refl → refl })
      (λ { {_} refl refl refl → refl })
      (λ { {_} refl → ◾ass _ _ _ ⁻¹
                    ◾ ap (λ z → (nat σ (map δ (map ν _)) ◾ z))
                         ( ap (λ z → ap (resp σ) (nat δ (map ν _)) ◾ z)
                              (apap (nat ν _))
                         ◾ ap◾ (resp σ) (nat δ (map ν _)) (ap (resp δ) (nat ν _)) ⁻¹) })

abstract
  ,∘S
    : {Γ Δ Σ : Setoid}{δ : Γ →S Δ}{σ : Σ →S Γ}{A : FamSetoid Δ}{a : Γ →F (A [ δ ]TS)}
    → (_,sS_ δ {A} a) ∘S σ ≡ (δ ∘S σ) ,sS coe (Γ→F= ([][]TS {A = A}{σ}{δ})) (a [ σ ]tS)
  ,∘S {Γ}{Δ}{Σ}{δ}{σ}{A}{a}
    = mk→S=
        (λ { {γ} refl → ,Σ= refl ({!!} ◾ ap (λ z → coe (z refl) (mapF (a [ σ ]tS) γ)) (∣∣F=mkFamSetoid= (λ { {_} refl → refl}) _ _) ⁻¹ ◾ mapFcoe [][]TS ⁻¹) })
        (λ { {_} refl refl refl → {!!} })
        (λ { {_} refl → {!!} })

abstract
  π₁βS
    : {Γ Δ : Setoid}{A : FamSetoid Δ}{δ : Γ →S Δ}{a : Γ →F (A [ δ ]TS)}
    → π₁S (_,sS_ δ {A} a) ≡ δ
  π₁βS {Γ}{Δ}{A}{δ}{a}
    = mk→S=
        (λ { {_} refl → refl })
        (λ { {_} refl refl refl → refl })
        (λ { {_} refl → ,Σ=β0 (nat δ _) (natF a _) })

abstract
  πηS
    : {Γ Δ : Setoid}{A : FamSetoid Δ}{δ : Γ →S (Δ ,S A)}
    → π₁S δ ,sS π₂S δ ≡ δ
  πηS {Γ}{Δ}{A}{δ}
    = mk→S=
        (λ { {_} refl → refl })
        (λ { {_} refl refl refl → refl })
        (λ { {_} refl → ,Σ=η (nat δ _) })

abstract
  εη : {Γ : Setoid}{σ : Γ →S ⊤S} → σ ≡ εS
  εη {Γ}{σ}
    = mk→S=
        (λ { {_} refl → refl })
        (λ { {_} refl refl refl → refl })
        (λ { {_} refl → set⊤ (nat σ _) refl })

abstract
  π₂βS
    : {Γ Δ : Setoid}{A : FamSetoid Δ}{δ : Γ →S Δ}{a : Γ →F (A [ δ ]TS)}
    → π₂S (_,sS_ δ {A} a) ≡[ Γ→F= (ap (_[_]TS A) (π₁βS {Γ}{Δ}{A}{δ}{a})) ]≡ a
  π₂βS {Γ}{Δ}{A}{δ}{a}
    = mk→F=
        (λ { {_} refl → {!!} })
        (λ { {_} refl refl refl → {!!} })
        (λ { {_} refl → {!!} })
