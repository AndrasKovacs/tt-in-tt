{-# OPTIONS --no-eta #-}

open import Cats

module PSh.Tms (C : Cat) where

open import lib
open import JM

open import PSh.Cxt C
open import PSh.Ty C

open Cat C

εᴹ : {Γ : PSh C} → Γ →n ∙ᴹ
εᴹ = record { _$n_ = λ _ → tt ; natn = refl }

_,sᴹ_ : {Γ Δ : PSh C}(δ : Γ →n Δ){A : FamPSh Δ}(u : Γ →S A [ δ ]Tᴹ)
      → Γ →n (Δ ,Cᴹ A)
δ ,sᴹ u = record { _$n_ = λ α → δ $n α ,Σ u $S α ; natn = ,Σ= (natn δ) (natS u) }

infixl 5 _,sᴹ_

idᴹ : {Γ : PSh C} → Γ →n Γ
idᴹ = record { _$n_ = λ α → α ; natn = refl }

_∘ᴹ_ : {Γᴹ Δᴹ Σᴹ : PSh C} → Δᴹ →n Σᴹ → Γᴹ →n Δᴹ → Γᴹ →n Σᴹ
σ ∘ᴹ δ = record { _$n_ = λ α → σ $n (δ $n α) ; natn = natn σ ◾ ap (_$n_ σ) (natn δ) }

infix 6 _∘ᴹ_

π₁ᴹ : {Γ Δ : PSh C}{A : FamPSh Δ}(δ : Γ →n (Δ ,Cᴹ A))
    → Γ →n Δ
π₁ᴹ δ = record { _$n_ = λ α → proj₁ (δ $n α) ; natn = ap proj₁ (natn δ) }
