{-# OPTIONS --no-eta #-}

open import Cats

module PSh.Tm (C : Cat) where

open import lib
open import JM

open import PSh.Cxt C
open import PSh.Exp C
open import PSh.Ty C
open import PSh.Tms C

open Cat C

----------------------------------------------------------------------
-- Substitution calculus
----------------------------------------------------------------------

_[_]tᴹ : {Γ Δ : PSh C}{A : FamPSh Δ}(t : Δ →S A)(δ : Γ →n Δ)
       → Γ →S A [ δ ]Tᴹ
_[_]tᴹ {Γ}{Δ}{A} t δ = record

  { _$S_ = λ α → t $S (δ $n α)
  ; natS = pSnat }

  where
  
    abstract
      pSnat : ∀{I J}{f : I ⇒ J}{α : Γ $P I}
            → coe (ap (_$F_ A) (natn δ))
                  (A $F f $ (t $S (δ $n α)))
            ≡ t $S (δ $n (Γ $P f $ α))
      pSnat {I}{J}{f}{α} = from≃ ( uncoe (ap (_$F_ A) (natn δ)) ⁻¹̃
                                 ◾̃ to≃ (natS t)
                                 ◾̃ ap≃' (_$S_ t)
                                        {Δ $P f $ (δ $n α)}
                                        {δ $n (Γ $P f $ α)}
                                        (natn δ)
                                        (to≃ (natn δ)))

infixl 8 _[_]tᴹ

π₂ᴹ : {Γ Δ : PSh C}{A : FamPSh Δ}
      (δ : Γ →n (Δ ,Cᴹ A))
    → Γ →S A [ π₁ᴹ {A = A} δ ]Tᴹ
π₂ᴹ {Γ}{Δ}{A} δ = record

  { _$S_ = λ α → proj₂ (δ $n α)
  ; natS = pSnat }
  
  where
    abstract
      pSnat : {I : Obj}{J : Obj}{f : I ⇒ J}{α : Γ $P I}
            → coe (ap (_$F_ A) (ap (λ r → proj₁ r) (natn δ)))
                  (A $F f $ proj₂ (δ $n α))
            ≡ proj₂ (δ $n (Γ $P f $ α))
      pSnat {I}{J}{f}{α} = from≃ ( uncoe (ap (_$F_ A) (ap (λ r → proj₁ r) (natn δ))) ⁻¹̃
                                 ◾̃ apd≃ proj₂ (natn δ))

----------------------------------------------------------------------
-- Pi
----------------------------------------------------------------------

appᴹ : {Γ : PSh C}{A : FamPSh Γ}{B : FamPSh (Γ ,Cᴹ A)}
     → Γ →S (Πᴹ A B) → (Γ ,Cᴹ A) →S B
appᴹ {Γ}{A}{B} t = record

  { _$S_ = λ α → coe (ap (_$F_ B) (idP (Γ ,Cᴹ A)))
                     (mapE A B
                          (t $S proj₁ α)
                          idc
                          (A $F idc $ proj₂ α))
  ; natS = appnat }
  
  where
  
    abstract
      appnat : {I J : Obj}{f : I ⇒ J}{α : (Γ ,Cᴹ A) $P I}
             → B $F f $ coe (ap (_$F_ B) (idP (Γ ,Cᴹ A)))
                            (mapE A B
                                 (t $S proj₁ α)
                                 idc
                                 (A $F idc $ proj₂ α))
              ≡ coe (ap (_$F_ B) (idP (Γ ,Cᴹ A)))
                    (mapE A B
                         (t $S proj₁ ((Γ ,Cᴹ A) $P f $ α))
                         idc
                         (A $F idc $ proj₂ ((Γ ,Cᴹ A) $P f $ α)))
      appnat {I}{J}{f}{α}
      
        =  from≃ ( $Ff$≃ B
                         (idP (Γ ,Cᴹ A) ⁻¹)
                         (uncoe (ap (_$F_ B) (idP (Γ ,Cᴹ A))) ⁻¹̃)
                 ◾̃ to≃ (natE A B (t $S proj₁ α))
                 ◾̃ uncoe (unmerge A B) ⁻¹̃
                 ◾̃ mapEABw≃ {Γ}{A}{B}{w = t $S proj₁ α}
                            (idr ◾ idl ⁻¹)
                            ( uncoe (merge A) ⁻¹̃
                            ◾̃ from≡ (ap (_$F_ A) (compP Γ)) (compF A) ⁻¹̃
                            ◾̃ $F-$x≃ A (idr ◾ idl ⁻¹)
                            ◾̃ from≡ (ap (_$F_ A) (compP Γ)) (compF A)
                            ◾̃ uncoe (ap (_$F_ A) (compP Γ ⁻¹)))
                 ◾̃ uncoe (ap (_$F_ B) (,Σ= (compP Γ) (coeap2 (compP Γ))))
                 ◾̃ to≃ (ap (λ z → mapE A B
                                       z
                                      idc
                                       (A $F idc $ proj₂ ((Γ ,Cᴹ A) $P f $ α)))
                           (natS t))
                 ◾̃ uncoe (ap (_$F_ B) (idP (Γ ,Cᴹ A))))

lamᴹ : {Γ : PSh C}{A : FamPSh Γ}{B : FamPSh (Γ ,Cᴹ A)}
     → (Γ ,Cᴹ A) →S B → Γ →S (Πᴹ A B)
lamᴹ {Γ}{A}{B} t = con→S lamw lamnat
  where
    lamw : {I : Obj} (α : Γ $P I) → ExpPSh A B α
    lamw {I} α = _,Σ_ (λ {J} f a → t $S ((Γ $P f $ α) ,Σ a))
                      lamwnat
      where
        abstract
          lamwnat : {J K : Obj}{f : I ⇒ J}{g : J ⇒ K}{a : A $F (Γ $P f $ α)}
                  → B $F g $ (t $S (Γ $P f $ α ,Σ a))
                  ≡ coe (unmerge A B)
                        (t $S (Γ $P (g ∘c f) $ α ,Σ coe (merge A) (A $F g $ a)))
          lamwnat {J}{K}{f}{g}{a}

            = natS t
            ◾ from≃ ( t$S≃ {t = t}
                           (,Σ= (compP Γ ⁻¹)
                                (from≃ ( uncoe (ap (_$F_ A) (compP Γ ⁻¹)) ⁻¹̃
                                       ◾̃ uncoe (merge A))))
                    ◾̃ uncoe (unmerge A B))

                   
    abstract
      lamnat : {I J : Obj}{f : I ⇒ J}{α : Γ $P I}
             → Πᴹ A B $F f $ lamw α ≡ lamw (Γ $P f $ α)
      lamnat {I}{J}{f}{α}

        = =ExpPShΓAB {Γ}{A}{B}
                     ( funexti (λ J
                     → funext (λ g
                     → funext (λ a
                     → from≃ ( uncoe (ap (_$F_ B) (,Σ= (compP Γ) (coeap2 (compP Γ)))) ⁻¹̃
                             ◾̃ t$S≃ {t = t}
                                    (,Σ= (compP Γ) (coeap2 (compP Γ))))))))
