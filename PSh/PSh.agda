{-# OPTIONS --no-eta #-}

open import Cats

-- the presheaf model is parameterised by a base type U and a base
-- family El

module PSh.PSh (C : Cat)(⟦U⟧ : PSh C)(⟦El⟧ : FamPSh ⟦U⟧) where

open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.Func

open Cat C

d : Decl
d = record { Con = PSh C ; Ty = FamPSh ; Tms = _→n_ ; Tm = _→S_ }

open import PSh.Cxt C
open import PSh.Ty C
open import PSh.Tms C
open import PSh.Tm C
open import PSh.HTy C
open import PSh.HTms C
open import PSh.HTm C

c : Core d
c = record
      { •     = ∙ᴹ
      ; _,_   = _,Cᴹ_
      ; _[_]T = _[_]Tᴹ
      ; id    = idᴹ
      ; _∘_   = _∘ᴹ_
      ; ε     = εᴹ
      ; _,s_  = _,sᴹ_
      ; π₁    = π₁ᴹ
      ; _[_]t = _[_]tᴹ
      ; π₂    = π₂ᴹ
      ; [id]T = [id]Tᴹ
      ; [][]T = [][]Tᴹ
      ; idl   = idlᴹ
      ; idr   = idrᴹ
      ; ass   = λ {_}{_}{_}{_}{σ}{δ}{ν} → assᴹ {σ = σ}{δ}{ν}
      ; ,∘    = ,∘ᴹ
      ; π₁β   = λ {_}{_}{_}{_}{a} → π₁βᴹ {a = a}
      ; πη    = πηᴹ
      ; εη    = εηᴹ
      ; π₂β   = π₂βᴹ
      }

b : Base c
b = record { U = Uᴹ ⟦U⟧ ; U[] = U[]ᴹ ⟦U⟧ ; El = Elᴹ ⟦El⟧ ; El[] = El[]ᴹ ⟦El⟧ }

f : Func c
f = record
      { Π = Πᴹ
      ; Π[] = Π[]ᴹ
      ; lam = lamᴹ
      ; app = appᴹ
      ; lam[] = lam[]ᴹ
      ; Πβ = Πβᴹ
      ; Πη = Πηᴹ
      }

open import TT.Rec

open rec d c b f

open import TT.Syntax

⟦_⟧C : Con → PSh C
⟦_⟧T : ∀{Γ} → Ty Γ → FamPSh ⟦ Γ ⟧C
⟦_⟧s : ∀{Γ Δ} → Tms Γ Δ → ⟦ Γ ⟧C →n ⟦ Δ ⟧C
⟦_⟧t : ∀{Γ}{A : Ty Γ} → (t : Tm Γ A) → ⟦ Γ ⟧C →S ⟦ A ⟧T

⟦_⟧C = RecCon
⟦_⟧T = RecTy
⟦_⟧s = RecTms
⟦_⟧t = RecTm
