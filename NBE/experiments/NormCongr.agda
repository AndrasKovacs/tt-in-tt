open import lib
open import TT.Syntax
open import NBE.Nf
open import NBE.Nfs
open import NBE.experiments.NConTy
open import TT.Congr

module NBE.experiments.NormCongr
  (norms : ∀{Γ Δ} → Tms Γ Δ → Nfs Γ Δ)
  (comps : ∀{Γ Δ}(σ : Tms Γ Δ) → σ ≡ ⌜ norms σ ⌝nfs)
  (normt : ∀{Γ A} → Tm Γ A → Nf Γ A)
  (compt : ∀{Γ A}(t : Tm Γ A) → t ≡ ⌜ normt t ⌝nf)
  (normC : Con → NCon)
  (compC : (Γ : Con) → Γ ≡ ⌜ normC Γ ⌝C)
  (stabC : (Γ : NCon) → normC ⌜ Γ ⌝C ≡ Γ)
  (normT : ∀{Γ} → Ty Γ → NTy (normC Γ))
  (compT : ∀{Γ}(A : Ty Γ) → A ≡[ Ty= (compC Γ) ]≡ ⌜ normT A ⌝T)
  (stabT : {Γ : NCon}(A : NTy Γ) → normT ⌜ A ⌝T ≡[ NTy= (stabC Γ) ]≡ A)
  where

open import JM

abstract
  normt≃ : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
           {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}(t₂ : t₀ ≃ t₁)
         → normt t₀ ≃ normt t₁
  normt≃ refl (refl , refl) = refl , refl

abstract
  normT≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
           {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
         → normT A₀ ≃ normT A₁
  normT≃ refl (refl , refl) = refl , refl
