{-# OPTIONS --no-eta #-}

open import lib hiding (_,_)
open import TT.Syntax
open import NBE.Nf

module NBE.experiments.NormalModel.Cxt where

open import TT.Congr
open import TT.Laws
open import TT.Elim
open import NBE.experiments.NConTy
open import NBE.experiments.NormalModel.Motives

mCon : MethodsCon M
mCon = record

  { •ᴹ    = •N Σ., refl
  
  ; _,Cᴹ_ = λ { {Γ}(Γ' Σ., pΓ){A}(A' Σ., pA)
  
            → (Γ' ,N A') Σ., ,C= pΓ pA } }
