{-# OPTIONS --no-eta #-}

open import lib hiding (_,_)
open import TT.Syntax
open import NBE.Nf

module NBE.experiments.NormalModel.HTy
  (norms  : ∀{Γ Δ} → Tms Γ Δ → Nfs Γ Δ)
  (compls : ∀{Γ Δ}(σ : Tms Γ Δ) → σ ≡ ⌜ norms σ ⌝nfs)
  (normt  : ∀{Γ A} → Tm Γ A → Nf Γ A)
  (complt : ∀{Γ A}(t : Tm Γ A) → t ≡ ⌜ normt t ⌝nf)
  (stabs  : ∀{Γ Δ}(τ : Nfs Γ Δ) → norms ⌜ τ ⌝nfs ≡ τ)
  (stabt  : ∀{Γ A}(n : Nf Γ A) → normt ⌜ n ⌝nf ≡ n)
  where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.Elim
open import NBE.experiments.NConTy
open import NBE.experiments.NormalModel.Motives
open import NBE.experiments.NormalModel.Cxt
open import NBE.experiments.NormalModel.Ty  norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.Tms norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.Tm norms compls normt complt stabs stabt

[]nT≃ : ∀{Θ}(A : NTy Θ){Γ₀ Γ₁ : NCon}(Γ₂ : Γ₀ ≡ Γ₁)
         {σ₀ : Nfs ⌜ Γ₀ ⌝C ⌜ Θ ⌝C}{σ₁ : Nfs ⌜ Γ₁ ⌝C ⌜ Θ ⌝C}(σ₂ : σ₀ ≃ σ₁)
       → A [ σ₀ ]nT ≃ A [ σ₁ ]nT
[]nT≃ N refl (refl ,≃ refl) = refl ,≃ refl

abstract
  normt≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
           {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}(t₂ : t₀ ≃ t₁)
         → normt t₀ ≃ normt t₁
  normt≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

abstract
  norms≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
           {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≃ σ₁)
         → norms σ₀ ≃ norms σ₁
  norms≃ refl refl (refl ,≃ refl) = refl ,≃ refl

-- todo: maybe move these to NBE.Stab

abstract
  inj⌜⌝nf : ∀{Γ A}{v₀ v₁ : Nf Γ A} → ⌜ v₀ ⌝nf ≡ ⌜ v₁ ⌝nf → v₀ ≡ v₁
  inj⌜⌝nf {Γ}{A}{v₀}{v₁} p = stabt v₀ ⁻¹ ◾ ap normt p ◾ stabt v₁

abstract
  inj⌜⌝nf' : ∀{Γ}{A₀ A₁}(A₂ : A₀ ≡ A₁){v₀ : Nf Γ A₀}{v₁ : Nf Γ A₁}
           → ⌜ v₀ ⌝nf ≃ ⌜ v₁ ⌝nf → v₀ ≃ v₁
  inj⌜⌝nf' refl {v₀}{v₁} (refl ,≃ p) = to≃ (stabt v₀ ⁻¹ ◾ ap normt p ◾ stabt v₁)

abstract
  inj⌜⌝nfs : ∀{Γ Δ}{τ₀ τ₁ : Nfs Γ Δ} → ⌜ τ₀ ⌝nfs ≡ ⌜ τ₁ ⌝nfs → τ₀ ≡ τ₁
  inj⌜⌝nfs {Γ}{Δ}{τ₀}{τ₁} p = stabs τ₀ ⁻¹ ◾ ap norms p ◾ stabs τ₁

abstract
  inj⌜⌝nfs' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
              {σ₀ : Nfs Γ₀ Δ₀}{σ₁ : Nfs Γ₁ Δ₁}
            → ⌜ σ₀ ⌝nfs ≃ ⌜ σ₁ ⌝nfs → σ₀ ≃ σ₁
  inj⌜⌝nfs' refl refl {σ₀}{σ₁} (refl ,≃ p) = to≃ (stabs σ₀ ⁻¹ ◾ ap norms p ◾ stabs σ₁)

abstract
  [id]nT : ∀{Γ}(A : NTy Γ) → A [ norms id ]nT ≡ A
  [id]nT {Γ}(ΠN A B)

    = ΠN=' ([id]nT A)
           ( []nT≃ B
                   (,N≃ refl (to≃ ([id]nT A)))
                   (inj⌜⌝nfs' (,C= refl (ap ⌜_⌝T ([id]nT A)))
                              refl
                              ( to≃ (compls ((coe (Tms= (,C= refl ⌜[]nT⌝) refl) (⌜ norms id ⌝nfs ∘ π₁ id , coe (ap (Tm (⌜ Γ ⌝C , ⌜ A ⌝T [ ⌜ norms id ⌝nfs ]T)) [][]T) (π₂ id)))) ⁻¹)
                              ◾̃ uncoe (Tms= (,C= refl ⌜[]nT⌝) refl) ⁻¹̃
                              ◾̃ ,s≃ (,C= refl ([]T= refl refl refl (compls id ⁻¹) ◾ [id]T))
                                    refl
                                    ( to≃ (ap (λ z → z ∘ π₁ id) (compls id ⁻¹))
                                    ◾̃ to≃ idl
                                    ◾̃ π₁id≃ refl (to≃ ([]T= refl refl refl (compls id ⁻¹) ◾ [id]T)))
                                    r̃
                                    ( uncoe (ap (Tm (⌜ Γ ⌝C , ⌜ A ⌝T [ ⌜ norms id ⌝nfs ]T)) [][]T) ⁻¹̃
                                    ◾̃ π₂id≃ refl (to≃ ([]T= refl refl refl (compls id ⁻¹) ◾ [id]T)))
                              ◾̃ to≃ πη
                              ◾̃ to≃ (compls id)))
           ◾̃ to≃ ([id]nT B))

  [id]nT UN = refl
  [id]nT (ElN Â) = ap ElN (inj⌜⌝nf ( complt (coe (TmΓ= U[]) (⌜ Â ⌝nf [ ⌜ norms id ⌝nfs ]t)) ⁻¹
                                   ◾ from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                                           ◾̃ []t≃ refl
                                                  (to≃ (compls id ⁻¹))
                                           ◾̃ [id]t')))

abstract
  [][]nT : ∀{Δ}(A : NTy Δ){Θ Γ}{σ : Nfs ⌜ Θ ⌝C ⌜ Δ ⌝C}{ν : Nfs ⌜ Γ ⌝C ⌜ Θ ⌝C}
         → A [ σ ]nT [ ν ]nT ≡ A [ norms (⌜ σ ⌝nfs ∘ ⌜ ν ⌝nfs) ]nT
  [][]nT (ΠN A B){_}{_}{σ}{ν}

    = ΠN=' ([][]nT A)
           ( to≃ ([][]nT B)
           ◾̃ []nT≃ B
                   (,N= refl ([][]nT A))
                   (norms≃ (,C= refl (ap ⌜_⌝T ([][]nT A)))
                           refl
                           ( ∘≃' (,C= refl (⌜[]nT⌝ ⁻¹))
                                 refl
                                 (,C= refl (⌜[]nT⌝ ⁻¹ ◾ ap (λ z → z [ ⌜ ν ⌝nfs ]T) (⌜[]nT⌝ ⁻¹)))
                                 (from≡ (Tms= (,C= refl ⌜[]nT⌝) refl) ⌜^nT⌝ ⁻¹̃)
                                 ( from≡ (Tms= (,C= refl ⌜[]nT⌝) refl) ⌜^nT⌝ ⁻¹̃
                                 ◾̃ ^≃''' refl refl r̃ (to≃ (⌜[]nT⌝ ⁻¹)))
                           ◾̃ ∘^ ⁻¹̃
                           ◾̃ ^≃ (compls (⌜ σ ⌝nfs ∘ ⌜ ν ⌝nfs))
                           ◾̃ uncoe (Tms= (,C= refl ⌜[]nT⌝) refl))))
    
  [][]nT UN = refl
  [][]nT (ElN Â){_}{_}{σ}{ν}
  
    = ap ElN
         (inj⌜⌝nf ( complt (coe (TmΓ= U[]) (⌜ normt (coe (TmΓ= U[]) (⌜ Â ⌝nf [ ⌜ σ ⌝nfs ]t)) ⌝nf [ ⌜ ν ⌝nfs ]t)) ⁻¹
                  ◾ from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                          ◾̃ []t≃' refl refl r̃ (to≃ (complt (coe (TmΓ= U[]) (⌜ Â ⌝nf [ ⌜ σ ⌝nfs ]t)) ⁻¹)) r̃
                          ◾̃ coe[]t' U[]
                          ◾̃ [][]t'
                          ◾̃ []t≃ refl (to≃ (compls (⌜ σ ⌝nfs ∘ ⌜ ν ⌝nfs)))
                          ◾̃ uncoe (TmΓ= U[]))
                  ◾ complt (coe (TmΓ= U[]) (⌜ Â ⌝nf [ ⌜ norms (⌜ σ ⌝nfs ∘ ⌜ ν ⌝nfs) ⌝nfs ]t))))

abstract
  [id]Tᴹ : {Γ : Con}{Γᴹ : Motives.Conᴹ M Γ}{A : Ty Γ}{Aᴹ : Motives.Tyᴹ M Γᴹ A}
         → (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Γ}{Γᴹ}{A} Aᴹ) (MethodsTms.idᴹ mTms {Γ}{Γᴹ})
         ≡[ Motives.TyΓᴹ= M {Γ}{Γᴹ} [id]T ]≡
           Aᴹ
  [id]Tᴹ {Γ}{Γ' Σ., pΓ}{A}{A' Σ., pA}

    = =Tyᴹ pΓ [id]T
           (from≃ ( []nT≃ A' refl (norms≃ refl refl (uncoe (Tms= pΓ pΓ) ⁻¹̃ ◾̃ id≃ pΓ)))
                  ◾ [id]nT A')
  

abstract
  [][]Tᴹ : {Γ : Con} {Γᴹ : Motives.Conᴹ M Γ} {Δ : Con}
           {Δᴹ : Motives.Conᴹ M Δ} {Σ : Con} {Σᴹ : Motives.Conᴹ M Σ}
           {A : Ty Σ} {Aᴹ : Motives.Tyᴹ M Σᴹ A} {σ : Tms Γ Δ}
           {σᴹ : Motives.Tmsᴹ M Γᴹ Δᴹ σ} {δ : Tms Δ Σ}
           {δᴹ : Motives.Tmsᴹ M Δᴹ Σᴹ δ}
         → MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{A [ δ ]T}(MethodsTy._[_]Tᴹ mTy {Δ}{Δᴹ}{Σ}{Σᴹ}{A} Aᴹ δᴹ) σᴹ
         ≡[ Motives.TyΓᴹ= M {Γ}{Γᴹ} [][]T ]≡
           MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Σ}{Σᴹ} Aᴹ (MethodsTms._∘ᴹ_ mTms {Γ}{Γᴹ}{Δ}{Δᴹ}{Σ}{Σᴹ} δᴹ σᴹ)
  [][]Tᴹ {Γ}{Γ' Σ., pΓ}{Θ}{Θ' Σ., pΘ}{Δ}{Δ' Σ., pΔ}{A}{A' Σ., pA}{σ}{σ' Σ., pσ}{ν}{ν' Σ., pν}

    = =Tyᴹ pΓ [][]T
           ( [][]nT A'
           ◾ from≃ ([]nT≃ A' refl
                          (norms≃ refl refl
                                  ( ∘≃' (pΘ ⁻¹) (pΔ ⁻¹) (pΓ ⁻¹) (from≡ (Tms= pΘ pΔ) pν ⁻¹̃) (from≡ (Tms= pΓ pΘ) pσ ⁻¹̃)
                                  ◾̃ uncoe (Tms= pΓ pΔ)))))
           
abstract
  U[]ᴹ : {Γ : Con} {Γᴹ : Motives.Conᴹ M Γ} {Δ : Con}
         {Δᴹ : Motives.Conᴹ M Δ} {δ : Tms Γ Δ}
         {δᴹ : Motives.Tmsᴹ M Γᴹ Δᴹ δ}
       → MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{U} (MethodsTy.Uᴹ mTy {Δ}{Δᴹ}) δᴹ
       ≡[ Motives.TyΓᴹ= M {Γ}{Γᴹ} U[] ]≡
         MethodsTy.Uᴹ mTy {Γ}{Γᴹ}
  U[]ᴹ {Γ}{Γ' Σ., pΓ}{Δ}{Δ' Σ., pΔ}{σ}{σ' Σ., pσ}
  
    = =Tyᴹ pΓ U[] refl

abstract
  El[]ᴹ : {Γ : Con} {Γᴹ : Motives.Conᴹ M Γ} {Δ : Con}
          {Δᴹ : Motives.Conᴹ M Δ} {δ : Tms Γ Δ} {δᴹ : Motives.Tmsᴹ M Γᴹ Δᴹ δ}
          {Â : Tm Δ U} {Âᴹ : Motives.Tmᴹ M Δᴹ (MethodsTy.Uᴹ mTy {Δ}{Δᴹ}) Â}
        → MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{El Â} (MethodsTy.Elᴹ mTy {Δ}{Δᴹ}{Â} Âᴹ) δᴹ
        ≡[ Motives.TyΓᴹ= M {Γ}{Γᴹ} El[] ]≡
          MethodsTy.Elᴹ mTy {Γ}{Γᴹ} (coe (Motives.TmΓᴹ= M {Γ}{Γᴹ} U[] U[]ᴹ refl) (MethodsTm._[_]tᴹ mTm {Γ}{Γᴹ}{Δ}{Δᴹ} Âᴹ δᴹ))
  El[]ᴹ {Γ}{Γ' Σ., pΓ}{Δ}{Δ' Σ., pΔ}{σ}{σ' Σ., pσ}{Â}{Â' Σ., pÂ}

    = =Tyᴹ pΓ El[]
           (ap ElN
               (from≃
                 (normt≃ refl r̃ (uncoe (TmΓ= U[]) ⁻¹̃ ◾̃ []t≃' (pΓ ⁻¹) (pΔ ⁻¹) (U≃ (pΔ ⁻¹)) (from≡ (Tm= pΔ (U= pΔ)) pÂ ⁻¹̃) (from≡ (Tms= pΓ pΔ) pσ ⁻¹̃) ◾̃ uncoe (Tm= pΓ ([]T= pΓ pΔ (U= pΔ) pσ ◾ ⌜[]nT⌝)))
                 ◾̃ proj₁coe
                     {Nf ⌜ Γ' ⌝C U} refl
                     {λ t' → Â [ σ ]t ≡[ Tm= pΓ ([]T= pΓ pΔ (U= pΔ) pσ ◾ ⌜[]nT⌝) ]≡ ⌜ t' ⌝nf}
                     {λ t' → coe (TmΓ= U[]) (Â [ σ ]t) ≡[ Tm= pΓ (U= pΓ) ]≡ ⌜ t' ⌝nf}
                     (funext λ t' → from≃ (≡≃ refl (uncoe (Tm= pΓ ([]T= pΓ pΔ (U= pΔ) pσ ◾ ⌜[]nT⌝)) ⁻¹̃ ◾̃ uncoe (TmΓ= U[]) ◾̃ uncoe (Tm= pΓ (U= pΓ))) r̃))
                     (Motives.TmΓᴹ= M {Γ}{Γ' , pΓ} U[] U[]ᴹ refl))
                 )
           )

abstract
  Π[]ᴹ : {Γ : Con}{Γᴹ : Motives.Conᴹ M Γ}
         {Δ : Con}{Δᴹ : Motives.Conᴹ M Δ}
         {δ : Tms Γ Δ}{δᴹ : Motives.Tmsᴹ M Γᴹ Δᴹ δ}
         {A : Ty Δ}{Aᴹ : Motives.Tyᴹ M Δᴹ A}
         {B : Ty (Δ , A)}{Bᴹ : Motives.Tyᴹ M ((mCon MethodsCon.,Cᴹ Δᴹ) Aᴹ) B}
       → MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{Π A B} (MethodsTy.Πᴹ mTy {Δ}{Δᴹ} Aᴹ Bᴹ) δᴹ
       ≡[ Motives.TyΓᴹ= M {Γ}{Γᴹ} Π[] ]≡
         MethodsTy.Πᴹ mTy  {Γ}{Γᴹ}
           (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{A} Aᴹ δᴹ)
           (MethodsTy._[_]Tᴹ mTy
                             {Γ , A [ δ ]T}
                             {MethodsCon._,Cᴹ_ mCon Γᴹ (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{A} Aᴹ δᴹ)}
                             {Δ , A}{MethodsCon._,Cᴹ_ mCon Δᴹ Aᴹ}{B} Bᴹ
              (MethodsTms._,sᴹ_
                 mTms
                 {Γ , A [ δ ]T}
                 {MethodsCon._,Cᴹ_ mCon Γᴹ (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{A} Aᴹ δᴹ)}
                 {Δ}{Δᴹ}
                 (MethodsTms._∘ᴹ_
                    mTms {Γ , A [ δ ]T}{MethodsCon._,Cᴹ_ mCon Γᴹ (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{A} Aᴹ δᴹ)}{Γ}{Γᴹ}{Δ}{Δᴹ}{δ}
                    δᴹ
                    {π₁ id}
                    (MethodsTms.π₁ᴹ mTms {Γ , A [ δ ]T}{MethodsCon._,Cᴹ_ mCon Γᴹ (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{A} Aᴹ δᴹ)}{Γ}{Γᴹ} (MethodsTms.idᴹ mTms {Γ , A [ δ ]T}{MethodsCon._,Cᴹ_ mCon Γᴹ (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{A} Aᴹ δᴹ)}))
                 )
                 (coe (Motives.TmΓᴹ= M {Γ , A [ δ ]T}{MethodsCon._,Cᴹ_ mCon Γᴹ (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{A} Aᴹ δᴹ)} [][]T [][]Tᴹ refl) (MethodsTm.π₂ᴹ mTm {Γ , A [ δ ]T}{MethodsCon._,Cᴹ_ mCon Γᴹ (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{A} Aᴹ δᴹ)}{Γ}{Γᴹ} (MethodsTms.idᴹ mTms {Γ , A [ δ ]T}{MethodsCon._,Cᴹ_ mCon Γᴹ (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ}{A} Aᴹ δᴹ)})))
              )
           )
  Π[]ᴹ {Γ}{Γ' Σ., pΓ}{Δ}{Δ' Σ., pΔ}{σ}{σ' Σ., pσ}{A}{A' Σ., pA}{B}{B' Σ., pB}

    = =Tyᴹ pΓ
           Π[]
           (ΠN=' refl
                 ([]nT≃ B'
                        refl
                        (norms≃ refl
                                refl
                                ( uncoe (Tms= (,C= refl ⌜[]nT⌝) refl) ⁻¹̃
                                ◾̃ ,s≃ (,C= pΓ ([]T= pΓ pΔ pA pσ) ⁻¹)
                                      (pΔ ⁻¹)
                                      (∘≃' (pΓ ⁻¹)
                                           (pΔ ⁻¹)
                                           (,C= pΓ ([]T= pΓ pΔ pA pσ) ⁻¹)
                                           (from≡ (Tms= pΓ pΔ) pσ ⁻¹̃)
                                           (π₁id≃ (pΓ ⁻¹) (from≡ (Ty= pΓ) ([]T= pΓ pΔ pA pσ) ⁻¹̃)))
                                      (from≡ (Ty= pΔ) pA ⁻¹̃)
                                      ( uncoe (TmΓ= [][]T) ⁻¹̃
                                      ◾̃ π₂id≃ (pΓ ⁻¹) (from≡ (Ty= pΓ) ([]T= pΓ pΔ pA pσ) ⁻¹̃)
                                      ◾̃ uncoe (TmΓ= [][]T))
                                ◾̃ uncoe (Tms= (,C= pΓ ([]T= pΓ pΔ pA pσ ◾ ⌜[]nT⌝)) (,C= pΔ pA))))))

mHTy : MethodsHTy M mCon mTy mTms mTm
mHTy = record
  { [id]Tᴹ = [id]Tᴹ
  ; [][]Tᴹ = [][]Tᴹ
  ; U[]ᴹ   = U[]ᴹ
  ; El[]ᴹ  = El[]ᴹ
  ; Π[]ᴹ   = Π[]ᴹ
  }
