{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import NBE.Nf
open import NBE.Nfs

module NBE.experiments.NormalModel.HTms
  (norms  : ∀{Γ Δ} → Tms Γ Δ → Nfs Γ Δ)
  (compls : ∀{Γ Δ}(σ : Tms Γ Δ) → σ ≡ ⌜ norms σ ⌝nfs)
  (normt  : ∀{Γ A} → Tm Γ A → Nf Γ A)
  (complt : ∀{Γ A}(t : Tm Γ A) → t ≡ ⌜ normt t ⌝nf)
  (stabs  : ∀{Γ Δ}(τ : Nfs Γ Δ) → norms ⌜ τ ⌝nfs ≡ τ)
  (stabt  : ∀{Γ A}(n : Nf Γ A) → normt ⌜ n ⌝nf ≡ n)
  where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.Elim
open import NBE.experiments.NConTy
open import NBE.experiments.NormalModel.Motives
open import NBE.experiments.NormalModel.Cxt
open import NBE.experiments.NormalModel.Ty  norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.Tms norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.Tm norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.HTy norms compls normt complt stabs stabt

abstract
  idlᴹ : {Γ : Con}{Γᴹ : Motives.Conᴹ M Γ}
         {Δ : Con}{Δᴹ : Motives.Conᴹ M Δ}
         {δ : Tms Γ Δ}{δᴹ : Motives.Tmsᴹ M Γᴹ Δᴹ δ}
       → MethodsTms._∘ᴹ_ mTms {Γ}{Γᴹ}{Δ}{Δᴹ}{Δ}{Δᴹ} (MethodsTms.idᴹ mTms {Δ}{Δᴹ}) δᴹ
       ≡[ Motives.TmsΓΔᴹ= M {Γ}{Γᴹ}{Δ}{Δᴹ} idl ]≡
         δᴹ
  idlᴹ {Γ}{Γ' , pΓ}{Δ}{Δ' , pΔ}{σ}{σ' , pσ}
    = =Tmsᴹ pΓ pΔ idl (inj⌜⌝nfs (compls (coe (Tms= pΓ pΔ) (id ∘ σ)) ⁻¹ ◾ from≃ (uncoe (Tms= pΓ pΔ) ⁻¹̃ ◾̃ to≃ idl ◾̃ from≡ (Tms= pΓ pΔ) pσ)))

abstract
  idrᴹ : {Γ : Con}{Γᴹ : Motives.Conᴹ M Γ}
         {Δ : Con}{Δᴹ : Motives.Conᴹ M Δ}
         {δ : Tms Γ Δ}{δᴹ : Motives.Tmsᴹ M Γᴹ Δᴹ δ}
       → MethodsTms._∘ᴹ_ mTms {Γ}{Γᴹ}{Γ}{Γᴹ}{Δ}{Δᴹ} δᴹ (MethodsTms.idᴹ mTms {Γ}{Γᴹ})
       ≡[ Motives.TmsΓΔᴹ= M {Γ}{Γᴹ}{Δ}{Δᴹ} idr ]≡
         δᴹ
  idrᴹ {Γ}{Γ' , pΓ}{Δ}{Δ' , pΔ}{σ}{σ' , pσ}
    = =Tmsᴹ pΓ pΔ idr (inj⌜⌝nfs (compls (coe (Tms= pΓ pΔ) (σ ∘ id)) ⁻¹ ◾ from≃ (uncoe (Tms= pΓ pΔ) ⁻¹̃ ◾̃ to≃ idr ◾̃ from≡ (Tms= pΓ pΔ) pσ)))

abstract
  assᴹ : {Γ : Con}{Γᴹ : Motives.Conᴹ M Γ}
         {Δ : Con}{Δᴹ : Motives.Conᴹ M Δ}
         {Σ : Con}{Σᴹ : Motives.Conᴹ M Σ}
         {Ω : Con}{Ωᴹ : Motives.Conᴹ M Ω}
         {σ : Tms Σ Ω}{σᴹ : Motives.Tmsᴹ M Σᴹ Ωᴹ σ}
         {δ : Tms Γ Σ}{δᴹ : Motives.Tmsᴹ M Γᴹ Σᴹ δ}
         {ν : Tms Δ Γ}{νᴹ : Motives.Tmsᴹ M Δᴹ Γᴹ ν}
       → MethodsTms._∘ᴹ_ mTms {Δ}{Δᴹ}{Γ}{Γᴹ}{Ω}{Ωᴹ} (MethodsTms._∘ᴹ_ mTms {Γ}{Γᴹ}{Σ}{Σᴹ}{Ω}{Ωᴹ} σᴹ δᴹ) νᴹ
       ≡[ Motives.TmsΓΔᴹ= M {Δ}{Δᴹ}{Ω}{Ωᴹ} ass ]≡
         MethodsTms._∘ᴹ_ mTms {Δ}{Δᴹ}{Σ}{Σᴹ}{Ω}{Ωᴹ} σᴹ (MethodsTms._∘ᴹ_ mTms {Δ}{Δᴹ}{Γ}{Γᴹ}{Σ}{Σᴹ} δᴹ νᴹ)
  assᴹ {Γ}{Γ' , pΓ}{Δ}{Δ' , pΔ}{Σ}{Σ' , pΣ}{Ω}{Ω' , pΩ}{σ}{σ' , pσ}{δ}{δ' , pδ}{ν}{ν' , pν}
    = =Tmsᴹ pΔ pΩ ass (ap (λ z → norms (coe (Tms= pΔ pΩ) z)) ass)

abstract
  π₁βᴹ : {Γ : Con}{Γᴹ : Motives.Conᴹ M Γ}
         {Δ : Con}{Δᴹ : Motives.Conᴹ M Δ}
         {A : Ty Δ}{Aᴹ : Motives.Tyᴹ M Δᴹ A}
         {δ : Tms Γ Δ}{δᴹ : Motives.Tmsᴹ M Γᴹ Δᴹ δ}
         {a : Tm Γ (A [ δ ]T)}{aᴹ : Motives.Tmᴹ M Γᴹ (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ} Aᴹ δᴹ) a}
       →  MethodsTms.π₁ᴹ mTms {Γ}{Γᴹ}{Δ}{Δᴹ}{A}{Aᴹ}
                         (MethodsTms._,sᴹ_ mTms {Γ}{Γᴹ}{Δ}{Δᴹ}{δ} δᴹ {A}{Aᴹ}{a} aᴹ)
       ≡[ Motives.TmsΓΔᴹ= M {Γ}{Γᴹ} π₁β ]≡
         δᴹ
  π₁βᴹ {Γ}{Γ' , pΓ}{Δ}{Δ' , pΔ}{A}{A' , pA}{δ}{δ' , pδ}{a}{a' , pa}
    = =Tmsᴹ pΓ pΔ π₁β (inj⌜⌝nfs (compls (coe (Tms= pΓ pΔ) (π₁ (δ , a))) ⁻¹ ◾ from≃ (uncoe (Tms= pΓ pΔ) ⁻¹̃ ◾̃ (to≃ π₁β ◾̃ from≡ (Tms= pΓ pΔ) pδ))))

abstract
  πηᴹ : {Γ : Con}{Γᴹ : Motives.Conᴹ M Γ}
        {Δ : Con}{Δᴹ : Motives.Conᴹ M Δ}
        {A : Ty Δ}{Aᴹ : Motives.Tyᴹ M Δᴹ A}
        {δ : Tms Γ (Δ , A)}{δᴹ : Motives.Tmsᴹ M Γᴹ ((mCon MethodsCon.,Cᴹ Δᴹ) Aᴹ) δ}
      → MethodsTms._,sᴹ_ mTms {Γ}{Γᴹ}{Δ}{Δᴹ}{π₁ δ} (MethodsTms.π₁ᴹ mTms {Γ}{Γᴹ}{Δ}{Δᴹ} δᴹ) (MethodsTm.π₂ᴹ mTm {Γ}{Γᴹ}{Δ}{Δᴹ} δᴹ)
      ≡[ Motives.TmsΓΔᴹ= M {Γ}{Γᴹ}{Δ , A}{MethodsCon._,Cᴹ_ mCon {Δ} Δᴹ {A} Aᴹ}{π₁ δ , π₂ δ}{δ} πη ]≡
        δᴹ
  πηᴹ {Γ}{Γ' , pΓ}{Δ}{Δ' , pΔ}{A}{A' , pA}{δ}{δ' , pδ}
  
    = =Tmsᴹ pΓ (,C= pΔ pA) πη (inj⌜⌝nfs (compls (coe (Tms= pΓ (,C= pΔ pA)) (π₁ δ , π₂ δ)) ⁻¹ ◾ from≃ (uncoe (Tms= pΓ (,C= pΔ pA)) ⁻¹̃ ◾̃ to≃ πη ◾̃ from≡ (Tms= pΓ (,C= pΔ pA)) pδ)))

abstract
  εηᴹ : {Γ : Con}{Γᴹ : Motives.Conᴹ M Γ}
        {σ : Tms Γ •}{σᴹ : Motives.Tmsᴹ M Γᴹ (MethodsCon.•ᴹ mCon) σ}
      → σᴹ ≡[ Motives.TmsΓΔᴹ= M {Γ}{Γᴹ} εη ]≡ MethodsTms.εᴹ mTms {Γ}{Γᴹ}
  εηᴹ {Γ}{Γ' , pΓ}{σ}{σ' , pσ}
    = =Tmsᴹ pΓ refl εη (inj⌜⌝nfs (from≃ (from≡ (Tms= pΓ refl) pσ ⁻¹̃ ◾̃ to≃ εη ◾̃ uncoe (Tms= pΓ refl)) ◾ compls (coe (Tms= pΓ refl) ε)))

abstract
  ,∘ᴹ : {Γ : Con}{Γᴹ : Motives.Conᴹ M Γ}
        {Δ : Con}{Δᴹ : Motives.Conᴹ M Δ}
        {Σ : Con}{Σᴹ : Motives.Conᴹ M Σ}
        {δ : Tms Γ Δ}{δᴹ : Motives.Tmsᴹ M Γᴹ Δᴹ δ}
        {σ : Tms Σ Γ}{σᴹ : Motives.Tmsᴹ M Σᴹ Γᴹ σ}
        {A : Ty Δ}{Aᴹ : Motives.Tyᴹ M Δᴹ A}
        {a : Tm Γ (A [ δ ]T)}{aᴹ : Motives.Tmᴹ M Γᴹ (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ} Aᴹ δᴹ) a}
      → MethodsTms._∘ᴹ_ mTms {Σ}{Σᴹ}{Γ}{Γᴹ}{Δ , A}{MethodsCon._,Cᴹ_ mCon Δᴹ Aᴹ} (MethodsTms._,sᴹ_ mTms {Γ}{Γᴹ}{Δ}{Δᴹ} δᴹ aᴹ) σᴹ
      ≡[ Motives.TmsΓΔᴹ= M {Σ}{Σᴹ}{Δ , A}{MethodsCon._,Cᴹ_ mCon Δᴹ Aᴹ} ,∘ ]≡
        MethodsTms._,sᴹ_ mTms {Σ}{Σᴹ}{Δ}{Δᴹ}
          (MethodsTms._∘ᴹ_ mTms {Σ}{Σᴹ}{Γ}{Γᴹ}{Δ}{Δᴹ} δᴹ σᴹ)
          (coe (Motives.TmΓᴹ= M [][]T (MethodsHTy.[][]Tᴹ mHTy {Σ}{Σᴹ}{Γ}{Γᴹ}{Δ}{Δᴹ}) refl)
               (MethodsTm._[_]tᴹ mTm {Σ}{Σᴹ}{Γ}{Γᴹ} aᴹ σᴹ))
  ,∘ᴹ {Γ}{Γ' , pΓ}{Δ}{Δ' , pΔ}{Σ}{Σ' , pΣ}{δ}{δ' , pδ}{σ}{σ' , pσ}{A}{A' , pA}{a}{a' , pa}
  
    = =Tmsᴹ pΣ (,C= pΔ pA) ,∘ (ap (λ z → norms (coe (Tms= pΣ (,C= pΔ pA)) z)) ,∘)

mHTms : MethodsHTms M mCon mTy mTms mTm mHTy
mHTms = record
  { idlᴹ = idlᴹ
  ; idrᴹ = idrᴹ
  ; assᴹ = λ { {_}{_}{_}{_}{_}{_}{_}{_}{_}{σᴹ}{_}{δᴹ}{_}{νᴹ} → assᴹ {σᴹ = σᴹ}{_}{δᴹ}{_}{νᴹ} }
  ; π₁βᴹ = λ { {_}{_}{_}{_}{_}{_}{_}{δᴹ}{_}{aᴹ} → π₁βᴹ {δᴹ = δᴹ}{_}{aᴹ} }
  ; πηᴹ  = πηᴹ
  ; εηᴹ  = εηᴹ
  ; ,∘ᴹ  = λ { {_}{_}{_}{_}{_}{_}{_}{δᴹ}{_}{σᴹ}{_}{_}{_}{aᴹ} → ,∘ᴹ {δᴹ = δᴹ}{σᴹ = σᴹ}{aᴹ = aᴹ} }
  }
