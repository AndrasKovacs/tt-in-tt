{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Ty.Pi where

open import lib
open import JM.JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty.Subst

open Motives M
open MethodsCon mCon

open import NBE.Cheat

abstract
  projTbase : ∀{Γ Ψ A}(⟦Γ⟧ : FamPSh (TMC Γ))
              {ρ : TMC Γ $P Ψ}
              {α : ⟦Γ⟧ $F ρ}{Ω}{β : Vars Ω Ψ}
              {u : TMT A $F (ρ ∘ ⌜ β ⌝V)}
            → _≡_ {A = (TMC Γ ,P TMT A ,P ⟦Γ⟧ [ wkn ]F) $P Ω}
                  (ρ ∘ ⌜ β ⌝V , u , ⟦Γ⟧ $F β $ α)
                  (π₁ (ρ ∘ ⌜ β ⌝V , u) , π₂ (ρ ∘ ⌜ β ⌝V , u) , coe (ap (_$F_ ⟦Γ⟧) (π₁β ⁻¹)) (⟦Γ⟧ $F β $ α))
  projTbase ⟦Γ⟧ = Tbase= ⟦Γ⟧ (π₁β ⁻¹)
                             (π₂β' ⁻¹̃)
                             (uncoe ($F= ⟦Γ⟧ (π₁β ⁻¹)))
                             
module mΠᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {A : Ty Γ}(⟦A⟧ : Tyᴹ ⟦Γ⟧ A)
  {B : Ty (Γ , A)}(⟦B⟧ : Tyᴹ (⟦Γ⟧ ,Cᴹ ⟦A⟧) B)
  where

    abstract
      pcoe1 : ∀{Ψ}{ρ : TMC Γ $P Ψ}{Ω}{δ : Tms Ω Ψ}{u : TMT A $F (ρ ∘ δ)}
            → B [ ρ ∘ δ ^ A ]T [ < u > ]T ≡ B [ ρ ∘ δ , u ]T
      pcoe1 = [][]T ◾ ap (_[_]T B) (^∘<> refl)

    abstract
      pcoe2 : ∀{Ψ}{ρ : TMC Γ $P Ψ}{Ω}{δ : Tms Ω Ψ}
            → Π A B [ ρ ]T [ δ ]T
            ≡ Π (A [ ρ ∘ δ ]T) (B [ ρ ∘ δ ^ A ]T)
      pcoe2 = [][]T ◾ Π[]

    abstract
      pcoe3 : ∀{Ψ}{ρ : TMC Γ $P Ψ}{Ω}{β : Vars Ω Ψ}{Ξ}{γ : Vars Ξ Ω}
            → A [ ρ ∘ ⌜ β ⌝V ]T [ ⌜ γ ⌝V ]T ≡ A [ ρ ∘ ⌜ β ∘V γ ⌝V ]T
      pcoe3 = [][]T ◾ ap (_[_]T A) (ass ◾ ap (_∘_ _) ⌜∘V⌝)

    abstract
      pcoe4 : ∀{Ψ}{ρ : TMC Γ $P Ψ}(s : TMT (Π A B) $F ρ){α : ⟦Γ⟧ $F ρ}
              {Ω}{β : Vars Ω Ψ}{u : TMT A $F (ρ ∘ ⌜ β ⌝V)}
              {Ξ}{γ : Vars Ξ Ω}
            → _≡_ {A = (TMC Γ ,P TMT A ,P ⟦Γ⟧ [ wkn ]F) $P Ξ}
                  ( (ρ ∘ ⌜ β ⌝V) ∘ ⌜ γ ⌝V
                  , coe (TmΓ= [][]T) (u [ ⌜ γ ⌝V ]t)
                  , ⟦Γ⟧ $F γ $ (⟦Γ⟧ $F β $ α))
                  ( ρ ∘ ⌜ β ∘V γ ⌝V
                  , coe (TmΓ= pcoe3) (u [ ⌜ γ ⌝V ]t)
                  , ⟦Γ⟧ $F β ∘V γ $ α)
      pcoe4 s = Tbase= ⟦Γ⟧ (ass ◾ ap (_∘_ _) ⌜∘V⌝)
                           ( uncoe (TmΓ= [][]T) ⁻¹̃
                           ◾̃ uncoe (TmΓ= pcoe3))
                           (compF' ⟦Γ⟧ ⁻¹̃)

    abstract
      pcoe5 : ∀{Ψ}{ρ : TMC Γ $P Ψ}{s : TMT (Π A B) $F ρ}{α : ⟦Γ⟧ $F ρ}
              {Ω}{β : Vars Ω Ψ}{u : TMT A $F (ρ ∘ ⌜ β ⌝V)}
              {v : ⟦A⟧ $F (ρ ∘ ⌜ β ⌝V , u , ⟦Γ⟧ $F β $ α)}
              {Ξ}{γ : Vars Ξ Ω}
            → _≡_ {A = (TMC (Γ , A) ,P TMT B ,P (⟦Γ⟧ ,Cᴹ ⟦A⟧) [ wkn ]F) $P Ξ}
                  ((TMC (Γ , A) ,P TMT B ,P (⟦Γ⟧ ,Cᴹ ⟦A⟧) [ wkn ]F) $P γ $
                      ( (ρ ∘ ⌜ β ⌝V , u)
                      , coe (TmΓ= pcoe1) (coe (TmΓ= pcoe2) (s [ ⌜ β ⌝V ]t) $$ u)
                      , ( coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β $ α)
                        , coe ($F= ⟦A⟧ (projTbase ⟦Γ⟧)) v)))
                  ( (ρ ∘ ⌜ β ∘V γ ⌝V , coe (TmΓ= pcoe3) (u [ ⌜ γ ⌝V ]t))
                  , coe (TmΓ= pcoe1) (coe (TmΓ= pcoe2) (s [ ⌜ β ∘V γ ⌝V ]t) $$ coe (TmΓ= pcoe3) (u [ ⌜ γ ⌝V ]t))
                  , ( coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β ∘V γ $ α)
                    , coe ($F= ⟦A⟧ (projTbase ⟦Γ⟧)) (coe ($F= ⟦A⟧ (pcoe4 s)) (⟦A⟧ $F γ $ v))))
      pcoe5 {Ψ}{ρ}{s}{α}{Ω}{β}{u}{v}{Ξ}{γ}
        = Tbase= (⟦Γ⟧ ,Cᴹ ⟦A⟧)
                 p
                 ( uncoe (TmΓ= [][]T) ⁻¹̃
                 ◾̃ coe[]t' pcoe1
                 ◾̃ [][]t'
                 ◾̃ []t≃ refl (to≃ ,∘)
                 ◾̃ app[,]
                 ◾̃ []t≃' refl
                         (,C≃ refl (to≃ q))
                         (to≃ [][]T ◾̃ []T≃ (,C≃ refl (to≃ q)) r)
                         (app≃ refl (to≃ q) (to≃ [][]T ◾̃ []T≃ (,C≃ refl (to≃ q)) r)
                               ( uncoe (TmΓ= Π[]) ⁻¹̃
                               ◾̃ coe[]t' pcoe2
                               ◾̃ [][]t'
                               ◾̃ []t≃'' (ap (_∘_ ⌜ β ⌝V) idl ◾ ⌜∘V⌝)
                               ◾̃ uncoe (TmΓ= pcoe2)))
                         (,s≃ refl
                              refl
                              r̃
                              (to≃ q)
                              ( uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃
                              ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                              ◾̃ coe[]t' ([id]T ⁻¹)
                              ◾̃ uncoe (TmΓ= pcoe3)
                              ◾̃ uncoe (TmΓ= ([id]T ⁻¹))))
                 ◾̃ uncoe (TmΓ= pcoe1))
                 (,≃ (funext≃ ($F= ⟦Γ⟧ (ap π₁ p))
                              (λ x₂ → to≃ ($F= ⟦A⟧ (Tbase= ⟦Γ⟧ (ap π₁ p) (π₂≃ p) x₂ ))))
                     ( uncoe ($F= ⟦Γ⟧ π₁∘) ⁻¹̃
                     ◾̃ $Ff$≃ ⟦Γ⟧ π₁β (uncoe ($F= ⟦Γ⟧ (π₁β ⁻¹)) ⁻¹̃)
                     ◾̃ compF' ⟦Γ⟧ ⁻¹̃
                     ◾̃ uncoe ($F= ⟦Γ⟧ (π₁β ⁻¹)))
                     ( uncoe ($F= ⟦A⟧ (m,Cᴹ.pcoe ⟦Γ⟧ ⟦A⟧ (coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β $ α) , coe ($F= ⟦A⟧ (projTbase ⟦Γ⟧)) v))) ⁻¹̃
                     ◾̃ $Ff$≃ ⟦A⟧ (Tbase= ⟦Γ⟧ π₁β
                                             π₂β'
                                             (uncoe (ap (_$F_ ⟦Γ⟧) (π₁β ⁻¹)) ⁻¹̃))
                                 (uncoe ($F= ⟦A⟧ (projTbase ⟦Γ⟧)) ⁻¹̃)
                     ◾̃ uncoe ($F= ⟦A⟧ (pcoe4 s))
                     ◾̃ uncoe ($F= ⟦A⟧ (projTbase ⟦Γ⟧))))
        where
          p : TMC (Γ , A) $P γ $ (ρ ∘ ⌜ β ⌝V , u) ≡
              ρ ∘ ⌜ β ∘V γ ⌝V , coe (TmΓ= pcoe3) (u [ ⌜ γ ⌝V ]t)
          p = ,∘
            ◾ ,s≃' (ass ◾ ap (_∘_ _) ⌜∘V⌝)
                   ( uncoe (TmΓ= [][]T) ⁻¹̃
                   ◾̃ uncoe (TmΓ= pcoe3))

          q : _≡_ {A = Ty Ξ}
                  (A [ ρ ∘ ⌜ β ⌝V ]T [ id ∘ ⌜ γ ⌝V ]T)
                  (A [ ρ ∘ ⌜ β ∘V γ ⌝V ]T)
          q = [][]T ◾ ap (_[_]T A) (ass ◾ ap (_∘_ ρ) (ap (_∘_ ⌜ β ⌝V) idl ◾ ⌜∘V⌝))

          r : (ρ ∘ ⌜ β ⌝V ^ A) ∘ (id ∘ ⌜ γ ⌝V ^ A [ ρ ∘ ⌜ β ⌝V ]T)
            ≃ ρ ∘ ⌜ β ∘V γ ⌝V ^ A
          r = ∘^ ⁻¹̃
            ◾̃ ^≃ (ap (_∘_ (ρ ∘ ⌜ β ⌝V)) idl ◾ (ass ◾ ap (_∘_ ρ) ⌜∘V⌝))

    record Exp {Ψ : Con}
               (ρ : TMC Γ $P Ψ)
               (s : TMT (Π A B) $F ρ)
               (α : ⟦Γ⟧ $F ρ) : Set where

      field
        map : ∀{Ω}(β : Vars Ω Ψ)
              {u : TMT A $F (ρ ∘ ⌜ β ⌝V)}
              (v : ⟦A⟧ $F (ρ ∘ ⌜ β ⌝V , u , ⟦Γ⟧ $F β $ α))
            → ⟦B⟧ $F ( (ρ ∘ ⌜ β ⌝V , u)
                     , coe (TmΓ= pcoe1) (coe (TmΓ= pcoe2) (s [ ⌜ β ⌝V ]t) $$ u)
                     , ( (coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β $ α))
                       , coe ($F= ⟦A⟧ (projTbase ⟦Γ⟧)) v))
        nat : ∀{Ω}{β : Vars Ω Ψ}
              {u : TMT A $F (ρ ∘ ⌜ β ⌝V)}
              {v : ⟦A⟧ $F (ρ ∘ ⌜ β ⌝V , u , ⟦Γ⟧ $F β $ α)}
              {Ξ}{γ : Vars Ξ Ω}
            → ⟦B⟧ $F γ $ map β v
            ≡[ $F= ⟦B⟧ pcoe5 ]≡
              map (β ∘V γ) (coe ($F= ⟦A⟧ (pcoe4 s)) (⟦A⟧ $F γ $ v))

    Πᴹ : Tyᴹ ⟦Γ⟧ (Π A B)
    Πᴹ = record
      { _$F_   = λ { (ρ , s , α) → Exp ρ s α }
      ; _$F_$_ = cheat
      ; idF    = cheat
      ; compF  = cheat
      }
