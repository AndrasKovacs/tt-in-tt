{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

open import lib
open import JM.JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty.Subst
open import NBE.LogPred.Ty.Laws
open import NBE.LogPred.Ty.Exp

open Motives M
open MethodsCon mCon

module NBE.LogPred.Ty.LiftExp
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {A : Ty Γ}{⟦A⟧ : Tyᴹ ⟦Γ⟧ A}
  {B : Ty (Γ , A)}{⟦B⟧ : Tyᴹ (⟦Γ⟧ ,Cᴹ ⟦A⟧) B}
  {Ψ : Con}
  {ρ : TMC Γ $P Ψ}
  {s : TMT (Π A B) $F ρ}
  {α : ⟦Γ⟧ $F ρ}
  {Ψ' : Con}(β' : Vars Ψ' Ψ)
  (e : E ⟦B⟧ ρ s α)
  where

abstract
  pcoe6 : ∀{Ω}{β : Vars Ω Ψ'}
        → (ρ ∘ ⌜ β' ⌝V) ∘ ⌜ β ⌝V ≡ ρ ∘ ⌜ β' ∘V β ⌝V
  pcoe6 = ass ◾ ap (_∘_ _) ⌜∘V⌝

abstract
  pcoe7 : ∀{Ω}{β : Vars Ω Ψ'}
          {u : TMT A $F ((TMC Γ $P β' $ ρ) ∘ ⌜ β ⌝V)}
        → _≡_ {A = (TMC Γ ,P TMT A ,P ⟦Γ⟧ [ wkn ]F) $P Ω}
              ( (TMC Γ $P β' $ ρ) ∘ ⌜ β ⌝V
              , u
              , ⟦Γ⟧ $F β $ (⟦Γ⟧ $F β' $ α))
              ( ρ ∘ ⌜ β' ∘V β ⌝V
              , coe (TmΓ= (ap (_[_]T A) pcoe6)) u
              , ⟦Γ⟧ $F β' ∘V β $ α)
  pcoe7 = Tbase= ⟦Γ⟧
                 pcoe6
                 (uncoe (TmΓ= (ap (_[_]T A) pcoe6)))
                 (compF' ⟦Γ⟧ ⁻¹̃)

abstract
  pcoe8
    : ∀{Ω}{β : Vars Ω Ψ'}
      {u : TMT A $F ((TMC Γ $P β' $ ρ) ∘ ⌜ β ⌝V)}
      {v : ⟦A⟧ $F ((TMC Γ $P β' $ ρ) ∘ ⌜ β ⌝V , u , ⟦Γ⟧ $F β $ (⟦Γ⟧ $F β' $ α))}
    → _≡_
        {A = (TMC (Γ , A) ,P TMT B ,P (⟦Γ⟧ ,Cᴹ ⟦A⟧) [ wkn ]F) $P Ω}
        ( (ρ ∘ ⌜ β' ∘V β ⌝V , coe (TmΓ= (ap (_[_]T A) pcoe6)) u)
        , coe (TmΓ= (pcoe1 ⟦B⟧ ρ s α)) (coe (TmΓ= (pcoe2 ⟦B⟧ ρ s α)) (s [ ⌜ β' ∘V β ⌝V ]t) $$ coe (TmΓ= (ap (_[_]T A) pcoe6)) u)
        , (coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β' ∘V β $ α) , coe ($F= ⟦A⟧ (projTbase ⟦B⟧ ρ s α)) (coe ($F= ⟦A⟧ pcoe7) v)))
        ( ((TMC Γ $P β' $ ρ) ∘ ⌜ β ⌝V , u)
        , coe (TmΓ= (pcoe1 ⟦B⟧ (TMC Γ $P β' $ ρ) (TMT (Π A B) $F β' $ s) (⟦Γ⟧ $F β' $ α))) (coe (TmΓ= (pcoe2 ⟦B⟧ (TMC Γ $P β' $ ρ) (TMT (Π A B) $F β' $ s) (⟦Γ⟧ $F β' $ α))) ((TMT (Π A B) $F β' $ s) [ ⌜ β ⌝V ]t) $$ u)
        , (coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β $ (⟦Γ⟧ $F β' $ α)) , coe ($F= ⟦A⟧ (projTbase ⟦B⟧ (TMC Γ $P β' $ ρ) (TMT (Π A B) $F β' $ s) (⟦Γ⟧ $F β' $ α))) v))
  pcoe8 {Ω}{β}{u}{v}
  
    = Tbase= (⟦Γ⟧ ,Cᴹ ⟦A⟧)
              (,s≃' (pcoe6 ⁻¹) (uncoe (TmΓ= (ap (_[_]T A) pcoe6)) ⁻¹̃))
              ( uncoe (TmΓ= (pcoe1 ⟦B⟧ ρ s α)) ⁻¹̃
              ◾̃ $≃' (ap (_[_]T A) (pcoe6 ⁻¹))
                    ([]T≃ (,C≃ refl (to≃ (ap (_[_]T A) (pcoe6 ⁻¹))))
                          (^≃ (pcoe6 ⁻¹)))
                    (uncoe (TmΓ= (pcoe2 ⟦B⟧ ρ s α)) ⁻¹̃
                    ◾̃ []t≃ refl (to≃ (⌜∘V⌝ ⁻¹))
                    ◾̃ [][]t' ⁻¹̃
                    ◾̃ coe[]t' [][]T ⁻¹̃
                    ◾̃ uncoe (TmΓ= (pcoe2 ⟦B⟧ (TMC Γ $P β' $ ρ) (TMT (Π A B) $F β' $ s) (⟦Γ⟧ $F β' $ α))))
                    (uncoe (TmΓ= (ap (_[_]T A) pcoe6)) ⁻¹̃)
              ◾̃ uncoe (TmΓ= (pcoe1 ⟦B⟧ (TMC Γ $P β' $ ρ) (TMT (Π A B) $F β' $ s) (⟦Γ⟧ $F β' $ α))))
              (,≃ (funext≃ ($F= ⟦Γ⟧ (π₁β ◾ pcoe6 ⁻¹ ◾ π₁β ⁻¹))
                           (λ x₂ → to≃ ($F= ⟦A⟧ (Tbase= ⟦Γ⟧
                                                        (π₁β ◾ pcoe6 ⁻¹ ◾ π₁β ⁻¹)
                                                        (π₂β' ◾̃ uncoe (TmΓ= (ap (_[_]T A) pcoe6)) ⁻¹̃ ◾̃ π₂β' ⁻¹̃)
                                                        x₂))))
                  ( uncoe ($F= ⟦Γ⟧ (π₁β ⁻¹)) ⁻¹̃
                  ◾̃ compF' ⟦Γ⟧
                  ◾̃ uncoe ($F= ⟦Γ⟧ (π₁β ⁻¹)))
                  ( uncoe ($F= ⟦A⟧ (projTbase ⟦B⟧ ρ s α)) ⁻¹̃
                  ◾̃ uncoe ($F= ⟦A⟧ pcoe7) ⁻¹̃
                  ◾̃ uncoe ($F= ⟦A⟧ (projTbase ⟦B⟧ (TMC Γ $P β' $ ρ) (TMT (Π A B) $F β' $ s) (⟦Γ⟧ $F β' $ α)))))

open E ⟦B⟧ ρ s α e

abstract
  pnat
    : {Ω : Con}{β : Vars Ω Ψ'}
      {u : TMT A $F ((TMC Γ $P β' $ ρ) ∘ ⌜ β ⌝V)}
      {v : ⟦A⟧ $F ((TMC Γ $P β' $ ρ) ∘ ⌜ β ⌝V , u , ⟦Γ⟧ $F β $ (⟦Γ⟧ $F β' $ α))}
      {Ξ : Con}{γ : Vars Ξ Ω}
    → ⟦B⟧ $F γ $ coe ($F= ⟦B⟧ pcoe8) (map (β' ∘V β) (coe ($F= ⟦A⟧ pcoe7) v))
      ≡[ $F= ⟦B⟧ (pcoe5 ⟦B⟧ (TMC Γ $P β' $ ρ) (TMT (Π A B) $F β' $ s) (⟦Γ⟧ $F β' $ α)) ]≡
      coe ($F= ⟦B⟧ pcoe8) (map (β' ∘V (β ∘V γ)) (coe ($F= ⟦A⟧ pcoe7) (coe ($F= ⟦A⟧ (pcoe4 ⟦B⟧ (TMC Γ $P β' $ ρ) (TMT (Π A B) $F β' $ s) (⟦Γ⟧ $F β' $ α))) (⟦A⟧ $F γ $ v))))
  pnat
    = from≃ ( uncoe ($F= ⟦B⟧ (pcoe5 ⟦B⟧ (TMC Γ $P β' $ ρ) (TMT (Π A B) $F β' $ s) (⟦Γ⟧ $F β' $ α))) ⁻¹̃
            ◾̃ $Ff$≃ ⟦B⟧ (pcoe8 ⁻¹) (uncoe ($F= ⟦B⟧ pcoe8) ⁻¹̃)
            ◾̃ from≡ ($F= ⟦B⟧ (pcoe5 ⟦B⟧ ρ s α)) nat
            ◾̃ map≃ ⟦B⟧ ρ s α e
                   assV
                   ( uncoe (TmΓ= (pcoe3 ⟦B⟧ ρ s α)) ⁻¹̃
                   ◾̃ coe[]t' (ap (_[_]T A) pcoe6)
                   ◾̃ uncoe (TmΓ= (pcoe3 ⟦B⟧ (TMC Γ $P β' $ ρ) (TMT (Π A B) $F β' $ s) (⟦Γ⟧ $F β' $ α)))
                   ◾̃ uncoe (TmΓ= (ap (_[_]T A) pcoe6)))
                   ( uncoe ($F= ⟦A⟧ (pcoe4 ⟦B⟧ ρ s α)) ⁻¹̃
                   ◾̃ $Ff$≃ ⟦A⟧ (pcoe7 ⁻¹) (uncoe ($F= ⟦A⟧ pcoe7) ⁻¹̃)
                   ◾̃ uncoe ($F= ⟦A⟧ (pcoe4 ⟦B⟧ (TMC Γ $P β' $ ρ) (TMT (Π A B) $F β' $ s) (⟦Γ⟧ $F β' $ α)))
                   ◾̃ uncoe ($F= ⟦A⟧ pcoe7))
            ◾̃ uncoe ($F= ⟦B⟧ pcoe8))


lift : E ⟦B⟧ (TMC Γ $P β' $ ρ) (TMT (Π A B) $F β' $ s) (⟦Γ⟧ $F β' $ α)
lift = record
  { map = λ β v → coe ($F= ⟦B⟧ pcoe8)
                      (map (β' ∘V β) (coe ($F= ⟦A⟧ pcoe7) v))
  ; nat = pnat
  }
