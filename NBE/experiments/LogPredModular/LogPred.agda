{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.LogPred
  (U̅ : FamPSh TMU) -- U^--
  (E̅l : FamPSh (TMU ,P TMEl ,P U̅ [ wkn ]F)) -- E^--l
  where

open import TT.Elim
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty U̅ E̅l
open import NBE.LogPred.Tms U̅ E̅l
open import NBE.LogPred.Tm U̅ E̅l
open import NBE.LogPred.HTy U̅ E̅l
open import NBE.LogPred.HTms U̅ E̅l
open import NBE.LogPred.HTm U̅ E̅l

open elim M mCon mTy mTms mTm mHTy mHTms mHTm

⟦_⟧C = Con-elim
⟦_⟧T = Ty-elim
⟦_⟧s = Tms-elim
⟦_⟧t = Tm-elim
