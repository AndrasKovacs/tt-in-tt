{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Tm.App
  (U̅ : FamPSh TMU) -- U^--
  (E̅l : FamPSh (TMU ,P TMEl ,P U̅ [ wkn ]F)) -- E^--l
  where

open import lib
open import JM.JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty U̅ E̅l
open import NBE.LogPred.Tms U̅ E̅l
open import NBE.LogPred.Ty.Pi
open import NBE.LogPred.Tm.Subst U̅ E̅l
open import NBE.LogPred.Tm.Proj U̅ E̅l

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms

open import NBE.Cheat

module mappᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {A : Ty Γ}{⟦A⟧ : Tyᴹ ⟦Γ⟧ A}
  {B : Ty (Γ , A)}{⟦B⟧ : Tyᴹ (⟦Γ⟧ ,Cᴹ ⟦A⟧) B}
  {t : Tm Γ (Π A B)}(⟦t⟧ : Tmᴹ ⟦Γ⟧ (Πᴹ ⟦A⟧ ⟦B⟧) t)
  where

    open mΠᴹ.Exp ⟦A⟧ ⟦B⟧

    abstract
      pcoe$S₁ : ∀{Ψ}{ρ : TMC (Γ , A) $P Ψ}(α : (⟦Γ⟧ ,Cᴹ ⟦A⟧) $F ρ)
              → _≡_ {A = (TMC Γ ,P TMT A ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}
                    (π₁ ρ , π₂ ρ , proj₁ α)
                    ( π₁ ρ ∘ ⌜ idV ⌝V
                    , coe (TmΓ= [][]T) (π₂ ρ [ ⌜ idV ⌝V ]t)
                    , ⟦Γ⟧ $F idV $ proj₁ α)
      pcoe$S₁ {Ψ}{ρ} α
      
        = Tbase= ⟦Γ⟧
                 (idr ⁻¹ ◾ ap (_∘_ _) (⌜idV⌝ ⁻¹))
                 ([⌜idV⌝]t ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T))
                 (idF' ⟦Γ⟧ ⁻¹̃)

    abstract
      pcoe$S₂ : ∀{Ψ}{ρ : TMC (Γ , A) $P Ψ}{α : (⟦Γ⟧ ,Cᴹ ⟦A⟧) $F ρ}
              → _≡_ {A = (TMC (Γ , A) ,P TMT B ,P (⟦Γ⟧ ,Cᴹ ⟦A⟧) [ wkn ]F) $P Ψ}
                    ( ( π₁ ρ ∘ ⌜ idV ⌝V
                      , coe (TmΓ= [][]T) (π₂ ρ [ ⌜ idV ⌝V ]t))
                    , coe (TmΓ= (mΠᴹ.pcoe1 ⟦A⟧ ⟦B⟧))
                          (app (coe (TmΓ= (mΠᴹ.pcoe2 ⟦A⟧ ⟦B⟧)) (t [ π₁ ρ ]t [ ⌜ idV ⌝V ]t))
                            [ < coe (ap (Tm Ψ) [][]T) (π₂ ρ [ ⌜ idV ⌝V ]t) > ]t)
                    , ( coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F idV $ proj₁ α)
                      , coe ($F= ⟦A⟧ (projTbase ⟦Γ⟧)) (coe ($F= ⟦A⟧ (pcoe$S₁ α)) (proj₂ α))))
                    ((TMt (app t) ^S (⟦Γ⟧ ,Cᴹ ⟦A⟧)) $n (ρ , α))
      pcoe$S₂ {Ψ}{ρ}{α , a}
      
        = Tbase= (⟦Γ⟧ ,Cᴹ ⟦A⟧)
                 (,s≃' (ap (_∘_ _) ⌜idV⌝ ◾ idr) (uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ [⌜idV⌝]t) ◾ πη)
                 ( uncoe (TmΓ= (mΠᴹ.pcoe1 ⟦A⟧ ⟦B⟧)) ⁻¹̃
                 ◾̃ []t≃' refl
                         q
                         ([]T≃ q (^≃ (ap (_∘_ _) ⌜idV⌝ ◾ idr)))
                         (app≃ refl
                               (to≃ p)
                               ([]T≃ q (^≃ (ap (_∘_ _) ⌜idV⌝ ◾ idr)))
                               ( uncoe (TmΓ= (mΠᴹ.pcoe2 ⟦A⟧ ⟦B⟧)) ⁻¹̃
                               ◾̃ [⌜idV⌝]t
                               ◾̃ uncoe (TmΓ= Π[])))
                         (,s≃ refl
                              refl
                              r̃
                              (to≃ p)
                              ( uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃
                              ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                              ◾̃ [⌜idV⌝]t
                              ◾̃ uncoe (TmΓ= ([id]T ⁻¹))))
                 ◾̃ app[,] ⁻¹̃
                 ◾̃ []t≃'' πη)
                 (,≃ (funext≃ ($F= ⟦Γ⟧ (π₁β ◾ (ap (_∘_ _) (⌜idV⌝) ◾ idr)))
                              (λ x₂ → to≃ ($F= ⟦A⟧ (Tbase= ⟦Γ⟧
                                                           (π₁β ◾ (ap (_∘_ _) (⌜idV⌝) ◾ idr))
                                                           (π₂β' ◾̃ (uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ [⌜idV⌝]t))
                                                           x₂))))
                     (uncoe ($F= ⟦Γ⟧ (π₁β ⁻¹)) ⁻¹̃ ◾̃ idF' ⟦Γ⟧)
                     (uncoe ($F= ⟦A⟧ (projTbase ⟦Γ⟧)) ⁻¹̃ ◾̃ uncoe ($F= ⟦A⟧ (pcoe$S₁ (α , a))) ⁻¹̃))
        where
          p : A [ π₁ ρ ∘ ⌜ idV ⌝V ]T ≡ A [ π₁ ρ ]T
          p = ap (_[_]T A) (ap (_∘_ _) ⌜idV⌝ ◾ idr)
          
          q : _≡_ {A = Con} (Ψ , A [ π₁ ρ ∘ ⌜ idV ⌝V ]T) (Ψ , A [ π₁ ρ ]T)
          q = ,C≃ refl (to≃ p)
          
      
    p$S : ∀{Ψ}(α : (TMC (Γ , A) ,P (⟦Γ⟧ ,Cᴹ ⟦A⟧)) $P Ψ)
        → ⟦B⟧ [ TMt (app t) ^S (⟦Γ⟧ ,Cᴹ ⟦A⟧) ]F $F α
    p$S {Ψ}(ρ , α) = coe ($F= ⟦B⟧ pcoe$S₂)
                         (map (⟦t⟧ $S (π₁ ρ , proj₁ α))
                              idV
                              (coe ($F= ⟦A⟧ (pcoe$S₁ α)) (proj₂ α)))

    appᴹ : Tmᴹ (⟦Γ⟧ ,Cᴹ ⟦A⟧) ⟦B⟧ (app t)
    appᴹ = record
      { _$S_ = p$S
      ; natS = cheat
      }
