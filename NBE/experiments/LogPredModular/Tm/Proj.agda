{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Tm.Proj
  (U̅ : FamPSh TMU) -- U^--
  (E̅l : FamPSh (TMU ,P TMEl ,P U̅ [ wkn ]F)) -- E^--l
  where

open import lib
open import JM.JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty U̅ E̅l
open import NBE.LogPred.Tms U̅ E̅l
open import NBE.LogPred.Ty.Subst
open import NBE.LogPred.Tm.Subst

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms

proj₂,Cᴹ≃ : {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}{A : Ty Γ}(⟦A⟧ : Tyᴹ ⟦Γ⟧ A)
            {Ψ : Con}{ρ₀ ρ₁ : TMC (Γ , A) $P Ψ}(ρ₂ : ρ₀ ≡ ρ₁)
            {α₀ : (⟦Γ⟧ ,Cᴹ ⟦A⟧) $F ρ₀}{α₁ : (⟦Γ⟧ ,Cᴹ ⟦A⟧) $F ρ₁}(α₂ : α₀ ≃ α₁)
          → proj₂ α₀ ≃ proj₂ α₁
proj₂,Cᴹ≃ ⟦A⟧ refl (refl , refl) = refl , refl

proj₂,Cᴹcoe : {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}{A : Ty Γ}(⟦A⟧ : Tyᴹ ⟦Γ⟧ A)
              {Ψ : Con}{ρ₀ ρ₁ : TMC (Γ , A) $P Ψ}(ρ₂ : ρ₀ ≡ ρ₁)
              {α₀ : (⟦Γ⟧ ,Cᴹ ⟦A⟧) $F ρ₀}
            → proj₂ (coe (ap (_$F_ (⟦Γ⟧ ,Cᴹ ⟦A⟧)) ρ₂) α₀) ≃ proj₂ α₀
proj₂,Cᴹcoe ⟦A⟧ refl = refl , refl

module mπ₂ᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {Δ : Con}{⟦Δ⟧ : Conᴹ Δ}
  {A : Ty Δ}{⟦A⟧ : Tyᴹ ⟦Δ⟧ A}
  {σ : Tms Γ (Δ , A)}(⟦σ⟧ : Tmsᴹ ⟦Γ⟧ (⟦Δ⟧ ,Cᴹ ⟦A⟧) σ)
  where

    abstract
      pcoe$S : ∀{Ψ}{α : (TMC Γ ,P ⟦Γ⟧) $P Ψ}
             → _≡_ {A = (TMC Δ ,P TMT A ,P ⟦Δ⟧ [ wkn ]F) $P Ψ}
                   ( π₁ (TMs σ $n (wkn {A = ⟦Γ⟧} $n α))
                   , π₂ (TMs σ $n (wkn {A = ⟦Γ⟧} $n α))
                   , proj₁ (⟦σ⟧ $S α))
                   ( π₁ σ ∘ proj₁ (proj₁ ((TMt (π₂ σ) ^S ⟦Γ⟧) $n α))
                   , coe (TmΓ= [][]T) (proj₂ (proj₁ ((TMt (π₂ σ) ^S ⟦Γ⟧) $n α)))
                   , π₁ᴹ ⟦σ⟧ $S (proj₁ (proj₁ ((TMt (π₂ σ) ^S ⟦Γ⟧) $n α)) , proj₂ ((TMt (π₂ σ) ^S ⟦Γ⟧) $n α)))
      pcoe$S {Ψ}{α}
      
        = Tbase= ⟦Δ⟧
                 (π₁∘ ⁻¹)
                 (π₂[]' ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T))
                 (uncoe (ap (_$F_ ⟦Δ⟧) (π₁∘ ⁻¹)))

    p$S : ∀{Ψ}(α : (TMC Γ ,P ⟦Γ⟧) $P Ψ)
        → ⟦A⟧ [ π₁ᴹ ⟦σ⟧ ]Tᴹ [ TMt (π₂ σ) ^S ⟦Γ⟧ ]F $F α
    p$S α = coe ($F= ⟦A⟧ pcoe$S) (proj₂ (⟦σ⟧ $S α))

    abstract
      pnatS : ∀{Ψ Ω}{f : Vars Ω Ψ}{α : (TMC Γ ,P ⟦Γ⟧) $P Ψ}
            → ⟦A⟧ [ π₁ᴹ ⟦σ⟧ ]Tᴹ [ TMt (π₂ σ) ^S ⟦Γ⟧ ]F $F f $ p$S α
            ≡ p$S ((TMC Γ ,P ⟦Γ⟧) $P f $ α)
      pnatS {Ψ}{Ω}{f}{ρ , α}
      
        = from≃ ( uncoe (ap (_$F_ (⟦A⟧ [ π₁ᴹ ⟦σ⟧ ]Tᴹ)) (natn (TMt (π₂ σ) ^S ⟦Γ⟧))) ⁻¹̃
                ◾̃ uncoe ($F= ⟦A⟧ (m[]Tᴹ.$F$coe ⟦A⟧ (π₁ᴹ ⟦σ⟧))) ⁻¹̃
                ◾̃ $Ff$≃ ⟦A⟧ (Tbase= ⟦Δ⟧
                                    π₁∘
                                    (uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ π₂[]')
                                    (uncoe ($F= ⟦Δ⟧ (π₁∘ ⁻¹)) ⁻¹̃))
                            (uncoe ($F= ⟦A⟧ pcoe$S) ⁻¹̃)
                ◾̃ uncoe ($F= ⟦A⟧ (m,Cᴹ.pcoe ⟦Δ⟧ ⟦A⟧ (⟦σ⟧ $S (ρ , α))))
                ◾̃ proj₂,Cᴹcoe ⟦A⟧ ass ⁻¹̃
                ◾̃ proj₂,Cᴹ≃ ⟦A⟧ refl (to≃ (natS ⟦σ⟧))
                ◾̃ uncoe ($F= ⟦A⟧ pcoe$S))
        
    π₂ᴹ : Tmᴹ ⟦Γ⟧ (⟦A⟧ [ π₁ᴹ ⟦σ⟧ ]Tᴹ) (π₂ σ)
    π₂ᴹ = record
      { _$S_ = p$S
      ; natS = pnatS
      }
