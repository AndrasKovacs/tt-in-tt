{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.HTms
  (U̅ : FamPSh TMU) -- U^--
  (E̅l : FamPSh (TMU ,P TMEl ,P U̅ [ wkn ]F)) -- E^--l
  where

open import lib
open import JM.JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty U̅ E̅l
open import NBE.LogPred.Tms U̅ E̅l
open import NBE.LogPred.Tm U̅ E̅l
open import NBE.LogPred.HTy U̅ E̅l

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms

open import NBE.Cheat

mHTms : MethodsHTms M mCon mTy mTms mTm mHTy
mHTms = cheat
