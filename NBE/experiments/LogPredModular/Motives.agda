{-# OPTIONS --no-eta #-}

module NBE.LogPredModular.Motives
  
  where

open import lib
open import JM.JM
open import TT.Syntax
open import TT.Elim
open import NBE.TM
open import NBE.Renamings
open import Cats

M : Motives
M = record
  { Conᴹ = λ Γ → FamPSh (TMC Γ)
  ; Tyᴹ  = λ {Γ} ⟦Γ⟧ A → FamPSh (TMC Γ ,P TMT A ,P ⟦Γ⟧ [ wkn ]F)
  ; Tmsᴹ = λ {Δ}{Γ} ⟦Δ⟧ ⟦Γ⟧ ρ → (TMC Δ ,P ⟦Δ⟧) →S ⟦Γ⟧ [ TMs ρ ]F [ wkn ]F
  ; Tmᴹ  = λ {Γ} ⟦Γ⟧ {A} ⟦A⟧ t → (TMC Γ ,P ⟦Γ⟧) →S ⟦A⟧ [ TMt t ^S ⟦Γ⟧ ]F }

Tbase= : ∀{Γ Ψ A}(⟦Γ⟧ : FamPSh (TMC Γ))
         {ρ₀ ρ₁ : TMC Γ $P Ψ}(ρ₂ : ρ₀ ≡ ρ₁)
         {t₀ : TMT A $F ρ₀}{t₁ : TMT A $F ρ₁}(t₂ : t₀ ≃ t₁)
         {v₀ : ⟦Γ⟧ $F ρ₀}{v₁ : ⟦Γ⟧ $F ρ₁}(v₂ : v₀ ≃ v₁)
       → _≡_ {A = (TMC Γ ,P TMT A ,P ⟦Γ⟧ [ wkn ]F) $P Ψ} (ρ₀ , t₀ , v₀) (ρ₁ , t₁ , v₁)
Tbase= ⟦Γ⟧ refl (refl , refl) (refl , refl) = refl
