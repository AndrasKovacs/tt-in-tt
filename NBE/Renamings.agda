{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings where

-- the category of renamings

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import Cats

open import NBE.Renamings.Var public
open import NBE.Renamings.Vars public
open import NBE.Renamings.Proj public
open import NBE.Renamings.Comp public
open import NBE.Renamings.Weaken public
open import NBE.Renamings.Id public
open import NBE.Renamings.Idl public
open import NBE.Renamings.VsuComp public
open import NBE.Renamings.IdComp public
open import NBE.Renamings.Idr public
open import NBE.Renamings.CompComp public
open import NBE.Renamings.Ass public
open import NBE.Renamings.Lift public

-- the category of renamings

REN : Cat
REN = record 
  { Obj  = Con
  ; _⇒_  = Vars
  ; idc  = idV
  ; _∘c_ = _∘V_
  ; idl  = idlV
  ; idr  = idrV
  ; ass  = assV
  }
