{-# OPTIONS --no-eta #-}

module NBE.Nf.Nfs where

open import lib
open import JM
open import Cats
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.Nf.Nf
open import NBE.Nf.Rename
open import NBE.Nf.IdComp
open import NBE.Nf.CompComp
open import NBE.TM

-- lists of normal forms

data Nfs : Con → Con → Set
⌜_⌝nfs : ∀{Γ Δ} → Nfs Γ Δ → Tms Γ Δ

data Nfs where
  εnfs   : ∀{Γ} → Nfs Γ •
  _,nfs_ : ∀{Γ Δ}(ρ : Nfs Γ Δ){A : Ty Δ} → Nf Γ (A [ ⌜ ρ ⌝nfs ]T) → Nfs Γ (Δ , A)

⌜ εnfs ⌝nfs = ε
⌜ ρ ,nfs n ⌝nfs = ⌜ ρ ⌝nfs ,s ⌜ n ⌝nf


-- eliminator

Nfs-elim : ∀{i}(Nfsᴹ : (Γ Δ : Con) → Nfs Γ Δ → Set i)
           (εnfsᴹ : ∀{Γ} → Nfsᴹ Γ • εnfs)
           (_,nfsᴹ_ : ∀{Γ Δ}{ρ : Nfs Γ Δ}(ρᴹ : Nfsᴹ Γ Δ ρ)
                      {A : Ty Δ}(n : Nf Γ (A [ ⌜ ρ ⌝nfs ]T)) → Nfsᴹ Γ (Δ , A) (ρ ,nfs n))
           {Γ Δ : Con}(ρ : Nfs Γ Δ) → Nfsᴹ Γ Δ ρ
Nfs-elim Nfsᴹ εnfsᴹ _,nfsᴹ_ εnfs = εnfsᴹ
Nfs-elim Nfsᴹ εnfsᴹ _,nfsᴹ_ (ρ ,nfs n) = Nfs-elim Nfsᴹ εnfsᴹ _,nfsᴹ_ ρ ,nfsᴹ n

-- congruence rules

Nfs= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁) → Nfs Γ₀ Δ₀ ≡ Nfs Γ₁ Δ₁
Nfs= refl refl = refl

,nfs= : ∀{Γ Δ}{ρ₀ ρ₁ : Nfs Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁){A : Ty Δ}
        {n₀ : Nf Γ (A [ ⌜ ρ₀ ⌝nfs ]T)}{n₁ : Nf Γ (A [ ⌜ ρ₁ ⌝nfs ]T)}
        (n₂ : n₀ ≃ n₁)
      → ρ₀ ,nfs n₀ ≡ ρ₁ ,nfs n₁
,nfs= refl (refl ,≃ refl) = refl

-- renaming

_[_]nfs : ∀{Γ Δ Θ} → Nfs Δ Γ → Vars Θ Δ → Nfs Θ Γ
⌜[]nfs⌝ : ∀{Γ Δ Θ}{ρ : Nfs Δ Γ}{β : Vars Θ Δ} → ⌜ ρ ⌝nfs ∘ ⌜ β ⌝V ≡ ⌜ ρ [ β ]nfs ⌝nfs

infixl 8 _[_]nfs

εnfs [ β ]nfs = εnfs
(ρ ,nfs n) [ β ]nfs = (ρ [ β ]nfs) ,nfs coe (NfΓ= ([][]T ◾ ap (_[_]T _) (⌜[]nfs⌝ {ρ = ρ}))) (n [ β ]nf)

abstract
  ⌜[]nfs⌝ {ρ = εnfs} = εη
  ⌜[]nfs⌝ {ρ = ρ ,nfs n}{β} = ,∘
                            ◾ ,s≃' (⌜[]nfs⌝ {ρ = ρ})
                                   ( uncoe (TmΓ= [][]T) ⁻¹̃
                                   ◾̃ to≃ (⌜[]nf⌝ {v = n})
                                   ◾̃ ⌜coe⌝nf ([][]T ◾ ap (_[_]T _) (⌜[]nfs⌝ {ρ = ρ})) ⁻¹̃)

-- categorical laws

abstract
  [id]nfs : ∀{Γ Δ}{ρ : Nfs Γ Δ} → ρ [ idV ]nfs ≡ ρ
  [id]nfs {ρ = εnfs} = refl
  [id]nfs {ρ = ρ ,nfs n} = ,nfs= ([id]nfs {ρ = ρ})
                                 ( uncoe (NfΓ= ([][]T ◾ ap (_[_]T _) (⌜[]nfs⌝ {ρ = ρ}))) ⁻¹̃
                                 ◾̃ [id]nf)

abstract
  [][]nfs : ∀{Γ Δ Θ Σ}{ρ : Nfs Θ Σ}{β : Vars Δ Θ}{γ : Vars Γ Δ} → ρ [ β ∘V γ ]nfs ≡ ρ [ β ]nfs [ γ ]nfs
  [][]nfs {ρ = εnfs} = refl
  [][]nfs {ρ = ρ ,nfs n}{β} = ,nfs= ([][]nfs {ρ = ρ})
                                 ( uncoe (ap (Nf _) ([][]T ◾ ap (_[_]T _) (⌜[]nfs⌝ {ρ = ρ}))) ⁻¹̃
                                 ◾̃ [][]nf {v = n} ⁻¹̃
                                 ◾̃ coe[]nf' ([][]T ◾ ap (_[_]T _) (⌜[]nfs⌝ {ρ = ρ})) ⁻¹̃
                                 ◾̃ uncoe (NfΓ= ([][]T ◾ ap (_[_]T _) (⌜[]nfs⌝ {ρ = ρ [ β ]nfs}))))

-- categorical definitions

open Cat (REN ᵒᵖ)

NFC : (Γ : Con) → PSh (REN ᵒᵖ)
NFC Γ = record
  { _$P_   = λ Δ → Nfs Δ Γ
  ; _$P_$_ = λ β ρ → ρ [ β ]nfs
  ; idP    = [id]nfs
  ; compP  = [][]nfs }

abstract
  NFTidF : ∀{Γ}{A : Ty Γ}{I : Obj}{α : TMC Γ $P I}
           {n : Nf I (A [ α ]T)}
         → coe (NfΓ= [][]T) (n [ idV ]nf)
         ≡[ ap (λ ρ → Nf I (A [ ρ ]T)) (idP (TMC Γ)) ]≡
           n
  NFTidF {Γ}{A}
     = from≃ ( uncoe (ap (λ ρ → Nf _ (A [ ρ ]T)) (idP (TMC Γ))) ⁻¹̃
             ◾̃ uncoe (NfΓ= [][]T) ⁻¹̃
             ◾̃ [id]nf)
abstract
  NFTcompF : ∀{Γ}{A : Ty Γ}{I J K : Obj}{f : I ⇒ J}{g : J ⇒ K}
             {α : TMC Γ $P I}{n : Nf I (A [ α ]T)}
           → coe (NfΓ= [][]T) (n [ g ∘c f ]nf)
           ≡[ ap (λ ρ → Nf K (A [ ρ ]T)) (compP (TMC Γ) {f = f}{g}) ]≡
             coe (NfΓ= [][]T) (coe (NfΓ= [][]T) (n [ f ]nf) [ g ]nf)
  NFTcompF {Γ}{A}{n = n}
    = from≃ ( uncoe (ap (λ ρ → Nf _ (A [ ρ ]T)) (compP (TMC Γ))) ⁻¹̃
            ◾̃ uncoe (NfΓ= [][]T) ⁻¹̃
            ◾̃ [][]nf {v = n} ⁻¹̃ 
            ◾̃ coe[]nf' [][]T ⁻¹̃
            ◾̃ uncoe (NfΓ= [][]T))

NFT : ∀{Γ}(A : Ty Γ) → FamPSh (TMC Γ)
NFT {Γ} A = record

  { _$F_   = λ {Δ} ρ → Nf Δ (A [ ρ ]T)
  
  ; _$F_$_ = λ β n → coe (NfΓ= [][]T) (n [ β ]nf)
                         
  ; idF    = NFTidF

  ; compF  = λ {_}{_}{_}{_}{_}{_}{n} →  NFTcompF {n = n}

  }

-- embeddings of NF into TM

⌜NF⌝C : ∀{Γ} → NFC Γ →n TMC Γ
⌜NF⌝C {Γ} = record { _$n_ = ⌜_⌝nfs ; natn = λ {I}{J}{f}{α} → ⌜[]nfs⌝ {_}{_}{_}{α}{f} }

⌜NF⌝T : ∀{Γ}{A : Ty Γ} → NFT A →N TMT A
⌜NF⌝T {Γ}{A} = record
  { _$N_ = ⌜_⌝nf
  ; natN = λ {_}{_}{_}{_}{n}
           → from≃ (uncoe (TmΓ= [][]T) ⁻¹̃
                   ◾̃ to≃ (⌜[]nf⌝ {v = n})
                   ◾̃ ⌜coe⌝nf [][]T ⁻¹̃)
  }
