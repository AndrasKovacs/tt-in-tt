module NBE.Nf.NeNfElim where

open import Agda.Primitive

open import TT.Syntax
open import NBE.Renamings
open import NBE.Nf.Nf

record MNeNf {i} : Set (lsuc i) where
  field
    Neᴹ : ∀ Γ A → Ne Γ A → Set i
    Nfᴹ : ∀ Γ A → Nf Γ A → Set i

    varᴹ : ∀{Γ A}(x : Var Γ A) → Neᴹ Γ A (var x)
    appNeᴹ : ∀{Γ A B}{n : Ne Γ (Π A B)}(nᴹ : Neᴹ Γ (Π A B) n)
             {v : Nf Γ A}(vᴹ : Nfᴹ Γ A v)
           → Neᴹ Γ (B [ < ⌜ v ⌝nf > ]T) (appNe n v)
    neuUᴹ : ∀{Γ}{n : Ne Γ U}(nᴹ : Neᴹ Γ U n) → Nfᴹ Γ U (neuU n)
    neuElᴹ : ∀{Γ Â}{n : Ne Γ (El Â)}(nᴹ : Neᴹ Γ (El Â) n) → Nfᴹ Γ (El Â) (neuEl n)
    lamNfᴹ : ∀{Γ A B}{v : Nf (Γ , A) B}(vᴹ : Nfᴹ (Γ , A) B v) → Nfᴹ Γ (Π A B) (lamNf v)

module _ {i}(M : MNeNf {i}) where
  open MNeNf M
  
  Ne-elim : ∀{Γ A}(n : Ne Γ A) → Neᴹ Γ A n
  Nf-elim : ∀{Γ A}(v : Nf Γ A) → Nfᴹ Γ A v

  Ne-elim (var x) = varᴹ x
  Ne-elim (appNe n v) = appNeᴹ (Ne-elim n) (Nf-elim v)

  Nf-elim (neuU n) = neuUᴹ (Ne-elim n)
  Nf-elim (neuEl n) = neuElᴹ (Ne-elim n)
  Nf-elim (lamNf v) = lamNfᴹ (Nf-elim v)
