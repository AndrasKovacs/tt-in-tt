{-# OPTIONS --no-eta #-}

module NBE.Nf.Nes where

open import lib
open import JM
open import Cats
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.ConElim
open import NBE.Renamings
open import NBE.Nf.Nf
open import NBE.Nf.Rename
open import NBE.Nf.IdComp
open import NBE.Nf.CompComp
open import NBE.TM

-- lists of neutral terms

data Nes : Con → Con → Set
⌜_⌝nes : ∀{Γ Δ} → Nes Γ Δ → Tms Γ Δ

data Nes where
  εnes   : ∀{Γ} → Nes Γ •
  _,nes_ : ∀{Γ Δ}(ρ : Nes Γ Δ){A : Ty Δ} → Ne Γ (A [ ⌜ ρ ⌝nes ]T) → Nes Γ (Δ , A)

⌜ εnes ⌝nes = ε
⌜ ρ ,nes n ⌝nes = ⌜ ρ ⌝nes ,s ⌜ n ⌝ne

-- congruence rules

Nes= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁) → Nes Γ₀ Δ₀ ≡ Nes Γ₁ Δ₁
Nes= refl refl = refl

,nes= : ∀{Γ Δ}{ρ₀ ρ₁ : Nes Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁){A : Ty Δ}
        {n₀ : Ne Γ (A [ ⌜ ρ₀ ⌝nes ]T)}{n₁ : Ne Γ (A [ ⌜ ρ₁ ⌝nes ]T)}
        (n₂ : n₀ ≃ n₁)
      → ρ₀ ,nes n₀ ≡ ρ₁ ,nes n₁
,nes= refl (refl ,≃ refl) = refl

-- eliminators

Nes-elim : ∀{i}(Nesᴹ : (Γ Δ : Con) → Nes Γ Δ → Set i)
           (εnesᴹ : ∀{Γ} → Nesᴹ Γ • εnes)
           (_,nesᴹ_ : ∀{Γ Δ}{ρ : Nes Γ Δ}(ρᴹ : Nesᴹ Γ Δ ρ)
                      {A : Ty Δ}(n : Ne Γ (A [ ⌜ ρ ⌝nes ]T)) → Nesᴹ Γ (Δ , A) (ρ ,nes n))
           {Γ Δ : Con}(ρ : Nes Γ Δ) → Nesᴹ Γ Δ ρ
Nes-elim Nesᴹ εnesᴹ _,nesᴹ_ εnes = εnesᴹ
Nes-elim Nesᴹ εnesᴹ _,nesᴹ_ (ρ ,nes n) = Nes-elim Nesᴹ εnesᴹ _,nesᴹ_ ρ ,nesᴹ n

NesElim, : ∀{i Γ Δ A}(P : (ρ : Nes Γ (Δ , A)) → Set i)
         → ((τ : Nes Γ Δ)(n : Ne Γ (A [ ⌜ τ ⌝nes ]T)) → P (τ ,nes n))
         → (ρ : Nes Γ (Δ , A)) → P ρ
NesElim, {_}{Γ}{Δ}{A} P p ρ
  = Nes-elim
       (λ Γ' Δ' ρ' → (pΓ : Γ ≡ Γ')(pΔA : (Δ , A) ≡ Δ')(pρ : ρ ≡[ Nes= pΓ pΔA ]≡ ρ') → P ρ)
       (λ _ p → ⊥-elim (disj•, (p ⁻¹)))
       (λ {Γ'}{Δ'}{τ} _ {A'} n pΓ pΔA pρ
        → f ρ τ n pΓ (inj,₀ pΔA) (inj,₁ pΔA) (UIP' (Nes= pΓ pΔA) (Nes= pΓ (,C= (inj,₀ pΔA) (inj,₁ pΔA))) pρ))
       ρ refl refl refl
  where
    f : (ρ' : Nes Γ (Δ , A))
        {Γ' : Con}{Δ' : Con}(τ : Nes Γ' Δ')
        {A' : Ty Δ'}(n : Ne Γ' (A' [ ⌜ τ ⌝nes ]T))
        (pΓ : Γ ≡ Γ')
        (pΔ : Δ ≡ Δ')
        (pA : A ≡[ Ty= pΔ ]≡ A')
        (pρ : ρ' ≡[ Nes= pΓ (,C= pΔ pA) ]≡ (τ ,nes n))
      → P ρ'
    f _ τ n refl refl refl refl = p τ n

-- renaming

_[_]nes : ∀{Γ Δ Θ} → Nes Δ Γ → Vars Θ Δ → Nes Θ Γ
⌜[]nes⌝ : ∀{Γ Δ Θ}{ρ : Nes Δ Γ}{β : Vars Θ Δ} → ⌜ ρ ⌝nes ∘ ⌜ β ⌝V ≡ ⌜ ρ [ β ]nes ⌝nes

infixl 8 _[_]nes

εnes [ β ]nes = εnes
(ρ ,nes n) [ β ]nes = (ρ [ β ]nes) ,nes coe (NeΓ= ([][]T ◾ ap (_[_]T _) (⌜[]nes⌝ {ρ = ρ}))) (n [ β ]ne)

abstract
  ⌜[]nes⌝ {ρ = εnes} = εη
  ⌜[]nes⌝ {ρ = ρ ,nes n}{β} = ,∘
                            ◾ ,s≃' (⌜[]nes⌝ {ρ = ρ})
                                   ( uncoe (TmΓ= [][]T) ⁻¹̃
                                   ◾̃ to≃ (⌜[]ne⌝ {n = n})
                                   ◾̃ ⌜coe⌝ne ([][]T ◾ ap (_[_]T _) (⌜[]nes⌝ {ρ = ρ})) ⁻¹̃)

-- categorical laws

abstract
  [id]nes : ∀{Γ Δ}{ρ : Nes Γ Δ} → ρ [ idV ]nes ≡ ρ
  [id]nes {ρ = εnes} = refl
  [id]nes {ρ = ρ ,nes n} = ,nes= ([id]nes {ρ = ρ})
                                 ( uncoe (NeΓ= ([][]T ◾ ap (_[_]T _) (⌜[]nes⌝ {ρ = ρ}))) ⁻¹̃
                                 ◾̃ [id]ne)

abstract
  [][]nes : ∀{Γ Δ Θ Σ}{ρ : Nes Θ Σ}{β : Vars Δ Θ}{γ : Vars Γ Δ} → ρ [ β ∘V γ ]nes ≡ ρ [ β ]nes [ γ ]nes
  [][]nes {ρ = εnes} = refl
  [][]nes {ρ = ρ ,nes n}{β} = ,nes= ([][]nes {ρ = ρ})
                                 ( uncoe (ap (Ne _) ([][]T ◾ ap (_[_]T _) (⌜[]nes⌝ {ρ = ρ}))) ⁻¹̃
                                 ◾̃ [][]ne {n = n} ⁻¹̃
                                 ◾̃ coe[]ne' ([][]T ◾ ap (_[_]T _) (⌜[]nes⌝ {ρ = ρ})) ⁻¹̃
                                 ◾̃ uncoe (NeΓ= ([][]T ◾ ap (_[_]T _) (⌜[]nes⌝ {ρ = ρ [ β ]nes}))))

-- categorical definitions

open Cat (REN ᵒᵖ)

NEC : (Γ : Con) → PSh (REN ᵒᵖ)
NEC Γ = record
  { _$P_   = λ Δ → Nes Δ Γ
  ; _$P_$_ = λ β ρ → ρ [ β ]nes
  ; idP    = [id]nes
  ; compP  = [][]nes }

abstract
  NETidF : ∀{Γ}{A : Ty Γ}{I : Obj}{α : TMC Γ $P I}
           {n : Ne I (A [ α ]T)}
         → coe (NeΓ= [][]T) (n [ idV ]ne)
         ≡[ ap (λ ρ → Ne I (A [ ρ ]T)) (idP (TMC Γ)) ]≡
           n
  NETidF {Γ}{A}
     = from≃ ( uncoe (ap (λ ρ → Ne _ (A [ ρ ]T)) (idP (TMC Γ))) ⁻¹̃
             ◾̃ uncoe (NeΓ= [][]T) ⁻¹̃
             ◾̃ [id]ne)
abstract
  NETcompF : ∀{Γ}{A : Ty Γ}{I J K : Obj}{f : I ⇒ J}{g : J ⇒ K}
             {α : TMC Γ $P I}{n : Ne I (A [ α ]T)}
           → coe (NeΓ= [][]T) (n [ g ∘c f ]ne)
           ≡[ ap (λ ρ → Ne K (A [ ρ ]T)) (compP (TMC Γ) {f = f}{g}) ]≡
             coe (NeΓ= [][]T) (coe (NeΓ= [][]T) (n [ f ]ne) [ g ]ne)
  NETcompF {Γ}{A}{n = n}
    = from≃ ( uncoe (ap (λ ρ → Ne _ (A [ ρ ]T)) (compP (TMC Γ))) ⁻¹̃
            ◾̃ uncoe (NeΓ= [][]T) ⁻¹̃
            ◾̃ [][]ne {n = n} ⁻¹̃ 
            ◾̃ coe[]ne' [][]T ⁻¹̃
            ◾̃ uncoe (NeΓ= [][]T))

NET : ∀{Γ}(A : Ty Γ) → FamPSh (TMC Γ)
NET {Γ} A = record

  { _$F_   = λ {Δ} ρ → Ne Δ (A [ ρ ]T)
  
  ; _$F_$_ = λ β n → coe (NeΓ= [][]T) (n [ β ]ne)
                         
  ; idF    = NETidF

  ; compF  = λ {_}{_}{_}{_}{_}{_}{n} →  NETcompF {n = n}

  }

-- embeddings of NE, NF into TM

⌜NE⌝C : ∀{Γ} → NEC Γ →n TMC Γ
⌜NE⌝C {Γ} = record { _$n_ = ⌜_⌝nes ; natn = λ {I}{J}{f}{α} → ⌜[]nes⌝ {_}{_}{_}{α}{f} }

⌜NE⌝T : ∀{Γ}{A : Ty Γ} → NET A →N TMT A
⌜NE⌝T {Γ}{A} = record
  { _$N_ = ⌜_⌝ne
  ; natN = λ {_}{_}{_}{_}{n}
           → from≃ (uncoe (TmΓ= [][]T) ⁻¹̃
                   ◾̃ to≃ (⌜[]ne⌝ {n = n})
                   ◾̃ ⌜coe⌝ne [][]T ⁻¹̃)
  }
