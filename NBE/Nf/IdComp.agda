{-# OPTIONS --no-eta #-}

module NBE.Nf.IdComp where

open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.Nf.Nf
open import NBE.Nf.Rename

[id]ne : ∀{Γ A}{n : Ne Γ A} → n [ idV ]ne ≃ n
[id]nf : ∀{Γ A}{v : Nf Γ A} → v [ idV ]nf ≃ v

abstract
  [id]ne {n = var x} = var≃ [⌜idV⌝]T ([id]v ⁻¹̃)
  [id]ne {n = appNe n v} = uncoe (NeΓ= ([][]T ◾ ap (_[_]T _) _ ◾ [][]T ⁻¹)) ⁻¹̃
                         ◾̃ appNe≃ (ap (_[_]T _) ⌜idV⌝ ◾ [id]T)
                                  ( []T≃ (,C≃ refl (to≃ [⌜idV⌝]T))
                                        ( to≃ (⌜^⌝ ⁻¹)
                                        ◾̃ ⌜idV^⌝
                                        ◾̃ to≃ ⌜idV⌝)
                                  ◾̃ to≃ [id]T)
                                  (uncoe (NeΓ= Π[]) ⁻¹̃ ◾̃ [id]ne)
                                  [id]nf

abstract
  [id]nf {v = neuU n} = uncoe (NfΓ= (U[] ⁻¹)) ⁻¹̃
                      ◾̃ to≃ (neuU=' (from≃ (uncoe (NeΓ= U[]) ⁻¹̃ ◾̃ [id]ne)))
  [id]nf {v = neuEl n} = uncoe (NfΓ= (El[] ⁻¹)) ⁻¹̃
                       ◾̃ neuEl≃ (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                                       ◾̃ []t≃ refl (to≃ ⌜idV⌝)
                                       ◾̃ from≡ (TmΓ= [id]T) [id]t))
                                ( uncoe (NeΓ= El[]) ⁻¹̃
                                ◾̃ [id]ne)
  [id]nf {v = lamNf {Γ}{A} v} = uncoe (NfΓ= (Π≃' refl (to≃ (ap (_[_]T _) ⌜^⌝)) ◾ Π[] ⁻¹)) ⁻¹̃
                              ◾̃ lamNf≃ [⌜idV⌝]T
                                       ( []T≃ (,C≃ refl (to≃ [⌜idV⌝]T))
                                              ( ⌜⌝V≃ (,C≃ refl (to≃ [⌜idV⌝]T)) refl {idV ^V A}{idV} (idV^ {A = A})
                                              ◾̃ to≃ ⌜idV⌝)
                                       ◾̃ to≃ [id]T)
                                       ( []nf≃ (,C≃ refl (to≃ [⌜idV⌝]T)) {v = v} {idV ^V A}{idV} (idV^ {A = A})
                                       ◾̃ [id]nf)
