{-# OPTIONS --no-eta #-}

module NBE.Nf.Nf where

open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NBE.Renamings

-- neutral terms and normal forms together with embeddings into the syntax

data Ne : ∀ Γ → Ty Γ → Set
data Nf  : ∀ Γ → Ty Γ → Set

⌜_⌝ne : ∀{Γ A} → Ne Γ A → Tm Γ A
⌜_⌝nf : ∀{Γ A} → Nf Γ A → Tm Γ A

data Ne where
  var : ∀{Γ A}(x : Var Γ A) → Ne Γ A
  appNe : ∀{Γ A B}(f : Ne Γ (Π A B))(v : Nf Γ A) → Ne Γ (B [ < ⌜ v ⌝nf > ]T)

data Nf where
  neuU  : ∀{Γ}(n : Ne Γ U) → Nf Γ U
  neuEl : ∀{Γ Â}(n : Ne Γ (El Â)) → Nf Γ (El Â)
  lamNf : ∀{Γ A B}(v : Nf (Γ , A) B) → Nf Γ (Π A B)

⌜ var x ⌝ne = ⌜ x ⌝v
⌜ appNe n u ⌝ne = ⌜ n ⌝ne $ ⌜ u ⌝nf

⌜ neuU n ⌝nf = ⌜ n ⌝ne
⌜ neuEl n ⌝nf = ⌜ n ⌝ne
⌜ lamNf t ⌝nf = lam ⌜ t ⌝nf

-- congruence rules

Ne= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ ap Ty Γ₂ ]≡ A₁)
    → Ne Γ₀ A₀ ≡ Ne Γ₁ A₁
Ne= refl refl = refl

NeΓ= : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁) → Ne Γ A₀ ≡ Ne Γ A₁
NeΓ= {Γ} = ap (Ne Γ)

⌜coe⌝ne : ∀{Γ A₀ A₁}(A₂ : A₀ ≡ A₁){n : Ne Γ A₀}
        → ⌜ coe (NeΓ= A₂) n ⌝ne ≃ ⌜ n ⌝ne
⌜coe⌝ne refl = refl ,≃ refl

Nf= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ ap Ty Γ₂ ]≡ A₁)
    → Nf Γ₀ A₀ ≡ Nf Γ₁ A₁
Nf= refl refl = refl

NfΓ= : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁) → Nf Γ A₀ ≡ Nf Γ A₁
NfΓ= {Γ} = ap (Nf Γ)

⌜coe⌝nf : ∀{Γ A₀ A₁}(A₂ : A₀ ≡ A₁){v : Nf Γ A₀}
        → ⌜ coe (NfΓ= A₂) v ⌝nf ≃ ⌜ v ⌝nf
⌜coe⌝nf refl = refl ,≃ refl

neuU= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
        (U₂ : U ≡[ Ty= Γ₂ ]≡ U)
        {n₀ : Ne Γ₀ U}{n₁ : Ne Γ₁ U}(n₂ : n₀ ≡[ Ne= Γ₂ U₂ ]≡ n₁)
      → neuU n₀ ≡[ Nf= Γ₂ U₂ ]≡ neuU n₁
neuU= refl refl refl = refl

neuU=' : ∀{Γ}{n₀ n₁ : Ne Γ U}(n₂ : n₀ ≡ n₁) → neuU n₀ ≡ neuU n₁
neuU=' refl = refl

neuEl= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
         {Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}(Â₂ : Â₀ ≡[ Tm= Γ₂ (U= Γ₂) ]≡ Â₁)
         {n₀ : Ne Γ₀ (El Â₀)}{n₁ : Ne Γ₁ (El Â₁)}(n₂ : n₀ ≡[ Ne= Γ₂ (El= Γ₂ Â₂) ]≡ n₁)
       → neuEl n₀ ≡[ Nf= Γ₂ (El= Γ₂ Â₂) ]≡ neuEl n₁
neuEl= refl refl refl = refl

neuEl≃ : ∀{Γ Â₀ Â₁}(Â₂ : Â₀ ≡ Â₁){n₀ : Ne Γ (El Â₀)}{n₁ : Ne Γ (El Â₁)}(n₂ : n₀ ≃ n₁)
      → neuEl n₀ ≃ neuEl n₁
neuEl≃ refl (refl ,≃ refl) = refl ,≃ refl

var≃ : ∀{Γ A₀ A₁}(A₂ : A₀ ≡ A₁){x₀ : Var Γ A₀}{x₁ : Var Γ A₁}(x₂ : x₀ ≃ x₁)
     → var x₀ ≃ var x₁
var≃ refl (refl ,≃ refl) = refl ,≃ refl

lamNf≃ : ∀{Γ A₀ A₁}(A₂ : A₀ ≡ A₁)
         {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}(B₂ : B₀ ≃ B₁)
         {v₀ : Nf (Γ , A₀) B₀}{v₁ : Nf (Γ , A₁) B₁}(v₂ : v₀ ≃ v₁)
       → _≃_ {A = Nf Γ (Π A₀ B₀)}{B = Nf Γ (Π A₁ B₁)} (lamNf v₀) (lamNf v₁)
lamNf≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

appNe≃ : ∀{Γ A₀ A₁}(A₂ : A₀ ≡ A₁)
         {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}(B₂ : B₀ ≃ B₁)
         {n₀ : Ne Γ (Π A₀ B₀)}{n₁ : Ne Γ (Π A₁ B₁)}(n₂ : n₀ ≃ n₁)
         {v₀ : Nf Γ A₀}{v₁ : Nf Γ A₁}(v₂ : v₀ ≃ v₁)
       → _≃_ {A = Ne Γ (B₀ [ < ⌜ v₀ ⌝nf > ]T)}
             {B = Ne Γ (B₁ [ < ⌜ v₁ ⌝nf > ]T)}
             (appNe n₀ v₀)
             (appNe n₁ v₁)
appNe≃ refl (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

lamNf= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
         {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≡[ Ty= (,C= Γ₂ A₂) ]≡ B₁)
         {v₀ : Nf (Γ₀ , A₀) B₀}{v₁ : Nf (Γ₁ , A₁) B₁}(v₂ : v₀ ≡[ Nf= (,C= Γ₂ A₂) B₂ ]≡ v₁)
       → lamNf v₀ ≡[ Nf= Γ₂ (Π= Γ₂ A₂ B₂) ]≡ lamNf v₁
lamNf= refl refl refl refl = refl

var= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
       {x₀ : Var Γ₀ A₀}{x₁ : Var Γ₁ A₁}(x₂ : x₀ ≡[ Var= Γ₂ A₂ ]≡ x₁)
     → var x₀ ≡[ Ne= Γ₂ A₂ ]≡ var x₁
var= refl refl refl = refl

⌜⌝nf≃ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
        {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
        {v₀ : Nf Γ₀ A₀}{v₁ : Nf Γ₁ A₁}(v₂ : v₀ ≃ v₁)
      → ⌜ v₀ ⌝nf ≃ ⌜ v₁ ⌝nf
⌜⌝nf≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl
