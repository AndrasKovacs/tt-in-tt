{-# OPTIONS --no-eta --rewriting #-}

module NBE.Quote.Ty.Subst where

open import lib
open import JM
open import Cats
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Quote.Motives
open import NBE.Quote.Cxt
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.LogPred.LogPred U̅ E̅l

open Motives M
open MethodsCon mCon

open import NBE.Cheat

open import NBE.Glue

module m[]Tᴹ
  {Γ : Con}{Γᴹ : Conᴹ Γ}
  {Δ : Con}{Δᴹ : Conᴹ Δ}
  {A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A)
  {σ : Tms Γ Δ}(σᴹ : Tmsᴹ Γᴹ Δᴹ σ)
  where

    module qT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (s : TMT (A [ σ ]T) $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      (a : ⟦ A [ σ ]T ⟧T $F (ρ ,Σ s ,Σ α))
      where

        abstract
          pcoe : Σ (NFT A $F (TMs σ $n ρ)) (λ n → coe (TmΓ= [][]T) s ≡ ⌜ n ⌝nf)
               ≡ Σ (NFT (A [ σ ]T) $F ρ)   (λ n → s ≡ ⌜ n ⌝nf)
          pcoe = Σ≃ (NfΓ= ([][]T ⁻¹))
                    (funext≃ (NfΓ= ([][]T ⁻¹))
                             (λ x₂ → ≡≃ (TmΓ= ([][]T ⁻¹))
                                        (uncoe (TmΓ= [][]T) ⁻¹̃)
                                        (ap≃ {B = Nf _} ⌜_⌝nf ([][]T ⁻¹) x₂)))

        ret : ≡NFT (A [ σ ]T) $F (ρ ,Σ s)
        ret = coe pcoe (qT Aᴹ $S (TMs σ $n ρ ,Σ coe (TmΓ= [][]T) s ,Σ ⟦ σ ⟧s $S (ρ ,Σ α) ,Σ a))

    module uT$S
      {Ψ : Con}
      (ρ : TMC Γ $P Ψ)
      (n : NET (A [ σ ]T) $F ρ)
      (α : ⟦ Γ ⟧C $F ρ)
      where

        abstract
          pcoe : ⟦ A ⟧T $F (TMs σ $n ρ ,Σ ⌜ coe (NeΓ= [][]T) n ⌝ne ,Σ ⟦ σ ⟧s $S (ρ ,Σ α))
               ≡ ⟦ A ⟧T $F (TMs σ $n ρ ,Σ coe (TmΓ= [][]T) ⌜ n ⌝ne ,Σ ⟦ σ ⟧s $S (ρ ,Σ α))
          pcoe = $F= ⟦ A ⟧T (Tbase= ⟦ Δ ⟧C refl (⌜coe⌝ne [][]T ◾̃ uncoe (TmΓ= [][]T)) r̃)

        ret : ⟦ A [ σ ]T ⟧T $F (ρ ,Σ ⌜ n ⌝ne ,Σ α)
        ret = coe pcoe (uT Aᴹ $S (TMs σ $n ρ ,Σ coe (NeΓ= [][]T) n ,Σ ⟦ σ ⟧s $S (ρ ,Σ α)))

    _[_]Tᴹ : Tyᴹ Γᴹ (A [ σ ]T)
    _[_]Tᴹ = record
      { qT = record
          { _$S_ = λ { (ρ ,Σ s ,Σ α ,Σ a) → qT$S.ret ρ s α a }
          ; natS = cheat
          }
      ; uT =  record
          { _$S_ = λ { (ρ ,Σ n ,Σ α) → uT$S.ret ρ n α }
          ; natS = cheat
          }
      }
