{-# OPTIONS --no-eta --rewriting #-}

module NBE.Quote.Ty.Pi.Quote where

open import lib hiding (zero)
open import JM
open import Cats
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Quote.Motives
open import NBE.Quote.Cxt
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.LogPred.LogPred U̅ E̅l
import NBE.LogPred.Ty.Pi

open Motives M
open MethodsCon mCon

open import NBE.Cheat

open import NBE.Glue

module qT$S
  {Γ : Con}{Γᴹ : Conᴹ Γ}
  {A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A)
  {B : Ty (Γ , A)}(Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B)
  {Ψ : Con}
  (δ : TMC Γ $P Ψ)
  (s' : TMT (Π A B) $F δ)
  (γ : ⟦ Γ ⟧C $F δ)
  (f : ⟦ Π A B ⟧T $F (δ ,Σ s' ,Σ γ))
  where

  open import NBE.LogPred.Ty.Pi.Exp ⟦ B ⟧T
  open E f

  α'' : (TMC Γ ,P TMT (Π A B) ,P ⟦ Γ ⟧C [ wkn ]F) $P Ψ
  α'' = δ ,Σ s' ,Σ γ

  abstract
    lcoe1 : TMC Γ $P wkV idV $ δ ≡ π₁ (δ ^ A)
    lcoe1 = ap (_∘_ δ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹

  γ' : ⟦ Γ ⟧C $F π₁ (δ ^ A)
  γ' = coe ($F= ⟦ Γ ⟧C lcoe1) (⟦ Γ ⟧C $F wkV idV $ γ)

  abstract
    lcoe2 : A [ δ ]T [ wk ]T ≡ A [ π₁ (δ ^ A) ]T
    lcoe2 = [][]T ◾ ap (_[_]T A) (π₁β ⁻¹)

  a' : ⟦ A ⟧T $F (π₁ (δ ^ A) ,Σ ⌜ coe (NeΓ= lcoe2) (var vze) ⌝ne ,Σ γ')
  a' = uT Aᴹ $S (π₁ (δ ^ A) ,Σ coe (NeΓ= lcoe2) (var vze) ,Σ γ')

  abstract
    lcoe3 : _≡_ {A = (TMC Γ ,P TMT A ,P ⟦ Γ ⟧C [ wkn ]F) $P (Ψ , A [ δ ]T)}
                (π₁ (δ ^ A) ,Σ ⌜ coe (NeΓ= lcoe2) (var vze) ⌝ne ,Σ γ')
                (π₁ (δ ^ A) ,Σ π₂ (δ ^ A) ,Σ γ')
    lcoe3 = Tbase= ⟦ Γ ⟧C refl (⌜coe⌝ne lcoe2 ◾̃ uncoe (TmΓ= [][]T) ◾̃ π₂β' ⁻¹̃) r̃

  a : ⟦ A ⟧T $F (π₁ (δ ^ A) ,Σ π₂ (δ ^ A) ,Σ γ')
  a = coe ($F= ⟦ A ⟧T lcoe3) a'

  γ',a : ⟦ Γ , A ⟧C $F (δ ^ A)
  γ',a = γ' ,Σ a

  abstract
    lcoe4 : A [ δ ]T [ π₁ {A = A [ δ ]T} id ]T ≡ A [ δ ∘ ⌜ wkV idV ⌝V ]T
    lcoe4 = [][]T ◾ ap (_[_]T A) (ap (_∘_ δ) ⌜wkid⌝)
  
  zero : Tm (Ψ , A [ δ ]T) (A [ δ ∘ ⌜ wkV idV ⌝V ]T)
  zero = coe (TmΓ= lcoe4) (π₂ id)

  abstract
    lcoe5 : _≡_ {A = (TMC Γ ,P TMT A ,P ⟦ Γ ⟧C [ wkn ]F) $P (Ψ , A [ δ ]T)}
                (π₁ (δ ^ A) ,Σ π₂ (δ ^ A) ,Σ γ')(
                δ ∘ ⌜ wkV idV ⌝V ,Σ zero ,Σ ⟦ Γ ⟧C $F wkV idV $ γ)
    lcoe5 = Tbase= ⟦ Γ ⟧C (π₁β ◾ ap (_∘_ δ) ⌜wkid⌝)
                          (π₂β' ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ uncoe (TmΓ= lcoe4))
                          (uncoe ($F= ⟦ Γ ⟧C lcoe1) ⁻¹̃)

  a''' : ⟦ A ⟧T $F (δ ∘ ⌜ wkV idV ⌝V ,Σ zero ,Σ ⟦ Γ ⟧C $F wkV idV $ γ)
  a''' = coe ($F= ⟦ A ⟧T lcoe5) a

  b : ⟦ B ⟧T $F ( (δ ∘ ⌜ wkV idV ⌝V ,s zero)
                ,Σ coe (TmΓ= (pcoe1 α'')) (coe (TmΓ= (pcoe2 α'')) (s' [ ⌜ wkV idV ⌝V ]t) $$ zero)
                ,Σ ( coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F wkV idV $ γ)
                  ,Σ coe ($F= ⟦ A ⟧T (projTbase α'')) a'''))
  b = map {Ψ , A [ δ ]T}(wkV idV) a'''

  abstract
    lcoe6a : (coe (TmΓ= (pcoe2 α'')) (s' [ ⌜ wkV {A = A [ δ ]T} idV ⌝V ]t)) $$ zero
           ≃ (coe (TmΓ= Π[]) (coe (TmΓ= Π[]) s' [ ⌜ wkV {A = A [ δ ]T} idV ⌝V ]t)) $$ coe (TmΓ= ([][]T ⁻¹)) zero
    lcoe6a = []t≃' refl
                   (,C≃ refl (to≃ ([][]T ⁻¹)))
                   ([]T≃ (,C≃ refl (to≃ ([][]T ⁻¹))) ∘^ ◾̃ to≃ ([][]T ⁻¹))
                   (app≃ refl
                         (to≃ ([][]T ⁻¹))
                         ([]T≃ (,C≃ refl (to≃ ([][]T ⁻¹))) ∘^ ◾̃ to≃ ([][]T ⁻¹))
                         (uncoe (TmΓ= (pcoe2 α'')) ⁻¹̃ ◾̃ coe[]t' Π[] ⁻¹̃ ◾̃ uncoe (TmΓ= Π[])))
                   (,s≃ refl refl r̃ (to≃ ([][]T ⁻¹)) (uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= ([][]T ⁻¹)) ◾̃ uncoe (TmΓ= ([id]T ⁻¹))))

  abstract
    lcoe6b : (coe (TmΓ= Π[]) (coe (TmΓ= Π[]) s' [ ⌜ wkV {A = A [ δ ]T} idV ⌝V ]t)) $$ coe (TmΓ= ([][]T ⁻¹)) zero
           ≃ app (coe (TmΓ= Π[]) s') [ ⌜ wkV {A = A [ δ ]T} idV ⌝V ^ A [ δ ]T ]t [ < coe (TmΓ= ([][]T ⁻¹)) zero > ]t
    lcoe6b = []t≃' refl
                   refl
                   r̃
                   (to≃ app[])
                   r̃

  abstract
    lcoe6c : (⌜ wkV idV ⌝V ^ A [ δ ]T) ∘ < coe (TmΓ= ([][]T ⁻¹)) zero > ≃ id {Ψ , A [ δ ]T}
    lcoe6c = to≃ (^∘<> refl) ◾̃ ,s≃ refl refl (to≃ (⌜wkid⌝ ⁻¹)) r̃ (uncoe (TmΓ= ([][]T ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= lcoe4) ⁻¹̃) ◾̃ to≃ πη

  abstract
    lcoe6 : _≡_ {A = (TMC (Γ , A) ,P TMT B ,P ⟦ Γ , A ⟧C [ wkn ]F) $P (Ψ , A [ δ ]T)}
                ( (δ ∘ ⌜ wkV idV ⌝V ,s zero)
                ,Σ coe (TmΓ= (pcoe1 α'')) (coe (TmΓ= (pcoe2 α'')) (s' [ ⌜ wkV idV ⌝V ]t) $$ zero)
                ,Σ ( coe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) (⟦ Γ ⟧C $F wkV idV $ γ)
                  ,Σ coe ($F= ⟦ A ⟧T (projTbase α'')) a'''))
                (δ ^ A ,Σ app (coe (TmΓ= Π[]) s') ,Σ γ',a)
    lcoe6 = Tbase= ⟦ Γ , A ⟧C (,s≃' (ap (_∘_ δ) (⌜wkid⌝ ⁻¹)) (uncoe (TmΓ= lcoe4) ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T)))
                              (uncoe (TmΓ= (pcoe1 α'')) ⁻¹̃ ◾̃ lcoe6a ◾̃ lcoe6b ◾̃ [][]t' ◾̃ []t≃ refl lcoe6c ◾̃ [id]t')
                              (,Σ≃≃ (funext≃ ($F= ⟦ Γ ⟧C (π₁β ◾ ap (_∘_ δ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹))
                                           (λ x₂ → to≃ ($F= ⟦ A ⟧T (Tbase= ⟦ Γ ⟧C (π₁β ◾ ap (_∘_ δ) (⌜wkid⌝ ⁻¹) ◾ π₁β ⁻¹)
                                                                                  (π₂β' ◾̃ uncoe (TmΓ= lcoe4) ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T) ◾̃ π₂β' ⁻¹̃)
                                                                                  x₂))))
                                  (uncoe ($F= ⟦ Γ ⟧C (π₁β ⁻¹)) ⁻¹̃ ◾̃ uncoe ($F= ⟦ Γ ⟧C lcoe1))
                                  ( uncoe ($F= ⟦ A ⟧T (projTbase α'')) ⁻¹̃
                                  ◾̃ uncoe ($F= ⟦ A ⟧T lcoe5) ⁻¹̃))

  b' : ⟦ B ⟧T $F (δ ^ A ,Σ app (coe (TmΓ= Π[]) s') ,Σ γ',a)
  b' = coe ($F= ⟦ B ⟧T lcoe6) b

  p : ≡NFT B $F (δ ^ A ,Σ app (coe (TmΓ= Π[]) s'))
  p = qT Bᴹ $S (δ ^ A ,Σ app (coe (TmΓ= Π[]) s') ,Σ γ',a ,Σ b')

  abstract
    eq : s' ≡ ⌜ coe (NfΓ= (Π[] ⁻¹)) (lamNf (proj₁ p)) ⌝nf
    eq = from≃ ( uncoe (TmΓ= Π[])
               ◾̃ to≃ (Πη ⁻¹)
               ◾̃ to≃ (ap lam (proj₂ p))
               ◾̃ ⌜coe⌝nf (Π[] ⁻¹) ⁻¹̃)

  ret : ≡NFT (Π A B) $F (δ ,Σ s')
  ret = coe (NfΓ= (Π[] ⁻¹)) (lamNf (proj₁ p)) ,Σ eq
