module NBE.Quote.Motives where

open import lib
open import JM
open import Cats
open import TT.Syntax hiding (_$_)
open import TT.ElimOld
open import TT.Congr
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Glue

open import NBE.Cheat

U̅ : FamPSh TMU
U̅ = record
  { _$F_   = λ Â → ≡NFT U $F (id ,Σ coe (TmΓ= ([id]T ⁻¹)) Â)
  ; _$F_$_ = cheat
  ; idF    = cheat
  ; compF  = cheat
  }

E̅l : FamPSh (TMU ,P TMEl ,P U̅ [ wkn ]F)
E̅l = record
  { _$F_   = λ {(Â ,Σ t ,Σ p) → ≡NFT (El Â) $F (id ,Σ coe (TmΓ= ([id]T ⁻¹)) t) }
  ; _$F_$_ = cheat
  ; idF    = cheat
  ; compF  = cheat
  }

open import NBE.LogPred.LogPred U̅ E̅l

record QuoteConᴹ (Δ : Con) : Set where
  field
    qC : (TMC Δ ,P ⟦ Δ ⟧C) →S ≡NFC Δ [ wkn ]F
    uC : NEC Δ →S ⟦ Δ ⟧C [ ⌜NE⌝C ]F

open QuoteConᴹ public

record QuoteTyᴹ {Γ : Con}(Γᴹ : QuoteConᴹ Γ)(A : Ty Γ) : Set where
  field
    qT : (TMC Γ ,P TMT A ,P ⟦ Γ ⟧C [ wkn ]F ,P ⟦ A ⟧T) →S ≡NFT A [ wkn ]F [ wkn ]F
    uT : (TMC Γ ,P NET A ,P ⟦ Γ ⟧C [ wkn ]F) →S ⟦ A ⟧T [ id, ⌜NE⌝T ,id ]F

open QuoteTyᴹ public

M : Motives
M = record

  { Conᴹ = QuoteConᴹ
  ; Tyᴹ  = QuoteTyᴹ
  ; Tmsᴹ = λ _ _ _ → ⊤
  ; Tmᴹ  = λ _ _ _ → ⊤ }
