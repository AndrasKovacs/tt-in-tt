{-# OPTIONS --no-eta --rewriting #-}

module NBE.Quote.Cxt where

open import lib
open import JM
open import Cats
open import TT.Syntax
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import TT.ConElim
open import NBE.TM
open import NBE.Nf
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.Quote.Motives
open import NBE.LogPred.LogPred U̅ E̅l

open Motives M

open import NBE.Cheat

•ᴹ : Conᴹ •
•ᴹ = record
  { qC = record { _$S_ = λ _ → εnfs ,Σ εη ; natS = cheat }
  ; uC = record { _$S_ = λ _ → tt ; natS = refl }
  }

module m,Cᴹ
  {Δ : Con}(Δᴹ : Conᴹ Δ)
  {A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A)
  where

  _,Cᴹ_ : Conᴹ (Δ , A)
  _,Cᴹ_ = record
  
    { qC = record
    
        { _$S_ = λ { (ρ ,Σ α) → let
                                 (τ ,Σ p)  = qC Δᴹ $S (π₁ ρ ,Σ proj₁ α)
                                 (n ,Σ p') = qT Aᴹ $S (π₁ ρ ,Σ π₂ ρ ,Σ proj₁ α ,Σ proj₂ α)
                               in   (τ ,nfs coe (NfΓ= (ap (_[_]T A) p)) n)
                                  ,Σ (πη ⁻¹ ◾ ,s≃' p (to≃ p' ◾̃ ⌜coe⌝nf (ap (_[_]T A) p) ⁻¹̃))
                 }
        
        ; natS = cheat
        
        }

    ; uC = record
    
        { _$S_ = λ {Γ}
                 → NesElim,
                     (λ ρ → ⟦ Δ , A ⟧C [ ⌜NE⌝C ]F $F ρ)
                     (λ τ n → let α = uC Δᴹ $S τ
                               in     coe ($F= ⟦ Δ ⟧C (π₁β ⁻¹)) α
                                  ,Σ coe ($F= ⟦ A ⟧T (Tbase= ⟦ Δ ⟧C (π₁β ⁻¹) (π₂β' ⁻¹̃) (uncoe ($F= ⟦ Δ ⟧C (π₁β ⁻¹)))))
                                          (uT Aᴹ $S (⌜ τ ⌝nes ,Σ n ,Σ α)))

        ; natS = cheat
        
        }
    }

mCon : MethodsCon M
mCon = record { •ᴹ = •ᴹ ; _,Cᴹ_ = m,Cᴹ._,Cᴹ_ }
