{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.Idl where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.ConElim

open import NBE.Renamings.Var
open import NBE.Renamings.Vars
open import NBE.Renamings.Proj
open import NBE.Renamings.Comp
open import NBE.Renamings.Weaken
open import NBE.Renamings.Id

abstract
  idlV : ∀{Γ Δ}{β : Vars Γ Δ} → idV ∘V β ≡ β
  idlV {Γ}{Δ}{β}
    = ElimCon'
        (λ Δ → (β : Vars Γ Δ) → idV {Δ} ∘V β ≡ β)
        (λ β'
         → Vars-elim (record
             { Varsᴹ = λ Γ' Δ' β'' → (pΔ : • ≡ Δ') → εV ≡[ Vars= refl pΔ ]≡ β''
             ; εVᴹ   = λ { refl → refl }
             ; _,Vᴹ_ = λ _ _ p → ⊥-elim (disj•, p)
             }) β' refl)
        (λ { {Γ'}{A'} idlVrec β'
         → Vars-elim (record
             { Varsᴹ = λ Γ'' Δ'' β'' → idV ∘V β'' ≡ β''
             ; εVᴹ   = refl
             ; _,Vᴹ_ = λ {Γ''}{Δ''}{β''} p {A''} x
                       → from≃ (,V≃ refl refl (to≃ (wkVβ {β = idV} ◾ p)) r̃
                                    ( uncoe (VarΓ= ([][]T ◾ ap (_[_]T A'') (⌜∘V⌝ (wkV idV)))) ⁻¹̃
                                    ◾̃ coe[]v' (ap (_[_]T A'') (pidV, idV _ ⁻¹))
                                    ◾̃ uncoe (VarΓ= (ap (_[_]T A'') (⌜π₁v⌝ (β'' ,V x) ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹)) ⁻¹̃
                                    ◾̃ uncoe (Var= refl (πv.,Vᴹ.pA[β] (β'' ,V x) _ x refl refl )) ⁻¹̃))
             }) β' })
        Δ β
