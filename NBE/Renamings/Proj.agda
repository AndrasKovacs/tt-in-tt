{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.Proj where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.ConElim

open import NBE.Renamings.Var
open import NBE.Renamings.Vars

-- π₁ and π₂ for renamings

module πv
  {Γ Δ A}
  (γ : Vars Γ (Δ , A))
  where

  module ,Vᴹ
    {Γ' : Con}{Δ' : Con}{β : Vars Γ' Δ'}
    (βᴹ : Γ' ≡ Γ → Δ' ≡ Δ , A → Σ (Vars Γ Δ) (λ β' → Var Γ (A [ ⌜ β' ⌝V ]T)))
    {A' : Ty Δ'}(x : Var Γ' (A' [ ⌜ β ⌝V ]T))
    (pΓ : Γ' ≡ Γ)(pΔA : Δ' , A' ≡ Δ , A)
    where

    abstract
      pA[β] : A' [ ⌜ β ⌝V ]T ≡[ Ty= pΓ ]≡ A [ ⌜ coe (Vars= pΓ (inj,₀ pΔA)) β ⌝V ]T
      pA[β] = []T= pΓ
                   (inj,₀ pΔA)
                   (inj,₁ pΔA)
                   (⌜⌝V= pΓ
                         (inj,₀ pΔA)
                         {β}
                         {coe (Vars= pΓ (inj,₀ pΔA)) β}
                         refl)

    res : Σ (Vars Γ Δ) (λ β' → Var Γ (A [ ⌜ β' ⌝V ]T))
    res
      =   (coe (Vars= pΓ (inj,₀ pΔA)) β)
      ,Σ coe (Var= pΓ pA[β]) x


  res : Σ (Vars Γ Δ) λ β
     →   (Var Γ (A [ ⌜ β ⌝V ]T))
  res
    = Vars-elim
        (record
          { Varsᴹ = λ Γ' Δ' γ' → Γ' ≡ Γ → Δ' ≡ (Δ , A)
                    → Σ (Vars Γ Δ) λ β
                    →   (Var Γ (A [ ⌜ β ⌝V ]T))
          ; εVᴹ   = λ _ pΓ → ⊥-elim (disj•, pΓ)
          ; _,Vᴹ_ = λ {Γ'}{Δ'}{β} βᴹ {A'} x pΓ pΔA → ,Vᴹ.res {Γ'}{Δ'}{β} βᴹ {A'} x pΓ pΔA
          })
        {Γ}{Δ , A} γ refl refl


π₁v : ∀{Γ Δ A} → Vars Γ (Δ , A) → Vars Γ Δ
π₁v γ = proj₁ (πv.res γ)

-- congruences

π₁v≃ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
       {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
       {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
       {β₀ : Vars Γ₀ (Δ₀ , A₀)}{β₁ : Vars Γ₁ (Δ₁ , A₁)}(β₂ : β₀ ≃ β₁)
     → π₁v β₀ ≃ π₁v β₁
π₁v≃ refl refl (refl ,≃ refl)(refl ,≃ refl) = refl ,≃ refl

π₂v : ∀{Γ Δ A}(γ : Vars Γ (Δ , A)) → Var Γ (A [ ⌜ π₁v γ ⌝V ]T)
π₂v γ = proj₂ (πv.res γ)

π₂v≃ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
       {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
       {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
       {β₀ : Vars Γ₀ (Δ₀ , A₀)}{β₁ : Vars Γ₁ (Δ₁ , A₁)}(β₂ : β₀ ≃ β₁)
     → π₂v β₀ ≃ π₂v β₁
π₂v≃ refl refl (refl ,≃ refl)(refl ,≃ refl) = refl ,≃ refl

-- beta rules

abstract
  π₁vβ : ∀{Γ Δ A}{β : Vars Γ Δ}{x : Var Γ (A [ ⌜ β ⌝V ]T)} → π₁v (β ,V x) ≡ β
  π₁vβ {β = β}{x} = refl

abstract
  π₂vβ' : ∀{Γ Δ A}{β : Vars Γ Δ}{x : Var Γ (A [ ⌜ β ⌝V ]T)} → π₂v (β ,V x) ≃ x
  π₂vβ' {β = β}{x} = uncoe (Var= refl (πv.,Vᴹ.pA[β] (β ,V x) _ x refl refl)) ⁻¹̃

-- laws about embeddings

abstract
  ⌜πv⌝ : ∀{Γ Δ A}{γ : Vars Γ (Δ , A)}
       → Σ (⌜ π₁v γ ⌝V ≡ π₁ ⌜ γ ⌝V) λ p
       →   (⌜ π₂v γ ⌝v ≡[ TmΓ= ([]T= refl refl refl p) ]≡ π₂ ⌜ γ ⌝V)
  ⌜πv⌝ {Γ}{Δ}{A}{γ}
    = Vars-elim
        (record
          { Varsᴹ = λ Γ' Δ' γ'
                    → (pΓ : Γ' ≡ Γ)(pΔA : Δ' ≡ (Δ , A))
                    → γ' ≡[ Vars= pΓ pΔA ]≡ γ
                    → Σ (⌜ π₁v γ ⌝V ≡ π₁ ⌜ γ ⌝V) λ p
                    →   (⌜ π₂v γ ⌝v ≡[ TmΓ= ([]T= refl refl refl p) ]≡ π₂ ⌜ γ ⌝V)
          ; εVᴹ   = λ _ p → ⊥-elim (disj•, p)
          ; _,Vᴹ_ = λ {Γ'}{Δ'}{β} _ {A'} x pΓ pΔA pγ
                    → t1 x pΓ pΔA pγ ,Σ t2 x pΓ pΔA pγ
          })
        {Γ}{Δ , A} γ refl refl refl

    where
    
      module _
        {Γ' Δ' : Con}{β : Vars Γ' Δ'}{A' : Ty Δ'}
        (x : Var Γ' (A' [ ⌜ β ⌝V ]T))
        (pΓ : Γ' ≡ Γ)(pΔA : Δ' , A' ≡ Δ , A)
        (pγ : β ,V x ≡[ Vars= pΓ pΔA ]≡ γ)
        where
          abstract
            t1 : ⌜ π₁v γ ⌝V ≡ π₁ ⌜ γ ⌝V
            t1 = from≃ (⌜⌝V≃ (pΓ ⁻¹)
                             (inj,₀ pΔA ⁻¹)
                             {π₁v γ}{β}
                             (π₁v≃ (pΓ ⁻¹)
                                   (inj,₀ pΔA ⁻¹)
                                   (from≡ (Ty= (inj,₀ pΔA)) (inj,₁ pΔA) ⁻¹̃)
                                   {γ}{β ,V x} (from≡ (Vars= pΓ pΔA) pγ ⁻¹̃))
                       ◾̃ to≃ (π₁β ⁻¹)
                       ◾̃ uncoe (Tms= pΓ (inj,₀ pΔA)))
               ◾ π₁= pΓ (inj,₀ pΔA) (inj,₁ pΔA)
                        {⌜ β ,V x ⌝V}{⌜ γ ⌝V}
                        (⌜⌝V= pΓ
                              (,C= (inj,₀ pΔA) (inj,₁ pΔA))
                              {β ,V x}{γ}
                              (UIP' (Vars= pΓ pΔA)(Vars= pΓ (,C= (inj,₀ pΔA) (inj,₁ pΔA))) pγ))

          abstract
            t2 : ⌜ π₂v γ ⌝v ≡[ TmΓ= ([]T= refl refl refl t1) ]≡ π₂ ⌜ γ ⌝V
            t2 =  from≃ ( uncoe (TmΓ= ([]T= refl refl refl t1)) ⁻¹̃
                       ◾̃ ⌜⌝v≃ (pΓ ⁻¹)
                              ([]T≃' (pΓ ⁻¹)
                                     (inj,₀ pΔA ⁻¹)
                                     (from≡ (Ty= (inj,₀ pΔA)) (inj,₁ pΔA) ⁻¹̃)
                                     ( to≃ t1
                                     ◾̃ π₁≃ (pΓ ⁻¹)
                                           (inj,₀ pΔA ⁻¹)
                                           (from≡ (Ty= (inj,₀ pΔA)) (inj,₁ pΔA) ⁻¹̃)
                                           (⌜⌝V≃ (pΓ ⁻¹)
                                                 (pΔA ⁻¹)
                                                 {γ}{β ,V x}
                                                 (from≡ (Vars= pΓ pΔA) pγ ⁻¹̃))
                                     ◾̃ to≃ π₁β))
                              {π₂v γ}{x}
                              ( π₂v≃ (pΓ ⁻¹)
                                     (inj,₀ pΔA ⁻¹)
                                     (from≡ (Ty= (inj,₀ pΔA)) (inj,₁ pΔA) ⁻¹̃)
                                     {γ}{β ,V x}
                                     (from≡ (Vars= pΓ pΔA) pγ ⁻¹̃)
                               ◾̃ uncoe (Var= refl (πv.,Vᴹ.pA[β] (β ,V x) _ x refl refl )) ⁻¹̃)
                       ◾̃ π₂β' ⁻¹̃
                       ◾̃ uncoe (Tm= pΓ ([]T= pΓ (inj,₀ pΔA) (inj,₁ pΔA) (π₁= pΓ (inj,₀ pΔA) (inj,₁ pΔA) (⌜⌝V= pΓ (,C= (inj,₀ pΔA) (inj,₁ pΔA)) (UIP' (Vars= pΓ pΔA) (Vars= pΓ (,C= (inj,₀ pΔA) (inj,₁ pΔA))) pγ))))))
               ◾ π₂= pΓ (inj,₀ pΔA) (inj,₁ pΔA) {⌜ β ,V x ⌝V}{⌜ γ ⌝V}
                        (⌜⌝V= pΓ (,C= (inj,₀ pΔA) (inj,₁ pΔA)){β ,V x}{γ} (UIP' (Vars= pΓ pΔA)(Vars= pΓ (,C= (inj,₀ pΔA) (inj,₁ pΔA))) pγ) )

abstract
  ⌜π₁v⌝ : ∀{Γ Δ A}(γ : Vars Γ (Δ , A))
        → ⌜ π₁v γ ⌝V ≡ π₁ ⌜ γ ⌝V
  ⌜π₁v⌝ γ = proj₁ (⌜πv⌝ {γ = γ})
  
abstract
  ⌜π₂v⌝ : ∀{Γ Δ A}(γ : Vars Γ (Δ , A))
        → ⌜ π₂v γ ⌝v ≡[ TmΓ= ([]T= refl refl refl (⌜π₁v⌝ γ)) ]≡ π₂ ⌜ γ ⌝V
  ⌜π₂v⌝ γ = proj₂ (⌜πv⌝ {γ = γ})

abstract
  ⌜π₂v⌝' : ∀{Γ Δ A}(γ : Vars Γ (Δ , A))
         → ⌜ π₂v γ ⌝v ≃ π₂ ⌜ γ ⌝V
  ⌜π₂v⌝' γ = from≡ (TmΓ= ([]T= refl refl refl (⌜π₁v⌝ γ))) (proj₂ (⌜πv⌝ {γ = γ}))
