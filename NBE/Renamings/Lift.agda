{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.Lift where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.ConElim

open import NBE.Renamings.Var
open import NBE.Renamings.Vars
open import NBE.Renamings.Proj
open import NBE.Renamings.Comp
open import NBE.Renamings.Weaken
open import NBE.Renamings.Id
open import NBE.Renamings.Idl
open import NBE.Renamings.VsuComp
open import NBE.Renamings.IdComp
open import NBE.Renamings.Idr
open import NBE.Renamings.CompComp
open import NBE.Renamings.Ass

-- lifting a renaming

abstract
  p^V : ∀{Γ Δ}(β : Vars Γ Δ){A : Ty Δ}
      → A [ ⌜ β ⌝V ]T [ wk {A = A [ ⌜ β ⌝V ]T} ]T ≡ A [ ⌜ β ∘V wkV idV ⌝V ]T
  p^V {Γ}{Δ} β {A}
    = [][]T
    ◾ ap (_[_]T A)
         ( ⌜wkV⌝ β
         ◾ ⌜⌝V= refl refl {wkV β}{β ∘V wkV idV}
                (idrV ⁻¹ ◾ wkVβ {β = β}{wkV idV}))

-- definition

_^V_ : ∀{Γ Δ}(β : Vars Γ Δ)(A : Ty Δ) → Vars (Γ , A [ ⌜ β ⌝V ]T) (Δ , A)
β ^V A = (β ∘V wkV idV) ,V coe (VarΓ= (p^V β)) vze

infixl 5 _^V_

-- relation with embedding

abstract
  ⌜^⌝ : ∀{Γ Δ A}{β : Vars Γ Δ} → ⌜ β ^V A ⌝V ≡ ⌜ β ⌝V ^ A
  ⌜^⌝ {Γ}{Δ}{A}{β}

    = ,s≃' (⌜∘V⌝ β ⁻¹ ◾ ap (_∘_ ⌜ β ⌝V) ⌜wkid⌝ ⁻¹)
           ( ⌜⌝v≃ refl
                  ( []T≃ refl
                         ( ⌜⌝V≃ refl refl
                               {β ∘V wkV idV}{wkV β}
                               (to≃ (wkVβ {β = β}{wkV idV} ⁻¹ ◾ idrV))
                         ◾̃ to≃ (⌜wkV⌝ β ⁻¹))
                  ◾̃ to≃ ([][]T ⁻¹))
                  (uncoe (VarΓ= (p^V β)) ⁻¹̃)
           ◾̃ uncoe (TmΓ= [][]T))

-- lifting of idV

abstract
  idV^ : ∀{Γ A} → idV {Γ} ^V A ≃ idV {Γ , A}
  idV^ {Γ}{A}
    = ,V≃ (,C= refl ([]T= refl refl refl ⌜idV⌝ ◾ [id]T))
          refl
          (to≃ (idlV {β = wkV idV}) ◾̃ wkV≃ refl refl r̃ (to≃ ([]T= refl refl refl ⌜idV⌝ ◾ [id]T)))
          r̃
          ( uncoe (VarΓ= (p^V idV)) ⁻¹̃
          ◾̃ vze≃ refl (to≃ ([]T= refl refl refl ⌜idV⌝ ◾ [id]T))
          ◾̃ uncoe (VarΓ= (ap (_[_]T A) (pidV, _ _ ⁻¹))))

-- embedding the lifting

abstract
  ⌜idV^⌝ : ∀{Γ A} → ⌜ idV {Γ} ^V A ⌝V ≃ ⌜ idV {Γ , A} ⌝V
  ⌜idV^⌝ {Γ}{A} = ⌜⌝V≃ (,C= refl ([]T= refl refl refl ⌜idV⌝ ◾ [id]T)) refl (idV^ {Γ}{A})

-- composition and lifting

abstract
  ∘V^ : ∀{Γ Δ Θ A}{β : Vars Δ Θ}{γ : Vars Γ Δ}
       → (β ^V A) ∘V (γ ^V A [ ⌜ β ⌝V ]T)
       ≃ (β ∘V γ) ^V A
  ∘V^ {Γ}{Δ}{Θ}{A}{β}{γ}
    = ,V≃ (,C≃ refl (to≃ ([⌜∘V⌝]T {A = A}{β = β}{γ = γ})))
          refl
          {(β ∘V wkV idV) ∘V (γ ^V A [ ⌜ β ⌝V ]T)}
          ( to≃ assV
          ◾̃ ∘V≃' (,C≃ refl (to≃ ([⌜∘V⌝]T {A = A}{β = β}{γ = γ})))
                 ( to≃ (wkVβ {β = idV} ◾ idlV)
                 ◾̃ ∘V≃' (,C≃ refl (to≃ ([⌜∘V⌝]T {A = A}{β = β}{γ = γ})))
                        (wkV≃ refl
                              refl
                              r̃
                              (to≃ ([⌜∘V⌝]T {A = A}{β = β}{γ = γ}))))
          ◾̃ to≃ (assV ⁻¹))
          r̃
          {coe (VarΓ= ([][]T ◾ ap (_[_]T A) (⌜∘V⌝ (β ∘V wkV idV)))) (coe (VarΓ= (p^V β)) vze [ γ ^V A [ ⌜ β ⌝V ]T ]v)}
          ( uncoe (VarΓ= ([][]T ◾ ap (_[_]T A) (⌜∘V⌝ (β ∘V wkV idV)))) ⁻¹̃
          ◾̃ coe[]v' {β = γ ^V A [ ⌜ β ⌝V ]T}(p^V β){x = vze}
          ◾̃ uncoe (VarΓ= (ap (_[_]T (A [ ⌜ β ⌝V ]T)) (⌜π₁v⌝ (γ ^V A [ ⌜ β ⌝V ]T) ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹)) ⁻¹̃
          ◾̃ π₂vβ'
          ◾̃ uncoe (VarΓ= (p^V γ)) ⁻¹̃
          ◾̃ vze≃ refl (to≃ ([][]T ◾ ap (_[_]T A) (⌜∘V⌝ β)))
          ◾̃ uncoe (VarΓ= (p^V (β ∘V γ))))
