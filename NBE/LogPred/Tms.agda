{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Tms
  (U̅ : FamPSh TMU) -- U^--
  (E̅l : FamPSh (TMU ,P TMEl ,P U̅ [ wkn ]F)) -- E^--l
  where

open import lib
open import JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty U̅ E̅l
open import NBE.LogPred.Ty.Subst

open Motives M
open MethodsCon mCon
open MethodsTy mTy

module m,sᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {Δ : Con}{⟦Δ⟧ : Conᴹ Δ}
  {σ : Tms Γ Δ}(⟦σ⟧ : Tmsᴹ ⟦Γ⟧ ⟦Δ⟧ σ)
  {A : Ty Δ}{⟦A⟧ : Tyᴹ ⟦Δ⟧ A}
  {t : Tm Γ (A [ σ ]T)}(⟦t⟧ : Tmᴹ ⟦Γ⟧ (⟦A⟧ [ ⟦σ⟧ ]Tᴹ) t)
  where

    abstract
      pcoe$S₁ : ∀{Ψ}{ρ : TMC Γ $P Ψ}
              → TMs σ $n ρ ≡ π₁ (TMs (σ ,s t) $n ρ)
      pcoe$S₁ = π₁β ⁻¹ ◾ ap π₁ (,∘ ⁻¹)

    abstract
      pcoe$S₂ : ∀{Ψ}{ρ : TMC Γ $P Ψ}{α : ⟦Γ⟧ $F ρ}
              → _≡_ {A = (TMC Δ ,P TMT A ,P ⟦Δ⟧ [ wkn ]F) $P Ψ}
                    ( σ ∘ ρ
                    ,Σ coe (TmΓ= [][]T) (TMt t $S ρ)
                    ,Σ ⟦σ⟧ $S (ρ ,Σ α))
                    ( π₁ (TMs (σ ,s t) $n ρ)
                    ,Σ π₂ (TMs (σ ,s t) $n ρ)
                    ,Σ coe ($F= ⟦Δ⟧ pcoe$S₁) (⟦σ⟧ $S (ρ ,Σ α)))
      pcoe$S₂ = Tbase= ⟦Δ⟧
                       pcoe$S₁
                       ( uncoe (TmΓ= [][]T) ⁻¹̃
                       ◾̃ uncoe (TmΓ= [][]T)
                       ◾̃ π₂β' ⁻¹̃
                       ◾̃ π₂≃ (,∘ ⁻¹))
                       (uncoe ($F= ⟦Δ⟧ pcoe$S₁))

    p$S : ∀{Ψ}(α : (TMC Γ ,P ⟦Γ⟧) $P Ψ)
        → (⟦Δ⟧ ,Cᴹ ⟦A⟧) [ TMs (σ ,s t) ]F [ wkn {A = ⟦Γ⟧} ]F $F α
    p$S {Ψ} α = coe ($F= ⟦Δ⟧ pcoe$S₁) (⟦σ⟧ $S α)
              ,Σ coe ($F= ⟦A⟧ pcoe$S₂) (⟦t⟧ $S α)

    abstract
      pnatS : ∀{Ψ Ω}{f : Vars Ω Ψ}{α : (TMC Γ ,P ⟦Γ⟧) $P Ψ}
            → _$F_$_ ((⟦Δ⟧ ,Cᴹ ⟦A⟧) [ TMs (σ ,s t) ]F [ wkn {A = ⟦Γ⟧} ]F)
                     f
                     {α}
                     (p$S α)
            ≡ p$S ((TMC Γ ,P ⟦Γ⟧) $P f $ α)
      pnatS {Ψ}{Ω}{f}{α}
      
        = from≃ ( uncoe (ap (λ ρ → Σ (⟦Δ⟧ $F π₁ ρ) (λ α₁ → ⟦A⟧ $F (π₁ ρ ,Σ π₂ ρ ,Σ α₁))) ass) ⁻¹̃
                ◾̃ ,Σ≃≃ (funext≃ ($F= ⟦Δ⟧ (ap π₁ ass)) (λ x₂ → to≃ ($F= ⟦A⟧ (Tbase= ⟦Δ⟧ (ap π₁ ass) (π₂≃ ass) x₂))))
                     ( uncoe ($F= ⟦Δ⟧ π₁∘) ⁻¹̃
                     ◾̃ $Ff$≃ ⟦Δ⟧ (ap π₁ ,∘ ◾ π₁β) (uncoe (ap (_$F_ ⟦Δ⟧) (π₁β ⁻¹ ◾ ap π₁ (,∘ ⁻¹))) ⁻¹̃)
                     ◾̃ uncoe (ap (_$F_ ⟦Δ⟧) ass)
                     ◾̃ to≃ (natS ⟦σ⟧)
                     ◾̃ uncoe ($F= ⟦Δ⟧ pcoe$S₁))
                     ( uncoe ($F= ⟦A⟧ (m,Cᴹ.pcoe ⟦Δ⟧ ⟦A⟧ (p$S α))) ⁻¹̃
                     ◾̃ $Ff$≃ ⟦A⟧ (Tbase= ⟦Δ⟧
                                         (ap π₁ ,∘ ◾ π₁β)
                                         (π₂≃ ,∘ ◾̃ π₂β' ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T))
                                         (uncoe ($F= ⟦Δ⟧ pcoe$S₁) ⁻¹̃))
                                 (uncoe ($F= ⟦A⟧ pcoe$S₂) ⁻¹̃)
                     ◾̃ uncoe (ap (_$F_ ⟦A⟧) (m[]Tᴹ.$F$coe ⟦A⟧ ⟦σ⟧))
                     ◾̃ uncoe (ap (_$F_ (⟦A⟧ [ ⟦σ⟧ ]Tᴹ)) (natn (TMt t ^S ⟦Γ⟧)))
                     ◾̃ to≃ (natS ⟦t⟧)
                     ◾̃ uncoe ($F= ⟦A⟧ pcoe$S₂)))

    _,sᴹ_ : Tmsᴹ ⟦Γ⟧ (⟦Δ⟧ ,Cᴹ ⟦A⟧) (σ ,s t)
    _,sᴹ_ = record
      { _$S_ = p$S
      ; natS = pnatS
      }

module midᴹ
  {Γ : Con}
  {⟦Γ⟧ : Conᴹ Γ}
  where
    p$S : ∀{Ψ}(α : (TMC Γ ,P ⟦Γ⟧) $P Ψ) → ⟦Γ⟧ [ TMs id ]F $F (proj₁ α)
    p$S (ρ ,Σ α) = coe ($F= ⟦Γ⟧ (idl ⁻¹)) α

    abstract
       pnatS : ∀{Ψ Ω}{f : Vars Ω Ψ}{α : (TMC Γ ,P ⟦Γ⟧) $P Ψ}
             → coe (ap (_$F_ ⟦Γ⟧) ass) (⟦Γ⟧ $F f $ coe (ap (_$F_ ⟦Γ⟧) (idl ⁻¹)) (proj₂ α))
             ≡ coe (ap (_$F_ ⟦Γ⟧) (idl ⁻¹)) (⟦Γ⟧ $F f $ proj₂ α)
       pnatS {Ψ}{Ω}{f}{α}
       
         = from≃ ( uncoe (ap (_$F_ ⟦Γ⟧) ass) ⁻¹̃
                 ◾̃ $Ff$≃ ⟦Γ⟧ idl (uncoe (ap (_$F_ ⟦Γ⟧) (idl ⁻¹)) ⁻¹̃)
                 ◾̃ uncoe (ap (_$F_ ⟦Γ⟧) (idl ⁻¹)))

    idᴹ : Tmsᴹ ⟦Γ⟧ ⟦Γ⟧ id
    idᴹ = record
      { _$S_ = p$S
      ; natS = pnatS
      }

module m∘ᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {Δ : Con}{⟦Δ⟧ : Conᴹ Δ}
  {Θ : Con}{⟦Θ⟧ : Conᴹ Θ}
  {σ : Tms Δ Θ}(⟦σ⟧ : Tmsᴹ ⟦Δ⟧ ⟦Θ⟧ σ)
  {ν : Tms Γ Δ}(⟦ν⟧ : Tmsᴹ ⟦Γ⟧ ⟦Δ⟧ ν)
  where

    p$S : ∀{Ψ}(α : (TMC Γ ,P ⟦Γ⟧) $P Ψ)
        → ⟦Θ⟧ [ TMs (σ ∘ ν) ]F [ wkn {A = ⟦Γ⟧} ]F $F α
    p$S {Ψ}(ρ ,Σ α) = coe ($F= ⟦Θ⟧ (ass ⁻¹))
                         (⟦σ⟧ $S (ν ∘ ρ ,Σ ⟦ν⟧ $S (ρ ,Σ α)))

    abstract
      pnatS : ∀{Ψ Ω}{f : Vars Ω Ψ}{α : (TMC Γ ,P ⟦Γ⟧) $P Ψ}
            → _$F_$_ (⟦Θ⟧ [ TMs (σ ∘ ν) ]F [ wkn {A = ⟦Γ⟧} ]F) f {α} (p$S α)
            ≡ p$S ((TMC Γ ,P ⟦Γ⟧) $P f $ α)
      pnatS
        = from≃ ( uncoe (ap (_$F_ ⟦Θ⟧) ass) ⁻¹̃
                ◾̃ $Ff$≃ ⟦Θ⟧ ass (uncoe (ap (_$F_ ⟦Θ⟧) (ass ⁻¹)) ⁻¹̃)
                ◾̃ from≡ (ap (_$F_ ⟦Θ⟧) ass) (natS ⟦σ⟧)
                ◾̃ t$S≃ {t = ⟦σ⟧} (,Σ≃' ass (from≡ (ap (_$F_ ⟦Δ⟧) ass) (natS ⟦ν⟧)))
                ◾̃ uncoe ($F= ⟦Θ⟧ (ass ⁻¹)))

    _∘ᴹ_ : Tmsᴹ ⟦Γ⟧ ⟦Θ⟧ (σ ∘ ν)
    _∘ᴹ_ = record
      { _$S_ = p$S
      ; natS = pnatS
      }

proj₁,Cᴹ≃ : {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}{A : Ty Γ}(⟦A⟧ : Tyᴹ ⟦Γ⟧ A)
            {Ψ : Con}{ρ₀ ρ₁ : TMC (Γ , A) $P Ψ}(ρ₂ : ρ₀ ≡ ρ₁)
            {α₀ : (⟦Γ⟧ ,Cᴹ ⟦A⟧) $F ρ₀}{α₁ : (⟦Γ⟧ ,Cᴹ ⟦A⟧) $F ρ₁}(α₂ : α₀ ≃ α₁)
          → proj₁ α₀ ≃ proj₁ α₁
proj₁,Cᴹ≃ ⟦A⟧ refl (refl ,≃ refl) = refl ,≃ refl

proj₁,Cᴹcoe : {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}{A : Ty Γ}(⟦A⟧ : Tyᴹ ⟦Γ⟧ A)
              {Ψ : Con}{ρ₀ ρ₁ : TMC (Γ , A) $P Ψ}(ρ₂ : ρ₀ ≡ ρ₁)
              {α₀ : (⟦Γ⟧ ,Cᴹ ⟦A⟧) $F ρ₀}
            → proj₁ (coe (ap (_$F_ (⟦Γ⟧ ,Cᴹ ⟦A⟧)) ρ₂) α₀) ≃ proj₁ α₀
proj₁,Cᴹcoe ⟦A⟧ refl = refl ,≃ refl

module mπ₁ᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {Δ : Con}{⟦Δ⟧ : Conᴹ Δ}
  {A : Ty Δ}{⟦A⟧ : Tyᴹ ⟦Δ⟧ A}
  {σ : Tms Γ (Δ , A)}(⟦σ⟧ : Tmsᴹ ⟦Γ⟧ (⟦Δ⟧ ,Cᴹ ⟦A⟧) σ)
  where

     p$S : ∀{Ψ}(α : (TMC Γ ,P ⟦Γ⟧) $P Ψ)
         → ⟦Δ⟧ [ TMs (π₁ σ) ]F [ wkn {A = ⟦Γ⟧} ]F $F α
     p$S {Ψ} α = coe ($F= ⟦Δ⟧ (π₁∘ ⁻¹))
                     (proj₁ (⟦σ⟧ $S α))

     abstract
       pnatS : ∀{Ψ Ω}{f : Vars Ω Ψ}{α : (TMC Γ ,P ⟦Γ⟧) $P Ψ}
             → _$F_$_ (⟦Δ⟧ [ TMs (π₁ σ) ]F [ wkn {A = ⟦Γ⟧} ]F) f {α} (p$S α)
             ≡ p$S ((TMC Γ ,P ⟦Γ⟧) $P f $ α)
       pnatS {Ψ}{Ω}{f}{ρ ,Σ α}
       
         = from≃ ( uncoe (ap (_$F_ ⟦Δ⟧) ass) ⁻¹̃
                 ◾̃ $Ff$≃ ⟦Δ⟧ π₁∘ (uncoe (ap (_$F_ ⟦Δ⟧) (π₁∘ ⁻¹)) ⁻¹̃)
                 ◾̃ uncoe ($F= ⟦Δ⟧ π₁∘)
                 ◾̃ proj₁,Cᴹcoe ⟦A⟧ ass ⁻¹̃
                 ◾̃ proj₁,Cᴹ≃ ⟦A⟧ refl (to≃ (natS ⟦σ⟧))
                 ◾̃ uncoe (ap (_$F_ ⟦Δ⟧) (π₁∘ ⁻¹)))

     π₁ᴹ : Tmsᴹ ⟦Γ⟧ ⟦Δ⟧ (π₁ σ)
     π₁ᴹ = record
       { _$S_ = p$S
       ; natS = pnatS
       }

mTms : MethodsTms M mCon mTy
mTms = record
  { εᴹ    = record { _$S_ = λ _ → tt ; natS = refl }
  ; _,sᴹ_ = m,sᴹ._,sᴹ_
  ; idᴹ   = midᴹ.idᴹ
  ; _∘ᴹ_  = m∘ᴹ._∘ᴹ_
  ; π₁ᴹ   = mπ₁ᴹ.π₁ᴹ }
