{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

open import lib
open import JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty.Subst

open Motives M
open MethodsCon mCon

module NBE.LogPred.Ty.Pi.Exp
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {A : Ty Γ}{⟦A⟧ : Tyᴹ ⟦Γ⟧ A}
  {B : Ty (Γ , A)}(⟦B⟧ : Tyᴹ (⟦Γ⟧ ,Cᴹ ⟦A⟧) B)
  {Ψ : Con}
  (α' : (TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ)
  where
  
ρ : TMC Γ $P Ψ
ρ = proj₁ (proj₁ α')

s : TMT (Π A B) $F ρ
s = proj₂ (proj₁ α')

α : ⟦Γ⟧ $F ρ
α = proj₂ α'

abstract
  projTbase : ∀{Ω}{β : Vars Ω Ψ}{u : TMT A $F (ρ ∘ ⌜ β ⌝V)}
            → _≡_ {A = (TMC Γ ,P TMT A ,P ⟦Γ⟧ [ wkn ]F) $P Ω}
                  (ρ ∘ ⌜ β ⌝V ,Σ u ,Σ ⟦Γ⟧ $F β $ α)
                  (π₁ (ρ ∘ ⌜ β ⌝V ,s u) ,Σ π₂ (ρ ∘ ⌜ β ⌝V ,s u) ,Σ coe (ap (_$F_ ⟦Γ⟧) (π₁β ⁻¹)) (⟦Γ⟧ $F β $ α))
  projTbase = Tbase= ⟦Γ⟧
                     (π₁β ⁻¹)
                     (π₂β' ⁻¹̃)
                     (uncoe ($F= ⟦Γ⟧ (π₁β ⁻¹)))
                         
abstract
  pcoe1 : ∀{Ω}{δ : Tms Ω Ψ}{u : TMT A $F (ρ ∘ δ)}
        → B [ ρ ∘ δ ^ A ]T [ < u > ]T ≡ B [ ρ ∘ δ ,s u ]T
  pcoe1 = [][]T ◾ ap (_[_]T B) (^∘<> refl)

abstract
  pcoe2 : ∀{Ω}{δ : Tms Ω Ψ}
    → Π A B [ ρ ]T [ δ ]T ≡ Π (A [ ρ ∘ δ ]T) (B [ ρ ∘ δ ^ A ]T)
  pcoe2 = [][]T ◾ Π[]

abstract
  pcoe3 : ∀{Ω}(β : Vars Ω Ψ){Ξ}{γ : Vars Ξ Ω}
    → A [ ρ ∘ ⌜ β ⌝V ]T [ ⌜ γ ⌝V ]T ≡ A [ ρ ∘ ⌜ β ∘V γ ⌝V ]T
  pcoe3 β = [][]T ◾ ap (_[_]T A) (ass ◾ ap (_∘_ _) (⌜∘V⌝ β))

abstract
  pcoe4 : ∀{Ω}(β : Vars Ω Ψ){u : TMT A $F (ρ ∘ ⌜ β ⌝V)}
          {Ξ}{γ : Vars Ξ Ω}
        → _≡_ {A = (TMC Γ ,P TMT A ,P ⟦Γ⟧ [ wkn ]F) $P Ξ}
              ( (ρ ∘ ⌜ β ⌝V) ∘ ⌜ γ ⌝V
              ,Σ coe (TmΓ= [][]T) (u [ ⌜ γ ⌝V ]t)
              ,Σ ⟦Γ⟧ $F γ $ (⟦Γ⟧ $F β $ α))
              ( ρ ∘ ⌜ β ∘V γ ⌝V
              ,Σ coe (TmΓ= (pcoe3 β)) (u [ ⌜ γ ⌝V ]t)
              ,Σ ⟦Γ⟧ $F β ∘V γ $ α)
  pcoe4 β
    = Tbase=
        ⟦Γ⟧
        (ass ◾ ap (_∘_ _) (⌜∘V⌝ β))
        ( uncoe (TmΓ= [][]T) ⁻¹̃
        ◾̃ uncoe (TmΓ= (pcoe3 β)))
        (compF' ⟦Γ⟧ ⁻¹̃)

abstract
  pcoe5 : ∀{Ω}{β : Vars Ω Ψ}{u : TMT A $F (ρ ∘ ⌜ β ⌝V)}
          {v : ⟦A⟧ $F (ρ ∘ ⌜ β ⌝V ,Σ u ,Σ ⟦Γ⟧ $F β $ α)}
          {Ξ}{γ : Vars Ξ Ω}
        → _≡_ {A = (TMC (Γ , A) ,P TMT B ,P (⟦Γ⟧ ,Cᴹ ⟦A⟧) [ wkn ]F) $P Ξ}
              ((TMC (Γ , A) ,P TMT B ,P (⟦Γ⟧ ,Cᴹ ⟦A⟧) [ wkn ]F) $P γ $
                  ( (ρ ∘ ⌜ β ⌝V ,s u)
                  ,Σ coe (TmΓ= pcoe1) (coe (TmΓ= pcoe2) (s [ ⌜ β ⌝V ]t) $$ u)
                  ,Σ ( coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β $ α)
                    ,Σ coe ($F= ⟦A⟧ projTbase) v)))
              ( (ρ ∘ ⌜ β ∘V γ ⌝V ,s coe (TmΓ= (pcoe3 β)) (u [ ⌜ γ ⌝V ]t))
              ,Σ coe (TmΓ= pcoe1) (coe (TmΓ= pcoe2) (s [ ⌜ β ∘V γ ⌝V ]t) $$ coe (TmΓ= (pcoe3 β)) (u [ ⌜ γ ⌝V ]t))
              ,Σ ( coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β ∘V γ $ α)
                ,Σ coe ($F= ⟦A⟧ projTbase) (coe ($F= ⟦A⟧ (pcoe4 β)) (⟦A⟧ $F γ $ v))))
  pcoe5 {Ω}{β}{u}{v}{Ξ}{γ}
    = Tbase= (⟦Γ⟧ ,Cᴹ ⟦A⟧)
             p
             ( uncoe (TmΓ= [][]T) ⁻¹̃
             ◾̃ coe[]t' pcoe1
             ◾̃ [][]t'
             ◾̃ []t≃ refl (to≃ ,∘)
             ◾̃ app[,]
             ◾̃ []t≃' refl
                     (,C≃ refl (to≃ q))
                     (to≃ [][]T ◾̃ []T≃ (,C≃ refl (to≃ q)) r)
                     (app≃ refl (to≃ q) (to≃ [][]T ◾̃ []T≃ (,C≃ refl (to≃ q)) r)
                           ( uncoe (TmΓ= Π[]) ⁻¹̃
                           ◾̃ coe[]t' pcoe2
                           ◾̃ [][]t'
                           ◾̃ []t≃'' (ap (_∘_ ⌜ β ⌝V) idl ◾ (⌜∘V⌝ β))
                           ◾̃ uncoe (TmΓ= pcoe2)))
                     (,s≃ refl
                          refl
                          r̃
                          (to≃ q)
                          ( uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃
                          ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                          ◾̃ coe[]t' ([id]T ⁻¹)
                          ◾̃ uncoe (TmΓ= (pcoe3 β))
                          ◾̃ uncoe (TmΓ= ([id]T ⁻¹))))
             ◾̃ uncoe (TmΓ= pcoe1))
             (,Σ≃≃ (funext≃ ($F= ⟦Γ⟧ (ap π₁ p))
                          (λ x₂ → to≃ ($F= ⟦A⟧ (Tbase= ⟦Γ⟧ (ap π₁ p) (π₂≃ p) x₂ ))))
                 ( uncoe ($F= ⟦Γ⟧ π₁∘) ⁻¹̃
                 ◾̃ $Ff$≃ ⟦Γ⟧ π₁β (uncoe ($F= ⟦Γ⟧ (π₁β ⁻¹)) ⁻¹̃)
                 ◾̃ compF' ⟦Γ⟧ ⁻¹̃
                 ◾̃ uncoe ($F= ⟦Γ⟧ (π₁β ⁻¹)))
                 ( uncoe ($F= ⟦A⟧ (m,Cᴹ.pcoe ⟦Γ⟧ ⟦A⟧ (coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β $ α) ,Σ coe ($F= ⟦A⟧ projTbase) v))) ⁻¹̃
                 ◾̃ $Ff$≃ ⟦A⟧ (Tbase= ⟦Γ⟧ π₁β
                                         π₂β'
                                         (uncoe (ap (_$F_ ⟦Γ⟧) (π₁β ⁻¹)) ⁻¹̃))
                             (uncoe ($F= ⟦A⟧ projTbase) ⁻¹̃)
                 ◾̃ uncoe ($F= ⟦A⟧ (pcoe4 β))
                 ◾̃ uncoe ($F= ⟦A⟧ projTbase)))
    where
      abstract
        p : (ρ ∘ ⌜ β ⌝V ,s u) ∘ ⌜ γ ⌝V
          ≡ ρ ∘ ⌜ β ∘V γ ⌝V ,s coe (TmΓ= (pcoe3 β)) (u [ ⌜ γ ⌝V ]t)
        p = ,∘
          ◾ ,s≃' (ass ◾ ap (_∘_ _) (⌜∘V⌝ β))
                 ( uncoe (TmΓ= [][]T) ⁻¹̃
                 ◾̃ uncoe (TmΓ= (pcoe3 β)))

      abstract
        q : _≡_ {A = Ty Ξ}
                (A [ ρ ∘ ⌜ β ⌝V ]T [ id ∘ ⌜ γ ⌝V ]T)
                (A [ ρ ∘ ⌜ β ∘V γ ⌝V ]T)
        q = [][]T ◾ ap (_[_]T A) (ass ◾ ap (_∘_ ρ) (ap (_∘_ ⌜ β ⌝V) idl ◾ (⌜∘V⌝ β)))

      abstract
        r : (ρ ∘ ⌜ β ⌝V ^ A) ∘ (id ∘ ⌜ γ ⌝V ^ A [ ρ ∘ ⌜ β ⌝V ]T)
          ≃ ρ ∘ ⌜ β ∘V γ ⌝V ^ A
        r = ∘^ ⁻¹̃
          ◾̃ ^≃ (ap (_∘_ (ρ ∘ ⌜ β ⌝V)) idl ◾ (ass ◾ ap (_∘_ ρ) (⌜∘V⌝ β)))

record E : Set where

  field
    map : ∀{Ω}(β : Vars Ω Ψ)
          {u : TMT A $F (ρ ∘ ⌜ β ⌝V)}
          (v : ⟦A⟧ $F (ρ ∘ ⌜ β ⌝V ,Σ u ,Σ ⟦Γ⟧ $F β $ α))
        → ⟦B⟧ $F ( (ρ ∘ ⌜ β ⌝V ,s u)
                 ,Σ coe (TmΓ= pcoe1) (coe (TmΓ= pcoe2) (s [ ⌜ β ⌝V ]t) $$ u)
                 ,Σ ( (coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β $ α))
                   ,Σ coe ($F= ⟦A⟧ projTbase) v))
    nat : ∀{Ω}{β : Vars Ω Ψ}
          {u : TMT A $F (ρ ∘ ⌜ β ⌝V)}
          {v : ⟦A⟧ $F (ρ ∘ ⌜ β ⌝V ,Σ u ,Σ ⟦Γ⟧ $F β $ α)}
          {Ξ}{γ : Vars Ξ Ω}
        → ⟦B⟧ $F γ $ map β v
        ≡[ $F= ⟦B⟧ pcoe5 ]≡
          map (β ∘V γ) (coe ($F= ⟦A⟧ (pcoe4 β)) (⟦A⟧ $F γ $ v))

abstract
  map≃ : (e : E)
         {Ω : Con}{β₀ β₁ : Vars Ω Ψ}(β₂ : β₀ ≡ β₁)
         {u₀ : TMT A $F (ρ ∘ ⌜ β₀ ⌝V)}{u₁ : TMT A $F (ρ ∘ ⌜ β₁ ⌝V)}(u₂ : u₀ ≃ u₁)
         {v₀ : ⟦A⟧ $F (ρ ∘ ⌜ β₀ ⌝V ,Σ u₀ ,Σ ⟦Γ⟧ $F β₀ $ α)}{v₁ : ⟦A⟧ $F (ρ ∘ ⌜ β₁ ⌝V ,Σ u₁ ,Σ ⟦Γ⟧ $F β₁ $ α)}(v₂ : v₀ ≃ v₁)
       → E.map e β₀ v₀ ≃ E.map e β₁ v₁
  map≃ e refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl
