{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Ty.El
  (U̅ : FamPSh TMU)
  (E̅l : FamPSh (TMU ,P TMEl ,P U̅ [ wkn ]F))
  where

open import lib
open import JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty.U U̅

open Motives M
open MethodsCon mCon

Elbase= : ∀{Ψ}{ρ₀ ρ₁ : TMU $P Ψ}(ρ₂ : ρ₀ ≡ ρ₁)
          {t₀ : TMEl $F ρ₀}{t₁ : TMEl $F ρ₁}(t₂ : t₀ ≃ t₁)
          {v₀ : U̅ $F ρ₀}{v₁ : U̅ $F ρ₁}(v₂ : v₀ ≃ v₁)
        → _≡_ {A = (TMU ,P TMEl ,P U̅ [ wkn ]F) $P Ψ} (ρ₀ ,Σ t₀ ,Σ v₀) (ρ₁ ,Σ t₁ ,Σ v₁)
Elbase= refl (refl ,≃ refl) (refl ,≃ refl) = refl


module mElᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {Â : Tm Γ U}(⟦Â⟧ : Tmᴹ ⟦Γ⟧ mUᴹ.Uᴹ Â)  
  where

    p$F : ∀{Ψ} → (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ → Set
    p$F (ρ ,Σ s ,Σ α) = E̅l $F ( coe (TmΓ= U[]) (Â [ ρ ]t)
                            ,Σ coe (TmΓ= El[]) s
                            ,Σ ⟦Â⟧ $S (ρ ,Σ α))

    abstract
      coe$F$ : ∀{Ψ Ω}(f : Vars Ω Ψ)
               {ρ : TMC Γ $P Ψ}{u : TMT (El Â) $F ρ}{α : ⟦Γ⟧ $F ρ}
             → _≡_ {A = (TMU ,P TMEl ,P U̅ [ wkn ]F) $P Ω}
                   ( coe (TmΓ= U[]) (coe (TmΓ= U[]) (Â [ ρ ]t) [ ⌜ f ⌝V ]t)
                   ,Σ coe (TmΓ= El[]) (coe (TmΓ= El[]) u [ ⌜ f ⌝V ]t)
                   ,Σ U̅ $F f $ (⟦Â⟧ $S (ρ ,Σ α)))
                   ( coe (TmΓ= U[]) (Â [ ρ ∘ ⌜ f ⌝V ]t)
                   ,Σ coe (TmΓ= El[]) (coe (TmΓ= [][]T) (u [ ⌜ f ⌝V ]t))
                   ,Σ ⟦Â⟧ $S (ρ ∘ ⌜ f ⌝V ,Σ ⟦Γ⟧ $F f $ α))
      coe$F$ {Ψ}{Ω} f {ρ}{u}{α}
      
        = Elbase= (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                         ◾̃ coe[]t' U[]
                         ◾̃ [][]t'
                         ◾̃ uncoe (TmΓ= U[])))
                  ( uncoe (TmΓ= El[]) ⁻¹̃
                  ◾̃ coe[]t' El[]
                  ◾̃ uncoe (TmΓ= [][]T)
                  ◾̃ uncoe (TmΓ= El[]))
                  ( uncoe ($F= U̅ (mUᴹ.coe$F$ f))
                  ◾̃ uncoe (ap (_$F_ (mUᴹ.Uᴹ {Γ}{⟦Γ⟧})) (natn (TMt Â ^S ⟦Γ⟧)))
                  ◾̃ to≃ (natS ⟦Â⟧))

    p$F$ : ∀{Ψ Ω}(f : Vars Ω Ψ){α : (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}
         → p$F α
         → p$F ((TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F) $P f $ α)
    p$F$ f {ρ ,Σ u ,Σ α} a = coe ($F= E̅l (coe$F$ f)) (E̅l $F f $ a)

    abstract
      pidF : ∀{Ψ}{α : (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}
             {a : p$F α}
           → p$F$ idV a ≡[ ap p$F (idP (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F)) ]≡ a
      pidF = from≃ ( uncoe (ap p$F (idP (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F))) ⁻¹̃
                   ◾̃ uncoe ($F= E̅l (coe$F$ idV)) ⁻¹̃
                   ◾̃ idF' E̅l)

    abstract
      pcompF : ∀{Ψ Ω Ξ}{f : Vars Ω Ψ}{g : Vars Ξ Ω}
               {α : (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}{a : p$F α}
             → p$F$ (f ∘V g) a
             ≡[ ap p$F (compP (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F){f = f}{g = g}) ]≡
               p$F$ g (p$F$ f a)
      pcompF {f = f}{g = g}
        = from≃ ( uncoe (ap p$F (compP (TMC Γ ,P TMT (El Â) ,P ⟦Γ⟧ [ wkn ]F){f = f}{g = g})) ⁻¹̃
                ◾̃ uncoe ($F= E̅l (coe$F$ (f ∘V g))) ⁻¹̃
                ◾̃ compF' E̅l
                ◾̃ $Ff$≃ E̅l (Elbase= (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                                           ◾̃ coe[]t' U[]
                                           ◾̃ [][]t'
                                           ◾̃ uncoe (TmΓ= U[])))
                                    ( uncoe (TmΓ= El[]) ⁻¹̃
                                    ◾̃ coe[]t' El[]
                                    ◾̃ uncoe (TmΓ= [][]T)
                                    ◾̃ uncoe (TmΓ= El[]))
                                    ( uncoe ($F= U̅ (mUᴹ.coe$F$ f))
                                    ◾̃ uncoe (ap (_$F_ (mUᴹ.Uᴹ {Γ}{⟦Γ⟧})) (natn (TMt Â ^S ⟦Γ⟧)))
                                    ◾̃ to≃ (natS ⟦Â⟧)))
                           (uncoe ($F= E̅l (coe$F$ f)))
                ◾̃ uncoe ($F= E̅l (coe$F$ g)))
      
    Elᴹ : Tyᴹ ⟦Γ⟧ (El Â)
    Elᴹ = record
      { _$F_   = p$F
      ; _$F_$_ = p$F$
      ; idF    = pidF
      ; compF  = pcompF
      }
