{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Tm.Lam
  (U̅ : FamPSh TMU) -- U^--
  (E̅l : FamPSh (TMU ,P TMEl ,P U̅ [ wkn ]F)) -- E^--l
  where

open import lib
open import JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty U̅ E̅l
open import NBE.LogPred.Ty.Pi
open import NBE.LogPred.Tms U̅ E̅l
open import NBE.LogPred.Tm.Subst U̅ E̅l
open import NBE.LogPred.Tm.Proj U̅ E̅l

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms

open import NBE.Cheat

module mlamᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {A : Ty Γ}{⟦A⟧ : Tyᴹ ⟦Γ⟧ A}
  {B : Ty (Γ , A)}{⟦B⟧ : Tyᴹ (⟦Γ⟧ ,Cᴹ ⟦A⟧) B}
  {t : Tm (Γ , A) B}(⟦t⟧ : Tmᴹ (⟦Γ⟧ ,Cᴹ ⟦A⟧) ⟦B⟧ t)
  where

    open import NBE.LogPred.Ty.Pi.Exp ⟦B⟧

    module p$S
      {Ψ : Con}(α : (TMC Γ ,P ⟦Γ⟧) $P Ψ)
      where

        module Kripke
          {Ω : Con}(β : Vars Ω Ψ)
          {u : Tm Ω (A [ proj₁ α ∘ ⌜ β ⌝V ]T)}
          (v : ⟦A⟧ $F (proj₁ α ∘ ⌜ β ⌝V ,Σ u ,Σ ⟦Γ⟧ $F β $ proj₂ α))
          where

            γ : (TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ
            γ = proj₁ α ,Σ lam t [ proj₁ α ]t ,Σ proj₂ α

            abstract
              applam : app (coe (TmΓ= (pcoe2 γ))
                           (lam t [ proj₁ α ]t [ ⌜ β ⌝V ]t))
                     ≃ t [ proj₁ α ∘ ⌜ β ⌝V  ^ A ]t
              applam = app≃ refl
                            r̃
                            r̃
                            ( uncoe (TmΓ= (pcoe2 γ)) ⁻¹̃
                            ◾̃ [][]t'
                            ◾̃ from≡ (TmΓ= Π[]) lam[])
                     ◾̃ to≃ Πβ

            abstract
              pcoemap : _≡_ {A = (TMC (Γ , A) ,P TMT B ,P (⟦Γ⟧ ,Cᴹ ⟦A⟧) [ wkn ]F) $P Ω}
                            ((TMt t ^S (⟦Γ⟧ ,Cᴹ ⟦A⟧)) $n ( (proj₁ α ∘ ⌜ β ⌝V ,s u)
                                                         ,Σ ( coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β $ proj₂ α)
                                                           ,Σ coe ($F= ⟦A⟧ (projTbase γ)) v)))
                            ( (proj₁ α ∘ ⌜ β ⌝V ,s u)
                            ,Σ coe (TmΓ= (pcoe1 γ))
                                  (app (coe (TmΓ= (pcoe2 γ)) (lam t [ proj₁ α ]t [ ⌜ β ⌝V ]t))
                                     [ < u > ]t)
                            ,Σ ( coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β $ proj₂ α)
                              ,Σ coe ($F= ⟦A⟧ (projTbase γ)) v))
              pcoemap = Tbase= (⟦Γ⟧ ,Cᴹ ⟦A⟧)
                               refl
                               ( []t≃'' (^∘<> refl ⁻¹)
                               ◾̃ [][]t' ⁻¹̃
                               ◾̃ []t≃' refl refl r̃ (applam ⁻¹̃) r̃
                               ◾̃ uncoe (TmΓ= (pcoe1 γ)))
                               r̃

            pmap : ⟦B⟧ $F ( (proj₁ α ∘ ⌜ β ⌝V ,s u)
                          ,Σ coe (TmΓ= (pcoe1 γ))
                                (app (coe (TmΓ= (pcoe2 γ))
                                     (lam t [ proj₁ α ]t [ ⌜ β ⌝V ]t))
                                   [ < u > ]t)
                          ,Σ ( coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β $ proj₂ α)
                            ,Σ coe ($F= ⟦A⟧ (projTbase γ)) v))
            pmap = coe ($F= ⟦B⟧ pcoemap)
                       (⟦t⟧ $S ( (proj₁ α ∘ ⌜ β ⌝V ,s u)
                               ,Σ ( coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β $ proj₂ α)
                                 ,Σ coe ($F= ⟦A⟧ (projTbase γ)) v)))

        $S : Πᴹ ⟦A⟧ ⟦B⟧ [ TMt (lam t) ^S ⟦Γ⟧ ]F $F α
        $S = record
          { map = Kripke.pmap
          ; nat = cheat
          }
  
    lamᴹ : Tmᴹ ⟦Γ⟧ (Πᴹ ⟦A⟧ ⟦B⟧) (lam t)
    lamᴹ = record
      { _$S_ = p$S.$S
      ; natS = cheat
      }
