{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Tm.App
  (U̅ : FamPSh TMU) -- U^--
  (E̅l : FamPSh (TMU ,P TMEl ,P U̅ [ wkn ]F)) -- E^--l
  where

open import lib
open import JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty U̅ E̅l
open import NBE.LogPred.Tms U̅ E̅l
open import NBE.LogPred.Ty.Pi
open import NBE.LogPred.Tm.Subst U̅ E̅l
open import NBE.LogPred.Tm.Proj U̅ E̅l

open Motives M
open MethodsCon mCon
open MethodsTy mTy
open MethodsTms mTms

open import NBE.Cheat

module mappᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {A : Ty Γ}{⟦A⟧ : Tyᴹ ⟦Γ⟧ A}
  {B : Ty (Γ , A)}{⟦B⟧ : Tyᴹ (⟦Γ⟧ ,Cᴹ ⟦A⟧) B}
  {t : Tm Γ (Π A B)}(⟦t⟧ : Tmᴹ ⟦Γ⟧ (Πᴹ ⟦A⟧ ⟦B⟧) t)
  where

    open import NBE.LogPred.Ty.Pi.Exp ⟦B⟧

    abstract
      pcoe$S₁ : ∀{Ψ}{ρ : TMC (Γ , A) $P Ψ}(α : (⟦Γ⟧ ,Cᴹ ⟦A⟧) $F ρ)
              → _≡_ {A = (TMC Γ ,P TMT A ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}
                    (π₁ ρ ,Σ π₂ ρ ,Σ proj₁ α)
                    ( π₁ ρ ∘ ⌜ idV ⌝V
                    ,Σ coe (TmΓ= [][]T) (π₂ ρ [ ⌜ idV ⌝V ]t)
                    ,Σ ⟦Γ⟧ $F idV $ proj₁ α)
      pcoe$S₁ {Ψ}{ρ} α
      
        = Tbase= ⟦Γ⟧
                 (idr ⁻¹ ◾ ap (_∘_ _) (⌜idV⌝ ⁻¹))
                 ([⌜idV⌝]t ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T))
                 (idF' ⟦Γ⟧ ⁻¹̃)

    abstract
      pcoe$S₂
        : ∀{Ψ}{δ : TMC (Γ , A) $P Ψ}{γ : (⟦Γ⟧ ,Cᴹ ⟦A⟧) $F δ}
        → _≡_ {A = (TMC (Γ , A) ,P TMT B ,P (⟦Γ⟧ ,Cᴹ ⟦A⟧) [ wkn ]F) $P Ψ}
              ( ρ ((TMt t ^S ⟦Γ⟧) $n (π₁ δ ,Σ proj₁ γ)) ∘ ⌜ idV ⌝V
              ,s coe (TmΓ= [][]T) (π₂ δ [ ⌜ idV ⌝V ]t)
              ,Σ coe (TmΓ= (pcoe1 ((TMt t ^S ⟦Γ⟧) $n (π₁ δ ,Σ proj₁ γ))))
                    (  coe (TmΓ= (pcoe2 ((TMt t ^S ⟦Γ⟧) $n (π₁ δ ,Σ proj₁ γ))))
                         (s ((TMt t ^S ⟦Γ⟧) $n (π₁ δ ,Σ proj₁ γ)) [ ⌜ idV ⌝V ]t)
                    $$ coe (TmΓ= [][]T) (π₂ δ [ ⌜ idV ⌝V ]t))
              ,Σ ( coe ($F= ⟦Γ⟧ (π₁β ⁻¹))
                      (⟦Γ⟧ $F idV $ α ((TMt t ^S ⟦Γ⟧) $n (π₁ δ ,Σ proj₁ γ)))
                ,Σ coe ($F= ⟦A⟧ (projTbase ((TMt t ^S ⟦Γ⟧) $n (π₁ δ ,Σ proj₁ γ))))
                      (coe ($F= ⟦A⟧ (pcoe$S₁ γ)) (proj₂ γ))
                )
              )
              ((TMt (app t) ^S (⟦Γ⟧ ,Cᴹ ⟦A⟧)) $n (δ ,Σ γ))
      pcoe$S₂ {Ψ}{δ}{γ ,Σ a}
        = Tbase= (⟦Γ⟧ ,Cᴹ ⟦A⟧)
                 (,s≃' (ap (_∘_ _) ⌜idV⌝ ◾ idr) (uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ [⌜idV⌝]t) ◾ πη)
                 ( uncoe (TmΓ= (pcoe1 ((TMt t ^S ⟦Γ⟧) $n (π₁ δ ,Σ γ)))) ⁻¹̃
                 ◾̃ []t≃' refl
                         q
                         ([]T≃ q (^≃ (ap (_∘_ _) ⌜idV⌝ ◾ idr)))
                         (app≃ refl
                               (to≃ p)
                               ([]T≃ q (^≃ (ap (_∘_ _) ⌜idV⌝ ◾ idr)))
                               ( uncoe (TmΓ= (pcoe2 ((TMt t ^S ⟦Γ⟧) $n (π₁ δ ,Σ γ)))) ⁻¹̃
                               ◾̃ [⌜idV⌝]t
                               ◾̃ uncoe (TmΓ= Π[])))
                         (,s≃ refl
                              refl
                              r̃
                              (to≃ p)
                              ( uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃
                              ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                              ◾̃ [⌜idV⌝]t
                              ◾̃ uncoe (TmΓ= ([id]T ⁻¹))))
                 ◾̃ app[,] ⁻¹̃
                 ◾̃ []t≃'' πη)
                 (,Σ≃≃ (funext≃ ($F= ⟦Γ⟧ (π₁β ◾ (ap (_∘_ _) (⌜idV⌝) ◾ idr)))
                              (λ x₂ → to≃ ($F= ⟦A⟧ (Tbase= ⟦Γ⟧
                                                           (π₁β ◾ (ap (_∘_ _) (⌜idV⌝) ◾ idr))
                                                           (π₂β' ◾̃ (uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ [⌜idV⌝]t))
                                                           x₂))))
                     (uncoe ($F= ⟦Γ⟧ (π₁β ⁻¹)) ⁻¹̃ ◾̃ idF' ⟦Γ⟧)
                     (uncoe ($F= ⟦A⟧ (projTbase ((TMt t ^S ⟦Γ⟧) $n (π₁ δ ,Σ γ)))) ⁻¹̃ ◾̃ uncoe ($F= ⟦A⟧ (pcoe$S₁ (γ ,Σ a))) ⁻¹̃))

        where
          p : A [ π₁ δ ∘ ⌜ idV ⌝V ]T ≡ A [ π₁ δ ]T
          p = ap (_[_]T A) (ap (_∘_ _) ⌜idV⌝ ◾ idr)
          
          q : _≡_ {A = Con} (Ψ , A [ π₁ δ ∘ ⌜ idV ⌝V ]T) (Ψ , A [ π₁ δ ]T)
          q = ,C≃ refl (to≃ p)

    p$S : ∀{Ψ}(α : (TMC (Γ , A) ,P (⟦Γ⟧ ,Cᴹ ⟦A⟧)) $P Ψ)
        → ⟦B⟧ [ TMt (app t) ^S (⟦Γ⟧ ,Cᴹ ⟦A⟧) ]F $F α
    p$S {Ψ}(δ ,Σ γ) = coe ($F= ⟦B⟧ pcoe$S₂)
                         (E.map (⟦t⟧ $S (π₁ δ ,Σ proj₁ γ))
                                idV
                                (coe ($F= ⟦A⟧ (pcoe$S₁ γ)) (proj₂ γ)))

    appᴹ : Tmᴹ (⟦Γ⟧ ,Cᴹ ⟦A⟧) ⟦B⟧ (app t)
    appᴹ = record
      { _$S_ = p$S
      ; natS = cheat
      }
