module NBE.Glue where

open import lib
open import Cats
open import TT.Syntax
open import NBE.TM
open import NBE.Nf

open import NBE.Cheat

≡NFC : (Δ : Con) → FamPSh (TMC Δ)
≡NFC Δ = record
  { _$F_   = λ {Ψ} ρ → Σ (NFC Δ $P Ψ) λ τ → ρ ≡ ⌜ τ ⌝nfs
  ; _$F_$_ = cheat
  ; idF    = cheat
  ; compF  = cheat
  }

≡NFT : {Γ : Con}(A : Ty Γ) → FamPSh (TMC Γ ,P TMT A)
≡NFT A = record
  { _$F_   = λ { {Ψ} (ρ ,Σ t) → Σ (NFT A $F ρ) λ n → t ≡ ⌜ n ⌝nf }
  ; _$F_$_ = cheat
  ; idF    = cheat
  ; compF  = cheat
  }
