{-# OPTIONS --without-K --rewriting #-}

module lib where

open import Agda.Primitive

_∋_ : ∀ {a} (A : Set a) → A → A
A ∋ x = x

infixl 0 _∋_

idfun : ∀{i}{A : Set i} → A → A
idfun x = x

------------------------------------------------------------------------------
-- equality
------------------------------------------------------------------------------

data _≡_ {i}{A : Set i} (x : A) : A → Set i where
  refl : x ≡ x

infix 4 _≡_

{-# BUILTIN REWRITE _≡_ #-}

_◾_ : ∀{i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ◾ refl = refl

infixl 4 _◾_

_⁻¹ : ∀{i}{A : Set i}{x y : A} → x ≡ y → y ≡ x
refl ⁻¹ = refl

infix 5 _⁻¹

_≡⟨_⟩_ : ∀{i}{A : Set i}(x : A){y z : A} → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ x≡y ⟩ y≡z = x≡y ◾ y≡z

infixr 2 _≡⟨_⟩_

_∎ : ∀{i}{A : Set i}(x : A) → x ≡ x
x ∎ = refl

infix  3 _∎

coe : ∀{i}{A B : Set i} → A ≡ B → A → B
coe refl a = a

_≡[_]≡_ : ∀{i}{A B : Set i} → A → A ≡ B → B → Set i
x ≡[ α ]≡ y = coe α x ≡ y

infix 4 _≡[_]≡_

coh : ∀{i}{A B : Set i}(p : A ≡ B) → (a : A) → a ≡[ p ]≡ coe p a
coh refl a = refl

ap : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
    → f a₀ ≡ f a₁
ap f refl = refl

transport : ∀{i j}{A : Set i}(P : A → Set j){x y : A}(p : x ≡ y) → P x → P y
transport P refl a = a

apd : ∀{i j}{A : Set i}{B : A → Set j}(f : (x : A) → B x){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
    → f a₀ ≡[ ap B a₂ ]≡ f a₁
apd f refl = refl

apid : ∀{i}{A : Set i}{x y : A}(p : x ≡ y) → ap (λ x → x) p ≡ p
apid refl = refl

apap : ∀{i j k}{A : Set i}{B : Set j}{C : Set k}{f : A → B}{g : B → C}
       {a₀ a₁ : A}(a₂ : a₀ ≡ a₁) → ap (λ x → g (f x)) a₂ ≡ ap g (ap f a₂)
apap refl = refl

J : ∀{i j}{A : Set i} {x : A} (P : {y : A} → x ≡ y → Set j) → P refl → {y : A} → (w : x ≡ y) → P w
J P pr refl = pr

≡inv : {A : Set} {x y : A} (p : x ≡ y) → (p ◾ p ⁻¹) ≡ refl
≡inv refl = refl

coeap2 : {A : Set}{B : A → Set}{a₀ a₁ : A}(a₂ : a₀ ≡ a₁){t : B a₁}
       → coe (ap B a₂) (coe (ap B (a₂ ⁻¹)) t) ≡ t
coeap2 refl = refl

ap2 : ∀{i j k}{A : Set i}{B : Set j}{C : Set k}(f : A → B → C)
    → {a₀ a₁ : A}(a₂ : a₀ ≡ a₁){b₀ b₁ : B}(b₂ : b₀ ≡ b₁)
    → f a₀ b₀ ≡ f a₁ b₁
ap2 f refl refl = refl

coe2r : ∀{i j}{A : Set i}{B : A → Set j}{a₀ a₁ : A}
        (a₂ : a₀ ≡ a₁){b₀ : B a₀}{b₁ : B a₁}
      → b₀ ≡[ ap B a₂ ]≡ b₁ → b₀ ≡ coe (ap B (a₂ ⁻¹)) b₁
coe2r refl p = p

coecoe : ∀{i}{A B C : Set i}(P : A ≡ B)(Q : B ≡ C){a : A}
       → coe Q (coe P a) ≡ coe (P ◾ Q) a
coecoe refl refl = refl

≡= : ∀{i}{A₀ A₁ : Set i}(A₂ : A₀ ≡ A₁)
     {a₀ : A₀}{a₁ : A₁}(a₂ : a₀ ≡[ A₂ ]≡ a₁)
     {b₀ : A₀}{b₁ : A₁}(b₂ : b₀ ≡[ A₂ ]≡ b₁)
   → (a₀ ≡ b₀) ≡ (a₁ ≡ b₁)
≡= refl refl refl = refl

◾lid : ∀{i}{A : Set i}{x y : A}(p : x ≡ y) → (refl ◾ p) ≡ p
◾lid refl = refl

◾rid : ∀{i}{A : Set i}{x y : A}(p : x ≡ y) → (p ◾ refl) ≡ p
◾rid refl = refl

◾ass : ∀{i}{A : Set i}{a b c d : A}(p : a ≡ b)(q : b ≡ c)(r : c ≡ d)
     → (p ◾ (q ◾ r)) ≡ ((p ◾ q) ◾ r)
◾ass refl refl refl = refl

ap◾ : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a b c : A}(p : a ≡ b)(q : b ≡ c)
    → ap f (p ◾ q) ≡ (ap f p ◾ ap f q)
ap◾ f refl refl = refl

------------------------------------------------------------------------------
-- sigma
------------------------------------------------------------------------------

record Σ {i j} (A : Set i) (B : A → Set j) : Set (i ⊔ j) where
  constructor _,Σ_
  field
    proj₁ : A
    proj₂ : B proj₁

infixl 5 _,Σ_

open Σ public

aptot : ∀{i}{A : Set i}{B : A → Set}(f : (x : A) → B x){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
    → _≡_ {A = Σ Set λ X → X} (B a₀ ,Σ f a₀) (B a₁ ,Σ f a₁)
aptot f refl = refl

,Σ= : ∀{i j}{A : Set i}{B : A → Set j}{a a' : A}{b : B a}{b' : B a'}
     (p : a ≡ a') → b ≡[ ap B p ]≡ b' → _≡_ {A = Σ A B} (a ,Σ b) (a' ,Σ b')
,Σ= refl refl = refl

,Σ=0 : ∀{i j}{A : Set i}{B : A → Set j}{a a' : A}{b : B a}{b' : B a'}
    → _≡_ {A = Σ A B} (a ,Σ b) (a' ,Σ b') → a ≡ a'
,Σ=0 refl = refl

,Σ=1 : ∀{i j}{A : Set i}{B : A → Set j}{a a' : A}{b : B a}{b' : B a'}
      (p : (a ,Σ b) ≡ (a' ,Σ b')) → b ≡[ ap B (,Σ=0 p) ]≡ b'
,Σ=1 refl = refl

,Σ=η : ∀{i j}{A : Set i}{B : A → Set j}{w w' : Σ A B}
      (p : w ≡ w') → ,Σ= (,Σ=0 p) (,Σ=1 p) ≡ p
,Σ=η refl = refl

,Σ=β0 : ∀{i j}{A : Set i}{B : A → Set j}{a a' : A}{b : B a}{b' : B a'}
       (p : a ≡ a')(q : b ≡[ ap B p ]≡ b') → ,Σ=0 (,Σ= p q) ≡ p
,Σ=β0 refl refl = refl

,Σ=β1 : ∀{i j}{A : Set i}{B : A → Set j}{a a' : A}{b : B a}{b' : B a'}
       (p : a ≡ a')(q : b ≡[ ap B p ]≡ b')
     → ,Σ=1 (,Σ= p q) ≡[ ap (λ r → b ≡[ ap B r ]≡ b') (,Σ=β0 p q) ]≡ q
,Σ=β1 refl refl = refl

Σ= : ∀{i j}
     {A₀ A₁ : Set i}(A₂ : A₀ ≡ A₁)
     {B₀ : A₀ → Set j}{B₁ : A₁ → Set j}(B₂ : B₀ ≡[ ap (λ z → z → Set j) A₂ ]≡ B₁)
   → Σ A₀ B₀ ≡ Σ A₁ B₁
Σ= refl refl = refl

-- pointed types

,Σp= : {A₀ A₁ : Set}(A₂ : A₀ ≡ A₁){a₀ : A₀}{a₁ : A₁}(a₂ : a₀ ≡[ A₂ ]≡ a₁)
    → _≡_ {A = Σ Set λ X → X} (A₀ ,Σ a₀) (A₁ ,Σ a₁)
,Σp= A₂ {b₀}{b₁} b₂ = ,Σ= A₂ (coe (ap (λ z → b₀ ≡[ z ]≡ b₁) (apid A₂ ⁻¹)) b₂)

,Σp=1 : {A₀ A₁ : Set}{a₀ : A₀}{a₁ : A₁}
     → (p : (A₀ ,Σ a₀) ≡ (A₁ ,Σ a₁)) → a₀ ≡[ ,Σ=0 p ]≡ a₁
,Σp=1 refl = refl

-- nondependent

_×_ : ∀{i j} → Set i → Set j → Set (i ⊔ j)
A × B = Σ A λ _ → B

infix 4 _×_

,×= : ∀{i}{A B : Set i}{a a' : A}{b b' : B}
    → a ≡ a' → b ≡ b' → _≡_ {A = A × B} (a ,Σ b) (a' ,Σ b')
,×= refl refl = refl

------------------------------------------------------------------------------
-- top
------------------------------------------------------------------------------

record ⊤ : Set where
  constructor tt

set⊤ : {x y : ⊤}(p q : x ≡ y) → p ≡ q
set⊤ refl refl = refl

------------------------------------------------------------------------------
-- bottom
------------------------------------------------------------------------------

data ⊥ : Set where

⊥-elim : ∀{i}{A : Set i} → ⊥ → A
⊥-elim ()

------------------------------------------------------------------------------
-- disjoint union
------------------------------------------------------------------------------

data _⊎_ (A B : Set) : Set where
  inl : A → A ⊎ B
  inr : B → A ⊎ B
infixr 1 _⊎_

ind⊎ : {A B : Set}(P : A ⊎ B → Set) → ((a : A) → P (inl a)) → ((b : B) → P (inr b))
     → (w : A ⊎ B) → P w
ind⊎ P ca cb (inl a) = ca a
ind⊎ P ca cb (inr b) = cb b

------------------------------------------------------------------------------
-- function space
------------------------------------------------------------------------------

postulate
  funext  : ∀{i j}{A : Set i}{B : A → Set j}{f g : (x : A) → B x}
          → ((x : A) → f x  ≡ g x) → _≡_ f g

  funexti : ∀{i j}{A : Set i}{B : A → Set j}{f g : {x : A} → B x}
          → ((x : A) → f {x} ≡ g {x}) → _≡_ {A = {x : A} → B x} f g

  funextβ : ∀{i j}{A : Set i}{B : A → Set j}{f : (x : A) → B x}
          → funext {i}{j}{A}{B}{f}{f}(λ x → refl) ≡ refl

  funextiβ : ∀{i j}{A : Set i}{B : A → Set j}{f : {x : A} → B x}
           → funexti {i}{j}{A}{B}{f}{f}(λ x → refl) ≡ refl

→=
  : ∀{i j}
    {A₀ A₁ : Set i}(A₂ : A₀ ≡ A₁)
    {B₀ : A₀ → Set j}{B₁ : A₁ → Set j}(B₂ : {x₀ : A₀}{x₁ : A₁}(x₂ : x₀ ≡[ A₂ ]≡ x₁) → B₀ x₀ ≡ B₁ x₁)
  → ((x₀ : A₀) → B₀ x₀) ≡ ((x₁ : A₁) → B₁ x₁)
→= {i}{j}{A} refl {B₀}{B₁} B₂
  = J {A = A → Set j}{B₀}
      (λ {B₁} B₂ → ((x₀ : A) → B₀ x₀) ≡ ((x₁ : A) → B₁ x₁))
      refl
      {B₁} (funext (λ x → B₂ {x} refl))

→=''
  : ∀{i j}
    {A₀ A₁ : Set i}(A₂ : A₀ ≡ A₁)
    {B₀ B₁ : Set j}(B₂ : B₀ ≡ B₁)
  → (A₀ → B₀) ≡ (A₁ → B₁)
→='' refl refl = refl

→='
  : ∀{i j}
    {A₀ A₁ : Set i}(A₂ : A₀ ≡ A₁)
    {B₀ : A₀ → Set j}{B₁ : A₁ → Set j}(B₂ : B₀ ≡[ →='' A₂ refl ]≡ B₁)
  → ((x₀ : A₀) → B₀ x₀) ≡ ((x₁ : A₁) → B₁ x₁)
→=' refl refl = refl

→i='
  : ∀{i j}
    {A₀ A₁ : Set i}(A₂ : A₀ ≡ A₁)
    {B₀ : A₀ → Set j}{B₁ : A₁ → Set j}(B₂ : B₀ ≡[ →='' A₂ refl ]≡ B₁)
  → ({x₀ : A₀} → B₀ x₀) ≡ ({x₁ : A₁} → B₁ x₁)
→i=' refl refl = refl

→='''
  : ∀{i j}
    {A₀ A₁ : Set i}(A₂ : A₀ ≡ A₁)
    {B₀ : A₀ → A₀ → Set j}{B₁ : A₁ → A₁ → Set j}(B₂ : B₀ ≡[ →='' A₂ (→='' A₂ refl) ]≡ B₁)
  → ((x₀ : A₀) → B₀ x₀ x₀) ≡ ((x₁ : A₁) → B₁ x₁ x₁)
→=''' refl refl = refl

$T=
  : ∀{i j}
    {A₀ A₁ : Set i}(A₂ : A₀ ≡ A₁)
    {B₀ : A₀ → Set j}{B₁ : A₁ → Set j}(B₂ : {x₀ : A₀}{x₁ : A₁}(x₂ : x₀ ≡[ A₂ ]≡ x₁) → B₀ x₀ ≡ B₁ x₁)
    {a₀ : A₀}{a₁ : A₁}(a₂ : a₀ ≡[ A₂ ]≡ a₁)
  → B₀ a₀ ≡ B₁ a₁
$T= refl B₂ refl = B₂ refl

{-
funext''
  : ∀{i j}
    {A₀ A₁ : Set i}(A₂ : A₀ ≡ A₁)
    {B₀ : A₀ → Set j}{B₁ : A₁ → Set j}(B₂ : {x₀ : A₀}{x₁ : A₁}(x₂ : x₀ ≡[ A₂ ]≡ x₁) → B₀ x₀ ≡ B₁ x₁)
    {f₀ : (x : A₀) → B₀ x}{f₁ : (x : A₁) → B₁ x}
    (f₂ : {x₀ : A₀}{x₁ : A₁}(x₂ : x₀ ≡[ A₂ ]≡ x₁) → f₀ x₀ ≡[ B₂ x₂ ]≡ f₁ x₁)
  → f₀ ≡[ →= A₂ B₂ ]≡ f₁
funext'' {i}{j}{A} refl {B₀}{B₁} B₂ {f₀}{f₁} f₂
  = J {A = A → Set j}{B₀}
      (λ {B₁'} B₂' → {f₁ : (x : A) → B₁' x}(f₂ : {x₀ x₁ : A}(x₂ : x₀ ≡ x₁) → f₀ x₀ ≡[ ap B₀ x₂ ◾ ap (λ z → z x₁) B₂' ]≡ f₁ x₁) → f₀ ≡[ →= refl (λ {x₀}{x₁} x₂ → ap B₀ x₂ ◾ ap (λ z → z x₁) B₂') ]≡ f₁)
      {!!} {!!} {!!}
-}

------------------------------------------------------------------------------
-- lifting a type
------------------------------------------------------------------------------

data Lift {i j}(A : Set i) : Set (i ⊔ j) where
  lift : A → Lift A

------------------------------------------------------------------------------
-- top
------------------------------------------------------------------------------

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ
