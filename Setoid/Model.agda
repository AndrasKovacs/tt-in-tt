{-# OPTIONS --without-K #-}

module Setoid.Model where

open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.Func

open import Setoid.Setoid
open import Setoid.SetoidExp

d : Decl
d = record
  { Con = Setoid
  ; Ty  = FamSetoid
  ; Tms = _→S_
  ; Tm  = _→F_
  }

c : Core d
c = record
  { •     = ⊤S
  ; _,_   = _,S_
  ; _[_]T = _[_]TS
  ; id    = idS
  ; _∘_   = _∘S_
  ; ε     = εS
  ; _,s_  = λ σ {A} t → σ ,sS A ∶ t
  ; π₁    = λ {_}{_}{A} σ → π₁S A σ
  ; _[_]t = _[_]tS
  ; π₂    = λ {_}{_}{A} σ → π₂S A σ
  ; [id]T = [id]TS
  ; [][]T = λ {_}{_}{_}{A}{σ}{δ} → [][]TS {A = A}{σ}{δ}
  ; idl   = idlS
  ; idr   = idrS
  ; ass   = λ {_}{_}{_}{_}{σ}{δ}{ν} → assS {_}{_}{_}{_}{σ}{δ}{ν}
  ; ,∘    = λ {_}{_}{_}{δ}{σ}{A}{a} → ,∘S.ret {_}{_}{_}{δ}{σ}{A}{a}
  ; π₁β   = λ {_}{_}{A}{δ}{a} → π₁βS {_}{_}{A}{δ}{a}
  ; πη    = λ {_}{_}{A}{δ} → πηS {_}{_}{A}{δ}
  ; εη    = εη
  ; π₂β   = π₂βS
  }

f : Func c
f = record
  { Π     = ΠS
  ; Π[]   = λ {Γ}{Δ}{σ}{A}{B} → Π[]S.ret {Γ}{Δ}{σ}{A}{B}
  ; lam   = λ {Γ}{A}{B} t → lamS {Γ}{A}{B} t
  ; app   = λ {Γ}{A}{B} t → appS {Γ}{A}{B} t
  ; lam[] = λ {Γ}{Δ}{σ}{A}{B}{t} → lam[]S.ret {Γ}{Δ}{σ}{A}{B}{t}
  ; Πβ    = λ {Γ}{A}{B}{t} → ΠβS {Γ}{A}{B}{t}
  ; Πη    = λ {Γ}{A}{B}{t} → ΠηS {Γ}{A}{B}{t}
  }
