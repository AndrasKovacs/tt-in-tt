module Setoid.SetoidExp where

open import lib
open import JM

open import Setoid.Setoid

-- TODO: move to stdlib

proj₂≃ : ∀{i j}{A₀ A₁ : Set i}(A₂ : A₀ ≡ A₁)
         {B₀ : A₀ → Set j}{B₁ : A₁ → Set j}(B₂ : B₀ ≃ B₁)
         {w₀ : Σ A₀ B₀}{w₁ : Σ A₁ B₁}(w₂ : w₀ ≃ w₁)
       → proj₂ w₀ ≃ proj₂ w₁
proj₂≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

proj₂coe : {A₀ A₁ : Set}(A₂ : A₀ ≡ A₁)
           {B₀ : A₀ → Set}{B₁ : A₁ → Set}(B₂ : B₀ ≡[ ap (λ z → z → Set) A₂ ]≡ B₁)
           (p : Σ A₀ B₀ ≡ Σ A₁ B₁)
           {a₀ : A₀}{b₀ : B₀ a₀}
         → b₀ ≃ proj₂ (coe p (a₀ ,Σ b₀))
proj₂coe refl refl refl = refl ,≃ refl

APP≃'
  : ∀{i j}{A : Set i}{B₀ B₁ : A → Set j}(B₂ : B₀ ≡ B₁)
    {f₀ : (x : A) → B₀ x}{f₁ : (x : A) → B₁ x}(f₂ : f₀ ≃ f₁)
  → {a₀ a₁ : A}(a₂ : a₀ ≡ a₁) → f₀ a₀ ≃ f₁ a₁
APP≃' refl (refl ,≃ refl) refl = refl ,≃ refl

APP≃''
  : ∀{i j}{A₀ A₁ : Set i}
    {B₀ : A₀ → Set j}{B₁ : A₁ → Set j}(B₂ : B₀ ≃ B₁)
    {f₀ : (x : A₀) → B₀ x}{f₁ : (x : A₁) → B₁ x}(f₂ : f₀ ≃ f₁)
  → {a₀ : A₀}{a₁ : A₁}(a₂ : a₀ ≃ a₁) → f₀ a₀ ≃ f₁ a₁
APP≃'' (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

-- laws

abstract
  coeF⁻¹[]
    : {Γ Δ : Setoid}{A : FamSetoid Δ}
      {σ : Γ →S Δ}{γ δ : ∣ Γ ∣}{a : ∣ A [ σ ]TS ∣F δ}
      (p : Γ ∶ γ ~ δ)
    → coeF⁻¹ A (resp σ p) a ≡ coeF⁻¹ (A [ σ ]TS) p a
  coeF⁻¹[] {Γ}{Δ}{A}{σ}{γ}{δ}{a} p = from≃ (coeF≃ {Δ}{A} refl refl (to≃ (prop Δ _ _)) r̃)

abstract
  cohF⁻¹[]
    : {Γ Δ : Setoid}{A : FamSetoid Δ}
      {σ : Γ →S Δ}{γ δ : ∣ Γ ∣}{a : ∣ A [ σ ]TS ∣F δ}
      (p : Γ ∶ γ ~ δ)
    → cohF⁻¹ A (resp σ p) a ≃ cohF⁻¹ (A [ σ ]TS) p a
  cohF⁻¹[] {Γ}{Δ}{A}{σ}{γ}{δ}{a} p
    = uncoe (∶F~=' A (prop Δ _ _)) ⁻¹̃
    ◾̃ symF≃ A refl refl (to≃ (prop Δ _ _)) r̃ (to≃ (coeF⁻¹[] {Γ}{Δ}{A}{σ}{γ}{δ}{a} p)) (cohF A (sym Δ (resp σ p)) a)(cohF (A [ σ ]TS) (sym Γ p) a) 
    ◾̃ uncoe (ap (λ z → A ∶ z F coeF A (resp σ (sym Γ p)) a ~ a) (prop Δ (sym Δ (resp σ (sym Γ p))) (resp σ (sym Γ (sym Γ p)))))
    ◾̃ uncoe (∶F~=' (A [ σ ]TS) (prop Γ _ _))

_^S_ : {Γ Δ : Setoid}(σ : Γ →S Δ)(A : FamSetoid Δ) → (Γ ,S A [ σ ]TS) →S (Δ ,S A)
σ ^S A = (σ ∘S π₁S (A [ σ ]TS) idS) ,sS A ∶ coe (Γ→F= ([][]TS {A = A}{π₁S (A [ σ ]TS) idS}{σ})) (π₂S (A [ σ ]TS) idS)

infixl 5 _^S_

abstract
  proj₂^
    : {Γ Δ : Setoid}{A : FamSetoid Δ}
      {σ : Γ →S Δ}{γ : ∣ Γ ∣}{a : ∣ A [ σ ]TS ∣F γ}
    → proj₂ (map (σ ^S A) (γ ,Σ a)) ≡ a
  proj₂^ {Γ}{Δ}{A}{σ}{γ}{a}
    = mapFcoe ([][]TS {A = A}{π₁S (A [ σ ]TS) idS}{σ}){_}{γ ,Σ a}
    ◾ UIP' (∣∣F= ([][]TS {A = A}{π₁S (A [ σ ]TS) idS}{σ}) refl) refl refl ⁻¹

abstract
  map^
    : {Γ Δ : Setoid}{A : FamSetoid Δ}
      {σ : Γ →S Δ}{δ : ∣ Γ ∣}{a : ∣ A [ σ ]TS ∣F δ}
    → map σ δ ,Σ a ≡ map (σ ^S A) (δ ,Σ a)
  map^ {Γ}{Δ}{A}{σ}{γ}{a} = ,Σ= refl (proj₂^ {Γ}{Δ}{A}{σ}{γ}{a} ⁻¹)

abstract
  resp^
    : {Γ Δ : Setoid}{A : FamSetoid Δ}
      {σ : Γ →S Δ}{γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)
      {a : ∣ A [ σ ]TS ∣F γ}{a' : ∣ A [ σ ]TS ∣F δ}(r : A [ σ ]TS ∶ p F a ~ a')
    → _≃_
        {A = (Δ ,S A) ∶ map σ γ ,Σ a ~ (map σ δ ,Σ a')}
        {B = (Δ ,S A) ∶ map (σ ^S A) (γ ,Σ a) ~ map (σ ^S A) (δ ,Σ a')}
        (resp σ p ,Σ r)
        (resp (σ ^S A) (p ,Σ r))
  resp^ {Γ}{Δ}{A}{σ}{γ}{δ} p {a}{a'} r
    = ,Σ≃≃ (funext≃' λ p → to≃ (∶F~=
                                  {Δ}{A} refl refl refl {p} refl
                                  (proj₂^ {Γ}{Δ}{A}{σ}{γ}{a} ⁻¹)
                                  (proj₂^ {Γ}{Δ}{A}{σ}{δ}{a'} ⁻¹)))
           r̃
           (respFcoe' ([][]TS {A = A}{π₁S (A [ σ ]TS) idS}{σ}) (p ,Σ r) ⁻¹̃)

abstract
  resp^'
    : {Γ Δ : Setoid}{A : FamSetoid Δ}
      {σ : Γ →S Δ}{γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)
      {a : ∣ A [ σ ]TS ∣F γ}{a' : ∣ A [ σ ]TS ∣F δ}(r : A [ σ ]TS ∶ p F a ~ a')
    → (resp σ p ,Σ r)
    ≡[ ∶~= {Δ ,S A} refl (map^ {Γ}{Δ}{A}{σ}{γ}{a}) (map^ {Γ}{Δ}{A}{σ}{δ}{a'}) ]≡
      (resp (σ ^S A) (p ,Σ r))
  resp^' {Γ}{Δ}{A}{σ}{γ}{δ} p {a}{a'} r
    = from≃ ( uncoe (∶~= {Δ ,S A} refl (map^ {Γ}{Δ}{A}{σ}{γ}{a}) (map^ {Γ}{Δ}{A}{σ}{δ}{a'})) ⁻¹̃
            ◾̃ resp^ {Γ}{Δ}{A}{σ}{γ}{δ} p {a}{a'} r)

-- setoid exponential

SetoidExp : {Γ : Setoid}(A : FamSetoid Γ)(B : FamSetoid (Γ ,S A))(γ : ∣ Γ ∣) → Set
SetoidExp {Γ} A B γ
  = Σ ((x : ∣ A ∣F γ) → ∣ B ∣F (γ ,Σ x)) λ mapE
    → ({x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE x ~ mapE y)

mapE
  : {Γ : Setoid}(A : FamSetoid Γ)(B : FamSetoid (Γ ,S A)){γ : ∣ Γ ∣}
    (w : SetoidExp A B γ) → ((x : ∣ A ∣F γ) → ∣ B ∣F (γ ,Σ x))
mapE _ _ = proj₁
respE
  : {Γ : Setoid}(A : FamSetoid Γ)(B : FamSetoid (Γ ,S A)){γ : ∣ Γ ∣}
    (w : SetoidExp A B γ) → ({x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE A B w x ~ mapE A B w y)
respE _ _ = proj₂

respET=
  : {Γ : Setoid}(A : FamSetoid Γ)(B : FamSetoid (Γ ,S A)){γ : ∣ Γ ∣}
    {mapE₀ mapE₁ : (x : ∣ A ∣F γ) → ∣ B ∣F (γ ,Σ x)}(mapE₂ : mapE₀ ≡ mapE₁)
  → ({x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE₀ x ~ mapE₀ y)
  ≡ ({x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE₁ x ~ mapE₁ y)
respET= _ _ refl = refl

mkSetoidExp='
  : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,S A)}{γ : ∣ Γ ∣}
    {mapE₀ mapE₁ : (x : ∣ A ∣F γ) → ∣ B ∣F (γ ,Σ x)}(mapE₂ : mapE₀ ≡ mapE₁)
    {respE₀ : {x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE₀ x ~ mapE₀ y}
    {respE₁ : {x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE₁ x ~ mapE₁ y}
    (respE₂ : (λ {x}{y} p → respE₀ {x}{y} p) ≡[ respET= A B mapE₂ ]≡ (λ {x}{y} p → respE₁ {x}{y} p))
  → _≡_ {A = SetoidExp A B γ} (mapE₀ ,Σ respE₀) (mapE₁ ,Σ respE₁)
mkSetoidExp=' refl refl = refl

mkSetoidExp=
  : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,S A)}{γ : ∣ Γ ∣}
    {mapE₀ mapE₁ : (x : ∣ A ∣F γ) → ∣ B ∣F (γ ,Σ x)}(mapE₂ : mapE₀ ≡ mapE₁)
    {respE₀ : {x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE₀ x ~ mapE₀ y}
    {respE₁ : {x y : ∣ A ∣F γ}(p : A ∶ ref Γ γ F x ~ y) → B ∶ ref Γ γ ,Σ p F mapE₁ x ~ mapE₁ y}
  → _≡_ {A = SetoidExp A B γ} (mapE₀ ,Σ respE₀) (mapE₁ ,Σ respE₁)
mkSetoidExp= {Γ}{A}{B}{γ} {mapE} refl {respE₀}{respE₁}
  = mkSetoidExp='
      {Γ}{A}{B}{γ} refl
      (funexti λ a → funexti λ a' → funext λ r
      → UIP'' (ap (λ z → B ∶ z F mapE a ~ mapE a')
                  (prop (Γ ,S A) (ref Γ γ ,Σ r) (ref Γ γ ,Σ r)))
              (propF B (respE₀ r)(respE₁ r)))

ΠS : {Γ : Setoid}(A : FamSetoid Γ)(B : FamSetoid (Γ ,S A)) → FamSetoid Γ
ΠS {Γ} A B = record
  { ∣_∣F_ = SetoidExp A B
  ; _∶_F_~_ = λ {γ}{δ} p f g
              → {x : ∣ A ∣F γ}{y : ∣ A ∣F δ}(r : A ∶ p F x ~ y) → B ∶ p ,Σ r F mapE A B f x ~ mapE A B g y
  ; propF
      = λ {γ}{δ}{p}{q}{f}{g} re se → funexti λ a → funexti λ a' → funext λ s
        → UIP'' (ap (λ z → B ∶ z F mapE A B f a ~ mapE A B g a') (prop (Γ ,S A) (q ,Σ s) (q ,Σ s)))
                (propF B (coe (ap (λ z → {x : ∣ A ∣F γ}{y : ∣ A ∣F δ}(r : A ∶ z F x ~ y) → B ∶ z ,Σ r F mapE A B f x ~ mapE A B g y) (prop Γ p q)) re s)
                         (se s))
  ; coeF
      = λ { {γ}{δ} p (mape ,Σ respe)
        → (λ a
           → coeF B (p ,Σ cohF⁻¹ A p a) (mape (coeF⁻¹ A p a)))
       ,Σ (λ {a}{a'} r
           → coe (∶F~=' B (prop (Γ ,S A) _ _))
                 (transF B
                   (symF B (cohF B (p ,Σ cohF⁻¹ A p a) (mape (coeF⁻¹ A p a))))
                   (transF B
                     (respe (coe (∶F~=' A (prop Γ _ _))
                                 (transF A (transF A (symF A (cohF A (sym Γ p) a)) r) (cohF A (sym Γ p) a'))))
                     (cohF B (p ,Σ cohF⁻¹ A p a') (mape (coeF⁻¹ A p a')))))) }
  ; cohF    = λ { {γ}{δ} p (mape ,Σ respe){a}{a'} r
              → coe
                  (∶F~=' B (prop (Γ ,S A) _ _))
                  (transF B
                    (respe (coe (∶F~=' A (prop Γ _ _)) (transF A r (cohF A (sym Γ p) a'))))
                    (cohF B (p ,Σ cohF⁻¹ A p a') (mape (coeF⁻¹ A p a')))) }
  ; refF    = respE A B
  ; symF    = λ {γ}{δ}{p}{f}{g} re {a}{a'} r
              → coe (∶F~=' B (prop (Γ ,S A) _ _))
                    (symF B (re (coe (∶F~=' A (prop Γ _ _)) (symF A r))))
  ; transF
      = λ { {γ}{γ'}{γ''}{p}{q}{mape ,Σ respe}{mape' ,Σ respe'}{mape'' ,Σ respe''} re se {a}{a''} r
        → coe (∶F~=' B (prop (Γ ,S A) _ _))
              (transF B
                 (re (cohF A p a))
                 (se (coe (∶F~=' A (prop Γ _ _))
                          (transF A (symF A (cohF A p a)) r)))) }
  }

module Π[]S
  {Γ Δ : Setoid}{σ : Γ →S Δ}{A : FamSetoid Δ}{B : FamSetoid (Δ ,S A)}
  where
    abstract
      Bσ₂ : {γ : ∣ Γ ∣}{a : ∣ A ∣F map σ γ}
          → ∣ B ∣F (map σ γ ,Σ a) ≡ ∣ B [ σ ^S A ]TS ∣F (γ ,Σ a)
      Bσ₂ {γ}{a} = ∣∣F= {A₀ = B} refl (map^ {Γ}{Δ}{A}{σ}{γ}{a})

    abstract
      AB₂ : (γ : ∣ Γ ∣)
          → ((x₀ : ∣ A ∣F map σ γ) → ∣ B ∣F (map σ γ ,Σ x₀))
          ≡ ((x₁ : ∣ A ∣F map σ γ) → ∣ B [ σ ^S A ]TS ∣F (γ ,Σ x₁))
      AB₂ γ = →= refl λ r → ∣∣F= {A₀ = B} refl (,Σ= {a = map σ γ} refl (r ◾ from≃ (mapFcoe' ([][]TS {A = A}) ⁻¹̃)))

    abstract
      mapE₂
        : (γ : ∣ Γ ∣)
        → _≃_ {A = (mapE₁ : (x₀ : ∣ A ∣F map σ γ) → ∣ B ∣F (map σ γ ,Σ x₀)) → _}
              {B = (mapE₁ : (x₁ : ∣ A ∣F map σ γ) → ∣ B [ σ ^S A ]TS ∣F (γ ,Σ x₁)) → _}
              (λ mapE₁ → {x y : ∣ A ∣F map σ γ}(p : A ∶ ref Δ (map σ γ) F x ~ y) → B ∶ ref Δ (map σ γ) ,Σ p F mapE₁ x ~ mapE₁ y)
              (λ mapE₁ → {x y : ∣ A [ σ ]TS ∣F γ}(p : A [ σ ]TS ∶ ref Γ γ F x ~ y) → B [ σ ^S A ]TS ∶ ref Γ γ ,Σ p F mapE₁ x ~ mapE₁ y)
      mapE₂ γ
        = funext≃ (AB₂ γ) (λ {f₀}{f₁} f₂ → to≃ (→i=' refl (funext λ a → →i=' refl (funext λ a' → →= (∶F~=' A (prop Δ _ _))
            (λ {r}{s} _ → ∶F~= {A₀ = B} refl
                               (map^ {Γ}{Δ}{A}{σ}{γ}{a})
                               (map^ {Γ}{Δ}{A}{σ}{γ}{a'})
                               {ref Δ (map σ γ) ,Σ r}
                               {resp (σ ^S A) (ref Γ γ ,Σ s)}
                               (prop (Δ ,S A) _ _)
                               (from≃ (uncoe Bσ₂ ⁻¹̃ ◾̃ APP≃ (funext λ a → Bσ₂ {γ}{a}) f₂ {a}))
                               (from≃ (uncoe Bσ₂ ⁻¹̃ ◾̃ APP≃ (funext λ a → Bσ₂ {γ}{a}) f₂ {a'})))))))

    abstract
      ∣∣₂ : (λ γ → ∣ ΠS A B ∣F map σ γ) ≡ SetoidExp (A [ σ ]TS) (B [ σ ^S A ]TS)
      ∣∣₂ = funext λ γ → Σ≃ (AB₂ γ) (mapE₂ γ)

    abstract
      ∶F~₂ : (λ {γ} {δ} p → _∶_F_~_ (ΠS A B) (resp σ p))
           ≡[ ∶F~T= {Γ} ∣∣₂ ]≡
             (λ {γ} {δ} p f g →
               {x : ∣ A [ σ ]TS ∣F γ} {y : ∣ A [ σ ]TS ∣F δ}
               (r : A [ σ ]TS ∶ p F x ~ y) →
               B [ σ ^S A ]TS ∶ p ,Σ r F mapE (A [ σ ]TS) (B [ σ ^S A ]TS) f x ~
               mapE (A [ σ ]TS) (B [ σ ^S A ]TS) g y)
      ∶F~₂ = from≃ ( uncoe (∶F~T= {Γ} ∣∣₂) ⁻¹̃
                   ◾̃ funexti≃' λ γ → funexti≃' λ δ → funext≃' λ p → funext≃ (ap (λ z → z γ) ∣∣₂)
                       λ {f₀}{f₁} f₂ → funext≃ (ap (λ z → z δ) ∣∣₂) λ {g₀}{g₁} g₂
                       → to≃ (→i=' refl (funext λ a → →i=' refl (funext λ a' → →=' refl (funext λ r
                       → ∶F~= {A₀ = B} refl (map^ {Γ}{Δ}{A}{σ}{γ}{a}) (map^ {Γ}{Δ}{A}{σ}{δ}{a'}) {resp σ p ,Σ r}{resp σ p ,Σ _}
                              (from≃ ( uncoe (∶~= {Δ ,S A} refl (map^ {Γ}{Δ}{A}{σ}{γ}{a}) (map^ {Γ}{Δ}{A}{σ}{δ}{a'})) ⁻¹̃
                                     ◾̃ resp^ {Γ}{Δ}{A}{σ} p r))
                              {proj₁ f₀ a}{proj₁ f₁ a}
                              (from≃ ( uncoe (∣∣F= refl (map^ {Γ}{Δ}{A}{σ}{γ}{a})) ⁻¹̃
                                     ◾̃ APP≃ (funext λ a → Bσ₂ {γ}{a})
                                            (proj₁≃ (AB₂ γ) (mapE₂ γ) f₂) {a}))
                              {proj₁ g₀ a'}{proj₁ g₁ a'}
                              (from≃ ( uncoe (∣∣F= refl (map^ {Γ}{Δ}{A}{σ}{δ}{a'})) ⁻¹̃
                                     ◾̃ APP≃ (funext λ a' → Bσ₂ {δ}{a'})
                                            (proj₁≃ (AB₂ δ) (mapE₂ δ) g₂) {a'})))))))

    abstract
      resp₂ : {γ δ : ∣ Γ ∣}
              (a : ∣ A ∣F map σ δ)
              (p : Γ ∶ γ ~ δ)
            → _≃_ {A = (Δ ,S A) ∶ map σ γ ,Σ coeF⁻¹ A (resp σ p) a ~ (map σ δ ,Σ a)}
                  (resp σ p ,Σ cohF⁻¹ A (resp σ p) a)
                  (resp (σ ^S A) (p ,Σ cohF⁻¹ (A [ σ ]TS) p a))
      resp₂ {γ}{δ} a p  -- TODO: replace this with usage of prop (Δ ,S A)
        = resp^ {Γ}{Δ}{A}{σ}{γ}{δ} p (cohF⁻¹ A (resp σ p) a)
        ◾̃ resp≃ {σ₀ = σ ^S A} refl (,Σ= refl (coeF⁻¹[] {A = A}{σ}{a = a} p)) refl
                {p ,Σ cohF⁻¹ A (resp σ p) a}{p ,Σ cohF⁻¹ (A [ σ ]TS) p a}
                ((∶~= {Γ ,S A [ σ ]TS} refl
                      {γ ,Σ coeF⁻¹ A (resp σ p) a}
                      {γ ,Σ coeF⁻¹ (A [ σ ]TS) p a}
                      (,Σ= refl (coeF⁻¹[] {A = A}{σ}{a = a} p))
                      {δ ,Σ a}
                      refl)
                ,≃ (prop (Γ ,S A [ σ ]TS) _ _))

    abstract
      proj₁f₂
        : {γ δ  : ∣ Γ ∣}{f₀ : ∣ ΠS A B ∣F map σ γ}{f₁ : SetoidExp (A [ σ ]TS) (B [ σ ^S A ]TS) γ}(f₂ : f₀ ≃ f₁)
          (a : ∣ A ∣F map σ δ)(p : Γ ∶ γ ~ δ)
        → proj₁ f₀ (coeF⁻¹ A (resp σ p) a)
        ≃ proj₁ f₁ (coeF⁻¹ (A [ σ ]TS) p a)
      proj₁f₂ {γ}{δ}{f₀}{f₁} f₂ a p
        = APP≃' {B₀ = λ x → ∣ B ∣F (map σ γ ,Σ x)}{λ x → ∣ B [ σ ^S A ]TS ∣F (γ ,Σ x)}
                (funext λ x → Bσ₂ {γ}{x})
                {proj₁ f₀}{proj₁ f₁}
                (proj₁≃ {A₀ = (x : ∣ A ∣F map σ γ) → ∣ B ∣F (map σ γ ,Σ x)}
                        {(x : ∣ A [ σ ]TS ∣F γ) → ∣ B [ σ ^S A ]TS ∣F (γ ,Σ x)}
                        (AB₂ γ)
                        {λ f → {x y : ∣ A ∣F map σ γ}  (q : A ∶ ref Δ (map σ γ) F x ~ y) → B ∶ ref Δ (map σ γ) ,Σ q F f x ~ f y}
                        {λ f → {x y : ∣ A [ σ ]TS ∣F γ}(q : A [ σ ]TS ∶ ref Γ γ F x ~ y) → B [ σ ^S A ]TS ∶ ref Γ γ ,Σ q F f x ~ f y}
                        (mapE₂ γ)
                        f₂)
                {coeF⁻¹ A (resp σ p) a}
                {coeF⁻¹ (A [ σ ]TS) p a}
                (coeF⁻¹[] {A = A}{σ}{a = a} p)

    abstract
      coeF₂'
        : {γ δ  : ∣ Γ ∣}{f₀ : ∣ ΠS A B ∣F map σ γ}{f₁ : SetoidExp (A [ σ ]TS) (B [ σ ^S A ]TS) γ}(f₂ : f₀ ≃ f₁)
          (a : ∣ A ∣F map σ δ)(p : Γ ∶ γ ~ δ)
        → coeF B (resp σ p ,Σ cohF⁻¹ A (resp σ p) a) (proj₁ f₀ (coeF⁻¹ A (resp σ p) a))
        ≡[ ∣∣F= {A₀ = B} refl (map^ {Γ}{Δ}{A}{σ}{δ}{a}) ]≡
          coeF B (resp (σ ^S A) (p ,Σ cohF⁻¹ (A [ σ ]TS) p a)) (proj₁ f₁ (coeF⁻¹ (A [ σ ]TS) p a))
      coeF₂' {γ}{δ}{f₀}{f₁} f₂ a p
        = from≃ ( uncoe (∣∣F= refl (map^ {Γ}{Δ}{A}{σ}{δ}{a})) ⁻¹̃
                ◾̃ coeF≃ {_}{B}
                        (map^ {Γ}{Δ}{A}{σ}{γ}{coeF⁻¹ A (resp σ p) a} ◾ ap (λ z → map (σ ^S A) (γ ,Σ z)) (coeF⁻¹[] {Γ}{Δ}{A}{σ}{γ} p))
                        (map^ {Γ}{Δ}{A}{σ}{δ}{a})
                        {resp σ p ,Σ cohF⁻¹ A (resp σ p) a}
                        {resp (σ ^S A) (p ,Σ cohF⁻¹ (A [ σ ]TS) p a)}
                        (resp₂ a p)
                        {proj₁ f₀ (coeF⁻¹ A (resp σ p) a)}
                        {proj₁ f₁ (coeF⁻¹ (A [ σ ]TS) p a)}
                        (proj₁f₂ f₂ a p))

    abstract
      coeF₂≃
        : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)
        → coeF (ΠS A B [ σ ]TS) p ≃ coeF (ΠS (A [ σ ]TS) (B [ σ ^S A ]TS)) p
      coeF₂≃ {γ}{δ} p
        = funext≃ (ap (λ z → z γ) ∣∣₂) (λ {f₀}{f₁} f₂
            → ,Σ≃≃ (mapE₂ δ)
                   (funext≃' λ a
                     → coeF≃
                         {Δ ,S A}{B}
                         (map^ {Γ}{Δ}{A}{σ}{γ}{coeF⁻¹ A (resp σ p) a} ◾ ap (λ z → map (σ ^S A) (γ ,Σ z)) (coeF⁻¹[] {Γ}{Δ}{A}{σ}{γ} p))
                         (map^ {Γ}{Δ}{A}{σ}{δ}{a})
                         (resp^ {Γ}{Δ}{A}{σ} p (cohF⁻¹ A (resp σ p) a) ◾̃ ap≃ {B = λ z → A ∶ resp σ p F z ~ a}(λ z → resp (σ ^S A) (p ,Σ z))(coeF⁻¹[] {Γ}{Δ}{A}{σ}{a = a} p) (cohF⁻¹[] {Γ}{Δ}{A}{σ}{a = a} p))
                         (APP≃'' (funext≃' (λ x → to≃ (∣∣F= {A₀ = B} refl (map^ {Γ}{Δ}{A}{σ}{γ}{x}))))
                              {f₀ = proj₁ f₀}{f₁ = proj₁ f₁}
                              (proj₁≃ (AB₂ γ) (mapE₂ γ) f₂)
                              (to≃ (coeF⁻¹[] {Γ}{Δ}{A}{σ}{a = a} p))))
                   (funexti≃' λ a₀ → funexti≃' λ a₁ → funext≃ (∶F~=' A (prop Δ _ _)) λ {p₀}{p₁} p₂ →
                        ∶F~= {A₀ = B} refl
                             (map^ {Γ}{Δ}{A}{σ}{δ}{a₀})
                             (map^ {Γ}{Δ}{A}{σ}{δ}{a₁})
                             {ref Δ (map σ δ) ,Σ p₀}
                             {resp (σ ^S A) (ref Γ δ ,Σ p₁)}
                             (prop (Δ ,S A) _ _)
                             (coeF₂' f₂ a₀ p)
                             (coeF₂' f₂ a₁ p)
                     ,≃ from≃ (propF' (B [ σ ^S A ]TS) _ _)))

    abstract
      coeF₂
        : (λ {γ}{δ} p → coeF (ΠS A B) (resp σ p))
        ≡[ coeFT= {Γ} ∣∣₂ ∶F~₂ ]≡
          (λ {γ}{δ} p → coeF (ΠS (A [ σ ]TS) (B [ σ ^S A ]TS)) p)
      coeF₂ = from≃ ( uncoe (coeFT= {Γ} ∣∣₂ ∶F~₂) ⁻¹̃
                    ◾̃ funexti≃' λ γ → funexti≃' λ δ → funext≃' λ p → coeF₂≃ p)

    abstract
      cohF₂
        : (λ {γ}{δ} p → cohF (ΠS A B) (resp σ p))
        ≡[ cohFT= {Γ} ∣∣₂ ∶F~₂ coeF₂ ]≡
          (λ {γ}{δ} p → cohF (ΠS (A [ σ ]TS) (B [ σ ^S A ]TS)) p)
      cohF₂
        = from≃
            ( uncoe (cohFT= {Γ} ∣∣₂ ∶F~₂ coeF₂) ⁻¹̃
            ◾̃ funexti≃' λ γ → funexti≃' λ δ → funext≃' λ p → funext≃ (ap (λ z → z γ) ∣∣₂) λ {f₀}{f₁} f₂
            →  (→i≃ {A₀ = ∣ A ∣F map σ γ} refl (to≃ (funext λ a₀ → →i≃ refl (to≃ (funext λ a₁
                  → →≃ refl (to≃ (funext λ q
                  → ∶F~= {A₀ = B} refl (map^ {A = A}{σ}{γ}{a₀}) (map^ {A = A}{σ}{δ}{a₁})
                         {resp σ p ,Σ q}{resp (σ ^S A) (p ,Σ q)}
                         (prop (Δ ,S A) _ _)
                         {mapE A B f₀ a₀}{mapE (A [ σ ]TS) (B [ σ ^S A ]TS) f₁ a₀}
                         (from≃ ( uncoe (∣∣F= {A₀ = B} refl (map^ {A = A}{σ}{γ}{a₀})) ⁻¹̃
                                ◾̃ APP≃
                                    {B₀ = λ x → ∣ B ∣F (map σ γ ,Σ x)}{λ x → ∣ B [ σ ^S A ]TS ∣F (γ ,Σ x)}
                                    (funext λ x → Bσ₂ {γ}{x})
                                    {proj₁ f₀}{proj₁ f₁}
                                    (proj₁≃
                                       {A₀ = (x : ∣ A ∣F map σ γ) → ∣ B ∣F (map σ γ ,Σ x)}
                                       {(x : ∣ A [ σ ]TS ∣F γ) → ∣ B [ σ ^S A ]TS ∣F (γ ,Σ x)}
                                       (AB₂ γ)
                                       {λ f → {x y : ∣ A ∣F map σ γ}  (q : A ∶ ref Δ (map σ γ) F x ~ y) → B ∶ ref Δ (map σ γ) ,Σ q F f x ~ f y}
                                       {λ f → {x y : ∣ A [ σ ]TS ∣F γ}(q : A [ σ ]TS ∶ ref Γ γ F x ~ y) → B [ σ ^S A ]TS ∶ ref Γ γ ,Σ q F f x ~ f y}
                                       (mapE₂ γ)
                                       f₂)
                                    {a₀}))
                         {mapE A B (coeF (ΠS A B) (resp σ p) f₀) a₁}
                         {mapE (A [ σ ]TS) (B [ σ ^S A ]TS) (coeF (ΠS (A [ σ ]TS) (B [ σ ^S A ]TS)) p f₁) a₁}
                         (from≃ ( uncoe (∣∣F= {A₀ = B} refl (map^ {A = A}{σ}{δ}{a₁})) ⁻¹̃
                                ◾̃ APP≃
                                    {B₀ = λ x → ∣ B ∣F (map σ δ ,Σ x)}{λ x → ∣ B [ σ ^S A ]TS ∣F (δ ,Σ x)}
                                    (funext λ x → Bσ₂ {δ}{x})
                                    {mapE A B (coeF (ΠS A B [ σ ]TS) p f₀)}
                                    {mapE (A [ σ ]TS) (B [ σ ^S A ]TS) (coeF (ΠS (A [ σ ]TS) (B [ σ ^S A ]TS)) p f₁)}
                                    (proj₁≃
                                       {A₀ = (x : ∣ A ∣F map σ δ) → ∣ B ∣F (map σ δ ,Σ x)}
                                       {(x : ∣ A [ σ ]TS ∣F δ) → ∣ B [ σ ^S A ]TS ∣F (δ ,Σ x)}
                                       (AB₂ δ)
                                       (mapE₂ δ)
                                       {coeF (ΠS A B [ σ ]TS) p f₀}
                                       {coeF (ΠS (A [ σ ]TS) (B [ σ ^S A ]TS)) p f₁}
                                       (APP≃''
                                          {A₀ = ∣ ΠS A B [ σ ]TS ∣F γ}{∣ ΠS (A [ σ ]TS) (B [ σ ^S A ]TS) ∣F γ}
                                          {λ _ → ∣ ΠS A B [ σ ]TS ∣F δ}
                                          {λ _ → ∣ ΠS (A [ σ ]TS) (B [ σ ^S A ]TS) ∣F δ}
                                          (funext≃ (ap (λ z → z γ) ∣∣₂) (λ _ → to≃ (ap (λ z → z δ) ∣∣₂)))
                                          (coeF₂≃ p)
                                          f₂))
                                    {a₁})))))))))
            ,≃ propF'' (ΠS (A [ σ ]TS) (B [ σ ^S A ]TS)) {p = p}{f₁}{coeF (ΠS (A [ σ ]TS) (B [ σ ^S A ]TS)) p f₁} _ _)
         
    abstract
      ret : ΠS A B [ σ ]TS ≡ ΠS (A [ σ ]TS) (B [ σ ^S A ]TS)
      ret = mkFamSetoid=
          ∣∣₂
          ∶F~₂
          coeF₂
          cohF₂

lamS : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,S A)} → (Γ ,S A) →F B → Γ →F ΠS A B
lamS {Γ}{A}{B} t
  = record
      { mapF
          = λ γ →  (λ x → mapF t (γ ,Σ x))
                ,Σ (λ {x}{y} p → respF t (ref Γ γ ,Σ p))
      ; respF = λ p q → respF t (p ,Σ q)
      }

appS : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,S A)} → Γ →F ΠS A B → (Γ ,S A) →F B
appS {Γ}{A}{B} f
  = record
      { mapF  = λ { (γ ,Σ x) → mapE A B (mapF f γ) x }
      ; respF = λ { (p ,Σ q) → respF f p q }
      }

module lam[]S
  {Γ Δ : Setoid}{σ : Γ →S Δ}{A : FamSetoid Δ}{B : FamSetoid (Δ ,S A)}{t :(Δ ,S A) →F B}
  where
    abstract
      mapF₂
        : mapF (coe (ap (_→F_ Γ) (Π[]S.ret {Γ}{Δ}{σ}{A}{B})) (lamS t [ σ ]tS))
        ≡ (λ γ → (λ x → mapF (t [ σ ^S A ]tS) (γ ,Σ x)) ,Σ (λ {x} {y} p → respF (t [ σ ^S A ]tS) (ref Γ γ ,Σ p)))
      mapF₂
        = funext λ γ → from≃ ( mapFcoe' (Π[]S.ret {Γ}{Δ}{σ}{A}{B}){lamS t [ σ ]tS}{γ}
                             ◾̃ ,Σ≃≃ {A₀ = (x : ∣ A ∣F (map σ γ)) → ∣ B ∣F (map σ γ ,Σ x)}
                                     {(x : ∣ A [ σ ]TS ∣F γ) → ∣ B [ σ ^S A ]TS ∣F (γ ,Σ x)}
                                     (Π[]S.mapE₂ {Γ}{Δ}{σ}{A}{B} γ)
                                     {λ x → mapF t (map σ γ ,Σ x)}
                                     {λ x → mapF (t [ σ ^S A ]tS) (γ ,Σ x)}
                                     (funext≃' λ a → mapF≃ {A = B}{t} refl  (map^ {A = A}{σ}{γ}{a}))
                                     {λ {x}{y} p → respF t (ref Δ (map σ γ) ,Σ p)}
                                     {λ {x} {y} p → respF (t [ σ ^S A ]tS) (ref Γ γ ,Σ p)}
                                     (funexti≃' λ a₀ → funexti≃' λ a₁ → funext≃ (∶F~=' A (prop Δ _ _)) (λ {p₀}{p₁} p₂
                                     → (∶F~= {A₀ = B} refl (map^ {A = A}{σ}{γ}{a₀}) (map^ {A = A}{σ}{γ}{a₁})
                                             {ref Δ (map σ γ) ,Σ p₀}{resp (σ ^S A) (ref Γ γ ,Σ p₁)}
                                             (prop (Δ ,S A) _ _)
                                             {mapF t (map σ γ ,Σ a₀)}{mapF (t [ σ ^S A ]tS) (γ ,Σ a₀)}
                                             (from≃ ( uncoe (∣∣F= {A₀ = B} refl (map^ {A = A}{σ}{γ}{a₀})) ⁻¹̃
                                                    ◾̃ mapF≃ {Δ ,S A}{B}{t} refl (map^ {A = A}{σ}{γ}{a₀})))
                                             {mapF t (map σ γ ,Σ a₁)}{mapF (t [ σ ^S A ]tS) (γ ,Σ a₁)}
                                             (from≃ ( uncoe (∣∣F= {A₀ = B} refl (map^ {A = A}{σ}{γ}{a₁})) ⁻¹̃
                                                    ◾̃ mapF≃ {Δ ,S A}{B}{t} refl (map^ {A = A}{σ}{γ}{a₁}))))
                                     ,≃ propF'' B _ _)))

    ret : lamS t [ σ ]tS ≡[ Γ→F= (Π[]S.ret {Γ}{Δ}{σ}{A}{B}) ]≡ lamS (t [ σ ^S A ]tS)
    ret
      = mk→F=
          mapF₂
          (from≃
             (funexti≃' λ γ → funexti≃' λ δ → funext≃' λ p → funexti≃' λ a → funexti≃' λ b → funext≃' λ r
             →  refl
             ,≃ propF'' (B [ σ ^S A ]TS) _ _))

abstract
  ΠβS : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,S A)}{t : (Γ ,S A) →F B}
      → appS (lamS {A = A}{B} t) ≡ t
  ΠβS {Γ}{A}{B}{t}
    = mk→F=
        (funext λ { (γ ,Σ x) → refl })
        (from≃ (uncoe (respFT= (funext (λ { (γ ,Σ x) → refl }))) ⁻¹̃))

abstract
  ΠηS : {Γ : Setoid}{A : FamSetoid Γ}{B : FamSetoid (Γ ,S A)}{f : Γ →F ΠS A B}
      → lamS (appS {B = B} f) ≡ f
  ΠηS {Γ}{A}{B}{f}
    = mk→F=
        (funext λ γ → ,Σ= refl (funexti λ a → funexti λ a' → funext λ r
        → UIP'' (ap (λ z → B ∶ z F mapE A B (mapF f γ) a ~ mapE A B (mapF f γ) a') (prop (Γ ,S A) (ref Γ γ ,Σ r) (ref Γ γ ,Σ r)))
                (propF B (respF f (ref Γ γ) r) (proj₂ (mapF f γ) r))))
        (from≃ (uncoe (respFT= (funext (λ γ → ,Σ= refl (funexti (λ a → funexti (λ a' → funext (λ r → UIP'' (ap (λ z → B ∶ z F proj₁ (mapF f γ) a ~ proj₁ (mapF f γ) a') (,Σ= (prop Γ (ref Γ γ) (ref Γ γ)) (propF A r r))) (propF B (respF f (ref Γ γ) r) (proj₂ (mapF f γ) r))))))))) ⁻¹̃))
