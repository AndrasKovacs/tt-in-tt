module Setoid.Setoid where

open import lib
open import JM

-- setoid

record Setoid : Set₁ where
  field
    ∣_∣ : Set
    _∶_~_ : ∣_∣ → ∣_∣ → Set
    prop : ∀{γ γ'}(p q : _∶_~_ γ γ') → p ≡ q
    ref : (γ : ∣_∣) → _∶_~_ γ γ
    sym : ∀{γ γ'} → _∶_~_ γ γ' → _∶_~_ γ' γ
    trans : ∀{γ γ' γ''} → _∶_~_ γ γ' → _∶_~_ γ' γ'' → _∶_~_ γ γ''

  infix 4 ∣_∣
  infix 5 _∶_~_

∶~T=
  : {∣∣₀ ∣∣₁ : Set}(∣∣₂ : ∣∣₀ ≡ ∣∣₁)
    {_~₀_ : ∣∣₀ → ∣∣₀ → Set}{_~₁_ : ∣∣₁ → ∣∣₁ → Set}
  → (∣∣₀ → ∣∣₀ → Set)
  ≡ (∣∣₁ → ∣∣₁ → Set)
∶~T= refl = refl

refT=
  : {∣∣₀ ∣∣₁ : Set}(∣∣₂ : ∣∣₀ ≡ ∣∣₁)
    {_~₀_ : ∣∣₀ → ∣∣₀ → Set}{_~₁_ : ∣∣₁ → ∣∣₁ → Set}
    (~₂ : _~₀_ ≡[ ∶~T= ∣∣₂ {_~₀_}{_~₁_} ]≡ _~₁_)
    {ref₀ : (γ : ∣∣₀) → γ ~₀ γ}{ref₁ : (γ : ∣∣₁) → γ ~₁ γ}
  → ((γ : ∣∣₀) → _~₀_ γ γ)
  ≡ ((γ : ∣∣₁) → _~₁_ γ γ)
refT= refl refl = refl
{-
mkSetoid=
  : {∣∣₀ ∣∣₁ : Set}(∣∣₂ : ∣∣₀ ≡ ∣∣₁)
    {_~₀_ : ∣∣₀ → ∣∣₀ → Set}{_~₁_ : ∣∣₁ → ∣∣₁ → Set}
    (~₂ : _~₀_ ≡[ ∶~T= ∣∣₂ {_~₀_}{_~₁_} ]≡ _~₁_)
    {ref₀ : (γ : ∣∣₀) → γ ~₀ γ}{ref₁ : (γ : ∣∣₁) → γ ~₁ γ}
    (ref₂ : ref₀ ≡[ refT= ∣∣₂ ~₂ {ref₀}{ref₁} ]≡ ref₁)
  → _≡_ {A = Setoid}
        (record { ∣_∣ = ∣∣₀ ; _∶_~_ = _~₀_ ; ref = ref₀ })
        (record { ∣_∣ = ∣∣₁ ; _∶_~_ = _~₁_ ; ref = ref₁ })
mkSetoid= refl refl refl = refl
-}
open Setoid public

∣∣= : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
   → ∣ Γ₀ ∣ ≡ ∣ Γ₁ ∣
∣∣= refl = refl

∶~=
  : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
    {γ₀ : ∣ Γ₀ ∣}{γ₁ : ∣ Γ₁ ∣}(γ₂ : γ₀ ≡[ ∣∣= Γ₂ ]≡ γ₁)
    {δ₀ : ∣ Γ₀ ∣}{δ₁ : ∣ Γ₁ ∣}(δ₂ : δ₀ ≡[ ∣∣= Γ₂ ]≡ δ₁)
  → Γ₀ ∶ γ₀ ~ δ₀ ≡ Γ₁ ∶ γ₁ ~ δ₁
∶~= refl refl refl = refl
    
ref=
  : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
    {γ₀ : ∣ Γ₀ ∣}{γ₁ : ∣ Γ₁ ∣}(γ₂ : γ₀ ≡[ ∣∣= Γ₂ ]≡ γ₁)
  → ref Γ₀ γ₀ ≡[ ∶~= Γ₂ γ₂ γ₂ ]≡ ref Γ₁ γ₁
ref= refl refl = refl

-- setoid morphism

record _→S_ (Γ Δ : Setoid) : Set where
  field
    map  : ∣ Γ ∣ → ∣ Δ ∣
    resp : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map γ) ~ (map δ)

infix 3 _→S_

respT=
  : {Γ Δ : Setoid}
    {map₀ map₁ : ∣ Γ ∣ → ∣ Δ ∣}(map₂ : map₀ ≡ map₁)
  → ({γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₀ γ) ~ (map₀ δ))
  ≡ ({γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₁ γ) ~ (map₁ δ))
respT= refl = refl

mk→S=
  : {Γ Δ : Setoid}
    {map₀ map₁ : ∣ Γ ∣ → ∣ Δ ∣}(map₂ : map₀ ≡ map₁)
    {resp₀ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₀ γ) ~ (map₀ δ)}
    {resp₁ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₁ γ) ~ (map₁ δ)}
    (resp₂ : (λ {γ}{δ} p → resp₀ {γ}{δ} p) ≡[ respT= {Γ}{Δ} map₂ ]≡ (λ {γ}{δ} p → resp₁ {γ}{δ} p))
  → _≡_ {A = Γ →S Δ}
        (record { map = map₀ ; resp = resp₀ })
        (record { map = map₁ ; resp = resp₁ })
mk→S= refl refl = refl

open _→S_ public

map=
  : {Γ Δ : Setoid}
    {σ₀ σ₁ : Γ →S Δ}(σ₂ : σ₀ ≡ σ₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → map σ₀ γ₀ ≡ map σ₁ γ₁
map= refl refl = refl

resp= 
  : {Γ Δ : Setoid}
    {σ₀ σ₁ : Γ →S Δ}(σ₂ : σ₀ ≡ σ₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
    {δ₀ δ₁ : ∣ Γ ∣}(δ₂ : δ₀ ≡ δ₁)
    {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≡[ ∶~= {Γ} refl γ₂ δ₂ ]≡ p₁)
  → resp σ₀ p₀ ≡[ ∶~= {Δ} refl (map= σ₂ γ₂) (map= σ₂ δ₂) ]≡ resp σ₁ p₁
resp= refl refl refl refl = refl

resp≃
  : {Γ Δ : Setoid}
    {σ₀ σ₁ : Γ →S Δ}(σ₂ : σ₀ ≡ σ₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
    {δ₀ δ₁ : ∣ Γ ∣}(δ₂ : δ₀ ≡ δ₁)
    {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≃ p₁)
  → resp σ₀ p₀ ≃ resp σ₁ p₁
resp≃ refl refl refl (refl ,≃ refl) = refl ,≃ refl

-- family of setoids

record FamSetoid (Γ : Setoid) : Set₁ where
  field
    ∣_∣F_   : ∣ Γ ∣ → Set
    _∶_F_~_ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣_∣F_ γ → ∣_∣F_ δ → Set
    propF   : {γ δ : ∣ Γ ∣}{p q : Γ ∶ γ ~ δ}{a : ∣_∣F_ γ}{b : ∣_∣F_ δ}
              (r : _∶_F_~_ p a b)(s : _∶_F_~_ q a b) → r ≡[ ap (λ z → _∶_F_~_ z a b) (prop Γ p q) ]≡ s
    coeF    : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → ∣_∣F_ γ → ∣_∣F_ δ
    cohF    : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)(a : ∣_∣F_ γ) → _∶_F_~_ p a (coeF p a)
    refF    : {γ : ∣ Γ ∣}(a : ∣_∣F_ γ) → _∶_F_~_ (ref Γ γ) a a
    symF    : ∀{γ δ}{p : Γ ∶ γ ~ δ}{a : ∣_∣F_ γ}{b : ∣_∣F_ δ}
            → _∶_F_~_ p a b → _∶_F_~_ (sym Γ p) b a
    transF  : ∀{γ γ' γ''}{p : Γ ∶ γ ~ γ'}{q : Γ ∶ γ' ~ γ''}
              {a : ∣_∣F_ γ}{a' : ∣_∣F_ γ'}{a'' : ∣_∣F_ γ''}
            → _∶_F_~_ p a a' → _∶_F_~_ q a' a'' → _∶_F_~_ (trans Γ p q) a a''

  infix 5 ∣_∣F_
  infix 5 _∶_F_~_

FamSetoid=
  : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
  → FamSetoid Γ₀ ≡ FamSetoid Γ₁
FamSetoid= refl = refl

∶F~T=
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
  → ({γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set)
  ≡ ({γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set)
∶F~T= refl = refl

propFT=
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
    (∶F~₂ : (λ {γ}{δ} → ∶F~₀ {γ}{δ}) ≡[ ∶F~T= {Γ} ∣∣F₂ ]≡ (λ {γ}{δ} → ∶F~₁ {γ}{δ}))
  → ({γ δ : ∣ Γ ∣}{p q : Γ ∶ γ ~ δ}{a : ∣∣F₀ γ}{b : ∣∣F₀ δ}(r : ∶F~₀ p a b)(s : ∶F~₀ q a b) → r ≡[ ap (λ z → ∶F~₀ z a b) (prop Γ p q) ]≡ s)
  ≡ ({γ δ : ∣ Γ ∣}{p q : Γ ∶ γ ~ δ}{a : ∣∣F₁ γ}{b : ∣∣F₁ δ}(r : ∶F~₁ p a b)(s : ∶F~₁ q a b) → r ≡[ ap (λ z → ∶F~₁ z a b) (prop Γ p q) ]≡ s)
propFT= refl refl = refl

coeFT=
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
    (∶F~₂ : (λ {γ}{δ} → ∶F~₀ {γ}{δ}) ≡[ ∶F~T= {Γ} ∣∣F₂ ]≡ (λ {γ}{δ} → ∶F~₁ {γ}{δ}))
  → ({γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → ∣∣F₀ γ → ∣∣F₀ δ)
  ≡ ({γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → ∣∣F₁ γ → ∣∣F₁ δ)
coeFT= refl refl = refl

cohFT=
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
    (∶F~₂ : (λ {γ}{δ} → ∶F~₀ {γ}{δ}) ≡[ ∶F~T= {Γ} ∣∣F₂ ]≡ (λ {γ}{δ} → ∶F~₁ {γ}{δ}))
    {coeF₀ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → ∣∣F₀ γ → ∣∣F₀ δ}
    {coeF₁ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → ∣∣F₁ γ → ∣∣F₁ δ}
    (coeF₂ : (λ {γ}{δ} → coeF₀ {γ}{δ}) ≡[ coeFT= ∣∣F₂ ∶F~₂ ]≡ (λ {γ}{δ} → coeF₁ {γ}{δ}))
  → ({γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)(a : ∣∣F₀ γ) → ∶F~₀ p a (coeF₀ p a))
  ≡ ({γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)(a : ∣∣F₁ γ) → ∶F~₁ p a (coeF₁ p a))
cohFT= refl refl refl = refl

refFT=
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
    (∶F~₂ : (λ {γ}{δ} → ∶F~₀ {γ}{δ}) ≡[ ∶F~T= {Γ} ∣∣F₂ ]≡ (λ {γ}{δ} → ∶F~₁ {γ}{δ}))
    {refF₀ : {γ : ∣ Γ ∣}(a : ∣∣F₀ γ) → ∶F~₀ (ref Γ γ) a a}
    {refF₁ : {γ : ∣ Γ ∣}(a : ∣∣F₁ γ) → ∶F~₁ (ref Γ γ) a a}
  → ({γ : ∣ Γ ∣}(a : ∣∣F₀ γ) → ∶F~₀ (ref Γ γ) a a)
  ≡ ({γ : ∣ Γ ∣}(a : ∣∣F₁ γ) → ∶F~₁ (ref Γ γ) a a)
refFT= refl refl = refl

symFT=
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
    (∶F~₂ : (λ {γ}{δ} → ∶F~₀ {γ}{δ}) ≡[ ∶F~T= {Γ} ∣∣F₂ ]≡ (λ {γ}{δ} → ∶F~₁ {γ}{δ}))
  → (∀{γ δ}{p : Γ ∶ γ ~ δ}{a : ∣∣F₀ γ}{b : ∣∣F₀ δ} → ∶F~₀ p a b → ∶F~₀ (sym Γ p) b a)
  ≡ (∀{γ δ}{p : Γ ∶ γ ~ δ}{a : ∣∣F₁ γ}{b : ∣∣F₁ δ} → ∶F~₁ p a b → ∶F~₁ (sym Γ p) b a)
symFT= refl refl = refl

transFT=
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
    (∶F~₂ : (λ {γ}{δ} → ∶F~₀ {γ}{δ}) ≡[ ∶F~T= {Γ} ∣∣F₂ ]≡ (λ {γ}{δ} → ∶F~₁ {γ}{δ}))
  → (∀{γ γ' γ''}{p : Γ ∶ γ ~ γ'}{q : Γ ∶ γ' ~ γ''}{a : ∣∣F₀ γ}{a' : ∣∣F₀ γ'}{a'' : ∣∣F₀ γ''} → ∶F~₀ p a a' → ∶F~₀ q a' a'' → ∶F~₀ (trans Γ p q) a a'')
  ≡ (∀{γ γ' γ''}{p : Γ ∶ γ ~ γ'}{q : Γ ∶ γ' ~ γ''}{a : ∣∣F₁ γ}{a' : ∣∣F₁ γ'}{a'' : ∣∣F₁ γ''} → ∶F~₁ p a a' → ∶F~₁ q a' a'' → ∶F~₁ (trans Γ p q) a a'')
transFT= refl refl = refl

mkFamSetoid='
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
    (∶F~₂ : (λ {γ}{δ} → ∶F~₀ {γ}{δ}) ≡[ ∶F~T= {Γ} ∣∣F₂ ]≡ (λ {γ}{δ} → ∶F~₁ {γ}{δ}))
    {propF₀ : {γ δ : ∣ Γ ∣}{p q : Γ ∶ γ ~ δ}{a : ∣∣F₀ γ}{b : ∣∣F₀ δ}
            (r : ∶F~₀ p a b)(s : ∶F~₀ q a b) → r ≡[ ap (λ z → ∶F~₀ z a b) (prop Γ p q) ]≡ s}
    {propF₁ : {γ δ : ∣ Γ ∣}{p q : Γ ∶ γ ~ δ}{a : ∣∣F₁ γ}{b : ∣∣F₁ δ}
            (r : ∶F~₁ p a b)(s : ∶F~₁ q a b) → r ≡[ ap (λ z → ∶F~₁ z a b) (prop Γ p q) ]≡ s}
    (propF₂ : (λ {γ}{δ}{p}{q}{a}{b} → propF₀ {γ}{δ}{p}{q}{a}{b}) ≡[ propFT= ∣∣F₂ ∶F~₂ ]≡ (λ {γ}{δ}{p}{q}{a}{b} → propF₁ {γ}{δ}{p}{q}{a}{b}))
    {coeF₀ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → ∣∣F₀ γ → ∣∣F₀ δ}
    {coeF₁ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → ∣∣F₁ γ → ∣∣F₁ δ}
    (coeF₂ : (λ {γ}{δ} → coeF₀ {γ}{δ}) ≡[ coeFT= ∣∣F₂ ∶F~₂ ]≡ (λ {γ}{δ} → coeF₁ {γ}{δ}))
    {cohF₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)(a : ∣∣F₀ γ) → ∶F~₀ p a (coeF₀ p a)}
    {cohF₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)(a : ∣∣F₁ γ) → ∶F~₁ p a (coeF₁ p a)}
    (cohF₂ : (λ {γ}{δ} → cohF₀ {γ}{δ}) ≡[ cohFT= ∣∣F₂ ∶F~₂ coeF₂ ]≡ (λ {γ}{δ} → cohF₁ {γ}{δ}))
    {refF₀ : {γ : ∣ Γ ∣}(a : ∣∣F₀ γ) → ∶F~₀ (ref Γ γ) a a}
    {refF₁ : {γ : ∣ Γ ∣}(a : ∣∣F₁ γ) → ∶F~₁ (ref Γ γ) a a}
    (refF₂ : (λ {γ} → refF₀ {γ}) ≡[ refFT= ∣∣F₂ ∶F~₂ {refF₀}{refF₁} ]≡ (λ {γ} → refF₁ {γ}))
    {symF₀ : ∀{γ δ}{p : Γ ∶ γ ~ δ}{a : ∣∣F₀ γ}{b : ∣∣F₀ δ} → ∶F~₀ p a b → ∶F~₀ (sym Γ p) b a}
    {symF₁ : ∀{γ δ}{p : Γ ∶ γ ~ δ}{a : ∣∣F₁ γ}{b : ∣∣F₁ δ} → ∶F~₁ p a b → ∶F~₁ (sym Γ p) b a}
    (symF₂ : (λ {γ}{δ}{p}{a}{b} → symF₀ {γ}{δ}{p}{a}{b}) ≡[ symFT= ∣∣F₂ ∶F~₂ ]≡ (λ {γ}{δ}{p}{a}{b} → symF₁ {γ}{δ}{p}{a}{b}))
    {transF₀ : ∀{γ γ' γ''}{p : Γ ∶ γ ~ γ'}{q : Γ ∶ γ' ~ γ''}{a : ∣∣F₀ γ}{a' : ∣∣F₀ γ'}{a'' : ∣∣F₀ γ''} → ∶F~₀ p a a' → ∶F~₀ q a' a'' → ∶F~₀ (trans Γ p q) a a''}
    {transF₁ : ∀{γ γ' γ''}{p : Γ ∶ γ ~ γ'}{q : Γ ∶ γ' ~ γ''}{a : ∣∣F₁ γ}{a' : ∣∣F₁ γ'}{a'' : ∣∣F₁ γ''} → ∶F~₁ p a a' → ∶F~₁ q a' a'' → ∶F~₁ (trans Γ p q) a a''}
    (transF₂ : (λ {γ}{γ'}{γ''}{p}{q}{a}{a'}{a''} → transF₀ {γ}{γ'}{γ''}{p}{q}{a}{a'}{a''}) ≡[ transFT= ∣∣F₂ ∶F~₂ ]≡ (λ {γ}{γ'}{γ''}{p}{q}{a}{a'}{a''} → transF₁ {γ}{γ'}{γ''}{p}{q}{a}{a'}{a''}))
  → _≡_ {A = FamSetoid Γ}
        (record { ∣_∣F_ = ∣∣F₀ ; _∶_F_~_ = ∶F~₀ ; propF = propF₀ ; coeF = coeF₀ ; cohF = cohF₀ ; refF = refF₀ ; symF = symF₀ ; transF = transF₀ })
        (record { ∣_∣F_ = ∣∣F₁ ; _∶_F_~_ = ∶F~₁ ; propF = propF₁ ; coeF = coeF₁ ; cohF = cohF₁ ; refF = refF₁ ; symF = symF₁ ; transF = transF₁ })
mkFamSetoid=' refl refl refl refl refl refl refl refl = refl

mkFamSetoid=
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
    (∶F~₂ : (λ {γ}{δ} → ∶F~₀ {γ}{δ}) ≡[ ∶F~T= {Γ} ∣∣F₂ ]≡ (λ {γ}{δ} → ∶F~₁ {γ}{δ}))
    {propF₀ : {γ δ : ∣ Γ ∣}{p q : Γ ∶ γ ~ δ}{a : ∣∣F₀ γ}{b : ∣∣F₀ δ}
            (r : ∶F~₀ p a b)(s : ∶F~₀ q a b) → r ≡[ ap (λ z → ∶F~₀ z a b) (prop Γ p q) ]≡ s}
    {propF₁ : {γ δ : ∣ Γ ∣}{p q : Γ ∶ γ ~ δ}{a : ∣∣F₁ γ}{b : ∣∣F₁ δ}
            (r : ∶F~₁ p a b)(s : ∶F~₁ q a b) → r ≡[ ap (λ z → ∶F~₁ z a b) (prop Γ p q) ]≡ s}
    {coeF₀ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → ∣∣F₀ γ → ∣∣F₀ δ}
    {coeF₁ : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → ∣∣F₁ γ → ∣∣F₁ δ}
    (coeF₂ : (λ {γ}{δ} → coeF₀ {γ}{δ}) ≡[ coeFT= ∣∣F₂ ∶F~₂ ]≡ (λ {γ}{δ} → coeF₁ {γ}{δ}))
    {cohF₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)(a : ∣∣F₀ γ) → ∶F~₀ p a (coeF₀ p a)}
    {cohF₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)(a : ∣∣F₁ γ) → ∶F~₁ p a (coeF₁ p a)}
    (cohF₂ : (λ {γ}{δ} → cohF₀ {γ}{δ}) ≡[ cohFT= ∣∣F₂ ∶F~₂ coeF₂ ]≡ (λ {γ}{δ} → cohF₁ {γ}{δ}))
    {refF₀ : {γ : ∣ Γ ∣}(a : ∣∣F₀ γ) → ∶F~₀ (ref Γ γ) a a}
    {refF₁ : {γ : ∣ Γ ∣}(a : ∣∣F₁ γ) → ∶F~₁ (ref Γ γ) a a}
    {symF₀ : ∀{γ δ}{p : Γ ∶ γ ~ δ}{a : ∣∣F₀ γ}{b : ∣∣F₀ δ} → ∶F~₀ p a b → ∶F~₀ (sym Γ p) b a}
    {symF₁ : ∀{γ δ}{p : Γ ∶ γ ~ δ}{a : ∣∣F₁ γ}{b : ∣∣F₁ δ} → ∶F~₁ p a b → ∶F~₁ (sym Γ p) b a}
    {transF₀ : ∀{γ γ' γ''}{p : Γ ∶ γ ~ γ'}{q : Γ ∶ γ' ~ γ''}{a : ∣∣F₀ γ}{a' : ∣∣F₀ γ'}{a'' : ∣∣F₀ γ''} → ∶F~₀ p a a' → ∶F~₀ q a' a'' → ∶F~₀ (trans Γ p q) a a''}
    {transF₁ : ∀{γ γ' γ''}{p : Γ ∶ γ ~ γ'}{q : Γ ∶ γ' ~ γ''}{a : ∣∣F₁ γ}{a' : ∣∣F₁ γ'}{a'' : ∣∣F₁ γ''} → ∶F~₁ p a a' → ∶F~₁ q a' a'' → ∶F~₁ (trans Γ p q) a a''}
  → _≡_ {A = FamSetoid Γ}
        (record { ∣_∣F_ = ∣∣F₀ ; _∶_F_~_ = ∶F~₀ ; propF = propF₀ ; coeF = coeF₀ ; cohF = cohF₀ ; refF = refF₀ ; symF = symF₀ ; transF = transF₀ })
        (record { ∣_∣F_ = ∣∣F₁ ; _∶_F_~_ = ∶F~₁ ; propF = propF₁ ; coeF = coeF₁ ; cohF = cohF₁ ; refF = refF₁ ; symF = symF₁ ; transF = transF₁ })
mkFamSetoid= {Γ}{∣∣F} refl {∶F~} refl {propF₀}{propF₁} refl refl {refF₀}{refF₁}{symF₀}{symF₁}{transF₀}{transF₁}
  = mkFamSetoid='
      refl
      refl
      ( funexti λ γ → funexti λ δ → funexti λ p → funexti λ q → funexti λ a → funexti λ b → funext λ r → funext λ s
      → UIP _ _)
      refl
      refl
      ( funexti λ γ → funext λ a
      → UIP'' (ap (λ z → ∶F~ z a a) (prop Γ (ref Γ γ) (ref Γ γ))) (propF₀ (refF₀ a)(refF₁ a)))
      ( funexti λ γ → funexti λ δ → funexti λ p → funexti λ a → funexti λ b → funext λ r
      → UIP'' ( ap (λ z → ∶F~ z b a) (prop Γ (sym Γ p) (sym Γ p))) (propF₀ (symF₀ r)(symF₁ r)))
      ( funexti λ γ → funexti λ γ' → funexti λ γ'' → funexti λ p → funexti λ q → funexti λ a → funexti λ a' → funexti λ a'' → funext λ r → funext λ s
      → UIP'' (ap (λ z → ∶F~ z a a'') (prop Γ (trans Γ p q) (trans Γ p q))) (propF₀ (transF₀ r s)(transF₁ r s)))

open FamSetoid public

∣∣F=
  : {Γ : Setoid}
    {A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → ∣ A₀ ∣F γ₀ ≡ ∣ A₁ ∣F γ₁
∣∣F= refl refl = refl

∶F~=
  : {Γ : Setoid}
    {A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
    {δ₀ δ₁ : ∣ Γ ∣}(δ₂ : δ₀ ≡ δ₁)
    {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≡[ ∶~= {Γ} refl γ₂ δ₂ ]≡ p₁)
    {a₀ : ∣ A₀ ∣F γ₀}{a₁ : ∣ A₁ ∣F γ₁}(a₂ : a₀ ≡[ ∣∣F= A₂ γ₂ ]≡ a₁)
    {b₀ : ∣ A₀ ∣F δ₀}{b₁ : ∣ A₁ ∣F δ₁}(b₂ : b₀ ≡[ ∣∣F= A₂ δ₂ ]≡ b₁)
  → A₀ ∶ p₀ F a₀ ~ b₀ ≡ A₁ ∶ p₁ F a₁ ~ b₁
∶F~= refl refl refl refl refl refl = refl

∶F~='
  : {Γ : Setoid}(A : FamSetoid Γ){γ : ∣ Γ ∣}{δ : ∣ Γ ∣}
    {p₀ p₁ : Γ ∶ γ ~ δ}(p₂ : p₀ ≡ p₁)
    {a : ∣ A ∣F γ}{b : ∣ A ∣F δ}
  → A ∶ p₀ F a ~ b ≡ A ∶ p₁ F a ~ b
∶F~=' {Γ} A {γ}{δ} p₂ {a}{b} = ∶F~= {Γ}{A} refl {γ} refl {δ} refl p₂ {a} refl {b} refl

coeF≃
  : {Γ : Setoid}{A : FamSetoid Γ}
    {γ₀ γ₁ δ₀ δ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)(δ₂ : δ₀ ≡ δ₁)
    {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≃ p₁)
  → {a₀ : ∣ A ∣F γ₀}{a₁ : ∣ A ∣F γ₁}(a₂ : a₀ ≃ a₁)
  → coeF A p₀ a₀ ≃ coeF A p₁ a₁
coeF≃ refl refl (refl ,≃ refl)(refl ,≃ refl) = refl ,≃ refl

cohF≃
  : {Γ : Setoid}{A : FamSetoid Γ}
    {γ₀ γ₁ δ₀ δ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)(δ₂ : δ₀ ≡ δ₁)
    {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≃ p₁)
  → {a₀ : ∣ A ∣F γ₀}{a₁ : ∣ A ∣F γ₁}(a₂ : a₀ ≃ a₁)
  → cohF A p₀ a₀ ≃ cohF A p₁ a₁
cohF≃ refl refl (refl ,≃ refl)(refl ,≃ refl) = refl ,≃ refl

coeF⁻¹
  : {Γ : Setoid}(A : FamSetoid Γ)
    {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → ∣ A ∣F δ → ∣ A ∣F γ
coeF⁻¹ {Γ} A p a = coeF A (sym Γ p) a 

cohF⁻¹
  : {Γ : Setoid}(A : FamSetoid Γ)
    {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)(a : ∣ A ∣F δ) → A ∶ p F coeF⁻¹ A p a ~ a
cohF⁻¹ {Γ} A p a = coe (∶F~=' A (prop Γ _ _)) (symF A (cohF A (sym Γ p) a))

refF=
  : {Γ : Setoid}
    {A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
    {a₀ : ∣ A₀ ∣F γ₀}{a₁ : ∣ A₁ ∣F γ₁}(a₂ : a₀ ≡[ ∣∣F= A₂ γ₂ ]≡ a₁)
  → refF A₀ a₀ ≡[ ∶F~= {Γ} A₂ γ₂ γ₂ (ref= {Γ} refl γ₂) a₂ a₂ ]≡ refF A₁ a₁
refF= refl refl refl = refl

symF≃
  : {Γ : Setoid}(A : FamSetoid Γ)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
    {δ₀ δ₁ : ∣ Γ ∣}(δ₂ : δ₀ ≡ δ₁)
    {p₀ : Γ ∶ γ₀ ~ δ₀}{p₁ : Γ ∶ γ₁ ~ δ₁}(p₂ : p₀ ≃ p₁)
    {a₀ : ∣ A ∣F γ₀}{a₁ : ∣ A ∣F γ₁}(a₂ : a₀ ≃ a₁)
    {b₀ : ∣ A ∣F δ₀}{b₁ : ∣ A ∣F δ₁}(b₂ : b₀ ≃ b₁)
    (r₀ : A ∶ p₀ F a₀ ~ b₀)(r₁ : A ∶ p₁ F a₁ ~ b₁)
  → symF A r₀ ≃ symF A r₁
symF≃ {Γ} A refl refl (refl ,≃ refl)(refl ,≃ refl)(refl ,≃ refl) r₀ r₁ = refl ,≃ UIP'' (ap (λ z → A ∶ z F _ ~ _) (prop Γ (sym Γ _) (sym Γ _))) (propF A (symF A r₀)(symF A r₁))

abstract
  propF'
    : {Γ : Setoid}(A : FamSetoid Γ)
      {γ δ : ∣ Γ ∣}{p q : Γ ∶ γ ~ δ}{a : ∣ A ∣F γ}{b : ∣ A ∣F δ}
      (r : A ∶ p F a ~ b)(s : A ∶ q F a ~ b)
    → r ≃ s
  propF' {Γ} A {γ}{δ}{p}{q}{a}{b} r s = from≡ (ap (λ z → A ∶ z F a ~ b) (prop Γ p q)) (propF A r s)

abstract
  propF''
    : {Γ : Setoid}(A : FamSetoid Γ)
      {γ δ : ∣ Γ ∣}{p : Γ ∶ γ ~ δ}{a : ∣ A ∣F γ}{b : ∣ A ∣F δ}
      (r s : A ∶ p F a ~ b)
    → r ≡ s
  propF'' {Γ} A {γ}{δ}{p}{a}{b} r s = from≃ (from≡ (ap (λ z → A ∶ z F a ~ b) (prop Γ p p)) (propF A r s))

∣∣F='
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → ∣∣F₀ γ₀ ≡ ∣∣F₁ γ₁
∣∣F=' refl refl = refl
{-
∣∣F=mkFamSetoid
  : {Γ : Setoid}
    {∣∣F₀ ∣∣F₁ : ∣ Γ ∣ → Set}(∣∣F₂ : ∣∣F₀ ≡ ∣∣F₁)
    {∶F~₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₀ γ → ∣∣F₀ δ → Set}
    {∶F~₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → ∣∣F₁ γ → ∣∣F₁ δ → Set}
    (∶F~₂ : (λ {γ}{δ} → ∶F~₀ {γ}{δ}) ≡[ ∶F~T= {Γ} ∣∣F₂ {∶F~₀}{∶F~₁} ]≡ (λ {γ}{δ} → ∶F~₁ {γ}{δ}))
    {refF₀ : {γ : ∣ Γ ∣}(a : ∣∣F₀ γ) → ∶F~₀ (ref Γ γ) a a}
    {refF₁ : {γ : ∣ Γ ∣}(a : ∣∣F₁ γ) → ∶F~₁ (ref Γ γ) a a}
    (refF₂ : (λ {γ} → refF₀ {γ}) ≡[ refFT= ∣∣F₂ ∶F~₂ {refF₀}{refF₁} ]≡ (λ {γ} → refF₁ {γ}))
    {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
  → _≡_ {A = ∣∣F₀ γ₀ ≡ ∣∣F₁ γ₁}
        (∣∣F= (mkFamSetoid= ∣∣F₂ ∶F~₂ refF₂) γ₂)
        (∣∣F=' {Γ} ∣∣F₂ γ₂)
∣∣F=mkFamSetoid refl refl refl refl = refl
-}
-- section of family of setoids

record _→F_ (Γ : Setoid)(A : FamSetoid Γ) : Set where
  field
    mapF  : (γ : ∣ Γ ∣) → ∣ A ∣F γ
    respF : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF γ) ~ (mapF δ)

infix 3 _→F_

Γ→F= : {Γ : Setoid}{A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
     → (Γ →F A₀) ≡ (Γ →F A₁)
Γ→F= {Γ} = ap (Γ →F_)

respFT=
  : {Γ : Setoid}{A : FamSetoid Γ}
    {mapF₀ mapF₁ : (γ : ∣ Γ ∣) → ∣ A ∣F γ}(mapF₂ : mapF₀ ≡ mapF₁)
  → ({γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₀ γ) ~ (mapF₀ δ))
  ≡ ({γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₁ γ) ~ (mapF₁ δ))
respFT= refl = refl

mk→F=
  : {Γ : Setoid}{A : FamSetoid Γ}
    {mapF₀ mapF₁ : (γ : ∣ Γ ∣) → ∣ A ∣F γ}(mapF₂ : mapF₀ ≡ mapF₁)
    {respF₀ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₀ γ) ~ (mapF₀ δ)}
    {respF₁ : {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ) → A ∶ p F (mapF₁ γ) ~ (mapF₁ δ)}
    (respF₂ : (λ {γ}{δ} → respF₀ {γ}{δ}) ≡[ respFT= {Γ}{A} mapF₂ ]≡ (λ {γ}{δ} → respF₁ {γ}{δ}))
  → _≡_ {A = Γ →F A}
        (record { mapF = mapF₀ ; respF = respF₀ })
        (record { mapF = mapF₁ ; respF = respF₁ })
mk→F= refl refl = refl

open _→F_ public

mapF≃ : {Γ : Setoid}{A : FamSetoid Γ}{σ₀ σ₁ : Γ →F A}(σ₂ : σ₀ ≡ σ₁)
        {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
      → mapF σ₀ γ₀ ≃ mapF σ₁ γ₁
mapF≃ refl refl = refl ,≃ refl

mapF≃' : {Γ : Setoid}{A : FamSetoid Γ}{σ : Γ →F A}
         {γ₀ γ₁ : ∣ Γ ∣}(γ₂ : γ₀ ≡ γ₁)
       → mapF σ γ₀ ≃ mapF σ γ₁
mapF≃' refl = refl ,≃ refl

mapFcoe
  : {Γ : Setoid}{A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {t : Γ →F A₀}{γ : ∣ Γ ∣}
  → mapF (coe (Γ→F= A₂) t) γ
  ≡ coe (∣∣F= A₂ refl) (mapF t γ)
mapFcoe refl = refl

mapFcoe'
  : {Γ : Setoid}{A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {t : Γ →F A₀}{γ : ∣ Γ ∣}
  → mapF (coe (Γ→F= A₂) t) γ ≃ mapF t γ
mapFcoe' refl = refl ,≃ refl

respFcoe
  : {Γ : Setoid}{A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {t : Γ →F A₀}{γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)
  → respF (coe (Γ→F= A₂) t) p
  ≡ coe (∶F~= {Γ} A₂ {γ} refl {δ} refl {p} refl {mapF t γ}{mapF (coe (Γ→F= A₂) t) γ} (mapFcoe A₂ ⁻¹) {mapF t δ}{mapF (coe (Γ→F= A₂) t) δ} (mapFcoe A₂ ⁻¹))
        (respF t p)
respFcoe refl p = refl

respFcoe'
  : {Γ : Setoid}{A₀ A₁ : FamSetoid Γ}(A₂ : A₀ ≡ A₁)
    {t : Γ →F A₀}{γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)
  → respF (coe (Γ→F= A₂) t) p
  ≃ respF t p
respFcoe' refl p = refl ,≃ refl

-- fixities

infixl 5 _,S_
infixl 7 _[_]TS

infixl 5 _,sS_∶_
infix  6 _∘S_
infixl 8 _[_]tS

-- trivial setoid

⊤S : Setoid
⊤S = record
  { ∣_∣   = ⊤
  ; _∶_~_ = λ _ _ → ⊤
  ; prop  = λ { tt tt → refl }
  ; ref   = λ _ → tt
  ; sym   = λ _ → tt
  ; trans = λ _ _ → tt
  }

-- setoid comprehension

_,S_ : (Γ : Setoid) → FamSetoid Γ → Setoid
Γ ,S A = record
  { ∣_∣   = Σ ∣ Γ ∣ ∣ A ∣F_
  ; _∶_~_ = λ { (γ₀ ,Σ a₀) (γ₁ ,Σ a₁) → Σ (Γ ∶ γ₀ ~ γ₁) λ γ₂ → A ∶ γ₂ F a₀ ~ a₁ }
  ; prop  = λ { {γ ,Σ a}{γ' ,Σ a'}(p ,Σ r)(q ,Σ s) → ,Σ= (prop Γ p q) (propF A r s) }
  ; ref   = λ { (γ ,Σ a) → (ref Γ γ) ,Σ refF A a }
  ; sym   = λ { {γ ,Σ a}{γ' ,Σ a'}(p ,Σ r) → (sym Γ p) ,Σ symF A r }
  ; trans = λ { {γ ,Σ a}{γ' ,Σ a'}{γ'' ,Σ a''}(p ,Σ r)(q ,Σ s) → trans Γ p q ,Σ transF A r s }
  }

,S=
  : {Γ₀ Γ₁ : Setoid}(Γ₂ : Γ₀ ≡ Γ₁)
    {A₀ : FamSetoid Γ₀}{A₁ : FamSetoid Γ₁}(A₂ : A₀ ≡[ FamSetoid= Γ₂ ]≡ A₁)
  → Γ₀ ,S A₀ ≡ Γ₁ ,S A₁
,S= refl refl = refl

-- substitution of families of setoids

_[_]TS : {Γ : Setoid}{Θ : Setoid}(A : FamSetoid Γ) → Θ →S Γ → FamSetoid Θ
_[_]TS {Γ}{Θ} A σ = record
  { ∣_∣F_   = λ γ → ∣ A ∣F map σ γ
  ; _∶_F_~_ = λ {γ}{δ} p a b → A ∶ resp σ p F a ~ b
  ; propF   = λ {γ}{δ}{p}{q}{a}{b} r s → coe (ap (λ z → r ≡[ z ]≡ s){(ap (λ z → A ∶ z F a ~ b) (prop Γ (resp σ p) (resp σ q)))}{ap (λ z → A ∶ resp σ z F a ~ b) (prop Θ p q)} (UIP _ _))
                                            (propF A r s)
  ; coeF    = λ {γ}{δ} p → coeF A (resp σ p)
  ; cohF    = λ {γ}{δ} p → cohF A (resp σ p)
  ; refF    = λ {γ} a → coe (ap (λ z → A ∶ z F a ~ a) (prop Γ (ref Γ (map σ γ))(resp σ (ref Θ γ)))) (refF A a)
  ; symF    = λ {γ}{δ}{p}{a}{b} r → coe (ap (λ z → A ∶ z F b ~ a) (prop Γ (sym Γ (resp σ p))(resp σ (sym Θ p))))
                                      (symF A r)
  ; transF  = λ {γ}{γ'}{γ'}{p}{q}{a}{a'}{a''} r s → coe (ap (λ z → A ∶ z F a ~ a'') (prop Γ (trans Γ (resp σ p) (resp σ q)) (resp σ (trans Θ p q))))
                                                       (transF A r s)
  }

-- identity setoid morphism

idS : {Γ : Setoid} → Γ →S Γ
idS = record
  { map  = λ γ → γ
  ; resp = λ p → p
  }

-- setoid morphism composition

_∘S_ : {Γ Δ Σ : Setoid} → Δ →S Σ → Γ →S Δ → Γ →S Σ
_∘S_ {Γ}{Δ}{Σ} σ ν = record
  { map  = λ γ → map σ (map ν γ)
  ; resp = λ γ₂ → resp σ (resp ν γ₂)
  }
  
-- 

εS : {Γ : Setoid} → Γ →S ⊤S
εS {Γ} = record { map = λ _ → tt ; resp = λ _ → tt }

-- setoid morphism extension

_,sS_∶_ : {Γ Δ : Setoid}(σ : Γ →S Δ)(A : FamSetoid Δ)
      → Γ →F (A [ σ ]TS) → Γ →S (Δ ,S A)
σ ,sS A ∶ t = record
  { map  = λ γ → (map σ γ) ,Σ (mapF t γ)
  ; resp = λ γ₂ → (resp σ γ₂) ,Σ respF t γ₂
  }

-- first projection

π₁S : {Γ Δ : Setoid}(A : FamSetoid Δ)(σ : Γ →S (Δ ,S A))
    → Γ →S Δ
π₁S A σ = record
  { map  = λ γ → proj₁ (map σ γ)
  ; resp = λ γ₂ → proj₁ (resp σ γ₂)
  }
-- substitution of sections

_[_]tS : {Γ Δ : Setoid}{A : FamSetoid Δ}
         (t : Δ →F A)(σ : Γ →S Δ) → Γ →F (A [ σ ]TS)
_[_]tS {Γ}{Δ}{A} t σ = record
  { mapF  = λ γ → mapF t (map σ γ)
  ; respF = λ γ₂ → respF t (resp σ γ₂)
  }

-- second projection

π₂S : {Γ Δ : Setoid}(A : FamSetoid Δ)(σ : Γ →S (Δ ,S A))
    → Γ →F (A [ π₁S A σ ]TS)
π₂S A σ = record
  { mapF  = λ γ → proj₂ (map σ γ)
  ; respF = λ γ₂ → proj₂ (resp σ γ₂)
  }

-- equalities

abstract
  [id]TS : {Γ : Setoid}{A : FamSetoid Γ} → A [ idS ]TS ≡ A
  [id]TS {Γ}{A} = mkFamSetoid= refl refl refl refl

abstract
  [][]TS : {Γ Δ Σ : Setoid}{A : FamSetoid Σ}{σ : Γ →S Δ}{δ : Δ →S Σ}
         → A [ δ ]TS [ σ ]TS ≡ A [ δ ∘S σ ]TS
  [][]TS = mkFamSetoid= refl refl refl refl

abstract
  idlS : {Γ Δ : Setoid}{δ : Γ →S Δ} → idS ∘S δ ≡ δ
  idlS = mk→S= refl refl

abstract
  idrS : {Γ Δ : Setoid}{δ : Γ →S Δ} → δ ∘S idS ≡ δ
  idrS = mk→S= refl refl

abstract
  assS
    : {Δ Γ Σ Ω : Setoid}{σ : Σ →S Ω}{δ : Γ →S Σ}{ν : Δ →S Γ}
    → (σ ∘S δ) ∘S ν ≡ σ ∘S (δ ∘S ν)
  assS = mk→S= refl refl

coerespT=
  : {Γ Δ : Setoid}
    {map₀ map₁ : ∣ Γ ∣ → ∣ Δ ∣}(map₂ : map₀ ≡ map₁)
    {resp' : {γ δ : ∣ Γ ∣} → Γ ∶ γ ~ δ → Δ ∶ (map₀ γ) ~ (map₀ δ)}
    {γ δ : ∣ Γ ∣}(p : Γ ∶ γ ~ δ)
  → coe (respT= {Γ}{Δ} map₂) resp' p
  ≡ coe (∶~= {Δ} refl (ap (λ z → z γ) map₂) (ap (λ z → z δ) map₂)) (resp' p)
coerespT= refl p = refl

module ,∘S
  {Γ Δ Σ : Setoid}{ν : Γ →S Δ}{σ : Σ →S Γ}{A : FamSetoid Δ}{a : Γ →F (A [ ν ]TS)}
  where
    abstract
      map₂ : (λ γ → map (ν ,sS A ∶ a) (map σ γ))
           ≡ (λ γ → map (ν ∘S σ) γ ,Σ mapF (coe (Γ→F= ([][]TS {A = A}{σ}{ν})) (a [ σ ]tS)) γ)
      map₂ = funext λ γ → ,Σ= refl (from≃ (uncoe (∣∣F= ([][]TS {A = A}{σ}{ν}) refl)) ◾ mapFcoe ([][]TS {A = A}{σ}{ν}) ⁻¹)

    abstract
      resp₂
        : (λ {_}{_} γ₂ → resp (ν ,sS A ∶ a) (resp {Σ}{Γ} σ γ₂))
        ≡[ respT= {Σ}{Δ ,S A} map₂ ]≡
          (λ {_}{_} γ₂ → resp (ν ∘S σ) γ₂ ,Σ respF (coe (Γ→F= ([][]TS {A = A}{σ}{ν})) (a [ σ ]tS)) γ₂)
      resp₂
        = funexti λ γ → funexti λ δ → funext λ p →
              coerespT= map₂ {λ γ₂ → resp (ν ,sS A ∶ a) (resp σ γ₂)} p
            ◾ from≃ ( uncoe (∶~= refl (ap (λ z → z γ) map₂) (ap (λ z → z δ) map₂)) ⁻¹̃
                    ◾̃ ,Σ≃≃ (to≃ (funext λ γ₂ → ∶F~= {Δ}{A} refl refl refl {γ₂} refl
                                                    {mapF a (map σ γ)}
                                                    {mapF (coe (Γ→F= ([][]TS {A = A}{σ}{ν})) (a [ σ ]tS)) γ}
                                                    (from≃ (uncoe (∣∣F= ([][]TS {A = A}{σ}{ν}) refl)) ◾ mapFcoe ([][]TS {A = A}{σ}{ν}) ⁻¹)
                                                    {mapF a (map σ δ)}
                                                    {mapF (coe (Γ→F= ([][]TS {A = A}{σ}{ν})) (a [ σ ]tS)) δ}
                                                    (from≃ (uncoe (∣∣F= ([][]TS {A = A}{σ}{ν}) refl)) ◾ mapFcoe ([][]TS {A = A}{σ}{ν}) ⁻¹)))
                           r̃
                           (respFcoe' ([][]TS {A = A}{σ}{ν}){a [ σ ]tS} p ⁻¹̃))

    ret : (ν ,sS A ∶ a) ∘S σ ≡ (ν ∘S σ) ,sS A ∶ coe (Γ→F= ([][]TS {A = A}{σ}{ν})) (a [ σ ]tS)
    ret = mk→S= map₂ resp₂

abstract
  π₁βS
    : {Γ Δ : Setoid}{A : FamSetoid Δ}{δ : Γ →S Δ}{a : Γ →F (A [ δ ]TS)}
    → π₁S A (δ ,sS A ∶ a) ≡ δ
  π₁βS = mk→S= refl refl

abstract
  πηS
    : {Γ Δ : Setoid}{A : FamSetoid Δ}{δ : Γ →S (Δ ,S A)}
    → π₁S A δ ,sS A ∶ π₂S A δ ≡ δ 
  πηS = mk→S= refl refl

abstract
  εη : {Γ : Setoid}{σ : Γ →S ⊤S} → σ ≡ εS
  εη = mk→S= refl refl

abstract
  π₂βS : {Γ Δ : Setoid}{A : FamSetoid Δ}{δ : Γ →S Δ}{a : Γ →F (A [ δ ]TS)}
       → π₂S  A (δ ,sS A ∶ a) ≡[ Γ→F= (ap (_[_]TS A) (π₁βS {Γ}{Δ}{A}{δ}{a})) ]≡ a
  π₂βS = mk→F= refl refl

