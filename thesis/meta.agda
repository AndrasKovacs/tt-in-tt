module thesis.meta where

-- formalisation corresponding to the metatheory chapter of (chapter
-- 2) of the PhD thesis: Type theory in a type theory with quotient
-- inductive types (Ambrus Kaposi)

-- section "Inductive types"

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

Recℕ : (ℕᴹ : Set)
       (zeroᴹ : ℕᴹ)(sucᴹ : ℕᴹ → ℕᴹ)
       → ℕ → ℕᴹ
Recℕ ℕᴹ zeroᴹ sucᴹ zero    = zeroᴹ
Recℕ ℕᴹ zeroᴹ sucᴹ (suc n) = sucᴹ (Recℕ ℕᴹ zeroᴹ sucᴹ n)

Elimℕ : (ℕᴹ : ℕ → Set)
        (zeroᴹ : ℕᴹ zero)(sucᴹ : (n : ℕ) → ℕᴹ n → ℕᴹ (suc n))
        (n : ℕ) → ℕᴹ n
Elimℕ ℕᴹ zeroᴹ sucᴹ zero    = zeroᴹ
Elimℕ ℕᴹ zeroᴹ sucᴹ (suc n) = sucᴹ n (Elimℕ ℕᴹ zeroᴹ sucᴹ n)

_+_ : ℕ → ℕ → ℕ
zero  + n = n
suc m + n = suc (m + n)

_+'_ : ℕ → ℕ → ℕ
_+'_ = Recℕ (ℕ → ℕ) (λ n → n) (λ f n → suc (f n))

data _≡_ {i}{A : Set i}(a : A) : A → Set i where
  refl : a ≡ a

infix 3 _≡_

ap : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
ap f refl = refl

transport : ∀{i j}{A : Set i}(P : A → Set j){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
            → P a₀ → P a₁
transport P refl u = u

coe : ∀{i}{A B : Set i} → A ≡ B → A → B
coe refl a = a

+≡+' : ∀(m : ℕ){n : ℕ} → (m + n) ≡ (m +' n)
+≡+' zero = refl
+≡+' (suc m) = ap suc (+≡+' m)

postulate
  funext : ∀{i j}{A : Set i}{B : A → Set j}{f g : (x : A) → B x}
         → ((x : A) → f x ≡ g x) → _≡_ f g

-- mutual inductive types, section "Definitions, universes and functions"

module Mutual where

  data Bool : Set where
    true false : Bool

  record _×_ (A B : Set) : Set where
    constructor _,_
    field
      proj₁ : A
      proj₂ : B

  open _×_

  isEven isOdd : ℕ → Bool

  isEven zero = true
  isEven (suc n) = isOdd n

  isOdd zero = false
  isOdd (suc n) = isEven n

  isEvenOdd : ℕ → Bool × Bool
  isEvenOdd zero = true , false
  isEvenOdd (suc n) = let (e , o) = isEvenOdd n
                      in  (o , e)

  -- we can't even write this with the recursor (and this version
  -- doesn't pass the termination checker)
  -- isEven' isOdd' : ℕ → Bool
  -- isEven' = Elimℕ (λ _ → Bool) true (λ n _ → isOdd' n)
  -- isOdd' = Elimℕ (λ _ → Bool) false (λ n _ → isEven' n)

  isEvenOdd' : ℕ → Bool × Bool
  isEvenOdd' = Recℕ (Bool × Bool) (true , false) (λ p → (proj₂ p , proj₁ p))

  -- unit tests

  five six : ℕ
  five = suc (suc (suc (suc (suc zero))))
  six = suc five

  u1 : isEven five ≡ false
  u2 : isOdd  five ≡ true
  u3 : isEven six  ≡ true
  u4 : isOdd  six  ≡ false
  u5 : isEvenOdd five ≡ (false , true)
  u6 : isEvenOdd six ≡ (true , false)
  -- u1' : isEven' five ≡ false
  -- u2' : isOdd'  five ≡ true
  -- u3' : isEven' six  ≡ true
  -- u4' : isOdd'  six  ≡ false
  u5' : isEvenOdd' five ≡ (false , true)
  u6' : isEvenOdd' six ≡ (true , false)

  u1 = refl
  u2 = refl
  u3 = refl
  u4 = refl
  u5 = refl
  u6 = refl
  -- u1' = refl
  -- u2' = refl
  -- u3' = refl
  -- u4' = refl
  u5' = refl
  u6' = refl

-- monoids as QITs and QTs

module monoid where

  postulate
    X : Set

  -- as a QIT

  data A : Set where
    emb : X → A
    id  : A
    _∘_ : A → A → A
  infixl 6 _∘_
  postulate
    idl : ∀{a} → id ∘ a ≡ a
    idr : ∀{a} → a ∘ id ≡ a
    ass : ∀{a a' a''} → (a ∘ a') ∘ a'' ≡ a ∘ (a' ∘ a'')

  record MA : Set₁ where
    field
      Aᴹ : Set
      embᴹ : X → Aᴹ
      idᴹ : Aᴹ
      _∘ᴹ_ : Aᴹ → Aᴹ → Aᴹ
      idlᴹ : ∀{aᴹ} → idᴹ ∘ᴹ aᴹ ≡ aᴹ
      idrᴹ : ∀{aᴹ} → aᴹ ∘ᴹ idᴹ ≡ aᴹ
      assᴹ : ∀{aᴹ a'ᴹ a''ᴹ} → (aᴹ ∘ᴹ a'ᴹ) ∘ᴹ a''ᴹ ≡ aᴹ ∘ᴹ (a'ᴹ ∘ᴹ a''ᴹ)

  module RecA (m : MA) where
    open MA m
    rec : A → Aᴹ
    rec (emb x)  = embᴹ x
    rec id       = idᴹ
    rec (a ∘ a') = rec a ∘ᴹ rec a'

  -- as a QT

  data B : Set where
    emb : X → B
    id  : B
    _∘_ : B → B → B

  record MB : Set₁ where
    field
      Bᴹ   : Set
      embᴹ : X → Bᴹ
      idᴹ  : Bᴹ
      _∘ᴹ_ : Bᴹ → Bᴹ → Bᴹ

  module RecB (m : MB) where
    open MB m
    rec : B → Bᴹ
    rec (emb x)  = embᴹ x
    rec id       = idᴹ
    rec (a ∘ a') = rec a ∘ᴹ rec a'

  data _~_ : B → B → Set where
    idl~   : ∀{b} → id ∘ b ~ b
    idr~   : ∀{b} → b ∘ id ~ b
    ass~   : ∀{b b' b''} → (b ∘ b') ∘ b'' ~ b ∘ (b' ∘ b'')
    cong~∘ : ∀{b b' b''} → b' ~ b'' → b ∘ b' ~ b ∘ b''
    cong~∘' : ∀{b b' b''} → b' ~ b'' → b' ∘ b ~ b'' ∘ b
  infix 3 _~_

  record M~ : Set₁ where
    field
      _~ᴹ_  : B → B → Set
      idl~ᴹ : ∀{b} → id ∘ b ~ᴹ b
      idr~ᴹ : ∀{b} → b ∘ id ~ᴹ b
      ass~ᴹ : ∀{b b' b''} → (b ∘ b') ∘ b'' ~ᴹ b ∘ (b' ∘ b'')
      cong~∘ᴹ : ∀{b b' b''} → b' ~ᴹ b'' → b ∘ b' ~ᴹ b ∘ b''
      cong~∘'ᴹ : ∀{b b' b''} → b' ~ᴹ b'' → b' ∘ b ~ᴹ b'' ∘ b
    infix 3 _~ᴹ_

  module Rec~ (m : M~) where
    open M~ m
    rec : ∀{b b'} → b ~ b' → b ~ᴹ b'
    rec idl~ = idl~ᴹ
    rec idr~ = idr~ᴹ
    rec ass~ = ass~ᴹ
    rec (cong~∘ p) = cong~∘ᴹ (rec p)
    rec (cong~∘' p) = cong~∘'ᴹ (rec p)

  data C : Set where
    [_] : B → C
  postulate
    [_]≡ : ∀{b b'} → b ~ b' → [ b ] ≡ [ b' ]

  record MC : Set₁ where
    field
      Cᴹ    : Set
      [_]ᴹ  : B → Cᴹ
      [_]≡ᴹ : ∀{b b'} → b ~ b' → [ b ]ᴹ ≡ [ b' ]ᴹ

  module RecC (m : MC) where
    open MC m
    rec : C → Cᴹ
    rec [ b ] = [ b ]ᴹ

  -- now we would like to reconstruct the constructors of the QIT

  embC : X → C
  embC x = [ emb x ]

  idC : C
  idC = [ id ]

  postulate
    cheat : ∀{A : Set} → A

  _∘C_ : C → C → C
  _∘C_ = RecC.rec (record
                     { Cᴹ    = C → C
                     ; [_]ᴹ  = λ b → RecC.rec (record
                                                 { Cᴹ    = C
                                                 ; [_]ᴹ  = λ b' → [ b ∘ b' ]
                                                 ; [_]≡ᴹ = λ {b'}{b''} p → [ cong~∘ p ]≡ })
                     ; [_]≡ᴹ = λ {b b'} p → cheat })
                       -- this can be filled in

module binary where

  -- binary branching trees where order of branches do not matter
  -- using quotient types
  
  data T₀ : Set where
    leaf : T₀
    node : T₀ → T₀ → T₀

  data _~_ : T₀ → T₀ → Set where
    leaf~ : leaf ~ leaf
    node~ : ∀{t t'}{t'' t'''} → t ~ t' → t'' ~ t''' → node t t'' ~ node t' t'''
    perm~ : ∀{t t'} → node t t' ~ node t' t

  refl~ : ∀ t → t ~ t
  refl~ leaf = leaf~
  refl~ (node t t') = node~ (refl~ t) (refl~ t')

  data T₁ : Set where
    [_]  : T₀ → T₁
  postulate
    [_]≡T : ∀ t t' → t ~ t' → _≡_ {A = T₁} [ t ] [ t' ]

  open import Agda.Primitive

  record MT₁ {i : Level} : Set (lsuc i) where
    field
      T₁ᴹ   : Set i
      [_]ᴹ  : T₀ → T₁ᴹ
      [_]≡ᴹ : ∀ t t' → t ~ t' → _≡_ {A = T₁ᴹ} [ t ]ᴹ [ t' ]ᴹ

  module RecT₁ {i}(m : MT₁ {i}) where
    open MT₁ m
    rec : T₁ → T₁ᴹ
    rec [ t₀ ] = [ t₀ ]ᴹ

  leaf₁ : T₁
  leaf₁ = [ leaf ]

  rec= : ∀{i}{T₁ᴹ : Set i}
         {[_]ᴹ₀ [_]ᴹ₁ : T₀ → T₁ᴹ}([_]ᴹ₂ : [_]ᴹ₀ ≡ [_]ᴹ₁)
         {[_]≡ᴹ₀ : ∀ t t' → t ~ t' → [ t ]ᴹ₀ ≡ [ t' ]ᴹ₀}
         {[_]≡ᴹ₁ : ∀ t t' → t ~ t' → [ t ]ᴹ₁ ≡ [ t' ]ᴹ₁}
         ([_]≡ᴹ₂ : _≡_ (transport
                         (λ z → ∀ t t' → t ~ t' → z t ≡ z t')
                         [_]ᴹ₂
                         [_]≡ᴹ₀
                       )
                       [_]≡ᴹ₁)
       → _≡_
           (RecT₁.rec (record { T₁ᴹ = T₁ᴹ ; [_]ᴹ = [_]ᴹ₀ ; [_]≡ᴹ = [_]≡ᴹ₀ }))
           (RecT₁.rec (record { T₁ᴹ = T₁ᴹ ; [_]ᴹ = [_]ᴹ₁ ; [_]≡ᴹ = [_]≡ᴹ₁ }))
  rec= refl refl = refl

  funext'' : ∀{i j k}{A : Set i}{B : A → A → Set j}{C : Set k}
             {D₀ D₁ : A → C}(D₂ : D₀ ≡ D₁)
             {f₀ : (x y : A) → B x y → D₀ x ≡ D₀ y}{f₁ : (x y : A) → B x y → D₁ x ≡ D₁ y}
             (f₂ : (x y : A)(z : B x y) → transport (λ D → D x ≡ D y) D₂ (f₀ x y z) ≡ f₁ x y z)
           → transport (λ D → (x y : A) → B x y → D x ≡ D y) D₂ f₀ ≡ f₁
  funext'' refl f₂ = funext λ x → funext λ y → funext λ z → f₂ x y z

  UIP : ∀{i}{A : Set i}{a a' : A}{p q : a ≡ a'} → p ≡ q
  UIP {p = refl} {q = refl} = refl

  node₁ : T₁ → T₁ → T₁
  node₁ = RecT₁.rec (record
    { T₁ᴹ = T₁ → T₁
    ; [_]ᴹ = λ t₀
             → RecT₁.rec
                 (record
                    { T₁ᴹ = T₁
                    ; [_]ᴹ = λ t₀' → [ node t₀ t₀' ]
                    ; [_]≡ᴹ = λ t₀' t₀'' p
                            → [_]≡T _ _ (node~ (refl~ t₀) p) 
                    })
    ; [_]≡ᴹ = λ t₀ t₀' p → rec= (funext (λ t₀'' → [_]≡T _ _ (node~ p (refl~ t₀''))))
                                (funext'' {A = T₀}{_~_}{T₁}
                                          {λ t₀'' → [ node t₀ t₀'' ]}{λ t₀'' → [ node t₀' t₀'' ]}
                                          (funext (λ t₀'' → [ node t₀ t₀'' ]≡T (node t₀' t₀'') (node~ p (refl~ t₀''))))
                                          {λ t₀'' t₀''' p₁ → [ node t₀ t₀'' ]≡T (node t₀ t₀''') (node~ (refl~ t₀) p₁)}
                                          {λ t₀'' t₀''' p₁ → [ node t₀' t₀'' ]≡T (node t₀' t₀''') (node~ (refl~ t₀') p₁)}
                                          (λ t₀' t₀'' p' → UIP))
    })

module tree_example where

  -- the same for infinitely branching trees

  data T₀ : Set where
    leaf : T₀
    node : (ℕ → T₀) → T₀

  postulate
    isIso : (ℕ → ℕ) → Set

  data _~T_ : T₀ → T₀ → Set where
    leaf : leaf ~T leaf
    node : ∀{f g} → (∀ n → f n ~T g n) → node f ~T node g
    perm : ∀{f g} → isIso g → node f ~T node (λ z → f (g z))

  data T₁ : Set where
    [_]  : T₀ → T₁
  postulate
    [_]≡T : ∀{t t'} → t ~T t' → _≡_ {A = T₁} [ t ] [ t' ]

  record MT₁ : Set₁ where
    field
      T₁ᴹ   : Set
      [_]ᴹ  : T₀ → T₁ᴹ
      [_]≡ᴹ : ∀{t t'} → t ~T t' → _≡_ {A = T₁ᴹ} [ t ]ᴹ [ t' ]ᴹ

  module RecT₁ (m : MT₁) where
    open MT₁ m
    rec : T₁ → T₁ᴹ
    rec [ t ] = [ t ]ᴹ

  leaf₁ : T₁
  leaf₁ = [ leaf ]

  postulate
    cheat : ∀{A : Set} → A

  node₁ : (ℕ → T₁) → T₁
  node₁ f₁ = [ node f₀ ]
    where
      f₀ : ℕ → T₀
      f₀ n = RecT₁.rec (record
                          { T₁ᴹ = T₀
                          ; [_]ᴹ = λ z → z
                          ; [_]≡ᴹ = λ {t t'} p → cheat
                          })
                       (f₁ n)

module funextFromInterval where

  data I : Set where
    zero one : I
  postulate
    seg : zero ≡ one

  RecI : {Iᴹ : Set}(zeroᴹ oneᴹ : Iᴹ) → zeroᴹ ≡ oneᴹ → I → Iᴹ
  RecI zeroᴹ oneᴹ q zero = zeroᴹ
  RecI zeroᴹ oneᴹ q one = oneᴹ

  -- we can turn an equality in A into a function I→A
  ≡2p : {A : Set}{x y : A} → x ≡ y → (I → A)
  ≡2p {A}{x}{y} p i = RecI {A} x y p i

  -- we can turn a function I→A into an equality
  p2≡ : {A : Set}{x y : A} → (p : I → A) → p zero ≡ p one
  p2≡ {A}{x}{y} p = ap p seg

  -- we can compose these two things to prove funext
  funext' : {X Y : Set}{f g : X → Y} → ((x : X) → f x ≡ g x) → f ≡ g
  funext' {X}{Y}{f}{g} φ = p2≡ {X → Y}{f}{g} H
    where
      H : I → (X → Y)
      H i x = ≡2p (φ x) i

record Σ (A : Set)(B : A → Set) : Set where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁

open Σ

module ElimFromRec where

  -- we show that the eliminator can be derived from the recursor, and
  -- the eliminator will have the correct computation rules,
  -- propositionally

  record Mℕ : Set₁ where
    field
      ℕᴹ : Set
      zeroᴹ : ℕᴹ
      sucᴹ : ℕᴹ → ℕᴹ

  module recℕ (m : Mℕ) where
    open Mℕ m

    rec : ℕ → ℕᴹ
    rec zero = zeroᴹ
    rec (suc n) = sucᴹ (rec n)

  record Eℕ : Set₁ where
    field
      ℕᴱ : ℕ → Set
      zeroᴱ : ℕᴱ zero
      sucᴱ : (n : ℕ) → ℕᴱ n → ℕᴱ (suc n)
  
  tot : Eℕ → Mℕ
  tot m = record
    { ℕᴹ = Σ ℕ ℕᴱ
    ; zeroᴹ = zero , zeroᴱ
    ; sucᴹ = λ { (n , p) → suc n , sucᴱ n p }
    }
    where
      open Eℕ m

  lemma-proj : (m : Eℕ)(n : ℕ) → proj₁ (recℕ.rec (tot m) n) ≡ n
  lemma-proj m zero = refl
  lemma-proj m (suc n) = ap suc (lemma-proj m n)

  module Elimℕ (m : Eℕ) where
    open Eℕ m

    elim : (n : ℕ) → ℕᴱ n
    elim n = coe (ap ℕᴱ (lemma-proj m n)) (proj₂ (recℕ.rec (tot m) n))

    zeroβ : elim zero ≡ zeroᴱ
    zeroβ = refl

    unsuc : {n m : ℕ} → suc n ≡ suc m → n ≡ m
    unsuc refl = refl

    lemma-suc : {n k : ℕ}{kᴱ : ℕᴱ k}
                (p : k ≡ n)
              → coe (ap ℕᴱ (ap suc p)) (sucᴱ k kᴱ)
              ≡ sucᴱ n (coe (ap ℕᴱ p) kᴱ)
    lemma-suc refl = refl

    sucβ : (n : ℕ) → elim (suc n) ≡ sucᴱ n (elim n)
    sucβ n = lemma-suc (lemma-proj m n)
