module asai where

open import lib
open import TT.Syntax
open import TT.Congr

-- examples from Kenichi Asai

-- λt:U.t
ex0 : Tm • (Π U U)
ex0 = lam (coe (TmΓ= U[]) vz)

ex0b : {Γ : Con} → Tm Γ (Π U U)
ex0b = lam (coe (TmΓ= U[]) vz)

-- λt:U.λx:El t.x
exA : Tm • (Π U
              (Π (El (coe (TmΓ= U[]) vz))
                  (El (coe (TmΓ= U[]) (coe (TmΓ= U[]) vz [ wk ]t)))))
exA = lam (lam (coe (TmΓ= El[]) vz))

-- λt':U.(λt:U.t)t'
ex1 : Tm • (Π U (U [ < coe (TmΓ= U[]) vz > ]T))
ex1 = lam (ex0b $ coe (TmΓ= U[]) vz)

-- λt':U.λx:El((λt:U.t)t').x
ex2 : Tm • (Π U
              (Π (El (coe (TmΓ= U[]) (ex0b $ coe (TmΓ= U[]) vz)))
                  (El (coe (TmΓ= U[]) (coe (TmΓ= U[]) (ex0b $ coe (TmΓ= U[]) vz) [ wk ]t)))))
ex2 = lam (lam (coe (TmΓ= El[]) vz))
