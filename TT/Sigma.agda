{-# OPTIONS --no-eta #-}

module TT.Sigma where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
import TT.Core.Laws
import TT.Decl.Congr

record Sigma {i j}{d : Decl}(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr d
  open Core c
  open TT.Core.Laws c

  field
    Σ' : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
    
    Σ[]    : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
           → ((Σ' A B) [ σ ]T) ≡ (Σ' (A [ σ ]T) (B [ σ ^ A ]T))

    _,Σ'_  : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}(a : Tm Γ A) → Tm Γ (B [ < a > ]T) → Tm Γ (Σ' A B)
    proj₁' : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}(w : Tm Γ (Σ' A B)) → Tm Γ A
    proj₂' : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}(w : Tm Γ (Σ' A B)) → Tm Γ (B [ < proj₁' w > ]T)
    Σβ₁    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{a : Tm Γ A}{b : Tm Γ (B [ < a > ]T)}
           → proj₁' (a ,Σ' b) ≡ a
    Σβ₂    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{a : Tm Γ A}{b : Tm Γ (B [ < a > ]T)}
           → proj₂' (a ,Σ' b) ≡[ TmΓ= (ap (λ z → B [ < z > ]T) Σβ₁) ]≡ b
    ,Σ[]   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{a : Tm Γ A}{b : Tm Γ (B [ < a > ]T)}
             {Θ}{σ : Tms Θ Γ}
           → (a ,Σ' b) [ σ ]t
           ≡[ TmΓ= Σ[] ]≡
             (   (a [ σ ]t)
             ,Σ' coe (TmΓ= ([][]T ◾ ap (_[_]T B) <>∘ ◾ [][]T ⁻¹)) (b [ σ ]t))
