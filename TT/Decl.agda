{-# OPTIONS --no-eta #-}

module TT.Decl where

open import Agda.Primitive

-- Declaration of a model

record Decl {i j} : Set (lsuc (i ⊔ j)) where
  field
    Con : Set i
    Ty  : Con → Set i
    Tms : Con → Con → Set j
    Tm  : (Γ : Con) → Ty Γ → Set j
