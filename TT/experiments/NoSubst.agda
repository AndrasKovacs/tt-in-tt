{-# OPTIONS --without-K --no-eta #-}

module TT.experiments.NoSubst where

-- trying to define type theory without substitutions (in fact we
-- don't need to mention substitutions in lam,app,β,η; but we need
-- weakenings to write down variables; and then we need to say how
-- weakenings interact with type formers - here we need the lifting of
-- a weakening for Π - this is not yet done)

open import Agda.Primitive
open import lib using (_≡_; refl; idfun)

----------------------------------------------------------------------
-- Declaration of a model
----------------------------------------------------------------------

record Decl {i j} : Set (lsuc (i ⊔ j)) where
  field
    Con : Set i
    Ty  : Con → Set i
    Var : (Γ : Con) → Ty Γ → Set j
    Tm  : (Γ : Con) → Ty Γ → Set j
    
  Con= : Con → Con → Set i
  Con= = _≡_

  Con== : {Γ₀₀ Γ₀₁ : Con}(Γ₀₂ : Con= Γ₀₀ Γ₀₁)
          {Γ₁₀ Γ₁₁ : Con}(Γ₁₂ : Con= Γ₁₀ Γ₁₁)
        → Con= Γ₀₀ Γ₁₀ → Con= Γ₀₁ Γ₁₁ → Set i
  Con== refl refl = _≡_

  coeCon : Con → Con
  coeCon = idfun

  Ty= : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
      → Ty Γ₀ → Ty Γ₁ → Set i
  Ty= refl = _≡_

  coeTy : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
        → Ty Γ₀ → Ty Γ₁
  coeTy refl = idfun

  coeTy= : {Γ₀₀ Γ₀₁ : Con}(Γ₀₂ : Con= Γ₀₀ Γ₀₁)
           {Γ₁₀ Γ₁₁ : Con}(Γ₁₂ : Con= Γ₁₀ Γ₁₁)
           {Γ₂₀ : Con= Γ₀₀ Γ₁₀}{Γ₂₁ : Con= Γ₀₁ Γ₁₁}(Γ₂₂ : Con== Γ₀₂ Γ₁₂ Γ₂₀ Γ₂₁)
           {A₀₀ : Ty Γ₀₀}{A₀₁ : Ty Γ₀₁}(A₀₂ : Ty= Γ₀₂ A₀₀ A₀₁)
           {A₁₀ : Ty Γ₁₀}{A₁₁ : Ty Γ₁₁}(A₁₂ : Ty= Γ₁₂ A₁₀ A₁₁)
         → Ty= Γ₂₀ A₀₀ A₁₀ → Ty= Γ₂₁ A₀₁ A₁₁
  coeTy= refl refl refl refl refl = idfun

  coe'Ty : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
         → Ty Γ₀ → Ty Γ₁
  coe'Ty refl = idfun

  Tm= : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
        {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty= Γ₂ A₀ A₁)
      → Tm Γ₀ A₀ → Tm Γ₁ A₁ → Set j
  Tm= refl refl = _≡_

  coeTm : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty= Γ₂ A₀ A₁)
        → Tm Γ₀ A₀ → Tm Γ₁ A₁
  coeTm refl refl = idfun

  cohTm : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty= Γ₂ A₀ A₁)
          {t₀ : Tm Γ₀ A₀}
        → Tm= Γ₂ A₂ t₀ (coeTm Γ₂ A₂ t₀)
  cohTm refl refl = refl

  coe'Tm : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
           {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty= Γ₂ A₀ A₁)
         → Tm Γ₁ A₁ → Tm Γ₀ A₀
  coe'Tm refl refl = idfun

----------------------------------------------------------------------
-- Core substitution calculus
----------------------------------------------------------------------

record Core {i j}(d : Decl {i}{j}) : Set (i ⊔ j) where
  open Decl d
  field
    •     : Con  -- \bub
    _,_   : (Γ : Con) → Ty Γ → Con

    wkT   : ∀{Γ B} → Ty Γ → Ty (Γ , B)
    
    zero  : ∀{Γ A} → Var (Γ , A) (wkT A)
    wkt   : ∀{Γ A B} → Tm Γ A → Tm (Γ , B) (wkT A)
  
    var   : ∀{Γ A} → Var Γ A → Tm Γ A

  infixl 5 _,_

  ,= : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty= Γ₂ A₀ A₁)
     → Con= (Γ₀ , A₀) (Γ₁ , A₁)
  ,= = λ { {Γ}{.Γ} refl refl → refl }
  
----------------------------------------------------------------------
-- Base type and family
----------------------------------------------------------------------

record Base {i j}(d : Decl)(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open Core c

  field
    U : ∀{Γ} → Ty Γ

  field
    wkU : ∀{Γ A} → Ty= refl (wkT {Γ}{A} U) U

  U= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
     → Ty= Γ₂ (U {Γ₀}) (U {Γ₁})
  U= = λ { {Γ₂ = refl} → refl }

  field
    El : ∀{Γ}(Â : Tm Γ U) → Ty Γ

  El= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
        {Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}(Â₂ : Tm= Γ₂ U= Â₀ Â₁)
      → Ty= Γ₂ (El Â₀) (El Â₁)
  El= = λ { {Γ₂ = refl} refl → refl }

  field
    wkEl : ∀{Γ Â A} → Ty= refl (wkT {Γ}{A} (El Â)) (El (coeTm refl wkU (wkt Â)))
  
----------------------------------------------------------------------
-- Function space
----------------------------------------------------------------------

record Func {i j}(d : Decl)(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open Core c

  field
    Π : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ

  Π= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty= Γ₂ A₀ A₁)
       {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : Ty= (,= Γ₂ A₂) B₀ B₁)
     → Ty= Γ₂ (Π A₀ B₀) (Π A₁ B₁)
  Π= = λ { {Γ₂ = refl} refl refl → refl }

  _^_ : ∀{Γ A} → Ty (Γ , A) → (C : Ty Γ) → Ty (Γ , C , wkT A)
  _^_ = λ { D C → {!!} }

  infixl 5 _^_

  field
    wkΠ : ∀{Γ C}{A : Ty Γ}{B : Ty (Γ , A)}
        → Ty= refl (wkT {Γ}{C} (Π A B)) (Π (wkT A) {!B!})
--    Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
--        → Ty= refl ((Π A B) [ σ ]T) (Π (A [ σ ]T) (B [ σ ^ A ]T))
    lam : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm (Γ , A) B → Tm Γ (Π A B)

  lam= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}{A₂ : Ty= Γ₂ A₀ A₁}
         {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}{B₂ : Ty= (,= Γ₂ A₂) B₀ B₁}
         {t₀ : Tm (Γ₀ , A₀) B₀}{t₁ : Tm (Γ₁ , A₁) B₁}(t₂ : Tm= (,= Γ₂ A₂) B₂ t₀ t₁)
       → Tm= Γ₂ (Π= A₂ B₂) (lam t₀) (lam t₁)
  lam= = λ { {Γ₂ = refl}{A₂ = refl}{B₂ = refl} refl → refl }
       
  field
    app : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm Γ (Π A B) → Tm (Γ , A) B

  app= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}{A₂ : Ty= Γ₂ A₀ A₁}
         {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}{B₂ : Ty= (,= Γ₂ A₂) B₀ B₁}
         {t₀ : Tm Γ₀ (Π A₀ B₀)}{t₁ : Tm Γ₁ (Π A₁ B₁)}(t₂ : Tm= Γ₂ (Π= A₂ B₂) t₀ t₁)
       → Tm= (,= Γ₂ A₂) B₂ (app t₀) (app t₁)
  app= = λ { {Γ₂ = refl}{A₂ = refl}{B₂ = refl} refl → refl }

  field
--     lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B}                       
--           → Tm= refl Π[] ((lam t) [ δ ]t) (lam (t [ δ ^ A ]t))
     Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
           → Tm= refl refl (app (lam t)) t
     Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
           → Tm= refl refl (lam (app t)) t
