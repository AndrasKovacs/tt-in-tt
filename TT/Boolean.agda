{-# OPTIONS --no-eta #-}

module TT.Boolean where

open import Agda.Primitive
open import lib
open import JM

open import TT.Decl
open import TT.Core
import TT.Core.Congr
import TT.Core.Laws
import TT.Decl.Congr

record Boolean {i j}(d : Decl)(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr d
  open Core c
  open TT.Core.Congr c
  open TT.Core.Laws c

  field
    Bool          : ∀{Γ} → Ty Γ
    Bool[]        : ∀{Γ Θ}{σ : Tms Γ Θ} → Bool [ σ ]T ≡ Bool
    true false    : ∀{Γ} → Tm Γ Bool
    true[]        : ∀{Γ Θ}{σ : Tms Γ Θ} → true [ σ ]t ≡[ TmΓ= Bool[] ]≡ true
    false[]       : ∀{Γ Θ}{σ : Tms Γ Θ} → false [ σ ]t ≡[ TmΓ= Bool[] ]≡ false
    if_then_else_ : ∀{Γ}{C : Ty (Γ , Bool)}(b : Tm Γ Bool) → Tm Γ (C [ < true > ]T) → Tm Γ (C [ < false > ]T)
                  → Tm Γ (C [ < b > ]T)
    ifthenelse[]  : ∀{Γ Θ}{σ : Tms Γ Θ}{C : Ty (Θ , Bool)}{b : Tm Θ Bool}
                    {c : Tm Θ (C [ < true > ]T)}{d : Tm Θ (C [ < false > ]T)}
                  → (if b then c else d) [ σ ]t
                  ≡[ TmΓ= ([<>][]' Bool[] b) ]≡
                    if   coe (TmΓ= Bool[]) (b [ σ ]t)
                    then coe (TmΓ= ([<>][] Bool[] true[])) (c [ σ ]t)
                    else coe (TmΓ= ([<>][] Bool[] false[])) (d [ σ ]t)
    trueβ         : ∀{Γ Θ}{σ : Tms Γ Θ}{C : Ty (Θ , Bool)}
                    {c : Tm Θ (C [ < true > ]T)}{d : Tm Θ (C [ < false > ]T)}
                  → if true then c else d ≡ c
    falseβ        : ∀{Γ Θ}{σ : Tms Γ Θ}{C : Ty (Θ , Bool)}
                    {c : Tm Θ (C [ < true > ]T)}{d : Tm Θ (C [ < false > ]T)}
                  → if false then c else d ≡ d
