{-# OPTIONS --rewriting #-}

module TT.ElimOld where

open import Agda.Primitive
open import lib
open import TT.Syntax
open import TT.Congr
open import TT.Laws

record Motives {i j} : Set (lsuc (i ⊔ j)) where
  field
    Conᴹ : Con → Set i
    Tyᴹ  : ∀{Γ} → Conᴹ Γ → Ty Γ → Set i
    Tmsᴹ : ∀{Γ Δ} → Conᴹ Γ → Conᴹ Δ → Tms Γ Δ → Set j
    Tmᴹ  : ∀{Γ}(Γᴹ : Conᴹ Γ) → {A : Ty Γ} → Tyᴹ Γᴹ A → Tm Γ A → Set j

  -- congruence rules

  TyΓᴹ= : {Γ : Con}{Γᴹ : Conᴹ Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
        → Tyᴹ Γᴹ A₀ ≡ Tyᴹ Γᴹ A₁
  TyΓᴹ= refl = refl

  ~TyΓᴹ== : {Γ : Con}{Γᴹ : Conᴹ Γ}
            {A₀ A₁ : Ty Γ}(A₂₀ A₂₁ : A₀ ≡ A₁)(A₂₂ : A₂₀ ≡ A₂₁)
            {A₀ᴹ : Tyᴹ Γᴹ A₀}{A₁ᴹ : Tyᴹ Γᴹ A₁}
          → (A₀ᴹ ≡[ TyΓᴹ= A₂₀ ]≡ A₁ᴹ) ≡ (A₀ᴹ ≡[ TyΓᴹ= A₂₁ ]≡ A₁ᴹ)
  ~TyΓᴹ== p (.p) refl = refl

  TmsΓΔᴹ= : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
            {δ₀ δ₁ : Tms Γ Δ}(δ₂ : δ₀ ≡ δ₁)
          → Tmsᴹ Γᴹ Δᴹ δ₀ ≡ Tmsᴹ Γᴹ Δᴹ δ₁
  TmsΓΔᴹ= refl = refl

  ~TmsΓΔᴹ== : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
              {δ₀ δ₁ : Tms Γ Δ}(δ₂₀ δ₂₁ : δ₀ ≡ δ₁)(δ₂₂ : δ₂₀ ≡ δ₂₁)
              {δ₀ᴹ : Tmsᴹ Γᴹ Δᴹ δ₀}{δ₁ᴹ : Tmsᴹ Γᴹ Δᴹ δ₁}
            → (δ₀ᴹ ≡[ TmsΓΔᴹ= δ₂₀ ]≡ δ₁ᴹ) ≡ (δ₀ᴹ ≡[ TmsΓΔᴹ= δ₂₁ ]≡ δ₁ᴹ)
  ~TmsΓΔᴹ== p (.p) refl = refl

  TmΓᴹ= : {Γ : Con}{Γᴹ : Conᴹ Γ}
          {A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
          {Aᴹ₀ : Tyᴹ Γᴹ A₀}{Aᴹ₁ : Tyᴹ Γᴹ A₁}(Aᴹ₂ : Aᴹ₀ ≡[ TyΓᴹ= A₂ ]≡ Aᴹ₁)
          {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}(t₂ : t₀ ≡[ TmΓ= A₂ ]≡ t₁)
        → Tmᴹ Γᴹ Aᴹ₀ t₀ ≡ Tmᴹ Γᴹ Aᴹ₁ t₁
  TmΓᴹ= refl refl refl = refl

  TmΓAᴹ= : {Γ : Con}{Γᴹ : Conᴹ Γ}
           {A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
           {t₀ t₁ : Tm Γ A}(t₂ : t₀ ≡ t₁)
         → Tmᴹ Γᴹ Aᴹ t₀ ≡ Tmᴹ Γᴹ Aᴹ t₁
  TmΓAᴹ= refl = refl

  ~TmΓAᴹ== : {Γ : Con}{Γᴹ : Conᴹ Γ}
             {A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
             {t₀ t₁ : Tm Γ A}(t₂₀ t₂₁ : t₀ ≡ t₁)(t₂₂ : t₂₀ ≡ t₂₁)
             {t₀ᴹ : Tmᴹ Γᴹ Aᴹ t₀}{t₁ᴹ : Tmᴹ Γᴹ Aᴹ t₁}
           → (t₀ᴹ ≡[ TmΓAᴹ= t₂₀ ]≡ t₁ᴹ) ≡ (t₀ᴹ ≡[ TmΓAᴹ= t₂₁ ]≡ t₁ᴹ)
  ~TmΓAᴹ== p (.p) refl = refl


record MethodsCon {i j}(M : Motives {i}{j}) : Set (i ⊔ j) where
  open Motives M
  field
    •ᴹ     : Conᴹ •
    _,Cᴹ_  : ∀{Γ}(Γᴹ : Conᴹ Γ){A : Ty Γ} → Tyᴹ Γᴹ A → Conᴹ (Γ , A)

  infixl 4 _,Cᴹ_

record MethodsTy {i j}(M : Motives {i}{j})(mCon : MethodsCon M) : Set (i ⊔ j) where
  open Motives    M
  open MethodsCon mCon
  field
    _[_]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A)
             {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ) → Tyᴹ Γᴹ (A [ δ ]T)
    Uᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ} → Tyᴹ Γᴹ U
    Elᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Â : Tm Γ U}(Âᴹ : Tmᴹ Γᴹ Uᴹ Â) → Tyᴹ Γᴹ (El Â)
    Πᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A){B : Ty (Γ , A)}
             (Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B) → Tyᴹ Γᴹ (Π A B)

  infixl 7 _[_]Tᴹ

record MethodsTms {i j}(M : Motives {i}{j})(mCon : MethodsCon M)(mTy : MethodsTy M mCon) : Set (i ⊔ j) where
  open Motives    M
  open MethodsCon mCon
  open MethodsTy  mTy
  field
    εᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ} → Tmsᴹ Γᴹ •ᴹ ε
    _,sᴹ_  : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
             {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{t : Tm Γ (A [ δ ]T)}(tᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) t)
           → Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) (δ ,s t)
    idᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ} → Tmsᴹ Γᴹ Γᴹ id
    _∘ᴹ_   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{σ : Tms Δ Σ}(σᴹ : Tmsᴹ Δᴹ Σᴹ σ)
             {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ) → Tmsᴹ Γᴹ Σᴹ (σ ∘ δ)
    π₁ᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {δ : Tms Γ (Δ , A)}(δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ) → Tmsᴹ Γᴹ Δᴹ (π₁ δ)

  infixl 4 _,sᴹ_
  infix 6 _∘ᴹ_

record MethodsTm {i j}(M : Motives {i}{j})(mCon : MethodsCon M)(mTy : MethodsTy M mCon)
                 (mTms : MethodsTms M mCon mTy) : Set (i ⊔ j) where
  open Motives M
  open MethodsCon mCon
  open MethodsTy  mTy
  open MethodsTms mTms
  field
    _[_]tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{t : Tm Δ A} → Tmᴹ Δᴹ Aᴹ t
           → {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ) → Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) (t [ δ ]t)
    π₂ᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {δ : Tms Γ (Δ , A)}(δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ)→ Tmᴹ Γᴹ (Aᴹ [ π₁ᴹ δᴹ ]Tᴹ) (π₂ δ)
    appᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm Γ (Π A B)} → Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) t → Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ (app t)
    lamᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm (Γ , A) B} → Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ t → Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) (lam t)

  infixl 8 _[_]tᴹ
  
  -- another congruence rule
  []Tᴹ= : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A)
            {δ₀ δ₁ : Tms Γ Δ}(δ₂ : δ₀ ≡ δ₁)
            {δ₀ᴹ : Tmsᴹ Γᴹ Δᴹ δ₀}{δ₁ᴹ : Tmsᴹ Γᴹ Δᴹ δ₁}(δ₂ᴹ : δ₀ᴹ ≡[ TmsΓΔᴹ= δ₂ ]≡ δ₁ᴹ)
          → Aᴹ [ δ₀ᴹ ]Tᴹ ≡[ TyΓᴹ= (ap (λ z → A [ z ]T) δ₂) ]≡ Aᴹ [ δ₁ᴹ ]Tᴹ
  []Tᴹ= = λ { Aᴹ refl refl → refl }

record MethodsHTy {i}{j}(M : Motives {i}{j})
                  (mCon : MethodsCon M)
                  (mTy  : MethodsTy  M mCon)
                  (mTms : MethodsTms M mCon mTy)
                  (mTm  : MethodsTm  M mCon mTy mTms) : Set (i ⊔ j) where
  open Motives    M
  open MethodsCon mCon
  open MethodsTy  mTy
  open MethodsTms mTms
  open MethodsTm  mTm
  field
    [id]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
           → Aᴹ [ idᴹ ]Tᴹ ≡[ TyΓᴹ= [id]T ]≡ Aᴹ
    [][]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{A : Ty Σ}{Aᴹ : Tyᴹ Σᴹ A}
             {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}{δ : Tms Δ Σ}{δᴹ : Tmsᴹ Δᴹ Σᴹ δ}
           → (Aᴹ [ δᴹ ]Tᴹ) [ σᴹ ]Tᴹ ≡[ TyΓᴹ= [][]T ]≡ Aᴹ [ δᴹ ∘ᴹ σᴹ ]Tᴹ
    U[]ᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
           → Uᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ= U[] ]≡ Uᴹ
    El[]ᴹ  : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {Â : Tm Δ U}{Âᴹ : Tmᴹ Δᴹ Uᴹ Â}
           → Elᴹ Âᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ= El[] ]≡ Elᴹ (coe (TmΓᴹ= U[] U[]ᴹ refl) (Âᴹ [ δᴹ ]tᴹ))

  _^ᴹ_ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
         {A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A) → Tmsᴹ (Γᴹ ,Cᴹ Aᴹ [ δᴹ ]Tᴹ) (Δᴹ ,Cᴹ Aᴹ) (δ ^ A)
  _^ᴹ_ = λ δᴹ Aᴹ → (δᴹ ∘ᴹ π₁ᴹ idᴹ) ,sᴹ coe (TmΓᴹ= [][]T [][]Tᴹ refl) (π₂ᴹ idᴹ)

  infixl 5 _^ᴹ_

  field
    Π[]ᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}{A : Ty Δ}
             {Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
           → Πᴹ Aᴹ Bᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ= Π[] ]≡ Πᴹ (Aᴹ [ δᴹ ]Tᴹ) (Bᴹ [ δᴹ ^ᴹ Aᴹ ]Tᴹ)

record MethodsHTms {i}{j}(M    : Motives {i}{j})
                   (mCon : MethodsCon M)
                   (mTy  : MethodsTy  M mCon)
                   (mTms : MethodsTms M mCon mTy)
                   (mTm  : MethodsTm  M mCon mTy mTms)
                   (mHTy : MethodsHTy M mCon mTy mTms mTm) : Set (i ⊔ j) where
  open Motives    M
  open MethodsCon mCon
  open MethodsTy  mTy
  open MethodsTms mTms
  open MethodsTm  mTm
  open MethodsHTy mHTy
  field
    idlᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
           → (idᴹ ∘ᴹ δᴹ) ≡[ TmsΓΔᴹ= idl ]≡ δᴹ
    idrᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
           → (δᴹ ∘ᴹ idᴹ) ≡[ TmsΓΔᴹ= idr ]≡ δᴹ
    assᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{Ω}{Ωᴹ : Conᴹ Ω}
             {σ : Tms Σ Ω}{σᴹ : Tmsᴹ Σᴹ Ωᴹ σ}{δ : Tms Γ Σ}{δᴹ : Tmsᴹ Γᴹ Σᴹ δ}
             {ν : Tms Δ Γ}{νᴹ : Tmsᴹ Δᴹ Γᴹ ν}
           → ((σᴹ ∘ᴹ δᴹ) ∘ᴹ νᴹ) ≡[ TmsΓΔᴹ= ass ]≡ (σᴹ ∘ᴹ (δᴹ ∘ᴹ νᴹ))
    π₁βᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {a : Tm Γ (A [ δ ]T)}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a}
           → (π₁ᴹ (δᴹ ,sᴹ aᴹ)) ≡[ TmsΓΔᴹ= π₁β ]≡ δᴹ
    πηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {δ : Tms Γ (Δ , A)}{δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ}
           → (π₁ᴹ δᴹ ,sᴹ π₂ᴹ δᴹ) ≡[ TmsΓΔᴹ= πη ]≡ δᴹ
    εηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{σ : Tms Γ •}{σᴹ : Tmsᴹ Γᴹ •ᴹ σ}
           → σᴹ ≡[ TmsΓΔᴹ= εη ]≡ εᴹ
    ,∘ᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}
             {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {σ : Tms Σ Γ}{σᴹ : Tmsᴹ Σᴹ Γᴹ σ}
             {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {a : Tm Γ (A [ δ ]T)}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a}
           → ((δᴹ ,sᴹ aᴹ) ∘ᴹ σᴹ) ≡[ TmsΓΔᴹ= ,∘ ]≡ ((δᴹ ∘ᴹ σᴹ) ,sᴹ coe (TmΓᴹ= [][]T [][]Tᴹ refl) (aᴹ [ σᴹ ]tᴹ))

record MethodsHTm  {i}{j}(M     : Motives {i}{j})
                   (mCon  : MethodsCon  M)
                   (mTy   : MethodsTy   M mCon)
                   (mTms  : MethodsTms  M mCon mTy)
                   (mTm   : MethodsTm   M mCon mTy mTms)
                   (mHTy  : MethodsHTy  M mCon mTy mTms mTm)
                   (mHTms : MethodsHTms M mCon mTy mTms mTm mHTy) : Set (i ⊔ j) where
  open Motives     M
  open MethodsCon  mCon
  open MethodsTy   mTy
  open MethodsTms  mTms
  open MethodsTm   mTm
  open MethodsHTy  mHTy
  open MethodsHTms mHTms
  field
    π₂βᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {a : Tm Γ (A [ δ ]T)}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a}
           → π₂ᴹ (δᴹ ,sᴹ aᴹ) ≡[ TmΓᴹ= (ap (λ z → A [ z ]T) π₁β) ([]Tᴹ= Aᴹ π₁β π₁βᴹ) π₂β ]≡ aᴹ
    lam[]ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
             {t : Tm (Δ , A) B}{tᴹ : Tmᴹ (Δᴹ ,Cᴹ Aᴹ) Bᴹ t}
           → (lamᴹ tᴹ) [ δᴹ ]tᴹ ≡[ TmΓᴹ= Π[] Π[]ᴹ lam[] ]≡ lamᴹ (tᴹ [ δᴹ ^ᴹ Aᴹ ]tᴹ)
    Πβᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
             {B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm (Γ , A) B}{tᴹ : Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ t}
           → appᴹ (lamᴹ tᴹ) ≡[ TmΓAᴹ= Πβ ]≡ tᴹ
    Πηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
             {B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm Γ (Π A B)}{tᴹ : Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) t}
           → lamᴹ (appᴹ tᴹ) ≡[ TmΓAᴹ= Πη ]≡ tᴹ

module elim {i}{j}(M : Motives {i}{j})
            (mCon  : MethodsCon  M)
            (mTy   : MethodsTy   M mCon)
            (mTms  : MethodsTms  M mCon mTy)
            (mTm   : MethodsTm   M mCon mTy mTms)
            (mHTy  : MethodsHTy  M mCon mTy mTms mTm)
            (mHTms : MethodsHTms M mCon mTy mTms mTm mHTy)
            (mHTm  : MethodsHTm  M mCon mTy mTms mTm mHTy mHTms) where
  open Motives     M
  open MethodsCon  mCon
  open MethodsTy   mTy
  open MethodsTms  mTms
  open MethodsTm   mTm

  postulate
    Con-elim : ∀ Γ → Conᴹ Γ
    Ty-elim  : ∀{Γ}(A : Ty Γ) → Tyᴹ (Con-elim Γ) A
    Tms-elim : ∀{Γ Δ}(δ : Tms Γ Δ) → Tmsᴹ (Con-elim Γ) (Con-elim Δ) δ
    Tm-elim  : ∀{Γ}{A : Ty Γ}(t : Tm Γ A) → Tmᴹ (Con-elim Γ) (Ty-elim A) t

  postulate
    β• : Con-elim • ≡ •ᴹ
    β, : ∀{Γ A} → Con-elim (Γ , A) ≡ (Con-elim Γ ,Cᴹ Ty-elim A)
    
  {-# REWRITE β• #-}
  {-# REWRITE β, #-}
  
  postulate
    β[]T : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ} → Ty-elim (A [ δ ]T) ≡ Ty-elim A [ Tms-elim δ ]Tᴹ

  {-# REWRITE β[]T #-}

  postulate
    βU   : ∀{Γ} → Ty-elim (U {Γ}) ≡ Uᴹ
    
  {-# REWRITE βU #-}

  postulate
    βEl  : ∀{Γ}{Â : Tm Γ U} → Ty-elim (El Â) ≡ Elᴹ (Tm-elim Â)
    
  {-# REWRITE βEl #-}

  postulate
    βΠ   : ∀{Γ A B} → Ty-elim (Π {Γ} A B) ≡ Πᴹ (Ty-elim A) (Ty-elim B)

  {-# REWRITE βΠ #-}
  
  postulate
    βε  : ∀{Γ} → Tms-elim (ε {Γ}) ≡ εᴹ
    β,s : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ (A [ σ ]T)}
        → (Tms-elim (σ ,s t)) ≡ (Tms-elim σ ,sᴹ Tm-elim t)
    βid : ∀{Γ} → Tms-elim (id {Γ}) ≡ idᴹ
    β∘  : ∀{Γ Δ Σ}{σ : Tms Δ Σ}{ν : Tms Γ Δ}
        → Tms-elim (σ ∘ ν) ≡ Tms-elim σ ∘ᴹ Tms-elim ν
    βπ₁ : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ , A)}
        → Tms-elim (π₁ σ) ≡ π₁ᴹ (Tms-elim σ)
  
  {-# REWRITE βid #-}
  {-# REWRITE β∘  #-}
  {-# REWRITE βε  #-}
  {-# REWRITE β,s #-}
  {-# REWRITE βπ₁ #-}

  postulate
    β[]t : ∀{Γ Δ}{A : Ty Δ}{t : Tm Δ A}{σ : Tms Γ Δ}
         → Tm-elim (t [ σ ]t) ≡ Tm-elim t [ Tms-elim σ ]tᴹ
    βπ₂  : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ , A)}
         → Tm-elim (π₂ σ)     ≡ π₂ᴹ (Tms-elim σ)
    βapp : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
         → Tm-elim (app t)    ≡ appᴹ (Tm-elim t)
    βlam : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
         → Tm-elim (lam t)    ≡ lamᴹ (Tm-elim t)
         
  {-# REWRITE β[]t #-}
  {-# REWRITE βπ₂  #-}
  {-# REWRITE βlam #-}
  {-# REWRITE βapp #-}
