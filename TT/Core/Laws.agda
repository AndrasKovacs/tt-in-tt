{-# OPTIONS --no-eta #-}

open import Agda.Primitive
open import TT.Decl
open import TT.Core

module TT.Core.Laws {i}{j}{d : Decl {i}{j}}(c : Core d) where

open import lib
open import JM

open Decl d
open Core c
open import TT.Decl.Congr d
open import TT.Core.Congr c

------------------------------------------------------------------------------
-- Con
------------------------------------------------------------------------------

abstract
  ,[],
    : {A : ∀{Γ} → Ty Γ}(A[] : ∀{Γ Θ}{σ : Tms Γ Θ} → A [ σ ]T ≡ A)
      {Γ Θ : Con}{σ : Tms Γ Θ}{P : Ty (Θ , A)}
    → Γ , A [ σ ]T , P [ σ ^ A ]T
    ≡ Γ , A , P [ coe (Tms-Γ= (,C= refl A[])) (σ ^ A) ]T
  ,[], A[] = ,C= (,C= refl A[]) ([]T=' (,C= refl A[]) refl refl)

------------------------------------------------------------------------------
-- Tms (1)
------------------------------------------------------------------------------

abstract
  π₁∘ : ∀{Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
       → π₁ δ ∘ ρ ≡ π₁ (δ ∘ ρ)
  π₁∘ {Γ}{Δ}{Θ}{A}{δ}{ρ}
    = π₁β ⁻¹ ◾ ap π₁ (,∘ ⁻¹) ◾ ap (λ σ → π₁ (σ ∘ ρ)) πη

abstract
  π₁idβ : ∀{Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ (A [ ρ ]T)}
        → π₁ id ∘ (ρ ,s t) ≡ ρ
  π₁idβ = π₁∘ ◾ ap π₁ idl ◾ π₁β

abstract
  ∘π₁id : ∀{Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}
        → ρ ∘ π₁ {A = A [ ρ ]T} id ≡ π₁ id ∘ (ρ ^ A)
  ∘π₁id {Γ}{Δ}{ρ}{A} = π₁β ⁻¹ ◾ ap π₁ idl ⁻¹ ◾ π₁∘ ⁻¹

abstract
  π₁id∘ : ∀{Γ Δ}{A : Ty Δ}{ρ : Tms Γ (Δ , A)}
        → π₁ id ∘ ρ ≡ π₁ ρ
  π₁id∘ = π₁∘ ◾ π₁= refl refl refl idl

abstract
  id^ : ∀{Γ}{A : Ty Γ} → id ^ A ≃ id {Γ , A}
  id^ {Γ}{A} = ,s≃ (,C≃ refl (to≃ [id]T))
                   refl
                   (to≃ idl ◾̃ π₁id≃ refl (to≃ [id]T))
                   r̃
                   (uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ π₂id≃ refl (to≃ [id]T))
             ◾̃ to≃ πη

abstract
  π₁id∘coe : ∀{Δ Γ₀ Γ₁}{A : Ty Δ}(Γ₂ : Γ₀ ≡ Γ₁){σ : Tms Γ₀ (Δ , A)}
           → π₁ id ∘ coe (Tms-Γ= Γ₂) σ
           ≡ coe (Tms-Γ= Γ₂) (π₁ σ)
  π₁id∘coe refl = π₁∘ ◾ ap π₁ idl

abstract
  π₁id∘coe' : ∀{Δ Γ₀ Γ₁}{A : Ty Δ}(Γ₂ : Γ₀ ≡ Γ₁){σ : Tms Γ₀ (Δ , A)}
           → π₁ id ∘ coe (Tms-Γ= Γ₂) σ
           ≃ π₁ σ
  π₁id∘coe' refl = to≃ (π₁∘ ◾ ap π₁ idl)

abstract
  π₁id∘coe⁻¹ : ∀{Δ Γ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){σ : Tms Γ (Δ , A₁)}
           → π₁ id ∘ coe (TmsΓ-= (,C≃ refl (to≃ A₂) ⁻¹)) σ
           ≃ π₁ σ
  π₁id∘coe⁻¹ refl = to≃ (π₁∘ ◾ ap π₁ idl)

------------------------------------------------------------------------------
-- Tm (1)
------------------------------------------------------------------------------

abstract
  π₂β' : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
       → π₂ (δ ,s a) ≃ a
  π₂β' {A = A} = from≡ (TmΓ= (ap (_[_]T A) π₁β)) π₂β

-- other naturality for comprehension

abstract
  π₂[]' : ∀{Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
        → π₂ δ [ ρ ]t ≃ π₂ (δ ∘ ρ)
  π₂[]' = uncoe (TmΓ= [][]T)
        ◾̃ π₂β' ⁻¹̃
        ◾̃ π₂≃ (,∘ ⁻¹)
        ◾̃ π₂≃ (ap (λ z → z ∘ _) πη)

abstract
  π₂id[]' : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
          → π₂ id [ δ ]t ≃ π₂ δ
  π₂id[]' = π₂[]' ◾̃ π₂≃ idl

abstract
  π₂idβ : ∀{Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ (A [ ρ ]T)}
        → π₂ id [ ρ ,s t ]t ≃ t
  π₂idβ = π₂[]' ◾̃ π₂≃ idl ◾̃ π₂β'

abstract
  π₂coe : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}{A : Ty Δ}{ρ : Tms Γ₀ (Δ , A)}
        → π₂ id [ coe (Tms-Γ= Γ₂) ρ ]t
        ≃ π₂ ρ
  π₂coe refl = π₂[]' ◾̃ π₂≃ idl

abstract
  π₂coe⁻¹ : ∀{Γ Δ}{A₀ A₁ : Ty Δ}{ρ : Tms Γ (Δ , A₁)}(A₂ : A₀ ≡ A₁)
        → π₂ id [ coe (TmsΓ-= (,C≃ refl (to≃ A₂) ⁻¹)) ρ ]t
        ≃ π₂ ρ
  π₂coe⁻¹ refl = π₂[]' ◾̃ π₂≃ idl

-- categorical laws for term substitution

abstract
  [id]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≡[ TmΓ= [id]T ]≡ t
  [id]t {Γ}{A}{t}

    = from≃ ( uncoe (TmΓ= [id]T) ⁻¹̃
            ◾̃ from≡ (TmΓ= (ap (_[_]T A) π₁β)) (π₂β {a = t [ id ]t}) ⁻¹̃
            ◾̃ π₂≃ (,s≃' (idl ⁻¹){_}{t [ id ]t}
                        {coe (TmΓ= [][]T)
                             (coe (TmΓ= ([id]T ⁻¹)) t [ id ]t)}
                        (coe[]t' ([id]T ⁻¹) ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T)))
            ◾̃ π₂≃ (,∘ ⁻¹)
            ◾̃ π₂≃ idr
            ◾̃ π₂β'
            ◾̃ uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃)

abstract
  [id]t' : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≃ t
  [id]t' = from≡ (TmΓ= [id]T) [id]t

abstract
  [][]t : ∀{Γ Δ Σ}{A : Ty Σ}{t : Tm Σ A}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → (t [ δ ]t) [ σ ]t ≡[ TmΓ= [][]T ]≡  t [ δ ∘ σ ]t
  [][]t {Γ}{Δ}{Σ}{A}{t}{σ}{δ}

    = from≃ ( uncoe (TmΓ= [][]T) ⁻¹̃
            ◾̃ []t≃' refl refl
                    (to≃ (ap (_[_]T A) (idl ⁻¹) ◾ [][]T ⁻¹))
                    (coe[]t' ([id]T ⁻¹) ⁻¹̃) r̃
            ◾̃ coe[]t' [][]T ⁻¹̃
            ◾̃ uncoe (TmΓ= [][]T)
            ◾̃ π₂β' ⁻¹̃
            ◾̃ π₂≃ (,∘ ⁻¹)
            ◾̃ π₂≃ (ap (λ z → z ∘ σ) (,∘ ⁻¹))
            ◾̃ π₂≃ ass
            ◾̃ π₂≃ ,∘
            ◾̃ π₂β' {δ = id ∘ (δ ∘ σ)}
                   {coe (TmΓ= [][]T) (coe (TmΓ= ([id]T ⁻¹)) t [ δ ∘ σ ]t)}
            ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
            ◾̃ coe[]t' ([id]T ⁻¹))

abstract
  [][]t' : ∀{Γ Δ Σ}{A : Ty Σ}{t : Tm Σ A}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
         → t [ δ ]t [ σ ]t ≃ t [ δ ∘ σ ]t
  [][]t' = from≡ (TmΓ= [][]T) [][]t

------------------------------------------------------------------------------
-- Tms (2)
------------------------------------------------------------------------------

abstract
  ∘^ : ∀{Γ Δ Θ}{δ : Tms Δ Θ}{σ : Tms Γ Δ}{A : Ty Θ}
     → ((δ ∘ σ) ^ A) ≃ ((δ ^ A) ∘ (σ ^ (A [ δ ]T)))
  ∘^ {Γ}{Δ}{Θ}{δ}{σ}{A}

    = ,s≃ p
          refl
          ( to≃ ass
          ◾̃ ∘≃ p
               ( ∘≃ p (π₁id≃ refl q)
               ◾̃ to≃ (π₁β ⁻¹ ◾ ap π₁ (idl ⁻¹) ◾ π₁∘ ⁻¹))
          ◾̃ to≃ (ass ⁻¹))
          r̃
          ( uncoe (TmΓ= [][]T) ⁻¹̃
          ◾̃ π₂id≃ refl q
          ◾̃ uncoe (TmΓ= [][]T)
          ◾̃ π₂idβ ⁻¹̃
          ◾̃ coecoe[]t [][]T [][]T ⁻¹̃)
    ◾̃ to≃ (,∘ ⁻¹)
    where
      q : A [ δ ∘ σ ]T ≃ A [ δ ]T [ σ ]T
      q = to≃ ([][]T ⁻¹)
      p : _≡_ {A = Con} (Γ , A [ δ ∘ σ ]T) (Γ , A [ δ ]T [ σ ]T)
      p = ,C≃ refl q

abstract
  ∘^^ : ∀{Γ Δ Θ}{δ : Tms Δ Θ}{σ : Tms Γ Δ}{A : Ty Θ}{B : Ty (Θ , A)}
     → ((δ ∘ σ) ^ A ^ B) ≃ ((δ ^ A ^ B) ∘ (σ ^ A [ δ ]T ^ B [ δ ^ A ]T))
  ∘^^ = ^≃' (,C≃ refl (to≃ ([][]T ⁻¹))) ∘^ ◾̃ ∘^

abstract
  ^∘<> : ∀{Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{B : Ty Γ}{t : Tm Γ B}(p : A [ ρ ]T ≡ B)
       → coe (Tms-Γ= (,C≃ refl (to≃ p))) (ρ ^ A) ∘ < t > ≡ (ρ ,s coe (TmΓ= (p ⁻¹)) t)
  ^∘<> {ρ = ρ} refl

    = ,∘
    ◾ ,s≃' ( ass
           ◾ ap (_∘_ ρ)
                (π₁∘ ◾ ap π₁ idl ◾ π₁β)
           ◾ idr)
           ( coecoe[]t [][]T [][]T
           ◾̃ π₂[]'
           ◾̃ π₂≃ idl
           ◾̃ π₂β'
           ◾̃ uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃)

abstract
  ^∘<>' : ∀{Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{B : Ty Γ}{t : Tm Γ B}(p : A [ ρ ]T ≡ B)
        → coe (Tms-Γ= (,C= refl p)) (ρ ^ A) ∘ < t > ≡ (ρ ,s coe (TmΓ= (p ⁻¹)) t)
  ^∘<>' {ρ = ρ} refl

    = ,∘
    ◾ ,s≃' ( ass
           ◾ ap (_∘_ ρ)
                (π₁∘ ◾ ap π₁ idl ◾ π₁β)
           ◾ idr)
           ( coecoe[]t [][]T [][]T
           ◾̃ π₂[]'
           ◾̃ π₂≃ idl
           ◾̃ π₂β'
           ◾̃ uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃)

abstract
  <>∘ : ∀{Γ Δ A}{u : Tm Δ A}{δ : Tms Γ Δ}
      → < u > ∘ δ ≡ (δ ^ A) ∘ < u [ δ ]t >
  <>∘ {Γ}{Δ}{A}{u}{δ}
    = ,∘
    ◾ ,s≃' (idl ◾ idr ⁻¹ ◾ ap (_∘_ δ) (π₁idβ ⁻¹) ◾ ass ⁻¹)
           (uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ coe[]t' ([id]T ⁻¹) ◾̃ uncoe (TmΓ= ([id]T ⁻¹)) ◾̃ π₂idβ ⁻¹̃ ◾̃ coe[]t' [][]T ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T))
    ◾ (,∘ ⁻¹)

abstract
  ,→^ : ∀{Γ Δ}{A : Ty Δ}{ρ : Tms Γ Δ}{u : Tm Γ (A [ ρ ]T)}
      → ρ ,s u ≡ (ρ ^ A) ∘ < u >
  ,→^ = ,s≃' (idr ⁻¹ ◾ ap (_∘_ _) (π₁idβ ⁻¹) ◾ ass ⁻¹)
             ( uncoe (TmΓ= ([id]T ⁻¹))
             ◾̃ π₂β' ⁻¹̃
             ◾̃ π₂≃ idl ⁻¹̃
             ◾̃ π₂[]' ⁻¹̃
             ◾̃ coe[]t' [][]T ⁻¹̃
             ◾̃ uncoe (TmΓ= [][]T))
      ◾ ,∘ ⁻¹

------------------------------------------------------------------------------
-- Ty
------------------------------------------------------------------------------

abstract
  [wk][id,]
    : {Γ : Con}{A B : Ty Γ}{u : Tm Γ (B [ id ]T)}
    → A ≡ A [ wk ]T [ id ,s u ]T
  [wk][id,] {Γ}{A}{B}{u}
    = [id]T ⁻¹
    ◾ ap
        (_[_]T A)
        ( π₁β ⁻¹
        ◾ π₁id∘ ⁻¹)
    ◾ [][]T ⁻¹

abstract
  [wk][wk][id,,]
    : {Γ : Con}{A B : Ty Γ}{C : Ty (Γ , B)}{u : Tm Γ (B [ id ]T)}{v : Tm Γ (C [ id ,s u ]T)}
    → A ≡ A [ wk ]T [ wk ]T [ id ,s u ,s v ]T
  [wk][wk][id,,] {Γ}{A}{B}{C}{u}{v}
    = [id]T ⁻¹
    ◾ ap
        (_[_]T A)
        ( π₁β ⁻¹
        ◾ ap π₁ (π₁β ⁻¹ ◾ π₁id∘ ⁻¹)
        ◾ π₁id∘ ⁻¹)
    ◾ [][]T ⁻¹
    ◾ [][]T ⁻¹

abstract
  [<>][]
    : {A : ∀{Γ} → Ty Γ}(A[] : ∀{Γ Θ}{σ : Tms Γ Θ} → A [ σ ]T ≡ A)
      {t : ∀{Γ} → Tm Γ A}(t[] : ∀{Γ Θ}{σ : Tms Γ Θ} → t [ σ ]t ≡[ TmΓ= A[] ]≡ t)
      {Γ Θ : Con}{σ : Tms Γ Θ}{C : Ty (Θ , A)}
    → C [ < t > ]T [ σ ]T ≡ C [ coe (Tms-Γ= (,C= refl A[])) (σ ^ A) ]T [ < t > ]T
  [<>][] A[] t[] = [][]T ◾ ap (_[_]T _) (<>∘ ◾ ∘=' (,C= refl A[]) refl (<>=' A[] t[])) ◾ [][]T ⁻¹

abstract
  [<>][]'
    : {A : ∀{Γ} → Ty Γ}(A[] : ∀{Γ Θ}{σ : Tms Γ Θ} → A [ σ ]T ≡ A)
      {Γ Θ : Con}(t : Tm Θ A){σ : Tms Γ Θ}{C : Ty (Θ , A)}
    → C [ < t > ]T [ σ ]T ≡ C [ coe (Tms-Γ= (,C= refl A[])) (σ ^ A) ]T [ < coe (TmΓ= A[]) (t [ σ ]t) > ]T
  [<>][]' A[] t = [][]T ◾ ap (_[_]T _) (<>∘ ◾ ∘=' (,C= refl A[]) refl (<>=' A[] refl)) ◾ [][]T ⁻¹

abstract
  [][^^]
    : {A : ∀{Γ} → Ty Γ}(A[] : ∀{Γ Θ}{σ : Tms Γ Θ} → A [ σ ]T ≡ A)
      (s : ∀{Γ} → Tm Γ A → Tm Γ A)(s[] : ∀{Γ Θ}{σ : Tms Γ Θ}{a : Tm Θ A} → s a [ σ ]t ≡[ TmΓ= A[] ]≡ s (coe (TmΓ= A[]) (a [ σ ]t)))
      {Γ Θ : Con}{σ : Tms Γ Θ}{P : Ty (Θ , A)}
    → P [ [ A[] , s ] ∘ wk ]T [ σ ^ A ^ P ]T
    ≡[ Ty= (,[], A[]) ]≡
      P [ coe (Tms-Γ= (,C= refl A[])) (σ ^ A) ]T [ [ A[] , s ] ∘ wk ]T
  [][^^] {A} A[] s s[]
    = from≃ ( uncoe (Ty= (,[], A[])) ⁻¹̃
            ◾̃ to≃ [][]T
            ◾̃ []T≃ (,[], A[])
                   ( to≃ ass
                   ◾̃ to≃ (ap (_∘_ [ A[] , s ]) π₁idβ)
                   ◾̃ to≃ (ass ⁻¹)
                   ◾̃ ∘≃' (,C= refl A[]) refl (,[], A[])
                         ( to≃ ,∘
                         ◾̃ ,s≃ (,C= refl A[]) refl
                               ( to≃ π₁idβ
                               ◾̃ ∘≃ (,C= refl A[])
                                    ( π₁id≃ refl (to≃ A[])
                                    ◾̃ to≃ (π₁idβ ⁻¹)
                                    ◾̃ ∘≃' (,C= refl (A[] ⁻¹)) refl refl (π₁id≃ refl (to≃ (A[] ⁻¹))) (uncoe (TmsΓ-= (,C= refl (A[] ⁻¹)))))
                               ◾̃ to≃ (ass ⁻¹))
                               r̃
                               ( uncoe (TmΓ= [][]T) ⁻¹̃
                               ◾̃ coe[]t' (A[] ⁻¹)
                               ◾̃ from≡ (TmΓ= A[]) s[]
                               ◾̃ suc'≃ {A}{s} (,C= refl A[]) (uncoe (TmΓ= A[]) ⁻¹̃ ◾̃ coe[]t' A[] ◾̃ π₂idβ ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ π₂id≃ refl (to≃ A[]) ◾̃ uncoe (TmΓ= A[]))
                               ◾̃ uncoe (TmΓ= (A[] ⁻¹))
                               ◾̃ π₂idβ ⁻¹̃
                               ◾̃ []t≃' refl (,C= refl (A[] ⁻¹)) (to≃ A[] ◾̃ APPi≃ {A = Con}{Ty} refl {A} r̃ (,C= refl (A[] ⁻¹)) ◾̃ to≃ (A[] ⁻¹) ◾̃ to≃ ([][]T ⁻¹)) (π₂id≃ refl (to≃ (A[] ⁻¹))) (uncoe (TmsΓ-= (,C= refl (A[] ⁻¹))))
                               ◾̃ coe[]t' ([][]T) ⁻¹̃
                               ◾̃ uncoe (TmΓ= [][]T))
                         ◾̃ to≃ (,∘ ⁻¹)
                         ◾̃ ∘≃' (,C= refl A[]) refl refl (uncoe (Tms-Γ= (,C= refl A[]))){coe (TmsΓ-= (,C= refl (A[] ⁻¹))) [ A[] , s ]}{[ A[] , s ]} (uncoe (TmsΓ-= (,C= refl (A[] ⁻¹))) ⁻¹̃))
                         (π₁id≃ (,C= refl A[]) ([]T≃ (,C= refl A[]) (uncoe (Tms-Γ= (,C= refl A[]))))) 
                   ◾̃ to≃ ass)
            ◾̃ to≃ ([][]T ⁻¹))

abstract
  [<suc>]
    : {A : ∀{Γ} → Ty Γ}(A[] : ∀{Γ Θ}{σ : Tms Γ Θ} → A [ σ ]T ≡ A)
      (s : ∀{Γ} → Tm Γ A → Tm Γ A)(s[] : ∀{Γ Θ}{σ : Tms Γ Θ}{a : Tm Θ A} → s a [ σ ]t ≡[ TmΓ= A[] ]≡ s (coe (TmΓ= A[]) (a [ σ ]t)))
      {Γ : Con}{P : Ty (Γ , A)}{a : Tm Γ A}{w : Tm Γ (P [ < a > ]T)}
    → P [ < s a > ]T
    ≡ P [ [ A[] , s ] ∘ wk ]T [ < a > ,s w ]T
  [<suc>] {A} A[] s s[] {P = P}{a}{w}
    = ap (P [_]T)
         ( ,s= refl refl (π₁idβ ⁻¹) refl
               (from≃ ( uncoe (Tm= refl ([]T= refl refl refl (π₁idβ ⁻¹))) ⁻¹̃
                      ◾̃ uncoe (TmΓ= ([id]T ⁻¹)) ⁻¹̃
                      ◾̃ suc'≃ {A}{s} refl
                              ( uncoe (TmΓ= ([id]T ⁻¹))
                              ◾̃ π₂idβ ⁻¹̃
                              ◾̃ coe[]t' A[] ⁻¹̃
                              ◾̃ uncoe (TmΓ= A[]))
                      ◾̃ from≡ (TmΓ= A[]) s[] ⁻¹̃
                      ◾̃ coe[]t' (A[] ⁻¹) ⁻¹̃
                      ◾̃ uncoe (TmΓ= [][]T)))
         ◾ ,∘ ⁻¹
         ◾ ap ([ A[] , s ] ∘_) (π₁idβ ⁻¹)
         ◾ ass ⁻¹)
    ◾ [][]T ⁻¹

------------------------------------------------------------------------------
-- Tm (2)
------------------------------------------------------------------------------

abstract
  vsvz[<vz>] : {Γ : Con}{A : Ty Γ}
               (p : Tm (Γ , A) (A [ wk ]T) ≡ Tm (Γ , A) (A [ wk ]T [ wk ]T [ < vz > ]T))
             → vz {Γ}{A} ≡[ p ]≡ vs vz [ < vz > ]t
  vsvz[<vz>] {Γ}{A} p
    = from≃
        ( uncoe p ⁻¹̃
        ◾̃ [id]t' ⁻¹̃
        ◾̃ []t≃ refl (to≃ (π₁β ⁻¹ ◾ π₁id∘ ⁻¹))
        ◾̃ [][]t' ⁻¹̃)

abstract
  vz[<vz>]
    : {Γ : Con}{A : Ty Γ}
      (p : Tm (Γ , A) (A [ wk ]T) ≡ Tm (Γ , A) (A [ wk ]T [ wk ]T [ < vz > ]T))
    → vz ≡[ p ]≡ vz [ < vz > ]t
  vz[<vz>] {Γ}{A} p
    = from≃
        ( uncoe p ⁻¹̃
        ◾̃ uncoe (TmΓ= ([id]T ⁻¹))
        ◾̃ π₂β' ⁻¹̃
        ◾̃ π₂id[]' ⁻¹̃)

abstract
  vsvz[<u>,]
    : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}
      {u : Tm Γ A}{v : Tm Γ (B [ < u > ]T)}
      (p : Tm Γ A ≡ Tm Γ (A [ wk ]T [ wk ]T [ < u > ,s v ]T))
    → u ≡[ p ]≡ vs vz [ < u > ,s v ]t
  vsvz[<u>,] {Γ}{A}{B}{u}{v} p
    = from≃
        ( uncoe p ⁻¹̃
        ◾̃ uncoe (TmΓ= ([id]T ⁻¹))
        ◾̃ π₂β' ⁻¹̃
        ◾̃ π₂≃ (π₁β ⁻¹ ◾ π₁id∘ ⁻¹)
        ◾̃ π₂id[]' ⁻¹̃
        ◾̃ [][]t' ⁻¹̃)

abstract
  vz[,coe]
    : {Γ : Con}{A : Ty Γ}{Δ : Con}{σ : Tms Γ Δ}
      {B : Ty Δ}{v : Tm Γ A}
      (q : A ≡ B [ σ ]T)
      (p : Tm Γ A ≡ Tm Γ (B [ wk ]T [ σ ,s coe (TmΓ= q) v ]T))
    → v ≡[ p ]≡ vz [ σ ,s coe (TmΓ= q) v ]t
  vz[,coe] {Γ}{A}{Δ}{σ}{B}{v} q p
    = from≃
        ( uncoe p ⁻¹̃
        ◾̃ uncoe (TmΓ= q)
        ◾̃ π₂β' ⁻¹̃
        ◾̃ π₂id[]' ⁻¹̃)
  
