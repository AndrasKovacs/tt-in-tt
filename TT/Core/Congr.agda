{-# OPTIONS --no-eta #-}

open import Agda.Primitive
open import TT.Decl
open import TT.Core

module TT.Core.Congr {i}{j}{d : Decl {i}{j}}(c : Core d) where

open import lib
open import JM

open Decl d
open Core c
open import TT.Decl.Congr d

-- Con

,C= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ ap Ty Γ₂ ]≡ A₁)
    → _≡_ {A = Con} (Γ₀ , A₀) (Γ₁ , A₁)
,C= refl refl = refl

,C≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
      → _≡_ {A = Con} (Γ₀ , A₀) (Γ₁ , A₁)

,C≃ refl (refl ,≃ refl) = refl

-- Ty

[]T= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
       {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ ap Ty Δ₂ ]≡ A₁)
       {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ σ₁)
     → A₀ [ σ₀ ]T ≡[ ap Ty Γ₂ ]≡ A₁ [ σ₁ ]T
[]T= refl refl refl refl = refl

[]T=' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ : Con}
        {A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁)
        {σ₀ : Tms Γ₀ Δ}{σ₁ : Tms Γ₁ Δ}(σ₂ : σ₀ ≡[ Tms-Γ= Γ₂ ]≡ σ₁)
      → A₀ [ σ₀ ]T ≡[ ap Ty Γ₂ ]≡ A₁ [ σ₁ ]T
[]T=' refl refl refl = refl

[]T≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}{A : Ty Δ}
         {ρ₀ : Tms Γ₀ Δ}{ρ₁ : Tms Γ₁ Δ}(ρ₂ : ρ₀ ≃ ρ₁)
       → A [ ρ₀ ]T ≃ A [ ρ₁ ]T
[]T≃ refl (refl ,≃ refl) = refl ,≃ refl

[]T≃' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
        {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≃ ρ₁)
       → A₀ [ ρ₀ ]T ≃ A₁ [ ρ₁ ]T
[]T≃' refl refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

-- Tms

id≃ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → id {Γ₀} ≃ id {Γ₁}
id≃ refl = refl ,≃ refl

ε= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → ε {Γ₀} ≡[ Tms= Γ₂ refl ]≡ ε {Γ₁}
ε= refl = refl

,s= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
      {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ σ₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ ap Ty Δ₂ ]≡ A₁)
      {t₀ : Tm Γ₀ (A₀ [ σ₀ ]T)}{t₁ : Tm Γ₁ (A₁ [ σ₁ ]T)}(t₂ : t₀ ≡[ Tm= Γ₂ ([]T= Γ₂ Δ₂ A₂ σ₂) ]≡ t₁)
    → (σ₀ ,s t₀) ≡[ Tms= Γ₂ (,C= Δ₂ A₂) ]≡ (σ₁ ,s t₁)
,s= refl refl refl refl refl = refl

,s≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
      {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≃ ρ₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
      {t₀ : Tm Γ₀ (A₀ [ ρ₀ ]T)}{t₁ : Tm Γ₁ (A₁ [ ρ₁ ]T)}(t₂ : t₀ ≃ t₁)
    → _≃_ {A = Tms Γ₀ (Δ₀ , A₀)}{Tms Γ₁ (Δ₁ , A₁)} (ρ₀ ,s t₀) (ρ₁ ,s t₁)
,s≃ refl refl (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

,s≃' : ∀{Γ}{Δ}
       {ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
       {A : Ty Δ}
       {t₀ : Tm Γ (A [ ρ₀ ]T)}{t₁ : Tm Γ (A [ ρ₁ ]T)}(t₂ : t₀ ≃ t₁)
     → _≡_ {A = Tms Γ (Δ , A)} (ρ₀ ,s t₀) (ρ₁ ,s t₁)
,s≃' refl (refl ,≃ refl) = refl

,st= : ∀{Γ}{Δ}
       {ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
       {A : Ty Δ}{t : Tm Γ (A [ ρ₀ ]T)}
     → _≡_ {A = Tms Γ (Δ , A)} (ρ₀ ,s t) (ρ₁ ,s coe (TmΓ= (ap (_[_]T A) ρ₂)) t)
,st= refl = refl
       
∘= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
     {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
     {Θ₀ Θ₁ : Con}(Θ₂ : Θ₀ ≡ Θ₁)
     {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ ρ₁)
     {σ₀ : Tms Θ₀ Γ₀}{σ₁ : Tms Θ₁ Γ₁}(σ₂ : σ₀ ≡[ Tms= Θ₂ Γ₂ ]≡ σ₁)
   → (ρ₀ ∘ σ₀) ≡[ Tms= Θ₂ Δ₂ ]≡ (ρ₁ ∘ σ₁)
∘= refl refl refl refl refl = refl

∘=' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ Θ : Con}
      {ρ₀ : Tms Γ₀ Δ}{ρ₁ : Tms Γ₁ Δ}(ρ₂ : ρ₀ ≡[ Tms-Γ= Γ₂ ]≡ ρ₁)
      {σ₀ : Tms Θ Γ₀}{σ₁ : Tms Θ Γ₁}(σ₂ : σ₀ ≡[ Tms= refl Γ₂ ]≡ σ₁)
    → (ρ₀ ∘ σ₀) ≡ (ρ₁ ∘ σ₁)
∘=' refl refl refl = refl

∘≃ : ∀{Γ Δ}{ρ : Tms Γ Δ}
     {Θ₀ Θ₁ : Con}(Θ₂ : Θ₀ ≡ Θ₁)
     {σ₀ : Tms Θ₀ Γ}{σ₁ : Tms Θ₁ Γ}(σ₂ : σ₀ ≃ σ₁)
   → (ρ ∘ σ₀) ≃ (ρ ∘ σ₁)
∘≃ refl (refl ,≃ refl) = refl ,≃ refl

∘≃' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
      {Θ₀ Θ₁ : Con}(Θ₂ : Θ₀ ≡ Θ₁)
      {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≃ ρ₁)
      {σ₀ : Tms Θ₀ Γ₀}{σ₁ : Tms Θ₁ Γ₁}(σ₂ : σ₀ ≃ σ₁)
    → (ρ₀ ∘ σ₀) ≃ (ρ₁ ∘ σ₁)
∘≃' refl refl refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

coe∘ : ∀{Θ Δ Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){σ : Tms Γ₀ Δ}{δ : Tms Θ Γ₁}
     → coe (Tms-Γ= Γ₂) σ ∘ δ
     ≡ σ ∘ coe (TmsΓ-= (Γ₂ ⁻¹)) δ
coe∘ refl = refl

π₁≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
      {δ₀ : Tms Γ₀ (Δ₀ , A₀)}{δ₁ : Tms Γ₁ (Δ₁ , A₁)}(δ₂ : δ₀ ≃ δ₁)
    → π₁ δ₀ ≃ π₁ δ₁
π₁≃ refl refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

π₁= : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
      {δ₀ : Tms Γ₀ (Δ₀ , A₀)}{δ₁ : Tms Γ₁ (Δ₁ , A₁)}(δ₂ : δ₀ ≡[ Tms= Γ₂ (,C= Δ₂ A₂) ]≡ δ₁)
    → π₁ δ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ π₁ δ₁
π₁= refl refl refl refl = refl

π₁id≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
        {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
      → π₁ {Γ₀ , A₀} id ≃ π₁ {Γ₁ , A₁} id
π₁id≃ refl (refl ,≃ refl) = refl ,≃ refl

π₁π₁id≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
          {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
        → π₁ {(Γ₀ , A₀) , B₀} (π₁ id) ≃ π₁ {(Γ₁ , A₁) , B₁} (π₁ id)
π₁π₁id≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

π₁id∘coe''' : ∀{Δ Γ₀ Γ₁}{A : Ty Δ}(Γ₂ : Γ₀ ≡ Γ₁){σ : Tms Γ₀ (Δ , A)}
         → π₁ id ∘ coe (Tms-Γ= Γ₂) σ
         ≃ π₁ id ∘ σ
π₁id∘coe''' refl = r̃

id,t≃ : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t : Tm Γ (A₀ [ id ]T)}
      → _≃_ {A = Tms Γ (Γ , A₀)}
            {Tms Γ (Γ , A₁)}
            (id ,s t)
            (id ,s coe (TmΓ= (ap (λ z → z [ id ]T) A₂)) t)
id,t≃ refl = refl ,≃ refl

id,t= : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t : Tm Γ (A₀ [ id ]T)}
      → _≡_ {A = Tms Γ (Γ , A₁)}
            (coe (TmsΓ-= (,C≃ refl (to≃ A₂))) (id ,s t))
            (id ,s coe (TmΓ= (ap (λ z → z [ id ]T) A₂)) t)
id,t= refl = refl

ε≃ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
   → ε {Γ₀} ≃ ε {Γ₁}
ε≃ refl = refl ,≃ refl

coe∘'' : ∀{Θ Δ Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){σ : Tms (Γ , A₀) Δ}{δ : Tms Θ Γ}
      → coe (Tms-Γ= (,C≃ refl (to≃ A₂))) σ ∘ (δ ^ A₁)
      ≃ σ ∘ (δ ^ A₀)
coe∘'' refl = refl ,≃ refl

^≃ : ∀{Γ Δ}{σ₀ σ₁ : Tms Γ Δ}(σ₂ : σ₀ ≡ σ₁){A : Ty Δ}
   → (σ₀ ^ A) ≃ (σ₁ ^ A)
^≃ refl = refl ,≃ refl

^≃' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}
      {σ₀ : Tms Γ₀ Δ}{σ₁ : Tms Γ₁ Δ}(σ₂ : σ₀ ≃ σ₁)
      {A : Ty Δ}
    → (σ₀ ^ A) ≃ (σ₁ ^ A)
^≃' refl (refl ,≃ refl) = refl ,≃ refl

^≃'' : ∀{Γ Δ}{σ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁)
     → (σ ^ A₀) ≃ (σ ^ A₁)
^≃'' refl = refl ,≃ refl

^≃''' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
        {Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
        {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≃ σ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
      → (σ₀ ^ A₀) ≃ (σ₁ ^ A₁)
^≃''' refl refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

coe<> : ∀{Γ Δ}{A : Ty Δ}{B : Ty Γ}{σ : Tms Γ Δ}{t : Tm Δ A}(p : A [ σ ]T ≡ B)
      → coe (Tms= refl (,C= refl p)) < t [ σ ]t > ≡ < coe (TmΓ= p) (t [ σ ]t) >
coe<> refl = refl

<>= : ∀{Γ}{A}{t₀ t₁ : Tm Γ A}(t₂ : t₀ ≡ t₁) → < t₀ > ≡ < t₁ >
<>= refl = refl

<>='
  : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
    {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}(t₂ : t₀ ≡[ TmΓ= A₂ ]≡ t₁)
  → < t₀ > ≡[ Tms= refl (,C= refl A₂) ]≡ < t₁ >
<>=' refl refl = refl

<>=''
  : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
    {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}(t₂ : t₀ ≡[ Tm= Γ₂ A₂ ]≡ t₁)
  → < t₀ > ≡[ Tms= Γ₂ (,C= Γ₂ A₂) ]≡ < t₁ >
<>='' refl refl refl = refl

<>≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
      {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}(t₂ : t₀ ≃ t₁)
    → < t₀ > ≃ < t₁ >
<>≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

-- Tm

[]t= : ∀{Γ}{Δ}{A : Ty Δ}{t₀ t₁ : Tm Δ A}(t₂ : t₀ ≡ t₁)
        {ρ : Tms Γ Δ}
       → t₀ [ ρ ]t ≡ t₁ [ ρ ]t
[]t= refl = refl

[]t≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}{A : Ty Δ}{t : Tm Δ A}
         {ρ₀ : Tms Γ₀ Δ}{ρ₁ : Tms Γ₁ Δ}(ρ₂ : ρ₀ ≃ ρ₁)
       → t [ ρ₀ ]t ≃ t [ ρ₁ ]t
[]t≃ refl (refl ,≃ refl) = refl ,≃ refl

[]t≃' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
        {t₀ : Tm Δ₀ A₀}{t₁ : Tm Δ₁ A₁}(t₂ : t₀ ≃ t₁)
        {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≃ ρ₁)
      → t₀ [ ρ₀ ]t ≃ t₁ [ ρ₁ ]t
[]t≃' refl refl (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

[]t≃'' : ∀{Γ Δ}{A : Ty Δ}{t : Tm Δ A}{ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
       → t [ ρ₀ ]t ≃ t [ ρ₁ ]t
[]t≃'' refl = refl ,≃ refl

coe[]t : ∀{Γ Δ}{ρ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){u : Tm Δ A₀}
       → coe (TmΓ= A₂) u [ ρ ]t ≡ coe (TmΓ= (ap (λ z → z [ ρ ]T) A₂)) (u [ ρ ]t)
coe[]t refl = refl

coe[]t' : ∀{Γ Δ}{ρ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){u : Tm Δ A₀}
        → coe (TmΓ= A₂) u [ ρ ]t ≃ u [ ρ ]t
coe[]t' refl = refl ,≃ refl

coecoe[]t : ∀{Γ Δ}{ρ : Tms Γ Δ}{A B : Ty Δ}{u : Tm Δ A}(q : A ≡ B)
            {C : Ty Γ}(r : B [ ρ ]T ≡ C)
          → coe (TmΓ= r) (coe (TmΓ= q) u [ ρ ]t) ≃ u [ ρ ]t
coecoe[]t refl refl = refl ,≃ refl

π₂≃ : ∀{Γ Δ}{A : Ty Δ}{ρ₀ ρ₁ : Tms Γ (Δ , A)}(ρ₂ : ρ₀ ≡ ρ₁)
    → π₂ ρ₀ ≃ π₂ ρ₁
π₂≃ refl = refl ,≃ refl

π₂≃' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
       {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
       {ρ₀ : Tms Γ₀ (Δ₀ , A₀)}{ρ₁ : Tms Γ₁ (Δ₁ , A₁)}(ρ₂ : ρ₀ ≃ ρ₁)
     → π₂ ρ₀ ≃ π₂ ρ₁
π₂≃' refl refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

π₂= : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
       {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
       {ρ₀ : Tms Γ₀ (Δ₀ , A₀)}{ρ₁ : Tms Γ₁ (Δ₁ , A₁)}(ρ₂ : ρ₀ ≡[ Tms= Γ₂ (,C= Δ₂ A₂)]≡ ρ₁)
     → π₂ ρ₀ ≡[ Tm= Γ₂ ([]T= Γ₂ Δ₂ A₂ (π₁= Γ₂ Δ₂ A₂ ρ₂)) ]≡ π₂ ρ₁
π₂= refl refl refl refl = refl

π₂id≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
        {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
       → π₂ {Γ₀ , A₀} id ≃ π₂ {Γ₁ , A₁} id
π₂id≃ refl (refl ,≃ refl) = refl ,≃ refl

π₂π₁id≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
          {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
        → π₂ {(Γ₀ , A₀) , B₀} (π₁ id) ≃ π₂ {(Γ₁ , A₁) , B₁} (π₁ id)
π₂π₁id≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

vz≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
    → vz {Γ₀}{A₀} ≃ vz {Γ₁}{A₁}
vz≃ refl (refl ,≃ refl) = refl ,≃ refl

suc'≃ : {A : ∀{Γ} → Ty Γ}{s : ∀{Γ} → Tm Γ A → Tm Γ A}
     {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
     {t₀ : Tm Γ₀ A}{t₁ : Tm Γ₁ A}(t₂ : t₀ ≃ t₁)
   → s t₀ ≃ s t₁
suc'≃ refl (refl ,≃ refl) = refl ,≃ refl
