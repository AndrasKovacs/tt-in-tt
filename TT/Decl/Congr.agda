{-# OPTIONS --no-eta #-}

open import Agda.Primitive
open import TT.Decl

module TT.Decl.Congr {i}{j}(d : Decl {i}{j}) where

open import lib
open import JM

open Decl d

TmΓ= : {Γ : Con}{A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
     → Tm Γ A₀ ≡ Tm Γ A₁
TmΓ= {Γ} = ap (Tm Γ)

Tms= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁) → Tms Γ₀ Δ₀ ≡ Tms Γ₁ Δ₁
Tms= refl refl = refl

Tm= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ ap Ty Γ₂ ]≡ A₁)
    → Tm Γ₀ A₀ ≡ Tm Γ₁ A₁
Tm= refl refl = refl

Ty= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → Ty Γ₀ ≡ Ty Γ₁
Ty= = ap Ty

Tm≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
    → Tm Γ₀ A₀ ≡ Tm Γ₁ A₁
Tm≃ refl (refl ,≃ refl) = refl

Tms-Γ= : ∀{Δ Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁) → Tms Γ₀ Δ ≡ Tms Γ₁ Δ
Tms-Γ= {Δ} = ap (λ z → Tms z Δ)

TmsΓ-= : ∀{Δ Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁) → Tms Δ Γ₀ ≡ Tms Δ Γ₁
TmsΓ-= {Δ} = ap (Tms Δ)

⁻¹Ty : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
     → A₁ ≡[ Ty= (Γ₂ ⁻¹) ]≡ A₀
⁻¹Ty refl refl = refl
