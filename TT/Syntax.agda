module TT.Syntax where

open import Agda.Primitive
open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.Func

----------------------------------------------------------------------
-- Postulating the syntax
----------------------------------------------------------------------

postulate
  syntaxDecl : Decl {lzero}{lzero}
  syntaxCore : Core syntaxDecl
  syntaxBase : Base syntaxCore
  syntaxFunc : Func syntaxCore
  
open Decl syntaxDecl public
open Core syntaxCore public
open Base syntaxBase public
open Func syntaxFunc public
