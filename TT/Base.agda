{-# OPTIONS --no-eta #-}

module TT.Base where

open import Agda.Primitive
open import lib
open import TT.Decl
open import TT.Core
import TT.Decl.Congr

-- Base type and family

record Base {i j}{d : Decl}(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open Core c
  open TT.Decl.Congr d

  field
    U : ∀{Γ} → Ty Γ
    U[] : ∀{Γ Δ}{σ : Tms Γ Δ} → (U [ σ ]T) ≡ U
    El : ∀{Γ}(Â : Tm Γ U) → Ty Γ
    El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{Â : Tm Δ U}
         → (El Â [ σ ]T) ≡ (El (coe (TmΓ= U[]) (Â [ σ ]t)))
