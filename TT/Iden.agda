{-# OPTIONS --no-eta #-}

module TT.Iden where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
import TT.Decl.Congr
import TT.Core.Congr
import TT.Core.Laws

-- Identity type

record Iden {i j}(d : Decl)(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr d
  open TT.Core.Congr c
  open TT.Core.Laws c
  open Core c

  field
    Id : ∀{Γ A} → Tm Γ A → Tm Γ A → Ty Γ

  Id= : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
        {a₀ : Tm Γ A₀}{a₁ : Tm Γ A₁}(a₂ : a₀ ≡[ TmΓ= A₂ ]≡ a₁)
        {b₀ : Tm Γ A₀}{b₁ : Tm Γ A₁}(b₂ : b₀  ≡[ TmΓ= A₂ ]≡ b₁)
      → Id a₀ b₀ ≡ Id a₁ b₁
  Id= = λ { {Γ} refl refl refl → refl }

  field
    Id[] : ∀{Γ Θ A}{u v : Tm Γ A}{σ : Tms Θ Γ}
         → Id u v [ σ ]T ≡ Id (u [ σ ]t) (v [ σ ]t)

    ref : ∀{Γ A}(u : Tm Γ A) → Tm Γ (Id u u)

    ref[] : ∀{Γ Θ A}{u : Tm Γ A}{σ : Tms Θ Γ}
          → ref u [ σ ]t ≡[ TmΓ= Id[] ]≡ ref (u [ σ ]t)

  triple
    : ∀{Γ A}{u v : Tm Γ A}(p : Tm Γ (Id u v))
    → Tms Γ (Γ , A , A [ wk ]T , Id (vs vz) vz)
  triple {Γ}{A}{u}{v} p
    =  < u >
    ,s coe (TmΓ= [wk][id,])
           v
    ,s coe (TmΓ= (Id= [wk][wk][id,,] (vsvz[<u>,] (TmΓ= [wk][wk][id,,])) (vz[,coe] ([wk][id,]) (TmΓ= [wk][wk][id,,])) ◾ Id[] ⁻¹))
           p

  diag : ∀{Γ A} → Tms (Γ , A) (Γ , A , A [ wk ]T , Id (vs vz) vz)
  diag {Γ}{A}
    =  < vz >
    ,s coe (TmΓ= (Id= [wk][id,] (vsvz[<vz>] (TmΓ= [wk][id,])) (vz[<vz>] (TmΓ= [wk][id,])) ◾ Id[] ⁻¹))
           (ref {Γ , A}{A [ wk ]T} vz)
  -- TODO: do diag with triple
{-
  field
    IdElim : ∀{Γ A a}(P : Ty (Γ , A , Id (a [ wk ]t) vz))
             (pref : Tm Γ (P [ < a > ,s coe (TmΓ= (Id= {!!} {!!} {!!} ◾ Id[] ⁻¹)) (ref a) ]T))
           → {!!}
-}

  field
    IdElim : ∀{Γ A}(P : Ty (Γ , A , A [ wk ]T , Id (vs vz) vz))
             (pref : Tm (Γ , A) (P [ diag ]T))
             {u v : Tm Γ A}(p : Tm Γ (Id u v))
           → Tm Γ (P [ triple p ]T)
{-
--    IdElim[]

    IdElimβ
      : ∀{Γ A}(P : Ty (Γ , A , A [ wk ]T , Id (vs vz) vz))
        (pref : Tm (Γ , A) (P [ diag ]T))


{u : Tm Γ A}
      → IdElim P pref (ref u) ≡ {!pref!}
-}
{-
  field
    Id  : ∀{Γ}(A : Ty Γ) → Ty (Γ , A , A [ wk ]T)
    ref : ∀{Γ A} → Tm (Γ , A) (Id A [ < vz > ]T )
    IdElim : ∀{Γ A}(P : Ty (Γ , A , A [ wk ]T , Id A))
             (pref : Tm (Γ , A) (P [ < vz > ,s ref ]T))
           → Tm (Γ , A , A [ wk ]T , Id A) P
    Idβ : ∀{Γ A}(P : Ty (Γ , A , A [ wk ]T , Id A))
          (pref : Tm (Γ , A) (P [ < vz > ,s ref ]T))
        → IdElim P pref [ < vz > ,s ref ]t ≡ pref
    Id[] : ∀{Γ A Δ}{σ : Tms Δ (Γ , A , A [ wk ]T)}
         → Id A [ σ ]T
         ≡ Id (A [ π₁ (π₁ σ) ]T)
           [ < π₂ (π₁ σ) > ,s coe (TmΓ= ([][]T ◾ {!!} ◾ [][]T ⁻¹)) (π₂ σ) ]T
-}
