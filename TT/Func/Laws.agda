{-# OPTIONS --no-eta #-}

open import Agda.Primitive
open import TT.Decl
open import TT.Core
open import TT.Func

module TT.Func.Laws {i}{j}{d : Decl {i}{j}}{c : Core d}(f : Func c) where

open import lib
open import JM

open Decl d
open Core c
open Func f
open import TT.Decl.Congr d
open import TT.Core.Congr c
open import TT.Func.Congr f
open import TT.Core.Laws c

------------------------------------------------------------------------------
-- Tm
------------------------------------------------------------------------------

-- other naturality for Π

abstract
  app[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm Δ (Π A B)}
        → app (coe (TmΓ= Π[]) (t [ δ ]t)) ≡ (app t) [ δ ^ A ]t
  app[] {Γ}{Δ}{δ}{A}{B}{t}

    = ap (λ z → app (coe (TmΓ= Π[]) (z [ δ ]t))) (Πη ⁻¹)
    ◾ app= lam[]
    ◾ Πβ

abstract
  $[] : ∀{Γ Δ A B}{t : Tm Δ (Π A B)}{u : Tm Δ A}{δ : Tms Γ Δ}
      → (t $ u) [ δ ]t ≃ coe (TmΓ= Π[]) (t [ δ ]t) $ (u [ δ ]t)
  $[] {Γ}{Δ}{A}{B}{t}{u}{δ}

    = [][]t'
    ◾̃ []t≃ refl (to≃ <>∘)
    ◾̃ [][]t' ⁻¹̃
    ◾̃ to≃ (ap (λ z → z [  < u [ δ ]t > ]t) (app[] {δ = δ}{t = t}) ⁻¹)

abstract
  app[,] : ∀{Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm Δ (Π A B)}
           {ρ : Tms Γ Δ}{u : Tm Γ (A [ ρ ]T)}
         → app t [ ρ ,s u ]t ≃ app (coe (TmΓ= Π[]) (t [ ρ ]t)) [ < u > ]t
  app[,] = []t≃'' ,→^
         ◾̃ [][]t' ⁻¹̃
         ◾̃ to≃ ((ap (λ z → z [ < _ > ]t)) (app[] ⁻¹))
