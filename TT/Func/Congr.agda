{-# OPTIONS --no-eta #-}

open import Agda.Primitive
open import TT.Decl
open import TT.Core
open import TT.Func

module TT.Func.Congr {i}{j}{d : Decl {i}{j}}{c : Core d}(f : Func c) where

open import lib
open import JM

open Decl d
open Core c
open Func f
open import TT.Decl.Congr d
open import TT.Core.Congr c

-- Ty

Π= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
     {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ ap Ty Γ₂ ]≡ A₁)
     {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≡[ ap Ty (,C= Γ₂ A₂) ]≡ B₁)
   → Π A₀ B₀ ≡[ Ty= Γ₂ ]≡ Π A₁ B₁
Π= refl refl refl = refl

Π=' : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}
     (B₂ : B₀ ≡[ ap Ty (ap (_,_ Γ) A₂) ]≡ B₁)
   → Π A₀ B₀ ≡ Π A₁ B₁
Π=' refl refl = refl

Π≃ : ∀{Γ₀}{Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
     {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
   → Π A₀ B₀ ≃ Π A₁ B₁
Π≃ refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

Π≃' : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
     {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}(B₂ : B₀ ≃ B₁)
   → Π A₀ B₀ ≡ Π A₁ B₁
Π≃' refl (refl ,≃ refl) = refl

-- Tm

app= : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t₀ t₁ : Tm Γ (Π A B)} → t₀ ≡ t₁ → app t₀ ≡ app t₁
app= refl = refl

app≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
       {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
       {t₀ : Tm Γ₀ (Π A₀ B₀)}{t₁ : Tm Γ₁ (Π A₁ B₁)}(t₂ : t₀ ≃ t₁)
     → app t₀ ≃ app t₁
app≃ refl (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

lam≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
       {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
       {t₀ : Tm (Γ₀ , A₀)  B₀}{t₁ : Tm (Γ₁ , A₁) B₁}(t₂ : t₀ ≃ t₁)
     → lam t₀ ≃ lam t₁
lam≃ refl (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

$≃ : ∀{Γ A B}{f₀ f₁ : Tm Γ (Π A B)}(f₂ : f₀ ≡ f₁)
     {a₀ a₁ : Tm Γ A}(a₂ : a₀ ≡ a₁)
   → f₀ $ a₀ ≃ f₁ $ a₁
$≃ refl refl = refl ,≃ refl

$≃' : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
      {B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}(B₂ : B₀ ≃ B₁)
      {f₀ : Tm Γ (Π A₀ B₀)}{f₁ : Tm Γ (Π A₁ B₁)}(f₂ : f₀ ≃ f₁)
      {a₀ : Tm Γ A₀}{a₁ : Tm Γ A₁}(a₂ : a₀ ≃ a₁)
    → f₀ $ a₀ ≃ f₁ $ a₁
$≃' refl (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl
