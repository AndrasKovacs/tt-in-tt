{-# OPTIONS --no-eta #-}

module TT.FuncU where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
open import TT.Base
import TT.Decl.Congr
import TT.Core.Congr
import TT.Core.Laws
import TT.Base.Congr

-- a universe with Pi

record FuncU {i j}{d : Decl}{c : Core {i}{j} d}(b : Base c) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr d
  open TT.Core.Congr c
  open TT.Base.Congr b
  open TT.Core.Laws c
  open Core c
  open Base b

  field
    Π   : ∀{Γ}(A : Tm Γ U)(B : Tm (Γ , El A) U) → Tm Γ U
    Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Tm Δ U}{B : Tm (Δ , El A) U}
        → ((Π A B) [ σ ]t)
        ≡[ TmΓ= U[] ]≡
          (Π (coe (TmΓ= U[]) (A [ σ ]t)) (coe (Tm= (,C= refl El[]) (U[]' (,C= refl El[]))) (B [ σ ^ El A ]t)))
        
    lam : ∀{Γ}{A : Tm Γ U}{B : Tm (Γ , El A) U} → Tm (Γ , El A) (El B) → Tm Γ (El (Π A B))
    app : ∀{Γ}{A : Tm Γ U}{B : Tm (Γ , El A) U} → Tm Γ (El (Π A B)) → Tm (Γ , El A) (El B)

    lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Tm Δ U}{B : Tm (Δ , El A) U}{t : Tm (Δ , El A) (El B)}                       
          → (lam t [ δ ]t)
          ≡[ TmΓ= (El[] ◾ ap El Π[]) ]≡
            (lam (coe (Tm= (,C= refl El[]) (El[]' (,C= refl El[]))) (t [ δ ^ El A ]t)))
    Πβ    : ∀{Γ}{A : Tm Γ U}{B : Tm (Γ , El A) U}{t : Tm (Γ , El A) (El B)}
          → app (lam t) ≡ t
    Πη    : ∀{Γ}{A : Tm Γ U}{B : Tm (Γ , El A) U}{t : Tm Γ (El (Π A B))}
          → lam (app t) ≡ t
