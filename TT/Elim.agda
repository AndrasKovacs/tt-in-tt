{-# OPTIONS --rewriting #-}

module TT.Elim where

-- TODO: computation rules for equalities

open import Agda.Primitive
open import lib
open import TT.Syntax
open import TT.Congr
open import TT.DepModel

module elim {i j}(d : Declᴹ)(c : Coreᴹ {i}{j} d)(b : Baseᴹ d c)(f : Funcᴹ d c) where
  open Declᴹ d
  open Coreᴹ c
  open Baseᴹ b
  open Funcᴹ f

  postulate
    ElimCon : ∀ Γ → Conᴹ Γ
    ElimTy  : ∀{Γ}(A : Ty Γ) → Tyᴹ (ElimCon Γ) A
    ElimTms : ∀{Γ Δ}(δ : Tms Γ Δ) → Tmsᴹ (ElimCon Γ) (ElimCon Δ) δ
    ElimTm  : ∀{Γ}{A : Ty Γ}(t : Tm Γ A) → Tmᴹ (ElimCon Γ) (ElimTy A) t

  postulate
    β• : ElimCon • ≡ •ᴹ
    β, : ∀{Γ A} → ElimCon (Γ , A) ≡ (ElimCon Γ ,Cᴹ ElimTy A)
    
  {-# REWRITE β• #-}
  {-# REWRITE β, #-}
  
  postulate
    β[]T : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ} → ElimTy (A [ δ ]T) ≡ ElimTy A [ ElimTms δ ]Tᴹ

  {-# REWRITE β[]T #-}

  postulate
    βU   : ∀{Γ} → ElimTy (U {Γ}) ≡ Uᴹ
    
  {-# REWRITE βU #-}

  postulate
    βEl  : ∀{Γ}{Â : Tm Γ U} → ElimTy (El Â) ≡ Elᴹ (ElimTm Â)
    
  {-# REWRITE βEl #-}

  postulate
    βΠ   : ∀{Γ A B} → ElimTy (Π {Γ} A B) ≡ Πᴹ (ElimTy A) (ElimTy B)

  {-# REWRITE βΠ #-}
  
  postulate
    βε  : ∀{Γ} → ElimTms (ε {Γ}) ≡ εᴹ
    β,s : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ (A [ σ ]T)}
        → (ElimTms (σ ,s t)) ≡ (ElimTms σ ,sᴹ ElimTm t)
    βid : ∀{Γ} → ElimTms (id {Γ}) ≡ idᴹ
    β∘  : ∀{Γ Δ Σ}{σ : Tms Δ Σ}{ν : Tms Γ Δ}
        → ElimTms (σ ∘ ν) ≡ ElimTms σ ∘ᴹ ElimTms ν
    βπ₁ : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ , A)}
        → ElimTms (π₁ σ) ≡ π₁ᴹ (ElimTms σ)
  
  {-# REWRITE βid #-}
  {-# REWRITE β∘  #-}
  {-# REWRITE βε  #-}
  {-# REWRITE β,s #-}
  {-# REWRITE βπ₁ #-}

  postulate
    β[]t : ∀{Γ Δ}{A : Ty Δ}{t : Tm Δ A}{σ : Tms Γ Δ}
         → ElimTm (t [ σ ]t) ≡ ElimTm t [ ElimTms σ ]tᴹ
    βπ₂  : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ , A)}
         → ElimTm (π₂ σ)     ≡ π₂ᴹ (ElimTms σ)
    βapp : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
         → ElimTm (app t)    ≡ appᴹ (ElimTm t)
    βlam : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
         → ElimTm (lam t)    ≡ lamᴹ (ElimTm t)
         
  {-# REWRITE β[]t #-}
  {-# REWRITE βπ₂  #-}
  {-# REWRITE βlam #-}
  {-# REWRITE βapp #-}
