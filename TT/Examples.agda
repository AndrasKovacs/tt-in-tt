module TT.Examples where

open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws

-- Example A

exA : Tm • (Π U
              (Π (El (coe (TmΓ= U[]) vz))
                 (El (coe (TmΓ= U[]) (coe (TmΓ= U[]) vz [ wk ]t)))))
exA = lam (lam (coe (TmΓ= El[]) vz))

-- Example B

exB : Ty (• , U)
exB = Π (El (coe (TmΓ= U[]) vz)) U

-- Example C

_⇒_ : ∀{Γ} → Ty Γ → Ty Γ → Ty Γ
A ⇒ B = Π A (B [ wk ]T)

_$⇒_ : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
f $⇒ a = coe (TmΓ= ([][]T ◾ ap (_[_]T _) (π₁∘ ◾ ap π₁ idl ◾ π₁β) ◾ [id]T)) (f $ a)

abstract
  ⇒[] : ∀{Γ A B}{Θ}{σ : Tms Θ Γ} → (A ⇒ B) [ σ ]T ≡ ((A [ σ ]T) ⇒ (B [ σ ]T))
  ⇒[] = Π[]
      ◾ ap (Π (_[_]T _ _)) ([][]T ◾ (ap (_[_]T _) (π₁∘ ◾ ap π₁ idl ◾ π₁β) ◾ [][]T ⁻¹))

N : Ty (• , U , El (coe (TmΓ= U[]) vz))
N = El (coe (TmΓ= ([][]T ◾ U[])) (vs vz))

Γ : Con
Γ = • , U , El (coe (TmΓ= U[]) vz) , (N ⇒ N)

abstract
  q : El (coe (TmΓ= U[]) vz) [ wk ]T [ wk {A = N ⇒ N} ]T ≡ N [ wk ]T
  q = [][]T
    ◾ El[]
    ◾ ap El (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                   ◾̃ coe[]t' U[]
                   ◾̃ [][]t' ⁻¹̃
                   ◾̃ coe[]t' ([][]T ◾ U[]) ⁻¹̃
                   ◾̃ uncoe (TmΓ= U[])))
    ◾ El[] ⁻¹

abstract
  z : Tm Γ (N [ wk ]T)
  z = coe (TmΓ= q) (vs vz)

abstract
  p : N [ wk {A = N ⇒ N} ]T ≡ El (coe (TmΓ= ([][]T ◾ [][]T ◾ U[])) (vs (vs vz)))
  p = El[]
    ◾ ap El (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                   ◾̃ coe[]t' ([][]T ◾ U[])
                   ◾̃ uncoe (TmΓ= ([][]T ◾ [][]T ◾ U[]))))

abstract
  sz : Tm Γ (El (coe (TmΓ= ([][]T ◾ [][]T ◾ U[])) (vs (vs vz))))
  sz = coe (TmΓ= (⇒[] ◾ ap (λ z → (N [ wk ]T) ⇒ z) p)) vz $⇒ z

exC : Tm Γ (El (coe (TmΓ= ([][]T ◾ [][]T ◾ U[])) (vs (vs vz))))
exC = coe (TmΓ= (⇒[] ◾ ap (λ z → z ⇒ z) p)) vz $⇒ sz
