module TT.Readme where

-- parts of the syntax defined as records (for models)

import TT.Decl
import TT.Decl.Congr

import TT.Core
import TT.Core.Congr
import TT.Core.Laws

import TT.Base
import TT.Base.Congr

import TT.Func
import TT.Func.Congr
import TT.Func.Laws

-- import TT.Identity

-- grouping together the above

import TT.Syntax
import TT.Rec
import TT.Elim
import TT.ConElim
import TT.TySpine

import TT.Congr
import TT.Laws

-- examples

import TT.Examples

-- experiments

-- folder experiments
