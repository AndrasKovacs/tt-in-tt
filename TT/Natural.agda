{-# OPTIONS --no-eta #-}

module TT.Natural where

open import Agda.Primitive
open import lib
open import JM

open import TT.Decl
open import TT.Core
import TT.Core.Congr
import TT.Core.Laws
import TT.Decl.Congr

record Natural {i j}(d : Decl)(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr d
  open Core c
  open TT.Core.Congr c
  open TT.Core.Laws c

  field
    Nat    : ∀{Γ} → Ty Γ
    Nat[]  : ∀{Γ Θ}{σ : Tms Γ Θ} → Nat [ σ ]T ≡ Nat
    zero'  : ∀{Γ} → Tm Γ Nat
    suc'   : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    ind    : ∀{Γ}(P : Ty (Γ , Nat))
           → Tm Γ (P [ < zero' > ]T)
           → Tm (Γ , Nat , P) (P [ [ Nat[] , suc' ] ∘ wk ]T)
           → (n : Tm Γ Nat) → Tm Γ (P [ < n > ]T)
    zero[] : ∀{Γ Θ}{σ : Tms Γ Θ}
           → zero' [ σ ]t ≡[ TmΓ= Nat[] ]≡ zero'
    suc[]  : ∀{Γ Θ}{σ : Tms Γ Θ}{n : Tm Θ Nat}
           → suc' n [ σ ]t ≡[ TmΓ= Nat[] ]≡ suc' (coe (TmΓ= Nat[]) (n [ σ ]t))
    ind[]  : ∀{Γ Θ}{σ : Tms Γ Θ}{P : Ty (Θ , Nat)}
             {pz : Tm Θ (P [ < zero' > ]T)}{ps : Tm (Θ , Nat , P) (P [ [ Nat[] , suc' ] ∘ wk ]T)}{n : Tm Θ Nat}
           → ind P pz ps n [ σ ]t
           ≡[ TmΓ= ([<>][]' Nat[] n) ]≡
             ind (P [ coe (Tms-Γ= (,C= refl Nat[])) (σ ^ Nat) ]T)
                 (coe (TmΓ= ([<>][] Nat[] zero[])) (pz [ σ ]t))
                 (coe (Tm= (,[], Nat[]) ([][^^] Nat[] suc' suc[])) (ps [ σ ^ Nat ^ P ]t))
                 (coe (TmΓ= Nat[]) (n [ σ ]t))
    zeroβ  : ∀{Γ}{P : Ty (Γ , Nat)}{pz : Tm Γ (P [ < zero' > ]T)}
             {ps : Tm (Γ , Nat , P) (P [ [ Nat[] , suc' ] ∘ wk ]T)}
           → ind P pz ps zero' ≡ pz
    sucβ   : ∀{Γ}{P : Ty (Γ , Nat)}{pz : Tm Γ (P [ < zero' > ]T)}
             {ps : Tm (Γ , Nat , P) (P [ [ Nat[] , suc' ] ∘ wk ]T)}{n : Tm Γ Nat}
           → ind P pz ps (suc' n)
           ≡[ TmΓ= ([<suc>] {Nat} Nat[] suc' suc[]) ]≡
             ps [ < n > ,s ind P pz ps n ]t
