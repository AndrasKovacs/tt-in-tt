{-# OPTIONS --no-eta #-}

open import Agda.Primitive
open import TT.Decl
open import TT.Core
open import TT.Base

module TT.Base.Congr {i}{j}{d : Decl {i}{j}}{c : Core d}(b : Base c) where

open import lib
open import JM

open Decl d
open Core c
open Base b
open import TT.Decl.Congr d
open import TT.Core.Congr c

U= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → U {Γ₀} ≡[ ap Ty Γ₂ ]≡ U {Γ₁}
U= refl = refl

El= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}(Â₂ : Â₀ ≡[ Tm= Γ₂ (U= Γ₂) ]≡ Â₁)
    → El Â₀ ≡[ ap Ty Γ₂ ]≡ El Â₁
El= refl refl = refl

El=' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)(U₂ : U {Γ₀} ≡[ Ty= Γ₂ ]≡ U {Γ₁})
       {Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}(Â₂ : Â₀ ≡[ Tm= Γ₂ U₂ ]≡ Â₁)
     → El Â₀ ≡[ ap Ty Γ₂ ]≡ El Â₁
El=' refl refl refl = refl

U≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
   → U {Γ₀} ≃ U {Γ₁}
U≃ refl = refl ,≃ refl

El≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}(Â₂ : Â₀ ≃ Â₁)
    → El Â₀ ≃ El Â₁
El≃ refl (refl ,≃ refl) = refl ,≃ refl

U[]' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ : Con}{σ : Tms Γ₀ Δ} → U [ σ ]T ≡[ Ty= Γ₂ ]≡ U
U[]' refl = U[]

El[]'' : ∀{Γ Δ}{σ : Tms Γ Δ}{Â : Tm Δ U}
     → (El Â [ σ ]T) ≡ (El (coe (Tm= refl U[]) (Â [ σ ]t)))
El[]'' {Γ}{Δ}{σ}{Â} = El[] ◾ ap El (ap (λ z → coe z (Â [ σ ]t)){TmΓ= U[]}{Tm= refl U[]} (UIP _ _))

El[]' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ : Con}{σ : Tms Γ₀ Δ}{A : Tm Δ U}
      → El A [ σ ]T ≡[ Ty= Γ₂ ]≡ El (coe (Tm= Γ₂ (U[]' Γ₂)) (A [ σ ]t))
El[]' refl = El[]''
