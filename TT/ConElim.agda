{-# OPTIONS --rewriting #-}

module TT.ConElim where

open import lib
open import JM

open import TT.Syntax
open import TT.RecOld
open import TT.ElimOld
open import TT.Congr

----------------------------------------------------------------------
-- A special recursor for contexts
----------------------------------------------------------------------

RecCon' : ∀{i}(Conᴹ : Set i)(•ᴹ : Conᴹ)(,Cᴹ : Conᴹ → Conᴹ)
        → Con → Conᴹ
RecCon' {i} Conᴹ •ᴹ ,Cᴹ = RecCon
  where

    record ⊤' : Set i where
      constructor tt

    M : TT.RecOld.Motives
    M = record
      { Conᴹ = Conᴹ
      ; Tyᴹ  = λ _ → ⊤'
      ; Tmsᴹ = λ _ _ → ⊤'
      ; Tmᴹ = λ _ _ → ⊤' }

    m : Methods M
    m = record
      { •ᴹ     = •ᴹ
      ; _,Cᴹ_  = λ Γᴹ _ → ,Cᴹ Γᴹ
      ; _[_]Tᴹ = λ _ _ → tt
      ; Uᴹ     = tt
      ; Πᴹ     = λ _ _ → tt
      ; Elᴹ    = λ _ → tt
      ; εᴹ     = tt
      ; _,sᴹ_  = λ _ _ → tt
      ; idᴹ    = tt
      ; _∘ᴹ_   = λ _ _ → tt
      ; π₁ᴹ    = λ _ → tt
      ; _[_]tᴹ = λ _ _ → tt
      ; π₂ᴹ    = λ _ → tt
      ; appᴹ   = λ _ → tt
      ; lamᴹ   = λ _ → tt
      ; [id]Tᴹ = refl
      ; [][]Tᴹ = refl
      ; U[]ᴹ   = refl
      ; El[]ᴹ  = refl
      ; Π[]ᴹ   = refl
      ; idlᴹ   = refl
      ; idrᴹ   = refl
      ; assᴹ   = refl
      ; ,∘ᴹ    = refl
      ; π₁βᴹ   = refl
      ; πηᴹ    = refl
      ; εηᴹ    = refl
      ; π₂βᴹ   = refl
      ; lam[]ᴹ = refl
      ; Πβᴹ    = refl
      ; Πηᴹ    = refl }

    open rec M m

----------------------------------------------------------------------
-- A special eliminator for contexts
----------------------------------------------------------------------

ElimCon' : ∀{i}
           (Conᴹ : Con → Set i)
           (•ᴹ : Conᴹ •)
           (,Cᴹ : ∀{Γ A} → Conᴹ Γ → Conᴹ (Γ , A))
         → (Γ : Con) → Conᴹ Γ
ElimCon' {i} Conᴹ •ᴹ ,Cᴹ = Con-elim
  where

    M : TT.ElimOld.Motives
    M = record
      { Conᴹ = Conᴹ
      ; Tyᴹ = λ _ _ → Lift ⊤
      ; Tmsᴹ = λ _ _ _ → Lift {j = i} ⊤
      ; Tmᴹ = λ _ _ _ → Lift ⊤
      }

    mCon : MethodsCon M
    mCon = record
      { •ᴹ = •ᴹ
      ; _,Cᴹ_ = λ Γᴹ _ → ,Cᴹ Γᴹ
      }

    mTy : MethodsTy M mCon
    mTy = record
      { _[_]Tᴹ = λ _ _ → lift tt
      ; Uᴹ = lift tt
      ; Elᴹ = λ _ → lift tt
      ; Πᴹ = λ _ _ → lift tt
      }

    mTms : MethodsTms M mCon mTy
    mTms = record
      { εᴹ = lift tt
      ; _,sᴹ_ = λ _ _ → lift tt
      ; idᴹ = lift tt
      ; _∘ᴹ_ = λ _ _ → lift tt
      ; π₁ᴹ = λ _ → lift tt
      }

    mTm : MethodsTm M mCon mTy mTms
    mTm = record
      { _[_]tᴹ = λ _ _ → lift tt
      ; π₂ᴹ = λ _ → lift tt
      ; appᴹ = λ _ → lift tt
      ; lamᴹ = λ _ → lift tt
      }

    mHTy : MethodsHTy M mCon mTy mTms mTm
    mHTy = record
      { [id]Tᴹ = λ { {_}{_}{_}{lift tt} → loopcoe (Motives.TyΓᴹ= M [id]T) } 
      ; [][]Tᴹ = loopcoe (Motives.TyΓᴹ= M [][]T)
      ; U[]ᴹ = loopcoe (Motives.TyΓᴹ= M U[])
      ; El[]ᴹ = loopcoe (Motives.TyΓᴹ= M El[])
      ; Π[]ᴹ = loopcoe (Motives.TyΓᴹ= M Π[])
      }

    mHTms : MethodsHTms M mCon mTy mTms mTm mHTy
    mHTms = record
      { idlᴹ = λ { {_}{_}{_}{_}{_}{lift tt} → loopcoe (Motives.TmsΓΔᴹ= M idl) }
      ; idrᴹ = λ { {_}{_}{_}{_}{_}{lift tt} → loopcoe (Motives.TmsΓΔᴹ= M idr) }
      ; assᴹ = loopcoe (Motives.TmsΓΔᴹ= M ass)
      ; π₁βᴹ = λ { {_}{_}{_}{_}{_}{_}{_}{lift tt}{_}{_} → loopcoe (Motives.TmsΓΔᴹ= M π₁β) }
      ; πηᴹ = λ { {_}{_}{_}{_}{_}{_}{_}{lift tt} → loopcoe (Motives.TmsΓΔᴹ= M πη) }
      ; εηᴹ = λ { {_}{_}{_}{lift tt} → loopcoe (Motives.TmsΓΔᴹ= M εη) }
      ; ,∘ᴹ = loopcoe (Motives.TmsΓΔᴹ= M ,∘)
      }
    
    mHTm : MethodsHTm M mCon mTy mTms mTm mHTy mHTms
    mHTm = record
      { π₂βᴹ = λ { {_}{_}{_}{_}{A}{Aᴹ}{_}{_}{_}{lift tt} → loopcoe (TT.ElimOld.Motives.TmΓᴹ= M (ap (_[_]T A) π₁β) (MethodsTm.[]Tᴹ= mTm Aᴹ π₁β (MethodsHTms.π₁βᴹ mHTms)) π₂β) }
      ; lam[]ᴹ = λ { {Γ}{Γᴹ}{Δ}{Δᴹ}{σ}{lift tt}{A}{lift tt}{B}{lift tt}{t}{lift tt}
                   → loopcoe (TT.ElimOld.Motives.TmΓᴹ= M Π[] (MethodsHTy.Π[]ᴹ mHTy {Γ}{Γᴹ}{Δ}{Δᴹ}{σ}{lift tt}{A}{lift tt}{B}{lift tt}) lam[])
                   }
      ; Πβᴹ = λ { {_}{_}{_}{_}{_}{_}{_}{lift tt} → loopcoe (Motives.TmΓAᴹ= M Πβ) }
      ; Πηᴹ = λ { {_}{_}{_}{_}{_}{_}{_}{lift tt} → loopcoe (Motives.TmΓAᴹ= M Πη) }
      }

    open elim M mCon mTy mTms mTm mHTy mHTms mHTm

----------------------------------------------------------------------
-- Disjointness and injectivity of constructors for contexts
----------------------------------------------------------------------

disj•, : ∀{Γ A} → • ≡ (Γ , A) → ⊥
disj•, {Γ}{A} p = coe (ap (RecCon' Set ⊥ (λ _ → ⊤)) p ⁻¹) tt

inj, : {Γ₀ Γ₁ : Con}{A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
      → Γ₀ , A₀ ≡ Γ₁ , A₁
      → Σ (Γ₀ ≡ Γ₁) λ Γ₂ → A₀ ≡[ Ty= Γ₂ ]≡ A₁
inj, {Γ₀}{Γ₁}{A₀}{A₁} q
  = transport
      {A = Con}
      (λ Γ → ElimCon' (λ _ → Set)
                      ⊥
                      (λ {Γ'}{A'} _ → Σ (Γ₀ ≡ Γ') λ Γ₂ → A₀ ≡[ Ty= Γ₂ ]≡ A')
                      Γ)
      {Γ₀ , A₀}
      {Γ₁ , A₁}
      q
      (refl ,Σ refl)
    
inj,₀ : {Γ₀ Γ₁ : Con}{A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
      → Γ₀ , A₀ ≡ Γ₁ , A₁
      → Γ₀ ≡ Γ₁
inj,₀ p = proj₁ (inj, p)

inj,₁ : {Γ₀ Γ₁ : Con}{A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
        (p : Γ₀ , A₀ ≡ Γ₁ , A₁)
      → A₀ ≡[ Ty= (inj,₀ p) ]≡ A₁
inj,₁ p = proj₂ (inj, p)
