{-# OPTIONS --no-eta #-}

module TT.Func where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
import TT.Decl.Congr

-- Function space

record Func {i j}{d : Decl}(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr d
  open Core c

  field
    Π : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ

    Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
        → ((Π A B) [ σ ]T) ≡ (Π (A [ σ ]T) (B [ σ ^ A ]T))
        
    lam : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm (Γ , A) B → Tm Γ (Π A B)

    app : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm Γ (Π A B) → Tm (Γ , A) B

    lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B}                       
          → ((lam t) [ δ ]t) ≡[ TmΓ= Π[] ]≡ (lam (t [ δ ^ A ]t))
    Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
          → (app (lam t)) ≡ t
    Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
          → (lam (app t)) ≡ t

  -- abbreviations

  _$_ : ∀ {Γ}{A : Ty Γ}{B : Ty (Γ , A)}(t : Tm Γ (Π A B))(u : Tm Γ A) → Tm Γ (B [ < u > ]T)
  t $ u = (app t) [ < u > ]t

  infixl 5 _$_
