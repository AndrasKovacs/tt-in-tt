module TT.Extrinsic where

data PCon    : Set
data PTy     : Set
data PTms    : Set
data PTm     : Set
data _~PTy_  : PTy → PTy → Set
data _~PTms_ : PTms → PTms → Set
data _~PTm_  : PTm → PTm → Set

data PCon where
  •   : PCon
  _,_ : PCon → PTy → PCon

data PTy where
  _[_]T : PTy → PTms → PTy
  Pi    : PTy → PTy → PTy
  U     : PTy
  El    : PTm → PTy

data PTms where
  id    : PTms
  _∘_   : PTms → PTms → PTms
  ε     : PTms
  _,s_  : PTms → PTm → PTms
  π₁    : PTms → PTms

data PTm where
  _[_]t : PTm → PTms → PTm
  π₂    : PTms → PTm
  lam   : PTm → PTm
  app   : PTm → PTm

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

infix 2 _~PTy_
infix 2 _~PTms_
infix 2 _~PTm_

-- or do we want a typed conversion relation? Yes, probably

data _~PTy_ where
  [id]T : {A : PTy} → A [ id ]T ~PTy A
  [][]T : {A : PTy}{σ δ : PTms} → (A [ δ ]T [ σ ]T) ~PTy (A [ δ ∘ σ ]T)

data _~PTms_ where
  idl   : {δ : PTms} → (id ∘ δ) ~PTms δ 
  idr   : {δ : PTms} → (δ ∘ id) ~PTms δ 
  ass   : {σ δ ν : PTms} → ((σ ∘ δ) ∘ ν) ~PTms (σ ∘ (δ ∘ ν))
  ,∘    : {δ σ : PTms}{A : PTy}{a : PTm}
        → (δ ,s a) ∘ σ ~PTms (δ ∘ σ) ,s (a [ σ ]t)
  π₁β   : {A : PTy}{δ : PTms}{a : PTm} → π₁ (δ ,s a) ~PTms δ
  πη    : {A : PTy}{δ : PTms} → π₁ δ ,s π₂ δ ~PTms δ
--  εη    : {σ : Tms Γ •} → σ ~PTms ε

data _~PTm_ where
  
