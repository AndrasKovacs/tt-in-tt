{-# OPTIONS --without-K --no-eta --rewriting #-}

module NoJM.Nf where

open import NoJM.Syntax
open import Agda.Primitive

----------------------------------------------------------------------
-- Variables
----------------------------------------------------------------------

data Var  : (Γ : Con)(A : Ty Γ) → Set
⌜_⌝v      : ∀{Γ A} → Var Γ A → Tm Γ A

data Var where
  vze : ∀ {Γ}{A : Ty Γ} → Var (Γ , A) (A [ wk ]T)
  vsu : ∀ {Γ}{A B : Ty Γ} → Var Γ A → Var (Γ , B) (A [ wk ]T)

⌜ vze ⌝v = vz
⌜ vsu v ⌝v = vs ⌜ v ⌝v

----------------------------------------------------------------------
-- Neutral terms and normal forms
----------------------------------------------------------------------

data Ne : (Γ : Con) → Ty Γ → Set
data Nf : (Γ : Con) → Ty Γ → Set

⌜_⌝ne : ∀{Γ A} → Ne Γ A → Tm Γ A
⌜_⌝nf : ∀{Γ A} → Nf Γ A → Tm Γ A

data Ne where
  var : ∀{Γ A}(x : Var Γ A) → Ne Γ A
  appNe : ∀{Γ A B}(f : Ne Γ (Π A B))(v : Nf Γ A) → Ne Γ (B [ < ⌜ v ⌝nf > ]T)

data Nf where
  neuU  : ∀{Γ}(n : Ne Γ U) → Nf Γ U
  neuEl : ∀{Γ Â}(n : Ne Γ (El Â)) → Nf Γ (El Â)
  lamNf : ∀{Γ A B}(v : Nf (Γ , A) B) → Nf Γ (Π A B)

⌜ var x ⌝ne = ⌜ x ⌝v
⌜ appNe n u ⌝ne = ⌜ n ⌝ne $ ⌜ u ⌝nf

⌜ neuU n ⌝nf = ⌜ n ⌝ne
⌜ neuEl n ⌝nf = ⌜ n ⌝ne
⌜ lamNf t ⌝nf = lam ⌜ t ⌝nf

----------------------------------------------------------------------
-- Elimination principles
----------------------------------------------------------------------

record MVar {i} : Set (lsuc i) where
  field
    Varᴹ : ∀ Γ A → Var Γ A → Set i
    vzeᴹ : ∀{Γ A} → Varᴹ (Γ , A) (A [ wk ]T) vze
    vsuᴹ : ∀{Γ}{A B : Ty Γ}{x : Var Γ A}(xᴹ : Varᴹ Γ A x) → Varᴹ (Γ , B) (A [ wk ]T) (vsu x)

module _ {i}(M : MVar {i}) where
  open MVar M
  
  elimVar : ∀{Γ A}(x : Var Γ A) → Varᴹ Γ A x
  elimVar vze = vzeᴹ
  elimVar (vsu x) = vsuᴹ (elimVar x)

record MNeNf {i} : Set (lsuc i) where
  field
    Neᴹ : ∀ Γ A → Ne Γ A → Set i
    Nfᴹ : ∀ Γ A → Nf Γ A → Set i

    varᴹ : ∀{Γ A}(x : Var Γ A) → Neᴹ Γ A (var x)
    appNeᴹ : ∀{Γ A B}{n : Ne Γ (Π A B)}(nᴹ : Neᴹ Γ (Π A B) n)
             {v : Nf Γ A}(vᴹ : Nfᴹ Γ A v)
           → Neᴹ Γ (B [ < ⌜ v ⌝nf > ]T) (appNe n v)
    neuUᴹ : ∀{Γ}{n : Ne Γ U}(nᴹ : Neᴹ Γ U n) → Nfᴹ Γ U (neuU n)
    neuElᴹ : ∀{Γ Â}{n : Ne Γ (El Â)}(nᴹ : Neᴹ Γ (El Â) n) → Nfᴹ Γ (El Â) (neuEl n)
    lamNfᴹ : ∀{Γ A B}{v : Nf (Γ , A) B}(vᴹ : Nfᴹ (Γ , A) B v) → Nfᴹ Γ (Π A B) (lamNf v)

module _ {i}(M : MNeNf {i}) where
  open MNeNf M
  
  elimNe : ∀{Γ A}(n : Ne Γ A) → Neᴹ Γ A n
  elimNf : ∀{Γ A}(v : Nf Γ A) → Nfᴹ Γ A v

  elimNe (var x) = varᴹ x
  elimNe (appNe n v) = appNeᴹ (elimNe n) (elimNf v)

  elimNf (neuU n) = neuUᴹ (elimNe n)
  elimNf (neuEl n) = neuElᴹ (elimNe n)
  elimNf (lamNf v) = lamNfᴹ (elimNf v)
