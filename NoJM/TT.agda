{-# OPTIONS --without-K --no-eta #-}

module NoJM.TT where

open import Agda.Primitive
open import lib using (_≡_; refl; idfun)

----------------------------------------------------------------------
-- Declaration of a model
----------------------------------------------------------------------

record Decl {i j} : Set (lsuc (i ⊔ j)) where
  field
    Con : Set i
    Ty  : Con → Set i
    Tms : Con → Con → Set j
    Tm  : (Γ : Con) → Ty Γ → Set j
    
  Con= : Con → Con → Set i
  Con= = _≡_

  Con== : {Γ₀₀ Γ₀₁ : Con}(Γ₀₂ : Con= Γ₀₀ Γ₀₁)
          {Γ₁₀ Γ₁₁ : Con}(Γ₁₂ : Con= Γ₁₀ Γ₁₁)
        → Con= Γ₀₀ Γ₁₀ → Con= Γ₀₁ Γ₁₁ → Set i
  Con== refl refl = _≡_

  coeCon : Con → Con
  coeCon = idfun

  Ty= : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
      → Ty Γ₀ → Ty Γ₁ → Set i
  Ty= refl = _≡_

  coeTy : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
        → Ty Γ₀ → Ty Γ₁
  coeTy refl = idfun

  coeTy= : {Γ₀₀ Γ₀₁ : Con}(Γ₀₂ : Con= Γ₀₀ Γ₀₁)
           {Γ₁₀ Γ₁₁ : Con}(Γ₁₂ : Con= Γ₁₀ Γ₁₁)
           {Γ₂₀ : Con= Γ₀₀ Γ₁₀}{Γ₂₁ : Con= Γ₀₁ Γ₁₁}(Γ₂₂ : Con== Γ₀₂ Γ₁₂ Γ₂₀ Γ₂₁)
           {A₀₀ : Ty Γ₀₀}{A₀₁ : Ty Γ₀₁}(A₀₂ : Ty= Γ₀₂ A₀₀ A₀₁)
           {A₁₀ : Ty Γ₁₀}{A₁₁ : Ty Γ₁₁}(A₁₂ : Ty= Γ₁₂ A₁₀ A₁₁)
         → Ty= Γ₂₀ A₀₀ A₁₀ → Ty= Γ₂₁ A₀₁ A₁₁
  coeTy= refl refl refl refl refl = idfun

  coe'Ty : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
         → Ty Γ₀ → Ty Γ₁
  coe'Ty refl = idfun

  Tms= : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
         {Δ₀ Δ₁ : Con}(Δ₂ : Con= Δ₀ Δ₁)
       → Tms Γ₀ Δ₀ → Tms Γ₁ Δ₁ → Set j
  Tms= refl refl = _≡_

  coeTms : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
           {Δ₀ Δ₁ : Con}(Δ₂ : Con= Δ₀ Δ₁)
         → Tms Γ₀ Δ₀ → Tms Γ₁ Δ₁
  coeTms refl refl = idfun

  cohTms : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
           {Δ₀ Δ₁ : Con}(Δ₂ : Con= Δ₀ Δ₁)
           {σ₀ : Tms Γ₀ Δ₀}
         → Tms= Γ₂ Δ₂ σ₀ (coeTms Γ₂ Δ₂ σ₀)
  cohTms refl refl = refl

  coe'Tms : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
            {Δ₀ Δ₁ : Con}(Δ₂ : Con= Δ₀ Δ₁)
          → Tms Γ₀ Δ₀ → Tms Γ₁ Δ₁
  coe'Tms refl refl = idfun

  Tm= : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
        {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty= Γ₂ A₀ A₁)
      → Tm Γ₀ A₀ → Tm Γ₁ A₁ → Set j
  Tm= refl refl = _≡_

  coeTm : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty= Γ₂ A₀ A₁)
        → Tm Γ₀ A₀ → Tm Γ₁ A₁
  coeTm refl refl = idfun

  cohTm : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty= Γ₂ A₀ A₁)
          {t₀ : Tm Γ₀ A₀}
        → Tm= Γ₂ A₂ t₀ (coeTm Γ₂ A₂ t₀)
  cohTm refl refl = refl

  coe'Tm : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
           {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty= Γ₂ A₀ A₁)
         → Tm Γ₁ A₁ → Tm Γ₀ A₀
  coe'Tm refl refl = idfun

----------------------------------------------------------------------
-- Core substitution calculus
----------------------------------------------------------------------

record Core {i j}(d : Decl {i}{j}) : Set (i ⊔ j) where
  open Decl d
  field
    •     : Con  -- \bub
    _,_   : (Γ : Con) → Ty Γ → Con
    
    _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

    id    : ∀{Γ} → Tms Γ Γ
    _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
    ε     : ∀{Γ} → Tms Γ •
    _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
    π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ

    _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) 
    π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)
    
  infixl 5 _,_
  infixl 7 _[_]T
  infixl 5 _,s_
  infix  6 _∘_
  infixl 8 _[_]t

  ,= : {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty= Γ₂ A₀ A₁)
     → Con= (Γ₀ , A₀) (Γ₁ , A₁)
  ,= = λ { {Γ}{.Γ} refl refl → refl }
  
  []T= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
         {Δ₀ Δ₁ : Con}{Δ₂ : Con= Δ₀ Δ₁}
         {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : Ty= Δ₂ A₀ A₁)
         {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : Tms= Γ₂ Δ₂ σ₀ σ₁)
       → Ty= Γ₂ (A₀ [ σ₀ ]T) (A₁ [ σ₁ ]T)
  []T= = λ { {Γ₂ = refl}{Δ₂ = refl} refl refl → refl }

  id= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
      → Tms= Γ₂ Γ₂ (id {Γ₀}) (id {Γ₁})
  id= = λ { {Γ₂ = refl} → refl }
  
  ∘= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
       {Δ₀ Δ₁ : Con}{Δ₂ : Con= Δ₀ Δ₁}
       {Θ₀ Θ₁ : Con}{Θ₂ : Con= Θ₀ Θ₁}
       {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : Tms= Γ₂ Δ₂ ρ₀ ρ₁)
       {σ₀ : Tms Θ₀ Γ₀}{σ₁ : Tms Θ₁ Γ₁}(σ₂ : Tms= Θ₂ Γ₂ σ₀ σ₁)
     → Tms= Θ₂ Δ₂ (ρ₀ ∘ σ₀) (ρ₁ ∘ σ₁)
  ∘= = λ { {Γ₂ = refl}{Δ₂ = refl}{Θ₂ = refl} refl refl → refl }

  ε= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
      → Tms= Γ₂ refl (ε {Γ₀}) (ε {Γ₁})
  ε= = λ { {Γ₂ = refl} → refl }

  ,s= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
        {Δ₀ Δ₁ : Con}{Δ₂ : Con= Δ₀ Δ₁}
        {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : Tms= Γ₂ Δ₂ σ₀ σ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}{A₂ : Ty= Δ₂ A₀ A₁}
        {t₀ : Tm Γ₀ (A₀ [ σ₀ ]T)}{t₁ : Tm Γ₁ (A₁ [ σ₁ ]T)}
          (t₂ : Tm= Γ₂ ([]T= A₂ σ₂) t₀ t₁)
      → Tms= Γ₂ (,= Δ₂ A₂) (σ₀ ,s t₀) (σ₁ ,s t₁)
  ,s= = λ { {Γ₂ = refl}{Δ₂ = refl} refl {A₂ = refl} refl → refl }

  π₁= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
        {Δ₀ Δ₁ : Con}{Δ₂ : Con= Δ₀ Δ₁}
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}{A₂ : Ty= Δ₂ A₀ A₁}
        {σ₀ : Tms Γ₀ (Δ₀ , A₀)}{σ₁ : Tms Γ₁ (Δ₁ , A₁)}(σ₂ : Tms= Γ₂ (,= Δ₂ A₂) σ₀ σ₁)
      → Tms= Γ₂ Δ₂ (π₁ σ₀) (π₁ σ₁)
  π₁= = λ { {Γ₂ = refl}{Δ₂ = refl}{A₂ = refl} refl → refl }

  []t= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
         {Δ₀ Δ₁ : Con}{Δ₂ : Con= Δ₀ Δ₁}
         {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}{A₂ : Ty= Δ₂ A₀ A₁}
         {t₀ : Tm Δ₀ A₀}{t₁ : Tm Δ₁ A₁}(t₂ : Tm= Δ₂ A₂ t₀ t₁)
         {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : Tms= Γ₂ Δ₂ σ₀ σ₁)
       → Tm= Γ₂ ([]T= A₂ σ₂) (t₀ [ σ₀ ]t) (t₁ [ σ₁ ]t)
  []t= = λ { {Γ₂ = refl}{Δ₂ = refl}{A₂ = refl} refl refl → refl }

  π₂= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
        {Δ₀ Δ₁ : Con}{Δ₂ : Con= Δ₀ Δ₁}
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}{A₂ : Ty= Δ₂ A₀ A₁}
        {σ₀ : Tms Γ₀ (Δ₀ , A₀)}{σ₁ : Tms Γ₁ (Δ₁ , A₁)}(σ₂ : Tms= Γ₂ (,= Δ₂ A₂) σ₀ σ₁)
      → Tm= Γ₂ ([]T= A₂ (π₁= σ₂)) (π₂ σ₀) (π₂ σ₁)
  π₂= = λ { {Γ₂ = refl}{Δ₂ = refl}{A₂ = refl} refl → refl }
  
  field
    [id]T : ∀{Γ}{A : Ty Γ} → Ty= refl (A [ id ]T) A
    [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
            → Ty= refl (A [ δ ]T [ σ ]T) (A [ δ ∘ σ ]T)
            
    idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → Tms= refl refl (id ∘ δ) δ 
    idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → Tms= refl refl (δ ∘ id) δ 
    ass   : ∀{Δ Γ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Γ Σ}{ν : Tms Δ Γ}
          → Tms= refl refl ((σ ∘ δ) ∘ ν) (σ ∘ (δ ∘ ν))
    ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
          → Tms= refl refl ((δ ,s a) ∘ σ) ((δ ∘ σ) ,s coeTm refl [][]T (a [ σ ]t))
    π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
          → Tms= refl refl (π₁ (δ ,s a)) δ
    πη    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
          → Tms= refl refl (π₁ δ ,s π₂ δ) δ
    εη    : ∀{Γ}{σ : Tms Γ •}
          → Tms= refl refl σ ε
    
    π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
          → Tm= refl ([]T= refl π₁β) (π₂ (δ ,s a)) a

  -- abbreviations

  wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
  wk = π₁ id

  vz : ∀ {Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
  vz = π₂ id

  vs : ∀ {Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T) 
  vs x = x [ wk ]t

  <_> : ∀ {Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
  < t > = id ,s coe'Tm refl [id]T t

----------------------------------------------------------------------
-- Base type and family
----------------------------------------------------------------------

record Base {i j}(d : Decl)(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open Core c

  field
    U : ∀{Γ} → Ty Γ

  U= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
     → Ty= Γ₂ (U {Γ₀}) (U {Γ₁})
  U= = λ { {Γ₂ = refl} → refl }

  field
    U[] : ∀{Γ Δ}{σ : Tms Γ Δ} → Ty= refl (U [ σ ]T) U

  field
    El : ∀{Γ}(Â : Tm Γ U) → Ty Γ

  El= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
        {Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}(Â₂ : Tm= Γ₂ U= Â₀ Â₁)
      → Ty= Γ₂ (El Â₀) (El Â₁)
  El= = λ { {Γ₂ = refl} refl → refl }

  field
     El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{Â : Tm Δ U}
          → Ty= refl (El Â [ σ ]T) (El (coeTm refl U[] (Â [ σ ]t)))

----------------------------------------------------------------------
-- Function space
----------------------------------------------------------------------

record Func {i j}(d : Decl)(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open Core c

  field
    Π : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ

  Π= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : Ty= Γ₂ A₀ A₁)
       {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : Ty= (,= Γ₂ A₂) B₀ B₁)
     → Ty= Γ₂ (Π A₀ B₀) (Π A₁ B₁)
  Π= = λ { {Γ₂ = refl} refl refl → refl }

  _^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ σ ]T) (Δ , A)
  _^_ = λ { σ A → (σ ∘ π₁ id) ,s coeTm refl [][]T (π₂ id) }

  infixl 5 _^_

  ^= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
       {Δ₀ Δ₁ : Con}{Δ₂ : Con= Δ₀ Δ₁}
       {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : Tms= Γ₂ Δ₂ σ₀ σ₁)
       {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : Ty= Δ₂ A₀ A₁)
     → Tms= (,= Γ₂ ([]T= A₂ σ₂)) (,= Δ₂ A₂) (σ₀ ^ A₀) (σ₁ ^ A₁)
  ^= = λ { {Γ₂ = refl}{Δ₂ = refl} refl refl → refl }

  field
    Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
        → Ty= refl ((Π A B) [ σ ]T) (Π (A [ σ ]T) (B [ σ ^ A ]T))
    lam : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm (Γ , A) B → Tm Γ (Π A B)

  lam= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}{A₂ : Ty= Γ₂ A₀ A₁}
         {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}{B₂ : Ty= (,= Γ₂ A₂) B₀ B₁}
         {t₀ : Tm (Γ₀ , A₀) B₀}{t₁ : Tm (Γ₁ , A₁) B₁}(t₂ : Tm= (,= Γ₂ A₂) B₂ t₀ t₁)
       → Tm= Γ₂ (Π= A₂ B₂) (lam t₀) (lam t₁)
  lam= = λ { {Γ₂ = refl}{A₂ = refl}{B₂ = refl} refl → refl }
       
  field
    app : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm Γ (Π A B) → Tm (Γ , A) B

  app= : {Γ₀ Γ₁ : Con}{Γ₂ : Con= Γ₀ Γ₁}
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}{A₂ : Ty= Γ₂ A₀ A₁}
         {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}{B₂ : Ty= (,= Γ₂ A₂) B₀ B₁}
         {t₀ : Tm Γ₀ (Π A₀ B₀)}{t₁ : Tm Γ₁ (Π A₁ B₁)}(t₂ : Tm= Γ₂ (Π= A₂ B₂) t₀ t₁)
       → Tm= (,= Γ₂ A₂) B₂ (app t₀) (app t₁)
  app= = λ { {Γ₂ = refl}{A₂ = refl}{B₂ = refl} refl → refl }

  field
     lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B}                       
           → Tm= refl Π[] ((lam t) [ δ ]t) (lam (t [ δ ^ A ]t))
     Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
           → Tm= refl refl (app (lam t)) t
     Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
           → Tm= refl refl (lam (app t)) t

  -- abbreviations

  _$_ : ∀ {Γ}{A : Ty Γ}{B : Ty (Γ , A)}(t : Tm Γ (Π A B))(u : Tm Γ A) → Tm Γ (B [ < u > ]T)
  t $ u = (app t) [ < u > ]t

  infixl 5 _$_
