{-# OPTIONS --no-eta --rewriting #-}

-- injectivity of Pi

module NormTy.Inj where

open import lib
open import JM

open import TT.Syntax
open import TT.Congr
open import NormTy.NTy
open import NormTy.NormTy
open import NormTy.Stab

abstract
  inj⌜⌝T' : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
            {A₀ : NTy Γ₀}{A₁ : NTy Γ₁}(A₂ : ⌜ A₀ ⌝T ≃ ⌜ A₁ ⌝T)
          → A₀ ≃ A₁
  inj⌜⌝T' {Γ} refl {A₀}{A₁} A₂
    = to≃ (stabT A₀ ⁻¹) ◾̃ normT≃ refl A₂ ◾̃ to≃ (stabT A₁)

abstract
  inj⌜⌝T : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
           {A₀ : NTy Γ₀}{A₁ : NTy Γ₁}(A₂ : ⌜ A₀ ⌝T ≡[ Ty= Γ₂ ]≡ ⌜ A₁ ⌝T)
         → A₀ ≡[ NTy= Γ₂ ]≡ A₁
  inj⌜⌝T refl A₂ = from≃ (inj⌜⌝T' refl (to≃ A₂))

abstract
  injΠN₁ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
           {A₀ : NTy Γ₀}{A₁ : NTy Γ₁}{B₀ : NTy (Γ₀ , ⌜ A₀ ⌝T)}{B₁ : NTy (Γ₁ , ⌜ A₁ ⌝T)}
          → NΠ A₀ B₀ ≡[ NTy= Γ₂ ]≡ NΠ A₁ B₁ → A₀ ≡[ NTy= Γ₂ ]≡ A₁
  injΠN₁ refl refl = refl

abstract
  injΠN₁' : ∀{Γ}{A₀ A₁ : NTy Γ}{B₀ : NTy (Γ , ⌜ A₀ ⌝T)}{B₁ : NTy (Γ , ⌜ A₁ ⌝T)}
          → NΠ A₀ B₀ ≡ NΠ A₁ B₁ → A₀ ≡ A₁
  injΠN₁' {Γ}{A₀}{A₁}{B₀}{B₁}
    = J {A = NTy Γ}{NΠ A₀ B₀}(λ { {NΠ A' B'} _ → A₀ ≡ A' ; _ → ⊥ })
        refl

abstract
  injΠN₂ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
           {A₀ : NTy Γ₀}{A₁ : NTy Γ₁}{B₀ : NTy (Γ₀ , ⌜ A₀ ⌝T)}{B₁ : NTy (Γ₁ , ⌜ A₁ ⌝T)}
           (p : NΠ A₀ B₀ ≡[ NTy= Γ₂ ]≡ NΠ A₁ B₁)
          → B₀ ≡[ NTy= (,C= Γ₂ (⌜⌝T= Γ₂ (injΠN₁ Γ₂ p))) ]≡ B₁
  injΠN₂ refl refl = refl

abstract
  injΠN₂' : ∀{Γ}{A : NTy Γ}{B₀ B₁ : NTy (Γ , ⌜ A ⌝T)}
          → NΠ A B₀ ≡ NΠ A B₁ → B₀ ≡ B₁
  injΠN₂' refl = refl

abstract
  injΠN₂'' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
             {A₀ : NTy Γ₀}{A₁ : NTy Γ₁}
             {B₀ : NTy (Γ₀ , ⌜ A₀ ⌝T)}{B₁ : NTy (Γ₁ , ⌜ A₁ ⌝T)}
           → NΠ A₀ B₀ ≃ NΠ A₁ B₁ → B₀ ≃ B₁
  injΠN₂'' refl (refl ,≃ refl) = refl ,≃ refl

abstract
  ΠnormAnormB₂
    : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}{B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
      (p : Π A₀ B₀ ≡[ Ty= Γ₂ ]≡ Π A₁ B₁)
    → NΠ (normT A₀) (coe (NTy= (,C= refl (complT A₀))) (normT B₀))
    ≡[ NTy= Γ₂ ]≡
      NΠ (normT A₁) (coe (NTy= (,C= refl (complT A₁))) (normT B₁))
  ΠnormAnormB₂ {Γ} refl {A₀}{A₁}{B₀}{B₁} p
    = inj⌜⌝T refl
             ( Π= refl
                  (complT A₀ ⁻¹)
                  (from≃ ( uncoe (Ty= (,C= refl (complT A₀ ⁻¹))) ⁻¹̃
                         ◾̃ ⌜⌝T≃ (,C= refl (complT A₀ ⁻¹))
                                ( uncoe (NTy= (,C= refl (complT A₀))) ⁻¹̃))
                  ◾ complT B₀ ⁻¹)
             ◾ p
             ◾ Π= refl
                  (complT A₁)
                  (from≃ ( uncoe (Ty= (,C= refl (complT A₁))) ⁻¹̃
                         ◾̃ to≃ (complT B₁)
                         ◾̃ ⌜⌝T≃ (,C= refl (complT A₁))
                                (uncoe (NTy= (,C= refl (complT A₁)))))))

abstract
  injΠ₁ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}{B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
        → Π A₀ B₀ ≡[ Ty= Γ₂ ]≡ Π A₁ B₁
        → A₀ ≡[ Ty= Γ₂ ]≡ A₁
  injΠ₁ {Γ} refl {A₀}{A₁}{B₀}{B₁} p
    = complT A₀
    ◾ ⌜⌝T= refl
           (injΠN₁ {Γ} refl
                   {normT A₀}{normT A₁}
                   {coe (NTy= (,C= refl (complT A₀))) (normT B₀)}
                   {coe (NTy= (,C= refl (complT A₁))) (normT B₁)}
                   (ΠnormAnormB₂ refl p))
    ◾ complT A₁ ⁻¹

abstract
  injΠ₁' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}{B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
        → Π A₀ B₀ ≃ Π A₁ B₁
        → A₀ ≃ A₁
  injΠ₁' {Γ} refl {A₀}{A₁}{B₀}{B₁} p
    = to≃ (injΠ₁ refl (from≃ p))

abstract
  injΠ₂' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
          {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}{B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
        → Π A₀ B₀ ≃ Π A₁ B₁
        → B₀ ≃ B₁
  injΠ₂' {Γ} refl {A₀}{A₁}{B₀}{B₁} p
    = to≃ (complT B₀)
    ◾̃ ⌜⌝T≃ (,C= refl (injΠ₁ refl (from≃ p)))
           {normT B₀}{normT B₁}
           ( uncoe (NTy= (,C= refl (complT A₀)))
           ◾̃ injΠN₂'' {Γ} refl 
                     {normT A₀}{normT A₁}
                     {coe (NTy= (,C= refl (complT A₀))) (normT B₀)}
                     {coe (NTy= (,C= refl (complT A₁))) (normT B₁)}
                     (to≃ (ΠnormAnormB₂ refl (from≃ p)))
           ◾̃ uncoe (NTy= (,C= refl (complT A₁))) ⁻¹̃)
    ◾̃ to≃ (complT B₁ ⁻¹)

abstract
  injNEl : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}
         → NEl Â₀ ≡[ NTy= Γ₂ ]≡ NEl Â₁ → Â₀ ≡[ Tm= Γ₂ (U= Γ₂) ]≡ Â₁
  injNEl refl refl = refl

abstract
  injEl : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
          {Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}
        → El Â₀ ≡[ Ty= Γ₂ ]≡ El Â₁ → Â₀ ≡[ Tm= Γ₂ (U= Γ₂) ]≡ Â₁
  injEl refl {Â₀}{Â₁} ElÂ₂ = injNEl refl (ap normT ElÂ₂)
