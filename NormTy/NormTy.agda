{-# OPTIONS --no-eta #-}

-- normalisation of types

module NormTy.NormTy where

open import lib
open import JM

open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NormTy.NTy

----------------------------------------------------------------------
-- Substitution of normal types
----------------------------------------------------------------------

-- we define it mutually with a naturality law

_[_]NT : ∀{Δ}(A : NTy Δ){Γ : Con}(σ : Tms Γ Δ) → NTy Γ

⌜[]⌝T : ∀{Δ}{A : NTy Δ}
        {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
        {σ₀ : Tms Γ₀ Δ}{σ₁ : Tms Γ₁ Δ}(σ₂ : σ₀ ≡[ Tms= Γ₂ refl ]≡ σ₁)
      → (⌜ A ⌝T [ σ₀ ]T) ≡[ Ty= Γ₂ ]≡ ⌜ A [ σ₁ ]NT ⌝T

NΠ A B [ σ ]NT
  = NΠ (A [ σ ]NT)
       (B [ coe (Tms= (,C= refl (⌜[]⌝T {A = A} refl {σ} refl)) refl) (σ ^ ⌜ A ⌝T) ]NT)
NU [ σ ]NT = NU
NEl Â [ σ ]NT = NEl (coe (Tm= refl U[]) (Â [ σ ]t))

⌜[]⌝T {A = NΠ A B} refl refl
  = Π[] ◾ Π= refl
             (⌜[]⌝T {A = A} refl refl)
             (⌜[]⌝T {A = B} (,C= refl (⌜[]⌝T {A = A} refl refl)) refl)
⌜[]⌝T {A = NU} refl refl = U[]
⌜[]⌝T {A = NEl Â} refl refl = El[] ◾ El= refl (from≃ (uncoe (TmΓ= U[]) ⁻¹̃ ◾̃ uncoe (Tm= refl U[])))

-- functoriality laws

[]NT= : {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
        {A₀ : NTy Δ₀}{A₁ : NTy Δ₁}(A₂ : A₀ ≡[ NTy= Δ₂ ]≡ A₁)
        {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
        {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ σ₁)
      → (A₀ [ σ₀ ]NT) ≡[ NTy= Γ₂ ]≡ (A₁ [ σ₁ ]NT)
[]NT= refl refl refl refl = refl

[]NT≃ : {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
        {A₀ : NTy Δ₀}{A₁ : NTy Δ₁}(A₂ : A₀ ≃ A₁)
        {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
        {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≃ σ₁)
      → (A₀ [ σ₀ ]NT) ≃ (A₁ [ σ₁ ]NT)
[]NT≃ refl (refl ,≃ refl) refl (refl ,≃ refl) = refl ,≃ refl

[id]NT : {Γ : Con}{A : NTy Γ} → (A [ id ]NT) ≡ A
[id]NT {Γ}{NΠ A B}
  = NΠ= {Γ} refl
        {A [ id ]NT}{A} ([id]NT {A = A})
        ( []NT= refl refl (,C= refl (⌜⌝T= refl [id]NT))
                (from≃ ( uncoe (Tms= (,C= refl (⌜⌝T= refl [id]NT)) refl) ⁻¹̃
                       ◾̃ uncoe (Tms= (,C= refl (⌜[]⌝T {Γ}{A} refl refl)) refl) ⁻¹̃
                       ◾̃ id^ {Γ}{⌜ A ⌝T}))
        ◾ [id]NT {A = B})
[id]NT {A = NU} = refl
[id]NT {A = NEl Â} = NEl= refl (from≃ ( uncoe (Tm= refl U[]) ⁻¹̃ ◾̃ [id]t'))

abstract
  [][]NT : ∀{Γ Δ Σ}{A : NTy Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → A [ δ ]NT [ σ ]NT ≡ A [ δ ∘ σ ]NT
  [][]NT {Γ}{Δ}{Σ}{NΠ A B}{σ}{δ}
    = NΠ= refl
          ([][]NT {A = A})
          (from≃ ( uncoe (NTy= (,C= refl (⌜⌝T= refl [][]NT))) ⁻¹̃
                 ◾̃ to≃ ([][]NT {A = B})
                 ◾̃ []NT≃ refl
                         r̃
                         (,C≃ refl (to≃ (⌜⌝T= refl ([][]NT {A = A}))))
                         (∘≃' (,C≃ refl (to≃ (⌜[]⌝T {Σ}{A} refl refl ⁻¹)))
                              refl
                              (,C= refl (⌜[]⌝T {A = A [ δ ]NT} refl refl ⁻¹
                                        ◾ []T= refl refl (⌜[]⌝T {A = A} refl refl ⁻¹) refl))
                              (uncoe (Tms= (,C= refl (⌜[]⌝T {A = A} refl refl)) refl) ⁻¹̃)
                              (uncoe (Tms= (,C= refl (⌜[]⌝T {A = A [ δ ]NT} refl refl)) refl) ⁻¹̃
                              ◾̃ ^≃'' (⌜[]⌝T {A = A} refl refl ⁻¹))
                         ◾̃ ∘^ ⁻¹̃
                         ◾̃ uncoe (Tms= (,C= refl (⌜[]⌝T {Σ}{A} refl refl)) refl))))
  [][]NT {A = NU} = refl
  [][]NT {A = NEl Â}
    = NEl= refl (from≃ ( uncoe (Tm= refl U[]) ⁻¹̃
                       ◾̃ []t≃' refl refl (to≃ (U[] ⁻¹)) (uncoe (Tm= refl U[]) ⁻¹̃) r̃
                       ◾̃ [][]t'
                       ◾̃ uncoe (Tm= refl U[])))

----------------------------------------------------------------------
-- Dependent model of normal types
----------------------------------------------------------------------

open import TT.DepModel

d : Declᴹ
d = record
  { Conᴹ = λ _ → ⊤
  ; Tyᴹ = λ {Γ} _ A → Σ (NTy Γ) λ A' → A ≡ ⌜ A' ⌝T
  ; Tmsᴹ = λ _ _ _ → ⊤
  ; Tmᴹ = λ _ _ _ → ⊤
  }

open Declᴹ d public

=Tyᴹ : {Γ : Con}
       {A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
       {A'₀ A'₁ : NTy Γ}(A'₂ : A'₀ ≡ A'₁)
       {pA₀ : A₀ ≡ ⌜ A'₀ ⌝T}{pA₁ : A₁ ≡ ⌜ A'₁ ⌝T}
     → (A'₀ ,Σ pA₀) ≡[ TyΓᴹ= {Γ} A₂ ]≡ (A'₁ ,Σ pA₁)
=Tyᴹ refl refl {refl}{refl} = refl

c : Coreᴹ d
c = record
  { •ᴹ     = tt
  ; _,Cᴹ_  = λ _ _ → tt
  ; _[_]Tᴹ = λ { {Γ}{_}{Δ}{_}{A}(A' ,Σ pA){σ} _
               →  A' [ σ ]NT
               ,Σ ([]T= refl refl pA refl ◾ ⌜[]⌝T {A = A'} refl refl)
               }
  ; εᴹ     = tt
  ; _,sᴹ_  = λ _ _ → tt
  ; idᴹ    = tt
  ; _∘ᴹ_   = λ _ _ → tt
  ; π₁ᴹ    = λ _ → tt
  ; _[_]tᴹ = λ _ _ → tt
  ; π₂ᴹ    = λ _ → tt
  ; [id]Tᴹ = λ { {Γ}{_}{A}{A' ,Σ pA}
               → =Tyᴹ {Γ}{A [ id ]T}{A} [id]T {A' [ id ]NT}{A'} [id]NT {_}{_}
               }
  ; [][]Tᴹ = λ { {Γ}{_}{Δ}{_}{Σ}{_}{A}{A' ,Σ pA}{σ}{_}{δ}{_}
               → =Tyᴹ {Γ}{A [ δ ]T [ σ ]T}{A [ δ ∘ σ ]T} [][]T
                       {A' [ δ ]NT [ σ ]NT}{A' [ δ ∘ σ ]NT} [][]NT
                       {_}{_}
               }
  ; idlᴹ   = refl
  ; idrᴹ   = refl
  ; assᴹ   = refl
  ; π₁βᴹ   = refl
  ; πηᴹ    = refl
  ; εηᴹ    = refl
  ; ,∘ᴹ    = refl
  ; π₂βᴹ   = refl
  }

b : Baseᴹ d c
b = record
  { Uᴹ    = λ {Γ}{_} → NU ,Σ refl
  ; Elᴹ   = λ {Γ}{_}{Â} _ → NEl Â ,Σ refl
  ; U[]ᴹ  = λ { {Γ}{_}{Δ}{_}{σ}{_}
             → =Tyᴹ {Γ}{U [ σ ]T}{U} U[] {NU [ σ ]NT}{NU} refl {_}{_}
             }
  ; El[]ᴹ = λ { {Γ}{_}{Δ}{_}{σ}{_}{Â}{_}
              → =Tyᴹ {Γ}{El Â [ σ ]T}{El (coe (TmΓ= U[]) (Â [ σ ]t))} El[]
                     (NEl= refl (from≃ (uncoe (Tm= refl U[]) ⁻¹̃ ◾̃ uncoe (TmΓ= U[]))))
                     {_}{_}
              }
  }

f : Funcᴹ d c
f = record
  { Πᴹ     = λ { {Γ}{_}{A}(A' ,Σ pA){B}(B' ,Σ pB)
           →  NΠ A' (coe (NTy= (,C= refl pA)) B')
           ,Σ Π= refl pA (from≃ ( uncoe (Ty= (,C= refl pA)) ⁻¹̃
                               ◾̃ to≃ pB
                               ◾̃ ⌜⌝T≃ (,C= refl pA) (uncoe (NTy= (,C= refl pA)))))
           }
  ; appᴹ   = λ _ → tt
  ; lamᴹ   = λ _ → tt
  ; Π[]ᴹ   = λ { {Γ}{_}{Δ}{_}{σ}{_}{A}{A' ,Σ pA}{B}{B' ,Σ pB}
             → =Tyᴹ {Γ}{Π A B [ σ ]T}{Π (A [ σ ]T) (B [ σ ^ A ]T)} Π[]
                     {NΠ A' (coe (NTy= (,C= refl pA)) B') [ σ ]NT}
                     {NΠ (A' [ σ ]NT) _}
                     (NΠ= refl
                          refl
                          (from≃ ( []NT≃ (,C= refl (pA ⁻¹))
                                         (uncoe (NTy= (,C= refl pA)) ⁻¹̃)
                                         (,C= refl (⌜[]⌝T {A = A'} refl refl ⁻¹ ◾ []T= refl refl (pA ⁻¹) refl))
                                         ( uncoe (Tms= (,C= refl (⌜[]⌝T {A = A'} refl refl)) refl) ⁻¹̃
                                         ◾̃ ^≃'' (pA ⁻¹) )
                                 ◾̃ uncoe (NTy= (,C= refl ([]T= refl refl pA refl ◾ ⌜[]⌝T {A = A'} refl refl))))))
                     {_}{_}
             }
  ; lam[]ᴹ = refl
  ; Πβᴹ    = refl
  ; Πηᴹ    = refl
  }

open import TT.Elim

open elim d c b f

normT : ∀{Γ} → Ty Γ → NTy Γ
normT A = proj₁ (ElimTy A)

abstract
  complT : ∀{Γ}(A : Ty Γ) → A ≡ ⌜ normT A ⌝T
  complT A = proj₂ (ElimTy A)

normT≃ : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
       → normT A₀ ≃ normT A₁
normT≃ refl (refl ,≃ refl) = refl ,≃ refl

normT= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
       → normT A₀ ≡[ NTy= Γ₂ ]≡ normT A₁
normT= refl refl = refl
