{-# OPTIONS --no-eta --rewriting #-}

-- stability of type normalisation

module NormTy.Stab where

open import lib
open import JM

open import TT.Syntax
open import TT.Congr
import TT.Elim
open import NormTy.NTy
open import NormTy.NormTy

abstract
  stabT : ∀{Γ}(A : NTy Γ) → normT ⌜ A ⌝T ≡ A
  stabT (NΠ A B)
    = NΠ= refl
          (stabT A)
          (from≃ ( uncoe (NTy= (,C= refl (⌜⌝T= refl (stabT A)))) ⁻¹̃
                 ◾̃ uncoe (NTy= (,C= refl (proj₂ (TT.Elim.elim.ElimTy d c b f ⌜ A ⌝T)))) ⁻¹̃
                 ◾̃ to≃ (stabT B)))
  stabT NU = refl
  stabT (NEl Â) = refl
