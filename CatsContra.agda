{-# OPTIONS --no-eta #-}

module CatsContra where

open import lib
open import JM

----------------------------------------------------------------------
-- Category
----------------------------------------------------------------------

-- the fields in Cat are not defined as projections but you are
-- expected to always do open Cat C for some category C (the category
-- is usually fixed)

record Cat : Set₁ where
  constructor conCat
  field
    Obj  : Set
    _⇒_  : Obj → Obj → Set
    idc  : ∀{I} → I ⇒ I
    _∘c_ : ∀{I J K} → J ⇒ K → I ⇒ J → I ⇒ K
    idl  : ∀{I J}{f : I ⇒ J} → idc ∘c f ≡ f
    idr  : ∀{I J}{f : I ⇒ J} → f ∘c idc ≡ f
    ass  : ∀{I J K L}{f : I ⇒ J}{g : J ⇒ K}{h : K ⇒ L}
         → (h ∘c g) ∘c f ≡ h ∘c (g ∘c f)
         
  infix 6 _∘c_
  infix 4 _⇒_

----------------------------------------------------------------------
-- Opposite category
----------------------------------------------------------------------

_ᵒᵖ : Cat → Cat
record { Obj = Obj ; _⇒_ = _⇒_ ; idc = idc ; _∘c_ = _∘c_ ; idl = idl ; idr = idr ; ass = ass } ᵒᵖ
  = record
  { Obj  = Obj
  ; _⇒_  = λ Γ Δ → Δ ⇒ Γ
  ; idc  = idc
  ; _∘c_ = λ β γ → γ ∘c β
  ; idl  = idr
  ; idr  = idl 
  ; ass  = ass ⁻¹ }

-- in the records below, the fields are defined as projections so they
-- are opened and when you use them, you always mention the particular
-- presheaf (quite often we have more than one presheaf in context)

----------------------------------------------------------------------
-- Presheaf (P)
----------------------------------------------------------------------

record PSh (C : Cat) : Set₁ where
  constructor conPSh
  open Cat C
  field
    _$P_   : Obj → Set                    
    _$P_$_ : ∀{I J} → J ⇒ I → _$P_ I → _$P_ J
    idP    : ∀{I}{α : _$P_ I} → _$P_$_ idc α ≡ α
    compP  : ∀{I J K}{f : J ⇒ I}{g : K ⇒ J}{α : _$P_ I}
           → _$P_$_ (f ∘c g) α ≡ _$P_$_ g (_$P_$_ f α)
         
  infix 6 _$P_ _$P_$_

open PSh public

----------------------------------------------------------------------
-- Natural transformation (n)
----------------------------------------------------------------------

record _→n_ {C : Cat}(Γ Δ : PSh C) : Set where
  constructor con→n
  open Cat C
  field 
    _$n_ : ∀{I} → Γ $P I → Δ $P I
    natn : ∀{I J}{f : J ⇒ I}{α : Γ $P I}
         → Δ $P f $ _$n_ α ≡ _$n_ (Γ $P f $ α)
         
  infix 6 _$n_
  
infix 5 _→n_

open _→n_ public

----------------------------------------------------------------------
-- Family of presheaves (F)
----------------------------------------------------------------------

record FamPSh {C : Cat}(Γ : PSh C) : Set₁ where
  constructor conFamPSh
  open Cat C
  field
    _$F_   : ∀{I} → Γ $P I → Set
    _$F_$_ : ∀{I J}(f : J ⇒ I){α : Γ $P I} → _$F_ α → _$F_ (Γ $P f $ α)
    idF    : ∀{I}{α : Γ $P I}{x : _$F_ α} → _$F_$_ idc x ≡[ ap _$F_ (idP Γ) ]≡ x
    compF  : ∀{I J K}{f : J ⇒ I}{g : K ⇒ J}{α : Γ $P I}{x : _$F_ α}
           → _$F_$_ (f ∘c g) x ≡[ ap _$F_ (compP Γ) ]≡ _$F_$_ g (_$F_$_ f x)
            
  infix 6 _$F_ _$F_$_
  
open FamPSh public

abstract
  idF' : ∀{C : Cat} → let open Cat C in
         {Γ : PSh C}(A : FamPSh Γ){I : Obj}{α : Γ $P I}{x : A $F α} → A $F idc $ x ≃ x
  idF' {Γ = Γ} A = from≡ (ap (_$F_ A) (idP Γ)) (idF A)

abstract
  compF'  : ∀{C : Cat} → let open Cat C in
            {Γ : PSh C}(A : FamPSh Γ)
            {I J K : Obj}{f : J ⇒ I}{g : K ⇒ J}{α : Γ $P I}{x : A $F α}
          → A $F (f ∘c g) $ x ≃ A $F g $ (A $F f $ x)
  compF' {Γ = Γ} A = from≡ (ap (_$F_ A) (compP Γ)) (compF A)

----------------------------------------------------------------------
-- Section of a family of presheaves (S)
----------------------------------------------------------------------

record _→S_ {C : Cat}(Γ : PSh C)(A : FamPSh Γ) : Set where
  constructor con→S
  open Cat C
  field
    _$S_ : ∀{I}(α : Γ $P I) → A $F α
    natS : ∀{I J}{f : J ⇒ I}{α : Γ $P I}
         → A $F f $ _$S_ α ≡ _$S_ (Γ $P f $ α)
         
  infix 6 _$S_

infix 5 _→S_

open _→S_ public

----------------------------------------------------------------------
-- A natural tranformation between two families of presheaves (N)
----------------------------------------------------------------------

record _→N_ {C : Cat}{Γ : PSh C}(A B : FamPSh Γ) : Set where
  constructor con→N
  open Cat C
  field
    _$N_ : ∀{I}{α : Γ $P I} → A $F α → B $F α
    natN : ∀{I J}{f : J ⇒ I}{α : Γ $P I}{x : A $F α}
          → B $F f $ (_$N_ x) ≡ _$N_ (A $F f $ x)
          
  infix 6 _$N_
  
infix 5 _→N_

open _→N_ public

----------------------------------------------------------------------
-- Equality constructors
----------------------------------------------------------------------

abstract
  =PSh : {C : Cat}{Γ₀ Γ₁ : PSh C} → let open Cat C in
         ($P= : _≡_ {A = Obj → Set} (_$P_ Γ₀) (_$P_ Γ₁))
       → (λ {I}{J} → _$P_$_ Γ₀ {I}{J}) ≡[ ap (λ z → {I J : Obj} → J ⇒ I → z I → z J) $P= ]≡ (λ {I}{J} → _$P_$_ Γ₁ {I}{J}) → Γ₀ ≡ Γ₁
  =PSh {Γ₀ = record { _$P_ = _$P_ ; _$P_$_ = _$P_$_ ; idP = idP₀ ; compP = compP₀ }}
       {record { _$P_ = ._$P_ ; _$P_$_ = ._$P_$_ ; idP = idP₁ ; compP = compP₁ }} refl refl
         = ap2 (λ idP compP → record { _$P_ = _$P_ ; _$P_$_ = _$P_$_ ; idP = λ {x}{y} → idP {x}{y} ; compP = λ {x₁}{x₂}{x₃}{x₄}{x₅}{x₆} → compP {x₁}{x₂}{x₃}{x₄}{x₅}{x₆} })
          (funexti (λ I → funexti (λ α → UIP idP₀ idP₁)))
          (funexti (λ I → funexti (λ J → funexti (λ K → funexti (λ f → funexti (λ g → funexti (λ α → UIP compP₀ compP₁)))))))

abstract
  =Psh' : {C : Cat}{Γ₀ Γ₁ : PSh C} → let open Cat C in
          (obj= : (I : Obj) → Γ₀ $P I ≡ Γ₁ $P I)
          (mor= : ∀{I J}{f : J ⇒ I}{α₀ : Γ₀ $P I}{α₁ : Γ₁ $P I}(α₂ : α₀ ≃ α₁) → Γ₀ $P f $ α₀ ≃ Γ₁ $P f $ α₁)
        → Γ₀ ≡ Γ₁
  =Psh' {C}{Γ₀}{Γ₁} obj= mor=
    = =PSh (funext obj=)
           (from≃ ( uncoe (ap (λ z → {I J : Obj} → J ⇒ I → z I → z J) (funext obj=)) ⁻¹̃
                  ◾̃ funexti≃' (λ I → funexti≃' (λ J → funext≃' (λ f → funext≃ (obj= I) mor=)))))
    where
      open Cat C

abstract
  =FamPSh
    : {C : Cat}{Γ : PSh C}{A₀ A₁ : FamPSh Γ}
      ($F= : _≡_ {A = ∀{I} → Γ $P I → Set} (_$F_ A₀) (_$F_ A₁))
    → (λ {I}{J} → _$F_$_ A₀ {I}{J})
      ≡[ ap (λ z → ∀{I J}(f : Cat._⇒_ C J I){α : Γ $P I} → z α → z (Γ $P f $ α)) $F= ]≡
      (λ {I}{J} → _$F_$_ A₁ {I}{J})
    → A₀ ≡ A₁
  =FamPSh {A₀ = conFamPSh  $F₀  $F$₀ idF₀ compF₀}
          {A₁ = conFamPSh .$F₀ .$F$₀ idF₁ compF₁}
          refl
          refl

    = ap2 (conFamPSh $F₀ $F$₀)
          ( funexti (λ I
          → funexti (λ α
          → funexti (λ a → UIP idF₀ idF₁))))
          ( funexti (λ I
          → funexti (λ J
          → funexti (λ K
          → funexti (λ f
          → funexti (λ g
          → funexti (λ α
          → funexti (λ x → UIP compF₀ compF₁)) ))))) )

abstract
  =→n  : {C : Cat}{Γ Δ : PSh C}{δ₀ δ₁ : Γ →n Δ}
       → _≡_ {A = ∀{I} → Γ $P I → Δ $P I} (_$n_ δ₀) (_$n_ δ₁)
       → δ₀ ≡ δ₁
  =→n {δ₀ = con→n _$n₀_ nat₀} {con→n ._$n₀_ nat₁} refl

    = ap (con→n _$n₀_)
         ( funexti (λ I
         → funexti (λ J
         → funexti (λ f
         → funexti (λ α → UIP nat₀ nat₁)))))

abstract
  ≃→S : {C : Cat}{Γ : PSh C}{A₀ A₁ : FamPSh Γ}(A₂ : A₀ ≡ A₁)
             {t₀ : Γ →S A₀}{t₁ : Γ →S A₁}
           → (t₂ : _≃_ {A = ∀{I}(α : Γ $P I) → A₀ $F α}
                       {B = ∀{I}(α : Γ $P I) → A₁ $F α}
                       (_$S_ t₀)
                       (_$S_ t₁))
           → t₀ ≃ t₁
  ≃→S refl {t₀ = con→S _$S₀_ Snat₀}{con→S ._$S₀_ Snat₁} (refl ,≃ refl)

    = refl ,≃ ap (con→S _$S₀_)
                 ( funexti (λ I
                 → funexti (λ J
                 → funexti (λ f
                 → funexti (λ α
                 → UIP Snat₀ Snat₁)))))

----------------------------------------------------------------------
-- Congruences
----------------------------------------------------------------------

Γ→S= : {C : Cat}{Γ : PSh C}{A₀ A₁ : FamPSh Γ}(A₂ : A₀ ≡ A₁)
          → Γ →S A₀ ≡ Γ →S A₁
Γ→S= {C}{Γ} = ap (_→S_ Γ)

$F= : {C : Cat} → let open Cat C in
      {Γ : PSh C}{I : Obj}(A : FamPSh Γ)
      {α₀ α₁ : Γ $P I}(α₂ : α₀ ≡ α₁)
    → A $F α₀ ≡ A $F α₁
$F= A = ap (_$F_ A)

$Ff$≃ : {C : Cat} → let open Cat C in
        {Γ : PSh C}{I J : Obj}(A : FamPSh Γ){f : J ⇒ I}
        {α₀ α₁ : Γ $P I}(α₂ : α₀ ≡ α₁)
        {x₀ : A $F α₀}{x₁ : A $F α₁}(x₂ : x₀ ≃ x₁)
      → A $F f $ x₀ ≃ A $F f $ x₁
$Ff$≃ A refl (refl ,≃ refl) = refl ,≃ refl

$F-$x≃ : {C : Cat} → let open Cat C in
         {Γ : PSh C}{I J : Obj}(A : FamPSh Γ)
         {f₀ f₁ : J ⇒ I}(f₂ : f₀ ≡ f₁)
         {α : Γ $P I}{x : A $F α}
       → A $F f₀ $ x ≃ A $F f₁ $ x
$F-$x≃ A refl = refl ,≃ refl

t$S≃ : {C : Cat} → let open Cat C in
       {Γ : PSh C}{A : FamPSh Γ}{t : Γ →S A}
       {I : Obj}{α₀ α₁ : Γ $P I}(α₂ : α₀ ≡ α₁)
     → t $S α₀ ≃ t $S α₁
t$S≃ refl = refl ,≃ refl

-$N≃ : {C : Cat}{F : PSh C}{A B : FamPSh F}(t : A →N B)
       {I : Cat.Obj C}
       {α₀ α₁ : F $P I}(α₂ : α₀ ≡ α₁)
       {v₀ : A $F α₀}{v₁ : A $F α₁}(v₂ : v₀ ≃ v₁)
     → t $N v₀ ≃ t $N v₁
-$N≃ _ refl (refl ,≃ refl) = refl ,≃ refl

----------------------------------------------------------------------
-- A law
----------------------------------------------------------------------

coe$S : {C : Cat} → let open Cat C in
        {Γ : PSh C}{A₀ A₁ : FamPSh Γ}(A₂ : A₀ ≡ A₁)
        {t : Γ →S A₀}{I : Obj}{α : Γ $P I}
      → coe (Γ→S= A₂) t $S α ≃ t $S α
coe$S refl = refl ,≃ refl

----------------------------------------------------------------------
-- extending a presheaf by a family of presheaves
----------------------------------------------------------------------

abstract
  _,P_idP : {C : Cat}{F : PSh C}(A : FamPSh F) → let open Cat C in
            {I : Obj}{α : Σ (F $P I) (_$F_ A)}
          → F $P idc $ proj₁ α ,Σ A $F idc $ proj₂ α ≡ α
  _,P_idP {F = F} A = ,Σ≃' (idP F) (from≡ (ap (_$F_ A) (idP F)) (idF A))

abstract
  _,P_compP : {C : Cat}{F : PSh C}(A : FamPSh F) → let open Cat C in
              {I J K : Obj}{f : J ⇒ I}{g : K ⇒ J}{α : Σ (F $P I) (_$F_ A)}
            → _≡_ {A = Σ (F $P K) λ α → A $F α}
                  (F $P f ∘c g $ proj₁ α ,Σ A $F f ∘c g $ proj₂ α)
                  (F $P g $ (F $P f $ proj₁ α) ,Σ A $F g $ (A $F f $ proj₂ α))
  _,P_compP {F = F} A = ,Σ≃' (compP F) (from≡ (ap (_$F_ A) (compP F)) (compF A))

_,P_ : {C : Cat}(F : PSh C)(A : FamPSh F) → PSh C
_,P_ {C} F A = record

  { _$P_   = λ I → Σ (F $P I) (_$F_ A)
  
  ; _$P_$_ = λ { f (α ,Σ a) → F $P f $ α ,Σ A $F f $ a }
  
  ; idP    = _,P_idP A
  
  ; compP  = _,P_compP A
  
  }
  
  where
  
    open Cat C

infixl 5 _,P_

----------------------------------------------------------------------
-- substituting a family of presheaves with a natural transformation
----------------------------------------------------------------------

_[_]F : {C : Cat}{P Q : PSh C} → FamPSh P → (Q →n P) → FamPSh Q
_[_]F {C}{P}{Q} F ω = record

  { _$F_   = λ α → F $F (ω $n α)
  
  ; _$F_$_ = λ β v → coe (ap (_$F_ F) (natn ω))
                         (F $F β $ v)
  
  ; idF    = pidF
  ; compF  = pcompF
  
  }
  where
    open Cat C
    abstract
      pidF : {I : Obj}{α : Q $P I}{x : F $F (ω $n α)}
           → coe (ap (_$F_ F) (natn ω)) (F $F Cat.idc C $ x)
           ≡[ ap (λ α₁ → F $F (ω $n α₁)) (idP Q) ]≡
             x
      pidF = from≃ ( uncoe (ap (λ α → F $F (ω $n α)) (idP Q)) ⁻¹̃
                   ◾̃ uncoe (ap (_$F_ F) (natn ω)) ⁻¹̃
                   ◾̃ from≡ (ap (_$F_ F) (idP P)) (idF F))

    abstract
      pcompF : {I J K : Obj}{f : J ⇒ I}{g : K ⇒ J}{α : Q $P I}
               {x : F $F (ω $n α)}
             → coe (ap (_$F_ F) (natn ω))
                   (F $F (C Cat.∘c f) g $ x)
             ≡[ ap (λ α₁ → F $F (ω $n α₁)) (compP Q) ]≡
               coe (ap (_$F_ F) (natn ω))
                   (F $F g $ coe (ap (_$F_ F) (natn ω))
                                 (F $F f $ x))
      pcompF = from≃ ( uncoe (ap (λ α → F $F (ω $n α)) (compP Q)) ⁻¹̃
                     ◾̃ uncoe (ap (_$F_ F) (natn ω)) ⁻¹̃
                     ◾̃ from≡ (ap (_$F_ F) (compP P)) (compF F)
                     ◾̃ $Ff$≃ F (natn ω) (uncoe (ap (_$F_ F) (natn ω))) 
                     ◾̃ uncoe (ap (_$F_ F) (natn ω)))

infixl 7 _[_]F

----------------------------------------------------------------------
-- pointwise product of presheaves
----------------------------------------------------------------------

_×P_ : {C : Cat} → PSh C → PSh C → PSh C
_×P_ {C} F G = record

  { _$P_   = λ I → (F $P I) × (G $P I)

  ; _$P_$_ = λ { f (a ,Σ b) → F $P f $ a ,Σ G $P f $ b }

  ; idP    = ,×= (idP F) (idP G)

  ; compP  = ,×= (compP F) (compP G)

  }

infixl 5 _×P_

----------------------------------------------------------------------
-- pointwise product of families of presheaves
----------------------------------------------------------------------

_×F_ : {C : Cat}{F G : PSh C}(A : FamPSh F)(B : FamPSh G)
     → FamPSh (F ×P G)
_×F_ {C}{F}{G} A B = record

  { _$F_   = λ α → (A $F proj₁ α) × (B $F proj₂ α)
  
  ; _$F_$_ = λ f a → (A $F f $ proj₁ a) ,Σ (B $F f $ proj₂ a)
  
  ; idF    = pidF

  ; compF  = pcompF

  }

  where

    open Cat C

    abstract
      pidF : ∀{I}{α : (F ×P G) $P I}{x : (A $F proj₁ α) × (B $F proj₂ α)}
           → A $F idc $ proj₁ x ,Σ B $F idc $ proj₂ x
           ≡[ ap (λ z → (A $F proj₁ z) × (B $F proj₂ z)) (idP (F ×P G)) ]≡
             x
      pidF = from≃ ( uncoe (ap (λ z → (A $F proj₁ z) × (B $F proj₂ z)) (idP (F ×P G))) ⁻¹̃
                   ◾̃ ,Σ≃≃ (funext≃ (ap (_$F_ A) (idP F))
                                   (λ _ → to≃ ($F= B (idP G))))
                          (idF' A)
                          (idF' B))

    abstract
      pcompF : ∀{I J K}{f : J ⇒ I}{g : K ⇒ J}
               {α : (F ×P G) $P I} {x : (A $F proj₁ α) × (B $F proj₂ α)}
             → A $F f ∘c g $ proj₁ x ,Σ B $F f ∘c g $ proj₂ x
             ≡[ ap (λ α₁ → (A $F proj₁ α₁) × (B $F proj₂ α₁)) (compP (F ×P G)) ]≡
               A $F g $ (A $F f $ proj₁ x) ,Σ B $F g $ (B $F f $ proj₂ x)
      pcompF = from≃ ( uncoe (ap (λ α₁ → (A $F proj₁ α₁) × (B $F proj₂ α₁)) (compP (F ×P G))) ⁻¹̃
                     ◾̃ ,Σ≃≃ (funext≃ (ap (_$F_ A) (compP F))
                                     (λ _ → to≃ ($F= B (compP G))))
                            (compF' A)
                            (compF' B))

infixl 5 _×F_

----------------------------------------------------------------------
-- pointwise product of families of presheaves over the same presheaf
----------------------------------------------------------------------

_×F'_ : {C : Cat}{F : PSh C}(A B : FamPSh F)
      → FamPSh F
_×F'_ {C}{F} A B = record

  { _$F_   = λ α → (A $F α) × (B $F α)

  ; _$F_$_ = λ f α → A $F f $ proj₁ α ,Σ B $F f $ proj₂ α

  ; idF    = pidF

  ; compF  = pcompF

  }
  
  where

    open Cat C

    abstract

      pidF : ∀{I}{α : F $P I}{x : (A $F α) × (B $F α)}
           → A $F idc $ proj₁ x ,Σ B $F idc $ proj₂ x
           ≡[ ap (λ α₁ → (A $F α₁) × (B $F α₁)) (idP F) ]≡
             x
      pidF = from≃ ( uncoe (ap (λ α₁ → (A $F α₁) × (B $F α₁)) (idP F)) ⁻¹̃
                   ◾̃ ,Σ≃≃ (funext≃ (ap (_$F_ A) (idP F))
                                   (λ _ → to≃ ($F= B (idP F))))
                          (idF' A)
                          (idF' B))

      pcompF : ∀{I J K}{f : J ⇒ I}{g : K ⇒ J}
               {α : F $P I}{x : (A $F α) × (B $F α)}
             → A $F f ∘c g $ proj₁ x ,Σ B $F f ∘c g $ proj₂ x
             ≡[ ap (λ α₁ → (A $F α₁) × (B $F α₁)) (compP F) ]≡
               A $F g $ (A $F f $ proj₁ x) ,Σ B $F g $ (B $F f $ proj₂ x)
      pcompF = from≃ ( uncoe (ap (λ α₁ → (A $F α₁) × (B $F α₁)) (compP F)) ⁻¹̃
                     ◾̃ ,Σ≃≃ (funext≃ (ap (_$F_ A) (compP F))
                                    (λ _ → to≃ ($F= B (compP F))))
                            (compF' A)
                            (compF' B))

infixl 5 _×F'_

----------------------------------------------------------------------
-- forming natural transformations
----------------------------------------------------------------------

_×n_ : {C : Cat}{F₀ G₀ F₁ G₁ : PSh C} → (F₀ →n F₁) → (G₀ →n G₁)
     → (F₀ ×P G₀) →n (F₁ ×P G₁)
_×n_ {C}{F₀}{G₀}{F₁}{G₁} ρ σ = record

  { _$n_ = λ α → (ρ $n proj₁ α) ,Σ σ $n proj₂ α
  
  ; natn = ,×= (natn ρ) (natn σ)
  
  }

infixl 5 _×n_

wkn : {C : Cat}{F : PSh C}{A : FamPSh F} → (F ,P A) →n F
wkn = record { _$n_ = λ α → proj₁ α ; natn = refl }

_∘n_ : {C : Cat}{F G H : PSh C} → G →n H → F →n G → F →n H
ρ ∘n σ = record { _$n_ = λ α → ρ $n (σ $n α) ; natn = natn ρ ◾ ap (_$n_ ρ) (natn σ) }

infix 6 _∘n_

idn : {C : Cat}{F : PSh C} → F →n F
idn = record { _$n_ = λ a → a ; natn = refl }

totS : {C : Cat}{F : PSh C}{A : FamPSh F} → F →S A → F →n (F ,P A)
totS ρ = record { _$n_ = λ α → α ,Σ ρ $S α ; natn = ,Σ= refl (natS ρ) }

abstract
  id,,idnat : {C : Cat}{F : PSh C}{A B : FamPSh F}(D : FamPSh F)
              (t : A →N B)
              {I J : Cat.Obj C}{f : Cat._⇒_ C J I}
              {α : (F ,P A ,P D [ wkn ]F) $P I}
            → _≡_ {A = (F ,P B ,P D [ wkn ]F) $P J}
                  (F $P f $ proj₁ (proj₁ α) ,Σ B $F f $ (t $N proj₂ (proj₁ α)) ,Σ D $F f $ proj₂ α)
                  (F $P f $ proj₁ (proj₁ α) ,Σ t $N (A $F f $ proj₂ (proj₁ α)) ,Σ D $F f $ proj₂ α)
  id,,idnat D t = ,Σ≃' (,Σ≃' refl (to≃ (natN t))) r̃
  
id,_,id : {C : Cat}{F : PSh C}{A B D : FamPSh F}
        → A →N B
        → (F ,P A ,P D [ wkn ]F) →n (F ,P B ,P D [ wkn ]F)
id,_,id {C}{F}{A}{B}{D} t = record
  { _$n_ = λ { {I} (α ,Σ a ,Σ d) → α ,Σ t $N a ,Σ d }
  ; natn = id,,idnat D t
  }

----------------------------------------------------------------------
-- pointwise product of sections
----------------------------------------------------------------------

_×S_ : {C : Cat}{F G : PSh C}{A : FamPSh F}{B : FamPSh G}
     → F →S A → G →S B → (F ×P G) →S (A ×F B)
t ×S u = record

  { _$S_ = λ α → t $S proj₁ α ,Σ u $S proj₂ α

  ; natS = ,×= (natS t) (natS u)

  }

infixl 5 _×S_

----------------------------------------------------------------------
-- lift a section
----------------------------------------------------------------------

_^S_ : {C : Cat}{F : PSh C}{A : FamPSh F} → F →S A → (B : FamPSh F)
      → (F ,P B) →n (F ,P A ,P B [ wkn ]F)
t ^S B = record

  { _$n_ = λ α → (proj₁ α) ,Σ (t $S proj₁ α) ,Σ proj₂ α

  ; natn = ,Σ≃' (,Σ≃' refl (to≃ (natS t))) r̃

  }

infixl 5 _^S_

abstract
  natS[]S : {C : Cat}{F G : PSh C}{A : FamPSh G}(t : G →S A)(σ : F →n G) → let open Cat C in
            {I J : Obj}{f : J ⇒ I}{α : F $P I}
          → A [ σ ]F $F f $ (t $S (σ $n α)) ≡ t $S (σ $n (F $P f $ α))
  natS[]S {A = A} t σ
    = from≃ ( uncoe (ap (_$F_ A) (natn σ)) ⁻¹̃ ◾̃ to≃ (natS t) ◾̃ t$S≃ {t = t} (natn σ))

_[_]S : {C : Cat}{F G : PSh C}{A : FamPSh G} → G →S A → (σ : F →n G) → F →S A [ σ ]F
t [ σ ]S = record { _$S_ = λ {I} α → t $S (σ $n α)
                  ; natS = natS[]S t σ }

infixl 7 _[_]S
