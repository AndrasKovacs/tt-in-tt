module Nf.Nf where

open import lib
open import TT.Syntax
open import TT.Congr
open import JM

-- a family of predicates over the syntax which expresses that we have
-- normal forms

data Var  : (Γ : Con)(A : Ty Γ) → Tm Γ A → Set
data Ne   : (Γ : Con)(A : Ty Γ) → Tm Γ A → Set
data Nf   : (Γ : Con)(A : Ty Γ) → Tm Γ A → Set

data Var where
  zeroN : {Γ : Con}{A : Ty Γ}
          {Δ : Con}(r : Δ ≡ Γ , A)
          {C : Ty Δ}(q : C ≡[ Ty= r ]≡ A [ wk ]T)
          {t' : Tm Δ C}(p : t' ≡[ Tm= r q ]≡ vz)
        → Var Δ C t'
  sucN  : {Γ : Con}{A : Ty Γ}{t : Tm Γ A}(tN : Var Γ A t){B : Ty Γ}
          {Δ : Con}(r : Δ ≡ Γ , B)
          {C : Ty Δ}(q : C ≡[ Ty= r ]≡ A [ wk ]T)
          {t' : Tm Δ C}(p : t' ≡[ Tm= r q ]≡ vs t)
        → Var Δ C t'

data Ne where
  var  : {Γ : Con}{A : Ty Γ}{t : Tm Γ A} → Var Γ A t → Ne Γ A t
  appN : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}
         {t : Tm Γ (Π A B)}(tN : Ne Γ (Π A B) t)
         {u : Tm Γ A}(uN : Nf Γ A u)
         {C : Ty Γ}(q : C ≡ (B [ < u > ]T))
         {t' : Tm Γ C}(p : t' ≡[ Tm= refl q ]≡ (t $ u))
       → Ne Γ C t'

data Nf where
  neuU  : {Γ : Con}{t : Tm Γ U} → Ne Γ U t → Nf Γ U t
  neuEl : {Γ : Con}{Â : Tm Γ U}
          {t : Tm Γ (El Â)}(n : Ne Γ (El Â) t)
          {C : Ty Γ}(q : C ≡ El Â)
          {t' : Tm Γ C}(p : t' ≡[ Tm= refl q ]≡ t)
        → Nf Γ C t'
  lamN  : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
          (v : Nf (Γ , A) B t)
          {C : Ty Γ}(q : C ≡ Π A B)
          {t' : Tm Γ C}(p : t' ≡[ Tm= refl q ]≡ lam t)
        → Nf Γ C t'

Var= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
         {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}(t₂ : t₀ ≡[ Tm= Γ₂ A₂ ]≡ t₁)
       → Var Γ₀ A₀ t₀ ≡ Var Γ₁ A₁ t₁
Var= refl refl refl = refl

Ne=  : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
         {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}(t₂ : t₀ ≡[ Tm= Γ₂ A₂ ]≡ t₁)
       → Ne Γ₀ A₀ t₀ ≡ Ne Γ₁ A₁ t₁
Ne= refl refl refl = refl

Nf=  : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
         {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}(t₂ : t₀ ≡[ Tm= Γ₂ A₂ ]≡ t₁)
       → Nf Γ₀ A₀ t₀ ≡ Nf Γ₁ A₁ t₁
Nf= refl refl refl = refl

injneuU : {Γ₀ : Con}{t₀ : Tm Γ₀ U}{n₀ : Ne Γ₀ U t₀}
          {Γ₁ : Con}{t₁ : Tm Γ₁ U}{n₁ : Ne Γ₁ U t₁}
          (Γ₂ : Γ₀ ≡ Γ₁)(U₂ : U {Γ₀} ≡[ Ty= Γ₂ ]≡ U {Γ₁})(t₂ : t₀ ≡[ Tm= Γ₂ U₂ ]≡ t₁)
        → neuU n₀ ≡[ Nf= Γ₂ U₂ t₂ ]≡ neuU n₁ → n₀ ≡[ Ne= Γ₂ U₂ t₂ ]≡ n₁
injneuU refl refl refl refl = refl

injneuEl-Â
  : {Γ₀ : Con}{Γ₁ : Con}
    {Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}
    {t₀ : Tm Γ₀ (El Â₀)}{t₁ : Tm Γ₁ (El Â₁)}
    {n₀ : Ne Γ₀ (El Â₀) t₀}{n₁ : Ne Γ₁ (El Â₁) t₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ El Â₀}{q₁ : C₁ ≡ El Â₁}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ t₀}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ t₁}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (neuEl₂ : neuEl n₀ q₀ p₀ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ neuEl n₁ q₁ p₁)
  → Â₀ ≡[ Tm= Γ₂ (U= Γ₂) ]≡ Â₁
injneuEl-Â refl refl refl refl = refl
injneuEl-t
  : {Γ₀ : Con}{Γ₁ : Con}
    {Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}
    {t₀ : Tm Γ₀ (El Â₀)}{t₁ : Tm Γ₁ (El Â₁)}
    {n₀ : Ne Γ₀ (El Â₀) t₀}{n₁ : Ne Γ₁ (El Â₁) t₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ El Â₀}{q₁ : C₁ ≡ El Â₁}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ t₀}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ t₁}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (neuEl₂ : neuEl n₀ q₀ p₀ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ neuEl n₁ q₁ p₁)
  → t₀ ≡[ Tm= Γ₂ (El= Γ₂ (injneuEl-Â Γ₂ C₂ t'₂ neuEl₂)) ]≡ t₁
injneuEl-t refl refl refl refl = refl
injneuEl-n
  : {Γ₀ : Con}{Γ₁ : Con}
    {Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}
    {t₀ : Tm Γ₀ (El Â₀)}{t₁ : Tm Γ₁ (El Â₁)}
    {n₀ : Ne Γ₀ (El Â₀) t₀}{n₁ : Ne Γ₁ (El Â₁) t₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ El Â₀}{q₁ : C₁ ≡ El Â₁}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ t₀}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ t₁}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (neuEl₂ : neuEl n₀ q₀ p₀ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ neuEl n₁ q₁ p₁)
  → n₀ ≡[ Ne= Γ₂ (El= Γ₂ (injneuEl-Â Γ₂ C₂ t'₂ neuEl₂)) (injneuEl-t Γ₂ C₂ t'₂ neuEl₂) ]≡ n₁
injneuEl-n refl refl refl refl = refl

neuEl=
  : {Γ₀ : Con}{Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
    {Â₀ : Tm Γ₀ U}{Â₁ : Tm Γ₁ U}(Â₂ : Â₀ ≡[ Tm= Γ₂ (U= Γ₂) ]≡ Â₁)
    {t₀ : Tm Γ₀ (El Â₀)}{t₁ : Tm Γ₁ (El Â₁)}(t₂ : t₀ ≡[ Tm= Γ₂ (El= Γ₂ Â₂) ]≡ t₁)
    {n₀ : Ne Γ₀ (El Â₀) t₀}{n₁ : Ne Γ₁ (El Â₁) t₁}(n₂ : n₀ ≡[ Ne= Γ₂ (El= Γ₂ Â₂) t₂ ]≡ n₁)
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}(C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    {q₀ : C₀ ≡ El Â₀}{q₁ : C₁ ≡ El Â₁}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}(t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ t₀}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ t₁}
  → neuEl n₀ q₀ p₀ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ neuEl n₁ q₁ p₁
neuEl= refl refl refl refl refl {refl}{refl} refl {refl}{refl} = refl

injlamN-A
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
    {t₀ : Tm (Γ₀ , A₀) B₀}{t₁ : Tm (Γ₁ , A₁) B₁}
    {v₀ : Nf (Γ₀ , A₀) B₀ t₀}{v₁ : Nf (Γ₁ , A₁) B₁ t₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ Π A₀ B₀}{q₁ : C₁ ≡ Π A₁ B₁}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ lam t₀}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ lam t₁}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (lamN₂ : lamN v₀ q₀ p₀ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ lamN v₁ q₁ p₁)
  → A₀ ≡[ Ty= Γ₂ ]≡ A₁
injlamN-A refl refl refl refl = refl
injlamN-B
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
    {t₀ : Tm (Γ₀ , A₀) B₀}{t₁ : Tm (Γ₁ , A₁) B₁}
    {v₀ : Nf (Γ₀ , A₀) B₀ t₀}{v₁ : Nf (Γ₁ , A₁) B₁ t₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ Π A₀ B₀}{q₁ : C₁ ≡ Π A₁ B₁}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ lam t₀}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ lam t₁}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (lamN₂ : lamN v₀ q₀ p₀ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ lamN v₁ q₁ p₁)
  → B₀ ≡[ Ty= (,C= Γ₂ (injlamN-A Γ₂ C₂ t'₂ lamN₂)) ]≡ B₁
injlamN-B refl refl refl refl = refl
injlamN-t
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
    {t₀ : Tm (Γ₀ , A₀) B₀}{t₁ : Tm (Γ₁ , A₁) B₁}
    {v₀ : Nf (Γ₀ , A₀) B₀ t₀}{v₁ : Nf (Γ₁ , A₁) B₁ t₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ Π A₀ B₀}{q₁ : C₁ ≡ Π A₁ B₁}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ lam t₀}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ lam t₁}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (lamN₂ : lamN v₀ q₀ p₀ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ lamN v₁ q₁ p₁)
  → t₀ ≡[ Tm= (,C= Γ₂ (injlamN-A Γ₂ C₂ t'₂ lamN₂)) (injlamN-B Γ₂ C₂ t'₂ lamN₂) ]≡ t₁
injlamN-t refl refl refl refl = refl
injlamN-v
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
    {t₀ : Tm (Γ₀ , A₀) B₀}{t₁ : Tm (Γ₁ , A₁) B₁}
    {v₀ : Nf (Γ₀ , A₀) B₀ t₀}{v₁ : Nf (Γ₁ , A₁) B₁ t₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ Π A₀ B₀}{q₁ : C₁ ≡ Π A₁ B₁}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ lam t₀}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ lam t₁}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (lamN₂ : lamN v₀ q₀ p₀ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ lamN v₁ q₁ p₁)
  → v₀ ≡[ Nf= (,C= Γ₂ (injlamN-A Γ₂ C₂ t'₂ lamN₂)) (injlamN-B Γ₂ C₂ t'₂ lamN₂) (injlamN-t Γ₂ C₂ t'₂ lamN₂) ]≡ v₁
injlamN-v refl refl refl refl = refl

injappN-A
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
    {t₀ : Tm Γ₀ (Π A₀ B₀)}{t₁ : Tm Γ₁ (Π A₁ B₁)}
    {tN₀ : Ne Γ₀ (Π A₀ B₀) t₀}{tN₁ : Ne Γ₁ (Π A₁ B₁) t₁}
    {u₀ : Tm Γ₀ A₀}{u₁ : Tm Γ₁ A₁}
    {uN₀ : Nf Γ₀ A₀ u₀}{uN₁ : Nf Γ₁ A₁ u₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ (B₀ [ < u₀ > ]T)}{q₁ : C₁ ≡ (B₁ [ < u₁ > ]T)}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ (t₀ $ u₀)}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ (t₁ $ u₁)}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (appN₂ : appN tN₀ uN₀ q₀ p₀ ≡[ Ne= Γ₂ C₂ t'₂ ]≡ appN tN₁ uN₁ q₁ p₁)
  → A₀ ≡[ Ty= Γ₂ ]≡ A₁
injappN-A refl refl refl refl = refl
injappN-B
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
    {t₀ : Tm Γ₀ (Π A₀ B₀)}{t₁ : Tm Γ₁ (Π A₁ B₁)}
    {tN₀ : Ne Γ₀ (Π A₀ B₀) t₀}{tN₁ : Ne Γ₁ (Π A₁ B₁) t₁}
    {u₀ : Tm Γ₀ A₀}{u₁ : Tm Γ₁ A₁}
    {uN₀ : Nf Γ₀ A₀ u₀}{uN₁ : Nf Γ₁ A₁ u₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ (B₀ [ < u₀ > ]T)}{q₁ : C₁ ≡ (B₁ [ < u₁ > ]T)}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ (t₀ $ u₀)}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ (t₁ $ u₁)}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (appN₂ : appN tN₀ uN₀ q₀ p₀ ≡[ Ne= Γ₂ C₂ t'₂ ]≡ appN tN₁ uN₁ q₁ p₁)
  → B₀ ≡[ Ty= (,C= Γ₂ (injappN-A Γ₂ C₂ t'₂ appN₂)) ]≡ B₁
injappN-B refl refl refl refl = refl
injappN-t
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
    {t₀ : Tm Γ₀ (Π A₀ B₀)}{t₁ : Tm Γ₁ (Π A₁ B₁)}
    {tN₀ : Ne Γ₀ (Π A₀ B₀) t₀}{tN₁ : Ne Γ₁ (Π A₁ B₁) t₁}
    {u₀ : Tm Γ₀ A₀}{u₁ : Tm Γ₁ A₁}
    {uN₀ : Nf Γ₀ A₀ u₀}{uN₁ : Nf Γ₁ A₁ u₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ (B₀ [ < u₀ > ]T)}{q₁ : C₁ ≡ (B₁ [ < u₁ > ]T)}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ (t₀ $ u₀)}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ (t₁ $ u₁)}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (appN₂ : appN tN₀ uN₀ q₀ p₀ ≡[ Ne= Γ₂ C₂ t'₂ ]≡ appN tN₁ uN₁ q₁ p₁)
  → t₀ ≡[ Tm= Γ₂ (Π= Γ₂ (injappN-A Γ₂ C₂ t'₂ appN₂) (injappN-B Γ₂ C₂ t'₂ appN₂)) ]≡ t₁
injappN-t refl refl refl refl = refl
injappN-tN
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
    {t₀ : Tm Γ₀ (Π A₀ B₀)}{t₁ : Tm Γ₁ (Π A₁ B₁)}
    {tN₀ : Ne Γ₀ (Π A₀ B₀) t₀}{tN₁ : Ne Γ₁ (Π A₁ B₁) t₁}
    {u₀ : Tm Γ₀ A₀}{u₁ : Tm Γ₁ A₁}
    {uN₀ : Nf Γ₀ A₀ u₀}{uN₁ : Nf Γ₁ A₁ u₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ (B₀ [ < u₀ > ]T)}{q₁ : C₁ ≡ (B₁ [ < u₁ > ]T)}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ (t₀ $ u₀)}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ (t₁ $ u₁)}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (appN₂ : appN tN₀ uN₀ q₀ p₀ ≡[ Ne= Γ₂ C₂ t'₂ ]≡ appN tN₁ uN₁ q₁ p₁)
  → tN₀ ≡[ Ne= Γ₂ (Π= Γ₂ (injappN-A Γ₂ C₂ t'₂ appN₂) (injappN-B Γ₂ C₂ t'₂ appN₂)) (injappN-t Γ₂ C₂ t'₂ appN₂) ]≡ tN₁
injappN-tN refl refl refl refl = refl
injappN-u
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
    {t₀ : Tm Γ₀ (Π A₀ B₀)}{t₁ : Tm Γ₁ (Π A₁ B₁)}
    {tN₀ : Ne Γ₀ (Π A₀ B₀) t₀}{tN₁ : Ne Γ₁ (Π A₁ B₁) t₁}
    {u₀ : Tm Γ₀ A₀}{u₁ : Tm Γ₁ A₁}
    {uN₀ : Nf Γ₀ A₀ u₀}{uN₁ : Nf Γ₁ A₁ u₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ (B₀ [ < u₀ > ]T)}{q₁ : C₁ ≡ (B₁ [ < u₁ > ]T)}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ (t₀ $ u₀)}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ (t₁ $ u₁)}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (appN₂ : appN tN₀ uN₀ q₀ p₀ ≡[ Ne= Γ₂ C₂ t'₂ ]≡ appN tN₁ uN₁ q₁ p₁)
  → u₀ ≡[ Tm= Γ₂ (injappN-A Γ₂ C₂ t'₂ appN₂) ]≡ u₁
injappN-u refl refl refl refl = refl
injappN-uN
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}
    {t₀ : Tm Γ₀ (Π A₀ B₀)}{t₁ : Tm Γ₁ (Π A₁ B₁)}
    {tN₀ : Ne Γ₀ (Π A₀ B₀) t₀}{tN₁ : Ne Γ₁ (Π A₁ B₁) t₁}
    {u₀ : Tm Γ₀ A₀}{u₁ : Tm Γ₁ A₁}
    {uN₀ : Nf Γ₀ A₀ u₀}{uN₁ : Nf Γ₁ A₁ u₁}
    {C₀ : Ty Γ₀}{C₁ : Ty Γ₁}
    {q₀ : C₀ ≡ (B₀ [ < u₀ > ]T)}{q₁ : C₁ ≡ (B₁ [ < u₁ > ]T)}
    {t'₀ : Tm Γ₀ C₀}{t'₁ : Tm Γ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ (t₀ $ u₀)}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ (t₁ $ u₁)}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
    (appN₂ : appN tN₀ uN₀ q₀ p₀ ≡[ Ne= Γ₂ C₂ t'₂ ]≡ appN tN₁ uN₁ q₁ p₁)
  → uN₀ ≡[ Nf= Γ₂ (injappN-A Γ₂ C₂ t'₂ appN₂) (injappN-u Γ₂ C₂ t'₂ appN₂) ]≡ uN₁
injappN-uN refl refl refl refl = refl

var=
  : {Γ₀ : Con}{Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
    {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}(t₂ : t₀ ≡[ Tm= Γ₂ A₂ ]≡ t₁)
    {x₀ : Var Γ₀ A₀ t₀}{x₁ : Var Γ₁ A₁ t₁}(x₂ : x₀ ≡[ Var= Γ₂ A₂ t₂ ]≡ x₁)
  → var x₀ ≡[ Ne= Γ₂ A₂ t₂ ]≡ var x₁
var= refl refl refl refl = refl

injvar
  : {Γ₀ : Con}{Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
    {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}(t₂ : t₀ ≡[ Tm= Γ₂ A₂ ]≡ t₁)
    {x₀ : Var Γ₀ A₀ t₀}{x₁ : Var Γ₁ A₁ t₁}
  → var x₀ ≡[ Ne= Γ₂ A₂ t₂ ]≡ var x₁
  → x₀ ≡[ Var= Γ₂ A₂ t₂ ]≡ x₁
injvar refl refl refl refl = refl

injsucN-Γ
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}
    {x₀ : Var Γ₀ A₀ t₀}{x₁ : Var Γ₁ A₁ t₁}
    {B₀ : Ty Γ₀}{B₁ : Ty Γ₁}
    {Δ₀ : Con}{Δ₁ : Con}
    {r₀ : Δ₀ ≡ Γ₀ , B₀}{r₁ : Δ₁ ≡ Γ₁ , B₁}
    {C₀ : Ty Δ₀}{C₁ : Ty Δ₁}
    {q₀ : C₀ ≡[ Ty= r₀ ]≡ A₀ [ wk ]T}{q₁ : C₁ ≡[ Ty= r₁ ]≡ A₁ [ wk ]T}
    {t'₀ : Tm Δ₀ C₀}{t'₁ : Tm Δ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= r₀ q₀ ]≡ vs t₀}{p₁ : t'₁ ≡[ Tm= r₁ q₁ ]≡ vs t₁}
    (Δ₂ : Δ₀ ≡ Δ₁)
    (C₂ : C₀ ≡[ Ty= Δ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Δ₂ C₂ ]≡ t'₁)
    (sucN₂ : sucN x₀ r₀ q₀ p₀ ≡[ Var= Δ₂ C₂ t'₂ ]≡ sucN x₁ r₁ q₁ p₁)
  → Γ₀ ≡ Γ₁
injsucN-Γ refl refl refl refl = refl
injsucN-A
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}
    {x₀ : Var Γ₀ A₀ t₀}{x₁ : Var Γ₁ A₁ t₁}
    {B₀ : Ty Γ₀}{B₁ : Ty Γ₁}
    {Δ₀ : Con}{Δ₁ : Con}
    {r₀ : Δ₀ ≡ Γ₀ , B₀}{r₁ : Δ₁ ≡ Γ₁ , B₁}
    {C₀ : Ty Δ₀}{C₁ : Ty Δ₁}
    {q₀ : C₀ ≡[ Ty= r₀ ]≡ A₀ [ wk ]T}{q₁ : C₁ ≡[ Ty= r₁ ]≡ A₁ [ wk ]T}
    {t'₀ : Tm Δ₀ C₀}{t'₁ : Tm Δ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= r₀ q₀ ]≡ vs t₀}{p₁ : t'₁ ≡[ Tm= r₁ q₁ ]≡ vs t₁}
    (Δ₂ : Δ₀ ≡ Δ₁)
    (C₂ : C₀ ≡[ Ty= Δ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Δ₂ C₂ ]≡ t'₁)
    (sucN₂ : sucN x₀ r₀ q₀ p₀ ≡[ Var= Δ₂ C₂ t'₂ ]≡ sucN x₁ r₁ q₁ p₁)
  → A₀ ≡[ Ty= (injsucN-Γ Δ₂ C₂ t'₂ sucN₂) ]≡ A₁
injsucN-A refl refl refl refl = refl
injsucN-t
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}
    {x₀ : Var Γ₀ A₀ t₀}{x₁ : Var Γ₁ A₁ t₁}
    {B₀ : Ty Γ₀}{B₁ : Ty Γ₁}
    {Δ₀ : Con}{Δ₁ : Con}
    {r₀ : Δ₀ ≡ Γ₀ , B₀}{r₁ : Δ₁ ≡ Γ₁ , B₁}
    {C₀ : Ty Δ₀}{C₁ : Ty Δ₁}
    {q₀ : C₀ ≡[ Ty= r₀ ]≡ A₀ [ wk ]T}{q₁ : C₁ ≡[ Ty= r₁ ]≡ A₁ [ wk ]T}
    {t'₀ : Tm Δ₀ C₀}{t'₁ : Tm Δ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= r₀ q₀ ]≡ vs t₀}{p₁ : t'₁ ≡[ Tm= r₁ q₁ ]≡ vs t₁}
    (Δ₂ : Δ₀ ≡ Δ₁)
    (C₂ : C₀ ≡[ Ty= Δ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Δ₂ C₂ ]≡ t'₁)
    (sucN₂ : sucN x₀ r₀ q₀ p₀ ≡[ Var= Δ₂ C₂ t'₂ ]≡ sucN x₁ r₁ q₁ p₁)
  → t₀ ≡[ Tm= (injsucN-Γ Δ₂ C₂ t'₂ sucN₂) (injsucN-A Δ₂ C₂ t'₂ sucN₂) ]≡ t₁
injsucN-t refl refl refl refl = refl
injsucN-x
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}
    {x₀ : Var Γ₀ A₀ t₀}{x₁ : Var Γ₁ A₁ t₁}
    {B₀ : Ty Γ₀}{B₁ : Ty Γ₁}
    {Δ₀ : Con}{Δ₁ : Con}
    {r₀ : Δ₀ ≡ Γ₀ , B₀}{r₁ : Δ₁ ≡ Γ₁ , B₁}
    {C₀ : Ty Δ₀}{C₁ : Ty Δ₁}
    {q₀ : C₀ ≡[ Ty= r₀ ]≡ A₀ [ wk ]T}{q₁ : C₁ ≡[ Ty= r₁ ]≡ A₁ [ wk ]T}
    {t'₀ : Tm Δ₀ C₀}{t'₁ : Tm Δ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= r₀ q₀ ]≡ vs t₀}{p₁ : t'₁ ≡[ Tm= r₁ q₁ ]≡ vs t₁}
    (Δ₂ : Δ₀ ≡ Δ₁)
    (C₂ : C₀ ≡[ Ty= Δ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Δ₂ C₂ ]≡ t'₁)
    (sucN₂ : sucN x₀ r₀ q₀ p₀ ≡[ Var= Δ₂ C₂ t'₂ ]≡ sucN x₁ r₁ q₁ p₁)
  → x₀ ≡[ Var= (injsucN-Γ Δ₂ C₂ t'₂ sucN₂) (injsucN-A Δ₂ C₂ t'₂ sucN₂) (injsucN-t Δ₂ C₂ t'₂ sucN₂) ]≡ x₁
injsucN-x refl refl refl refl = refl
injsucN-B
  : {Γ₀ : Con}{Γ₁ : Con}
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
    {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}
    {x₀ : Var Γ₀ A₀ t₀}{x₁ : Var Γ₁ A₁ t₁}
    {B₀ : Ty Γ₀}{B₁ : Ty Γ₁}
    {Δ₀ : Con}{Δ₁ : Con}
    {r₀ : Δ₀ ≡ Γ₀ , B₀}{r₁ : Δ₁ ≡ Γ₁ , B₁}
    {C₀ : Ty Δ₀}{C₁ : Ty Δ₁}
    {q₀ : C₀ ≡[ Ty= r₀ ]≡ A₀ [ wk ]T}{q₁ : C₁ ≡[ Ty= r₁ ]≡ A₁ [ wk ]T}
    {t'₀ : Tm Δ₀ C₀}{t'₁ : Tm Δ₁ C₁}
    {p₀ : t'₀ ≡[ Tm= r₀ q₀ ]≡ vs t₀}{p₁ : t'₁ ≡[ Tm= r₁ q₁ ]≡ vs t₁}
    (Δ₂ : Δ₀ ≡ Δ₁)
    (C₂ : C₀ ≡[ Ty= Δ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Δ₂ C₂ ]≡ t'₁)
    (sucN₂ : sucN x₀ r₀ q₀ p₀ ≡[ Var= Δ₂ C₂ t'₂ ]≡ sucN x₁ r₁ q₁ p₁)
  → B₀ ≡[ Ty= (injsucN-Γ Δ₂ C₂ t'₂ sucN₂) ]≡ B₁
injsucN-B refl refl refl refl = refl

disj-zero-suc
  : {Γ₀ : Con}{A₀ : Ty Γ₀}
    {Δ₀ : Con}{r₀ : Δ₀ ≡ Γ₀ , A₀}
    {C₀ : Ty Δ₀}{q₀ : C₀ ≡[ Ty= r₀ ]≡ A₀ [ wk ]T}
    {t'₀ : Tm Δ₀ C₀}{p₀ : t'₀ ≡[ Tm= r₀ q₀ ]≡ vz}
    {Γ₁ : Con}{A₁ : Ty Γ₁}{t₁ : Tm Γ₁ A₁}{x₁ : Var Γ₁ A₁ t₁}
    {B₁ : Ty Γ₁}
    {Δ₁ : Con}{r₁ : Δ₁ ≡ Γ₁ , B₁}
    {C₁ : Ty Δ₁}{q₁ : C₁ ≡[ Ty= r₁ ]≡ A₁ [ wk ]T}
    {t'₁ : Tm Δ₁ C₁}{p₁ : t'₁ ≡[ Tm= r₁ q₁ ]≡ vs t₁}
    (Δ₂ : Δ₀ ≡ Δ₁)
    (C₂ : C₀ ≡[ Ty= Δ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Δ₂ C₂ ]≡ t'₁)
  → zeroN r₀ q₀ p₀ ≡[ Var= Δ₂ C₂ t'₂ ]≡ sucN x₁ r₁ q₁ p₁
  → ⊥
disj-zero-suc refl refl refl ()
disj-suc-zero
  : {Γ₀ : Con}{A₀ : Ty Γ₀}
    {Δ₀ : Con}{r₀ : Δ₀ ≡ Γ₀ , A₀}
    {C₀ : Ty Δ₀}{q₀ : C₀ ≡[ Ty= r₀ ]≡ A₀ [ wk ]T}
    {t'₀ : Tm Δ₀ C₀}{p₀ : t'₀ ≡[ Tm= r₀ q₀ ]≡ vz}
    {Γ₁ : Con}{A₁ : Ty Γ₁}{t₁ : Tm Γ₁ A₁}{x₁ : Var Γ₁ A₁ t₁}
    {B₁ : Ty Γ₁}
    {Δ₁ : Con}{r₁ : Δ₁ ≡ Γ₁ , B₁}
    {C₁ : Ty Δ₁}{q₁ : C₁ ≡[ Ty= r₁ ]≡ A₁ [ wk ]T}
    {t'₁ : Tm Δ₁ C₁}{p₁ : t'₁ ≡[ Tm= r₁ q₁ ]≡ vs t₁}
    (Δ₂ : Δ₁ ≡ Δ₀)
    (C₂ : C₁ ≡[ Ty= Δ₂ ]≡ C₀)
    (t'₂ : t'₁ ≡[ Tm= Δ₂ C₂ ]≡ t'₀)
  → sucN x₁ r₁ q₁ p₁ ≡[ Var= Δ₂ C₂ t'₂ ]≡ zeroN r₀ q₀ p₀
  → ⊥
disj-suc-zero refl refl refl ()

disj-var-app
  : {Γ₀ : Con}{A₀ : Ty Γ₀}{t₀ : Tm Γ₀ A₀}
    {x₀ : Var Γ₀ A₀ t₀}
    {Γ₁ : Con}{A₁ : Ty Γ₁}{B₁ : Ty (Γ₁ , A₁)}
    {t₁ : Tm Γ₁ (Π A₁ B₁)}{n₁ : Ne Γ₁ (Π A₁ B₁) t₁}
    {u₁ : Tm Γ₁ A₁}{v₁ : Nf Γ₁ A₁ u₁}
    {C₁ : Ty Γ₁}{q₁ : C₁ ≡ (B₁ [ < u₁ > ]T)}
    {t'₁ : Tm Γ₁ C₁}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ (t₁ $ u₁)}
    (Δ₂ : Γ₀ ≡ Γ₁)
    (C₂ : A₀ ≡[ Ty= Δ₂ ]≡ C₁)
    (t'₂ : t₀ ≡[ Tm= Δ₂ C₂ ]≡ t'₁)
  →  var x₀ ≡[ Ne= Δ₂ C₂ t'₂ ]≡ appN n₁ v₁ q₁ p₁
  → ⊥
disj-var-app refl refl refl ()
disj-app-var
  : {Γ₀ : Con}{A₀ : Ty Γ₀}{t₀ : Tm Γ₀ A₀}
    {x₀ : Var Γ₀ A₀ t₀}
    {Γ₁ : Con}{A₁ : Ty Γ₁}{B₁ : Ty (Γ₁ , A₁)}
    {t₁ : Tm Γ₁ (Π A₁ B₁)}{n₁ : Ne Γ₁ (Π A₁ B₁) t₁}
    {u₁ : Tm Γ₁ A₁}{v₁ : Nf Γ₁ A₁ u₁}
    {C₁ : Ty Γ₁}{q₁ : C₁ ≡ (B₁ [ < u₁ > ]T)}
    {t'₁ : Tm Γ₁ C₁}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ (t₁ $ u₁)}
    (Δ₂ : Γ₁ ≡ Γ₀)
    (C₂ : C₁ ≡[ Ty= Δ₂ ]≡ A₀)
    (t'₂ : t'₁ ≡[ Tm= Δ₂ C₂ ]≡ t₀)
  → appN n₁ v₁ q₁ p₁  ≡[ Ne= Δ₂ C₂ t'₂ ]≡ var x₀
  → ⊥
disj-app-var refl refl refl ()

disj-neuU-neuEl
  : {Γ₀ : Con}{t₀ : Tm Γ₀ U}{n₀ : Ne Γ₀ U t₀}
    {Γ₁ : Con}{Â₁ : Tm Γ₁ U}{t₁ : Tm Γ₁ (El Â₁)}
    {n₁ : Ne Γ₁ (El Â₁) t₁}{C₁ : Ty Γ₁}{q₁ : C₁ ≡ El Â₁}
    {t'₁ : Tm Γ₁ C₁}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ t₁}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : U ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
  → neuU n₀ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ neuEl n₁ q₁ p₁
  → ⊥
disj-neuU-neuEl refl refl refl ()
disj-neuEl-neuU
  : {Γ₀ : Con}{t₀ : Tm Γ₀ U}{n₀ : Ne Γ₀ U t₀}
    {Γ₁ : Con}{Â₁ : Tm Γ₁ U}{t₁ : Tm Γ₁ (El Â₁)}
    {n₁ : Ne Γ₁ (El Â₁) t₁}{C₁ : Ty Γ₁}{q₁ : C₁ ≡ El Â₁}
    {t'₁ : Tm Γ₁ C₁}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ t₁}
    (Γ₂ : Γ₁ ≡ Γ₀)
    (C₂ : C₁ ≡[ Ty= Γ₂ ]≡ U)
    (t'₂ : t'₁ ≡[ Tm= Γ₂ C₂ ]≡ t₀)
  → neuEl n₁ q₁ p₁ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ neuU n₀
  → ⊥
disj-neuEl-neuU refl refl refl ()

disj-neuU-lamN
  : {Γ₀ : Con}{t₀ : Tm Γ₀ U}{n₀ : Ne Γ₀ U t₀}
    {Γ₁ : Con}{A₁ : Ty Γ₁}{B₁ : Ty (Γ₁ , A₁)}
    {t₁ : Tm (Γ₁ , A₁) B₁}{v₁ : Nf (Γ₁ , A₁) B₁ t₁}
    {C₁ : Ty Γ₁}{q₁ : C₁ ≡ Π A₁ B₁}
    {t'₁ : Tm Γ₁ C₁}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ lam t₁}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : U ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
  → neuU n₀ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ lamN v₁ q₁ p₁
  → ⊥
disj-neuU-lamN refl refl refl ()
disj-lamN-neuU
  : {Γ₀ : Con}{t₀ : Tm Γ₀ U}{n₀ : Ne Γ₀ U t₀}
    {Γ₁ : Con}{A₁ : Ty Γ₁}{B₁ : Ty (Γ₁ , A₁)}
    {t₁ : Tm (Γ₁ , A₁) B₁}{v₁ : Nf (Γ₁ , A₁) B₁ t₁}
    {C₁ : Ty Γ₁}{q₁ : C₁ ≡ Π A₁ B₁}
    {t'₁ : Tm Γ₁ C₁}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ lam t₁}
    (Γ₂ : Γ₁ ≡ Γ₀)
    (C₂ : C₁ ≡[ Ty= Γ₂ ]≡ U)
    (t'₂ : t'₁ ≡[ Tm= Γ₂ C₂ ]≡ t₀)
  → lamN v₁ q₁ p₁ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ neuU n₀
  → ⊥
disj-lamN-neuU refl refl refl ()

disj-neuEl-lamN
  : {Γ₀ : Con}{Â₀ : Tm Γ₀ U}{t₀ : Tm Γ₀ (El Â₀)}
    {n₀ : Ne Γ₀ (El Â₀) t₀}{C₀ : Ty Γ₀}{q₀ : C₀ ≡ El Â₀}
    {t'₀ : Tm Γ₀ C₀}{p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ t₀}
    {Γ₁ : Con}{A₁ : Ty Γ₁}{B₁ : Ty (Γ₁ , A₁)}
    {t₁ : Tm (Γ₁ , A₁) B₁}{v₁ : Nf (Γ₁ , A₁) B₁ t₁}
    {C₁ : Ty Γ₁}{q₁ : C₁ ≡ Π A₁ B₁}
    {t'₁ : Tm Γ₁ C₁}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ lam t₁}
    (Γ₂ : Γ₀ ≡ Γ₁)
    (C₂ : C₀ ≡[ Ty= Γ₂ ]≡ C₁)
    (t'₂ : t'₀ ≡[ Tm= Γ₂ C₂ ]≡ t'₁)
  → neuEl n₀ q₀ p₀ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ lamN v₁ q₁ p₁
  → ⊥
disj-neuEl-lamN refl refl refl ()
disj-lamN-neuEl
  : {Γ₀ : Con}{Â₀ : Tm Γ₀ U}{t₀ : Tm Γ₀ (El Â₀)}
    {n₀ : Ne Γ₀ (El Â₀) t₀}{C₀ : Ty Γ₀}{q₀ : C₀ ≡ El Â₀}
    {t'₀ : Tm Γ₀ C₀}{p₀ : t'₀ ≡[ Tm= refl q₀ ]≡ t₀}
    {Γ₁ : Con}{A₁ : Ty Γ₁}{B₁ : Ty (Γ₁ , A₁)}
    {t₁ : Tm (Γ₁ , A₁) B₁}{v₁ : Nf (Γ₁ , A₁) B₁ t₁}
    {C₁ : Ty Γ₁}{q₁ : C₁ ≡ Π A₁ B₁}
    {t'₁ : Tm Γ₁ C₁}{p₁ : t'₁ ≡[ Tm= refl q₁ ]≡ lam t₁}
    (Γ₂ : Γ₁ ≡ Γ₀)
    (C₂ : C₁ ≡[ Ty= Γ₂ ]≡ C₀)
    (t'₂ : t'₁ ≡[ Tm= Γ₂ C₂ ]≡ t'₀)
  → lamN v₁ q₁ p₁ ≡[ Nf= Γ₂ C₂ t'₂ ]≡ neuEl n₀ q₀ p₀
  → ⊥
disj-lamN-neuEl refl refl refl ()
