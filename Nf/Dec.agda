module Nf.Dec where

open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.ConElim
open import Nf.Nf
open import NormTy.Inj

data Dec {i}(A : Set i) : Set i where
  yes : A → Dec A
  no  : (A → ⊥) → Dec A

decVar : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
         {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}
         (x₀ : Var Γ₀ A₀ t₀)(x₁ : Var Γ₁ A₁ t₁)
       → Dec (Σ (A₀ ≡[ Ty= Γ₂ ]≡ A₁) λ A₂ →
              Σ (t₀ ≡[ Tm= Γ₂ A₂ ]≡ t₁) λ t₂ →
                (x₀ ≡[ Var= Γ₂ A₂ t₂ ]≡ x₁))

decNe  : {Γ : Con}
         {A₀ A₁ : Ty Γ}
         {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}
         (tN₀ : Ne Γ A₀ t₀)(tN₁ : Ne Γ A₁ t₁)
       → Dec (Σ (A₀ ≡ A₁) λ A₂ →
              Σ (t₀ ≡[ Tm= refl A₂ ]≡ t₁) λ t₂ →
                (tN₀ ≡[ Ne= refl A₂ t₂ ]≡ tN₁))

decNf  : {Γ : Con}
         {A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
         {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}
         (v₀ : Nf Γ A₀ t₀)(v₁ : Nf Γ A₁ t₁)
       → Dec (Σ (t₀ ≡[ Tm= refl A₂ ]≡ t₁) λ t₂ →
                (v₀ ≡[ Nf= refl A₂ t₂ ]≡ v₁))

decVar Δ₂ (zeroN refl refl refl) (zeroN refl refl refl) with inj,₀ Δ₂ | inj,₁ Δ₂
decVar refl (zeroN refl refl refl) (zeroN refl refl refl) | refl | refl = yes (refl ,Σ (refl ,Σ refl))
decVar Δ₂ (zeroN r q p) (sucN x₁ r₁ q₁ p₁) = no (λ { (C₂ ,Σ (t'₂ ,Σ p)) → disj-zero-suc Δ₂ C₂ t'₂ p })
decVar Δ₂ (sucN x₀ r q p) (zeroN r₁ q₁ p₁) = no (λ { (C₂ ,Σ (t'₂ ,Σ p)) → disj-suc-zero Δ₂ C₂ t'₂ p })
decVar Δ₂ (sucN x₀ refl refl refl) (sucN x₁ refl refl refl) with inj,₀ Δ₂ | inj,₁ Δ₂
decVar refl (sucN x₀ refl refl refl) (sucN x₁ refl refl refl) | refl | refl with decVar refl x₀ x₁
decVar refl (sucN x refl refl refl) (sucN .x refl refl refl) | refl | refl | yes (refl ,Σ (refl ,Σ refl))
  = yes (refl ,Σ (refl ,Σ refl))
decVar refl (sucN x₀ refl refl refl) (sucN x₁ refl refl refl) | refl | refl | no q
  = no (λ { (C₂ ,Σ (t'₂ ,Σ sucN₂))
        → q (UIP'' (Ty= (injsucN-Γ refl C₂ t'₂ sucN₂)) (injsucN-A refl C₂ t'₂ sucN₂)
         ,Σ (UIP' (Tm= (injsucN-Γ refl C₂ t'₂ sucN₂) (injsucN-A refl C₂ t'₂ sucN₂))
                  (Tm= refl (UIP'' (Ty= (injsucN-Γ refl C₂ t'₂ sucN₂)) (injsucN-A refl C₂ t'₂ sucN₂)))
                  (injsucN-t refl C₂ t'₂ sucN₂)
         ,Σ UIP' (Var= (injsucN-Γ refl C₂ t'₂ sucN₂) (injsucN-A refl C₂ t'₂ sucN₂) (injsucN-t refl C₂ t'₂ sucN₂))
                 (Var= refl (UIP'' (Ty= (injsucN-Γ refl C₂ t'₂ sucN₂)) (injsucN-A refl C₂ t'₂ sucN₂)) (UIP' (Tm= (injsucN-Γ refl C₂ t'₂ sucN₂) (injsucN-A refl C₂ t'₂ sucN₂)) (Tm= refl (UIP'' (Ty= (injsucN-Γ refl C₂ t'₂ sucN₂)) (injsucN-A refl C₂ t'₂ sucN₂))) (injsucN-t refl C₂ t'₂ sucN₂)))
                 (injsucN-x refl C₂ t'₂ sucN₂))) })

decNe (var x₀) (var x₁) with decVar refl x₀ x₁
decNe (var x₀) (var x₁) | yes (A₂ ,Σ (t'₂ ,Σ x₂)) = yes (A₂ ,Σ (t'₂ ,Σ var= refl A₂ t'₂ x₂))
decNe (var x₀) (var x₁) | no q = no λ { (A₂ ,Σ (t'₂ ,Σ x₂)) → q (A₂ ,Σ (t'₂ ,Σ injvar refl A₂ t'₂ x₂)) }
decNe (var x₀) (appN n₁ v₁ q₁ p₁) = no (λ { (C₂ ,Σ (t'₂ ,Σ p)) → disj-var-app refl C₂ t'₂ p })
decNe (appN n₀ v₀ q₀ p₀) (var x) = no (λ { (C₂ ,Σ (t'₂ ,Σ p)) → disj-app-var refl C₂ t'₂ p })
decNe (appN n₀ v₀ q₀ p₀) (appN n₁ v₁ q₁ p₁) with decNe n₀ n₁
decNe (appN n₀ v₀ q₀ p₀) (appN .(coe (Ne= refl ΠAB₂ refl) n₀) v₁ q₁ p₁) | yes (ΠAB₂ ,Σ (refl ,Σ refl)) with injΠ₁ refl ΠAB₂
decNe (appN n₀ v₀ q₀ p₀) (appN .(coe (Ne= refl ΠAB₂ refl) n₀) v₁ q₁ p₁) | yes (ΠAB₂ ,Σ (refl ,Σ refl)) | refl with injΠ₂' refl (to≃ ΠAB₂)
decNe (appN n₀ v₀ q₀ p₀) (appN .(coe (Ne= refl refl refl) n₀) v₁ q₁ p₁) | yes (refl ,Σ (refl ,Σ refl)) | refl | refl ,≃ refl with decNf refl v₀ v₁
decNe (appN n₀ v₀ refl refl) (appN .(coe (Ne= refl refl refl) n₀) .v₀ refl refl) | yes (refl ,Σ (refl ,Σ refl)) | refl | refl ,≃ refl | yes (refl ,Σ refl) = yes (refl ,Σ (refl ,Σ refl))
decNe (appN n₀ v₀ q₀ p₀) (appN .(coe (Ne= refl refl refl) n₀) v₁ q₁ p₁) | yes (refl ,Σ (refl ,Σ refl)) | refl | refl ,≃ refl | no q
  = no λ { (A₂ ,Σ (t'₂ ,Σ appN₂))
       → q (UIP'' (Tm= refl (injappN-A refl A₂ t'₂ appN₂)) (injappN-u refl A₂ t'₂ appN₂)
        ,Σ UIP' (Nf= refl (injappN-A refl A₂ t'₂ appN₂) (injappN-u refl A₂ t'₂ appN₂))
                (Nf= refl refl (UIP'' (Tm= refl (injappN-A refl A₂ t'₂ appN₂)) (injappN-u refl A₂ t'₂ appN₂)))
                (injappN-uN refl A₂ t'₂ appN₂)) }
decNe (appN n₀ v₀ q₀ p₀) (appN n₁ v₁ q₁ p₁) | no q
  = no (λ { (A₂ ,Σ (t'₂ ,Σ appN₂))
        → q (Π= refl (injappN-A refl A₂ t'₂ appN₂) (injappN-B refl A₂ t'₂ appN₂)
         ,Σ (injappN-t refl A₂ t'₂ appN₂
         ,Σ injappN-tN refl A₂ t'₂ appN₂)) })

decNf A₂ (neuU n₀) (neuU n₁) with A₂
decNf _ (neuU n₀) (neuU n₁) | refl with decNe n₀ n₁
decNf _ (neuU n₀) (neuU .n₀) | refl | yes (refl ,Σ (refl ,Σ refl)) = yes (refl ,Σ refl)
decNf _ (neuU n₀) (neuU n₁) | refl | no q = no λ { (t'₂ ,Σ neuU₂) → q (refl ,Σ (t'₂ ,Σ injneuU refl refl t'₂ neuU₂)) }
decNf C₂ (neuU n₀) (neuEl n₁ q₁ p₁) = no λ { (t'₂ ,Σ p₂) → disj-neuU-neuEl refl C₂ t'₂ p₂ }
decNf C₂ (neuU n₀) (lamN v₁ q₁ p₁) = no λ { (t'₂ ,Σ p₂) → disj-neuU-lamN refl C₂ t'₂ p₂ }
decNf C₂ (neuEl n₀ q₀ p₀) (neuU n₁) = no λ { (t'₂ ,Σ p₂) → disj-neuEl-neuU refl C₂ t'₂ p₂ }
decNf A₂ (neuEl {_}{Â₀}{t₀} n₀ {C₀} q₀ {t'₀} p₀) (neuEl {_}{Â₁}{t₁} n₁ {C₁} q₁ {t'₁} p₁) with A₂
decNf _ (neuEl {_}{Â₀}{t₀} n₀ {C} q₀ {t'₀} p₀) (neuEl {_}{Â₁}{t₁} n₁ {.C} q₁ {t'₁} p₁) | refl with decNe n₀ n₁
decNf _ (neuEl {_}{Â₀}{t₀} n₀ {C} q₀ {t'₀} p₀) (neuEl {_}{Â₁}{t₁} n₁ {.C} q₁ {t'₁} p₁) | refl | yes (ElÂ₂ ,Σ (t₂ ,Σ n₂))
  = yes (from≃ (from≡ (Tm= refl q₀) p₀ ◾̃ from≡ (Tm= refl ElÂ₂) t₂ ◾̃ from≡ (Tm= refl q₁) p₁ ⁻¹̃)
     ,Σ neuEl= refl
               (injEl refl ElÂ₂)
               (UIP' (Tm= refl ElÂ₂) (Tm= refl (El= refl (injEl refl ElÂ₂))) t₂)
               (UIP' (Ne= refl ElÂ₂ t₂) (Ne= refl (El= refl (injEl refl ElÂ₂)) (UIP' (Tm= refl ElÂ₂) (Tm= refl (El= refl (injEl refl ElÂ₂))) t₂)) n₂)
               refl
               (from≃ (from≡ (Tm= refl q₀) p₀ ◾̃ from≡ (Tm= refl ElÂ₂) t₂ ◾̃ from≡ (Tm= refl q₁) p₁ ⁻¹̃)))
decNf _ (neuEl {_}{Â₀}{t₀} n₀ {C} q₀ {t'₀} p₀) (neuEl {_}{Â₁}{t₁} n₁ {.C} q₁ {t'₁} p₁) | refl | no q
  = no λ { (t'₂ ,Σ neuEl₂)
       → q (El= refl (injneuEl-Â refl refl t'₂ neuEl₂)
        ,Σ (injneuEl-t refl refl t'₂ neuEl₂
        ,Σ injneuEl-n refl refl t'₂ neuEl₂)) }
decNf C₂ (neuEl n₀ q₀ p₀) (lamN v₁ q₁ p₁) = no λ { (t'₂ ,Σ p₂) → disj-neuEl-lamN refl C₂ t'₂ p₂ }
decNf C₂ (lamN v₀ q₀ p₀) (neuU n₁) = no λ { (t'₂ ,Σ p₂) → disj-lamN-neuU refl C₂ t'₂ p₂ }
decNf C₂ (lamN v₀ q₀ p₀) (neuEl n₁ q₁ p₁) = no λ { (t'₂ ,Σ p₂) → disj-lamN-neuEl refl C₂ t'₂ p₂ }
decNf A₂ (lamN v₀ q₀ p₀) (lamN v₁ q₁ p₁) with A₂
decNf _ (lamN v₀ refl refl) (lamN v₁ q₁ p₁) | refl with injΠ₁ refl q₁ | injΠ₂' refl (to≃ q₁)
decNf _ (lamN v₀ refl refl) (lamN v₁ refl refl) | refl | refl | refl ,≃ refl with decNf refl v₀ v₁
decNf A₂ (lamN v₀ refl refl) (lamN .v₀ refl refl) | refl | refl | refl ,≃ refl | yes (refl ,Σ refl)
  = yes (refl ,Σ refl)
decNf A₂ (lamN v₀ refl refl) (lamN v₁ refl refl) | refl | refl | refl ,≃ refl | no q
  = no λ { (lam₂ ,Σ lamN₂)
       → q (UIP'' (Tm= (,C= refl (injlamN-A refl refl lam₂ lamN₂)) (injlamN-B refl refl lam₂ lamN₂))
                  (injlamN-t refl refl lam₂ lamN₂)
        ,Σ UIP' (Nf= (,C= refl (injlamN-A refl refl lam₂ lamN₂)) (injlamN-B refl refl lam₂ lamN₂) (injlamN-t refl refl lam₂ lamN₂))
                (Nf= refl refl (UIP'' (Tm= (,C= refl (injlamN-A refl refl lam₂ lamN₂)) (injlamN-B refl refl lam₂ lamN₂)) (injlamN-t refl refl lam₂ lamN₂)))
                (injlamN-v refl refl lam₂ lamN₂)) }
