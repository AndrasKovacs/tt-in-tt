open import Cats

module experiments.PShFamPSh (C : Cat)(Γ : PSh C) where

open import lib

from : (Δ : PSh C) → Δ →n Γ → FamPSh Γ
from Δ σ = record
  { _$F_   = λ {I} α → Σ (Δ $P I) λ α' → σ $n α' ≡ α
  ; _$F_$_ = λ { {I}{J} f {α}(α' ,Σ r) → (Δ $P f $ α') ,Σ (natn σ {I}{J}{f}{α'} ⁻¹ ◾ ap (Γ $P f $_) r) }
  ; idF    = λ { {I}{α}{α' ,Σ p} → {!!} }
  ; compF  = {!!}
  }

to : FamPSh Γ → Σ (PSh C) λ Δ → Δ →n Γ
to A = record
  { _$P_   = λ I → Σ (Γ $P I) λ α → A $F α
  ; _$P_$_ = λ { {I}{J} f (α ,Σ a) → (Γ $P f $ α) ,Σ A $F f $ a}
  ; idP    = {!!}
  ; compP  = {!!}
  }

  ,Σ record
  { _$n_ = λ { {I}(α ,Σ a) → α }
  ; natn = λ { {I}{J}{f}{α ,Σ a} → refl }
  }
