module Term.Term where

-- the term model

open import lib

open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.Func

open import TT.Syntax

d : Decl
d = record { Con = Con ; Ty = Ty ; Tms = Tms ; Tm = Tm }

c : Core d
c = record
  { •     = •
  ; _,_   = _,_
  ; _[_]T = _[_]T
  ; id    = id
  ; _∘_   = _∘_
  ; ε     = ε
  ; _,s_  = _,s_
  ; π₁    = π₁
  ; _[_]t = _[_]t
  ; π₂    = π₂
  ; [id]T = [id]T
  ; [][]T = [][]T
  ; idl   = idl
  ; idr   = idr
  ; ass   = ass
  ; ,∘    = ,∘
  ; π₁β   = π₁β
  ; πη    = πη
  ; εη    = εη
  ; π₂β   = π₂β
  }

b : Base c
b = record { U = U ; U[] = U[] ; El = El ; El[] = El[] }

f : Func c
f = record
  { Π     = Π
  ; Π[]   = Π[]
  ; lam   = lam
  ; app   = app
  ; lam[] = lam[]
  ; Πβ    = Πβ
  ; Πη    = Πη
  }
