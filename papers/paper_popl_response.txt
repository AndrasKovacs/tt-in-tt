Two reviewers are concerned about the fact that the theory of QITs is not fully developed in our work. The same is true for the more general Higher Inductive Types for which however progress has been made recently by Coquand's cubical set model. We believe that a simpler approach based on the setoid model would solve this problem for QITs. However, our paper only aims to present a promising application for QITs. As the second reviewer points out: "it is not entirely clear what the semantic and computational status of inductive-inductive HITs might be. Should therefore everyone stop using them until someone puts all the doubts at rest? Well, people who use them run a certain risk of being embarrassed in the future, but I heard a lot of stories about physicists being ahead of mathematicians."

Answers for the first review (157A):

* In addition to the philosophical concerns, we chose to formalise well-typed terms as they have a more direct connection to the semantics, in particular there is no need to prove a coherence theorem stating that the semantics of a term is independent of the derivation.

* Our starting point was not Categories with Families but pure syntax (with explicit substitutions and De Bruijn indices) and we only observed afterwards that the motives and methods (algebras) for the eliminator of the inductive-inductive type defining the syntax corresponds to Categories with Families.

* We are aware of the proposal to model QITs by mutual quotient types and inductive types, we will add this to the paper. However, this poses the problem to model this extension of inductive-inductive definitions.

* It is plausible that the horror of using eliminators for QITs instead of pattern matching can be avoided soon as shown in the recent development of the cubical proof assistent by Coquand and his group: here higher constructors give additional cases for pattern matching.

* We will add a comment on the consistency of Agda extended by our QIT definitions. As we only use the eliminator for these types this is not a problem, however ensuring that pattern matching is impossible by hiding the constructors was beyond the scope of our paper.

* We will add citations to quotient types in the Related work section.

Answers for the second review (157B):

* The only issue raised by the review was that the "standard" interpretation cannot be defined directly in HoTT. We defined an approximation of this by an inductive-recursive universe and we sketched a similar construction to the one suggested by the reviewer in section 6: Nicolai Kraus has a suggestion by eliminating first into an intermediate definition which states all the necessary coherence equations, and mapping to the universe from there.

Answers for the third review (157C):

* We acknowledge the the presentation could be improved especially for non-experts.

* The computational content of the path constructors seem to be addressed by the recent work on cubical type theory.

* An example of using metaprogramming as [11] is section 5: here, by using reflection, parametricity theorems could be derived automatically by the system.