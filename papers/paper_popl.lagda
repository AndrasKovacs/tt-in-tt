\documentclass{sigplanconf}

%include lhs2TeX.fmt
%include agda.fmt
%include lib.fmt

\usepackage{amsmath}
\usepackage{proof}
\usepackage{stmaryrd}
%\usepackage{sfmath}
%\usepackage[\sfdefault]{mathastext}
%\renewcommand{\cite}[1]{[#1]}
\usepackage{flushend}

\usepackage[firstpage]{draftwatermark}
\SetWatermarkText{\hspace*{8in}\raisebox{4.6in}{\includegraphics[scale=0.1]{aec-badge-popl}}}
\SetWatermarkAngle{0}

\begin{document}
\toappear{}
%\special{papersize=8.5in,11in}
\setlength{\pdfpageheight}{\paperheight}
\setlength{\pdfpagewidth}{\paperwidth}

\conferenceinfo{CONF 'yy}{Month d--d, 20yy, City, ST, Country} 
\copyrightyear{20yy} 
\copyrightdata{978-1-nnnn-nnnn-n/yy/mm} 
\doi{nnnnnnn.nnnnnnn}

% Uncomment one of the following two, if you are not going for the 
% traditional copyright transfer agreement.

%\exclusivelicense                % ACM gets exclusive license to publish, 
                                  % you retain copyright

%\permissiontopublish             % ACM gets nonexclusive license to publish
                                  % (paid open-access papers, 
                                  % short abstracts)

\titlebanner{banner above paper title}        % These are ignored unless
\preprintfooter{short description of paper}   % 'preprint' option specified.

\title{Type Theory in Type Theory using Quotient Inductive Types
\thanks{Supported by EPSRC grant EP/M016951/1.}
}
%\subtitle{Subtitle Text, if any}

%\authorinfo{}{}{}
\authorinfo{Thorsten Altenkirch \and Ambrus Kaposi}
          {School for Computer Science, University of Nottingham, United Kingdom}
          {\{txa,auk\}\verb$@$cs.nott.ac.uk}

\maketitle

\begin{abstract}
  We present an internal formalisation of a type theory with dependent types in
  Type Theory using a special case of higher inductive types from
  Homotopy Type Theory which we
  call quotient inductive types (QITs). Our formalisation of type
  theory avoids referring to preterms or a typability relation but
  defines directly well typed objects by an inductive definition. We
  use the elimination principle to define the set-theoretic and
  logical predicate interpretation. The work has been formalized using
  the Agda system extended with QITs using postulates.
\end{abstract}

\category{D.3.1}{Formal Definitions and Theory}{}
\category{F.4.1}{Mathematical Logic}{Lambda calculus and related systems}


% general terms are not compulsory anymore, 
% you may leave them out
%\terms
%term1, term2

\keywords
Higher Inductive Types, Homotopy Type Theory, Logical Relations, Metaprogramming, Agda

\section{Introduction}

We would like to reflect the syntax and typing rules of Type Theory in
itself. This offers exciting opportunities for typed metaprogramming
to support interactive theorem proving. We can also implement
extensions of Type Theory by providing a sound interpretation giving
rise to a form of template Type Theory. This paper describes a novel
approach to achieve this ambition.

Within Type Theory it is straightforward to represent the simply typed
$\lambda$-calculus as an inductive type where contexts and types are
defined as inductive types and terms are given as an inductively
defined family indexed by contexts and types (see figure \ref{fig:stl}
for a definition in idealized Agda).
\begin{figure}[h]
\centering
\begin{spec}
data Ty   : Set where
  ι       : Ty
  _⇒_     : Ty → Ty → Ty

data Con  : Set where
  •       : Con
  _,_     : Con → Ty → Con

data Var  : Con → Ty → Set where
  zero    : Var (Γ , σ) σ
  suc     : Var Γ σ → Var (Γ , τ) σ  

data Tm   :  Con → Ty → Set where
  var     : Var Γ σ → Tm Γ σ
  _@_     : Tm Γ (σ ⇒ τ)
            → Tm Γ σ     → Tm Γ τ 
  Λ       : Tm (Γ , σ) τ → Tm Γ (σ ⇒ τ)
\end{spec}
\caption{Simply typed $\lambda$-calculus}
\label{fig:stl}
\end{figure}

Here we inductively define Types (|Ty|) and contexts (|Con|) which in
a de Bruijn setting are just sequences of types. We define the
families of variables and terms where variables are \emph{typed de
  Bruijn indices} and terms are inductively generated from variables
using application (|_@_|) and abstraction (|Λ|). 
\footnote{We are oversimplifying things a bit here: we would really
  like to restrict operations to those which preserve
  $\beta\eta$-equality, i.e. work with a quotient of |Tm|.}

In this approach we never define preterms and a typing relation but
directly present the typed syntax. This has technical advantages: in
typed metaprogramming we only want to deal with typed syntax and it
avoids the need to prove subject reduction theorems separately. But
more importantly this approach reflects our type-theoretic philosophy
that typed objects are first and preterms are designed at a 2nd step
with the intention that they should contain enough information so that
we can reconstruct the typed objects. Typechecking can be expressed in
this view as constructing a partial inverse to the forgetful function
which maps typed into untyped syntax (a sort of printing operation).

Naturally, we would like to do the same for the language we are
working in, that is we would like to perform typed metaprogramming for
a dependently typed language. There are at least two complications:
\begin{enumerate}
\item[(1)]  types, terms and contexts have to be defined mutually but also
depend on each other, 
\item[(2)] due to the conversion rule which allows us to coerce terms
  along type-equality we have to define the equality mutually with the
  syntactic typing rules:
  \begin{equation*}
    \infer{|Γ ⊢ t : B|}{|Γ ⊢ A = B| && |Γ ⊢ t : A|}
  \end{equation*}
\end{enumerate}
(1) can be addressed by using inductive-inductive definitions
\cite{alti:catind2} (see section \ref{sec:induct-induct-types}) --
indeed doing Type Theory in Type Theory was one of the main
motivations behind introducing inductive-inductive types. It seemed
that this would also suffice to address (2), since we can define the
conversion relation mutually with the rest of the syntax. However, it
turns out that the overhead in form of type-theoretic boilerplate is
considerable. For example terms depend on both contexts and types, we
have to include special constructors which formalize the idea that
this is a setoid indexed over two given setoids. Moreover each
constructor comes with a congruence rule. In the end the resulting
definition becomes unfeasible, almost impossible to write down
explicitly but even harder to work with in practice.

This is where Higher Inductive Types \cite{book} come in, because they allow us to
define constructors for equalities at the same time as we define
elements. Hence we can present the conversion relation just by stating
the equality rules as equality constructors. Since we are defining
equalities we don't have to formalize any indexed setoid structure or
explicitly state congruence rules. This second step turns out to be
essential to have a workable internal definition of Type Theory.

Indeed, we only use a rather simple special case of higher inductive
types, namely we are only interested in first order equalities and we
are ignoring higher equalities. This is what we call Quotient
Inductive Types (QITs). From the perspective of Homotopy Type Theory QITs are HITs which
are truncated to be sets. The main aspect of HITs we are using is that
they allow us to introduce new equalities and constructors at the same
time. This has already been exploited in a different way in
\cite{book} for the definition of the reals and the constructible
hierarchy without having to use the axiom of choice.

\subsection{Overview of the Paper}

We start by explaining in some detail the type theory we are using as
a metatheory and how we implement it using Agda in section
\ref{sec:meta}. In particular we are reviewing inductive-inductive
types (section \ref{sec:induct-induct-types}) and we motivate the
  importance of QITs as simple HITs (section \ref{sec:qit}). Then, in section
\ref{sec:syntax} we turn to our main goal and define the syntax of a
basic type theory with only $\Pi$-types and an uninterpreted universe
with explicit substitutions. We explain how a general recursor and
eliminator can be derived. As a warm-up we define formally the
\emph{standard interpretation} which interprets every piece of syntax
by the corresponding metatheoretic construction in section
\ref{sec:standard}. Our main real-world example (section
\ref{sec:extern-param}) is to formally give the logical predicate
translation which has been introduced by Bernardy \cite{bernardy} to
explain parametricity for dependent types. The constructions presented
in this paper have been formalized in Agda, this is available as
supplementary material \cite{suppl}.

% However, doing the same thing for a theory involving dependent types remains a hard problem, however a very natural one because we would like to reflect the theory we are working in. As James Chapman expressed it \emph{Type Theory should eat itself} \cite{james}. The first problem we encounter is that we inductively define a type (e.g. contexts) and another type depending on this one (e.g. terms). This situation has been investigated under the name \emph{inductive-inductive} types \cite{forsberg-setzer,forsberg-setzer-altenkirch,forsberg-phd} and while the final word on this topic hasn't been spoken, we do know how to deal with this. And, indeed, the Agda system, not scared by semantic considerations, is very happy to accept such definitions. However, this is not enough! Since all syntactic types in a dependently typed theory are defined together with their conversion relation, we really are defining familes of setoids indexed by setoids. This was indeed the main idea behind Chapman's work. This is not an issue for simple types because there is no dependnecy on a type which should be viewed as a setoid, indeed only terms depend on contexts and types and those are given syntactically. While the setoid approach works in principle it is burdened with a heavy beaurocratic overhed which makes it unusuable in practice.  Here we suggest a new approach which uses a concept from Homotopy Type Theory \cite{book}: Higher Inductive Types. Basically, higher inductve types allow us to introduce constructors for equalities at the same time we introduce constructors for elements. This is \emph{higher} becuase we introduce elements in higher dimensions from the view point of the homotopy interpretation. This is extremely useful for our problem, since we can introduce conversion rules as constructors for equalities. The overhead of having to formalize families of setoids disappears because we are actually defining equality not an arbitrary equivalence relation. And indeed the cogruence rules come for free.

% Some caveats are in place. The precise format of higer inductive types hasn't been fixed yet. And indeed, we are very liberal in combining higher inductive types and inductive-inductive definitions, both of them are under development and the properties of their combination isn't very well explored - e.g. see the work by Shulman \cite{shulman}. Also our higher inductive types aren't very high because we still want that our syntax is given by sets in the sense of HoTT, that is their equalities are propositional, i.e. allow at most one proof. Another serious issue is that the computationa interpretation of HoTT and HITs in particular hasn't yet worked out, which means that we cannot reduce all of our programs to canonical forms. Howver, we are hopeful that progress can be made on this front \cite{thierry, ourselves}/

\subsection{Related Work}
\label{sec:related-work}

Our goal is to give a faithful exposition of the typed syntax of Type
Theory in a first order setting in the sense that our syntax is
finitary. Hence what we are doing is different from e.g.
\cite{Palsberg} which defines a higher order syntax for reflection. We
also diverge from \cite{conor} which exploits the metatheoretic
equality to make reflection feasible because we would like to have the
freedom to interpret equality in any way we would like.  We also note
the work \cite{devriese} which provides a very good motivation for our
work but treats definitional equality in a non-standard way. Indeed,
we want to express the syntax of Type Theory in a natural way without
any special assumptions.

The work by Chapman \cite{james} and also the earlier work by
Danielsson \cite{nisse} have a motivation very similar to
ours. \cite{nisse} uses implicit substitutions and this seems rather
difficult to use in general, his definitions have rather adhoc
character. \cite{james} is more principled but relies on setoids and 
has to add a lot of boilerplate code to characterize families of
setoids over a setoid explicitly. This boilerplate makes the
definition in the end unusable in practice.


A nice application of type-theoretic metaprogramming is developed in
\cite{forcing} where the authors present a mechanism to safely extend
Coq with new principles. This relies on presenting a proof-irrelevant
presheaf model and then proving constants in the presheaf
interpretation. Our approach is in some sense complementary in that we
provide a safe translation from well typed syntax into a model, but
also more general because we are not limited to any particular class
of models.

Our definition of the internal syntax of Type Theory is very much
inspired by categories with families (CwFs)
\cite{hofmann1997syntax,dybjer1996internal}. Indeed, one can summarize our work by saying
that we construct an initial CwF using QITs. That something like this
should be possible in principle was clear since a while, however, the
progress reported here is that we have actually done it. 

The style of the presentation can also be described as a generalized
algebraic theory \cite{gat} which has been recently used by Coquand to
give very concise presentations of Type Theory \cite{cubical}. Our
work shows that it should be possible to internalize this style of
presentation in type theory itself.

Higher Inductive Types are described in chapter 6 in \cite{book}, and
it is shown that they generalize quotient types, e.g. see
\cite{men:90,HofmannPhD}.

% First of all we are looking for a concrete representation of typed
% terms, that is we are not going to use higher order abstract syntax as
% for example in \cite{palsberg}. 

% All these approaches fail at some point to achieve the goal to
% resemble the simplicity of the definition for simply typed lambda
% calculus. While the use of higher order abstract syntax is interesting
% it requires a particular infrastructure which is not available and
% does not appear to be compatible with dependnetly typed languages like
% Agda. Other approaches (Conor, Devriese) assume that conversion is
% given by the metalanguage thereby restricting the number of possible
% functions we may want to write. The work by Chapman and Danielsson is
% closest to our goal but suffers from a complexity, indeed an
% overcharge of boilerplate, which makes it hard to use their approaches
% in practice.

\section{The Type Theory We Live In}
\label{sec:meta}

We are working in a Martin-L{\"o}f Type Theory using Agda as a
vehicle. That is our syntax is the one used by the functional
programming language and interactive proof assistant Agda
\cite{agda,agdawiki}. In the paper, to improve readability we omit
implicitly quantified variables whose types can be inferred from the
context (in this respect we follow rather Idris \cite{idris}).

In Agda, $\Pi$-types are written as |(x:A) → B| for $\Pi(x:A).B$,
implicit arguments are indicated by curly brackets |{x:A} → B|, these
can be omitted if Agda can infer them from the types of later
arguments. Agda supports mixfix notation, eg. function space can be
denoted |_⇒_| where the underscores show the placement of
arguments. Underscores can also be used when defining a function if we
are not interested in the value of the argument eg. the constant
function can be defined as |const x _ = x|. The keyword |data| is used
to define inductively generated families and the keyword |record| is
for defining dependent record types (iterated $\Sigma$-types) by
listing their fields after the keyword |field|. Records are
automatically modules (separate name spaces) in Agda, this is why they
can be opened using the |open| keyword. In this case the field names
become projection functions. Just as inductive types can have
parameters, records and modules can also be parameterised (by a
telescope of types), we use this feature of Agda to structure our
code. Agda allows overloading of constructor names, we use this
e.g. when using the same constructor |_,_| for context extension and
substitution extension. Equality (or the identity type) is denoted by
|_≡_| and has the constructor |refl|. We use the syntax |a ≡[ p ]≡ b|
to express that two objects in different types |a:A| and |b:B| are
equal via an equality of their types |p : A ≡ B|, see also section
\ref{sec:syntax}.

To support this flexible syntax Agda diverges from most programming
languages in that space matters. E.g. |⟦Γ⟧| is just a variable name
but |⟦ Γ ⟧| is the application of the operation |⟦_⟧| to |Γ|.

We use QITs which are not available in Agda, however we can simulate
them by a technique pioneered by \cite{licata} for HITs. While QITs
are a special case of HITs and are inspired by Homotopy Type Theory (HoTT),
for most of the paper we shall work with a type theory with a strict
equality, i.e. we assume that all equality proofs are equal. We will
get back to this issue and explain how our work relates to Homotopy
Type Theory in section \ref{sec:hott}.

However,  we do assume functional extensionality which follows in any
case from the presence of QITs. The theory poses a canonicity problem,
i.e. can all closed term of type |ℕ| be reduced to numerals, 
which can be adressed using techniques developed in the context of
\emph{Observational Type Theory} \cite{alti:ott-conf}. 
Recent work 
\cite{cubical} suggest that also the harder problem of canonicity in
the presence of univalence can be addressed.

\subsection{Inductive-Inductive Types}
\label{sec:induct-induct-types}

One central construction in Type Theory are \emph{inductive types},
where types are specified using constructors - see the definition of
types and contexts in figure \ref{fig:stl}. In practical dependently
typed programming we use pattern matching - however it is good to know
that this can be replaced by adding just one elimination constant to
each inductive type we are defining \cite{conorthesis}. Here we
differentiate between the \emph{recursor} which enables us to define
non-dependent functions and the eliminator which allows the definition
of dependent functions. The latter corresponds logically to an
induction principle for the given type. Note that the recursor is just
a special case of the eliminator. As an example consider the
recursor and the eliminator for the inductive type |Ty| defined in
figure \ref{fig:stl}:
%\samepage{
\begin{spec}
RecTy  :  (Tyᴹ : Set)
          (ιᴹ : Tyᴹ)
          (⇒ᴹ : Tyᴹ → Tyᴹ → Tyᴹ)
       →  Ty → Tyᴹ
RecTy Tyᴹ ιᴹ ⇒ᴹ ι  = ιᴹ
RecTy Tyᴹ ιᴹ ⇒ᴹ (A ⇒ B)
  = ⇒ᴹ  (RecTy Tyᴹ ιᴹ ⇒ᴹ A)
        (RecTy Tyᴹ ιᴹ ⇒ᴹ B)

ElimTy  :  (Tyᴹ : Ty → Set)
           (ιᴹ  : Tyᴹ ι)
           (⇒ᴹ  :  {A : Ty}(Aᴹ : Tyᴹ A)
                   {B : Ty}(Bᴹ : Tyᴹ B)
                →  Tyᴹ (A ⇒ B))
        →  (A : Ty) → Tyᴹ A
ElimTy Tyᴹ ιᴹ ⇒ᴹ ι = ιᴹ
ElimTy Tyᴹ ιᴹ ⇒ᴹ (A ⇒ B)
  =  ⇒ᴹ  (ElimTy Tyᴹ ιᴹ ⇒ᴹ A)
         (ElimTy Tyᴹ ιᴹ ⇒ᴹ B)
\end{spec}
% }
The type/dependent type we use in the recursor/eliminator (|Tyᴹ|) we
call the \emph{motive} and the functions corresponding to the
constructors (|ιᴹ|,|⇒ᴹ|) are the \emph{methods}. The motive and the
methods of the recursor are the \emph{algebras} of the corresponding
signature functor. The motive |Tyᴹ| is a \emph{family indexed} over
|Ty| and the methods are \emph{fibers} of the family over the
constructors.

We can also define dependent families of types inductively - examples
are |Var| and |Tm| in figure \ref{fig:stl}. We may extend this to
mutual inductive types or mutual inductive dependent types --- however
they can be reduced to a single inductive family by using an extra
parameter of type |Bool| which provides the information which type is
meant.

However, there are examples of mutual definitions which are not
covered by this explanation: that is if we define a type and a family
which depends on this type mutually. In this case we may also refer to
constructors which have been defined previously. A canonical example
for this is a fragment of the definition of the syntax of dependent
types where we only define types and contexts (figure
\ref{fig:indind}).
\begin{figure}[h]
\centering
\begin{spec}
data Con  : Set
data Ty   : Con → Set

data Con where
  •       : Con  
  _,_    : (Γ : Con) → Ty Γ → Con

data Ty where
  U       : ∀{Γ} → Ty Γ
  Π       : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
\end{spec}
\caption{An example of an inductive-inductive type: contexts and types
  in a dependent type theory}
\label{fig:indind}
\end{figure}
Indeed we have to define types and contexts at the same time but types
are indexed by contexts to express that we want to define the type of
valid types in a given context. The constructor |_,_| appears in the
type of the |Π|-constructor to express that the codomain type lives in
the context extended by the domain type.

The definition of such \emph{inductive-inductive} types in Agda is
standard. We first declare the types of all the types we want to
define inductively but without giving the constructors and then
complete the definition of the constructors when all the type
signatures are in scope.

As before, programming with inductive-inductive types can be reduced
to using the elimination constants - see figures \ref{fig:indind-rec}
and \ref{fig:indind-elim} for those of the mutual inductive types
|Con| and |Ty| defined in figure \ref{fig:indind}. To make working
with complex elimination constants feasible we organize their
parameters into records: |Motives|, |Methods|. The motives and methods
for the recursor can be defined just by adding |ᴹ| indices to the
types of the types and constructors. Defining the motives and methods
for the eliminator is more subtle: we need to define families over the
types and fibers of those families over the constructors taking into
account the mutual dependencies --- see section \ref{sec:general-elim}
for a generic way of deriving these. The $\beta$-rules can be added to
the system by pattern matching on the constructors --- these rules are
the same for the recursor and the eliminator.
\begin{figure}
\centering
\begin{spec}
module RecConTy where
  record Motives : Set₁ where
    field
      Conᴹ   : Set
      Tyᴹ    : Conᴹ → Set

  record Methods (M : Motives) : Set₁ where
    open Motives M
    field
      •ᴹ     : Conᴹ
      _,Cᴹ_  : (Γᴹ : Conᴹ) → Tyᴹ Γᴹ → Conᴹ
      Uᴹ     : {Γᴹ : Conᴹ} → Tyᴹ Γᴹ
      Πᴹ     : {Γᴹ : Conᴹ}(Aᴹ : Tyᴹ Γᴹ)
               (Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ)) → Tyᴹ Γᴹ

  module rec (M : Motives)(m : Methods M) where

    open Motives M
    open Methods m

    RecCon : Con → Conᴹ
    RecTy  : {Γ : Con}(A : Ty Γ) → Tyᴹ (RecCon Γ)

    RecCon  •         = •ᴹ
    RecCon  (Γ , A)  = RecCon Γ ,Cᴹ RecTy A

    RecTy   U         = Uᴹ
    RecTy   (Π A B)   = Πᴹ (RecTy A) (RecTy B)  
\end{spec}
\caption{Recursor for the inductive-inductive type of figure
  \ref{fig:indind}}
\label{fig:indind-rec}
\end{figure}
\begin{figure}
\centering
\begin{spec}
module ElimConTy where
  record Motives : Set₁ where
    field
      Conᴹ   :  Con → Set
      Tyᴹ    :  {Γ : Con} → Conᴹ Γ → Ty Γ → Set

  record Methods (M : Motives) : Set₁ where
    open Motives M
    field
      •ᴹ     :  Conᴹ •
      _,Cᴹ_  :  {Γ : Con}(Γᴹ : Conᴹ Γ)
                {A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A)
             →  Conᴹ (Γ , A)
      Uᴹ     :  {Γ : Con}{Γᴹ : Conᴹ Γ}
             → Tyᴹ Γᴹ U
      Πᴹ     :  {Γ : Con}{Γᴹ : Conᴹ Γ}
                {A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A)
                {B : Ty (Γ , A)}
                (Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B)
             →  Tyᴹ Γᴹ (Π A B)

  module elim (M : Motives)(m : Methods M) where

    open Motives M
    open Methods m

    ElimCon :  (Γ : Con) → Conᴹ Γ
    ElimTy  :  {Γ : Con}(A : Ty Γ)
            →  Tyᴹ (ElimCon Γ) A

    ElimCon  •         =   •ᴹ
    ElimCon  (Γ , A)  =   ElimCon Γ ,Cᴹ ElimTy A

    ElimTy   U         =   Uᴹ
    ElimTy   (Π A B)   =   Πᴹ (ElimTy A) (ElimTy B)  
\end{spec}
\caption{Eliminator for the inductive-inductive type of figure
  \ref{fig:indind}}
\label{fig:indind-elim}
\end{figure}

The categorical semantics of inductive-inductive types has been
explored in \cite{alti:catind2,forsberg-phd}. From a computational
point of view inductive-inductive types are unproblematic and pattern
matching can be reduced to using elimination constants which are
derived from the type signature and constructor types.

\subsection{Quotient Inductive Types (QITs)}
\label{sec:qit}

% = 1st order HITs, with truncation
% example: Tree example, refer to book for reals & constr.hierarchy
% quotient types
% emphasize difference!
% Andy Pitts proposal 

One of the main applications of Higher Inductive Types in Homotopy
Type Theory is to represent types with non-trivial equalities
corresponding to the path spaces of topological spaces. Here we are
working in a Type Theory with a strict equality, i.e. all higher path
spaces are trivial. However, there are still interesting applications
for these degenerate HITs which we call Quotient Inductive Types
(QITs). E.g. in \cite{book} QITs (even though not by that name) are
used to define the constructible hierarchy of sets in an encoding of
set theory within HoTT and later to define the Cauchy Reals. What is
striking is that in both cases ordinary quotient types would not have
been sufficient but would have required some form of the axiom of
choice.

Agda does not allow the definition of equality constructors for
inductive types, however following \cite{licata} we can simulate them
by postulating the equality constructors and defining the
recursor/eliminator by pattern matching (see eg. figures
\ref{fig:quotient-constr-elim} and \ref{fig:tree-elim}). This is the
only place where we are allowed to pattern match on an element of a
QIT: later we should only use the eliminator/recursor. This can be
enforced by hiding techniques or turning off pattern matching. Using
this technique we retain the computational behaviour of lower
constructors while enforcing respect for the higher constructors. The
computation rules for the equality constructors always hold
(propositionally) because we work in a theory with UIP. We conjecture
that the logical
consistency of Agda extended by QITs can be
justified by the setoid model. For the more general case of HITs,
some examples have been studied in the context of cubical type theory
\cite{cubical}.

While this is not the use of quotient inductive types which is
directly relevant for our representation of dependently typed calculi
it is worthwhile to explain this in some detail to make clear what
QITs are about.

Our goal is to define infinitely branching trees where the actual
order of subtrees doesn't matter. We start by defining the type of
infinite trees:
\begin{code}
data T₀ : Set where
  leaf : T₀
  node : (ℕ → T₀) → T₀
\end{code}
and now we specify an equivalence relation which allows us to use an
isomorphism to reorder a tree locally.
\begin{code}
data _~_  :  T₀ → T₀ → Set where
  leaf    :  leaf ~ leaf
  node    :  {f g : ℕ → T₀} → (∀ {n}  → f n ~ g n)
          →  node f ~ node g
  perm    :  (g : ℕ → T₀)(f : ℕ → ℕ) → isIso f
          →  node g ~ node (g ∘ f)  
\end{code}
Here |isIso : (ℕ → ℕ) → Set| specifies that the argument is an
isomorphism, i.e. there is an inverse function. Quotient types
\cite{HofmannPhD} can be postulated as shown in figure
\ref{fig:quotient-constr-elim}. With the help of this, we can
construct the type:
\begin{code}
T : Set
T = T₀ / _~_  
\end{code}
Note that this doesn't require to show that |_~_| is an equivalence
relation but the resulting type is equivalent to the quotient with the
equivalence closure of the given relation. The elements of |T| are
equivalence classes |[ t ] : T₀ / _~_| and given |p : t ~ t'| we have
|[ p ]≡ : [ t ] ≡ [ t' ]|. The elimination principle |Elim| allows us
to lift a function |[_]ᴹ| which respects |_~_| (expressed by |[_]≡ᴹ|)
to any element of the quotient.
\begin{figure}
\begin{spec}
data _/_ (A : Set)(R : A → A → Set) : Set where
  [_] : A → A / R
postulate
  [_]≡  :  ∀ {A}{R : A → A → Set}{a b : A}
        →  R a b → [ a ] ≡ [ b ]

module Elim_/_
  (A : Set)(R : A → A → Set)
  (Qᴹ : A / R → Set)
  ([_]ᴹ : (a : A) → Qᴹ [ a ])
  ([_]≡ᴹ  :  {a b : A}(r : R a b)
          →  [ a ]ᴹ ≡[ ap Qᴹ [ r ]≡ ]≡ [ b ]ᴹ)
  where

    Elim : (x : A / R) → Qᴹ x
    Elim [ x ] = [ x ]ᴹ
\end{spec}
  \caption{The constructors and elimination principle for quotient
    types in HoTT-style. Note that the |[_]≡| equality constructor
    needs to be postulated and pattern matching on elements of |A / R|
    is disallowed except when defining |Elim| (this is not checked by
    Agda, we need to ensure this by hand): the only way to define a
    function from |A / R| should be using the eliminator.}
\label{fig:quotient-constr-elim}
\end{figure}

We would expect that we should be able to lift |node| to equivalence
classes, i.e. we would like to define a function |node' : (ℕ → T) → T|
such that |[node f] ≡ node' ([_] ∘ f)|. However, it seems not possible
to do this. To see what the problem is it is instructive to solve the
same exercise for finitely branching trees. It turns out that we have
to sequentially iterate the eliminator depending on the branching
factor. However, clearly this approach doesn't work for infinite
trees. And indeed, in general assuming that we can lift function types
of equivalence classes is equivalent to the axiom of choice. And this
is an intuitionistic taboo since it entails the excluded middle
\cite{diaconescu1975axiom}.

However, if we use a QIT and specify the constructor for equality at
the same time we avoid this issue altogether. Such a version of |T| is
defined in figure \ref{fig:tree-elim}. The main difference here is
that we specify the new equality and the constructors at the same
time. We also do not need to assume that the relation is a congruence
because this is provable in general for the equality type. The
dependent eliminator is given in figure \ref{fig:tree-elim}: we see
that we also need to interpret the equation when we eliminate from |T|
(pattern matching on |T| should be avoided after defining the
eliminator).

% When working in HoTT we should add another constructor forcing the
% type to be a set:
% \begin{code}
%   isSet  : {u v : T'}{e0 e1 : u ≡ v} → e0 ≡ e1  
% \end{code}
% Alternatively, we could globally assume the K-axiom or something
% equivalent since QITs are compatible with assuming that al types are
% sets. We will ignore this issue for the moment.

% While we are not going to define any functions on |T|, it is helpful
% to explicitly state the non-dependent elimination principle: Given a
% type |M : Set| with |l : M| and |n : (ℕ → M) → M| and a proof |p : (g
% : ℕ → M)(f : ℕ → ℕ) → isIso f → n g ≡ n (g ∘ f)| then we can define a
% function |Trec l n p : T → M| with |Trec l n p leaf = l| and |Trec l n
% p (node f) = n (λ i → Trec l n p (f i))|. The dependent eliminator is
% given in figure \ref{fig:tree-elim}.

\begin{figure}
\begin{spec}
data T  :  Set where
  leaf  :  T
  node  :  (ℕ → T) → T
postulate
  perm  :  (g : ℕ → T)(f : ℕ → ℕ) → isIso f
        →  node g ≡ node (g ∘ f)   

module ElimT
  (Tᴹ     :  T → Set)
  (leafᴹ  :  Tᴹ leaf)
  (nodeᴹ  :  {f : ℕ → T}(fᴹ : (n : ℕ) → Tᴹ (f n))
          →  Tᴹ (node f))
  (permᴹ  :  {g : ℕ → T}(gᴹ : (n : ℕ) → Tᴹ (g n))
             (f : ℕ → ℕ)(p : isIso f)
          →  nodeᴹ gᴹ ≡[ ap Tᴹ (perm g f p) ]≡
             nodeᴹ (gᴹ ∘ f))
  where

  Elim : (t : T) → Tᴹ t
  Elim leaf = leafᴹ
  Elim (node f) = nodeᴹ (λ n → Elim (f n))
\end{spec}
\caption{The constructors and eliminator for the quotient-inductive type |T|}
\label{fig:tree-elim}
\end{figure}

Pitts \cite{pitts:private} observed that an alternative way to solve
this problem would be to mutually define |~| and |T₀| and then use 
|T = T₀ / _~| in the negative position of the constructors for |T|,
i.e. 
\begin{code}
  node  : (ℕ → T) → T₀.
\end{code}
It seems plausible that QITs could be reduced to inductive-inductive
definitions and quotient types and the reasonable assumption that
quotient types preserves strict positivity.

% TODO: add something about coe and cubical thinking, move here coe and
% add ap, |◾|, refl.

\section{Representing the Syntax of Type Theory}
\label{sec:syntax}

We are now going to apply the tools introduced in the previous section
to formalize a simple dependent type theory, that is a type theory
with $\Pi$-types and an uninterpreted family denoted by $U : Type$ and
$El : U \to Type$. Despite the naming this is not a universe, but a
base type in the same way as $\iota$ is a base type for the simply
typed $\lambda$ calculus. Without this the syntax would be trivial as
there would be no types or terms we could construct.

\begin{figure}
  \centering
  \begin{spec}
data Con  : Set
data Ty   : Con → Set
data Tms  : Con → Con → Set
data Tm   : ∀ Γ → Ty Γ → Set
  \end{spec}
  \caption{Signature of the syntax}
  \label{fig:syntax-sig}
\end{figure}

The signature of the QIT we are using to represent the syntax of type
theory is given in figure \ref{fig:syntax-sig}. We have already seen
contexts |Con| and Types |Ty| in our previous example for an
inductive-inductive definition. We extend this by explicit
substitutions (|Tms|) which are indexed by two contexts and terms |Tm|
which are indexed by a context and a type in the given context.

\begin{figure}
  \centering
  \begin{spec}
data Con where
  •      : Con
  _,_    : (Γ : Con) → Ty Γ → Con

data Ty where
  _[_]T  : Ty Δ → Tms Γ Δ → Ty Γ
  Π      : (A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
  U      : Ty Γ
  El     : (A : Tm Γ U) → Ty Γ
  \end{spec}
  \caption{Constructors for contexts and types}
  \label{fig:syntax-ty}
\end{figure}

\begin{figure}
  \centering
  \begin{spec}
data Tms where
  ε     :  Tms Γ •
  _,_   :  (δ : Tms Γ Δ) → Tm Γ (A [ δ ]T)
        →  Tms Γ (Δ , A)
  id    :  Tms Γ Γ
  _∘_   :  Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  π₁    :  Tms Γ (Δ , A) →  Tms Γ Δ
  \end{spec}
  \caption{Constructors for substitutions}
  \label{fig:syntax-tms}
\end{figure}

The constructors for contexts are exactly the same as in the previous
example but we are introducing some new constructors for types (figure
\ref{fig:syntax-ty}). Most notably we introduce a substitution
operator |_[_]T| which applies a substitution from |Γ| to |Δ| to a
type in context |Δ| producing a type in context |Γ|. The contravariant
character of substitution arises from the desire to semantically
interpret substitutions as functions going in the same direction, as
we will see in detail in section \ref{sec:standard}. Syntactically it
is good to think of an element of |Tms Γ Δ| as a sequence of terms in
context |Γ| which inhabit all the types in |Δ|. This intuition is
reflected in the syntax for substitutions (figure
\ref{fig:syntax-tms}): |ε| is the empty sequence of terms, and |_,_|
extends a given sequence by an additional term. It is worthwhile to
note that while the type which is added lives in the previous target
context |Δ|, the term has free variables in |Γ| which makes it
necessary to apply the substitution operator on types: |A [ δ ]T|.  We
also introduce inverses to |_,_|, i.e. projections. The first one |π₁|
produces a substitution by forgetting the last term.  Since we have
explicit substitutions we also have explicit composition |_∘_| of
substitutions and consequently also the identity substitution which
will be essential when we reconstruct variables.

\begin{figure}
  \centering
  \begin{spec}
data Tm where
  _[_]t  :  Tm Δ A → (δ : Tms Γ Δ)
         →  Tm Γ (A [ δ ]T) 
  π₂     :  (δ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ δ ]T)
  app    :  Tm Γ (Π A B) → Tm (Γ , A) B
  lam    :  Tm (Γ , A) B → Tm Γ (Π A B)
 \end{spec}
  \caption{Constructors for terms}
  \label{fig:syntax-tm}
\end{figure}

For terms (see figure \ref{fig:syntax-tm}) we also have a
contravariant substitution operator |_[_]t| whose type uses the
substitution operator for types. We also introduce the second
projection |π₂| which projects out the final term from a non-empty
substitution. Finally, we introduce |app| and |lam| which construct an
isomorphism between |Tm (Γ , A) B| and |Tm Γ (Π A B)|.

Let's explore how our categorically inspired syntax can be used to
derive more mundane syntactical components such as variables. First we
derive the weakening substitution:
\begin{spec}
wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
wk = π₁ id
\end{spec}
We need to derive typed de Bruijn variables as in the example for the
simply typed $\lambda$-calculus. However, we have to be more precise
because the result types live in the extended context and hence we
need weakening. However, the definitions are straightforward:
\begin{spec}
vz : Tm (Γ , A) (A [ wk ]T)
vz = π₂ id

vs : Tm Γ A → Tm (Γ , B) (A [ wk ]T) 
vs x = x [ wk ]t  
\end{spec}

Now we turn our attention to the constructors giving the equations. To
define these we sometimes need to \emph{transport} elements along
equalities. To simplify this we introduce a number of convenient
operations. |coe| turns an equality between types into a function:
\begin{spec}
coe : A ≡ B → A → B  
\end{spec}
Specifically in our current construction we often want to coerce terms
along an equality of syntactic types, to facilitate this we introduce
an operation which lifts an equality between syntactic types to an
equality of semantic types of terms:
\begin{spec}
TmΓ≡  :  {A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
      →  Tm Γ A₀ ≡ Tm Γ A₁
\end{spec}
A more general version of this function is |ap| (apply path in HoTT
terminology):
\begin{spec}
ap  :  (f : A → B){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
    →  f a₀ ≡ f a₁
\end{spec}

We introduce no additional equations on contexts - however, the
equality of contexts is not syntactic since they contain types which
has non-trivial equalities.

\begin{figure}
  \centering
\begin{spec}
   [id]T  :  A [ id ]T ≡ A
   [][]T  :  A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T
   U[]    :  U [ δ ]T ≡ U
   El[]   :  El A [ δ ]T ≡ El (coe (TmΓ≡ U[]) (A [ δ ]t))

_ ^ _  :  (δ : Tms Γ Δ)(A : Ty Δ)
       →  Tms (Γ , A [ δ ]T) (Δ , A)
δ ^ A = (δ ∘ π₁ id) , coe (TmΓ≡ [][]T) (π₂ id)

   Π[]    :  (Π A B) [ δ ]T ≡ Π (A [ δ ]T) (B [ δ ^ A ]T) 
\end{spec}
  \caption{Equations for types}
  \label{fig:eqn-ty}
\end{figure}

Let us turn our attention to syntactic types |Ty|, see figure
\ref{fig:eqn-ty}. The first two equations explain how substitution
interacts with the identity substitution and composition. The
remaining equations explain how substitutions move inside the other
type constructors. When introducing the equation for |Π| we notice
that we need to lift a substitution along a type, that is given an
element of |Tms Γ Δ| we want to derive |Tms (Γ , A [ δ ]T) (Δ ,
A)|. This is accomplished by |_ ^ _| which can be defined using
existing constructors of substitutions and terms, namely |δ ^ A = (δ ∘
wk , vz)|. However this doesn't typecheck since the second component of
the substitution should have type |Tm (Γ , A [ δ ]T) (A [ δ ∘ wk ]T)|
but |vz| has type |Tm (Γ , A[ δ ]T) (A[ δ ]T [ wk ]T)|. However using
the equation just introduced which describes the interaction between
substitution and composition can be used to fix this issue.

\begin{figure}
  \centering
\begin{spec}
   idl   : id ∘ δ ≡ δ 
   idr   : δ ∘ id ≡ δ 
   ass   : (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
   ,∘    : (δ , t) ∘ σ ≡ (δ ∘ σ) , coe (TmΓ≡ [][]T) (t [ σ ]t)
   π₁β   : π₁ (δ , t) ≡ δ
   πη    : (π₁ δ , π₂ δ) ≡ δ
   εη    : {σ : Tms Γ •} → σ ≡ ε
\end{spec}
  \caption{Equations for substitutions}
  \label{fig:eqn-tms}
\end{figure}

The equations for substitutions (figure \ref{fig:eqn-tms}) state that
substitutions form a category and how composition commutes with |_,_|
which relies again on |[][]T|. We also state that |π₁| works as
expected and that surjective pairing holds. There is only one
substitution into the empty context (|εη|), this entails that |ε| is a
terminal object in the category of substitutions.

\begin{figure}
  \centering
\begin{spec}
   [id]t : t [ id ]t ≡[ TmΓ≡ [id]T ]≡ t
   [][]t : (t [ δ ]t) [ σ ]t ≡[ TmΓ≡ [][]T ]≡  t [ δ ∘ σ ]t
   π₂β   : π₂ (δ , a) ≡[ TmΓ≡ (ap (_[_]T A) π₁β) ]≡ a
   Πβ    : app (lam t) ≡ t
   Πη    : lam (app t) ≡ t
   lam[] : (lam t) [ δ ]t ≡[ TmΓ≡ Π[] ]≡ lam (t [ δ ^ A ]t)
\end{spec}
  \caption{Equations for terms}
  \label{fig:eqn-tm}
\end{figure}

The equations for terms (figure \ref{fig:eqn-tm}) start similarly to
those for types: first we explain how term substitution interacts with
the identity substitution and composition. Unsurprisingly, these laws
are \emph{upto} the corresponding laws for types. We state the law for
the second projection whose typing relies on the equation for the
first projection. The equations |Πβ| and |Πη| state that |lam| and
|app| are inverse to each other. |lam[]| explains how substitutions
can be moved into $\lambda$-abstractions. This law refers to the
substitution law for |Π|-types which can be viewed as an example of
the Beck-Chevalley condition \cite{beckchevalley}. Note that a
corresponding law for |app| is derivable:
\begin{spec}
app (coe (TmΓ≡ Π[]) (t [ δ ]t)) 
  ≡⟨ ap (λ z → app  (coe (TmΓ≡ Π[]) (z [ δ ]t)))
                    (Πη ⁻¹) ⟩
app (coe (TmΓ≡ Π[]) ((lam (app t)) [ δ ]t))
  ≡⟨ ap app lam[] ⟩
app (lam (app t [ δ ^ A ]t))
  ≡⟨ Πβ ⟩
app t [ δ ^ A ]t
\end{spec}

We observe that our inductive definition defines an initial categories
with families (CwF) \cite{dybjer1996internal}. The equations can be
summarized as follows: the contexts form a category with a terminal
object and we have the corresponding laws |[id]T/t|, |[][]T/t| for
substitutions of types and terms; we have substitution rules for type
formers |U[]|, |El[]|, |Π[]|; the rest of the equations express two
natural isomorphisms, one for the substitution extension |_,_| and one
for |Π|: the |β| laws express that going down and up is the identity,
the |η| laws express that going up and then down is the identity,
while naturalities give the relationship with substitutions (an
isomorphism is natural in both directions if it is natural in one
direction).

\begin{equation*}
    |_,_| \downarrow\begin{array}{l}\infer={|Tms Γ (Δ , A)|}{|ρ : Tms Γ Δ| && |Tm Γ (A [ ρ ]T)|}\end{array} \uparrow |π₁ , π₂|
\end{equation*}
\begin{equation*}
    |lam| \downarrow\begin{array}{l}\infer={|Tm Γ (Π A B)|}{|Tm (Γ , A) B|}\end{array}\uparrow |app|
\end{equation*}

We show how a more conventional application operator can be
derived. First we introduce one term substitution:

\begin{spec}
<_> : Tm Γ A → Tms Γ (Γ , A)
< t > = id , coe (TmΓ≡ ([id]T ⁻¹)) t
\end{spec}
Note that here we need to apply the equation for identity
substitutions backward exploiting symmetry for equations |_⁻¹|.
Given this it is easy to state and derive ordinary application:
\begin{spec}
_ $ _  :  Tm Γ (Π A B) → (u : Tm Γ A)
       →  Tm Γ (B [ < u > ]T)
t $ u = (app t) [ < u > ]t
\end{spec}

We prefer to use the categorical combinators in the definition of the
syntax since they are easier to work with, avoiding unnecessary
introduction of single term substitutions which correspond to using
|id|.

We define the recursor and the eliminator analogously to the examples
in section \ref{sec:meta}. The motives and methods have the same names
as the types and constructors with an added |ᴹ| index. We list the
motives and the methods for types for the eliminator in figure
\ref{fig:synt-motives}. Note the usage of lifted congruence rules such
as |TyΓᴹ≡| and |TmΓᴹ≡| and how lifting the coerces is done. Also we
define a lifting of the |_ ^ _| helper function.
\begin{figure}[h]
\begin{spec}
record Motives : Set₁ where
  field
    Conᴹ    :  Con → Set
    Tyᴹ     :  Conᴹ Γ → Ty Γ → Set
    Tmsᴹ    :  Conᴹ Γ → Conᴹ Δ → Tms Γ Δ
            →  Set
    Tmᴹ     :  Tyᴹ Γᴹ A → Tm Γ A → Set
    
record Methods (M : Motives) : Set₁ where
  open Motives M
  field
    ...
    _[_]Tᴹ    :   (δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
              →   Tyᴹ Γᴹ (A [ δ ]T)
    Uᴹ        :   {Γᴹ : Conᴹ Γ} → Tyᴹ Γᴹ U
    Elᴹ       :   (Âᴹ : Tmᴹ Γᴹ Uᴹ Â) → Tyᴹ Γᴹ (El Â)
    Πᴹ        :   (Aᴹ : Tyᴹ Γᴹ A)
                  (Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B)
              →   Tyᴹ Γᴹ (Π A B)
    ...
    [id]Tᴹ    :   Aᴹ [ idᴹ ]Tᴹ ≡[ TyΓᴹ≡ [id]T ]≡ Aᴹ
    [][]Tᴹ    :   Aᴹ [ δᴹ ]Tᴹ [ σᴹ ]Tᴹ
                  ≡[ TyΓᴹ≡ [][]T ]≡
                  Aᴹ [ δᴹ ∘ᴹ σᴹ ]Tᴹ
    U[]ᴹ      :   Uᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ≡ U[] ]≡ Uᴹ
    El[]ᴹ     :   Elᴹ Âᴹ [ δᴹ ]Tᴹ
                  ≡[ TyΓᴹ≡ El[] ]≡
                  Elᴹ (coe  (TmΓᴹ≡ U[]ᴹ refl)
                            (Âᴹ [ δᴹ ]tᴹ))
               
  _^ᴹ_  :  (δᴹ : Tmsᴹ Γᴹ Δᴹ δ)(Aᴹ : Tyᴹ Δᴹ A)
        →  Tmsᴹ  (Γᴹ ,Cᴹ Aᴹ [ δᴹ ]Tᴹ)
                 (Δᴹ ,Cᴹ Aᴹ) (δ ^ A)
                 (δ ^ A)
  δᴹ ^ᴹ Aᴹ  =    (δᴹ ∘ᴹ π₁ᴹ idᴹ)
            ,sᴹ  coe (TmΓᴹ≡ [][]Tᴹ refl) (π₂ᴹ idᴹ)

  field
    Π[]ᴹ :  Πᴹ Aᴹ Bᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ≡ Π[] ]≡
            Πᴹ (Aᴹ [ δᴹ ]Tᴹ) (Bᴹ [ δᴹ ^ᴹ Aᴹ ]Tᴹ)
    ...
\end{spec}
\caption{The motives and some methods for the eliminator for the
  syntax}
\label{fig:synt-motives}
\end{figure}

An interpretation of the syntax can be given by providing elements of
the records |Motives| and |Methods|. Soundness is ensured by the
methods for equality constructors. This way a model of Type Theory can
be viewed as an algebra of the syntax.

% relate to CwF isos

\section{The Standard Model}
\label{sec:standard}

\begin{figure}
  \centering
\begin{spec}
M : Motives
M = record
  {   Conᴹ               =  Set
  ;   Tyᴹ   ⟦Γ⟧          =  ⟦Γ⟧ → Set
  ;   Tmsᴹ  ⟦Γ⟧   ⟦Δ⟧    =  ⟦Γ⟧ → ⟦Δ⟧
  ;   Tmᴹ   ⟦Γ⟧   ⟦A⟧    =  (γ : ⟦Γ⟧) → (⟦A⟧ γ)
  }

⟦_⟧C   :   Con           →  Set
⟦_⟧T   :   Ty Γ          →  ⟦ Γ ⟧C → Set
⟦_⟧s   :   Tms Γ Δ       →  ⟦ Γ ⟧C → ⟦ Δ ⟧C
⟦_⟧t   :   (t : Tm Γ A)  →  (γ : ⟦ Γ ⟧C) → ⟦ A ⟧T γ
\end{spec}
  \caption{Motives for the standard model and how we would define the
    type of the interpretation functions using usual Agda syntax}
  \label{fig:mot-std}
\end{figure}

As a first sanity check of our syntax we define the standard model
where every syntactic construct is interpreted by its semantic
counterpart --- this is also sometimes called the metacircular
interpretation. That means we interpret contexts as types, types as
dependent types indexed over the interpretation of their context,
terms as dependent functions and substitutions as functions. We use
the recursor to define this interpretation, the motives are given in
figure \ref{fig:mot-std}. We use an idealized record notation where
fields can have parameters (in real Agda these need to be lambda
expressions). The parameters of the fields of the motives are the
results of the recursive calls of the recursor.

 % For comparison, we
% have also given the standard recursive definition of the type of the
% mutually defined functions from the syntax.


\begin{figure}
  \centering
\begin{spec}
m : Methods M
m = record
  {  •ᴹ                =  ⊤
  ;  ⟦Γ⟧ ,Cᴹ ⟦A⟧       =  Σ ⟦Γ⟧ ⟦A⟧
  ;  ⟦A⟧ [ ⟦δ⟧ ]Tᴹ  γ  =  ⟦A⟧ (⟦δ⟧ γ)
  ;  Uᴹ             _  =  ⟦U⟧
  ;  Elᴹ ⟦t⟧        γ  =  ⟦El⟧ (⟦t⟧ γ)
  ;  Πᴹ ⟦A⟧ ⟦B⟧     γ  =  (x : ⟦A⟧ γ) → ⟦B⟧ (γ , x)
  ;  εᴹ             _  =  tt
  ;  ⟦δ⟧ ,sᴹ ⟦t⟧    γ  =  ⟦δ⟧ γ , ⟦t⟧ γ
  ;  idᴹ            γ  =  γ
  ;  ⟦δ⟧ ∘ᴹ ⟦σ⟧     γ  =  ⟦δ⟧ (⟦σ⟧ γ)
  ;  π₁ᴹ ⟦δ⟧        γ  =  proj₁ (⟦δ⟧ γ)
  ;  ⟦t⟧ [ ⟦δ⟧ ]tᴹ  γ  =  ⟦t⟧ (⟦δ⟧ γ)
  ;  π₂ᴹ ⟦δ⟧        γ  =  proj₂ (⟦δ⟧ γ)
  ;  appᴹ ⟦t⟧       γ  =  ⟦t⟧ (proj₁ γ) (proj₂ γ)
  ;  lamᴹ ⟦t⟧       γ  =  λ a → ⟦t⟧ (γ , a)
  ;  [id]Tᴹ            =  refl
     ...
  }
\end{spec}
  \caption{Methods for the standard model}
  \label{fig:std-meth}
\end{figure}

The definition of the methods is now straightforward (figure
\ref{fig:std-meth}). In particular the interpretation of substitution
nicely explains the contravariant character of the substitution
rules. Note that |⟦U⟧ : Set| and |⟦El⟧ : ⟦U⟧ → Set| are
module parameters.

We have omitted the interpretation of all the equational
constants because they are trivial: all of them are |refl| because the
two sides are actually convertible in the metatheory.

A consequence of the standard model is soundness, that is in our case
we can show that there is no closed term of |U| because we can
instantiate |U| with the empty type. It should also be clear that to
construct the standard model we need a stronger metatheory than the
object theory we are considering. In our case this is given by the
presence of an additional universe (here we have to eliminate over
|Set₁|).

% as a trivial algebra
% evaluation, soundness
% decidability as a metatheorem

\section{The Logical Predicate Interpretation}
\label{sec:extern-param}

% TODO: > Martin-Loef talk, Reynolds, old theorem

In this section, after briefly introducing parametricity for a simple
dependent type theory we describe our formalisation of this
interpretation using the syntax given in section
\ref{sec:syntax}. This is a real-world example of the usefulness of
our representation of the syntax; note that not only the domain of our
interpretation but also the codomain is the syntax. This
interpretation could be useful in connection with metaprogramming:
using a quoting mechanism, one could automatically derive
parametricity properties of functions defined in Agda.

\subsection{Logical Relations for Dependent Types}

Logical relations were introduced in computer science by Reynolds
\cite{reynolds83abstraction} for expressing the idea of
representation-independence in the context of the polymorphic
$\lambda$-calculus. Reynold's abstraction theorem (also called
parametricity) states that logically related interpretations of a term
are logically related in the relation generated by the type of the
term. This was later extended to dependent types by \cite{bernardy}:
Type Theory is expressive enough to express the parametricity
statement of its own terms --- the logic in which the logical
relations are expressed can be the theory itself. Contexts are
interpreted as syntactic contexts, types as types in the
interpretation of their context and terms as witnesses of the
interpretation of their types.

We describe the parametric interpretation for an explicit substitution
calculus with named variables, universes a la Russel, Pi types and one
universe. The syntax of contexts, terms and types is the following:
\begin{spec}
  Γ        ::=  • | Γ , x : A
  t,u,A,B  ::=  x | Set | (x:A) → B | λ x → t | t u
             |  t [ σ ]
  σ , δ    ::=  ε | (σ , x ↦ t) | σ ∘ δ | id
\end{spec}
|t [ σ ]| is the notation for a substituted term, weakening is
implicit. We omit the typing rules, they are standard and are given in
the formal development.

We define the unary logical predicate operation |_ᴾ| on the syntax
following \cite{bernardy}. This takes types to their corresponding
logical predicates, contexts (lists of types) to lists of related
types and terms to witnesses of relatedness in the predicate
corresponding to their types. We define |_ᴾ| by first giving its
typing rules for contexts, terms (which here include types) and
substitutions:
\begin{equation*}
  \begin{gathered}
    \infer{|Γ ᴾ| \text{ valid}}{|Γ| \text{ valid}}
  \end{gathered}
  \hspace{2em}
  \begin{gathered}
    \infer{|Γ ᴾ ⊢ t ᴾ : (A ᴾ) t|}{|Γ ⊢ t : A|}
  \end{gathered}
  \hspace{2em}
  \begin{gathered}
    \infer{|σ ᴾ : Γ ᴾ ⟶ Δ ᴾ|}{|σ : Γ ⟶ Δ|}
  \end{gathered}
\end{equation*}
The second rule expresses parametricity, i.e. the internal fundamental
theorem for logical relations. The rule for terms, when specialised to
elements of the universe expresses that |A ᴾ| is a predicate over |A|:
\begin{equation*}
  \infer{|Γ ᴾ ⊢ A ᴾ : A → Set|}{|Γ ⊢ A : Set|}
\end{equation*}
The operation |_ᴾ| is defined by induction on the syntax as follows:
\begin{spec}
  • ᴾ               = •
  (Γ , x : A) ᴾ     = Γ ᴾ , x : A , xᴹ : A ᴾ x
  x ᴾ               = xᴹ
  Set ᴾ             = λ A → (A → Set)
  ((x : A) → B) ᴾ   = λ f →  ((x : A) (xᴹ : A ᴾ x)
                             → B ᴾ (f x))
  (λ x → t) ᴾ       = λ x xᴹ → t ᴾ
  (t u) ᴾ           = t ᴾ u (u ᴾ)
  (t [ σ ]) ᴾ       = t ᴾ [ σ ᴾ ]
  ε ᴾ               = ε
  (σ , x ↦ t) ᴾ     = (σ ᴾ , x ↦ x, xᴹ ↦ t ᴾ)
  (σ ∘ δ) ᴾ         = σ ᴾ ∘ δ ᴾ
  id ᴾ              = id
\end{spec}

\subsection{Formal Development}

Using Agda, we formalise the above interpretation for the theory
described in section \ref{sec:syntax}. Note that here the type |U|
will be interpreted as a universe since we need to express
predicates. We express parametricity by a dependent function of the
following type:
\begin{spec}
(t : Tm Γ A) → Tm (Γ ᴾ) (A ᴾ [ < t [ pr Γ ]t > ]T)
\end{spec}
As |t| lives in context |Γ|, we need to substitute it by using a
substitution |pr Γ : Tms (Γ ᴾ) Γ|. |A ᴾ| will not be a predicate
function anymore but a type in an extended context, see below. Because
the above given type is a dependent type, contrary to the
standard model, we cannot use the recursor to define this
interpretation, we need the eliminator.

In contrast to the informal presentation of parametricity, here we
have an additional judgement for types and we use de Bruijn indices
instead of variable names which makes explicit weakening
necessary. The motives for the eliminator are defined in figure
\ref{fig:logpred-motives}.
\begin{figure}
\begin{spec}
record Conᵐ (Γ : Con) : Set where
  field
    =C : Con
    Pr : Tms =C Γ

Tyᵐ : Conᵐ Γ → Ty Γ → Set
Tyᵐ Γᴹ A = Ty (=C Γᴹ , A [ Pr Γᴹ ]T)

record Tmsᵐ  (Γᴹ : Conᵐ Γ)(Δᴹ : Conᵐ Δ)
             (ρ : Tms Γ Δ) : Set where
  field
    =s    : Tms (=C Γᴹ) (=C Δᴹ)
    PrNat : (Pr Δᴹ) ∘ =s ≡ ρ ∘ (Pr Γᴹ)

Tmᵐ : (Γᴹ : Conᵐ Γ) → Tyᵐ Γᴹ A → Tm Γ A → Set
Tmᵐ Γᴹ Aᴹ a = Tm (=C Γᴹ) (Aᴹ [ < a [ Pr Γᴹ ]t > ]T)

M : Motives
M = record  {  Conᴹ  = Conᵐ
            ;  Tyᴹ   = Tyᵐ
            ;  Tmsᴹ  = Tmsᵐ
            ;  Tmᴹ   = Tmᵐ }
\end{spec}
\caption{Motives for the eliminator in the logical predicate interpretation}
\label{fig:logpred-motives}
\end{figure}
We need to specify the fields |Conᴹ|, |Tyᴹ|, |Tmsᴹ| and |Tmᴹ| in the
record type |Motives|. We construct these components separately as
|Conᵐ|, |Tyᵐ|, |Tmsᵐ| and |Tmᵐ|. |Conᵐ| specifies what contexts will
be mapped to: they will not only be mapped to the doubled context
|=C|, but also to a weakening substitution |Pr| from the doubled
context to the original one. We put these together into a record where
|=C| and |Pr| are the projections, so if |Γᴹ : Conᴹ| then |=C Γᴹ :
Con| and |Pr Γᴹ : Tms (=C Γᴹ) Γ|.

The motive for types receives the result |Γᴹ| of the eliminator on the
context and the type as |A| arguments. It will need to return a type
in the context |=C Γᴹ| extended with the type |A| (which needs to be
substituted by the projection). The predicate over |A| is expressed by
a type in a context extended with the domain of the predicate. A
substitution |ρ| will be mapped to the interpretation |=s| which is
between two doubled contexts and to a naturality property |PrNat|
which expresses that |=s| commutes with |Pr|. This property is used
eg. to define the method for |_[_]T|, see below. Terms will be mapped
to terms in the doubled context and their type is the predicate at the
original term (which has to be weakened by |Pr|): this is expressed by
substituting the type by the term using |<_>| (defined in section
\ref{sec:syntax}). After defining the methods for this interpretation,
the eliminator |ElimTm| will give us a proof of parametricity for this
theory:
\begin{spec}
=C ∘ ElimCon  :  Con → Con
=s ∘ ElimTms  :  Tms Γ Δ
              →  Tms  (=C (ElimCon Γ))
                      (=C (ElimCon Δ))
Pr ∘ ElimCon  :  (Γ : Con)
              →  Tms (=C (ElimCon Γ)) Γ
ElimTy        :  (A : Ty Γ)
              →  Ty  (  =C (ElimCon Γ)
                     ,  A [ Pr (ElimCon Γ) ]T)
ElimTm        :  (t : Tm Γ A)
              →  Tm  (=s (ElimCon Γ))
                     (  ElimTy A
                        [ < t [ Pr (ElimCon Γ) ]t > ]T)
\end{spec}

We list the methods for contexts and types as fields of the record
|Methods| in figure \ref{fig:logpred-conty}. We omitted some implicit
arguments and used record syntax more liberally than Agda. The other
methods are straightforward but tedious to define due to the coercions
that need to be performed. For details see the supplementary material.

The empty context is mapped to the empty context and the empty
substitution. The context |Γ , A| is mapped to the doubled context |=C
Γᴹ| for |Γ| extended by |A| and the interpretation |Aᴹ|. The
projection substitution for |Γ , A| just projects out the |A| by
lifting |Pr Γᴹ| and is weakened so that it forgets about the
additional |Aᴹ| in the context.

For deriving the interpretation of a type |A : Ty Δ| substituted by |δ
: Tms Γ Δ| we need to give a type in the context |=C Γᴹ , A [ δ ]T [
  Pr Γᴹ ]T| by the motive for types. The type |Aᴹ| lives in the
context |=C Δᴹ , A [ Pr Δᴹ ]T| and by the interpretation of the
substitution |δ| and lifting over |A [ Pr Δᴹ ]T| by |_ ^ _| we get
\begin{spec}
  =s δᴹ ^ A [ Pr Δᴹ ]T
    : Tms  (=C Γᴹ , A [ Pr Δᴹ ]T [ =s δᴹ ]T)
           (=C Δᴹ , A [ Pr Δᴹ ]T).
\end{spec}
We can substitute |Aᴹ| by |=s δᴹ ^ A [ Pr Δᴹ ]T| but still we would
get a type in the context
\begin{spec}
=C Γᴹ , A [ Pr Δᴹ ]T [ =s δᴹ ]T
\end{spec}
instead of
\begin{spec}
=C Γᴹ , A [ δ ]T [ Pr Γᴹ ]T.
\end{spec}
However by the naturality rule |PrNat δᴹ| we know that
\begin{spec}
  Pr Δᴹ ∘ =s δᴹ ≡ δ ∘ Pr Γᴹ .
\end{spec}
With this in mind we can perform the following equality reasoning:
\begin{spec}
A [ Pr Δᴹ ]T [ =s δᴹ ]T  ζ
                         ≡⟨  [][]T                    ⟩
A [ Pr Δᴹ ∘ =s δᴹ ]T
                         ≡⟨  ap (_[_]T A) (PrNat δᴹ)  ⟩
A [ δ ∘ Pr Γᴹ ]T    
                         ≡⟨  [][]T ⁻¹                 ⟩
A [ δ ]T [ Pr Γᴹ ]T    
\end{spec}
Coercing |=s δᴹ ^ A [ Pr Δᴹ ]T| along this equality (we denote
transitivity of equality by |_◾_|) we get a substitution of the right
type
\begin{spec}
Tms  (=C Γᴹ , A [ Pr Δᴹ ]T [ =s δᴹ ]T)
     (=C Δᴹ , A [ Pr Δᴹ ]T).
\end{spec}
Similar coercions are taking place everywhere in the
interpretation. In the rest of the code-snippet we just write |_| for
the proofs of equalities for the coercions. The interpretation of |U|
is like that of |Set| in the informal presentation: predicates over
the type corresponding to the code in the last element of the context
which can be projected by |π₂ id|. The predicate returns a code of a
type in |U|. The interpretation of |El| goes the opposite way: it
takes the predicate |Aᴹ| from |A| into the universe and turns it into
a predicate type by first using |app| to depend on the last element of
the context and then applying |El| to get the type corresponding to
the code. The interpretation of |Π| is the usual logical relation
interpretation: we are in the context |=C Γᴹ , Π A B [ Pr Γᴹ ]T| and
we would like to state that related arguments are mapped to related
results by the function given in the last element of the context. We
quantify over the argument type |A| (which needs to be weakened by |Pr
Γᴹ| because we are in an interpreted context, and by one step further
because of the last element in the context) and then over |Aᴹ| which
depends on |A| so the weakening here over |Π A B [ Pr Γᴹ ]T| needs to
be lifted by |_ ^ _|. The target of the function is |Bᴹ| which lives
in the context |(=C (Γᴹ ,Cᴹ Aᴹ) , B [ Pr (Γᴹ ,Cᴹ Aᴹ) ]T)|, the first
part of which is provided by the substitution
\begin{spec}
  π₁ id ^ A [ Pr Γᴹ ]T ^ Aᴹ
    : Tms  (  =C Γᴹ , Π A B [ Pr Γᴹ ]T
           ,  A [ Pr Γᴹ ]T [ π₁ id ]T
           ,  Aᴹ [ π₁ id ^ A [ Pr Γᴹ ]T ]T)
           (=C Γᴹ , A [ Pr Γᴹ ]T , Aᴹ)
\end{spec}
which forgets the function from the context and is identity on the
rest of the context and the second part is given by applying the
function to the next element in the context and appropriately
weakening and coercing the result.

\begin{figure}
\begin{spec}
m : Methods M
m = record
  {  •ᴹ = record { =C = • ; Pr = ε }
  
  ;  Γᴹ ,Cᴹ Aᴹ

     = record  {  =C  =  (=C Γᴹ ,  A [ Pr Γᴹ ]T) , Aᴹ
               ;  Pr  =  (Pr Γᴹ ^ A) ∘ π₁ id } }

  ;  Aᴹ [ δᴹ ]Tᴹ
  
     =  Aᴹ [ coe  (ap  (λ σ → Tms (=C Γᴹ , σ) _)
                       (  [][]T
                       ◾  ap (_[_]T A) (PrNat δᴹ)
                       ◾  [][]T ⁻¹))
                  (=s δᴹ ^ A [ Pr Δᴹ ]T) ]T
                    
  ;  Uᴹ = Π (El (coe _ (π₂ id))) U
                          
  ;  Elᴹ Âᴹ = El (app (coe _ Âᴹ))
  
  ;  Πᴹ Aᴹ Bᴹ

     = Π  (A [ Pr Γᴹ ]T [ π₁ id ]T)
          (Π  (Aᴹ  [ π₁ id ^ A [ Pr Γᴹ ]T ]T)
              (Bᴹ  [  π₁ id ^ A [ Pr Γᴹ ]T ^ Aᴹ
                   ,  coe _ (  app (coe _ (π₂ id))
                               [ π₁ id ]t) ]T))
  }
\end{spec}
\caption{Methods for specifying the logical predicate interpretation
  for contexts and types}
\label{fig:logpred-conty}
\end{figure}

Defining the logical predicate interpretation is tedious but
feasible. Most of the work that needs to be done is coercing syntactic
expressions using equality reasoning which can be simplified by using
a heterogeneous equality \cite{alti:ott-conf} --- two terms of
different types are equal if there is an equality between the types
and an equality between the terms up to this previous equality.
\begin{spec}
record _≃_ {A B : Set}(a : A)(b : B) : Set₁ where
  constructor _,_
  field
    projT : A ≡ B
    projt : a ≡[ projT ]≡ b
\end{spec}
|_≃_| can be proven to be reflexive, symmetric and transitive so
equality reasoning can be done in the usual way. But it has the
advantage that we can forget about coercions during reasoning:
\begin{spec}
uncoe : {a : A}(p : A ≡' B) → a ≃ coe p a
\end{spec}
Also, we can convert back and forth with the usual homogeneous
equality:
\begin{spec}
from≡  :  (p : A ≡ B)  →  a ≡[ p ]≡ b → a ≃ b

to≡    :  (p : a ≃ b)  →  a ≡[ projT p ]≡ b
\end{spec}
We note that the axiom of function extensionality was not used
throughout the logical preciate interpretation.

\subsection{The Eliminator for Closed Inductive-Inductive Types}
\label{sec:general-elim}

An application of the logical predicate interpretation is to derive
the syntax of the motives and methods for the eliminator of a closed
inductive-inductive type.

A general closed inductive-inductive type has the following
description in Agda notation:
\begin{spec}
  data A₁  : T₁      (signatures of types)
  ...
  data Aₙ  : Tₙ

  data A₁ where      (constructors for A₁)
    c₁₁    : A₁₁
    ...
    c₁ₘ₁   : A₁ₘ₁
  ...
  data Aₙ  where     (constructors for Aₙ)
    cₙ₁    : Aₙ₁
    ...
    cₙₘₙ   : Aₙₘₙ
\end{spec}
We have |n| types and the type |Aᵢ| has |mᵢ| constructors. Agda
restricts parameters of the constructors to only have strictly positive
recursive occurrences of the type. The same restriction applies here.

First we note that the above description can be collected into the
context where the variable names are the type names and constructor
names, and they have the corresponding types:
\begin{spec}
  • , A₁ : T₁ , ..., Aₙ : Tₙ , c₁₁ : A₁₁ , ..., c₁ₘ₁ : A₁ₘ₁ ,
  ..., cₙ₁ : Aₙ₁ , ..., cₙₘₙ : Aₙₘₙ
\end{spec}
To define the motives and methods for the eliminator, we need a family
over the types and fibers of that family over the constructors. By
applying the |_ᴾ| operation to this context, the context is extended
by new elements |A₁ᴹ , ..., Aₙᴹ| the types of which are the motives
and by new elements |c₁₁ᴹ , ..., cₙₘₙᴹ| the types of which will be the
methods, and they can be listed in a record:
\begin{spec}
  record Motives : Set where
    field
      A₁ᴹ    : T₁ ᴾ A₁
      ...
      Aₙᴹ    : Tₙ ᴾ Aₙ

  record Methods (M : Motives) : Set where
    open Motives M
    field
      c₁₁ᴹ   : A₁₁ ᴾ c₁₁
      ...
      cₙₘₙᴹ  : Aₙₘₙ ᴾ cₙₘₙ
\end{spec}

The method described here extends to types with equality constructors
by using the logical predicate interpretation of the equality
type. This is how we derived the motives and methods for the
eliminator of the syntax.

\section{Homotopy Type Theory}
\label{sec:hott}

So far we have assumed uniqueness of identity proofs so let us have a
look at what happens if we give this up to be compatible with Homotopy
Type Theory as presented in \cite{book}. If we take our definition of
the Syntax and consider it as a HIT, we get a strange theory. Because
we have not identified any of the equality constructors we introduced
this leads to a very non-standard type theory. I.e. we may consider
two types which have the same syntactic structure but which at some
point use two different derivations to derive the same equality but
these cannot be shown to be equal.

However, this can be easily remedied by \emph{truncating} our syntax
to be a set, i.e. by introducing additional constructors:
\begin{spec}
    setT  : {A B : Ty Γ}     {e0 e1 : A ≡ B}  → e0 ≡ e1
    sets  : {δ σ : Tms Γ Δ}  {e0 e1 : δ ≡ σ}  → e0 ≡ e1
    sett  : {u v : Tm Γ A}   {e0 e1 : u ≡ v}  → e0 ≡ e1
\end{spec}
These force our syntax to be a \emph{set} in the sense of HoTT, i.e. a
type for which UIP holds. We don't need to do this for |Con| because
this can be shown to be a set from the assumption that |Ty| are sets.
It seems to be entirely sensible to assume that the syntax forms a
set, indeed we would want to show that equality is decidable which
implies that the type is a set by Hedberg's theorem \cite{hedberg}.

However, we now run into a different problem: we can only eliminate
into a type which is a set itself. That means that we cannot even
define the standard model because we have to eliminate into |Set₁|,
the type of all small types, which is not a set in the sense of HoTT
due to univalence, that is it has there may be more that one equality
proof between two sets.
One way around this would be to replace |Set₁| by an
inductive-recursive universe, which can be shown to be a set but for
which univalence fails (see the formal development for the proofs).
\begin{spec}
data UU : Set
EL : UU → Set

data UU where
  ′Π′ : (A : UU) → (EL A → UU) → UU 
  ′Σ′ : (A : UU) → (EL A → UU) → UU
  ′⊤′ : UU

EL (′Π′ A B) =   (x : EL A) → EL (B x)
EL (′Σ′ A B) = Σ (EL A) λ x → EL (B x)
EL ′⊤′ = ⊤  
\end{spec}

An apparent way around the limitation that we can only eliminate into
sets would be to only define the syntax in normal form and use a
normalisation theorem. Since the normal forms do not require equality
constructors there is no need to force the type to be a set and hence
we could eliminate into any type. Indeed, this was proposed as a
possible solution to the coherence problem in HoTT (e.g. how to define
semi-simplicial types).  However, it seems likely that this is not
possible either. While we should be able to define the syntax of
normal forms without equations we will need to incorporate
normalisation. An example would be the rule for application for normal
forms:
\begin{spec}
_ $ _  :  Ne Γ (Π A B) → (u : Nf Γ A)
       →  Ne Γ (B [ < u > ]T)
\end{spec} %$
Here we assume that we mutually define normal |Nf| and neutral terms
|Ne| and that all the types are in normal form. However, a problem is
the substitution appearing in the result which has to substitute a
normal term into a normal type giving rise to a normal type. This
cannot be a constructor since then we would have to add equalities to
specify how substitution has to be behave. Hence we have to execute
the substitution and at the same time normalize the result (this is
known as hereditary substitution \cite{hereditary}). We may still think that this may
be challenging but possible using an inductive-recursive
definition. However, even in the simplest case, i.e. in a type theory
only with variables we have to prove equational properties of the
explicit substitution operation, which in turn appear in the proof
terms, leading to a coherence problem which we have so far failed to
solve.

Nicolai Kraus raised the question whether it may be possible to give
the interpretation of a strict model like the standard model (section
\ref{sec:standard}) with the truncation even though we do not
eliminate into a set. This is motivated by his work on general
eliminations for the truncation operator \cite{kraus:phd}. Following
this idea it may be possible to eliminate into set via an intermediate
definition which states all the necessary coherence equations.

While defining the internal type theory as a set in HoTT seems to be
of limited use, there are interesting applications in a 2-level theory
similar to HTS as proposed by Voevodsky \cite{hts}. While the original
proposal of HTS works in an extensional setting, it makes sense to
consider a 2-level theory in an intensional setting like Agda. We
start with a \emph{strict} type theory with uniqueness of identity
proofs (UIP) but within this we
introduce a HoTT universe. This universe comes with its own propositional
equality which is univalent but isn't proof-irrelevant. From this
equality we can only eliminate into types within the universe. We call
the types on the outside \emph{pretypes} and the types in the universe
\emph{types}. The construction of the type-theoretic syntax takes
place on the level of pretypes which is compatible with our assumption
of UIP. On the other hand we can eliminate into the HoTT
universe which is univalent. In this setting definitional equalities
are modelled by strict equality and propositional equality by the
univalent equality within the universe. Our definition of the syntax
takes place at the level of pretypes but when constructing specific
interpretations we eliminate into types.


% \subsection{Higher Inductive Types}

% In \cite{book} one of the main motivations for the use of HITs is to
% encode definitions from Homotopy theory in Type Theory. One of the
% first examples is the sphere, which we may express in idealized
% \footnote{In real Agda we can approximate this using a technique
%   developed by Dan Licata \cite{licata}.}  Agda
% \begin{spec}
% data S¹ : Set where
%   base : S¹
%   loop : base ≡ base  
% \end{spec}
% Here we specify a type which has one element |base| and one
% non-trivial equality proof |loop|.Here non-trivial means that it is
% not equal to |refl|. We can compose |loop| with itself using
% transitivity and we can turn it around using symmetry. However, we
% have that |trans p (sym p)| is equal to |refl|. And indeed it can be
% shown that the loop space |base ≡ base| is isomorphic (and hence equal
% in HoTT) to the integers. This proof has been verified in Agda and Coq
% \cite{book}.

% HoTT in Agda
% representating HITs -> Licata
% Inductive-inductive types
% eliminators


\section{Discussion and Further Work}
\label{sec:disc-furth-work}

We have for the first time presented a workable internal syntax of
dependent type theory which only features typed objects. We have shown that the
definition is feasible by constructing not only the standard model but
also the logical predicate interpretation. Further interpretations are
in preparation, e.g. the setoid interpretation and the presheaf
interpretation. The setoid interpretation is essential for a formal
justification of QITs and the presheaf interpretation is an essential
ingredient to extend normalisation by evaluation \cite{alti:ctcs95} to
dependent types. These constructions for dependent types require an
attention to detail which can only convincingly demonstrated by a
formal development. At the same time this approach would give us a
certified implementation of key algorithms such as normalisation. 

Clearly, we have only considered a very rudimentary type theory here,
mainly for reasons of space. It is quite straightforward to add other
type constructors, e.g. $\Sigma$-types, equality types, universes. 
We also would like to reflect our very general syntax for
inductive-inductive types and QITs but this is a more serious
challenge. 

Having an internal syntax of type theory opens up the exciting
possibility of developing \emph{template type theory}. We may define
an interpretation of type theory by defining an algebra for the syntax
and the interpretation of new constants in this algebra. We can then
interpret code using these new principles by interpreting it in the
given algebra. The new code can use all the conveniences of the host
system such as implicit arguments and definable syntactic
extensions. There are a number of exciting applications of this
approach: the use of presheaf models to justify guarded type theory
has already been mentioned \cite{forcing}. Another example is to model
the local state monad (Haskell's STM monad) in another presheaf
category to be able to program with and reason about local state and
other resources. In the extreme such a template type theory may allow
us to start with a fairly small core because everything else can be
programmed as templates. This may include the computational
explanation of Homotopy Type Theory by the cubical model --- we may
not have to build in univalence into our type theory.


% Template Type Theory, relate forcing models, etc. 
% Agda's untyped reflection API

% Thierry's cubical theory - no need for defining eliminators, we will
% be able to do pattern matching on equality constructors too
% Meta theory needs to be fixed
% potentiially simplify derivation
% pattern matching syntax

% \appendix
% \section{Appendix Title}

% This is the text of the appendix, if you need one.

\acks

%(excluded because of the blind review process)

Thanks to Frederik Forsberg for discussions related to various aspects
of the paper and joint work on the normal form problem of the pure
version of internal type theory. We would also like to thank Paolo
Capriotti, Gabe Dijkstra and Nicolai Kraus for discussions and work
related to the topic of this paper, in particular questions related to
HITs and coherence problems. We are also grateful to the anonymous
reviewers for their helpful comments and suggestions.

% We recommend abbrvnat bibliography style.

\bibliographystyle{abbrvnat}

% The bibliography should be embedded for final submission.

\bibliography{local,alti}

%\begin{thebibliography}{}
%\softraggedright
%
%\bibitem[Smith et~al.(2009)Smith, Jones]{smith02}
%P. Q. Smith, and X. Y. Jones. ...reference text...
%
%\end{thebibliography}

\end{document}
