{-# OPTIONS --without-K --no-eta #-}

module HoTT.Syntax where

open import lib

-- types of the syntax

data Con : Set
data Ty : Con → Set
data Tms : Con → Con → Set
data Tm : ∀ Γ → Ty Γ → Set

-- a congruence rule (corresponds to refl(TmΓ) in the cubical syntax:
-- Γ is in the context, we degenerate it)
TmΓ= : {Γ : Con}{A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
     → Tm Γ A₀ ≡ Tm Γ A₁
TmΓ= {Γ} p = ap (Tm Γ) p

-- constructors
-- note: we are using the categorical application rule and π₁, π₂

data Con where
  •     : Con  
  _,_   : (Γ : Con) → Ty Γ → Con

infixl 5 _,_

data Ty where
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
  Π     : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
  U     : ∀{Γ} → Ty Γ
  El    : ∀{Γ}(A : Tm Γ U) → Ty Γ

infixl 7 _[_]T



TmΓA[]= : ∀{Γ Δ}{A : Ty Δ}{ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
        → Tm Γ (A [ ρ₀ ]T) ≡ Tm Γ (A [ ρ₁ ]T)
TmΓA[]= refl = refl


data Tms where
  ε     : ∀{Γ} → Tms Γ •
  _,_   : ∀{Γ Δ}(δ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ δ ]T) → Tms Γ (Δ , A)
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ
  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ

infix 6 _∘_

data Tm where
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (δ : Tms Γ Δ) → Tm Γ (A [ δ ]T) 
  π₂    : ∀{Γ Δ}{A : Ty Δ}(δ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ δ ]T)
  app   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm Γ (Π A B) → Tm (Γ , A) B
  lam   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm (Γ , A) B → Tm Γ (Π A B)

infixl 8 _[_]t

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
wk = π₁ id

vz : ∀ {Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
vz = π₂ id

vs : ∀ {Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T) 
vs x = x [ wk ]t

-- higher constructors are postulated

postulate
   -- higher constructors for Ty
   [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
   [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → (A [ δ ]T) [ σ ]T ≡ A [ δ ∘ σ ]T
   U[]   : ∀{Γ Δ}{δ : Tms Γ Δ} → U [ δ ]T ≡ U
   El[]  : ∀{Γ Δ}{δ : Tms Γ Δ}{Â : Tm Δ U} → El Â [ δ ]T ≡ El (coe (TmΓ= U[]) (Â [ δ ]t))

_^_ : ∀{Γ Δ}(δ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ δ ]T) (Δ , A)
δ ^ A = (δ ∘ wk) , coe (TmΓ= [][]T) vz

postulate
   Π[]   : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
         → (Π A B) [ δ ]T ≡ Π (A [ δ ]T) (B [ δ ^ A ]T)
   setT  : ∀{Γ}{A B : Ty Γ}{e0 e1 : A ≡ B} → e0 ≡ e1

   -- higher constructors for Tms
   idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → id ∘ δ ≡ δ 
   idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → δ ∘ id ≡ δ 
   ass   : ∀{Δ Γ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Γ Σ}{ν : Tms Δ Γ}
         → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
   ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
         → (δ , a) ∘ σ ≡ (δ ∘ σ) , coe (TmΓ= [][]T) (a [ σ ]t)
   π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₁ (δ , a) ≡ δ
   πη    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)} → (π₁ δ , π₂ δ) ≡ δ
   εη    : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε
   sets  : ∀{Γ Δ}{δ σ : Tms Γ Δ}{e0 e1 : δ ≡ σ} → e0 ≡ e1

   -- higher constructors for Tm
   [id]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≡[ TmΓ= [id]T ]≡ t
   [][]t : ∀{Γ Δ Σ}{A : Ty Σ}{t : Tm Σ A}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
         → (t [ δ ]t) [ σ ]t ≡[ TmΓ= [][]T ]≡  t [ δ ∘ σ ]t
   π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
         → π₂ (δ , a) ≡[ TmΓ= (ap (_[_]T A) π₁β) ]≡ a
   lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B}
         → (lam t) [ δ ]t ≡[ TmΓ= Π[] ]≡ lam (t [ δ ^ A ]t)
   Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
         →  app (lam t) ≡ t
   Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
         → lam (app t) ≡ t
   sett  : ∀{Γ}{A : Ty Γ}{u v : Tm Γ A}{e0 e1 : u ≡ v} → e0 ≡ e1

-- some defined conversion rules

app= : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t₀ t₁ : Tm Γ (Π A B)} → t₀ ≡ t₁ → app t₀ ≡ app t₁
app= refl = refl

app[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm Δ (Π A B)}
      → app (coe (TmΓ= Π[]) (t [ δ ]t)) ≡ (app t) [ δ ^ A ]t
app[] {Γ}{Δ}{δ}{A}{B}{t}

  = app (coe (TmΓ= Π[]) (t [ δ ]t))
                                     ≡⟨ ap (λ z → app (coe (TmΓ= Π[]) (z [ δ ]t))) (Πη ⁻¹) ⟩
    app (coe (TmΓ= Π[]) ((lam (app t)) [ δ ]t))
                                     ≡⟨ ap app lam[] ⟩
    app (lam (app t [ δ ^ A ]t))
                                     ≡⟨ Πβ ⟩
    app t [ δ ^ A ]t
                                     ∎

π₁∘ : ∀{Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
     → π₁ δ ∘ ρ ≡ π₁ (δ ∘ ρ)
π₁∘ {Γ}{Δ}{Θ}{A}{δ}{ρ}
  = π₁β ⁻¹ ◾ ap π₁ (,∘ ⁻¹) ◾ ap (λ σ → π₁ (σ ∘ ρ)) πη

Π= : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){B₀ : Ty (Γ , A₀)}{B₁ : Ty (Γ , A₁)}
     (B₂ : B₀ ≡[ ap Ty (ap (_,_ Γ) A₂) ]≡ B₁)
   → Π A₀ B₀ ≡ Π A₁ B₁
Π= refl refl = refl

<_> : ∀ {Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
< t > = id , coe (TmΓ= ([id]T ⁻¹)) t

_$_ : ∀ {Γ}{A : Ty Γ}{B : Ty (Γ , A)} → (t : Tm Γ (Π A B)) → (u : Tm Γ A) → Tm Γ (B [ < u > ]T)
t $ u = (app t) [ < u > ]t

-- we can prove that Con is a set, no need to postulate a higher constructor
setC : {Γ Δ : Con}{e0 e1 : Γ ≡ Δ} → e0 ≡ e1
setC {•}     {•}    {refl}{refl} = refl
setC {•}     {Δ , A}{()}
setC {Γ , A} {•}    {()}
setC {Γ , A} {Δ , B}{e0}  {e1}   = ,=η e0 ◾ ,== α β ◾ ,=η e1 ⁻¹
  where

    ,= : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ}(p : Γ ≡ Δ)(q : A ≡[ ap Ty p ]≡ B)
       → _≡_ {A = Con} (Γ , A) (Δ , B)
    ,= refl refl = refl

    ,=0 : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ} → _≡_ {A = Con} (Γ , A) (Δ , B) → Γ ≡ Δ
    ,=0 refl = refl

    ,=1 : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ} → (p : _≡_ {A = Con} (Γ , A) (Δ , B)) → A ≡[ ap Ty (,=0 p) ]≡ B
    ,=1 refl = refl

    ,=η : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ} → (p : _≡_ {A = Con} (Γ , A) (Δ , B)) → p ≡ ,= (,=0 p) (,=1 p)
    ,=η refl = refl

    ,=β0 : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ}(p : Γ ≡ Δ)(q : A ≡[ ap Ty p ]≡ B) → p ≡ ,=0 (,= p q)
    ,=β0 refl refl = refl

    ,=β1 : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ}(p : Γ ≡ Δ)(q : A ≡[ ap Ty p ]≡ B)
         → q ≡[ ap (λ z → A ≡[ ap Ty z ]≡ B) (,=β0 p q) ]≡ ,=1 (,= p q) 
    ,=β1 refl refl = refl

    α : ,=0 e0 ≡ ,=0 e1
    α = setC {Γ}{Δ}{,=0 e0}{,=0 e1}

    β : ,=1 e0 ≡[ ap (λ z → A ≡[ ap Ty z ]≡ B) α ]≡ ,=1 e1
    β = setT {Δ}{coe (ap Ty (,=0 e1)) A}{B}{coe (ap (λ z → A ≡[ ap Ty z ]≡ B) α) (,=1 e0)}{,=1 e1}

    ,== : {Γ Δ : Con}{A : Ty Γ}{B : Ty Δ}{p p' : Γ ≡ Δ}{q : A ≡[ ap Ty p ]≡ B}{q' : A ≡[ ap Ty p' ]≡ B}
        (α : p ≡ p')(β : q ≡[ ap (λ z → A ≡[ ap Ty z ]≡ B) α ]≡ q') → ,= p q ≡ ,= p' q'
    ,== refl refl = refl
