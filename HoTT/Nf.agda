{-# OPTIONS --without-K #-} 

module HoTT.Nf where

open import Agda.Primitive
open import lib

subst : ∀{i j}{A : Set i}(P : A → Set j){a a' : A}(p : a ≡ a') → P a → P a'
subst P refl u = u

trans = _◾_

cong = ap

sym = _⁻¹

cong₂d : ∀ {a b c} {A : Set a} {B : A -> Set b} {C : Set c}
        (f : (x : A) → B x → C) {x y u v} → (p : x ≡ y) → subst B p u ≡ v → f x u ≡ f y v
cong₂d f refl refl = refl

data Con : Set
data Ty : Con → Set
data Var : ∀ Γ → Ty Γ → Set
data Vars : Con → Con → Set

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con)(A : Ty Γ) → Con

data Ty where
  U     : ∀{Γ} → Ty Γ
  El    : ∀{Γ}(A : Var Γ U) → Ty Γ 

_[_]T : ∀{Γ Δ} → Ty Δ → Vars Γ Δ → Ty Γ
_[_]v : ∀{Γ Δ A} → Var Δ A → (δ : Vars Γ Δ) → Var Γ (A [ δ ]T) 



data Vars where
  ε     : ∀{Γ} → Vars Γ •
  _,_   : ∀{Γ Δ}(δ : Vars Γ Δ){A : Ty Δ} → Var Γ (A [ δ ]T) → Vars Γ (Δ , A)

wk : ∀{Γ}{A : Ty Γ} → Vars (Γ , A) Γ

data Var where
  vz : ∀ {Γ}{A : Ty Γ} → Var (Γ , A) (A [ wk ]T)
  vs : ∀ {Γ}{A B : Ty Γ} → Var Γ A → Var (Γ , B) (A [ wk ]T) 


_●_ : ∀ {Γ Δ E} -> Vars Δ E -> Vars Γ Δ -> Vars Γ E
{-# TERMINATING #-}
●T-assoc : ∀ {Γ Δ E} →  (g : Vars Γ Δ) -> (f : Vars Δ E) ->
        ∀ {A} → ((A [ f ]T) [ g ]T) ≡ (A [ f ● g ]T)

●v-assoc : ∀ {Γ Δ E} →  (g : Vars Γ Δ) -> (f : Vars Δ E) ->
        ∀ {A x} → _[_]v {A = A} x (f ● g)
                            ≡ subst (Var Γ) (●T-assoc g f {A}) ((x [ f ]v) [ g ]v)


π₁ : ∀{Γ Δ}{A : Ty Δ} → Vars Γ (Δ , A) →  Vars Γ Δ
π₁ (g , x) = g


ε ● zs = ε
(ys , y) ● zs = (ys ● zs) , subst (Var _) (●T-assoc zs ys) (y [ zs ]v)

U [ δ ]T = U
El A [ δ ]T = El (A [ δ ]v)

ids : ∀ {Γ} -> Vars Γ Γ
vs* : ∀ {Γ Δ A} → Vars Γ Δ -> Vars (Γ , A) Δ
vs*-natural : ∀ {Δ Δ' Γ A} → (ys : Vars Γ Δ) -> (zs : Vars Δ Δ') ->
           (zs ● vs* {A = A} ys) ≡ vs* (zs ● ys)
vs*-β : ∀ {Δ Γ Γ'}{A : Ty Γ} (ys : Vars Δ Γ)(y : Var Δ (A [ ys ]T))(zs : Vars Γ Γ') -> (zs ● ys) ≡ (vs* zs ● (ys , y))
ids-l-unit : ∀ {Γ Δ} -> (zs : Vars Δ Γ) -> zs ≡ (ids ● zs)
ids-r-unit : ∀ {Γ Δ} -> (ys : Vars Γ Δ) -> (ys ● ids) ≡ ys

wk = vs* ids

vs* ε = ε
vs* {A = A} (_,_ {Γ} ys {A = B}  y) = vs* ys , subst (Var (Γ , A)) (trans (●T-assoc wk ys) (cong (λ z → B [ z ]T) (trans (vs*-natural ids ys) (cong vs* (ids-r-unit ys))))) (vs y)

ids {•} = ε
ids {Γ , A} = vs* ids , vz

vz [ _,_ {Γ} ys {A = A} y ]v = subst (Var Γ) (sym (●T-assoc (ys , y) wk)) (subst (λ w → Var Γ (A [ w ]T)) (trans (ids-l-unit ys) (vs*-β ys y ids)) y)
vs {A = A} x [ _,_ {Γ} ys  y ]v = subst (Var _) (sym (●T-assoc (ys , y) wk)) (subst (λ w → Var Γ (A [ w ]T)) (trans (ids-l-unit ys) (vs*-β ys y ids)) (x [ ys ]v))




ids-r-unit ε = refl
ids-r-unit {Γ} (_,_ ys {A = A} y) = cong₂d (λ a b → a , b) {ys ● ids} {ys} {subst (Var Γ) (●T-assoc ids ys) (y [ ids ]v)} {y} (ids-r-unit ys) {!!}



ids-l-unit ε = refl
ids-l-unit (_,_ {Δ} ys {A} y) = cong₂d (λ a b → a , b) {ys} {wk ● (ys , y)} {y} {subst (Var _) (●T-assoc (ys , y) (vs* ids))
       (subst (Var Δ) (sym (●T-assoc (ys , y) wk))
        (subst (λ w → Var Δ (A [ w ]T))
         (trans (ids-l-unit ys) (vs*-β ys y ids)) y))} (trans (ids-l-unit ys) (vs*-β ys y ids)) {!!}

vs*-natural ys ε = refl
vs*-natural {Γ} ys (zs , z) = {!!}

vs*-β ys y ε = refl
vs*-β ys y (zs , z) = {!!}

●T-assoc f g {U} = refl
●T-assoc f g {El A} = cong El (sym (●v-assoc f g {x = A}))


●v-assoc f (_,_ ys {A} y) {x = vz} = {!!}
●v-assoc f g {x = vs x} = {!●v-assoc f g!}
