{-# OPTIONS --without-K --no-eta #-}

module HoTT.Rec where

open import lib
open import HoTT.Syntax

record Motives : Set₁ where
  field
    Conᴹ : Set
    Tyᴹ  : Conᴹ → Set
    Tmsᴹ : Conᴹ → Conᴹ → Set
    Tmᴹ  : (Γᴹ : Conᴹ) → Tyᴹ Γᴹ → Set

  TmΓᴹ= : {Γᴹ : Conᴹ}{A₀ᴹ : Tyᴹ Γᴹ}{A₁ᴹ : Tyᴹ Γᴹ}(A₂ᴹ : A₀ᴹ ≡ A₁ᴹ)
        → Tmᴹ Γᴹ A₀ᴹ ≡ Tmᴹ Γᴹ A₁ᴹ
  TmΓᴹ= refl = refl

record Methods (M : Motives) : Set₁ where
  open Motives M
  field
    -- methods for Con
    •ᴹ     : Conᴹ
    _,Cᴹ_  : (Γᴹ : Conᴹ) → Tyᴹ Γᴹ → Conᴹ

    -- methods for Ty
    _[_]Tᴹ : ∀{Γᴹ Δᴹ} → Tyᴹ Δᴹ → Tmsᴹ Γᴹ Δᴹ → Tyᴹ Γᴹ
    Uᴹ     : ∀{Γᴹ} → Tyᴹ Γᴹ
    Πᴹ     : ∀{Γᴹ}(Aᴹ : Tyᴹ Γᴹ)(Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ)) → Tyᴹ Γᴹ
    Elᴹ    : ∀{Γᴹ}(Âᴹ : Tmᴹ Γᴹ Uᴹ) → Tyᴹ Γᴹ

    -- methods for Tms
    εᴹ     : ∀{Γᴹ} → Tmsᴹ Γᴹ •ᴹ
    _,sᴹ_  : ∀{Γᴹ Δᴹ}(δᴹ : Tmsᴹ Γᴹ Δᴹ){Aᴹ : Tyᴹ Δᴹ} → Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ)
           → Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ)
    idᴹ    : ∀{Γᴹ} → Tmsᴹ Γᴹ Γᴹ
    _∘ᴹ_   : ∀{Γᴹ Δᴹ Σᴹ} → Tmsᴹ Δᴹ Σᴹ → Tmsᴹ Γᴹ Δᴹ → Tmsᴹ Γᴹ Σᴹ
    π₁ᴹ    : ∀{Γᴹ Δᴹ}{Aᴹ : Tyᴹ Δᴹ} → Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) → Tmsᴹ Γᴹ Δᴹ

    -- methods for Tm
    _[_]tᴹ : ∀{Γᴹ Δᴹ}{Aᴹ : Tyᴹ Δᴹ} → Tmᴹ Δᴹ Aᴹ → (δᴹ : Tmsᴹ Γᴹ Δᴹ) → Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) 
    π₂ᴹ    : ∀{Γᴹ Δᴹ}{Aᴹ : Tyᴹ Δᴹ}(δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ)) → Tmᴹ Γᴹ (Aᴹ [ π₁ᴹ δᴹ ]Tᴹ)
    appᴹ   : ∀{Γᴹ}{Aᴹ : Tyᴹ Γᴹ}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ)} → Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) → Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ
    lamᴹ   : ∀{Γᴹ}{Aᴹ : Tyᴹ Γᴹ}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ)} → Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ → Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ)

  -- a congruence rule
  _[_]Tᴹ= : {Γᴹ Δᴹ : Conᴹ}(Aᴹ : Tyᴹ Δᴹ)
           {δ₀ᴹ δ₁ᴹ : Tmsᴹ Γᴹ Δᴹ}(δ₂ᴹ : δ₀ᴹ ≡ δ₁ᴹ)
         → Aᴹ [ δ₀ᴹ ]Tᴹ ≡ Aᴹ [ δ₁ᴹ ]Tᴹ
  _[_]Tᴹ= = λ { Aᴹ refl → refl }

  infixl 5 _,Cᴹ_
  infixl 7 _[_]Tᴹ
  infixl 5 _,sᴹ_
  infix 6 _∘ᴹ_
  infixl 8 _[_]tᴹ

  field
    -- higher methods for Ty
    [id]Tᴹ : ∀{Γᴹ}{Aᴹ : Tyᴹ Γᴹ} → Aᴹ [ idᴹ ]Tᴹ ≡ Aᴹ
    [][]Tᴹ : ∀{Γᴹ Δᴹ Σᴹ}{Aᴹ : Tyᴹ Σᴹ}{σᴹ : Tmsᴹ Γᴹ Δᴹ}{δᴹ : Tmsᴹ Δᴹ Σᴹ}
           → (Aᴹ [ δᴹ ]Tᴹ) [ σᴹ ]Tᴹ ≡ Aᴹ [ δᴹ ∘ᴹ σᴹ ]Tᴹ
    U[]ᴹ   : ∀{Γᴹ Δᴹ}{δᴹ : Tmsᴹ Γᴹ Δᴹ} → Uᴹ [ δᴹ ]Tᴹ ≡ Uᴹ
    El[]ᴹ  : ∀{Γᴹ Δᴹ}{δᴹ : Tmsᴹ Γᴹ Δᴹ}{Âᴹ : Tmᴹ Δᴹ Uᴹ}
           → Elᴹ Âᴹ [ δᴹ ]Tᴹ ≡ Elᴹ (coe (TmΓᴹ= U[]ᴹ) (Âᴹ [ δᴹ ]tᴹ))

  _^ᴹ_ : ∀{Γᴹ Δᴹ}(δᴹ : Tmsᴹ Γᴹ Δᴹ)(Aᴹ : Tyᴹ Δᴹ) → Tmsᴹ (Γᴹ ,Cᴹ Aᴹ [ δᴹ ]Tᴹ) (Δᴹ ,Cᴹ Aᴹ)
  _^ᴹ_ = λ δᴹ Aᴹ → (δᴹ ∘ᴹ π₁ᴹ idᴹ) ,sᴹ coe (TmΓᴹ= [][]Tᴹ) (π₂ᴹ idᴹ)

  field
    Π[]ᴹ   : ∀{Γᴹ Δᴹ}{δᴹ : Tmsᴹ Γᴹ Δᴹ}{Aᴹ : Tyᴹ Δᴹ}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ)}
           → (Πᴹ Aᴹ Bᴹ) [ δᴹ ]Tᴹ ≡ Πᴹ (Aᴹ [ δᴹ ]Tᴹ) (Bᴹ [ δᴹ ^ᴹ Aᴹ ]Tᴹ)
    setTᴹ  : ∀{Γᴹ}{Aᴹ Bᴹ : Tyᴹ Γᴹ}{e0ᴹ e1ᴹ : Aᴹ ≡ Bᴹ} → e0ᴹ ≡ e1ᴹ
  
    -- higher methods for Tms
    idlᴹ   : ∀{Γᴹ Δᴹ}{δᴹ : Tmsᴹ Γᴹ Δᴹ} → idᴹ ∘ᴹ δᴹ ≡ δᴹ
    idrᴹ   : ∀{Γᴹ Δᴹ}{δᴹ : Tmsᴹ Γᴹ Δᴹ} → δᴹ ∘ᴹ idᴹ ≡ δᴹ
    assᴹ   : ∀{Δᴹ Γᴹ Σᴹ Ωᴹ}{σᴹ : Tmsᴹ Σᴹ Ωᴹ}{δᴹ : Tmsᴹ Γᴹ Σᴹ}{νᴹ : Tmsᴹ Δᴹ Γᴹ}
           → (σᴹ ∘ᴹ δᴹ) ∘ᴹ νᴹ ≡ σᴹ ∘ᴹ (δᴹ ∘ᴹ νᴹ)
    ,∘ᴹ    : ∀{Γᴹ Δᴹ Σᴹ}{δᴹ : Tmsᴹ Γᴹ Δᴹ}{σᴹ : Tmsᴹ Σᴹ Γᴹ}{Aᴹ : Tyᴹ Δᴹ}
             {aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ)}
           → (δᴹ ,sᴹ aᴹ) ∘ᴹ σᴹ ≡ (δᴹ ∘ᴹ σᴹ) ,sᴹ coe (TmΓᴹ= [][]Tᴹ) (aᴹ [ σᴹ ]tᴹ)
    π₁βᴹ   : ∀{Γᴹ Δᴹ}{Aᴹ : Tyᴹ Δᴹ}{δᴹ : Tmsᴹ Γᴹ Δᴹ}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ)}
           → π₁ᴹ (δᴹ ,sᴹ aᴹ) ≡ δᴹ
    πηᴹ    : ∀{Γᴹ Δᴹ}{Aᴹ : Tyᴹ Δᴹ}{δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ)}
           → (π₁ᴹ δᴹ ,sᴹ π₂ᴹ δᴹ) ≡ δᴹ
    εηᴹ    : ∀{Γᴹ}{σᴹ : Tmsᴹ Γᴹ •ᴹ} → σᴹ ≡ εᴹ
    setsᴹ  : ∀{Γᴹ Δᴹ}{δᴹ σᴹ : Tmsᴹ Γᴹ Δᴹ}{e0ᴹ e1ᴹ : δᴹ ≡ σᴹ} → e0ᴹ ≡ e1ᴹ

    -- higher methods for Tm
    [id]tᴹ : ∀{Γᴹ}{Aᴹ : Tyᴹ Γᴹ}{tᴹ : Tmᴹ Γᴹ Aᴹ}
           → tᴹ [ idᴹ ]tᴹ ≡[ TmΓᴹ= [id]Tᴹ ]≡ tᴹ
    [][]tᴹ : ∀{Γᴹ Δᴹ Σᴹ}{Aᴹ : Tyᴹ Σᴹ}{tᴹ : Tmᴹ Σᴹ Aᴹ}
             {σᴹ : Tmsᴹ Γᴹ Δᴹ}{δᴹ : Tmsᴹ Δᴹ Σᴹ}
           → (tᴹ [ δᴹ ]tᴹ) [ σᴹ ]tᴹ ≡[ TmΓᴹ= [][]Tᴹ ]≡  tᴹ [ δᴹ ∘ᴹ σᴹ ]tᴹ
    π₂βᴹ   : ∀{Γᴹ Δᴹ}{Aᴹ : Tyᴹ Δᴹ}{δᴹ : Tmsᴹ Γᴹ Δᴹ}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ)}
           → π₂ᴹ (δᴹ ,sᴹ aᴹ) ≡[ TmΓᴹ= (Aᴹ [ π₁βᴹ ]Tᴹ=) ]≡ aᴹ
    lam[]ᴹ : ∀{Γᴹ Δᴹ}{δᴹ : Tmsᴹ Γᴹ Δᴹ}{Aᴹ : Tyᴹ Δᴹ}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ)}
             {tᴹ : Tmᴹ (Δᴹ ,Cᴹ Aᴹ) Bᴹ}                       
           → (lamᴹ tᴹ) [ δᴹ ]tᴹ ≡[ TmΓᴹ= Π[]ᴹ ]≡ lamᴹ (tᴹ [ δᴹ ^ᴹ Aᴹ ]tᴹ)
    Πβᴹ    : ∀{Γᴹ}{Aᴹ : Tyᴹ Γᴹ}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ)}{tᴹ : Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ}
           →  appᴹ (lamᴹ tᴹ) ≡ tᴹ
    Πηᴹ    : ∀{Γᴹ}{Aᴹ : Tyᴹ Γᴹ}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ)}{tᴹ : Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ)}
           → lamᴹ (appᴹ tᴹ) ≡ tᴹ
    settᴹ  : ∀{Γᴹ}{Aᴹ : Tyᴹ Γᴹ}{uᴹ vᴹ : Tmᴹ Γᴹ Aᴹ}{e0ᴹ e1ᴹ : uᴹ ≡ vᴹ} → e0ᴹ ≡ e1ᴹ
    
module rec (M : Motives)(m : Methods M) where

  open Motives M
  open Methods m

  RecCon : Con → Conᴹ
  RecTy  : ∀{Γ}(A : Ty Γ) → Tyᴹ (RecCon Γ)
  RecTms : ∀{Γ Δ}(δ : Tms Γ Δ) → Tmsᴹ (RecCon Γ) (RecCon Δ)
  RecTm  : ∀{Γ}{A : Ty Γ}(t : Tm Γ A) → Tmᴹ (RecCon Γ) (RecTy A)

  RecCon •       = •ᴹ
  RecCon (Γ , A) = RecCon Γ ,Cᴹ RecTy A

  RecTy (A [ δ ]T) = RecTy A [ RecTms δ ]Tᴹ
  RecTy U          = Uᴹ
  RecTy (El Â)     = Elᴹ (RecTm Â)
  RecTy (Π A B)    = Πᴹ (RecTy A) (RecTy B)

  RecTms ε       = εᴹ
  RecTms (δ , t) = RecTms δ ,sᴹ RecTm t
  RecTms id      = idᴹ
  RecTms (δ ∘ σ) = RecTms δ ∘ᴹ RecTms σ
  RecTms (π₁ δ)  = π₁ᴹ (RecTms δ)

  RecTm (t [ δ ]t) = RecTm t [ RecTms δ ]tᴹ
  RecTm (π₂ δ)     = π₂ᴹ (RecTms δ)
  RecTm (app t)    = appᴹ (RecTm t)
  RecTm (lam t)    = lamᴹ (RecTm t)
