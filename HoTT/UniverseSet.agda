{-# OPTIONS --without-K --no-eta #-}

-- in this module we define the inductive-recursive universe UU,EL
-- which is a set

module HoTT.UniverseSet where

open import lib

------------------------------------------------------------------------------
-- the inductive-recursive universe
------------------------------------------------------------------------------

data UU : Set
EL : UU → Set

data UU where
  ′Π′ : (A : UU) → (EL A → UU) → UU -- \'
  ′Σ′ : (A : UU) → (EL A → UU) → UU
  ′⊤′ : UU

EL (′Π′ A B) =   (x : EL A) → EL (B x)
EL (′Σ′ A B) = Σ (EL A) λ x → EL (B x)
EL ′⊤′ = ⊤

------------------------------------------------------------------------------
-- equality for ′Π′ and ′Σ′
------------------------------------------------------------------------------

′Π′= : {A A' : UU}{B : EL A → UU}{B' : EL A' → UU}
      → (w : A ≡ A') → B ≡[ ap (λ A → EL A → UU) w ]≡ B' → ′Π′ A B ≡ ′Π′ A' B'
′Π′= refl refl = refl

′Π′=0 : {A A' : UU}{B : EL A → UU}{B' : EL A' → UU} → ′Π′ A B ≡ ′Π′ A' B' → A ≡ A'
′Π′=0 refl = refl

′Π′=1 : {A A' : UU}{B : EL A → UU}{B' : EL A' → UU}(w : ′Π′ A B ≡ ′Π′ A' B')
      → B ≡[ ap (λ A → EL A → UU) (′Π′=0 w) ]≡ B'
′Π′=1 refl = refl

′Π′=η : {A A' : UU}{B : EL A → UU}{B' : EL A' → UU}(α : ′Π′ A B ≡ ′Π′ A' B')
       → α ≡ ′Π′= (′Π′=0 α) (′Π′=1 α)
′Π′=η refl = refl

′Π′=β0 : {A A' : UU}{B : EL A → UU}{B' : EL A' → UU}
          (α : A ≡ A')(β : B ≡[ ap (λ X → EL X → UU) α ]≡ B')
        → ′Π′=0 (′Π′= α β) ≡ α
′Π′=β0 refl refl = refl

′Π′=β1 : {A A' : UU}{B : EL A → UU}{B' : EL A' → UU}
          (α : A ≡ A')(β : B ≡[ ap (λ X → EL X → UU) α ]≡ B')
        → ′Π′=1 (′Π′= α β) ≡[ ap (λ γ → B ≡[ ap (λ X → EL X → UU) γ ]≡ B') (′Π′=β0 α β) ]≡ β
′Π′=β1 refl refl = refl

′Σ′= : {A A' : UU}{B : EL A → UU}{B' : EL A' → UU}
      → (w : A ≡ A') → B ≡[ ap (λ A → EL A → UU) w ]≡ B' → ′Σ′ A B ≡ ′Σ′ A' B'
′Σ′= refl refl = refl

′Σ′=0 : {A A' : UU}{B : EL A → UU}{B' : EL A' → UU} → ′Σ′ A B ≡ ′Σ′ A' B' → A ≡ A'
′Σ′=0 refl = refl

′Σ′=1 : {A A' : UU}{B : EL A → UU}{B' : EL A' → UU}(w : ′Σ′ A B ≡ ′Σ′ A' B')
      → B ≡[ ap (λ A → EL A → UU) (′Σ′=0 w) ]≡ B'
′Σ′=1 refl = refl

′Σ′=η : {A A' : UU}{B : EL A → UU}{B' : EL A' → UU}(α : ′Σ′ A B ≡ ′Σ′ A' B')
       → α ≡ ′Σ′= (′Σ′=0 α) (′Σ′=1 α)
′Σ′=η refl = refl

′Σ′=β0 : {A A' : UU}{B : EL A → UU}{B' : EL A' → UU}
          (α : A ≡ A')(β : B ≡[ ap (λ X → EL X → UU) α ]≡ B')
        → ′Σ′=0 (′Σ′= α β) ≡ α
′Σ′=β0 refl refl = refl

′Σ′=β1 : {A A' : UU}{B : EL A → UU}{B' : EL A' → UU}
          (α : A ≡ A')(β : B ≡[ ap (λ X → EL X → UU) α ]≡ B')
        → ′Σ′=1 (′Σ′= α β) ≡[ ap (λ γ → B ≡[ ap (λ X → EL X → UU) γ ]≡ B') (′Σ′=β0 α β) ]≡ β
′Σ′=β1 refl refl = refl

------------------------------------------------------------------------------
-- strong functional extensionality
------------------------------------------------------------------------------

postulate
  fun= : {A : Set}{B : A → Set}{f f' : (x : A) → B x} → (∀ x → f x ≡ f' x) → f ≡ f'

fun=elim : {A : Set}{B : A → Set}{f f' : (x : A) → B x} → f ≡ f' → (∀ x → f x ≡ f' x)
fun=elim refl x = refl

postulate -- TODO: derive this from fun=
  fun=η : {A : Set}{B : A → Set}{f f' : (x : A) → B x} → (w : f ≡ f') → fun= (fun=elim w) ≡ w

------------------------------------------------------------------------------
-- proof that UU and EL are sets
------------------------------------------------------------------------------

′Π′=2 : {A : UU}{B : EL A → UU}{α : A ≡ A}{β : B ≡[ ap (λ X → EL X → UU) α ]≡ B}
      → (w : α ≡ refl) → β ≡[ ap (λ γ → B ≡[ ap (λ X → EL X → UU) γ ]≡ B) w ]≡ refl
      → ′Π′= {A}{A}{B}{B} α β ≡ refl
′Π′=2 refl refl = refl

′Σ′=2 : {A : UU}{B : EL A → UU}{α : A ≡ A}{β : B ≡[ ap (λ X → EL X → UU) α ]≡ B}
      → (w : α ≡ refl) → β ≡[ ap (λ γ → B ≡[ ap (λ X → EL X → UU) γ ]≡ B) w ]≡ refl
      → ′Σ′= {A}{A}{B}{B} α β ≡ refl
′Σ′=2 refl refl = refl

Σ=2 : {A : Set}{B : A → Set}{a : A}{b : B a}
      {α : a ≡ a}{β : b ≡[ ap B α ]≡ b}
    → (w : α ≡ refl) → β ≡[ ap (λ γ → b ≡[ ap B γ ]≡ b) w ]≡ refl
    → ,Σ= α β ≡ refl
Σ=2 refl refl = refl

fun=2 : {A : Set}{B : A → Set}{f : (x : A) → B x}
      → ((x : A)(p : f x ≡ f x) → p ≡ refl) → (q : f ≡ f) → q ≡ refl
fun=2 w q = coe (ap (λ x → x ≡ refl)
                  (fun=η q))
                  (coe (ap (λ x → fun= (fun=elim q) ≡ x)
                         (fun=η refl))
                         (ap fun= (fun= (λ x → w x (fun=elim q x)))))

setUU' : {A : UU}{α : A ≡ A} → α ≡ refl
setUU' {′Π′ A B}{α} = ′Π′=η α ◾
                      (′Π′=2 (setUU' {A} {′Π′=0 α})
                             (fun=2 (λ x p → setUU' {B x} {p}) _))
setUU' {′Σ′ A B}{α} = ′Σ′=η α ◾
                      (′Σ′=2 (setUU' {A} {′Σ′=0 α})
                             (fun=2 (λ x p → setUU' {B x} {p}) _))
setUU' {′⊤′} {refl} = refl

setEL' : {A : UU}{a : EL A}{α : a ≡ a} → α ≡ refl
setEL' {′Π′ A B}{f}{α} = fun=2 (λ x p → setEL' {B x}{f x}{p}) α
setEL' {′Σ′ A B}{(a ,Σ b)}{α}
  = (,Σ=η α ⁻¹) ◾
    (Σ=2 (setEL' {A}{a}{,Σ=0 α})
         (setEL' {B a}
                 {b}
                 {coe (ap (λ r → b ≡[ ap (λ x → EL (B x)) r ]≡ b)
                        (setEL' {A}{a}))
                        (,Σ=1 α)}))
setEL' {′⊤′} {tt} {refl} = refl

setUU : {A B : UU}{α β : A ≡ B} → α ≡ β
setUU {α = α}{β = refl} = setUU' {α = α}

setEL : {A : UU}{a a' : EL A}{α β : a ≡ a'} → α ≡ β
setEL {α = α}{β = refl} = setEL' {α = α}

setCodomain : {A : Set}{B : A → Set}(w : {a : A}{b : B a}{α : b ≡ b} → α ≡ refl)
            → {f g : (x : A) → B x}{α β : f ≡ g} → α ≡ β
setCodomain w {α = α}{refl} = fun=2 (λ x p → w {α = p}) α
