{-# OPTIONS --without-K --no-eta #-}

module HoTT.Elim where

open import lib
open import HoTT.Syntax

record Motives : Set₁ where
  field
    Conᴹ : Con → Set
    Tyᴹ  : ∀{Γ} → Conᴹ Γ → Ty Γ → Set
    Tmsᴹ : ∀{Γ Δ} → Conᴹ Γ → Conᴹ Δ → Tms Γ Δ → Set
    Tmᴹ  : ∀{Γ}(Γᴹ : Conᴹ Γ) → {A : Ty Γ} → Tyᴹ Γᴹ A → Tm Γ A → Set

  -- congruence rules

  TyΓᴹ= : {Γ : Con}{Γᴹ : Conᴹ Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
        → Tyᴹ Γᴹ A₀ ≡ Tyᴹ Γᴹ A₁
  TyΓᴹ= refl = refl

  ~TyΓᴹ== : {Γ : Con}{Γᴹ : Conᴹ Γ}
            {A₀ A₁ : Ty Γ}(A₂₀ A₂₁ : A₀ ≡ A₁)(A₂₂ : A₂₀ ≡ A₂₁)
            {A₀ᴹ : Tyᴹ Γᴹ A₀}{A₁ᴹ : Tyᴹ Γᴹ A₁}
          → (A₀ᴹ ≡[ TyΓᴹ= A₂₀ ]≡ A₁ᴹ) ≡ (A₀ᴹ ≡[ TyΓᴹ= A₂₁ ]≡ A₁ᴹ)
  ~TyΓᴹ== p (.p) refl = refl

  TmsΓΔᴹ= : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
            {δ₀ δ₁ : Tms Γ Δ}(δ₂ : δ₀ ≡ δ₁)
          → Tmsᴹ Γᴹ Δᴹ δ₀ ≡ Tmsᴹ Γᴹ Δᴹ δ₁
  TmsΓΔᴹ= refl = refl

  ~TmsΓΔᴹ== : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
              {δ₀ δ₁ : Tms Γ Δ}(δ₂₀ δ₂₁ : δ₀ ≡ δ₁)(δ₂₂ : δ₂₀ ≡ δ₂₁)
              {δ₀ᴹ : Tmsᴹ Γᴹ Δᴹ δ₀}{δ₁ᴹ : Tmsᴹ Γᴹ Δᴹ δ₁}
            → (δ₀ᴹ ≡[ TmsΓΔᴹ= δ₂₀ ]≡ δ₁ᴹ) ≡ (δ₀ᴹ ≡[ TmsΓΔᴹ= δ₂₁ ]≡ δ₁ᴹ)
  ~TmsΓΔᴹ== p (.p) refl = refl

  TmΓᴹ= : {Γ : Con}{Γᴹ : Conᴹ Γ}
          {A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
          {Aᴹ₀ : Tyᴹ Γᴹ A₀}{Aᴹ₁ : Tyᴹ Γᴹ A₁}(Aᴹ₂ : Aᴹ₀ ≡[ TyΓᴹ= A₂ ]≡ Aᴹ₁)
          {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}(t₂ : t₀ ≡[ TmΓ= A₂ ]≡ t₁)
        → Tmᴹ Γᴹ Aᴹ₀ t₀ ≡ Tmᴹ Γᴹ Aᴹ₁ t₁
  TmΓᴹ= refl refl refl = refl

  TmΓAᴹ= : {Γ : Con}{Γᴹ : Conᴹ Γ}
           {A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
           {t₀ t₁ : Tm Γ A}(t₂ : t₀ ≡ t₁)
         → Tmᴹ Γᴹ Aᴹ t₀ ≡ Tmᴹ Γᴹ Aᴹ t₁
  TmΓAᴹ= refl = refl

  ~TmΓAᴹ== : {Γ : Con}{Γᴹ : Conᴹ Γ}
             {A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
             {t₀ t₁ : Tm Γ A}(t₂₀ t₂₁ : t₀ ≡ t₁)(t₂₂ : t₂₀ ≡ t₂₁)
             {t₀ᴹ : Tmᴹ Γᴹ Aᴹ t₀}{t₁ᴹ : Tmᴹ Γᴹ Aᴹ t₁}
           → (t₀ᴹ ≡[ TmΓAᴹ= t₂₀ ]≡ t₁ᴹ) ≡(t₀ᴹ ≡[ TmΓAᴹ= t₂₁ ]≡ t₁ᴹ)
  ~TmΓAᴹ== p (.p) refl = refl

record MethodsCon (M : Motives) : Set₁ where
  open Motives M
  field
    •ᴹ     : Conᴹ •
    _,Cᴹ_  : ∀{Γ}(Γᴹ : Conᴹ Γ){A : Ty Γ} → Tyᴹ Γᴹ A → Conᴹ (Γ , A)
    
  infixl 4 _,Cᴹ_

record MethodsTy (M : Motives)(mCon : MethodsCon M) : Set₁ where
  open Motives    M
  open MethodsCon mCon
  field
    _[_]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A)
             {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ) → Tyᴹ Γᴹ (A [ δ ]T)
    Uᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ} → Tyᴹ Γᴹ U
    Elᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Â : Tm Γ U}(Âᴹ : Tmᴹ Γᴹ Uᴹ Â) → Tyᴹ Γᴹ (El Â)
    Πᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A){B : Ty (Γ , A)}
             (Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B) → Tyᴹ Γᴹ (Π A B)

  infixl 7 _[_]Tᴹ

record MethodsTms (M : Motives)(mCon : MethodsCon M)(mTy : MethodsTy M mCon) : Set₁ where
  open Motives    M
  open MethodsCon mCon
  open MethodsTy  mTy
  field
    εᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ} → Tmsᴹ Γᴹ •ᴹ ε
    _,sᴹ_  : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
             {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{t : Tm Γ (A [ δ ]T)}(tᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) t)
           → Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) (δ , t)
    idᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ} → Tmsᴹ Γᴹ Γᴹ id
    _∘ᴹ_   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{σ : Tms Δ Σ}(σᴹ : Tmsᴹ Δᴹ Σᴹ σ)
             {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ) → Tmsᴹ Γᴹ Σᴹ (σ ∘ δ)
    π₁ᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {δ : Tms Γ (Δ , A)}(δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ) → Tmsᴹ Γᴹ Δᴹ (π₁ δ)

  infixl 4 _,sᴹ_
  infix 6 _∘ᴹ_
  
record MethodsTm (M : Motives)(mCon : MethodsCon M)(mTy : MethodsTy M mCon)
                 (mTms : MethodsTms M mCon mTy) : Set₁ where
  open Motives M
  open MethodsCon mCon
  open MethodsTy  mTy
  open MethodsTms mTms
  field
    _[_]tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{t : Tm Δ A} → Tmᴹ Δᴹ Aᴹ t
           → {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ) → Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) (t [ δ ]t)
    π₂ᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {δ : Tms Γ (Δ , A)}(δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ)→ Tmᴹ Γᴹ (Aᴹ [ π₁ᴹ δᴹ ]Tᴹ) (π₂ δ)
    appᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm Γ (Π A B)} → Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) t → Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ (app t)
    lamᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm (Γ , A) B} → Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ t → Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) (lam t)

  infixl 8 _[_]tᴹ
  
  -- another congruence rule
  []Tᴹ= : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A)
            {δ₀ δ₁ : Tms Γ Δ}(δ₂ : δ₀ ≡ δ₁)
            {δ₀ᴹ : Tmsᴹ Γᴹ Δᴹ δ₀}{δ₁ᴹ : Tmsᴹ Γᴹ Δᴹ δ₁}(δ₂ᴹ : δ₀ᴹ ≡[ TmsΓΔᴹ= δ₂ ]≡ δ₁ᴹ)
          → Aᴹ [ δ₀ᴹ ]Tᴹ ≡[ TyΓᴹ= (ap (λ z → A [ z ]T) δ₂) ]≡ Aᴹ [ δ₁ᴹ ]Tᴹ
  []Tᴹ= = λ { Aᴹ refl refl → refl }

record MethodsHTy (M    : Motives)
                  (mCon : MethodsCon M)
                  (mTy  : MethodsTy  M mCon)
                  (mTms : MethodsTms M mCon mTy)
                  (mTm  : MethodsTm  M mCon mTy mTms) : Set₁ where
  open Motives    M
  open MethodsCon mCon
  open MethodsTy  mTy
  open MethodsTms mTms
  open MethodsTm  mTm
  field
    [id]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
           → Aᴹ [ idᴹ ]Tᴹ ≡[ TyΓᴹ= [id]T ]≡ Aᴹ
    [][]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{A : Ty Σ}{Aᴹ : Tyᴹ Σᴹ A}
             {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}{δ : Tms Δ Σ}{δᴹ : Tmsᴹ Δᴹ Σᴹ δ}
           → (Aᴹ [ δᴹ ]Tᴹ) [ σᴹ ]Tᴹ ≡[ TyΓᴹ= [][]T ]≡ Aᴹ [ δᴹ ∘ᴹ σᴹ ]Tᴹ
    U[]ᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
           → Uᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ= U[] ]≡ Uᴹ
    El[]ᴹ  : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {Â : Tm Δ U}{Âᴹ : Tmᴹ Δᴹ Uᴹ Â}
           → Elᴹ Âᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ= El[] ]≡ Elᴹ (coe (TmΓᴹ= U[] U[]ᴹ refl) (Âᴹ [ δᴹ ]tᴹ))

  _^ᴹ_ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
         {A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A) → Tmsᴹ (Γᴹ ,Cᴹ Aᴹ [ δᴹ ]Tᴹ) (Δᴹ ,Cᴹ Aᴹ) (δ ^ A)
  _^ᴹ_ = λ δᴹ Aᴹ → (δᴹ ∘ᴹ π₁ᴹ idᴹ) ,sᴹ coe (TmΓᴹ= [][]T [][]Tᴹ refl) (π₂ᴹ idᴹ)

  field
    Π[]ᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}{A : Ty Δ}
             {Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
           → Πᴹ Aᴹ Bᴹ [ δᴹ ]Tᴹ ≡[ TyΓᴹ= Π[] ]≡ Πᴹ (Aᴹ [ δᴹ ]Tᴹ) (Bᴹ [ δᴹ ^ᴹ Aᴹ ]Tᴹ)

    setTᴹ  : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{B : Ty Γ}{Bᴹ : Tyᴹ Γᴹ B}{e0 e1 : A ≡ B}
           → {e0ᴹ : Aᴹ ≡[ TyΓᴹ= e0 ]≡ Bᴹ}{e1ᴹ : Aᴹ ≡[ TyΓᴹ= e1 ]≡ Bᴹ}
           → e0ᴹ ≡[ ~TyΓᴹ== e0 e1 (setT {e0 = e0}{e1}) ]≡ e1ᴹ

record MethodsHTms (M    : Motives)
                   (mCon : MethodsCon M)
                   (mTy  : MethodsTy  M mCon)
                   (mTms : MethodsTms M mCon mTy)
                   (mTm  : MethodsTm  M mCon mTy mTms) : Set₁ where
  open Motives    M
  open MethodsCon mCon
  open MethodsTy  mTy
  open MethodsTms mTms
  open MethodsTm  mTm
  field
    idlᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
           → (idᴹ ∘ᴹ δᴹ) ≡[ TmsΓΔᴹ= idl ]≡ δᴹ
    idrᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
           → (δᴹ ∘ᴹ idᴹ) ≡[ TmsΓΔᴹ= idr ]≡ δᴹ
    assᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{Ω}{Ωᴹ : Conᴹ Ω}
             {σ : Tms Σ Ω}{σᴹ : Tmsᴹ Σᴹ Ωᴹ σ}{δ : Tms Γ Σ}{δᴹ : Tmsᴹ Γᴹ Σᴹ δ}
             {ν : Tms Δ Γ}{νᴹ : Tmsᴹ Δᴹ Γᴹ ν}
           → ((σᴹ ∘ᴹ δᴹ) ∘ᴹ νᴹ) ≡[ TmsΓΔᴹ= ass ]≡ (σᴹ ∘ᴹ (δᴹ ∘ᴹ νᴹ))
    π₁βᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {a : Tm Γ (A [ δ ]T)}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a}
           → (π₁ᴹ (δᴹ ,sᴹ aᴹ)) ≡[ TmsΓΔᴹ= π₁β ]≡ δᴹ
    πηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {δ : Tms Γ (Δ , A)}{δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ}
           → (π₁ᴹ δᴹ ,sᴹ π₂ᴹ δᴹ) ≡[ TmsΓΔᴹ= πη ]≡ δᴹ
    εηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{σ : Tms Γ •}{σᴹ : Tmsᴹ Γᴹ •ᴹ σ}
           → σᴹ ≡[ TmsΓΔᴹ= εη ]≡ εᴹ
    setsᴹ  : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}{e0 e1 : δ ≡ σ}
             {e0ᴹ : δᴹ ≡[ TmsΓΔᴹ= e0 ]≡ σᴹ}{e1ᴹ : δᴹ ≡[ TmsΓΔᴹ= e1 ]≡ σᴹ}
           → e0ᴹ ≡[ ~TmsΓΔᴹ== e0 e1 (sets {e0 = e0}{e1}) ]≡ e1ᴹ

record MethodsHTm  (M     : Motives)
                   (mCon  : MethodsCon  M)
                   (mTy   : MethodsTy   M mCon)
                   (mTms  : MethodsTms  M mCon mTy)
                   (mTm   : MethodsTm   M mCon mTy mTms)
                   (mHTy  : MethodsHTy  M mCon mTy mTms mTm)
                   (mHTms : MethodsHTms M mCon mTy mTms mTm) : Set₁ where
  open Motives     M
  open MethodsCon  mCon
  open MethodsTy   mTy
  open MethodsTms  mTms
  open MethodsTm   mTm
  open MethodsHTy  mHTy
  open MethodsHTms mHTms
  field
    [id]tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}{t : Tm Γ A}{tᴹ : Tmᴹ Γᴹ Aᴹ t}
           → tᴹ [ idᴹ ]tᴹ ≡[ TmΓᴹ= [id]T [id]Tᴹ [id]t ]≡ tᴹ
    [][]tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}
             {A : Ty Σ}{Aᴹ : Tyᴹ Σᴹ A}{t : Tm Σ A}{tᴹ : Tmᴹ Σᴹ Aᴹ t}
             {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}{δ : Tms Δ Σ}{δᴹ : Tmsᴹ Δᴹ Σᴹ δ}
           → (tᴹ [ δᴹ ]tᴹ) [ σᴹ ]tᴹ ≡[ TmΓᴹ= [][]T [][]Tᴹ [][]t ]≡ tᴹ [ δᴹ ∘ᴹ σᴹ ]tᴹ
    π₂βᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {a : Tm Γ (A [ δ ]T)}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a}
           → π₂ᴹ (δᴹ ,sᴹ aᴹ) ≡[ TmΓᴹ= (ap (λ z → A [ z ]T) π₁β) ([]Tᴹ= Aᴹ π₁β π₁βᴹ) π₂β ]≡ aᴹ
    lam[]ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Aᴹ) B}
             {t : Tm (Δ , A) B}{tᴹ : Tmᴹ (Δᴹ ,Cᴹ Aᴹ) Bᴹ t}
           → (lamᴹ tᴹ) [ δᴹ ]tᴹ ≡[ TmΓᴹ= Π[] Π[]ᴹ lam[] ]≡ lamᴹ (tᴹ [ δᴹ ^ᴹ Aᴹ ]tᴹ)
    Πβᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
             {B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm (Γ , A) B}{tᴹ : Tmᴹ (Γᴹ ,Cᴹ Aᴹ) Bᴹ t}
           → appᴹ (lamᴹ tᴹ) ≡[ TmΓAᴹ= Πβ ]≡ tᴹ
    Πηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
             {B : Ty (Γ , A)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B}
             {t : Tm Γ (Π A B)}{tᴹ : Tmᴹ Γᴹ (Πᴹ Aᴹ Bᴹ) t}
           → lamᴹ (appᴹ tᴹ) ≡[ TmΓAᴹ= Πη ]≡ tᴹ
    settᴹ  : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
             {u : Tm Γ A}{uᴹ : Tmᴹ Γᴹ Aᴹ u}{v : Tm Γ A}{vᴹ : Tmᴹ Γᴹ Aᴹ v}{e0 e1 : u ≡ v}
             {e0ᴹ : uᴹ ≡[ TmΓAᴹ= e0 ]≡ vᴹ}{e1ᴹ : uᴹ ≡[ TmΓAᴹ= e1 ]≡ vᴹ}
           → e0ᴹ ≡[ ~TmΓAᴹ== e0 e1 (sett {e0 = e0}{e1}) ]≡ e1ᴹ

module elim (M : Motives)
            (mCon  : MethodsCon  M)
            (mTy   : MethodsTy   M mCon)
            (mTms  : MethodsTms  M mCon mTy)
            (mTm   : MethodsTm   M mCon mTy mTms)
            (mHTy  : MethodsHTy  M mCon mTy mTms mTm)
            (mHTms : MethodsHTms M mCon mTy mTms mTm)
            (mHTm  : MethodsHTm  M mCon mTy mTms mTm mHTy mHTms) where
  open Motives     M
  open MethodsCon  mCon
  open MethodsTy   mTy
  open MethodsTms  mTms
  open MethodsTm   mTm

  ElimCon : ∀ Γ → Conᴹ Γ
  ElimTy  : ∀{Γ}(A : Ty Γ) → Tyᴹ (ElimCon Γ) A
  ElimTms : ∀{Γ Δ}(δ : Tms Γ Δ) → Tmsᴹ (ElimCon Γ) (ElimCon Δ) δ
  ElimTm  : ∀{Γ}{A : Ty Γ}(t : Tm Γ A) → Tmᴹ (ElimCon Γ) (ElimTy A) t

  ElimCon •       = •ᴹ
  ElimCon (Γ , A) = ElimCon Γ ,Cᴹ ElimTy A
  
  ElimTy (A [ δ ]T) = ElimTy A [ ElimTms δ ]Tᴹ
  ElimTy U          = Uᴹ
  ElimTy (El Â)     = Elᴹ (ElimTm Â)
  ElimTy (Π A B)    = Πᴹ (ElimTy A) (ElimTy B)
  
  ElimTms ε       = εᴹ
  ElimTms (δ , t) = ElimTms δ ,sᴹ ElimTm t
  ElimTms id      = idᴹ
  ElimTms (δ ∘ σ) = ElimTms δ ∘ᴹ ElimTms σ
  ElimTms (π₁ δ)  = π₁ᴹ (ElimTms δ)

  ElimTm (t [ δ ]t) = ElimTm t [ ElimTms δ ]tᴹ
  ElimTm (π₂ δ)     = π₂ᴹ (ElimTms δ)
  ElimTm (app t)    = appᴹ (ElimTm t)
  ElimTm (lam t)    = lamᴹ (ElimTm t)
