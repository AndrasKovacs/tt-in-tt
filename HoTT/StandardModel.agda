{-# OPTIONS --without-K --no-eta #-}

module HoTT.StandardModel where

open import lib
open import HoTT.Syntax
open import HoTT.Rec
open import HoTT.UniverseSet

M : Motives
M = record
      { Conᴹ =             UU
      ; Tyᴹ  = λ ⟦Γ⟧     → EL ⟦Γ⟧ → UU
      ; Tmsᴹ = λ ⟦Γ⟧ ⟦Δ⟧ → EL ⟦Γ⟧ → EL ⟦Δ⟧
      ; Tmᴹ  = λ ⟦Γ⟧ ⟦A⟧ → (γ : EL ⟦Γ⟧) → EL (⟦A⟧ γ)
      }

m : Methods M
m = record
      { •ᴹ     =             ′⊤′
      ; _,Cᴹ_  = λ ⟦Γ⟧ ⟦A⟧ → ′Σ′ ⟦Γ⟧ ⟦A⟧
      
      ; _[_]Tᴹ = λ ⟦A⟧ ⟦δ⟧ γ → ⟦A⟧ (⟦δ⟧ γ)
      ; Uᴹ     = λ         _ → ′⊤′
      ; Elᴹ    = λ ⟦Â⟧     γ → ′⊤′
      ; Πᴹ     = λ ⟦A⟧ ⟦B⟧ γ → ′Π′ (⟦A⟧ γ) (λ x → ⟦B⟧ (γ ,Σ x))
      
      ; εᴹ     = λ         _ → tt
      ; _,sᴹ_  = λ ⟦δ⟧ ⟦t⟧ γ → ⟦δ⟧ γ ,Σ ⟦t⟧ γ
      ; idᴹ    = λ         γ → γ
      ; _∘ᴹ_   = λ ⟦δ⟧ ⟦σ⟧ γ → ⟦δ⟧ (⟦σ⟧ γ)
      ; π₁ᴹ    = λ ⟦δ⟧     γ → (proj₁ (⟦δ⟧ γ))
      
      ; _[_]tᴹ = λ ⟦t⟧ ⟦δ⟧ γ → ⟦t⟧ (⟦δ⟧ γ)
      ; π₂ᴹ    = λ ⟦δ⟧     γ → (proj₂ (⟦δ⟧ γ))
      ; appᴹ   = λ ⟦t⟧     γ → ⟦t⟧ (proj₁ γ) (proj₂ γ)
      ; lamᴹ   = λ ⟦t⟧     γ → λ a → ⟦t⟧ (γ ,Σ a)
      
      ; [id]Tᴹ = refl
      ; [][]Tᴹ = refl
      ; U[]ᴹ   = refl
      ; El[]ᴹ  = refl
      ; Π[]ᴹ   = refl
      ; setTᴹ  = setCodomain setUU
      
      ; idlᴹ   = refl
      ; idrᴹ   = refl
      ; assᴹ   = refl
      ; ,∘ᴹ    = refl
      ; π₁βᴹ   = refl
      ; πηᴹ    = refl
      ; εηᴹ    = refl
      ; setsᴹ  = setCodomain setEL
      
      ; [id]tᴹ = refl
      ; [][]tᴹ = refl
      ; π₂βᴹ   = refl
      ; lam[]ᴹ = refl
      ; Πβᴹ    = refl
      ; Πηᴹ    = refl
      ; settᴹ  = setCodomain setEL
      }

open rec M m

⟦_⟧C : Con → UU
⟦_⟧T : ∀{Γ} → Ty Γ → EL (⟦ Γ ⟧C) → UU
⟦_⟧s : ∀{Γ Δ} → Tms Γ Δ → EL (⟦ Γ ⟧C) → EL (⟦ Δ ⟧C)
⟦_⟧t : ∀{Γ}{A : Ty Γ} → (t : Tm Γ A) → (γ : EL (⟦ Γ ⟧C)) → EL (⟦ A ⟧T γ)

⟦_⟧C = RecCon
⟦_⟧T = RecTy
⟦_⟧s = RecTms
⟦_⟧t = RecTm
