module Section1-2.IndIndEx where

data Con  : Set
data Ty   : Con → Set

data Con where
  •       : Con  
  _,C_    : (Γ : Con) → Ty Γ → Con

data Ty where
  U       : ∀{Γ} → Ty Γ
  Π       : ∀{Γ}(A : Ty Γ)(B : Ty (Γ ,C A)) → Ty Γ

module Rec where
  record Motives : Set₁ where
    field
      Conᴹ   : Set
      Tyᴹ    : Conᴹ → Set

  record Methods (M : Motives) : Set₁ where
    open Motives M
    field
      •ᴹ     : Conᴹ
      _,Cᴹ_  : (Γᴹ : Conᴹ) → Tyᴹ Γᴹ → Conᴹ
      Uᴹ     : {Γᴹ : Conᴹ} → Tyᴹ Γᴹ
      Πᴹ     : {Γᴹ : Conᴹ}(Aᴹ : Tyᴹ Γᴹ)
               (Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ)) → Tyᴹ Γᴹ

  module rec (M : Motives)(m : Methods M) where

    open Motives M
    open Methods m

    RecCon : Con → Conᴹ
    RecTy  : {Γ : Con}(A : Ty Γ) → Tyᴹ (RecCon Γ)

    RecCon  •         = •ᴹ
    RecCon  (Γ ,C A)  = RecCon Γ ,Cᴹ RecTy A

    RecTy   U         = Uᴹ
    RecTy   (Π A B)   = Πᴹ (RecTy A) (RecTy B)  

module Elim where
  record Motives : Set₁ where
    field
      Conᴹ   :  Con → Set
      Tyᴹ    :  {Γ : Con} → Conᴹ Γ → Ty Γ → Set

  record Methods (M : Motives) : Set₁ where
    open Motives M
    field
      •ᴹ     :  Conᴹ •
      _,Cᴹ_  :  {Γ : Con}(Γᴹ : Conᴹ Γ)
                {A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A)
             →  Conᴹ (Γ ,C A)
      Uᴹ     :  {Γ : Con}{Γᴹ : Conᴹ Γ}
             → Tyᴹ Γᴹ U
      Πᴹ     :  {Γ : Con}{Γᴹ : Conᴹ Γ}
                {A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A)
                {B : Ty (Γ ,C A)}
                (Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Aᴹ) B)
             →  Tyᴹ Γᴹ (Π A B)

  module elim (M : Motives)(m : Methods M) where

    open Motives M
    open Methods m

    ElimCon :  (Γ : Con) → Conᴹ Γ
    ElimTy  :  {Γ : Con}(A : Ty Γ)
            →  Tyᴹ (ElimCon Γ) A

    ElimCon  •         =   •ᴹ
    ElimCon  (Γ ,C A)  =   ElimCon Γ ,Cᴹ ElimTy A

    ElimTy   U         =   Uᴹ
    ElimTy   (Π A B)   =   Πᴹ (ElimTy A) (ElimTy B)  
