module Section1-2.STL where

data Ty : Set where
  ι : Ty
  _⇒_ : Ty → Ty → Ty

RecTy :  (Tyᴹ : Set)
         (ιᴹ : Tyᴹ)
         (⇒ᴹ : Tyᴹ → Tyᴹ → Tyᴹ)
      →  Ty → Tyᴹ
RecTy Tyᴹ ιᴹ ⇒ᴹ ι  = ιᴹ
RecTy Tyᴹ ιᴹ ⇒ᴹ (A ⇒ B) = ⇒ᴹ  (RecTy Tyᴹ ιᴹ ⇒ᴹ A)
                              (RecTy Tyᴹ ιᴹ ⇒ᴹ B)

ElimTy :  (Tyᴹ : Ty → Set)
          (ιᴹ : Tyᴹ ι)
          (⇒ᴹ : {A : Ty}(Aᴹ : Tyᴹ A){B : Ty}(Bᴹ : Tyᴹ B) → Tyᴹ (A ⇒ B))
       →  (A : Ty) → Tyᴹ A
ElimTy Tyᴹ ιᴹ ⇒ᴹ ι = ιᴹ
ElimTy Tyᴹ ιᴹ ⇒ᴹ (A ⇒ B) =  ⇒ᴹ  (ElimTy Tyᴹ ιᴹ ⇒ᴹ A)
                                (ElimTy Tyᴹ ιᴹ ⇒ᴹ B)


data Con : Set where
  • : Con
  _,_ : Con → Ty → Con

data Var : Con → Ty → Set where
  zero : ∀ {Γ}{σ} → Var (Γ , σ) σ
  suc : ∀ {Γ}{σ}{τ} → Var Γ σ → Var (Γ , τ) σ  

data Tm :  Con → Ty → Set where
  var : ∀ {Γ}{σ} → Var Γ σ → Tm Γ σ
  _$_ :  ∀ {Γ}{σ}{τ} → Tm Γ (σ ⇒ τ) → Tm Γ σ → Tm Γ τ 
  ƛ :  ∀ {Γ}{σ}{τ} → Tm (Γ , σ) τ → Tm Γ (σ ⇒ τ)
  
data _* (X : Ty → Set) : Con → Set where
  ε : (X *) •
  _,_ :  ∀ {Γ}{σ} → (X *) Γ → X σ → (X *) (Γ , σ)

_** : ∀ {X}{Y} → (∀ {σ} → X σ → Y σ) → ∀ {Γ} → (X *) Γ → (Y *) Γ
_** f ε = ε
_** f (xs , x) = ((f **) xs) , (f x)

Vars : Con → Con → Set
Vars Γ = (Var Γ) *

_!!_ : ∀ {X}{Γ}{σ} → (X *) Γ → Var Γ σ → X σ
ε !! ()
(xs , x) !! zero = x
(xs , x) !! suc y = xs !! y

wkv : ∀ {Γ}{Δ}{σ} → Vars Γ Δ → Vars (Γ , σ) Δ
wkv = suc **

extv : ∀ {Γ}{Δ}{σ} → Vars Γ Δ → Vars (Γ , σ) (Δ , σ)
extv xs = (suc **) xs , zero

idV : ∀ {Γ} → Vars Γ Γ
idV {•} = ε
idV {Γ , σ} = extv (idV {Γ})
--idV {Γ , σ} = wkv (idV {Γ}) , zero

Tms : Con → Con → Set
Tms Γ = (Tm Γ) *

_[_]v : ∀ {Γ}{Δ}{σ} → Tm Γ σ → Vars Δ Γ → Tm Δ σ
var x [ xs ]v = var (xs !! x )
(t $ u) [ xs ]v = (t [ xs ]v) $ (u [ xs ]v)
ƛ t [ xs ]v = ƛ (t [ extv xs ]v)

wkt : ∀ {Γ}{Δ}{σ} → Tms Γ Δ → Tms (Γ , σ) Δ
wkt = (λ {σ} t → t [ wkv idV ]v) **

extt : ∀ {Γ}{Δ}{σ} → Tms Γ Δ → Tms (Γ , σ) (Δ , σ)
extt xs = wkt xs , var zero

_[_]t : ∀ {Γ}{Δ}{σ} → Tm Γ σ → Tms Δ Γ → Tm Δ σ
var x [ ts ]t = ts !! x
(t $ u) [ ts ]t = (t [ ts ]t) $ (u [ ts ]t)
ƛ t [ ts ]t = ƛ (t [ extt ts ]t)

-- decidability of normal forms

module NormalForms where

  data Ne : Con → Ty → Set
  data Nf : Con → Ty → Set

  data Ne where
    var : ∀{Γ A}(x : Var Γ A) → Ne Γ A
    app : ∀{Γ A B}(f : Ne Γ (A ⇒ B))(v : Nf Γ A) → Ne Γ B

  data Nf where
    neu : ∀{Γ}(n : Ne Γ ι) → Nf Γ ι
    lam  : ∀{Γ A B}(v : Nf (Γ , A) B) → Nf Γ (A ⇒ B)

  open import lib
  open import JM

  data isDec {i}(A : Set i) : Set i where
    yes : A → isDec A
    no  : (A → ⊥) → isDec A

  inj⇒ : {A₀ A₁ B₀ B₁ : Ty} → A₀ ⇒ B₀ ≡ A₁ ⇒ B₁ → (A₀ ≡ A₁) × (B₀ ≡ B₁)
  inj⇒ refl = refl ,Σ refl

  decTy : (A₀ A₁ : Ty) → isDec (A₀ ≡ A₁)
  decTy ι ι = yes refl
  decTy ι (A₁ ⇒ A₂) = no λ { () }
  decTy (A₀ ⇒ A₁) ι = no λ { () }
  decTy (A₀₀ ⇒ A₀₁) (A₁₀ ⇒ A₁₁) with decTy A₀₀ A₁₀ | decTy A₀₁ A₁₁
  decTy (A₀₀ ⇒ A₀₁) (.A₀₀ ⇒ .A₀₁) | yes refl | yes refl = yes refl
  decTy (A₀₀ ⇒ A₀₁) (A₁₀ ⇒ A₁₁) | yes p₀ | no p₁ = no λ q → p₁ (proj₂ (inj⇒ q))
  decTy (A₀₀ ⇒ A₀₁) (A₁₀ ⇒ A₁₁) | no p₀ | p₁ = no λ q → p₀ (proj₁ (inj⇒ q))

  injsuc : {Γ : Con}{A B : Ty}{x₀ x₁ : Var Γ A}
         → _≡_ {A = Var (Γ , B) A} (suc {Γ}{A}{B} x₀) (suc {Γ}{A}{B} x₁)
         → x₀ ≡ x₁
  injsuc refl = refl

  decVar : ∀{Γ A}(x₀ x₁ : Var Γ A) → isDec (x₀ ≡ x₁)
  decVar zero zero = yes refl
  decVar zero (suc x₁) = no (λ { () })
  decVar (suc x₀) zero = no (λ { () })
  decVar (suc x₀) (suc x₁) with decVar x₀ x₁
  decVar (suc x₀) (suc .x₀) | yes refl = yes refl
  decVar (suc x₀) (suc x₁) | no p = no λ q → p (injsuc q)

  record Tot (T : Con → Ty → Set) : Set where
    constructor tot
    field
      {con} : Con
      {ty}  : Ty
      tm    : T con ty

  injtotNe : ∀{Γ A}{n₀ n₁ : Ne Γ A} → _≡_ {A = Tot Ne} (tot n₀) (tot n₁) → n₀ ≡ n₁
  injtotNe refl = refl

  injvar : ∀{Γ A}{x₀ x₁ : Var Γ A} → _≡_ {A = Ne Γ A} (var x₀) (var x₁) → x₀ ≡ x₁
  injvar refl = refl

  injapp : ∀{Γ A B}{n₀ n₁ : Ne Γ (A ⇒ B)}{v₀ : Nf Γ A}{v₁ : Nf Γ A}
         → app n₀ v₀ ≡ app n₁ v₁ → (v₀ ≡ v₁) × (n₀ ≡ n₁)
  injapp refl = refl ,Σ refl

  injapp' : ∀{Γ B A₀ A₁}{n₀ : Ne Γ (A₀ ⇒ B)}{n₁ : Ne Γ (A₁ ⇒ B)}{v₀ : Nf Γ A₀}{v₁ : Nf Γ A₁}
          → app n₀ v₀ ≡ app n₁ v₁ → A₀ ≡ A₁
  injapp' refl = refl

  injtotNf : ∀{Γ A}{v₀ v₁ : Nf Γ A} → _≡_ {A = Tot Nf} (tot v₀) (tot v₁) → v₀ ≡ v₁
  injtotNf refl = refl

  injneu : ∀{Γ}{n₀ n₁ : Ne Γ ι} → neu n₀ ≡ neu n₁ → n₀ ≡ n₁
  injneu refl = refl

  injlam : ∀{Γ A B}{v₀ v₁ : Nf (Γ , A) B} → lam v₀ ≡ lam v₁ → v₀ ≡ v₁
  injlam refl = refl

  decNe : ∀{Γ A}(n₀ : Ne Γ A)(n₁ : Ne Γ A)
         → isDec (_≡_ {A = Tot Ne} (tot n₀) (tot n₁))

  decNf : ∀{Γ A}(v₀ : Nf Γ A)(v₁ : Nf Γ A)
         → isDec (_≡_ {A = Tot Nf} (tot v₀) (tot v₁))

  decNe (var x₀) (var x₁) with decVar x₀ x₁
  decNe (var x₀) (var .x₀) | yes refl = yes refl
  decNe (var x₀) (var x₁) | no p = no λ q → p (injvar (injtotNe q))
  decNe (var x₀) (app n₁ v₁) = no λ { () }
  decNe (app n₀ v₀) (var x₁) = no λ { () }
  decNe (app {A = A₀} n₀ v₀) (app {A = A₁} n₁ v₁) with decTy A₀ A₁
  decNe (app n₀ v₀) (app n₁ v₁) | yes refl with decNe n₀ n₁
  decNe (app n₀ v₀) (app .n₀ v₁) | yes refl | yes refl with decNf v₀ v₁
  decNe (app n₀ v₀) (app .n₀ .v₀) | yes refl | yes refl | yes refl = yes refl
  decNe (app n₀ v₀) (app .n₀ v₁) | yes refl | yes refl | no p = no λ q → p (ap tot (proj₁ (injapp (injtotNe q))))
  decNe (app n₀ v₀) (app n₁ v₁) | yes refl | no p = no λ q → p (ap tot (proj₂ (injapp (injtotNe q))))
  decNe (app n₀ v₀) (app n₁ v₁) | no p = no λ q → p (injapp' (injtotNe q))

  decNf (neu n₀) (neu n₁) with decNe n₀ n₁
  decNf (neu n₀) (neu .n₀) | yes refl = yes refl
  decNf (neu n₀) (neu n₁)  | no p = no λ { q → p (ap tot (injneu (injtotNf q))) }
  decNf (lam v₀) (lam v₁) with decNf v₀ v₁
  decNf (lam v₀) (lam .v₀) | yes refl = yes refl
  decNf (lam v₀) (lam v₁) | no p = no λ q → p (ap tot (injlam (injtotNf q)))

  dec : ∀{Γ A}(v₀ : Nf Γ A)(v₁ : Nf Γ A) → isDec (v₀ ≡ v₁)
  dec v₀ v₁ with decNf v₀ v₁
  dec v₀ v₁ | yes p = yes (injtotNf p)
  dec v₀ v₁ | no  p = no (λ q → p (ap tot q))
