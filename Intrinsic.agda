{-# OPTIONS --without-K #-}

module Intrinsic where

open import lib

-- comparing extrinsic and intrinsic syntax for simle type theory

data Ty : Set where
  ι : Ty
  _⇒_ : Ty → Ty → Ty

infixr 4 _⇒_

data Con : Set where
  • : Con
  _,_ : Con → Ty → Con

infixl 3 _,_

-- Intrinsic syntax

data Var : Con → Ty → Set where
  vz : ∀{Γ}{σ} → Var (Γ , σ) σ
  vs  : ∀{Γ}{σ}{τ} → Var Γ σ → Var (Γ , τ) σ  

data Tm : Con → Ty → Set where
  var : ∀{Γ σ} → Var Γ σ → Tm Γ σ
  lam : ∀{Γ σ τ} → Tm (Γ , σ) τ → Tm Γ (σ ⇒ τ)
  app : ∀{Γ σ τ} → Tm Γ (σ ⇒ τ) → Tm (Γ , σ) τ

-- Extrinsic syntax

data PTm : Set where
  pvar : ℕ → PTm
  plam : PTm → PTm
  papp : PTm → PTm

data _!_≡_ : Con → ℕ → Ty → Set where
  vze : ∀{Γ σ} → Γ , σ ! zero ≡ σ
  vsu  : ∀{Γ σ τ n} → Γ ! n ≡ σ → Γ , τ ! suc n ≡ σ

infix 2 _!_≡_

data _⊢_∶_ : Con → PTm → Ty → Set where
  VAR : ∀{Γ σ n} → Γ ! n ≡ σ → Γ ⊢ pvar n ∶ σ
  ⇒I : ∀{Γ σ τ u} → Γ , σ ⊢ u ∶ τ → Γ ⊢ plam u ∶ σ ⇒ τ
  ⇒E : ∀{Γ σ τ u} → Γ ⊢ u ∶ σ ⇒ τ → Γ , σ ⊢ papp u ∶ τ

infix 2 _⊢_∶_

-- Intrinsic to Extrinsic

IEV : ∀{Γ σ} → Var Γ σ → Σ ℕ λ n → Γ ! n ≡ σ
IEV vz = zero ,Σ vze
IEV (vs x) with IEV x
IEV (vs x) | n ,Σ p = suc n ,Σ vsu p

IE : ∀{Γ σ} → Tm Γ σ → Σ PTm λ u → Γ ⊢ u ∶ σ
IE (var x) with IEV x
IE (var x) | n ,Σ p = pvar n ,Σ VAR p
IE (lam t) with IE t
IE (lam t) | u ,Σ p = plam u ,Σ ⇒I p
IE (app t) with IE t
IE (app t) | u ,Σ p = papp u ,Σ ⇒E p

-- Extrinsic to Intrinsic

EIV : ∀{Γ σ} → (Σ ℕ λ n → Γ ! n ≡ σ) → Var Γ σ
EIV (_ ,Σ vze) = vz
EIV (_ ,Σ vsu p) = vs (EIV (_ ,Σ p))

EI : ∀{Γ σ} → (Σ PTm λ u → Γ ⊢ u ∶ σ) → Tm Γ σ
EI (_ ,Σ VAR p) = var (EIV (_ ,Σ p))
EI (_ ,Σ ⇒I p) = lam (EI (_ ,Σ p))
EI (_ ,Σ ⇒E p) = app (EI (_ ,Σ p))

-- Intrinsic to Extrinsic to Intrinsic

IEIV : ∀{Γ σ}(x : Var Γ σ) → EIV (IEV x) ≡ x
IEIV vz = refl
IEIV (vs x) = ap vs (IEIV x)

IEI : ∀{Γ σ}(t : Tm Γ σ) → EI (IE t) ≡ t
IEI (var x) = ap var (IEIV x)
IEI (lam t) = ap lam (IEI t)
IEI (app t) = ap app (IEI t)

-- Extrinsic to Intrinsic to Extrinsic

EIEV : ∀{Γ σ}(w : Σ ℕ λ n → Γ ! n ≡ σ) → IEV (EIV w) ≡ w
EIEV (zero ,Σ vze) = refl
EIEV {Γ , τ}{σ}(suc n ,Σ vsu p)
  = ,Σ= (ap (λ z → suc (proj₁ z)) (EIEV (n ,Σ p)))
        ( ap (λ z → coe z (vsu (proj₂ (IEV (EIV (n ,Σ p)))))) (apap (EIEV (n ,Σ p)) ⁻¹)
        ◾ apd (λ z → vsu (proj₂ z)) (EIEV (n ,Σ p)))

EIE : ∀{Γ σ}(w : Σ PTm λ u → Γ ⊢ u ∶ σ) → IE (EI w) ≡ w
EIE {Γ}{σ}(pvar n ,Σ VAR p)
  = ,Σ= (ap (λ z → pvar (proj₁ z)) (EIEV (n ,Σ p)))
        (ap (λ z → coe z (VAR (proj₂ (IEV (EIV (n ,Σ p))))))
            (apap (EIEV (n ,Σ p)) ⁻¹)
        ◾ apd (λ z → VAR (proj₂ z)) (EIEV (n ,Σ p)))
EIE {Γ}{σ ⇒ τ}(plam u ,Σ ⇒I p)
  = ,Σ= (ap (λ z → plam (proj₁ z)) (EIE (u ,Σ p)))
        (ap (λ z → coe z (⇒I (proj₂ (IE (EI (u ,Σ p))))))
            (apap (EIE (u ,Σ p)) ⁻¹)
        ◾ apd (λ z → ⇒I (proj₂ z)) (EIE (u ,Σ p)))
EIE {Γ , σ}{τ}(papp u ,Σ ⇒E p)
  = ,Σ= (ap (λ z → papp (proj₁ z)) (EIE (u ,Σ p)))
        (ap (λ z → coe z (⇒E (proj₂ (IE (EI (u ,Σ p))))))
            (apap (EIE (u ,Σ p)) ⁻¹)
        ◾ apd (λ z → ⇒E (proj₂ z)) (EIE (u ,Σ p)))
