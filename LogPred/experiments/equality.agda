module LogPred.experiments.equality where

-- the logpred operation for equality

data _≡_ {i}{A : Set i}(a : A) : A → Set i where
  refl : a ≡ a

infix 3 _≡_

ap : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
ap f refl = refl

transport : ∀{i j}{A : Set i}(P : A → Set j){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
            → P a₀ → P a₁
transport P refl u = u

coe : ∀{i}{A B : Set i} → A ≡ B → A → B
coe refl a = a

J : (A : Set)(a : A)
    (Q : (x : A) → a ≡ x → Set)
    (r : Q a refl)
    (x : A)(q : a ≡ x) → Q x q
J A a Q r .a refl = r

record Σ (A : Set)(B : A → Set) : Set where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁

open Σ

_,≡_ : {A : Set}{B : A → Set}{a a' : A}{b : B a}{b' : B a'}
       (p : a ≡ a') → transport B p b ≡ b' → (a , b) ≡ (a' , b')
_,≡_ refl refl = refl

proj≡₁ : {A : Set}{B : A → Set}{a a' : A}{b : B a}{b' : B a'}
       → (a , b) ≡ (a' , b') → a ≡ a'
proj≡₁ refl = refl

proj≡₂ : {A : Set}{B : A → Set}{a a' : A}{b : B a}{b' : B a'}
         (p : (a , b) ≡ (a' , b')) → transport B (proj≡₁ p) b ≡ b'
proj≡₂ refl = refl

Σβ≡₁ : {A : Set}{B : A → Set}{a a' : A}{b : B a}{b' : B a'}
       {p : a ≡ a'}{q : transport B p b ≡ b'}
     → proj≡₁ (p ,≡ q) ≡ p
Σβ≡₁ {p = refl}{refl} = refl

Σβ≡₂ : {A : Set}{B : A → Set}{a a' : A}{b : B a}{b' : B a'}
       {p : a ≡ a'}{q : transport B p b ≡ b'}
     → transport (λ z → transport B z b ≡ b') (Σβ≡₁ {p = p}{q}) (proj≡₂ (p ,≡ q)) ≡ q
Σβ≡₂ {p = refl}{refl} = refl

Jᴾ : (A : Set)(Aᴹ : A → Set)(a : A)(aᴹ : Aᴹ a)
     (Q : (x : A) → a ≡ x → Set)
     (Qᴹ : (x : A)(xᴹ : Aᴹ x)(q : a ≡ x)(qᴹ : transport Aᴹ q aᴹ ≡ xᴹ) → Q x q → Set)
     (r : Q a refl)(rᴹ : Qᴹ a aᴹ refl refl r)
     (x : A)(xᴹ : Aᴹ x)(q : a ≡ x)(qᴹ : transport Aᴹ q aᴹ ≡ xᴹ)
   → Qᴹ x xᴹ q qᴹ (J A a Q r x q)
Jᴾ A Aᴹ a aᴹ Q Qᴹ r rᴹ .a .aᴹ refl refl = rᴹ

Jᴾ' : (A : Set)(Aᴹ : A → Set)(a : A)(aᴹ : Aᴹ a)
     (Q : (x : A) → a ≡ x → Set)
     (Qᴹ : (x : A)(xᴹ : Aᴹ x)(q : a ≡ x)(qᴹ : transport Aᴹ q aᴹ ≡ xᴹ) → Q x q → Set)
     (r : Q a refl)(rᴹ : Qᴹ a aᴹ refl refl r)
     (x : A)(xᴹ : Aᴹ x)(q : a ≡ x)(qᴹ : transport Aᴹ q aᴹ ≡ xᴹ)
   → Qᴹ x xᴹ q qᴹ (J A a Q r x q)
Jᴾ' A Aᴹ a aᴹ Q Qᴹ r rᴹ x xᴹ q qᴹ
  = coe (Qᴹ= Σβ≡₁ (Σβ≡₂ {B = Aᴹ}{p = q}{qᴹ}))
        (J (Σ A Aᴹ) (a , aᴹ)
           (λ c s → Qᴹ (proj₁ c) (proj₂ c) (proj≡₁ s) (proj≡₂ s) (J A a Q r (proj₁ c) (proj≡₁ s)))
           rᴹ
           (x , xᴹ)
           (q ,≡ qᴹ))
  where
    Qᴹ= : {x : A}{xᴹ : Aᴹ x}{q₀ q₁ : a ≡ x}(q₂ : q₀ ≡ q₁)
          {qᴹ₀ : transport Aᴹ q₀ aᴹ ≡ xᴹ}{qᴹ₁ : transport Aᴹ q₁ aᴹ ≡ xᴹ}(qᴹ₂ : transport (λ q → transport Aᴹ q aᴹ ≡ xᴹ) q₂ qᴹ₀ ≡ qᴹ₁)
        → Qᴹ x xᴹ q₀ qᴹ₀ (J A a Q r x q₀) ≡ Qᴹ x xᴹ q₁ qᴹ₁ (J A a Q r x q₁)
    Qᴹ= refl refl = refl
