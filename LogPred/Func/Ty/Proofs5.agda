{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Func.Ty.Proofs5 where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

open import LogPred.Func.Ty
import LogPred.Func.Ty.Proofs1
import LogPred.Func.Ty.Proofs2
import LogPred.Func.Ty.Proofs3
import LogPred.Func.Ty.Proofs4

module Π[]ᴹ
  {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}
  {δᴹ : Tmsᴹ Γᴹ Δᴹ δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{B : Ty (Δ , A)}
  where

  open LogPred.Func.Ty.Proofs1.Π[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}
  open LogPred.Func.Ty.Proofs2.Π[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}
  open LogPred.Func.Ty.Proofs3.Π[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}
  open LogPred.Func.Ty.Proofs4.Π[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}

  abstract
    Goal : (π₁ id ^ A [ Pr Δᴹ ]T ^ Aᴹ ,s
           coe (TmΓ= (pΠᴹ Δᴹ)) (app (coe (TmΓ= (pΠᴹ' Δᴹ)) (π₂ id)) [ π₁ id ]t))
          ∘
          (coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (∣ δᴹ ∣s ^ Π A B [ Pr Δᴹ ]T) ^
           (A [ Pr Δᴹ ]T) [ π₁ id ]T
           ^ Aᴹ [ π₁ id ^ A [ Pr Δᴹ ]T ]T)
          ≃
          coe (Tms-Γ= (,C≃ refl (to≃ (TyNat (δᴹ ^ᴹ Aᴹ)))))
          (∣ δᴹ ^ᴹ Aᴹ ∣s ^ B [ Pr (Δᴹ ,Cᴹ Aᴹ) ]T)
          ∘
          (π₁ id ^ (A [ δ ]T) [ Pr Γᴹ ]T ^ Aᴹ [ δᴹ ]Tᴹ ,s
           coe (TmΓ= (pΠᴹ Γᴹ)) (app (coe (TmΓ= (pΠᴹ' Γᴹ)) (π₂ id)) [ π₁ id ]t))

    Goal = to≃ ,∘
         ◾̃ ,s≃ (,C≃ (,C≃ p6 p7) pAᴹ)
               refl
               p9
               r̃
               p8
         ◾̃ to≃ (,∘ ⁻¹ ◾ coe∘ (,C≃ refl (to≃ (TyNat (δᴹ ^ᴹ Aᴹ)))) ⁻¹)
