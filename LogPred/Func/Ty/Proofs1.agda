{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Func.Ty.Proofs1 where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

module Π[]ᴹ
  {Γ : Con} {Γᴹ : Conᴹ Γ} {Δ : Con} {Δᴹ : Conᴹ Δ} {δ : Tms Γ Δ}
  {δᴹ : Tmsᴹ Γᴹ Δᴹ δ} {A : Ty Δ} {Aᴹ : Tyᴹ Δᴹ A} {B : Ty (Δ , A)}
  where

  abstract
    p1 : ∣ Γᴹ ∣C , Π A B [ δ ]T [ Pr Γᴹ ]T ≡ ∣ Γᴹ ∣C , Π A B [ Pr Δᴹ ]T [ ∣ δᴹ ∣s ]T
    p1 = ,C= refl ([][]T ◾ []T= refl refl refl (PrNat δᴹ ⁻¹) ◾ [][]T ⁻¹)

  abstract
    p3 : A [ Pr Δᴹ ∘ ∣ δᴹ ∣s ]T ≡ A [ δ ]T [ Pr Γᴹ ]T
    p3 = []T= refl refl refl (PrNat δᴹ) ◾ [][]T ⁻¹

  abstract
    p2 : ∣ Γᴹ ∣C , A [ Pr Δᴹ ∘ ∣ δᴹ ∣s ]T ≡ ∣ Γᴹ ∣C , A [ δ ]T [ Pr Γᴹ ]T
    p2 = ,C= refl p3

  abstract
    p4 : (Pr Δᴹ ∘ ∣ δᴹ ∣s) ∘ π₁ id ,s coe (ap (Tm (∣ Γᴹ ∣C , A [ Pr Δᴹ ∘ ∣ δᴹ ∣s ]T)) [][]T) (π₂ id)
       ≃ (δ ∘ π₁ id ,s coe (ap (Tm (Γ , A [ δ ]T)) [][]T) (π₂ id)) ∘ (Pr Γᴹ ∘ π₁ id ,s coe (ap (Tm (∣ Γᴹ ∣C , A [ δ ]T [ Pr Γᴹ ]T)) [][]T) (π₂ id))
    p4 = ,s≃ p2
             refl
             ( to≃ (ap (λ z → z ∘ π₁ id) (PrNat δᴹ))
             ◾̃ to≃ ass
             ◾̃ ∘≃ p2
                  ( ∘≃ p2 (π₁id≃ refl (to≃ p3))
                  ◾̃ to≃ (π₁idβ ⁻¹))
             ◾̃ to≃ (ass ⁻¹))
             r̃
             ( uncoe (ap (Tm (∣ Γᴹ ∣C , A [ Pr Δᴹ ∘ ∣ δᴹ ∣s ]T)) [][]T) ⁻¹̃
             ◾̃ π₂id≃ refl (to≃ p3)
             ◾̃ uncoe (ap (Tm (∣ Γᴹ ∣C , A [ δ ]T [ Pr Γᴹ ]T)) [][]T)
             ◾̃ π₂idβ ⁻¹̃
             ◾̃ coe[]t' [][]T ⁻¹̃
             ◾̃ uncoe (TmΓ= [][]T))
       ◾̃ to≃ (,∘ ⁻¹)

  abstract
    pA : A [ Pr Δᴹ ]T [ π₁ id ]T [ coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (∣ δᴹ ∣s ^ Π A B [ Pr Δᴹ ]T) ]T
       ≃ A [ δ ]T [ Pr Γᴹ ]T [ π₁ {∣ Γᴹ ∣C , Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T} id ]T
    pA
      = to≃ ([][]T ◾ [][]T)
      ◾̃ []T≃ (,C= refl ([]T= refl refl Π[] refl))
             ( ∘≃' refl refl (,C= refl ([]T= refl refl Π[] refl)) r̃
                   ( ∘≃ p1 (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                   ◾̃ to≃ π₁id∘
                   ◾̃ to≃ π₁β
                   ◾̃ ∘≃ (,C= refl
                             ( [][]T
                             ◾ Π[]
                             ◾ Π= refl p3 (from≃ (uncoe (Ty= p2) ⁻¹̃ ◾̃ []T≃ p2 p4 ◾̃ to≃ ([][]T ⁻¹)))
                             ◾ Π[] ⁻¹))
                        (π₁id≃ refl
                               ( to≃ ([][]T ◾ Π[])
                               ◾̃ Π≃ refl
                                    (to≃ p3)
                                    ([]T≃ p2 p4 ◾̃ to≃ ([][]T ⁻¹))
                               ◾̃ to≃ (Π[] ⁻¹))))
             ◾̃ to≃ (ass ⁻¹)
             ◾̃ to≃ (ap (λ z → z ∘ π₁ id) (PrNat δᴹ))
             ◾̃ to≃ ass)
      ◾̃ to≃ ([][]T ⁻¹ ◾ [][]T ⁻¹)

  abstract
    p5 : Π A B [ Pr Δᴹ ]T [ ∣ δᴹ ∣s ]T ≡ Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T
    p5 = [][]T ◾ []T= refl refl refl (PrNat δᴹ) ◾ [][]T ⁻¹ ◾ []T= refl refl Π[] refl

  abstract
    pAᴹ : Aᴹ [ π₁ id ^ A [ Pr Δᴹ ]T ]T
             [ coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (∣ δᴹ ∣s ^ Π A B [ Pr Δᴹ ]T) ^ A [ Pr Δᴹ ]T [ π₁ id ]T ]T
        ≃ Aᴹ [ δᴹ ]Tᴹ [ π₁ {A = Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T} id ^ A [ δ ]T [ Pr Γᴹ ]T ]T
    pAᴹ = to≃ [][]T
      ◾̃ []T≃ (,C≃ (,C= refl (ap (λ z → z [ Pr Γᴹ ]T) Π[])) pA)
             ( ∘^ ⁻¹̃
             ◾̃ ^≃' (,C= refl ([]T= refl refl Π[] refl))
                   ( ∘≃ p1 (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                   ◾̃ to≃ π₁idβ
                   ◾̃ ∘≃ (,C= refl p5) (π₁id≃ refl (to≃ p5)))
             ◾̃ ∘^
             ◾̃ ∘≃' (,C= refl ([][]T ◾ p3))
                   refl
                   (,C= refl ([]T= refl refl ([][]T ◾ p3) refl))
                   (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))))
                   {π₁ {A = Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T} id ^ A [ Pr Δᴹ ]T [ ∣ δᴹ ∣s ]T}
                   {π₁ id ^ A [ δ ]T [ Pr Γᴹ ]T}
                   (^≃'' ([][]T ◾ p3)))
      ◾̃ to≃ ([][]T ⁻¹)

  abstract
    p6 : ∣ Γᴹ ∣C , Π A B [ δ ]T [ Pr Γᴹ ]T ≡ ∣ Γᴹ ∣C , Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T
    p6 = p1 ◾ ,C= refl p5

  abstract
    p7 : A [ Pr Δᴹ ]T [ π₁ id ]T
           [ coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (∣ δᴹ ∣s ^ Π A B [ Pr Δᴹ ]T) ]T
       ≃ A [ δ ]T [ Pr Γᴹ ]T [ π₁ {A = Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T} id ]T
    p7 = to≃ ([][]T ◾ [][]T)
      ◾̃ []T≃ p6
             ( ∘≃ p6
                  ( ∘≃ (p6 ◾ ,C≃ refl (to≃ p5 ⁻¹̃))
                       (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                  ◾̃ to≃ (∘π₁id {ρ = ∣ δᴹ ∣s} ⁻¹)
                  ◾̃ ∘≃ (,C≃ refl (to≃ p5)) (π₁id≃ refl (to≃ p5)))
             ◾̃ to≃ (ass ⁻¹ ◾ ap (λ z → z ∘ π₁ id) (PrNat δᴹ) ◾ ass))
      ◾̃ to≃ ([][]T ⁻¹ ◾ [][]T ⁻¹)

  abstract
    s' : ((B [ Pr Δᴹ ^ A ]T) [ π₁ id ^ A [ Pr Δᴹ ]T ]T) [
        coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (∣ δᴹ ∣s ^ Π A B [ Pr Δᴹ ]T)
        ^ (A [ Pr Δᴹ ]T) [ π₁ id ]T
        ]T
        ≃
        ((B [ δ ^ A ]T) [ Pr Γᴹ ^ A [ δ ]T ]T) [
        π₁ {A = Π (A [ δ ]T) (B [ δ ^ A ]T) [ Pr Γᴹ ]T} id ^ (A [ δ ]T) [ Pr Γᴹ ]T ]T
    s' = to≃ ([][]T ◾ [][]T)
       ◾̃ []T≃ (,C≃ p6 p7)
              ( to≃ (ass ⁻¹)
              ◾̃ (∘≃' (,C≃ refl (to≃ [][]T))
                     refl
                     refl
                     (∘^ ⁻¹̃)
                     (uncoe (TmsΓ-= (,C≃ refl (to≃ [][]T))))
              ◾̃ ∘≃' (,C≃ refl (to≃ ([][]T ⁻¹)))
                    refl
                    refl
                    (uncoe (Tms-Γ= (,C≃ refl (to≃ ([][]T ⁻¹)))))
                    (uncoe (TmsΓ-= (,C≃ refl (to≃ [][]T))) ⁻¹̃)
              ◾̃ coe∘'' ([][]T ⁻¹)
              ◾̃ ∘^ ⁻¹̃
              ◾̃ ^≃' p6
                    ( to≃ ass
                    ◾̃ ∘≃ p6
                         ( to≃ (π₁id∘coe (,C≃ refl (to≃ (TyNat δᴹ))))
                         ◾̃ uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃
                         ◾̃ to≃ π₁β
                         ◾̃ ∘≃ (,C≃ refl (to≃ (TyNat δᴹ)) ◾ p6)
                              (π₁id≃ refl (to≃ (TyNat δᴹ ◾ ap (λ z → z [ Pr Γᴹ ]T) Π[]))))
                    ◾̃ to≃ ( ass ⁻¹
                          ◾ ap (λ z → z ∘ π₁ id) (PrNat δᴹ))))
              ◾̃ ∘^
              ◾̃ ∘≃' (,C≃ refl (to≃ ([][]T ⁻¹)))
                    refl
                    (,C≃ refl (to≃ (ap (λ z → z [ π₁ id ]T) ([][]T ⁻¹))))
                    ∘^
                    (^≃''' (,C≃ refl r̃)
                           refl
                           (π₁id≃ refl r̃)
                           (to≃ ([][]T ⁻¹)))
              ◾̃ to≃ ass)
       ◾̃ to≃ ([][]T ⁻¹ ◾ [][]T ⁻¹)
