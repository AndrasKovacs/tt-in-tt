{-# OPTIONS --no-eta #-}

module LogPred.Core where

open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl

-- contexts

•ᴹ : Conᴹ •
•ᴹ = record { ∣_∣C = • ; Pr = ε }

_,Cᴹ_ : {Γ : Con}(Γᴹ : Conᴹ Γ){A : Ty Γ}(Aᴾ : Ty (∣ Γᴹ ∣C , A [ Pr Γᴹ ]T))
      → Conᴹ (Γ , A)
_,Cᴹ_ {Γ} Γᴹ {A} Aᴾ = record
  { ∣_∣C = (∣ Γᴹ ∣C ,  A [ Pr Γᴹ ]T) , Aᴾ
  ; Pr   = (Pr Γᴹ ^ A) ∘ π₁ id
  }

-- types

abstract
  TyNat : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}
          {Γᴹ : Conᴹ Γ}{Δᴹ : Conᴹ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
        → A [ Pr Δᴹ ]T [ ∣ δᴹ ∣s ]T ≡ A [ δ ]T [ Pr Γᴹ ]T
  TyNat δᴹ = [][]T ◾ ap (_[_]T _) (PrNat δᴹ) ◾ [][]T ⁻¹

_[_]Tᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{A : Ty Δ}(Aᴾ : Ty (∣ Δᴹ ∣C , A [ Pr Δᴹ ]T))
         {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ) → Ty (∣ Γᴹ ∣C , A [ δ ]T [ Pr Γᴹ ]T)
_[_]Tᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{A} Aᴹ {δ} δᴹ
  = Aᴹ [ coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (∣ δᴹ ∣s ^ A [ Pr Δᴹ ]T) ]T

-- substitutions

εᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ} → Tmsᴹ Γᴹ •ᴹ ε
εᴹ = record { ∣_∣s = ε ; PrNat = εη ◾ εη ⁻¹ }

abstract
  p,sᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
       {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{t : Tm Γ (A [ δ ]T)}
     → Aᴹ [ δᴹ ]Tᴹ [ id ,s coe (TmΓ= ([id]T ⁻¹)) (t [ Pr Γᴹ ]t) ]T
     ≡ Aᴹ [ ∣ δᴹ ∣s ,s coe (TmΓ= (TyNat δᴹ ⁻¹)) (t [ Pr Γᴹ ]t) ]T
  p,sᴹ δᴹ  = [][]T ◾ ap (_[_]T _) (^∘<> (TyNat δᴹ))

abstract
  PrNat,sᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{t : Tm Γ (A [ δ ]T)}{tᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) t}
           → Pr (Δᴹ ,Cᴹ Aᴹ) ∘ (∣ δᴹ ∣s
                              ,s coe (TmΓ= (TyNat δᴹ ⁻¹)) (t [ Pr Γᴹ ]t)
                              ,s coe (TmΓ= (p,sᴹ δᴹ)) tᴹ)
           ≡ (δ ,s t) ∘ Pr Γᴹ

  PrNat,sᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{t}{tᴹ}

    = ass
    ◾ ap (_∘_ (Pr Δᴹ ^ A)) π₁idβ
    ◾ ,∘
    ◾ ,s≃' ( ass
           ◾ ap (_∘_ (Pr Δᴹ)) π₁idβ
           ◾ PrNat δᴹ)
           ( coecoe[]t [][]T [][]T
           ◾̃ π₂idβ
           ◾̃ uncoe (TmΓ= (TyNat δᴹ ⁻¹)) ⁻¹̃ ◾̃ uncoe (TmΓ= [][]T))
    ◾ ,∘ ⁻¹

_,sᴹ_
  : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
    {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
    {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
    {t : Tm Γ (A [ δ ]T)}(tᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) t)
  → Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) (δ ,s t)
_,sᴹ_ {Γᴹ = Γᴹ} δᴹ {t = t} tᴹ = record
  { ∣_∣s  = ( ∣ δᴹ ∣s
           ,s coe (TmΓ= (TyNat δᴹ ⁻¹)) (t [ Pr Γᴹ ]t))
           ,s coe (TmΓ= (p,sᴹ δᴹ)) tᴹ
  ; PrNat = PrNat,sᴹ }

idᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ} → Tmsᴹ Γᴹ Γᴹ id
idᴹ = record { ∣_∣s = id ; PrNat = idr ◾ idl ⁻¹ }

abstract
  PrNat∘ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{σ : Tms Δ Σ}{σᴹ : Tmsᴹ Δᴹ Σᴹ σ}
            {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
          → Pr Σᴹ ∘ (∣ σᴹ ∣s ∘ ∣ δᴹ ∣s) ≡ (σ ∘ δ) ∘ Pr Γᴹ
  PrNat∘ᴹ {σ = σ}{σᴹ}{δ}{δᴹ}

    = ass ⁻¹
    ◾ ap (λ z → z ∘ ∣ δᴹ ∣s) (PrNat σᴹ)
    ◾ ass
    ◾ ap (λ z → σ ∘ z) (PrNat δᴹ)
    ◾ ass ⁻¹

_∘ᴹ_ : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{Σ : Con}{Σᴹ : Conᴹ Σ}
       {σ : Tms Δ Σ}(σᴹ : Tmsᴹ Δᴹ Σᴹ σ){δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
     → Tmsᴹ Γᴹ Σᴹ (σ ∘ δ)
σᴹ ∘ᴹ δᴹ = record
  { ∣_∣s  = ∣ σᴹ ∣s ∘ ∣ δᴹ ∣s
  ; PrNat = PrNat∘ᴹ {σᴹ = σᴹ}{δᴹ = δᴹ}
  }

abstract
  PrNatπ₁ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
             {δ : Tms Γ (Δ , A)}{δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ}
           → Pr Δᴹ ∘ π₁ (π₁ (∣ δᴹ ∣s)) ≡ π₁ δ ∘ Pr Γᴹ
  PrNatπ₁ᴹ {Δᴹ = Δᴹ}{δᴹ = δᴹ}

    = ap (_∘_ (Pr Δᴹ))
         ( ap π₁
              ((π₁∘ ◾ ap π₁ idl) ⁻¹)
         ◾ (π₁∘ ◾ ap π₁ idl) ⁻¹)
    ◾ ass ⁻¹
    ◾ ass ⁻¹
    ◾ π₁β ⁻¹
    ◾ ap π₁ (,∘ ⁻¹ ◾ ap (λ z → z ∘ ∣ δᴹ ∣s) (,∘ ⁻¹) ◾ PrNat δᴹ)
    ◾ π₁∘ ⁻¹

π₁ᴹ
  : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Ty (∣ Δᴹ ∣C , A [ Pr Δᴹ ]T)}
    {δ : Tms Γ (Δ , A)}(δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ)
  → Tmsᴹ Γᴹ Δᴹ (π₁ δ)
π₁ᴹ {Δᴹ = Δᴹ} δᴹ = record { ∣_∣s = π₁ (π₁ (∣ δᴹ ∣s)) ; PrNat = PrNatπ₁ᴹ {Δᴹ = Δᴹ}{δᴹ = δᴹ} }

-- terms

abstract
  TmNat : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}
          {Γᴹ : Conᴹ Γ}{Δᴹ : Conᴹ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
          {t : Tm Δ A}
        → t [ Pr Δᴹ ]t [ ∣ δᴹ ∣s ]t ≃ t [ δ ]t [ Pr Γᴹ ]t
  TmNat δᴹ = [][]t' ◾̃ []t≃'' (PrNat δᴹ) ◾̃ [][]t' ⁻¹̃

abstract
  p[]tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{t : Tm Δ A}
          {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
        → Aᴹ [ < t [ Pr Δᴹ ]t > ]T [ ∣ δᴹ ∣s ]T
        ≡ Aᴹ [ δᴹ ]Tᴹ [ < t [ δ ]t [ Pr Γᴹ ]t > ]T
  p[]tᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{A}{Aᴹ}{t}{δ} δᴹ

    = [][]T
    ◾ ap (_[_]T Aᴹ)
         ( ,∘
         ◾ ,s≃' idl
                ( coecoe[]t ([id]T ⁻¹) [][]T
                ◾̃ TmNat δᴹ
                ◾̃ uncoe (TmΓ= (TyNat δᴹ ⁻¹)))
         ◾ ^∘<> (TyNat δᴹ) ⁻¹)
    ◾ [][]T ⁻¹

_[_]tᴹ
  : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
    {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
    {t : Tm Δ A}(tᴹ : Tmᴹ Δᴹ Aᴹ t)
    {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
  → Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) (t [ δ ]t)
tᴹ [ δᴹ ]tᴹ = coe (TmΓ= (p[]tᴹ δᴹ)) (tᴹ [ ∣ δᴹ ∣s ]t)

abstract
  pπ₂ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}(Δᴹ : Conᴹ Δ){A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
         {δ : Tms Γ (Δ , A)}(δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ)
       → Aᴹ [ π₁ (∣ δᴹ ∣s) ]T
       ≡ Aᴹ [ π₁ᴹ {Δᴹ = Δᴹ} δᴹ ]Tᴹ [ < π₂ δ [ Pr Γᴹ ]t > ]T
  pπ₂ᴹ {Γ}{Γᴹ}{Δ} Δᴹ {A}{Aᴹ}{δ} δᴹ

    = ap (_[_]T Aᴹ)
         ( πη ⁻¹
         ◾ ,s≃' refl
                (π₂≃ (ap π₁ idl ⁻¹ ◾ π₁∘ ⁻¹ ◾ idl ⁻¹)
                ◾̃ π₂[]' ⁻¹̃
                ◾̃ coecoe[]t [][]T [][]T ⁻¹̃
                ◾̃ π₂β' ⁻¹̃
                ◾̃ π₂≃ (,∘ ⁻¹ ◾ ass ⁻¹ ◾ PrNat δᴹ)
                ◾̃ π₂[]' ⁻¹̃
                ◾̃ uncoe (TmΓ= (TyNat (π₁ᴹ {Δᴹ = Δᴹ} δᴹ) ⁻¹)))
         ◾ ^∘<> (TyNat (π₁ᴹ {Δᴹ = Δᴹ} δᴹ)) ⁻¹)
    ◾ [][]T ⁻¹

π₂ᴹ
  : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
    {δ : Tms Γ (Δ , A)}(δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ)
  → Tmᴹ Γᴹ {A [ π₁ δ ]T} (Aᴹ [ π₁ᴹ {Δᴹ = Δᴹ} δᴹ ]Tᴹ) (π₂ δ)
π₂ᴹ {Δᴹ = Δᴹ} δᴹ = coe (TmΓ= (pπ₂ᴹ Δᴹ δᴹ)) (π₂ (∣ δᴹ ∣s))

-- type equalities

abstract
  [id]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
         → _[_]Tᴹ {Γᴹ = Γᴹ} Aᴹ idᴹ ≡[ Declᴹ.TyΓᴹ= d {Γᴹ = Γᴹ} [id]T ]≡ Aᴹ
  [id]Tᴹ {Γ}{Γᴹ}{A}{Aᴹ}
    = from≃ ( uncoe (Declᴹ.TyΓᴹ= d {Γᴹ = Γᴹ} [id]T) ⁻¹̃
            ◾̃ []T≃ (,C≃ refl (to≃ [][]T ◾̃ []T≃ refl (to≃ idl)))
                   ( uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat (idᴹ {Γ}{Γᴹ}))))) ⁻¹̃
                   ◾̃ id^ {∣ Γᴹ ∣C}{A [ Pr Γᴹ ]T})
            ◾̃ to≃ [id]T)

abstract
  [][]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{A : Ty Σ}{Aᴹ : Tyᴹ Σᴹ A}
           {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}{δ : Tms Δ Σ}{δᴹ : Tmsᴹ Δᴹ Σᴹ δ}
         → Aᴹ [ δᴹ ]Tᴹ [ σᴹ ]Tᴹ ≡[ Declᴹ.TyΓᴹ= d {Γᴹ = Γᴹ} [][]T ]≡ Aᴹ [ δᴹ ∘ᴹ σᴹ ]Tᴹ
  [][]Tᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{Σ}{Σᴹ}{A}{Aᴹ}{σ}{σᴹ}{δ}{δᴹ}
    = from≃ ( uncoe (Declᴹ.TyΓᴹ= d [][]T) ⁻¹̃
            ◾̃ to≃ [][]T
            ◾̃ []T≃ p
                   ( ∘≃' (,C≃ refl (to≃ q))
                         refl
                         (,C≃ refl (to≃ ([][]T ◾ [][]T ◾ []T= refl refl refl (ap (_∘_ δ) (PrNat σᴹ ⁻¹) ◾ ass ⁻¹ ◾ ap (λ z → z ∘ (∣ σᴹ ∣s)) (PrNat δᴹ ⁻¹) ◾ ass) ◾ [][]T ⁻¹ ◾ [][]T ⁻¹)))
                         (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                         ( uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat σᴹ)))) ⁻¹̃ ◾̃ ^≃'' q)
                   ◾̃ ∘^ ⁻¹̃
                   ◾̃ uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat (δᴹ ∘ᴹ σᴹ)))))))
    where
    p : ∣ Γᴹ ∣C , A [ δ ]T [ σ ]T [ Pr Γᴹ ]T ≡ ∣ Γᴹ ∣C , A [ δ ∘ σ ]T [ Pr Γᴹ ]T
    p = ,C≃ refl (to≃ [][]T ◾̃ to≃ [][]T ◾̃ []T≃ refl (to≃ (ass ⁻¹)) ◾̃ to≃ [][]T ⁻¹̃)

    q : A [ δ ]T [ Pr Δᴹ ]T ≡ A [ Pr Σᴹ ]T [ ∣ δᴹ ∣s ]T
    q = [][]T ◾ []T= refl refl refl (PrNat δᴹ ⁻¹) ◾ [][]T ⁻¹

-- substitutions equalities

Tmsᴹ= : {Γ Δ : Con}{Γᴹ : Conᴹ Γ}{Δᴹ : Conᴹ Δ}
        {ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
        {ρᴹ₀ : Tmsᴹ Γᴹ Δᴹ ρ₀}{ρᴹ₁ : Tmsᴹ Γᴹ Δᴹ ρ₁}
      → ∣ ρᴹ₀ ∣s ≡ ∣ ρᴹ₁ ∣s → ρᴹ₀ ≡[ Declᴹ.TmsΓΔᴹ= d ρ₂ ]≡ ρᴹ₁
Tmsᴹ= refl {ρᴹ₀ = cTmsᴹ ρᴹs PrNat₀} {cTmsᴹ .ρᴹs PrNat₁} refl = ap (cTmsᴹ ρᴹs) (K PrNat₀ PrNat₁)
  where
    K : {A : Set}{x y : A}(p q : x ≡ y) → p ≡ q
    K refl refl = refl

abstract
  π₁βᴹ
    : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
      {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ} {a : Tm Γ (A [ δ ]T)}
      {aᴹ : Declᴹ.Tmᴹ d Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a} →
      π₁ᴹ (δᴹ ,sᴹ aᴹ) ≡[ Declᴹ.TmsΓΔᴹ= d π₁β ]≡ δᴹ
  π₁βᴹ = Tmsᴹ= π₁β (ap π₁ π₁β ◾ π₁β)

abstract
  πηᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{A : Ty Δ}
        {Aᴹ : Tyᴹ Δᴹ A}{δ : Tms Γ (Δ , A)}{δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ}
      → (π₁ᴹ {Δᴹ = Δᴹ} δᴹ ,sᴹ π₂ᴹ δᴹ) ≡[ Declᴹ.TmsΓΔᴹ= d πη ]≡ δᴹ
  πηᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{A}{Aᴹ}{δ}{δᴹ}
    = Tmsᴹ= πη
            ( ,s≃' ( ,s≃' refl
                          ( uncoe (TmΓ= (TyNat (π₁ᴹ {Δᴹ = Δᴹ} δᴹ) ⁻¹)) ⁻¹̃
                          ◾̃ π₂[]'
                          ◾̃ π₂≃ (PrNat δᴹ ⁻¹ ◾ ass ◾ ,∘)
                          ◾̃ π₂β'
                          ◾̃ coecoe[]t [][]T [][]T
                          ◾̃ π₂[]'
                          ◾̃ π₂≃ (idl ◾ π₁∘ ◾ ap π₁ idl))
                   ◾ πη)
                   ( uncoe (TmΓ= (p,sᴹ (π₁ᴹ {Δᴹ = Δᴹ} δᴹ))) ⁻¹̃
                   ◾̃ uncoe {a = π₂ (∣ δᴹ ∣s)}(TmΓ= (pπ₂ᴹ Δᴹ δᴹ)) ⁻¹̃)
            ◾ πη)

abstract
  ,∘ᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
        {Σ : Con}{Σᴹ : Conᴹ Σ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
        {σ : Tms Σ Γ}{σᴹ : Tmsᴹ Σᴹ Γᴹ σ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
        {a : Tm Γ (A [ δ ]T)}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a} →
        (δᴹ ,sᴹ aᴹ) ∘ᴹ σᴹ ≡[ Declᴹ.TmsΓΔᴹ= d ,∘ ]≡
        ((δᴹ ∘ᴹ σᴹ) ,sᴹ coe (Declᴹ.TmΓᴹ= d [][]T [][]Tᴹ refl) (aᴹ [ σᴹ ]tᴹ))
  ,∘ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{Σ}{Σᴹ}{δ}{δᴹ}{σ}{σᴹ}{A}{Aᴹ}{a}{aᴹ}
    = Tmsᴹ= ,∘
            ( ,∘
            ◾ ,st= ,∘
            ◾ ,s≃' (,s≃' refl
                         ( uncoe (TmΓ= [][]T) ⁻¹̃
                         ◾̃ coe[]t' (TyNat δᴹ ⁻¹)
                         ◾̃ TmNat σᴹ
                         ◾̃ coe[]t' [][]T ⁻¹̃
                         ◾̃ uncoe (TmΓ= (TyNat (δᴹ ∘ᴹ σᴹ) ⁻¹))))
                   ( uncoe (TmΓ= (ap (_[_]T Aᴹ) ,∘)) ⁻¹̃
                   ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                   ◾̃ coe[]t' (p,sᴹ δᴹ)
                   ◾̃ uncoe (TmΓ= (p[]tᴹ σᴹ))
                   ◾̃ uncoe (Declᴹ.TmΓᴹ= d [][]T ([][]Tᴹ {σᴹ = σᴹ}{δᴹ = δᴹ}) refl)
                   ◾̃ uncoe (TmΓ= (p,sᴹ (δᴹ ∘ᴹ σᴹ)))))

-- term equalities

abstract
  π₂βᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{A : Ty Δ}
         {Aᴹ : Tyᴹ Δᴹ A}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
         {a : Tm Γ (A [ δ ]T)}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a}
       → π₂ᴹ (δᴹ ,sᴹ aᴹ)
       ≡[ Declᴹ.TmΓᴹ= d (ap (_[_]T A) π₁β) (Declᴹ.[]Tᴹ= d _[_]Tᴹ Aᴹ π₁β π₁βᴹ) π₂β ]≡
         aᴹ
  π₂βᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{A}{Aᴹ}{δ}{δᴹ}{a}{aᴹ}

    = from≃ ( uncoe {A = Tmᴹ Γᴹ (Aᴹ [ π₁ᴹ {Δᴹ = Δᴹ} (δᴹ ,sᴹ aᴹ) ]Tᴹ) (π₂ (δ ,s a))}
                    {Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a}
                    {π₂ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{A}{Aᴹ}(δᴹ ,sᴹ aᴹ)}
                    (Declᴹ.TmΓᴹ= d (ap (_[_]T A) π₁β) (Declᴹ.[]Tᴹ= d _[_]Tᴹ Aᴹ π₁β (π₁βᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{A}{Aᴹ}{δ}{δᴹ}{a}{aᴹ})) π₂β) ⁻¹̃
            ◾̃ uncoe (TmΓ= (pπ₂ᴹ Δᴹ (δᴹ ,sᴹ aᴹ))) ⁻¹̃
            ◾̃ π₂β'
            ◾̃ uncoe (TmΓ= (p,sᴹ δᴹ)) ⁻¹̃)

c : Coreᴹ d
c = record
  { •ᴹ     = •ᴹ
  ; _,Cᴹ_  = _,Cᴹ_
  ; _[_]Tᴹ = _[_]Tᴹ
  ; εᴹ     = εᴹ
  ; _,sᴹ_  = _,sᴹ_
  ; idᴹ    = idᴹ
  ; _∘ᴹ_   = _∘ᴹ_
  ; π₁ᴹ    = π₁ᴹ
  ; _[_]tᴹ = _[_]tᴹ
  ; π₂ᴹ    = π₂ᴹ
  ; [id]Tᴹ = [id]Tᴹ
  ; [][]Tᴹ = [][]Tᴹ
  ; idlᴹ   = Tmsᴹ= idl idl
  ; idrᴹ   = Tmsᴹ= idr idr
  ; assᴹ   = Tmsᴹ= ass ass
  ; π₁βᴹ   = π₁βᴹ
  ; πηᴹ    = πηᴹ
  ; εηᴹ    = Tmsᴹ= εη εη
  ; ,∘ᴹ    = ,∘ᴹ
  ; π₂βᴹ   = π₂βᴹ
  }

