{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Base
  (U̅  : Ty (• , U))
  (El̅ : Ty (• , U , El (coe (TmΓ= U[]) vz) , U̅ [ wk ]T))
  where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

abstract
  pU : ∀{Γ}(Γᴹ : Conᴹ Γ)(A  : Ty (∣ Γᴹ ∣C))
     → U [ Pr Γᴹ ]T [ wk {A = A} ]T ≡ U [ ε ]T
  pU _ _ = [][]T ◾ U[] ◾ U[] ⁻¹

Uᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ} → Tyᴹ Γᴹ U
Uᴹ {Γ}{Γᴹ} = U̅ [ ε ,s coe (TmΓ= (pU Γᴹ (U [ Pr Γᴹ ]T))) vz ]T

abstract
  pEl : ∀{Γ}(Γᴹ : Conᴹ Γ)
        {Â  : Tm Γ U}(Âᴹ : Tmᴹ Γᴹ (U̅ [ ε ,s coe (TmΓ= (pU Γᴹ (U [ Pr Γᴹ ]T))) vz ]T) Â)
      → El Â [ Pr Γᴹ ]T [ wk {A = El Â [ Pr Γᴹ ]T} ]T
      ≡ El (coe (TmΓ= U[]) vz) [ ε ,s coe (TmΓ= (pU Γᴹ (El Â [ Pr Γᴹ ]T))) (Â [ Pr Γᴹ ]t [ wk ]t) ]T
  pEl Γᴹ Âᴹ = [][]T
         ◾ El[]
         ◾ ap El (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                        ◾̃ [][]t' ⁻¹̃
                        ◾̃ uncoe (TmΓ= ([][]T ◾ U[] ◾ U[] ⁻¹))
                        ◾̃ π₂idβ ⁻¹̃
                        ◾̃ coe[]t' U[] ⁻¹̃
                        ◾̃ uncoe (TmΓ= U[])))
         ◾ El[] ⁻¹

abstract
  pEl' : ∀{Γ}(Γᴹ : Conᴹ Γ)
         {Â  : Tm Γ U}(Âᴹ : Tmᴹ Γᴹ (U̅ [ ε ,s coe (TmΓ= (pU Γᴹ (U [ Pr Γᴹ ]T))) vz ]T) Â)
       → U̅ [ ε ,s coe (TmΓ= (pU Γᴹ (U [ Pr Γᴹ ]T))) vz ]T [ < Â [ Pr Γᴹ ]t > ]T [ wk ]T
       ≡ U̅ [ wk ]T [ ε ,s coe (TmΓ= (pU Γᴹ (El Â [ Pr Γᴹ ]T))) (Â [ Pr Γᴹ ]t [ wk ]t) ,s coe (TmΓ= (pEl Γᴹ Âᴹ)) vz ]T
  pEl' Γᴹ {Â} Âᴹ
    = [][]T
    ◾ [][]T
    ◾ from≃ ([]T≃ refl
                  (to≃ ( ,∘
                       ◾ ,s≃' εη
                              ( uncoe (TmΓ= [][]T) ⁻¹̃
                              ◾̃ coe[]t' (pU Γᴹ (U [ Pr Γᴹ ]T))
                              ◾̃ []t≃ refl (to≃ ,∘)
                              ◾̃ π₂idβ
                              ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                              ◾̃ coe[]t' ([id]T ⁻¹)
                              ◾̃ uncoe (TmΓ= (pU Γᴹ (El Â [ Pr Γᴹ ]T))))
                       ◾ π₁idβ ⁻¹)))
    ◾ [][]T ⁻¹

Elᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ}{Â : Tm Γ U}(Âᴹ : Tmᴹ Γᴹ (Uᴹ {Γ}{Γᴹ}) Â)
    → Tyᴹ Γᴹ (El Â)
Elᴹ {Γ}{Γᴹ}{Â} Âᴹ
  = El̅ [ ε
      ,s coe (TmΓ= (pU Γᴹ (El Â [ Pr Γᴹ ]T))) (Â [ Pr Γᴹ ]t [ wk ]t)
      ,s coe (TmΓ= (pEl Γᴹ Âᴹ)) vz
      ,s coe (TmΓ= (pEl' Γᴹ Âᴹ)) (Âᴹ [ wk ]t) ]T

abstract
  U[]ᴹ
    : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
      {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
    → Uᴹ {Δ}{Δᴹ} [ δᴹ ]Tᴹ ≡[ Declᴹ.TyΓᴹ= d {Γ}{Γᴹ}(U[] {Γ}{Δ}{δ}) ]≡ Uᴹ {Γ}{Γᴹ}
  U[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}
    = from≃ ( uncoe (Declᴹ.TyΓᴹ= d {Γ}{Γᴹ}(U[] {Γ}{Δ}{δ})) ⁻¹̃
            ◾̃ to≃ [][]T
            ◾̃ []T≃ p
                   ( to≃ ,∘
                   ◾̃ ,s≃ p
                         refl
                         (to≃ εη ◾̃ ε≃ p)
                         r̃
                         ( uncoe (TmΓ= [][]T) ⁻¹̃
                         ◾̃ coe[]t' (pU Δᴹ _)
                         ◾̃ []t≃ (p ◾ ,C= refl (U[] ◾ U[] ⁻¹ ◾ [][]T ⁻¹)) (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                         ◾̃ π₂idβ
                         ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                         ◾̃ π₂id≃ refl (to≃ ([][]T ◾ (U[] ◾ U[] ⁻¹)))
                         ◾̃ uncoe (TmΓ= (pU Γᴹ _)))))
    where
      p : ∣ Γᴹ ∣C , U [ δ ]T [ Pr Γᴹ ]T ≡ ∣ Γᴹ ∣C , U [ Pr Γᴹ ]T
      p = ,C= refl ([][]T ◾ (U[] ◾ U[] ⁻¹))

module El[]ᴹ
  {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}
  {δᴹ : Tmsᴹ Γᴹ Δᴹ δ}{Â : Tm Δ U}{Âᴹ : Tmᴹ Δᴹ (Uᴹ {Γᴹ = Δᴹ}) Â}
  where
    abstract
      p : El Â [ δ ]T [ Pr Γᴹ ]T
        ≡ El (coe (TmΓ= U[]) (Â [ δ ]t)) [ Pr Γᴹ ]T
      p = [][]T
        ◾ El[]
        ◾ ap El (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                       ◾̃ [][]t' ⁻¹̃
                       ◾̃ coe[]t' U[] ⁻¹̃
                       ◾̃ uncoe (TmΓ= U[])))
        ◾ El[] ⁻¹

    abstract
      p1 : coe (TmΓ= [][]T)
               (coe (TmΓ= (pU Δᴹ _))
                    (Â [ Pr Δᴹ ]t [ wk ]t) [ coe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) (∣ δᴹ ∣s ^ El Â [ Pr Δᴹ ]T) ]t)
         ≃ coe (TmΓ= (pU {Γ} Γᴹ (El (coe (TmΓ= U[]) (Â [ δ ]t)) [ Pr Γᴹ ]T)))
               (coe (TmΓ= U[]) (Â [ δ ]t) [ Pr Γᴹ ]t [ wk ]t)
      p1 = uncoe (TmΓ= [][]T) ⁻¹̃
         ◾̃ coe[]t' (pU Δᴹ _)
         ◾̃ [][]t'
         ◾̃ [][]t'
         ◾̃ []t≃ (,C= refl p)
                ( ∘≃ (,C= refl p)
                     ( ∘≃ (,C= refl (TyNat δᴹ ⁻¹)) (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                     ◾̃ to≃ π₁idβ
                     ◾̃ ∘≃ (,C= refl (TyNat δᴹ ◾ p)) (π₁id≃ refl (to≃ (TyNat δᴹ ◾ p))))
                ◾̃ to≃ (ass ⁻¹ ◾ ap (_∘ wk) (PrNat δᴹ) ◾ ass))
         ◾̃ [][]t' ⁻¹̃
         ◾̃ coe[]t' U[] ⁻¹̃
         ◾̃ [][]t' ⁻¹̃
         ◾̃ uncoe (TmΓ= (pU Γᴹ _))

    abstract
      ret : Elᴹ Âᴹ [ δᴹ ]Tᴹ
        ≡[ Declᴹ.TyΓᴹ= d {Γ}{Γᴹ} (El[] {Γ}{Δ}{δ}{Â}) ]≡
          Elᴹ (coe (Declᴹ.TmΓᴹ= d {Γ}{Γᴹ} (U[] {Γ}{Δ}{δ}) (U[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}) {Â [ δ ]t} refl)
                   (Âᴹ [ δᴹ ]tᴹ))
      ret
        = from≃ ( uncoe (Declᴹ.TyΓᴹ= d {Γ}{Γᴹ} (El[] {Γ}{Δ}{δ}{Â})) ⁻¹̃
                ◾̃ to≃ [][]T
                ◾̃ []T≃ (,C≃ refl (to≃ p))
                       ( to≃ ,∘
                       ◾̃ ,s≃ (,C≃ refl (to≃ p))
                              {• , U , El (coe (TmΓ= U[]) vz)}
                              refl
                              ( to≃ ,∘
                              ◾̃ ,s≃ (,C≃ refl (to≃ p))
                                     refl
                                     ( to≃ ,∘
                                     ◾̃ ,s≃ (,C≃ refl (to≃ p))
                                           refl
                                           (to≃ εη ◾̃ ε≃ (,C≃ refl (to≃ p)))
                                           r̃
                                           p1)
                                     r̃
                                     ( uncoe (TmΓ= [][]T) ⁻¹̃
                                     ◾̃ coe[]t' (pEl Δᴹ Âᴹ){vz}
                                     ◾̃ []t≃ (,C= refl (TyNat δᴹ ⁻¹)) (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
                                     ◾̃ π₂idβ
                                     ◾̃ uncoe (TmΓ= [][]T) ⁻¹̃
                                     ◾̃ π₂id≃ refl (to≃ (TyNat δᴹ ◾ p))
                                     ◾̃ uncoe (TmΓ= (pEl Γᴹ (coe (Declᴹ.TmΓᴹ= d {Γ}{Γᴹ} (U[] {Γ}{Δ}{δ}) (U[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}) {Â [ δ ]t} refl) (Âᴹ [ δᴹ ]tᴹ))))))
                              r̃
                              ( uncoe (TmΓ= [][]T) ⁻¹̃
                              ◾̃ coe[]t' (pEl' Δᴹ Âᴹ) {Âᴹ [ wk ]t}
                              ◾̃ [][]t'
                              ◾̃ []t≃ (,C= refl (TyNat δᴹ ⁻¹)) (∘≃ (,C= refl (TyNat δᴹ ⁻¹)) (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃) ◾̃ to≃ π₁idβ)
                              ◾̃ []t≃ (,C= refl (TyNat δᴹ ◾ p))
                                     (∘≃ (,C= refl (TyNat δᴹ ◾ p)) (π₁id≃ refl (to≃ (TyNat δᴹ ◾ p))))
                              ◾̃ [][]t' ⁻¹̃
                              ◾̃ coe[]t' (p[]tᴹ δᴹ) ⁻¹̃
                              ◾̃ []t≃' refl refl
                                      ([]T≃' refl
                                             (,C= refl ([][]T ◾ U[] ◾ U[] ⁻¹))
                                             (from≡ (Declᴹ.TyΓᴹ= d {Γ}{Γᴹ}(U[] {Γ}{Δ}{δ})) U[]ᴹ)
                                             (<>≃ refl (to≃ ([][]T ◾ U[] ◾ U[] ⁻¹)) (coe[]t' U[] ⁻¹̃)))
                                      (uncoe (Declᴹ.TmΓᴹ= d {Γ}{Γᴹ} (U[] {Γ}{Δ}{δ}) (U[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}) {Â [ δ ]t} refl)) r̃
                              ◾̃ uncoe (TmΓ= (pEl' Γᴹ (coe (Declᴹ.TmΓᴹ= d {Γ}{Γᴹ} (U[] {Γ}{Δ}{δ}) (U[]ᴹ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}) {Â [ δ ]t} refl) (Âᴹ [ δᴹ ]tᴹ)))))))

b : Baseᴹ d c
b = record
  { Uᴹ    = λ {Γ}{Γᴹ} → Uᴹ {Γ}{Γᴹ}
  ; Elᴹ   = Elᴹ
  ; U[]ᴹ  = U[]ᴹ
  ; El[]ᴹ = El[]ᴹ.ret
  }
