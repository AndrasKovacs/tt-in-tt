module NBE.TT_normalforms2 where

data NCon : Set
data NTy  : NCon → Set
data Var  : (Γ : NCon) → NTy Γ → Set
data Ne   : (Γ : NCon) → NTy Γ → Set
data Nf   : (Γ : NCon) → NTy Γ → Set
data Nfs  : NCon → NCon → Set

data NCon where
  •N   : NCon
  _,N_ : (Γ : NCon) → NTy Γ → NCon

data NTy where
  ΠN  : {Γ : NCon}(A : NTy Γ) → NTy (Γ ,N A) → NTy Γ
  UN  : {Γ : NCon} → NTy Γ
  ElN : {Γ : NCon}(Â : Nf Γ UN) → NTy Γ

_[_]T : {Γ : NCon} → NTy Γ → {Δ : NCon} → Nfs Δ Γ → NTy Δ
_[_]nf : {Γ : NCon}{A : NTy Γ} → Nf Γ A → {Δ : NCon}(σ : Nfs Δ Γ) → Nf Δ (A [ σ ]T)
_[_]ne : {Γ : NCon}{A : NTy Γ} → Ne Γ A → {Δ : NCon}(σ : Nfs Δ Γ) → Nf Δ (A [ σ ]T)
_[_]v : {Γ : NCon}{A : NTy Γ} → Var Γ A → {Δ : NCon}(σ : Nfs Δ Γ) → Nf Δ (A [ σ ]T)

ΠN A B [ σ ]T = ΠN (A [ σ ]T) {!!}
UN [ σ ]T = UN
ElN Â [ σ ]T = ElN (Â [ σ ]nf)

-- wk : {Γ Δ : NCon}{A : NTy Δ} → Nfs Γ (Δ ,N A) → Nfs Γ Δ
-- wk : {Γ : NCon}{A : NTy Γ} → Nfs (Γ ,N A) Γ
wk : {Γ Δ : NCon} → Nfs Γ Δ → {A : NTy Γ} → Nfs (Γ ,N A) Δ
id : {Γ : NCon} → Nfs Γ Γ

data Var where
  vze : ∀{Γ}{A : NTy Γ} → Var (Γ ,N A) (A [ wk id ]T)
  vsu : ∀{Γ}{A B : NTy Γ} → Var Γ A → Var (Γ ,N B) (A [ wk id ]T)

data Ne where
  var : ∀{Γ A}(x : Var Γ A) → Ne Γ A
  appNe : ∀{Γ A B}(f : Ne Γ (ΠN A B))(v : Nf Γ A) → Ne Γ {!B!} -- (B [ < ⌜ v ⌝nf > ]T)

data Nf where
  neuU  : ∀{Γ}(n : Ne Γ UN) → Nf Γ UN
  neuEl : ∀{Γ Â}(n : Ne Γ (ElN Â)) → Nf Γ (ElN Â)
  lamNf : ∀{Γ A B}(v : Nf (Γ ,N A) B) → Nf Γ (ΠN A B)

data Nfs where
  ε      : ∀{Γ} → Nfs Γ •N
  _,nfs_ : ∀{Γ Δ}(σ : Nfs Γ Δ){A : NTy Δ} → Nf Γ (A [ σ ]T) → Nfs Γ (Δ ,N A)

vzeNf : ∀{Γ}{A : NTy Γ} → Nf (Γ ,N A) (A [ wk id ]T)
vzeNf {A = ΠN A B} = {!!}
vzeNf {A = UN} = neuU (var vze)
vzeNf {A = ElN Â} = neuEl (var vze)

wk ε = ε
wk (σ ,nfs v) = wk σ ,nfs {!v [ wk id ]nf!}

id {•N} = ε
id {Γ ,N A} = wk (id {Γ}) ,nfs vzeNf

neuU n [ σ ]nf = n [ σ ]ne
neuEl n [ σ ]nf = n [ σ ]ne
lamNf v [ σ ]nf = lamNf (v [ {!σ!} ]nf)

var x [ σ ]ne = x [ σ ]v
appNe n v [ σ ]ne = {!appNe (n [ σ ]ne) (v [ σ ]nf)!}

vze [ σ ,nfs v ]v = {!v!}
vsu x [ σ ]v = {!!}
