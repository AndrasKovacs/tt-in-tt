module S1 where

open import library

--open import Relation.Binary.PropositionalEquality

data S¹ : Set where
  base : S¹

postulate
  loop : base ≡ base

record Motives : Set₁ where
  field
    S¹ᴹ : Set

record Methods (M : Motives) : Set₁ where
  open Motives M
  field
    baseᴹ : S¹ᴹ
    loopᴹ : baseᴹ ≡ baseᴹ

module rec (M : Motives)(m : Methods M) where

  open Motives M
  open Methods m

  S¹-Rec : S¹ → S¹ᴹ
  S¹-Rec base = baseᴹ

  postulate
    S¹-loopβ : ap S¹-Rec loop ≡ loopᴹ 
  

