{-# OPTIONS --type-in-type #-} 

open import Relation.Binary.PropositionalEquality hiding  ([_])
open import Data.Nat
open import Data.Unit
open import Data.Product

module SimplyTyped where

data Ty : Set where
  ℕ₁ : Ty
  _→F_ : Ty → Ty → Ty

data Con : Set where
  ∙ : Con
  _,_ : Con → Ty → Con

data Tm : Con → Ty → Set
data _⇒_ : Con → Con → Set

data Tm where
  π₂ : { Γ Δ : Con } { A : Ty } → Γ ⇒ ( Δ , A ) → Tm Γ A
  λt : { Γ : Con } { A B : Ty } → Tm (  Γ , A ) B → Tm Γ ( A →F B )
  app : { Γ : Con } { A B : Ty } → Tm Γ ( A →F B ) → Tm ( Γ , A ) B
  _[_] : { Γ Δ : Con } { A : Ty } → Tm Γ A → Δ ⇒ Γ → Tm Δ A

data _⇒_ where
  id : ∀ {Γ} → Γ ⇒ Γ
  ε : { Γ : Con } → Γ  ⇒ ∙
  _∘_ : { Γ Δ Θ : Con } → Δ ⇒ Γ → Θ ⇒ Δ → Θ ⇒ Γ
  _,_  : { Γ Δ : Con } { A : Ty } → Γ ⇒ Δ → Tm Γ A → Γ ⇒ ( Δ , A )
  π₁ : { Γ Δ : Con } { A : Ty } → Γ ⇒ ( Δ , A ) → Γ ⇒ Δ


--Term rules
postulate
  η : { Γ : Con } { A B : Ty } { t : Tm Γ ( A →F B ) } → λt ( app t ) ≡ t
  β : { Γ : Con } { A B : Ty } { t : Tm ( Γ , A ) B } → app ( λt t ) ≡ t
  λ[] : { Γ Δ : Con } { A B : Ty } { t : Tm ( Δ , A ) B } { σ : Γ ⇒ Δ } → ( λt t ) [ σ ] ≡ λt ( t [ ( σ ∘ π₁ id ) , π₂ id  ] )


--⇒ rules
postulate
  idᵣ : { Γ Δ : Con } { δ : Γ ⇒ Δ } → δ ∘ id ≡ δ
  idₗ : { Γ Δ : Con } { δ : Γ ⇒ Δ } → id ∘ δ ≡ δ
  assoc : { Γ Δ Θ τ : Con } { δ : Θ ⇒ τ } { σ : Δ ⇒ Θ } { ρ : Γ ⇒ Δ } → (δ ∘ σ) ∘ ρ ≡ δ ∘ (σ ∘ ρ)
  π₁β : { Γ Δ : Con } { A : Ty } { t : Tm Γ A } { δ : Γ ⇒ Δ } → π₁ (δ , t) ≡ δ
  π₂β : { Γ Δ : Con } { A : Ty } { t : Tm Γ A } { δ : Γ ⇒ Δ } → π₂ (δ , t) ≡ t
  π₁η : { Γ Δ : Con } { A : Ty } { δ : Γ ⇒ ( Δ , A ) } → ( π₁ δ , π₂ δ ) ≡ δ
  ,∘ : { Γ Δ Θ : Con } { A : Ty } { t : Tm Δ A } { δ : Γ ⇒ Δ } { σ : Δ ⇒ Θ } → ( σ , t ) ∘ δ ≡ ( (σ ∘ δ) , t [ δ ] )



--Variables----------------------------------------------------------------
z : { Γ : Con } { A : Ty } → Tm ( Γ , A ) A
z {Γ}{A} = π₂ id

s : { Γ : Con } {A B : Ty } → Tm Γ A → Tm ( Γ , B ) A
s {Γ}{A}{B} t =  t  [ π₁ id ] 
------------------------------------------------------------------------------


_$_ : { Γ : Con } { A B : Ty } → Tm Γ ( A →F B ) → Tm Γ A → Tm Γ B
t₁ $ t₂ = ( app t₁ ) [  id , t₂ ]

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Proof of : π₁ ( σ ∘ δ ) ≡ ( π₁ σ ) ∘ δ

f₁ : { Γ Δ Θ : Con } { A : Ty } { δ : Γ ⇒ Θ } → Θ ⇒ ( Δ , A ) → Γ ⇒ Δ
f₁ {δ = δ} σ =  π₁ ( σ ∘  δ)

π₁∘-lem : { Γ Δ Θ : Con } { A : Ty } { δ : Γ ⇒ Θ } { σ : Θ ⇒ ( Δ , A ) } → π₁ ( σ ∘ δ ) ≡ π₁ ( ( π₁ σ , π₂ σ ) ∘ δ )
π₁∘-lem {Γ}{Δ}{Θ}{A}{δ}{σ} = sym ( cong  f₁  π₁η) 

π₁∘ : { Γ Δ Θ : Con } { A : Ty } { δ : Γ ⇒ Θ } { σ : Θ ⇒ ( Δ , A ) } →  π₁ ( σ ∘ δ ) ≡ ( π₁ σ ) ∘ δ
π₁∘ {Γ}{Δ}{Θ}{A}{δ}{σ} =  trans π₁∘-lem (trans ( cong π₁ ,∘ ) π₁β)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Proof of : π₂ ( σ ∘ δ ) ≡ ( π₂ σ ) [ δ ]

f₂ : { Γ Δ Θ : Con } { A : Ty } { δ : Γ ⇒ Θ } → Θ ⇒ ( Δ , A ) → Tm Γ A
f₂ {δ = δ} σ =  π₂ ( σ ∘  δ)

π₂∘-lem : { Γ Δ Θ : Con } { A : Ty } { δ : Γ ⇒ Θ } { σ : Θ ⇒ ( Δ , A ) } → π₂ ( σ ∘ δ ) ≡ π₂ ( ( π₁ σ , π₂ σ ) ∘ δ )
π₂∘-lem {Γ}{Δ}{Θ}{A}{δ}{σ} = sym ( cong  f₂  π₁η) 

π₂∘ : { Γ Δ Θ : Con } { A : Ty } { δ : Γ ⇒ Θ } { σ : Θ ⇒ ( Δ , A ) } →  π₂ ( σ ∘ δ ) ≡ ( π₂ σ ) [ δ ]
π₂∘ {Γ}{Δ}{Θ}{A}{δ}{σ} =  trans π₂∘-lem (trans ( cong π₂ ,∘ ) π₂β)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Proof of : app ( (λt t) [ σ ] ) ≡ (app λt t) [ σ ∘ π₁ id , π₂ id ]

app[]-lem₁ : { Γ Δ : Con } { A B : Ty } { t : Tm ( Γ , A ) B  } { σ : Δ ⇒ Γ } → app ( (λt t) [ σ ] ) ≡ app ( λt ( t [ σ ∘ π₁ id , π₂ id ] ) )
app[]-lem₁ =  cong app λ[]

f₃ :  { Γ Δ : Con } { B : Ty } { σ : Δ ⇒ Γ } → Tm Γ B  → Tm Δ B
f₃ {σ = σ} t = t [ σ ]

app[]-lem₂ : { Γ Δ : Con } { A B : Ty } { t : Tm ( Γ , A ) B  } { σ : Δ ⇒ Γ } → t [ σ ∘ π₁ id , π₂ id ] ≡ ( app (λt t) ) [ σ ∘ π₁ id , π₂ id ]
app[]-lem₂ {Γ}{Δ}{A}{B} = sym (cong (f₃ {B = B}) β)

app[] : { Γ Δ : Con } { A  B : Ty } { t : Tm ( Γ , A ) B } { σ : Δ ⇒ Γ } → app ( (λt t) [ σ ] ) ≡ ( app (λt t) ) [ σ ∘ π₁ id , π₂ id ]
app[] {σ = σ} = trans app[]-lem₁ (trans β app[]-lem₂)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


{-
_++_ : Con → Con → Con
Γ ++ ∙ = Γ
Γ ++ (Δ , A) = (Γ ++ Δ) , A
-}

record Motives :  Set  where
  constructor c
  field
    Tyᴿ : Set --₁
    Conᴿ : Set --₁
    Tmᴿ : Conᴿ → Tyᴿ → Set
    _⇒ᴿ_ : Conᴿ → Conᴿ → Set

record Methods ( M : Motives ): Set where --Set₁
  constructor c
  open Motives M
  field
    --Methods for 0-constructors
    --Ty
    ℕ₁ᴿ : Tyᴿ
    _→Fᴿ_ : Tyᴿ → Tyᴿ → Tyᴿ
    --Con
    ∙ᴿ : Conᴿ
    _,cᴿ_ : Conᴿ → Tyᴿ →  Conᴿ
    --Tm
    π₂ᴿ : { Γᴿ Δᴿ : Conᴿ } { Aᴿ : Tyᴿ } → Γᴿ ⇒ᴿ ( Δᴿ ,cᴿ Aᴿ ) → Tmᴿ Γᴿ Aᴿ
    λtᴿ : { Γᴿ : Conᴿ } { Aᴿ Bᴿ : Tyᴿ } → Tmᴿ (  Γᴿ ,cᴿ Aᴿ ) Bᴿ → Tmᴿ Γᴿ ( Aᴿ →Fᴿ Bᴿ )
    appᴿ : { Γᴿ : Conᴿ } { Aᴿ Bᴿ : Tyᴿ } → Tmᴿ Γᴿ ( Aᴿ →Fᴿ Bᴿ ) → Tmᴿ ( Γᴿ ,cᴿ Aᴿ ) Bᴿ
    _[_]ᴿ : { Γᴿ Δᴿ : Conᴿ } { Aᴿ : Tyᴿ } → Tmᴿ Γᴿ Aᴿ → Δᴿ ⇒ᴿ Γᴿ → Tmᴿ Δᴿ Aᴿ
    --_⇒_
    idᴿ : ∀ {Γᴿ} → Γᴿ ⇒ᴿ Γᴿ
    εᴿ : { Γᴿ : Conᴿ } → Γᴿ  ⇒ᴿ ∙ᴿ
    _∘ᴿ_ : { Γᴿ Δᴿ Θᴿ : Conᴿ } → Δᴿ ⇒ᴿ Γᴿ → Θᴿ ⇒ᴿ Δᴿ → Θᴿ ⇒ᴿ Γᴿ
    _,sᴿ_  : { Γᴿ Δᴿ : Conᴿ } { Aᴿ : Tyᴿ } → Γᴿ ⇒ᴿ Δᴿ → Tmᴿ Γᴿ Aᴿ → Γᴿ ⇒ᴿ ( Δᴿ ,cᴿ Aᴿ )
    π₁ᴿ : { Γᴿ Δᴿ : Conᴿ } { Aᴿ : Tyᴿ } → Γᴿ ⇒ᴿ ( Δᴿ ,cᴿ Aᴿ ) → Γᴿ ⇒ᴿ Δᴿ
    -- Methods for 1-constructors
    --Tm
    ηᴿ : { Γᴿ : Conᴿ } { Aᴿ Bᴿ : Tyᴿ } { tᴿ : Tmᴿ Γᴿ ( Aᴿ →Fᴿ Bᴿ ) } → λtᴿ ( appᴿ tᴿ ) ≡ tᴿ
    βᴿ : { Γᴿ : Conᴿ } { Aᴿ Bᴿ : Tyᴿ } { tᴿ : Tmᴿ ( Γᴿ ,cᴿ Aᴿ ) Bᴿ } → appᴿ ( λtᴿ tᴿ ) ≡ tᴿ
    λ[]ᴿ : { Γᴿ Δᴿ : Conᴿ } { Aᴿ Bᴿ : Tyᴿ } { tᴿ : Tmᴿ ( Δᴿ ,cᴿ Aᴿ ) Bᴿ } { σᴿ : Γᴿ ⇒ᴿ Δᴿ } → ( λtᴿ tᴿ ) [ σᴿ ]ᴿ ≡ λtᴿ ( tᴿ [ ( σᴿ ∘ᴿ π₁ᴿ idᴿ ) ,sᴿ π₂ᴿ idᴿ  ]ᴿ )
    --_⇒_
    idᵣᴿ : { Γᴿ Δᴿ : Conᴿ } { δᴿ : Γᴿ ⇒ᴿ Δᴿ } → δᴿ ∘ᴿ idᴿ ≡ δᴿ
    idₗᴿ : { Γᴿ Δᴿ : Conᴿ } { δᴿ : Γᴿ ⇒ᴿ Δᴿ } → idᴿ ∘ᴿ δᴿ ≡ δᴿ
    assocᴿ : { Γᴿ Δᴿ Θᴿ τᴿ : Conᴿ } { δᴿ : Θᴿ ⇒ᴿ τᴿ } { σᴿ : Δᴿ ⇒ᴿ Θᴿ } { ρᴿ : Γᴿ ⇒ᴿ Δᴿ } → (δᴿ ∘ᴿ σᴿ) ∘ᴿ ρᴿ ≡ δᴿ ∘ᴿ (σᴿ ∘ᴿ ρᴿ)
    π₁βᴿ : { Γᴿ Δᴿ : Conᴿ } { Aᴿ : Tyᴿ } { tᴿ : Tmᴿ Γᴿ Aᴿ } { δᴿ : Γᴿ ⇒ᴿ Δᴿ } → π₁ᴿ (δᴿ ,sᴿ tᴿ) ≡ δᴿ
    π₂βᴿ : { Γᴿ Δᴿ : Conᴿ } { Aᴿ : Tyᴿ } { tᴿ : Tmᴿ Γᴿ Aᴿ } { δᴿ : Γᴿ ⇒ᴿ Δᴿ } → π₂ᴿ (δᴿ ,sᴿ tᴿ) ≡ tᴿ
    π₁ηᴿ : { Γᴿ Δᴿ : Conᴿ } { Aᴿ : Tyᴿ } { δᴿ : Γᴿ ⇒ᴿ ( Δᴿ ,cᴿ Aᴿ ) } → ( π₁ᴿ δᴿ ,sᴿ π₂ᴿ δᴿ ) ≡ δᴿ
    ,∘ᴿ : { Γᴿ Δᴿ Θᴿ : Conᴿ } { Aᴿ : Tyᴿ } { tᴿ : Tmᴿ Δᴿ Aᴿ } { δᴿ : Γᴿ ⇒ᴿ Δᴿ } { σᴿ : Δᴿ ⇒ᴿ Θᴿ } → ( σᴿ ,sᴿ tᴿ ) ∘ᴿ δᴿ ≡ ( (σᴿ ∘ᴿ δᴿ) ,sᴿ (tᴿ [ δᴿ ]ᴿ) )




module rec (mot : Motives) (met : Methods mot)  where
  open Motives mot
  open Methods met

  con-rec : Con → Conᴿ
  ty-rec : Ty → Tyᴿ
  tm-rec : { Γ : Con } { A : Ty } → Tm Γ A → Tmᴿ ( con-rec Γ ) ( ty-rec A )
  ⇒-rec : { Γ Δ : Con } → Γ ⇒ Δ → ( con-rec Γ ) ⇒ᴿ ( con-rec Δ )


  con-rec ∙ = ∙ᴿ
  con-rec (Γ , A) =   con-rec Γ ,cᴿ ty-rec A

  ty-rec ℕ₁ = ℕ₁ᴿ
  ty-rec (A →F B) =  ty-rec A →Fᴿ ty-rec B
  
  tm-rec (π₂ δ) = π₂ᴿ ( ⇒-rec δ )
  tm-rec (λt t) = λtᴿ (tm-rec t)
  tm-rec (app t) = appᴿ (tm-rec t)
  tm-rec (t [ δ ]) =  tm-rec t [ ⇒-rec δ ]ᴿ
 
  ⇒-rec id = idᴿ
  ⇒-rec ε = εᴿ
  ⇒-rec (δ ∘ σ) =  ( ⇒-rec δ ) ∘ᴿ ( ⇒-rec σ )
  ⇒-rec (δ , t) =  ⇒-rec δ ,sᴿ tm-rec t
  ⇒-rec (π₁ δ) = π₁ᴿ (⇒-rec δ)



------ Interpretation in 'Set' ------
-- First using pattern matching
{-
inter-con : Con → Set
inter-ty : Ty → Set
inter-tm : { Γ : Con } { A :  Ty } → Tm Γ A → inter-con Γ → inter-ty A
inter-⇒ : { Γ Δ : Con } → Γ ⇒ Δ → inter-con Γ → inter-con Δ


inter-ty ℕ₁ = ℕ
inter-ty (A →F B) =  inter-ty A → inter-ty B

inter-con ∙ = ⊤
inter-con (Γ , A) =  inter-con Γ × inter-ty A

inter-tm (π₂ δ) = λ icγ →  proj₂ (inter-⇒ δ icγ)
inter-tm (λt t) = λ icγ ita → inter-tm t (icγ , ita)
inter-tm (app t)  = λ {(icγ , ita) →  inter-tm t icγ ita}
inter-tm (t [ δ ]) = λ icγ → inter-tm t (inter-⇒ δ icγ)

inter-⇒ id icΓ =  icΓ
inter-⇒ ε icΓ = tt
inter-⇒ (δ ∘ σ) icΓ = inter-⇒ δ (inter-⇒ σ icΓ)
inter-⇒ (δ , t) icΓ = inter-⇒ δ icΓ , inter-tm t icΓ 
inter-⇒ (π₁ δ) icΓ = proj₁ (inter-⇒ δ icΓ)

-}


-- Second using recursors

mot : Motives
mot = record { 
               Tyᴿ = Set
             ; Conᴿ = Set
             ; Tmᴿ = λ Γᴿ Aᴿ → Γᴿ → Aᴿ
             ; _⇒ᴿ_ = λ Γᴿ Δᴿ → Γᴿ → Δᴿ
             }
    

met : Methods mot
met = record {
               ℕ₁ᴿ = ℕ
             ; _→Fᴿ_ = λ Aᴿ Bᴿ → Aᴿ → Bᴿ
             ; ∙ᴿ = ⊤
             ; _,cᴿ_ = λ Γᴿ Aᴿ → Γᴿ × Aᴿ
             ; π₂ᴿ = λ δ γᴿ → proj₂ (δ γᴿ)
             ; λtᴿ = λ t γᴿ Aᴿ → t (γᴿ , Aᴿ) 
             ; appᴿ = λ { t ( γᴿ , Aᴿ )  →  t γᴿ Aᴿ }
             ; _[_]ᴿ =  λ t δ γᴿ → t (δ γᴿ)
             ; idᴿ = λ Γᴿ → Γᴿ
             ; εᴿ = λ Γᴿ → tt
             ; _∘ᴿ_ = λ δ σ γᴿ → δ (σ γᴿ)
             ; _,sᴿ_ = λ δ t γᴿ → (δ γᴿ) , (t γᴿ)
             ; π₁ᴿ = λ δ γᴿ → proj₁ (δ γᴿ)
             ; ηᴿ = refl
             ; βᴿ = refl
             ; λ[]ᴿ = refl
             ; idᵣᴿ = refl
             ; idₗᴿ = refl
             ; assocᴿ = refl
             ; π₁βᴿ = refl
             ; π₂βᴿ = refl
             ; π₁ηᴿ = refl
             ; ,∘ᴿ = refl
             }



--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Showing that ⟦ Tm Γ ((A → A) → (A→A)) $ Tm Γ (A→A) ⟧ ≡ ⟦ Tm Γ (A→A) ⟧
 
idA : { Γ : Con } { A : Ty } → Tm Γ ( A →F A )
idA {Γ}{A} = λt z

idAA : { Γ : Con } { A : Ty } → Tm Γ ( ( A →F A ) →F ( A →F A ) )
idAA {Γ}{A} = λt ( z { A = A →F A } )

--idA$ : { Γ : Con } {A : Ty } → Tm Γ ( ( A →F A ) →F ( A →F A ) ) → Tm Γ ( A →F A ) → Tm Γ ( A →F A )
--idA$ t₁ t₂ =  t₁ $ t₂


-- inter-idA : { Γ : Con } { A : Ty } → rec.inter-tm ( idAA {Γ}{A} $ idA ) ≡ inter-tm idA
-- inter-idA {Γ}{A} = refl

inter-idA' : { Γ : Con } { A : Ty } → rec.tm-rec mot met ( idAA {Γ}{A} $ idA ) ≡  rec.tm-rec mot met (idA {Γ} {A})
inter-idA' {Γ}{A} = refl
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



