{-# OPTIONS --type-in-type #-} 
--open import Agda.Primitive
open import Data.Product
open import Data.Unit
open import Relation.Binary.PropositionalEquality --hiding  ([_])


module Setoid where


postulate funext-dep  : {A : Set}{B : A → Set}(f g : (a : A) → B a)
                             → ((a : A) → f a ≡ g a)
                             → f ≡ g

postulate funext-dep-impl  : {A : Set}{B : A → Set}(f g : {a : A} → B a)
                             → ((a : A) → f {a} ≡ g {a})
                             → _≡_ {A = {a : A} → B a} f g

postulate funext  : {A B : Set}(f g : A → B)
                             → ((a : A) → f a ≡ g a)
                             → f ≡ g



record Setoid : Set where
  constructor cₛ
  field
    ∣_∣ : Set --i
    _∶_∼_ : ∣_∣ → ∣_∣ → Set 
    _~prp : {a b : ∣_∣} → (p q : _∶_∼_ a b) → p ≡ q
    reflₛ : { a : ∣_∣ } → _∶_∼_ a a
    symₛ : { a b : ∣_∣ } → _∶_∼_ a b → _∶_∼_ b a
    transₛ : { a b c : ∣_∣ } → _∶_∼_ a b → _∶_∼_ b c → _∶_∼_ a c

open Setoid


record _⇒ₛ_ (Γ : Setoid)(Δ : Setoid) : Set where
  constructor cₘ
  field
    f : ∣ Γ ∣ → ∣ Δ ∣
    resp : {γ γ' : ∣ Γ ∣} → Γ ∶ γ ∼ γ' → Δ ∶  f γ ∼ f γ'
    -- A ∶ a ~ a'     is the same as   _∶_~_ A a a'
    -- e.g. sym A is the sym-component of the setoid A
    -- trans A a b c

open _⇒ₛ_ 

⇒ₛ≡-aux : {Γ Δ : Setoid} (f₁ f₂ : ∣ Γ ∣ → ∣ Δ ∣) 
                 → (resp₁ : {γ γ' : ∣ Γ ∣} → Γ ∶ γ ∼ γ' → Δ ∶  f₁ γ ∼ f₁ γ')
                 → (resp₂ : {γ γ' : ∣ Γ ∣} → Γ ∶ γ ∼ γ' → Δ ∶  f₂ γ ∼ f₂ γ')
                 → (q : f₁ ≡ f₂)
                 → (_≡_ {A ={γ γ' : ∣ Γ ∣} → Γ ∶ γ ∼ γ' → Δ ∶  f₂ γ ∼ f₂ γ' } 
                              (subst (λ f → {γ γ' : ∣ Γ ∣} → Γ ∶ γ ∼ γ' → Δ ∶  f γ ∼ f γ') {f₁} {f₂} q resp₁) 
                              resp₂)
                 → _≡_ {A = Γ ⇒ₛ Δ} (cₘ f₁ resp₁) (cₘ f₂ resp₂)
⇒ₛ≡-aux {cₛ ∣_∣ _∶_∼_ _~prp reflₛ symₛ transₛ} {cₛ ∣_∣₁ _∶_∼₁_ _~prp₁ reflₛ₁ symₛ₁ transₛ₁} f₁ .f₁ resp₁ .resp₁ refl refl = refl


resp-prp :  {Γ Δ : Setoid}  (f : ∣ Γ ∣ → ∣ Δ ∣) 
                  → (resp₁ : {γ γ' : ∣ Γ ∣} → Γ ∶ γ ∼ γ' → Δ ∶  f γ ∼ f γ')
                  → (resp₂ : {γ γ' : ∣ Γ ∣} → Γ ∶ γ ∼ γ' → Δ ∶  f γ ∼ f γ')
                  → _≡_ {A = {γ γ' : ∣ Γ ∣} → Γ ∶ γ ∼ γ' → Δ ∶  f γ ∼ f γ'} resp₁ resp₂
resp-prp {Δ = Δ} f resp₁ resp₂ = funext-dep-impl _ _ ( 
                            λ γ → funext-dep-impl _ _ (
                             λ γ' → funext _ _ (
                               λ p → (Δ ~prp) _ _))) 



⇒ₛ≡ : {Γ Δ : Setoid} (δ σ : Γ ⇒ₛ Δ) 
                     → (q : f δ ≡ f σ)
                     → _≡_ {A = Γ ⇒ₛ Δ} δ σ
⇒ₛ≡ {Γ} {Δ} δ σ q = ⇒ₛ≡-aux (f δ) (f σ) (resp δ) (resp σ) q (resp-prp {Γ} {Δ} (f σ) _ (resp σ))

 


_∘ₛ_ : {Γ Δ Θ : Setoid} → Δ ⇒ₛ Θ → Γ ⇒ₛ Δ → Γ ⇒ₛ Θ
_∘ₛ_ {Γ}{Δ}{Θ} ΔΘ ΓΔ = cₘ (λ γ → f ΔΘ (f ΓΔ γ)) (λ γγ' → resp ΔΘ (resp ΓΔ γγ'))

idₛ : {Γ : Setoid} → Γ ⇒ₛ Γ
idₛ {Γ} = cₘ (λ x → x) (λ x → x)

{-
Setoid⇒' : {A B : Setoid} → Setoid
Setoid⇒' {A}{B} = cₛ (A ⇒ₛ B) 
                                   (λ ( g h : A ⇒ₛ B ) →  { a : ∣ A ∣ } → B ∶ f g a ∼ f h a)
                                   (λ {g} → resp g (reflₛ A))
                                   (λ {g}{h} → λ gh → λ {a} → symₛ B gh)
                                   (λ {p}{q}{r} → λ pq qr → transₛ B pq qr)
-}

Setoid⇒ : {Γ Δ : Setoid} → Setoid
Setoid⇒ {Γ}{Δ} = cₛ (Γ ⇒ₛ Δ) 
                                   (λ ( g g' : Γ ⇒ₛ Δ ) →  { a a' : ∣ Γ ∣ } → (Γ ∶ a ∼ a' → Δ ∶ f g a ∼ f g' a'))
                                   (λ {x}{y} p q →  funext-dep-impl _ _ (λ z → funext-dep-impl p q (λ w → funext p q (λ r → (Δ ~prp) (p r) (q r)))))
                                   (λ {g} → λ a → resp g a)
                                   (λ {g}{h} → λ gh a → symₛ Δ (gh (symₛ Γ a)))
                                   (λ {p}{q}{r} → λ pq qr a → transₛ Δ (pq a) (qr (reflₛ Γ))) 


data ∙ₛ : Set where



-- uses K, but this can be fixed very easily
⊤ₛ : Setoid
⊤ₛ = cₛ ⊤  _≡_  irr refl sym trans
          where
            irr : {a b : ⊤} → (x y : a ≡ b) → x ≡ y
            irr {tt} {tt} refl refl = refl


---------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------


×≡ : {A B : Set}{a₁ a₂ : A}{b₁ b₂ : B} → a₁ ≡ a₂ → b₁ ≡ b₂ → (a₁ , b₁) ≡ (a₂ , b₂)
×≡ refl refl = refl

prop-×-closed : {A B : Set} → ((a₁ a₂ : A) → a₁ ≡ a₂) → ((b₁ b₂ : B) → b₁ ≡ b₂) → (x y : A × B) → x ≡ y
prop-×-closed {A}{B} p q (proj₁ , proj₂) (proj₃ , proj₄) = ×≡ (p proj₁ proj₃) (q proj₂ proj₄)


_×ₛ_ : (Γ : Setoid) → (Δ : Setoid) → Setoid
(cₛ ∣Γ∣  _~Γ_ pΓ rΓ sΓ tΓ) ×ₛ (cₛ ∣Δ∣ _~Δ_ pΔ rΔ sΔ tΔ) = cₛ (∣Γ∣ × ∣Δ∣) 
                                                                                           (λ { (a₁ , b₁) (a₂ , b₂) → (a₁ ~Γ a₂) × (b₁ ~Δ b₂) } ) 
                                                                                           (prop-×-closed pΓ pΔ)
                                                                                           (λ {ab} → rΓ , rΔ) 
                                                                                           (λ {ab1}{ab2} x → (sΓ (proj₁ x)) , (sΔ (proj₂ x))) 
                                                                                           (λ {x}{y}{z} xy yz → (tΓ (proj₁ xy) (proj₁ yz)) , (tΔ (proj₂ xy) (proj₂ yz)))



proj₁ₛ : {Γ Δ : Setoid} → (Γ ×ₛ Δ) ⇒ₛ Γ
proj₁ₛ {Γ}{Δ} = cₘ (λ ab → proj₁ ab) (λ {(a₁ , b₁) → a₁}) 

proj₂ₛ : {Γ Δ : Setoid} → (Γ ×ₛ Δ) ⇒ₛ Δ
proj₂ₛ {Γ}{Δ} = cₘ (λ ab → proj₂ ab) (λ {(a₁ , b₁) → b₁})



appₛ : {Γ Δ Θ : Setoid} → Γ ⇒ₛ Setoid⇒ {Δ}{Θ} → (Γ ×ₛ Δ) ⇒ₛ Θ
appₛ (cₘ f resp) = cₘ (λ γa →  _⇒ₛ_.f (f (proj₁ γa)) (proj₂ γa))
                                   (λ {γa₁}{γa₂} γa₁∼γa₂ → resp (proj₁ γa₁∼γa₂) (proj₂ γa₁∼γa₂))

λtₛ : {Γ Δ Θ : Setoid} → (Γ ×ₛ Δ) ⇒ₛ Θ → Γ ⇒ₛ Setoid⇒ {Δ}{Θ}
λtₛ {cₛ γ ∼γ pγ rγ sγ tγ} (cₘ f resp) = cₘ (λ γ₁ → cₘ (λ a₁ → f (γ₁ , a₁))
                                                                                  (λ {a₁}{a₂} a₁∼a₂ → resp ( rγ , a₁∼a₂)))
                                                                 (λ {γ₁}{γ₂} γ₁∼γ₂ {a₁}{a₂} a₁∼a₂ → resp (γ₁∼γ₂ , a₁∼a₂))

_,sₛ_ : {Γ Δ Θ : Setoid} → Γ ⇒ₛ Δ → Γ ⇒ₛ Θ → Γ ⇒ₛ (Δ ×ₛ Θ)
_,sₛ_  (cₘ fΓΔ respΓΔ) (cₘ fΓΘ respΓΘ) = cₘ (λ γ₁ → fΓΔ γ₁ , fΓΘ γ₁)
                                                                  (λ {γ₁}{γ₂} γ₁∼γ₂ →  respΓΔ γ₁∼γ₂ , respΓΘ γ₁∼γ₂)


