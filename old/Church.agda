
-- implementation of http://www.cse.chalmers.se/~coquand/Church.pdf

{-# OPTIONS --type-in-type --without-K #-}

module Church where

open import sets hiding (_!_)
open import sum
open import equality
open import function



------------------------------
-- Syntax
------------------------------

-- we define the syntax of simple type theory

infix 9 _∙_
infix 1 _⇒_

data Con : ℕ → Set
data Ty  : Set
data Tm  : {n : ℕ} → Con n → Ty → Set

-- a context is a list of types
data Con where
  []  : Con zero
  _∙_ : {n : ℕ} → Con n → Ty → Con (suc n)

_!_ : {n : ℕ} → Con n → Fin n → Ty  -- indexing a context
[] ! ()
(_ ∙ A) ! zero  = A
(Γ ∙ _) ! suc n = Γ ! n

-- types: we have a type of propositions and a function type
data Ty where
  o   : Ty
  _⇒_ : (A B : Ty) → Ty

-- terms: usual thing
data Tm where
  var : {n : ℕ} {Γ : Con n} (i : Fin n) → Tm Γ (Γ ! i)
  lam : {n : ℕ} {Γ : Con n} {A B : Ty} → Tm (Γ ∙ A) B → Tm Γ (A ⇒ B)
  app : {n : ℕ} {Γ : Con n} {A B : Ty} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B



------------------------------
-- Semantics
------------------------------

-- now we give a model in MLTT. each context, type and term is given a meaning, i.e. an object in MLTT.
-- a type is interpreted as a type and an equivalence relation on that type
-- a context is interpreted as a type and an equivalence relation on that type
-- a term is interpreted as a function from the interpretation of the context to the interpretation of the type
-- and a function from equivalences between contexts to equivalences between elements of the type

-- definitional equality judgements are interpreted as the propositional equality of this metatheory
-- (or maybe, most of the things hold definitionally?)

-- by defining these interpretations in Agda, we just ...

-- this is like implementing a compiler. the syntax is parsed into the above constructors and
-- typechecked etc. then for each syntactic construct we give another meaning in our metatheory 
-- by the interpretation below (like code generation)



-- 1. Types
--------------------

-- we interpret a type as a type
⟦_⟧ : Ty → Set

-- equality on types
eq  : (A : Ty) → ⟦ A ⟧ → ⟦ A ⟧ → Set

⟦ o ⟧ = Set
⟦ A ⇒ B ⟧ = Σ (⟦ A ⟧ → ⟦ B ⟧) λ f → (x u : ⟦ A ⟧) → eq A x u → eq B (f x) (f u)  -- a function together with a proof that it is extensional

eq (A ⇒ B) f g = (x : ⟦ A ⟧) → eq B (proj₁ f x) (proj₁ g x)
eq o φ₁ φ₂ = (φ₁ → φ₂) × (φ₂ → φ₁)

-- equality on types is reflexive and transitive
one : {A : Ty} {a : ⟦ A ⟧} → eq A a a
one {o} = id , id
one {A ⇒ B} = λ x → one {B}
_◾_ : {A : Ty} {a₁ a₂ a₃ : ⟦ A ⟧} → eq A a₁ a₂ → eq A a₂ a₃ → eq A a₁ a₃   -- \sq5
_◾_ {o} p q = proj₁ q ∘ proj₁ p , proj₂ p ∘ proj₂ q
_◾_ {A ⇒ B} p q = λ x → p x ◾ q x



-- 2. Contexts
--------------------

-- we interpret a context as a type
⟦_⟧C : {n : ℕ} → Con n → Set
⟦ [] ⟧C    = ⊤
⟦ Γ ∙ A ⟧C = ⟦ Γ ⟧C × ⟦ A ⟧     -- instead of ⟦ A ⟧ we will need ⟦ Tm Γ A ⟧ i think

-- equality on contexts
eqC : {n : ℕ} → (Γ : Con n) → ⟦ Γ ⟧C → ⟦ Γ ⟧C → Set
eqC [] tt tt = ⊤
eqC (Γ ∙ A) (σ₁ , a₁) (σ₂ , a₂) = eqC Γ σ₁ σ₂ × eq A a₁ a₂

-- indexing in the interpretation of a context
_!!_ : {n : ℕ} {Γ : Con n} → ⟦ Γ ⟧C → (i : Fin n) → ⟦ Γ ! i ⟧
_!!_ {zero} {[]} σ ()
_!!_ {suc _} {Γ ∙ A} (_ , a) zero    = a
_!!_ {suc _} {Γ ∙ A} (σ , _) (suc i) = σ !! i

-- indexing in the equality of contexts
_!!!_ : {n : ℕ} {Γ : Con n} {σ₁ σ₂ : ⟦ Γ ⟧C} → eqC Γ σ₁ σ₂ → (i : Fin n) → eq (Γ ! i) (σ₁ !! i) (σ₂ !! i)
_!!!_ {zero} q ()
_!!!_ {suc n} {Γ ∙ A} (_ , p) zero = p
_!!!_ {suc n} {Γ ∙ A} (q , _) (suc i) = q !!! i

-- equality on contexts is reflexive
oneC : {n : ℕ} {Γ : Con n} {σ : ⟦ Γ ⟧C} → eqC Γ σ σ
oneC {zero}  {[]} = tt
oneC {suc n} {Γ ∙ A} = oneC , one



-- 2. Terms
--------------------

-- interpretation of terms
⟦_⟧T : {n : ℕ} {Γ : Con n} {A : Ty} → Tm Γ A → (⟦ Γ ⟧C → ⟦ A ⟧)

-- equality of terms
eqT : {n : ℕ} {Γ : Con n} {A : Ty} {σ₁ σ₂ : ⟦ Γ ⟧C} → (t : Tm Γ A) → eqC Γ σ₁ σ₂ → eq A (⟦ t ⟧T σ₁) (⟦ t ⟧T σ₂)

⟦ var i ⟧T σ = σ !! i
⟦ lam t ⟧T σ = (λ a → ⟦ t ⟧T (σ , a)) , (λ a₁ a₂ p → eqT t (oneC , p))
⟦ app t u ⟧T σ = proj₁ (⟦ t ⟧T σ) (⟦ u ⟧T σ)

eqT (var i) q = q !!! i
eqT (lam t) q = λ a → eqT t (q , one)
eqT {σ₁ = σ₁} {σ₂ = σ₂} (app t u) q = proj₂ (⟦ t ⟧T σ₁) (⟦ u ⟧T σ₁) (⟦ u ⟧T σ₂) (eqT u q) ◾ eqT t q (⟦ u ⟧T σ₂)

