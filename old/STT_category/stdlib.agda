{-# OPTIONS --without-K #-}

module stdlib where

-- a standard library for small types, small eliminators

--------------------------------------
-- sigma type
--------------------------------------

infixr 4 _,_

record Σ (A : Set) (B : A → Set) : Set where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁

open Σ public

_×_ : Set → Set → Set
A × B = Σ A (λ _ → B)
infixr 2 _×_

_×′_ : {A A' B B' : Set} → (A → A') → (B → B') → A × B → A' × B'
f ×′ g = λ { (a , b) → f a , g b }
infixr 2 _×′_

--------------------------------------
-- identity type
--------------------------------------

data _≡_ {A : Set} (a : A) : A → Set where
  refl : a ≡ a
infix 4 _≡_

J : {A : Set} {x : A} (P : {y : A} → x ≡ y → Set) → P refl → {y : A} → (w : x ≡ y) → P w
J P pr refl = pr

J' : {A : Set} (P : {x y : A} → x ≡ y → Set) → ({x : A} → P {x} {x} refl) → {x y : A} → (w : x ≡ y) → P w
J' P pr refl = pr

transport : {A : Set} {x y : A} (P : A → Set) → x ≡ y → P x → P y
transport _ refl a = a

transport² : {A : Set} (P : A → Set) {x y : A} {p q : x ≡ y} (r : p ≡ q) (u : P x) → transport P p u ≡ transport P q u
transport² P refl u = refl

_◾_ : {A : Set} {x y z : A} → x ≡ y → y ≡ z → x ≡ z -- \sq5
_◾_ refl p = p
infixl 9 _◾_

_⁻¹ : {A : Set} {x y : A} → x ≡ y → y ≡ x
refl ⁻¹ = refl

ap : {A B : Set} {x y : A} (f : A → B) → (p : x ≡ y) → f x ≡ f y
ap f refl = refl

ap₂ : {A B C : Set} {a a' : A} {b b' : B} (f : A → B → C) → a ≡ a' → b ≡ b' → f a b ≡ f a' b'
ap₂ f refl refl = refl

apd : {A : Set} {B : A → Set} {x y : A} (f : (a : A) → B a) → (p : x ≡ y) → transport B p (f x) ≡ f y
apd f refl = refl

-- notation for an equality over a path
_∶_≡[_]_ : {A : Set} {a a' : A} (P : A → Set) → P a → a ≡ a' → P a' → Set
P ∶ x ≡[ p ] y = transport _ p x ≡ y

apd² : {A : Set} {P : A → Set} {x y : A} {p q : x ≡ y} (f : (a : A) → P a) (r : p ≡ q) → (λ t → P ∶ f x ≡[ t ] f y) ∶ apd f p ≡[ r ] apd f q
apd² f refl = refl

-- identity type for the second universe

data _≡'_ {A : Set₁} (a : A) : A → Set where
  refl : a ≡' a
infix 4 _≡'_

--------------------------------------
-- syntax for equational reasoning
--------------------------------------

infix  4 _IsRelatedTo_
infix  2 _∎
infixr 2 _≡⟨_⟩_
infix  1 begin_
data _IsRelatedTo_ {X : Set} (x y : X) : Set where
  relTo : x ≡ y → x IsRelatedTo y
begin_ : {X : Set} {x y : X} → x IsRelatedTo y → x ≡ y
begin relTo p = p
_≡⟨_⟩_ : {X : Set} (x : X) {y z : X} → x ≡ y → y IsRelatedTo z → x IsRelatedTo z
_ ≡⟨ p ⟩ relTo q = relTo (p ◾ q)
_∎ : {X : Set} (x : X) → x IsRelatedTo x
_∎ _ = relTo refl

--------------------------------------
-- isomorphism
--------------------------------------

record _≃_ (A B : Set) : Set where
  constructor mk≃
  field
    f : A → B
    g : B → A
    β : (a : A) → g (f a) ≡ a
    η : (b : B) → f (g b) ≡ b

--------------------------------------
-- functions
--------------------------------------

id : {A : Set} → A → A
id = λ x → x

const : {A B : Set} (a : A) → B → A
const a _ = a

infixr 9 _∘_

_∘_ : {A : Set} {B : A → Set} {C : {x : A} → B x → Set} →
      ({x : A} (y : B x) → C y) → (g : (x : A) → B x) →
      ((x : A) → C (g x))
f ∘ g = λ x → f (g x)

uncurry : {A : Set} {B : A → Set} {C : {x : A} → B x → Set}
          → ((a : A) (b : B a) → C b) → ((w : Σ A B) → C (proj₂ w))
uncurry f w = f (proj₁ w) (proj₂ w)

curry : {A : Set} {B : A → Set} {C : {x : A} → B x → Set}
        → ((w : Σ A B) → C (proj₂ w)) → ((a : A) (b : B a) → C b)
curry g a b = g (a , b)

-- we rely on definitional η for Pi and Sigma for the following two lemmas
uncurry∘curry : {A : Set} {B : A → Set} {C : {x : A} → B x → Set}
                (f : (w : Σ A B) → C (proj₂ w)) → uncurry {A} {B} {C} (curry f) ≡ f
uncurry∘curry f = refl
curry∘uncurry : {A : Set} {B : A → Set} {C : {x : A} → B x → Set}
                (g : (a : A) (b : B a) → C b) → curry (uncurry g) ≡ g
curry∘uncurry g = refl

Funext : Set₁
Funext = {A : Set} {B : A → Set} {f g : (a : A) → B a} → ((x : A) → f x ≡ g x) → f ≡ g

fun-impl≃ : {A : Set} {B : A → Set} → ((a : A) → B a) ≃ ({a : A} → B a)
fun-impl≃ = λ {A} {B} → mk≃ (λ f {a} → f a) (λ f a → f {a}) (λ _ → refl) (λ _ → refl)

funext-impl : Funext → {X : Set} {Y : X → Set} {f g : {x : X} → Y x}
              → ((x : X) → f {x} ≡ g {x}) → (λ {x} → f {x}) ≡ g
funext-impl funext {f = f} {g = g} h = ap (_≃_.f fun-impl≃) (funext {f = λ a → f {a}} {g = λ a → g {a}} h)

infixr 0 _$_

_$_ : {A : Set} {B : A → Set} → ((x : A) → B x) → ((x : A) → B x)
f $ x = f x


--------------------------------------
-- some laws for identity
--------------------------------------

≡inv : {A : Set} {x y : A} (p : x ≡ y) → p ◾ p ⁻¹ ≡ refl
≡inv refl = refl

≡rid : {A : Set} {x y : A} (p : x ≡ y) → p ◾ refl ≡ p
≡rid refl = refl

◾⁻¹ : {A : Set} {x y z : A} (p : x ≡ y) (q : y ≡ z) → q ⁻¹ ◾ p ⁻¹ ≡ (p ◾ q) ⁻¹
◾⁻¹ refl refl = refl

refl⁻¹ : {X : Set} {x : X} → (refl {a = x}) ⁻¹ ≡ refl
refl⁻¹ = refl

idempot : {A : Set} {x y : A} (p : x ≡ y) → p ≡ (p ⁻¹) ⁻¹
idempot refl = refl

-- if the predicate is constant we can ignore transport
-- 2.3 lemma somewhere in the middle
transport-const : {A : Set} {a a' : A} (p : a ≡ a') (B : Set) (b : B) → transport (λ _ → B) p b ≡ b
transport-const refl _ _  = refl

-- 2.3 last but two lemma
transport-hom : {A : Set} {x y z : A} (P : A → Set) (p : x ≡ y) (q : y ≡ z) (u : P x) → transport P (p ◾ q) u ≡ transport P q (transport P p u)
transport-hom _ refl _ _ = refl

-- 2.3 last but one lemma
transport-nat : {A B : Set} {x y : A} (P : B → Set) (f : A → B) (p : x ≡ y) (r : P (f x)) → transport (λ z → P (f z)) p r ≡ transport P (ap f p) r
transport-nat _ _ refl _ = refl

-- 2.3 last lemma
transport-polymorphicfct-interch : {A : Set} (P Q : A → Set) (f : {x : A} → P x → Q x) {x y : A} (p : x ≡ y) (u : P x) → transport Q p (f u) ≡ f (transport P p u)
transport-polymorphicfct-interch _ _ _ refl u = refl

transport-sym-interch : {A : Set} {x y : A} (P : A → Set) (p : x ≡ y) (u : P x) (v : P y) → transport P p u ≡ v → u ≡ transport P (p ⁻¹) v
transport-sym-interch P refl u .u refl = refl

ap⁻¹ : {A B : Set} {x y : A} (f : A → B) → (p : x ≡ y) → ap f (p ⁻¹) ≡ (ap f p) ⁻¹
ap⁻¹ f refl = refl

hide-transport : {A B : Set} (P : A → Set) (f : {z : A} → P z → B) {x y : A} (u : P x) (p : x ≡ y) → f (transport P p u) ≡ f u
hide-transport {B = B} P f u p = (transport-polymorphicfct-interch P (λ _ → B) f p u) ⁻¹ ◾ transport-const p B (f u)

-- this can be proved with hide-transport and uncurrying f
hide-transport2 : {A B : Set} (P Q : A → Set) (f : {z : A} → P z → Q z → B) {x y : A} (u : P x) (v : Q x) (p : x ≡ y)
                → f (transport P p u) (transport Q p v) ≡ f u v
hide-transport2 _ _ _ _ _ refl = refl

-- TODO: this is a duplicate
transportconst : {A : Set} {x y : A} (B : Set) (p : x ≡ y) (u : B) → transport (λ _ → B) p u ≡ u
transportconst B refl u = refl

apid : {A : Set} {x y : A} (p : x ≡ y) → ap id p ≡ p
apid refl = refl

apdid : {A : Set} {x y : A} {B : A → Set} (p : x ≡ y) → apd id p ≡ transportconst A p x ◾ p
apdid refl = refl

apdconst : {A : Set} {x y : A} {B : A → Set} (b : B x) (p : x ≡ y) → apd (const b) p ≡ transportconst (B x) p b
apdconst b refl = refl

transporteq : {A : Set} {B : A → Set} (f g : (a : A) → B a) {x y : A} (p : x ≡ y) (q : f x ≡ g x)
            → transport (λ x → f x ≡ g x) p q ≡ ((apd f p) ⁻¹ ◾ ap (transport B p) q) ◾ apd g p
transporteq f g refl q = (apid q) ⁻¹ ◾ (≡rid (ap id q)) ⁻¹

atransporteq : {A : Set} {x y : A} (p : x ≡ y) (a : A) (q : a ≡ x) → transport (_≡_ a) p q ≡ q ◾ p
atransporteq refl a q = (≡rid q) ⁻¹

-- a set is a groupoid (h-level 1)
set→groupoid : {A : Set} (set : {x y : A} (p q : x ≡ y) → p ≡ q)
               → {x y : A} (p q : x ≡ y) → Σ (p ≡ q) (λ t → (s : p ≡ q) → t ≡ s)
set→groupoid set p q = set p q ◾ (set q q) ⁻¹ , J (λ {q} s → set p q ◾ set q q ⁻¹ ≡ s) (≡inv (set p p))

-- in a set, all 1-equalities are equal
set→h1eq : {A : Set} (set : {x y : A} (p q : x ≡ y) → p ≡ q)
           → {x y : A} {p q : x ≡ y} (r s : p ≡ q) → r ≡ s
set→h1eq set {p = p} {q = q} r s = (t≡ r) ⁻¹ ◾ t≡ s
  where
    t   = proj₁ (set→groupoid set p q)
    t≡_ = proj₂ (set→groupoid set p q)

-- if we have a fiber P over a set and all the fibers are sets,
-- we prove some stuff
-- maybe this can be done better with the total space
fiberset' : {A : Set} (set : {x y : A} (p q : x ≡ y) → p ≡ q)
            {P : A → Set} (fiberset : {u v : A} (p q : u ≡ v) (a : P u) (b : P v)
                                      (r : P ∶ a ≡[ p ] b) (s : P ∶ a ≡[ q ] b)
                                      → (λ z → P ∶ a ≡[ z ] b) ∶ r ≡[ set p q ] s)
            {u v : A} (p : u ≡ v) {a : P u} {b : P v} {r s : P ∶ a ≡[ p ] b} → r ≡ s
fiberset' set {P} fiberset p {a} {b} {r} {s} =
  transport (λ z → (λ z → P ∶ a ≡[ z ] b) ∶ r ≡[ z ] s)
            (set→h1eq set (set p p) refl)
            (fiberset p p a b r s)

fiberset→h1eq : {A : Set} (set : {x y : A} (p q : x ≡ y) → p ≡ q)
                {P : A → Set} (fiberset : {u v : A} (p q : u ≡ v) (a : P u) (b : P v)
                                          (r : P ∶ a ≡[ p ] b) (s : P ∶ a ≡[ q ] b)
                                          → (λ z → P ∶ a ≡[ z ] b) ∶ r ≡[ set p q ] s)
                {u v : A} {p q : u ≡ v} (w : p ≡ q)
                {a : P u} {b : P v} {r : P ∶ a ≡[ p ] b} {s : P ∶ a ≡[ q ] b}
                {t t' : (λ z → P ∶ a ≡[ z ] b) ∶ r ≡[ w ] s} → t ≡ t'
fiberset→h1eq set fiberset {p = refl} refl {t = t} {t' = t'} =
  set→h1eq (λ _ _ → fiberset' set fiberset refl) t t'  

-- a more general version of transportconst, the previous two should be removed probably
transportconst' : {A B : Set} {x y : A} {u v : B} (p : x ≡ y) → u ≡ v → transport (λ _ → B) p u ≡ v
transportconst' refl refl = refl

transportconst'' : {A B : Set} {x y : A} {u v : B} (p : x ≡ y) → transport (λ _ → B) p u ≡ v → u ≡ v
transportconst'' refl refl = refl

-- paths in a constant fibration are over any path, so we can get rid of the transport
transportconst≃ : {A B : Set} {x y : A} {u v : B} (p : x ≡ y) → (transport (λ _ → B) p u ≡ v) ≃ (u ≡ v)
transportconst≃ refl = mk≃ id id (λ _ → refl) (λ _ → refl)

-- a helper lemma for STT_comprehension, we could have a more general lemma by
-- combining how transport works on Σ types and transportconst
transportΣconst : {A B : Set} {C : B → A → Set} {x y : A} (b : B) {c : C b x} (q : x ≡ y) → proj₁ (transport (λ p → Σ B λ b → C b p) q (b , c)) ≡ b
transportΣconst b refl = refl

isContr : Set → Set
isContr A = Σ A λ a → (x : A) → a ≡ x

singletonsAreContractible : {A : Set}(a : A) → isContr (Σ A λ x → a ≡ x)
singletonsAreContractible a = (a , refl) , (λ { (.a , refl) → refl })

--------------------------------------
-- identity for product types
--------------------------------------

pair= : {A : Set} {B : A → Set} {x y : A} {u : B x} {v : B y} (p : x ≡ y) → transport B p u ≡ v → (x , u) ≡ (y , v)
pair= refl refl = refl

×pair= : {A B : Set} {x y : A} {u v : B} → x ≡ y → u ≡ v → (x , u) ≡ (y , v)
×pair= refl refl = refl

×=β : {A B : Set} {x y : A × B} (r : x ≡ y) → ×pair= (ap proj₁ r) (ap proj₂ r) ≡ r
×=β = J' (λ r → ×pair= (ap proj₁ r) (ap proj₂ r) ≡ r) refl 

×=η : {A B : Set} {a a' : A} {b b' : B} (p : a ≡ a') (q : b ≡ b') → ap proj₁ (×pair= p q) ≡ p × ap proj₂ (×pair= p q) ≡ q
×=η refl refl = refl , refl

-- with ap proj₁ r | ap proj₂ r
-- ×=β r | r₁ | r₂ = {!!}

--------------------------------------
-- booleans
--------------------------------------

data Bool : Set where
  true false : Bool

--------------------------------------
-- natural numbers
--------------------------------------

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

_+_ : ℕ → ℕ → ℕ
zero  + m = m
suc n + m = suc (n + m)
infixl 6 _+_

+-associativity : (a b c : ℕ) → (a + b) + c ≡ a + (b + c)
+-associativity zero b c = refl
+-associativity (suc a) b c = ap suc (+-associativity a b c)

zeroright : (a : ℕ) → a ≡ a + zero
zeroright zero = refl
zeroright (suc a) = ap suc (zeroright a)

sucright : (a b : ℕ) → a + suc b ≡ suc (a + b)
sucright zero b = refl
sucright (suc a) b = ap suc (sucright a b)

+-commutativity : (a b : ℕ) → a + b ≡ b + a
+-commutativity zero    b = zeroright b
+-commutativity (suc a) b = ap suc (+-commutativity a b) ◾ (sucright b a) ⁻¹

--------------------------------------
-- ⊤, ⊥
--------------------------------------

data ⊤ : Set where
  * : ⊤

data ⊥ : Set where

⊥-elim : {A : Set} → ⊥ → A
⊥-elim ()

¬_ : Set → Set
¬ X = X → ⊥

_≢_ : {A : Set} → A → A → Set
x ≢ y = ¬ (x ≡ y)
infix 4 _≢_

--------------------------------------
-- ⊎
--------------------------------------

data _⊎_ (A B : Set) : Set where
  inl : A → A ⊎ B
  inr : B → A ⊎ B
infixr 1 _⊎_

ind⊎ : {A B : Set}(P : A ⊎ B → Set) → ((a : A) → P (inl a)) → ((b : B) → P (inr b)) → (w : A ⊎ B) → P w
ind⊎ P ca cb (inl a) = ca a
ind⊎ P ca cb (inr b) = cb b

--------------------------------------
-- h-levels
--------------------------------------

isSet : Set → Set
isSet A = {x y : A} (p q : x ≡ y) → p ≡ q

×set : {A B : Set} → isSet A → isSet B → isSet (A × B)
×set Aset Bset p q =
    (×=β p) ⁻¹
  ◾ ap (λ { (p₁ , p₂) → ×pair= p₁ p₂ })
       (×pair= (Aset (ap proj₁ p) (ap proj₁ q))
               (Bset (ap proj₂ p) (ap proj₂ q)))
  ◾ ×=β q

--------------------------------------
-- ...
--------------------------------------

data Fin : ℕ → Set where
  zero : {n : ℕ} → Fin (suc n)
  suc  : {n : ℕ} → Fin n → Fin (suc n)

-- codes for an n-length Σ, given by a snoc-list of types
data Telescope : ℕ → Set → Set₁ where
  []  : Telescope zero ⊤
  _∷_ : {n : ℕ} {A : Set} → Telescope n A → (B : A → Set) → Telescope (suc n) (Σ A B)

-- give the Σ-type corresponding to the telescope
⟦_⟧ : {n : ℕ} {A : Set} → Telescope n A → Set
⟦_⟧ {_} {A} _ = A

-- forget about the last m types in the telescope and give the corresponding Σ type
⟦forgetT : {n : ℕ} {A : Set} (m : Fin n) → Telescope n A → Set
⟦forgetT {suc n} {A} zero t       = A
⟦forgetT {suc n} (suc m)  (t ∷ B) = ⟦forgetT m t

-- given an n-length tuple, forget about the last m elements
forget : {n : ℕ} {A : Set} {t : Telescope n A} (m : Fin n) (w : ⟦ t ⟧) → ⟦forgetT m t
forget             zero    w = w
forget {t = t ∷ _} (suc m) (w , _) = forget m w

-- we need a kind of application 

-- _$$_ : {n : ℕ} {A : Set} {B : A → Set} (t : Telescope (suc n) (Σ A B)) → ⟦forgetT (suc zero) t → Set
-- t $$ w = {!!}

-- get the m^th element from a Σ of length n (counting from backwards)
-- projT : {n : ℕ} {A : Set} {t : Telescope n A} (m : Fin n) (w : ⟦ t ⟧) → forgetT m t $$ forget (suc m) w
-- projT = ?
