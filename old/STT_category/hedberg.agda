
{-# OPTIONS --without-K #-}

module hedberg where

open import stdlib

-- proof of Hedberg's theorem as in http://red.cs.nott.ac.uk/~ngk/hedberg.pdf

-- there is a constant endofunction
coll : Set → Set
coll X = Σ (X → X) λ f → (x y : X) → f x ≡ f y

-- the path space is collapsible
path-coll : Set → Set
path-coll X = (x y : X) → coll (x ≡ y)

-- has decidable equality
discrete : Set → Set
discrete X = (x y : X) → ¬ (x ≡ y) ⊎ (x ≡ y)

-- if a type has decidable equality, there is a constant endofunction in it's path space
lemma1 : {X : Set} → discrete X → path-coll X
lemma1 {X} p x y = ind⊎ (λ _ → coll (x ≡ y))
                        (λ x≢y → id          , (λ x≡y → ⊥-elim (x≢y x≡y)))
                        (λ x≡y → (const x≡y) , (λ x≡y x≡y' → refl))
                        (p x y)

-- if there is a constant endofunction in the path space, it is a set
-- the trick:
-- 1. we can show by J that p ≡ f p ◾ (f refl) ⁻¹
-- 2. but because f is constant, the right hand sides are the same for all p : x ≡ y
lemma2 : {X : Set} → path-coll X → isSet X
lemma2 {X} pathcoll {x} {y} p q = begin
      p
    ≡⟨ J (λ p → p ≡ f p ◾ (f refl) ⁻¹) ((≡inv (f refl)) ⁻¹) p ⟩
      f p ◾ (f refl) ⁻¹
    ≡⟨ ap (λ z → z ◾ (f refl) ⁻¹) (fconst p q) ⟩
      f q ◾ (f refl) ⁻¹
    ≡⟨ J (λ q → f q ◾ (f refl) ⁻¹ ≡ q) (≡inv (f refl)) q ⟩
      q
    ∎
  where
    f : {x y : X} → x ≡ y → x ≡ y
    f {x} {y} = proj₁ (pathcoll x y)
    fconst : {x y : X} (p q : x ≡ y) → f p ≡ f q
    fconst {x} {y} = proj₂ (pathcoll x y)

hedberg : {X : Set} → discrete X → isSet X
hedberg = lemma2 ∘ lemma1
