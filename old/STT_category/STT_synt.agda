module STT_synt where

open import stdlib
open import hedberg using (hedberg; discrete)

------------------------------
-- syntax (term model)
------------------------------

data Ty : Set where
  _⇒_ : (A B : Ty) → Ty       -- \=>
  o   : Ty
infix 5 _⇒_

private -- the stuff in this private section could be automated if we used W-types
  Tyinj : {A B : Ty} → o ≡ A ⇒ B → ⊥
  Tyinj p = transport aFamily p *
    where
      aFamily : Ty → Set
      aFamily o = ⊤
      aFamily _ = ⊥

  Tyinj' : {A B A' B' : Ty} → A ⇒ A' ≡ B ⇒ B' → A ≡ B × A' ≡ B'
  Tyinj' refl = refl , refl

  Tydec' : {A B A' B' : Ty} → A ≢ B ⊎ A ≡ B → A' ≢ B' ⊎ A' ≡ B'
           → (A ⇒ A' ≡ B ⇒ B' → ⊥) ⊎ A ⇒ A' ≡ B ⇒ B'
  Tydec' (inl p) _       = inl (p ∘ proj₁ ∘ Tyinj' )
  Tydec' (inr p) (inl q) = inl (q ∘ proj₂ ∘ Tyinj')
  Tydec' (inr p) (inr q) = inr (ap (λ { (A , A') → A ⇒ A'}) (×pair= p q))

  Tydec : discrete Ty -- (A B : Ty) → A ≢ B ⊎ A ≡ B
  Tydec (A ⇒ A') (B ⇒ B') = Tydec' (Tydec A B) (Tydec A' B')
  Tydec (A ⇒ A') o        = inl (Tyinj ∘ _⁻¹)
  Tydec o        (B ⇒ B') = inl Tyinj
  Tydec o         o       = inr refl

Tyset : isSet Ty
Tyset = hedberg Tydec


data Con : Set where
  ◇   : Con                       -- \di2
  _∙_ : (Γ : Con) (A : Ty) → Con  -- \.
infix 9 _∙_

private
  aFamily : Con → Set
  aFamily ◇ = ⊤
  aFamily _ = ⊥

  Coninj : {Γ : Con} {A : Ty} → ◇ ≡ Γ ∙ A → ⊥
  Coninj p = transport aFamily p *

  Coninj' : {Γ Δ : Con} {A B : Ty} → Γ ∙ A ≡ Δ ∙ B → Γ ≡ Δ × A ≡ B
  Coninj' refl = refl , refl

  Condec' : {Γ Δ : Con} {A B : Ty} → Γ ≢ Δ ⊎ Γ ≡ Δ → A ≢ B ⊎ A ≡ B
            → Γ ∙ A ≢ Δ ∙ B ⊎ Γ ∙ A ≡ Δ ∙ B
  Condec' (inl p) _ = inl (p ∘ proj₁ ∘ Coninj')
  Condec' (inr p) (inl q) = inl (q ∘ proj₂ ∘ Coninj')
  Condec' (inr p) (inr q) = inr (ap (λ { (Γ , A) → Γ ∙ A }) (×pair= p q))

  Condec : discrete Con
  Condec ◇ ◇ = inr refl
  Condec ◇ (Δ ∙ A) = inl Coninj
  Condec (Γ ∙ A) ◇ = inl (Coninj ∘ _⁻¹)
  Condec (Γ ∙ A) (Δ ∙ B) = Condec' (Condec Γ Δ) (Tydec A B)

Conset : isSet Con
Conset = hedberg Condec


data Tm : Ty → Con → Set

-- Dan Licata's trick: context morphisms and terms have higher constructors,
-- so we hide their 0-constructors to avoid pattern matching over them.
-- we re-export them with different names later
private
  -- context morphisms
  data _⇨'_ : Con → Con → Set where  -- \r7
    one'   : {Γ : Con} → Γ ⇨' Γ
    _◦'_   : {Γ Δ Θ : Con} → Δ ⇨' Θ → Γ ⇨' Δ → Γ ⇨' Θ
    p'     : {Γ : Con} {A : Ty} → Γ ∙ A ⇨' Γ
    ⟨_⟩'   : {Δ Γ : Con} {A : Ty} → Δ ⇨' Γ × Tm A Δ → Δ ⇨' Γ ∙ A
    !'     : (Γ : Con) → Γ ⇨' ◇
  infix 9 _◦'_
  infix 5 _⇨'_

  data Tm where
    q'    : {Γ : Con} {A : Ty} → Tm A (Γ ∙ A)
    _[_]' : {Δ Γ : Con} {A : Ty} → Tm A Γ → Δ ⇨' Γ → Tm A Δ
    lam'  : {Γ : Con} {A B : Ty} → Tm B (Γ ∙ A) → Tm (A ⇒ B) Γ
    app'  : {Γ : Con} {A B : Ty} → Tm (A ⇒ B) Γ → Tm B (Γ ∙ A)

postulate
  -- higher constructors for _⇨_
  lid   : {Δ Γ : Con} (σ : Δ ⇨' Γ) → one' ◦' σ ≡ σ
  rid   : {Δ Γ : Con} (σ : Δ ⇨' Γ) → σ ◦' one' ≡ σ
  ◦-ass : {Δ Γ Θ Ω : Con} (σ : Θ ⇨' Ω) (δ : Γ ⇨' Θ) (ν : Δ ⇨' Γ) 
          → (σ ◦' δ) ◦' ν ≡ σ ◦' (δ ◦' ν)
  pβ    : {Δ Γ : Con} {A : Ty} (σ : Δ ⇨' Γ) (u : Tm A Δ) → p' ◦' ⟨ σ , u ⟩' ≡ σ
  pqη   : {Δ Γ : Con} {A : Ty} (δ : Δ ⇨' Γ ∙ A) → ⟨ p' ◦' δ , q' [ δ ]' ⟩' ≡ δ
  !uniq : {Γ : Con} → (δ : Γ ⇨' ◇) → δ ≡ !' Γ
  ⇨set  : {Δ Γ : Con} → isSet (Δ ⇨' Γ)

  -- higher constructors for Tm
  []id   : {A : Ty} {Γ : Con} (t : Tm A Γ) → t [ one' ]' ≡ t
  []cong : {A : Ty} {Δ Γ Θ : Con} (t : Tm A Θ) (σ : Γ ⇨' Θ) (δ : Δ ⇨' Γ) 
           → t [ σ ◦' δ ]' ≡ (t [ σ ]') [ δ ]'
  qβ     : {Δ Γ : Con} {A : Ty} (σ : Δ ⇨' Γ) (u : Tm A Δ) → q' [ ⟨ σ , u ⟩' ]' ≡ u
  ⇒β     : {Γ : Con} {A B : Ty} (t : Tm B (Γ ∙ A)) → app' (lam' t) ≡ t
  ⇒η     : {Γ : Con} {A B : Ty} (f : Tm (A ⇒ B) Γ) → lam' (app' f) ≡ f
  lamnat : {Δ Γ : Con} {A B : Ty} (δ : Δ ⇨' Γ) (u : Tm B (Γ ∙ A))
           → lam' (u [ ⟨ δ ◦' p' , q' ⟩' ]') ≡ (lam' u) [ δ ]'
  Tmset  : {A : Ty} {Γ : Con} → isSet (Tm A Γ)

-- re-exporting type formers and 0-constructors that were hidden
_⇨_ = _⇨'_
infix 5 _⇨_
one : {Γ : Con} → Γ ⇨ Γ
one = one'
_◦_ : {Γ Δ Θ : Con} → Δ ⇨ Θ → Γ ⇨ Δ → Γ ⇨ Θ
_◦_ = _◦'_
infix 9 _◦_
p : {Γ : Con} {A : Ty} → Γ ∙ A ⇨ Γ
p = p'
⟨_⟩ : {Δ Γ : Con} {A : Ty} → Δ ⇨ Γ × Tm A Δ → Δ ⇨ Γ ∙ A
⟨_⟩ = ⟨_⟩'
! : (Γ : Con) → Γ ⇨ ◇
! = !'
q : {Γ : Con} {A : Ty} → Tm A (Γ ∙ A)
q = q'
_[_] : {Δ Γ : Con} {A : Ty} → Tm A Γ → Δ ⇨ Γ → Tm A Δ
_[_] = _[_]'
lam : {Γ : Con} {A B : Ty} → Tm B (Γ ∙ A) → Tm (A ⇒ B) Γ
lam = lam'
app : {Γ : Con} {A B : Ty} → Tm (A ⇒ B) Γ → Tm B (Γ ∙ A)
app = app'

pqβ : {Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ)
      → p ◦ ⟨ σ,u ⟩ , q [ ⟨ σ,u ⟩ ] ≡ σ,u
pqβ (σ , u) = ×pair= (pβ σ u) (qβ σ u)


module ⇨-elim
  (P      : {Δ Γ : Con} → Δ ⇨ Γ → Set)
  (mone   : {Γ : Con} → P {Γ} {Γ} one)
  (m◦     : {Γ Δ Θ : Con} {σ : Δ ⇨ Θ} {δ : Γ ⇨ Δ} → P σ → P δ → P (σ ◦ δ))
  (mp     : {Γ : Con} {A : Ty} → P {Γ ∙ A} {Γ} p)
  (m⟨⟩    : {Δ Γ : Con} {A : Ty} {δ : Δ ⇨ Γ} → P δ → (u : Tm A Δ) → P ⟨ δ , u ⟩)
  (m!     : {Γ : Con} → P (! Γ))
  (mlid   : {Δ Γ : Con} {σ : Δ ⇨' Γ} (s : P σ) → P ∶ m◦ mone s ≡[ lid σ ] s)
  (mrid   : {Δ Γ : Con} {σ : Δ ⇨' Γ} (s : P σ) → P ∶ m◦ s mone ≡[ rid σ ] s)
  (m◦-ass : {Δ Γ Θ Ω : Con} {σ : Θ ⇨' Ω} {δ : Γ ⇨' Θ} {ν : Δ ⇨' Γ} (r : P σ)
            (s : P δ) (t : P ν)
            → P ∶ m◦ (m◦ r s) t ≡[ ◦-ass σ δ ν ] m◦ r (m◦ s t))
  (mpβ    : {Δ Γ : Con} {A : Ty} {σ : Δ ⇨' Γ} (s : P σ) (u : Tm A Δ)
            → P ∶ m◦ mp (m⟨⟩ s u) ≡[ pβ σ u ] s)
  (mpqη   : {Δ Γ : Con} {A : Ty} {δ : Δ ⇨' Γ ∙ A} (s : P {Δ} {Γ ∙ A} δ)
            → P {Δ} {Γ ∙ A} ∶ m⟨⟩ (m◦ mp s) (q [ δ ]) ≡[ pqη δ ] s)
  (m!uniq : {Γ : Con} → {δ : Γ ⇨' ◇} (p : P δ) → P ∶ p ≡[ !uniq δ ] m!)
  (m⇨set  : {Δ Γ : Con} {σ δ : Δ ⇨' Γ} (p q : σ ≡ δ) (a : P σ) (b : P δ)
            (r : P ∶ a ≡[ p ] b) (s : P ∶ a ≡[ q ] b)
            → (λ z → P ∶ a ≡[ z ] b) ∶ r ≡[ ⇨set p q ] s)
  where

    -- the eliminator
    -- definitional computation rules for 0-constructors
    elim : {Δ Γ : Con} (σ : Δ ⇨ Γ) → P σ
    elim one'       = mone
    elim (σ ◦' δ)   = m◦ (elim σ) (elim δ)
    elim p'         = mp
    elim ⟨ δ , u ⟩' = m⟨⟩ (elim δ) u
    elim (!' Γ)     = m!

    private -- a weaker version of m⇨set
      m⇨set' : {Δ Γ : Con} {u v : Δ ⇨ Γ} (p : u ≡ v) {a : P u} {b : P v}
               {r s : P ∶ a ≡[ p ] b} → r ≡ s
      m⇨set' = fiberset' ⇨set m⇨set

    -- propositional computation rules for higher constructors can all be proven
    β-lid : {Δ Γ : Con} (σ : Δ ⇨ Γ) → apd elim (lid σ) ≡ mlid (elim σ)
    β-lid σ = m⇨set' (lid σ)
    β-rid : {Δ Γ : Con} (σ : Δ ⇨ Γ) → apd elim (rid σ) ≡ mrid (elim σ)
    β-rid σ = m⇨set' (rid σ)
    β-◦-ass : {Δ Γ Θ Ω : Con} (σ : Θ ⇨ Ω) (δ : Γ ⇨ Θ) (ν : Δ ⇨ Γ)
              → apd elim (◦-ass σ δ ν) ≡ m◦-ass (elim σ) (elim δ) (elim ν)
    β-◦-ass σ δ ν = m⇨set' (◦-ass σ δ ν)
    β-pβ : {Δ Γ : Con} {A : Ty} (σ : Δ ⇨' Γ) (u : Tm A Δ)
           → apd elim (pβ σ u) ≡ mpβ (elim σ) u
    β-pβ σ u = m⇨set' (pβ σ u)
    β-pqη : {Δ Γ : Con} {A : Ty} (δ : Δ ⇨' Γ ∙ A)
            → apd elim (pqη δ) ≡ mpqη (elim δ)
    β-pqη δ = m⇨set' (pqη δ)
    β-!uniq : {Γ : Con} → (δ : Γ ⇨' ◇) → apd elim (!uniq δ) ≡ m!uniq (elim δ)
    β-!uniq δ = m⇨set' (!uniq δ)
    β-⇨set : {Δ Γ : Con} {σ δ : Δ ⇨' Γ} (p q : σ ≡ δ)
             → apd² elim (⇨set p q) ≡ m⇨set p q (elim σ) (elim δ) (apd elim p) (apd elim q)
    β-⇨set p q = fiberset→h1eq ⇨set m⇨set (⇨set p q)


module Tm-elim
  (P       : {A : Ty} {Γ : Con} → Tm A Γ → Set)
  (mq      : {A : Ty} {Γ : Con} → P (q {Γ} {A}))
  (_m[_]   : {Δ Γ : Con} {A : Ty} {t : Tm A Γ} (s : P t) (δ : Δ ⇨ Γ) → P (t [ δ ]))
  (mlam    : {Γ : Con} {A B : Ty} {t : Tm B (Γ ∙ A)} (s : P t) → P (lam t))
  (mapp    : {Γ : Con} {A B : Ty} {f : Tm (A ⇒ B) Γ} (s : P f) → P (app f))
  (m[]id   : {A : Ty} {Γ : Con} {t : Tm A Γ} (s : P t) → P ∶ s m[ one ] ≡[ []id t ] s)
  (m[]cong : {A : Ty} {Δ Γ Θ : Con} {t : Tm A Θ} (s : P t) (σ : Γ ⇨' Θ) (δ : Δ ⇨' Γ)
             → P ∶ s m[ σ ◦ δ ] ≡[ []cong t σ δ ] (s m[ σ ]) m[ δ ])
  (mqβ     : {Δ Γ : Con} {A : Ty} (σ : Δ ⇨' Γ) {u : Tm A Δ} (s : P u)
             → P ∶ mq m[ ⟨ σ , u ⟩ ] ≡[ qβ σ u ] s)
  (m⇒β     : {Γ : Con} {A B : Ty} {t : Tm B (Γ ∙ A)} (s : P t)
             → P ∶ mapp (mlam s) ≡[ ⇒β t ] s)
  (m⇒η     : {Γ : Con} {A B : Ty} {f : Tm (A ⇒ B) Γ} (s : P f)
             → P ∶ mlam (mapp s) ≡[ ⇒η f ] s)
  (mlamnat : {Δ Γ : Con} {A B : Ty} (δ : Δ ⇨' Γ) {u : Tm B (Γ ∙ A)} (s : P u)
             → P ∶ mlam (s m[ ⟨ δ ◦ p , q ⟩ ]) ≡[ lamnat δ u ] mlam s m[ δ ])
  (mTmset  : {A : Ty} {Γ : Con} {u v : Tm A Γ} (p q : u ≡ v) (a : P u) (b : P v)
             (r : P ∶ a ≡[ p ] b) (s : P ∶ a ≡[ q ] b)
             → (λ z → P ∶ a ≡[ z ] b) ∶ r ≡[ Tmset p q ] s)
  where

    -- the eliminator
    -- definitional computation rules for 0-constructors
    elim : {A : Ty} {Γ : Con} (t : Tm A Γ) → P t
    elim q'         = mq
    elim (t [ σ ]') = (elim t) m[ σ ]
    elim (lam' t)   = mlam (elim t)
    elim (app' t)   = mapp (elim t)

    private -- a weaker version of mTmset
      mTmset' : {A : Ty} {Γ : Con} {u v : Tm A Γ} (p : u ≡ v) {a : P u} {b : P v}
                {r s : P ∶ a ≡[ p ] b} → r ≡ s
      mTmset' = fiberset' Tmset mTmset

    -- propositional computation rules for higher constructors can all be proven
    β-[]id : {A : Ty} {Γ : Con} (t : Tm A Γ) → apd elim ([]id t) ≡ m[]id (elim t)
    β-[]id t = mTmset' ([]id t)
    β-[]cong : {A : Ty} {Δ Γ Θ : Con} (t : Tm A Θ) (σ : Γ ⇨' Θ) (δ : Δ ⇨' Γ)
               → apd elim ([]cong t σ δ) ≡ m[]cong (elim t) σ δ
    β-[]cong t σ δ = mTmset' ([]cong t σ δ)
    β-qβ : {Δ Γ : Con} {A : Ty} (σ : Δ ⇨' Γ) (u : Tm A Δ)
           → apd elim (qβ σ u) ≡ mqβ σ (elim u)
    β-qβ σ u = mTmset' (qβ σ u)
    β-⇒β : {Γ : Con} {A B : Ty} (t : Tm B (Γ ∙ A)) → apd elim (⇒β t) ≡ m⇒β (elim t)
    β-⇒β t = mTmset' (⇒β t)
    β-⇒η : {Γ : Con} {A B : Ty} (f : Tm (A ⇒ B) Γ) → apd elim (⇒η f) ≡ m⇒η (elim f)
    β-⇒η f = mTmset' (⇒η f)
    β-lamnat : {Δ Γ : Con} {A B : Ty} (δ : Δ ⇨' Γ) (u : Tm B (Γ ∙ A))
               → apd elim (lamnat δ u) ≡ mlamnat δ (elim u)
    β-lamnat δ u = mTmset' (lamnat δ u)
    β-Tmset : {A : Ty} {Γ : Con} {u v : Tm A Γ} (p q : u ≡ v)
              → apd² elim (Tmset p q)
              ≡ mTmset p q (elim u) (elim v) (apd elim p) (apd elim q)
    β-Tmset p q = fiberset→h1eq Tmset mTmset (Tmset p q)
