
{-# OPTIONS --without-K #-}

module STT_model where

open import stdlib

record STT : Set₁ where
  field
    -----------------------------------------------------------------------
    -- category of contexts
    -----------------------------------------------------------------------
    Con    : Set
    Conset : isSet Con -- TODO: this should be replaced by stating that this is a univalent category
    _⇨_    : Con → Con → Set -- \r7       -- hom_Con
    ⇨set   : {Δ Γ : Con} → isSet (Δ ⇨ Γ)  -- these are sets so this is a 1-category
    one    : {Γ : Con} → Γ ⇨ Γ
    _◦_    : {Δ Γ Θ : Con} → Γ ⇨ Θ → Δ ⇨ Γ → Δ ⇨ Θ -- \bu2
    lid    : {Δ Γ : Con} (σ : Δ ⇨ Γ) → one ◦ σ ≡ σ
    rid    : {Δ Γ : Con} (σ : Δ ⇨ Γ) → σ ◦ one ≡ σ
    ◦-ass  : {Δ Γ Θ Ω : Con} (σ : Θ ⇨ Ω) (δ : Γ ⇨ Θ) (ν : Δ ⇨ Γ)
             → (σ ◦ δ) ◦ ν ≡ σ ◦ (δ ◦ ν)
    Conuniv : (Γ Δ : Con) ??

    -- Con has a terminal object
    ◇     : Con -- \di2
    !     : (Γ : Con) → Γ ⇨ ◇
    !uniq : {Γ : Con} → (δ : Γ ⇨ ◇) → δ ≡ ! Γ

    -----------------------------------------------------------------------
    -- set of types (a discrete category)
    -----------------------------------------------------------------------
    Ty    : Set
    Tyset : isSet Ty

    -----------------------------------------------------------------------
    -- a functor Tm_A : Con^op → Set for all (A : Ty)
    -----------------------------------------------------------------------
    Tm     : (A : Ty) (Γ : Con) → Set
    Tmset  : {A : Ty} {Γ : Con} → isSet (Tm A Γ)
    _[_]   : {A : Ty} {Δ Γ : Con} → Tm A Γ → Δ ⇨ Γ → Tm A Δ
    []id   : {A : Ty} {Γ : Con} (t : Tm A Γ) → t [ one ] ≡ t
    []cong : {A : Ty} {Δ Γ Θ : Con} (t : Tm A Θ) (σ : Γ ⇨ Θ) (δ : Δ ⇨ Γ) 
             → t [ σ ◦ δ ] ≡ (t [ σ ]) [ δ ]

    -----------------------------------------------------------------------
    -- comprehension
    -----------------------------------------------------------------------
    -- (see also STT_comprehension.agda)
    -- 
    -- we a functor _∙_ : Con × Ty → Con
    --   action on a morphism δ : Δ ⇨ Γ defined by:
    --     ∙δ := ⟨δ◦p,q⟩ : Δ∙A ⇨ Γ∙A
    --       where these are defined below:
    --         p   : Δ∙A ⇨ Δ
    --         δ◦p : Δ∙A ⇨ Γ
    --         q   : Tm Δ∙A A
    -- 
    -- a natural isomorphism ⟨_⟩ between
    -- 
    --   λ (Δ,Γ) . Δ ⇨ Γ × Tm A Δ   and   λ (Δ,Γ) . Δ ⇨ Γ∙A  :  Con^op × Con → Set
    --   <Hom_Con, Tm A ∘ proj₁>          Hom_Con ∘ id×_∙A
    --
    -- functors, so for all
    --
    --   (ν, δ) : (Θ, Γ) → (Δ, Ω)
    --
    -- the following diagram commutes:
    --
    --                           → ⟨_⟩
    --            Δ ⇨ Γ × Tm A Δ   ≃   Δ ⇨ Γ∙A
    --                  |        ← pq    |
    --       δ◦_◦ν×′_[ν]|                |⟨δ◦p,q⟩◦_◦ν = ∙δ◦_◦ν
    --                  |                |
    --                  ∨                ∨
    --            Θ ⇨ Ω × Tm A Θ   ≃   Θ ⇨ Ω∙A
    --
    -- p := proj₁ (pq one) : Γ ∙ A ⇨ Γ
    -- q := proj₂ (pq one) : Tm A (Γ ∙ A)
    --
    _∙_   : (Γ : Con) (A : Ty) → Con
    ⟨_⟩   : {Δ Γ : Con} {A : Ty} → Δ ⇨ Γ × Tm A Δ → Δ ⇨ Γ ∙ A
    pq    : {Δ Γ : Con} {A : Ty} → Δ ⇨ Γ ∙ A → Δ ⇨ Γ × Tm A Δ
    ∙β    : {Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) → pq ⟨ σ,u ⟩ ≡ σ,u
    ∙η    : {Δ Γ : Con} {A : Ty} (δ : Δ ⇨ Γ ∙ A) → ⟨ pq δ ⟩ ≡ δ
    ∙ξ    : {Δ Γ Θ Ω : Con} {A : Ty} (δ : Γ ⇨ Ω) (ν : Θ ⇨ Δ) (σ,u : Δ ⇨ Γ × Tm A Δ) → let σ = proj₁ σ,u; u = proj₂ σ,u in
            ⟨ δ ◦ σ ◦ ν , u [ ν ] ⟩ ≡ ⟨ δ ◦ proj₁ (pq one) , proj₂ (pq one) ⟩ ◦ ⟨ σ,u ⟩ ◦ ν

    -----------------------------------------------------------------------
    -- function space
    -----------------------------------------------------------------------
    -- we have a functor _⇒_ : Ty^op × Ty → Ty (an internalisation of Hom)
    --   the morphism part is uninteresting because Ty is a discrete category
    -- 
    -- lam is a natural isomorphism between
    -- 
    --   Tm B (_ ∙ A)   and   Tm (A ⇒ B) _ : Con^op → Set
    --   Tm B ∘ (_∙A)         Tm (A ⇒ B)
    -- 
    -- so for all
    -- 
    --   δ : Δ ⇨ Γ
    -- 
    -- the following diagram commutes:
    --
    --                    → lam
    --        Tm B (Γ ∙ A)  ≃  Tm (A ⇒ B) Γ
    --              |     ← app      |
    --    _[⟨δ◦p,q⟩]|                |_[δ]
    --              |                |
    --              ∨                ∨
    --        Tm B (Δ ∙ A)  ≃  Tm (A ⇒ B) Δ
    -- 
    _⇒_ : Ty → Ty → Ty
    lam : {Γ : Con} {A B : Ty} → Tm B (Γ ∙ A) → Tm (A ⇒ B) Γ
    app : {Γ : Con} {A B : Ty} → Tm (A ⇒ B) Γ → Tm B (Γ ∙ A)
    ⇒β  : {Γ : Con} {A B : Ty} (t : Tm B (Γ ∙ A)) → app (lam t) ≡ t
    ⇒η  : {Γ : Con} {A B : Ty} (f : Tm (A ⇒ B) Γ) → lam (app f) ≡ f
    ⇒ξ  : {Δ Γ : Con} {A B : Ty} (δ : Δ ⇨ Γ) (u : Tm B (Γ ∙ A)) 
          → lam u [ δ ] ≡ lam (u [ ⟨ δ ◦ proj₁ (pq one) , proj₂ (pq one) ⟩ ])

  infix 5 _⇒_
  infix 5 _⇨_
  infixl 9 _◦_

  -----------------------------------------------------------------------
  -- helpers for comprehension
  -----------------------------------------------------------------------
  p : {Γ : Con} {A : Ty} → Γ ∙ A ⇨ Γ
  p {Γ} {A} = proj₁ (pq (one {Γ ∙ A}))

  q : {Γ : Con} {A : Ty} → Tm A (Γ ∙ A)
  q {Γ} {A} = proj₂ (pq (one {Γ ∙ A}))

  pq1 : {Γ : Con} {A : Ty} → ⟨ p {Γ} {A} , q ⟩ ≡ one
  pq1 = ∙η one

  pqβ : {Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) → p ◦ ⟨ σ,u ⟩ , q [ ⟨ σ,u ⟩ ] ≡ σ,u
  pqβ (σ , u) =
      (∙β _) ⁻¹
    ◾ ap pq
         (begin
                                                                                        ⟨ p ◦ ⟨ σ , u ⟩ , q [ ⟨ σ , u ⟩ ] ⟩
          ≡⟨ (ap (λ z → ⟨ z ◦ ⟨ σ , u ⟩ , q [ ⟨ σ , u ⟩ ] ⟩) (lid p)) ⁻¹ ⟩
                                                                                        ⟨ one ◦ p ◦ ⟨ σ , u ⟩ , q [ ⟨ σ , u ⟩ ] ⟩
          ≡⟨                                                 ∙ξ one ⟨ σ , u ⟩ (p , q) ⟩
                                                                                        ⟨ one ◦ p , q ⟩ ◦ ⟨ p , q ⟩ ◦ ⟨ σ , u ⟩
          ≡⟨ ap (λ z → ⟨ z , q ⟩ ◦ ⟨ p , q ⟩ ◦ ⟨ σ , u ⟩)    (lid p) ⟩
                                                                                        ⟨ p , q ⟩ ◦ ⟨ p , q ⟩ ◦ ⟨ σ , u ⟩
          ≡⟨ ap (λ z → z ◦ ⟨ p , q ⟩ ◦ ⟨ σ , u ⟩)             pq1 ⟩
                                                                                        one ◦ ⟨ p , q ⟩ ◦ ⟨ σ , u ⟩
          ≡⟨ ap (λ z → z ◦ ⟨ σ , u ⟩)                        (lid _) ⟩
                                                                                        ⟨ p , q ⟩ ◦ ⟨ σ , u ⟩
          ≡⟨ ap (λ z → z ◦ ⟨ σ , u ⟩)                         pq1 ⟩
                                                                                        one ◦ ⟨ σ , u ⟩
          ≡⟨                                                  lid _ ⟩
                                                                                        ⟨ σ , u ⟩
          ∎)
    ◾ ∙β _

  pβ : {Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) → let σ = proj₁ σ,u in   p ◦ ⟨ σ,u ⟩   ≡ σ
  pβ = ap proj₁ ∘ pqβ

  qβ : {Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) → let u = proj₂ σ,u in   q [ ⟨ σ,u ⟩ ] ≡ u
  qβ = ap proj₂ ∘ pqβ

  pqη : {Δ Γ : Con} {A : Ty} (δ : Δ ⇨ Γ ∙ A) → ⟨ p ◦ δ , q [ δ ] ⟩ ≡ δ
  pqη δ =
    begin
                                                                ⟨ p ◦ δ , q [ δ ] ⟩ 
    ≡⟨ (ap (λ z → ⟨ z ◦ δ , q [ δ ] ⟩)      (lid p)) ⁻¹ ⟩
                                                                ⟨ one ◦ p ◦ δ , q [ δ ] ⟩
    ≡⟨                                      ∙ξ one δ (p , q) ⟩
                                                                ⟨ one ◦ p , q ⟩ ◦ ⟨ p , q ⟩ ◦ δ
    ≡⟨ ap (λ z → ⟨ z , q ⟩ ◦ ⟨ p , q ⟩ ◦ δ) (lid p) ⟩
                                                                ⟨ p , q ⟩ ◦ ⟨ p , q ⟩ ◦ δ
    ≡⟨ ap (λ z → z ◦ ⟨ p , q ⟩ ◦ δ)          pq1 ⟩
                                                                one ◦ ⟨ p , q ⟩ ◦ δ
    ≡⟨ ap (λ z → z ◦ δ)                     (lid _)  ⟩
                                                                ⟨ p , q ⟩ ◦ δ
    ≡⟨ ap (λ z → z ◦ δ)                      pq1 ⟩
                                                                one ◦ δ
    ≡⟨                                       lid δ ⟩
                                                                δ
    ∎

  ⟨⟩◦ : {Δ Γ Θ : Con} {A : Ty} (σ : Δ ⇨ Γ) (u : Tm A Δ) (δ : Θ ⇨ Δ)
        → ⟨ σ ◦ δ , u [ δ ] ⟩ ≡ ⟨ σ , u ⟩ ◦ δ
  ⟨⟩◦ σ u δ = 
    begin
      ⟨ σ ◦ δ , u [ δ ] ⟩
    ≡⟨ ap ⟨_⟩ (×pair= (ap (λ z → z ◦ δ) (pβ (σ , u))) (ap (λ z → z [ δ ]) (qβ (σ , u)))) ⁻¹ ⟩
      ⟨ (p ◦ ⟨ σ , u ⟩) ◦ δ , (q [ ⟨ σ , u ⟩ ]) [ δ ] ⟩
    ≡⟨ ap ⟨_⟩ (×pair= ((◦-ass p ⟨ σ , u ⟩ δ) ⁻¹) ([]cong q ⟨ σ , u ⟩ δ)) ⁻¹ ⟩
      ⟨ p ◦ (⟨ σ , u ⟩ ◦ δ) , q [ ⟨ σ , u ⟩ ◦ δ ] ⟩
    ≡⟨ pqη (⟨ σ , u ⟩ ◦ δ) ⟩
      ⟨ σ , u ⟩ ◦ δ
    ∎

  -----------------------------------------------------------------------
  -- helpers for function space
  -----------------------------------------------------------------------

  -- (app f) [ δ ] ≡ app (f [ p ◦ δ ]) [ ⟨ 1 , q [ δ ] ⟩ ] ???

  appξ : {Δ Γ : Con} {A B : Ty} (δ : Δ ⇨ Γ) (f : Tm (A ⇒ B) Γ)
         → app (f [ δ ]) ≡ app f [ ⟨ δ ◦ p , q ⟩ ]
  appξ δ f = begin
                                              app (f [ δ ])
    ≡⟨ ap (λ f → app (f [ δ ])) ((⇒η f) ⁻¹) ⟩
                                              app ((lam (app f)) [ δ ])
    ≡⟨ ap app (⇒ξ δ (app f)) ⟩
                                              app (lam (app f [ ⟨ δ ◦ p , q ⟩ ]))
    ≡⟨ ⇒β ((app f) [ ⟨ δ ◦ p , q ⟩ ]) ⟩
                                              app f [ ⟨ δ ◦ p , q ⟩ ]
    ∎

  -- the more usual application operator
  App : {Γ : Con} {A B : Ty} → Tm (A ⇒ B) Γ → Tm A Γ → Tm B Γ
  App f u = (app f) [ ⟨ one , u ⟩ ]

  -- it's relation with substitution
  Appsub : {Γ Δ : Con} {A B : Ty} (f : Tm (A ⇒ B) Γ) (u : Tm A Γ) (δ : Δ ⇨ Γ) → App f u [ δ ] ≡ App (f [ δ ]) (u [ δ ])
  Appsub f u δ =                                                       begin
    App f u [ δ ]
                                                                       ≡⟨                                                       refl ⟩
    app f [ ⟨ one , u ⟩ ] [ δ ]
                                                                       ≡⟨                                                       []cong (app f) ⟨ one , u ⟩ δ ⁻¹ ⟩
    app f [ ⟨ one , u ⟩ ◦ δ ]
                                                                       ≡⟨ ap (λ z → app f [ z ])                               (⟨⟩◦ one u δ) ⁻¹ ⟩
    app f [ ⟨ one ◦ δ , u [ δ ] ⟩ ]
                                                                       ≡⟨ ap (λ z → app f [ ⟨ z , u [ δ ] ⟩ ] )                (lid δ) ⟩
    app f [ ⟨ δ , u [ δ ] ⟩ ]
                                                                       ≡⟨ ap (λ z → app f [ ⟨ z , u [ δ ] ⟩ ] )                (rid δ ⁻¹) ⟩
    app f [ ⟨ δ ◦ one , u [ δ ] ⟩ ]
                                                                       ≡⟨ ap (λ { (z , z') → app f [ ⟨ δ ◦ z , z' ⟩ ] })       (pqβ (one , u [ δ ])) ⁻¹ ⟩
    app f [ ⟨ δ ◦ (p ◦ ⟨ one , u [ δ ] ⟩) , q [ ⟨ one , u [ δ ] ⟩ ] ⟩ ]
                                                                       ≡⟨ ap (λ z → app f [ ⟨ z , q [ ⟨ one , u [ δ ] ⟩ ] ⟩ ]) (◦-ass δ p ⟨ one , u [ δ ] ⟩) ⁻¹ ⟩
    app f [ ⟨ (δ ◦ p) ◦ ⟨ one , u [ δ ] ⟩ , q [ ⟨ one , u [ δ ] ⟩ ] ⟩ ]
                                                                       ≡⟨ ap (λ z → app f [ z ])                               (⟨⟩◦ (δ ◦ p) q ⟨ one , u [ δ ] ⟩) ⟩
    app f [ ⟨ δ ◦ p , q ⟩ ◦ ⟨ one , u [ δ ] ⟩ ]
                                                                       ≡⟨                                                       []cong (app f) ⟨ δ ◦ p , q ⟩ ⟨ one , u [ δ ] ⟩ ⟩
    app f [ ⟨ δ ◦ p , q ⟩ ] [ ⟨ one , u [ δ ] ⟩ ]
                                                                       ≡⟨ ap (λ z → z [ ⟨ one , u [ δ ] ⟩ ])                   (appξ δ f ⁻¹) ⟩
    app (f [ δ ]) [ ⟨ one , u [ δ ] ⟩ ]
                                                                       ≡⟨                                                        refl ⟩
    App (f [ δ ]) (u [ δ ])
                                                                       ∎

  Appβ : {Δ Γ : Con} {A B : Ty} (b : Tm B (Γ ∙ A)) (σ : Δ ⇨ Γ) (u : Tm A Δ)
         → App ((lam b) [ σ ]) u ≡ b [ ⟨ σ , u ⟩ ]
  Appβ b σ u =                                                  begin
    App (lam b [ σ ]) u
                                                                 ≡⟨                                                       refl ⟩
    app (lam b [ σ ]) [ ⟨ one , u ⟩ ]
                                                                 ≡⟨ ap (λ z → z [ ⟨ one , u ⟩ ])                         (appξ σ (lam b)) ⟩
    (app (lam b) [ ⟨ σ ◦ p , q ⟩ ]) [ ⟨ one , u ⟩ ]
                                                                 ≡⟨                                                       []cong (app (lam b)) ⟨ σ ◦ p , q ⟩ ⟨ one , u ⟩ ⁻¹ ⟩
    app (lam b) [ ⟨ σ ◦ p , q ⟩ ◦ ⟨ one , u ⟩ ]
                                                                 ≡⟨ ap (λ z → app (lam b) [ z ])                         (⟨⟩◦ (σ ◦ p) q ⟨ one , u ⟩ ⁻¹) ⟩
    app (lam b) [ ⟨ (σ ◦ p) ◦ ⟨ one , u ⟩ , q [ ⟨ one , u ⟩ ] ⟩ ]
                                                                 ≡⟨ ap (λ z → app (lam b) [ ⟨ z , q [ ⟨ one , u ⟩ ] ⟩ ]) (◦-ass σ p ⟨ one , u ⟩) ⟩
    app (lam b) [ ⟨ σ ◦ (p ◦ ⟨ one , u ⟩) , q [ ⟨ one , u ⟩ ] ⟩ ]
                                                                 ≡⟨ ap (λ { (z , z') → app (lam b) [ ⟨ σ ◦ z , z' ⟩ ] }) (∙β _ ⁻¹ ◾ ap pq (pqη ⟨ one , u ⟩) ◾ ∙β _) ⟩
    app (lam b) [ ⟨ σ ◦ one , u ⟩ ]
                                                                 ≡⟨ ap (λ z → app (lam b) [ ⟨ z , u ⟩ ] )                (rid σ) ⟩
    app (lam b) [ ⟨ σ , u ⟩ ]
                                                                 ≡⟨ ap (λ z → z [ ⟨ σ , u ⟩ ])                           (⇒β b) ⟩
    b [ ⟨ σ , u ⟩ ]
                                                                 ∎
