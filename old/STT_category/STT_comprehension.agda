
{-# OPTIONS --without-K #-}

open import STT_model

module STT_comprehension (stt : STT) where

-- this module shows that two different definitions of comprehension
-- are the same (C₁ and C₂). C₁ is used in STT_model.

open import stdlib

open STT stt

{-
⟨_⟩ is a natural isomorphism between

  F := λ (Δ,Γ) . Δ ⇨ Γ × Tm A Δ   :   Con^op × Con → Set

and

  G := λ (Δ,Γ) . Δ ⇨ Γ.A          :   Con^op × Con → Set

actions on morphisms:

  (ν, δ) : (Θ, Γ) ⇨ (Δ, Ω)

  F (ν, δ) : Δ ⇨ Γ × Tm A Δ → Θ ⇨ Ω × Tm A Θ
  F (ν, δ) = (δ◦_◦ν, _[ν])

  G (ν, δ) : Δ ⇨ Γ.A → Θ ⇨ Ω.A
  G (ν, δ) = ⟨δ◦pr₁(pq one), pr₂(pq one)⟩◦_◦ν

naturality means:
                                → ⟨_⟩
                 Δ ⇨ Γ × Tm A Δ   ≃   Δ ⇨ Γ.A
                       |        ← pq    |
F (ν, δ) = δ◦_◦ν×′_[ν] |                | ⟨δ◦pr₁(pq 1),pr₂(pq 1)⟩◦_◦ν = G (ν, δ)
                       |                |
                       ∨                ∨
                 Θ ⇨ Ω × Tm A Θ   ≃   Θ ⇨ Ω.A

-}
C₁ : Set
C₁ = 
  Σ ((Γ : Con) (A : Ty) → Con)                                                       λ _∙_   →
  Σ ({Δ Γ : Con} {A : Ty} → Δ ⇨ Γ × Tm A Δ → Δ ⇨ Γ ∙ A)                              λ ⟨_⟩   →
  Σ ({Δ Γ : Con} {A : Ty} → Δ ⇨ Γ ∙ A → Δ ⇨ Γ × Tm A Δ)                              λ pq    →
  Σ ({Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) → pq ⟨ σ,u ⟩ ≡ σ,u)                 λ ∙β →
  Σ ({Δ Γ : Con} {A : Ty} (δ : Δ ⇨ Γ ∙ A)        → ⟨ pq δ ⟩ ≡ δ)                     λ ∙η →
  Σ ({Δ Γ Θ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) (δ : Θ ⇨ Δ)
     → ⟨ ((λ z → z ◦ δ) ×′ λ z → z [ δ ]) σ,u ⟩ ≡ ⟨ σ,u ⟩ ◦ δ)                       λ ∙ξ₁ →        
                        --  ⟨ σ ◦ δ , u [ δ ] ⟩ ≡ ⟨ σ,u ⟩ ◦ δ
    ({Δ Γ Θ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) (δ : Γ ⇨ Θ)
     → ⟨ ((_◦_ δ) ×′ id) σ,u ⟩ ≡ ⟨ δ ◦ proj₁ (pq one) , proj₂ (pq one) ⟩ ◦ ⟨ σ,u ⟩) -- ∙ξ₂
              -- ⟨ σ ◦ δ , u ⟩ ≡ ⟨ δ ◦ proj₁ (pq one) , proj₂ (pq one) ⟩ ◦ ⟨ σ,u ⟩

-- abbreviations for the projections (boilerplate)
∙₁  : (c : C₁) (Γ : Con) (A : Ty) → Con
∙₁  c = proj₁ c
⟨⟩₁ : (c : C₁) {Δ Γ : Con} {A : Ty} → Δ ⇨ Γ × Tm A Δ → Δ ⇨ ∙₁ c Γ A
⟨⟩₁ c = proj₁ (proj₂ c)
-- we need to prime these names because they clash with the ones imported from STT_model
pq'  : (c : C₁) {Δ Γ : Con} {A : Ty} → Δ ⇨ ∙₁ c Γ A → Δ ⇨ Γ × Tm A Δ
pq'  c = proj₁ (proj₂ (proj₂ c))
∙β'  : (c : C₁) {Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) → pq' c (⟨⟩₁ c σ,u) ≡ σ,u
∙β'  c = proj₁ (proj₂ (proj₂ (proj₂ c)))
∙η'  : (c : C₁) {Δ Γ : Con} {A : Ty} (δ : Δ ⇨ ∙₁ c Γ A) → ⟨⟩₁ c (pq' c δ) ≡ δ
∙η'  c = proj₁ (proj₂ (proj₂ (proj₂ (proj₂ c))))
∙ξ₁ : (c : C₁) {Δ Γ Θ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) (δ : Θ ⇨ Δ)
      → ⟨⟩₁ c (((λ z → z ◦ δ) ×′ λ z → z [ δ ]) σ,u) ≡ ⟨⟩₁ c σ,u ◦ δ
∙ξ₁ c = proj₁ (proj₂ (proj₂ (proj₂ (proj₂ (proj₂ c)))))
∙ξ₂ : (c : C₁) {Δ Γ Θ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) (δ : Γ ⇨ Θ)
      → ⟨⟩₁ c (((_◦_ δ) ×′ id) σ,u) ≡ ⟨⟩₁ c (δ ◦ proj₁ (pq' c one) , proj₂ (pq' c one)) ◦ ⟨⟩₁ c σ,u
∙ξ₂ c = proj₂ (proj₂ (proj₂ (proj₂ (proj₂ (proj₂ c)))))

-- ∙ξ₁ and ∙ξ₂ are equivalent to ∙ξ:
∙ξ'    : (c : C₁) {Δ Γ Θ Ω : Con} {A : Ty} (δ : Γ ⇨ Ω) (ν : Θ ⇨ Δ) (σ,u : Δ ⇨ Γ × Tm A Δ)
        → let σ = proj₁ σ,u; u = proj₂ σ,u in
          ⟨⟩₁ c (δ ◦ σ ◦ ν , u [ ν ])
        ≡ ⟨⟩₁ c (δ ◦ proj₁ (pq' c one) , proj₂ (pq' c one)) ◦ ⟨⟩₁ c σ,u ◦ ν
∙ξ' c δ ν (σ , u) = ∙ξ₁ c (δ ◦ σ , u) ν ◾ ap (λ z → z ◦ ν) (∙ξ₂ c (σ , u) δ)

C₂ : Set
C₂ = 
  Σ ((Γ : Con) (A : Ty) → Con)                                                λ _∙_ →
  Σ ({Δ Γ : Con} {A : Ty} → Δ ⇨ Γ × Tm A Δ → Δ ⇨ Γ ∙ A)                       λ ⟨_⟩ →
  Σ ({Γ : Con} {A : Ty} → Γ ∙ A ⇨ Γ)                                          λ p   →
  Σ ({Γ : Con} {A : Ty} → Tm A (Γ ∙ A))                                       λ q   →
  Σ ({Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) → p ◦ ⟨ σ,u ⟩   ≡ proj₁ σ,u) λ pβ  →
  Σ ({Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) → q [ ⟨ σ,u ⟩ ] ≡ proj₂ σ,u) λ qβ  →
    ({Δ Γ : Con} {A : Ty} (δ : Δ ⇨ Γ ∙ A) → ⟨ p ◦ δ , q [ δ ] ⟩ ≡ δ)         -- pqη

-- pqη is equivalent to the following rules:
--   ⟨⟩◦ : {Δ Γ Θ : Con} {A : Ty} (σ : Δ ⇨ Γ) (u : Tm A Δ) (δ : Θ ⇨ Δ)
--         → ⟨ σ , u ⟩ ◦ δ ≡ ⟨ σ ◦ δ , u [ δ ] ⟩
--   pq1 : {Γ : Con} {A : Ty} → ⟨ p {Γ} {A} , q ⟩ ≡ one

-- abbreviations for the projections (boilerplate)
∙₂  : (c : C₂) (Γ : Con) (A : Ty) → Con
∙₂  c = proj₁ c
⟨⟩₂ : (c : C₂) {Δ Γ : Con} {A : Ty} → Δ ⇨ Γ × Tm A Δ → Δ ⇨ ∙₂ c Γ A
⟨⟩₂ c = proj₁ (proj₂ c)
p'  : (c : C₂) {Γ : Con} {A : Ty} → ∙₂ c Γ A ⇨ Γ
p'  c = proj₁ (proj₂ (proj₂ c))
q'  : (c : C₂) {Γ : Con} {A : Ty} → Tm A (∙₂ c Γ A)
q'  c = proj₁ (proj₂ (proj₂ (proj₂ c)))
pβ  : (c : C₂) {Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) → p' c ◦ ⟨⟩₂ c σ,u  ≡ proj₁ σ,u
pβ  c = proj₁ (proj₂ (proj₂ (proj₂ (proj₂ c))))
qβ  : (c : C₂) {Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) → q' c [ ⟨⟩₂ c σ,u ] ≡ proj₂ σ,u
qβ  c = proj₁ (proj₂ (proj₂ (proj₂ (proj₂ (proj₂ c)))))
pqη : (c : C₂) {Δ Γ : Con} {A : Ty} (δ : Δ ⇨ ∙₂ c Γ A) → ⟨⟩₂ c (p' c ◦ δ , q' c [ δ ]) ≡ δ
pqη c = proj₂ (proj₂ (proj₂ (proj₂ (proj₂ (proj₂ c)))))


C₁≃C₂ : Funext → C₁ ≃ C₂
C₁≃C₂ funext = mk≃
  f g
  (λ c → pair= refl $
         pair= refl $
         pair= (pq=gfpq c) $
         ×pair= (ext $ λ Δ → ext $ λ Γ → ext $ λ A → funext $ λ σ,u →
                   ×set (⇨set {Δ} {Γ}) (Tmset {A} {Δ}) _ (∙β' c σ,u)) $
         ×pair= (ext $ λ Δ → ext $ λ Γ → ext $ λ A → funext $ λ δ →
                   ⇨set _ (∙η' c δ)) $
         ×pair= (ext $ λ Δ → ext $ λ Γ → ext $ λ Θ → ext $ λ A → funext $ λ σ,u → funext $ λ δ →
                   ⇨set _ (∙ξ₁ c σ,u δ)) $
                 ext $ λ Δ → ext $ λ Γ → ext $ λ Θ → ext $ λ A → funext $ λ σ,u → funext $ λ δ →
                   ⇨set _ (∙ξ₂ c σ,u δ))
  (λ c → pair= refl $
         pair= refl $
         pair= (p=fgp c) $
         pair= (transportΣ (λ {Γ : Con} {A : Ty} → q' c [ one ]) (p=fgp c) ◾ q=fgq c) $
         ×pair= (ext $ λ Δ → ext $ λ Γ → ext $ λ A → funext $ λ σ,u →
                   ⇨set _ (pβ c σ,u)) $
         ×pair= (ext $ λ Δ → ext $ λ Γ → ext $ λ A → funext $ λ σ,u →
                   Tmset _ (qβ c σ,u)) $
                 ext $ λ Δ → ext $ λ Γ → ext $ λ A → funext $ λ δ →
                   ⇨set _ (pqη c δ))
  where
    abstract
      ext : {X : Set} {Y : X → Set} {f g : {x : X} → Y x}
            → ((x : X) → f {x} ≡ g {x}) → (λ {x} → f {x}) ≡ g
      ext = funext-impl funext

      pq=gfpq : (c : C₁) → (λ {Δ} {Γ} {A} (δ : Δ ⇨ ∙₁ c Γ A) → proj₁ (pq' c {∙₁ c Γ A} {Γ} {A} one) ◦ δ , proj₂ (pq' c {∙₁ c Γ A} {Γ} {A} one) [ δ ]) ≡ pq' c
      pq=gfpq c = ext $ λ Δ → ext $ λ Γ → ext $ λ A → funext $ λ δ →
          (∙β' c _) ⁻¹
        ◾ ap (pq' c)
             (begin
                ⟨⟩₁ c (proj₁ (pq' c one) ◦ δ , proj₂ (pq' c one) [ δ ])
              ≡⟨ ∙ξ₁ c (pq' c one) δ ⟩
                ⟨⟩₁ c (pq' c one) ◦ δ
              ≡⟨ ap (λ z → z ◦ δ) (∙η' c one) ⟩
                one ◦ δ
              ≡⟨ lid δ ⟩
                δ
              ≡⟨ (∙η' c δ) ⁻¹ ⟩
                ⟨⟩₁ c (pq' c δ)
              ∎)
        ◾ ∙β' c _

      p=fgp : (c : C₂) → (λ {Γ} {A} → p' c {Γ} {A} ◦ one) ≡ p' c
      p=fgp c = ext $ λ Γ → ext $ λ A → rid $ p' c

      transportΣ : {A B : Set} {C : B → A → Set} {x y : A} (b : B) {c : C b x} (q : x ≡ y)
                   → proj₁ (transport (λ p → Σ B λ b → C b p) q (b , c)) ≡ b
      transportΣ b refl = refl

      q=fgq : (c : C₂) → (λ {Γ} {A} → q' c {Γ} {A} [ one ]) ≡ q' c
      q=fgq c = ext $ λ Γ → ext $ λ A → []id $ q' c

    f : C₁ → C₂
    f (_∙_ , ⟨_⟩ , pq ,  ∙β , ∙η , ∙ξ₁ , ∙ξ₂) =
      ( _∙_
      , ⟨_⟩
      , proj₁ (pq one)
      , proj₂ (pq one)
      , pβ'
      , qβ'
      , pqη'
      )
      where
        abstract
          pqβ : {Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ)
                → proj₁ (pq one) ◦ ⟨ σ,u ⟩ , proj₂ (pq one) [ ⟨ σ,u ⟩ ] ≡ σ,u
          pqβ σ,u = (∙β _) ⁻¹ 
                  ◾ ap pq
                       (begin
                          ⟨ proj₁ (pq one) ◦ ⟨ σ,u ⟩ , proj₂ (pq one) [ ⟨ σ,u ⟩ ] ⟩
                        ≡⟨ ∙ξ₁ (proj₁ (pq one) , proj₂ (pq one)) ⟨ σ,u ⟩ ⟩
                          ⟨ proj₁ (pq one) , proj₂ (pq one) ⟩ ◦ ⟨ σ,u ⟩
                        ≡⟨ ap (λ z → ⟨ z , proj₂ (pq one) ⟩ ◦ ⟨ σ,u ⟩) (lid (proj₁ (pq one)) ⁻¹) ⟩
                          ⟨ one ◦ proj₁ (pq one) , proj₂ (pq one) ⟩ ◦ ⟨ σ,u ⟩
                        ≡⟨ (∙ξ₂ σ,u one) ⁻¹ ⟩
                          ⟨ one ◦ proj₁ σ,u , proj₂ σ,u ⟩
                        ≡⟨ ap (λ z → ⟨ z , proj₂ σ,u ⟩) (lid (proj₁ σ,u)) ⟩
                          ⟨ proj₁ σ,u , proj₂ σ,u ⟩
                        ∎)
                  ◾ ∙β _
          pβ'  : {Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) → proj₁ (pq one) ◦ ⟨ σ,u ⟩   ≡ proj₁ σ,u
          pβ' = ap proj₁ ∘ pqβ
          qβ'  : {Δ Γ : Con} {A : Ty} (σ,u : Δ ⇨ Γ × Tm A Δ) → proj₂ (pq one) [ ⟨ σ,u ⟩ ] ≡ proj₂ σ,u
          qβ' = ap proj₂ ∘ pqβ
          pqη' : {Δ Γ : Con} {A : Ty} (δ : Δ ⇨ Γ ∙ A)
                → ⟨ proj₁ (pq one) ◦ δ , proj₂ (pq one) [ δ ] ⟩ ≡ δ
          pqη' δ =
            begin
              ⟨ proj₁ (pq one) ◦ δ , proj₂ (pq one) [ δ ] ⟩
            ≡⟨ ∙ξ₁ (proj₁ (pq one) , proj₂ (pq one)) δ  ⟩
              ⟨ proj₁ (pq one) , proj₂ (pq one) ⟩ ◦ δ
            ≡⟨ ap (λ z → z ◦ δ) (∙η one) ⟩
              one ◦ δ
            ≡⟨ lid δ ⟩
              δ
            ∎

    g : C₂ → C₁
    g (_∙_ , ⟨_⟩ , p , q , pβ , qβ , pqη) =
      ( _∙_
      , ⟨_⟩
      , (λ δ → p ◦ δ , q [ δ ]) 
      , ∙β''
      , pqη
      , ∙ξ₁'
      , ∙ξ₂'
      )
      where
        abstract
          ∙β'' : {Δ Γ : Con} {A : Ty} (σ,u : Σ (Δ ⇨ Γ) (λ _ → Tm A Δ)) → (p ◦ ⟨ σ,u ⟩ , q [ ⟨ σ,u ⟩ ]) ≡ σ,u
          ∙β'' σ,u = pair= (pβ σ,u) $ transportconst' (pβ σ,u) (qβ σ,u)
          ⟨⟩◦ : {Δ Γ Θ : Con} {A : Ty} (σ : Δ ⇨ Γ) (u : Tm A Δ) (δ : Θ ⇨ Δ)
                → ⟨ σ ◦ δ , u [ δ ] ⟩ ≡ ⟨ σ , u ⟩ ◦ δ
          ⟨⟩◦ σ u δ = 
            begin
              ⟨ σ ◦ δ , u [ δ ] ⟩
            ≡⟨ (ap ⟨_⟩ (pair= (ap (λ z → z ◦ δ) (pβ (σ , u))) (transportconst' (ap (λ z → z ◦ δ) (pβ (σ , u))) (ap (λ z → z [ δ ]) (qβ (σ , u)))))) ⁻¹ ⟩
              ⟨ (p ◦ ⟨ σ , u ⟩) ◦ δ , (q [ ⟨ σ , u ⟩ ]) [ δ ] ⟩
            ≡⟨ (ap ⟨_⟩ (pair= ((◦-ass p ⟨ σ , u ⟩ δ) ⁻¹) (transportconst' (◦-ass p ⟨ σ , u ⟩ δ ⁻¹) ([]cong q ⟨ σ , u ⟩ δ)))) ⁻¹ ⟩
              ⟨ p ◦ (⟨ σ , u ⟩ ◦ δ) , q [ ⟨ σ , u ⟩ ◦ δ ] ⟩
            ≡⟨ pqη (⟨ σ , u ⟩ ◦ δ) ⟩
              ⟨ σ , u ⟩ ◦ δ
            ∎
          ∙ξ₁' : {Δ Γ Θ : Con} {A : Ty} (σ,u : Σ (Δ ⇨ Γ) (λ _ → Tm A Δ)) (δ : Θ ⇨ Δ)
                → ⟨ proj₁ σ,u ◦ δ , proj₂ σ,u [ δ ] ⟩ ≡ ⟨ σ,u ⟩ ◦ δ
          ∙ξ₁' σ,u δ = ⟨⟩◦ (proj₁ σ,u) (proj₂ σ,u) δ
          ∙ξ₂' : {Δ Γ Θ : Con} {A : Ty} (σ,u : Σ (Δ ⇨ Γ) (λ _ → Tm A Δ)) (δ : Γ ⇨ Θ)
                → ⟨ δ ◦ proj₁ σ,u , proj₂ σ,u ⟩ ≡ ⟨ δ ◦ (p ◦ one) , q [ one ] ⟩ ◦ ⟨ σ,u ⟩
          ∙ξ₂' σ,u δ =
            begin
              ⟨ δ ◦ proj₁ σ,u , proj₂ σ,u ⟩
            ≡⟨ (ap ⟨_⟩ (pair= (ap (_◦_ δ) (pβ σ,u)) (transportconst' (ap (_◦_ δ) (pβ σ,u)) (qβ σ,u)))) ⁻¹ ⟩
              ⟨ δ ◦ (p ◦ ⟨ σ,u ⟩) , q [ ⟨ σ,u ⟩ ] ⟩
            ≡⟨ (ap (λ z → ⟨ z , q [ ⟨ σ,u ⟩ ] ⟩) (◦-ass δ p ⟨ σ,u ⟩)) ⁻¹ ⟩
              ⟨ δ ◦ p ◦ ⟨ σ,u ⟩ , q [ ⟨ σ,u ⟩ ] ⟩
            ≡⟨ (ap ⟨_⟩ (pair= (ap (λ z → δ ◦ z ◦ ⟨ σ,u ⟩) (rid p)) (transportconst' (ap (λ z → δ ◦ z ◦ ⟨ σ,u ⟩) (rid p)) (ap (λ z → z [ ⟨ σ,u ⟩ ]) ([]id q))))) ⁻¹ ⟩
              ⟨ (δ ◦ (p ◦ one)) ◦ ⟨ σ,u ⟩ , (q [ one ]) [ ⟨ σ,u ⟩ ] ⟩
            ≡⟨ ⟨⟩◦ (δ ◦ (p ◦ one)) (q [ one ]) ⟨ σ,u ⟩ ⟩
              ⟨ δ ◦ (p ◦ one) , q [ one ] ⟩ ◦ ⟨ σ,u ⟩
            ∎
