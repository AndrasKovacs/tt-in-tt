module TT where

open import Relation.Binary.PropositionalEquality

data Con : Set
data Ty : Con → Set
data Tm : ∀ Γ → Ty Γ → Set
data Tms : Con → Con → Set

data Con where
  • : Con -- \bub
  _,_ : (Γ : Con) → Ty Γ → Con

data Ty where
  _[_]T : ∀ {Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
  U  : ∀{Γ} → Ty Γ
  El : ∀{Γ} → Tm Γ U → Ty Γ

_⊢_≡_ : ∀ {Γ Δ}{A : Ty Δ}{δ δ' : Tms Γ Δ} → δ ≡ δ' → Tm Γ (A [ δ ]T) → Tm Γ (A [ δ' ]T) → Set
refl ⊢ a ≡ a' = a ≡ a'

data Tms where
  ε   : ∀{Γ} → Tms Γ • 
  _,_ : ∀{Γ Δ}(δ : Tms Γ Δ) → {A : Ty Δ} → Tm Γ (A [ δ ]T) → Tms Γ (Δ , A)
  id  : ∀{Γ} → Tms Γ Γ
  _∘_ : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  π₁  : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ

data Tm where
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (δ : Tms Γ Δ) → Tm Γ (A [ δ ]T) 
  π₂    : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ π₁ ]T)
  u     : ∀{Γ} → Tm Γ (U {Γ})
  Π     : ∀{Γ}(A : Tm Γ (U {Γ}))(B : Tm (Γ , El A) U) → Tm Γ U
  app   : ∀{Γ}{A : Tm Γ U}{B : Tm (Γ , El A) U} → Tm Γ (El (Π A B)) → Tm (Γ , El A) (El B)
  lam   : ∀{Γ}{A : Tm Γ U}{B : Tm (Γ , El A) U} → Tm (Γ , El A) (El B) → Tm Γ (El (Π A B))

postulate
   idl : ∀{Γ Δ}{δ : Tms Γ Δ} → id ∘ δ ≡ δ 
   idr : ∀{Γ Δ}{δ : Tms Γ Δ} → δ ∘ id ≡ δ 
   ass : ∀{Δ Γ Θ Ω}{σ : Tms Θ Ω}{δ : Tms Γ Θ}{ν : Tms Δ Γ} → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
   π₁β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₁ ∘ (δ , a) ≡ δ
   πid : ∀{Γ}{A : Ty Γ} → (π₁ , π₂) ≡ (id {Γ , A})
   εη  : ∀{Γ}(σ : Tms Γ •) → σ ≡ ε

   Elu   : ∀{Γ} → El (u {Γ}) ≡ U
   
   [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
   [][]T : ∀{Γ Δ Θ}{A : Ty Θ}{σ : Tms Γ Δ}{δ : Tms Δ Θ} → (A [ δ ]T) [ σ ]T ≡ A [ δ ∘ σ ]T

   U[]   : ∀{Γ Δ}{δ : Tms Γ Δ} → U [ δ ]T ≡ U
   El[]  : ∀{Γ Δ}{δ : Tms Γ Δ}{t : Tm Δ U} → (El t) [ δ ]T ≡ El (subst (Tm Γ) U[] (t [ δ ]t))

_^_ : ∀ {Γ Δ}(δ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ δ ]T) (Δ , A)
δ ^ A = (δ ∘ π₁) , subst (Tm _) [][]T π₂ 


postulate
   [id]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → subst (Tm Γ) [id]T (t [ id ]t) ≡ t
   [][]t : ∀{Γ Δ Θ}{A : Ty Θ}{t : Tm Θ A}{σ : Tms Γ Δ}{δ : Tms Δ Θ}
         → subst (Tm Γ) [][]T ((t [ δ ]t) [ σ ]t) ≡  t [ δ ∘ σ ]t
{-
   [][]t' : ∀{Γ Δ Θ}{A : Ty Θ}{t : Tm {Θ} A}{σ : Tms Γ Δ}{δ : Tms Δ Θ}
         → subst Tm [][]T ((t [ δ ]t) [ σ ]t) ≡  t [ δ ∘ σ ]t
-}
   π₂β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₁β ⊢ subst (Tm Γ) [][]T (π₂ [ δ , a ]t) ≡ a
   u[]   : ∀{Γ Δ}{δ : Tms Γ Δ} → subst (Tm Γ) U[] (u [ δ ]t) ≡ u {Γ}
   Π[]   : ∀ {Γ Δ}{δ : Tms Γ Δ}{A : Tm Δ U}{B : Tm (Δ , El A) U}
         → subst (Tm Γ) U[] ((Π A B)[ δ ]t) ≡ Π (subst (Tm Γ) U[] (A [ δ ]t)) 
                     {!subst (Tm (Γ , subst (Tm Γ) U[] (A [ δ ]t))) U[] (B [ ? ]t)!}
--                 (subst (λ X → Tm (Γ , X)) ) U[] (B [ (δ ∘ π₁) , {!subst Tm ([][]T π₂!} ]t))

   
  -- Πβ : ∀ {Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm {Γ , A} B}
  --    →  app (lam t) ≡ t

-- ⟦_⟧ : Con → Set
-- ⟦_⟧R : (Γ : Con) → ⟦ Γ ⟧ → Set
-- ⟦_⟧ : (Γ : Con) → Ty Γ → ⟦ Γ ⟧ → Set
-- ⟦_⟧R :  (Γ : Con) → (A : Ty Γ) → (γ : ⟦ Γ ⟧) → ⟦ Γ ⟧R γ → ⟦ A ⟧ γ → Set
-- ⟦_⟧ : (t : Tm {Γ} A) → (γ : ⟦ Γ ⟧) → ⟦ A ⟧ γ
-- ⟦_⟧R :  (t : Tm {Γ} A) → (γ : ⟦ Γ ⟧) → ⟦ Γ ⟧R γ → ⟦ A ⟧R (⟦ t ⟧ γ)
