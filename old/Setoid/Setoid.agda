{-# OPTIONS --type-in-type #-} 


module Setoid.Setoid where

open import lib hiding (coe) -- (coe ; ap ; apd ; _≡[_]≡_)

coe : {A B : Set } → A ≡ B → A → B
coe refl a = a

{-
_≡[_]≡_ : {A B : Set} → A → A ≡ B → B → Set
x ≡[ α ]≡ y = coe α x ≡ y

ap : {A B : Set}(f : A → B){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
    → f a₀ ≡ f a₁
ap f refl = refl

apd : {A : Set}{B : A → Set}(f : (x : A) → B x){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
    → f a₀ ≡[ ap B a₂ ]≡ f a₁
apd f refl = refl
-}

record Setoid : Set where  -- was : Set₁
  constructor cₛ
  field
    ∣_∣ : Set --i
    _∶_∼_ : ∣_∣ → ∣_∣ → Set 
    _∼prp : {a b : ∣_∣} → (p q : _∶_∼_ a b) → p ≡ q
    reflₛ : { a : ∣_∣ } → _∶_∼_ a a
    symₛ : { a b : ∣_∣ } → _∶_∼_ a b → _∶_∼_ b a
    transₛ : { a b c : ∣_∣ } → _∶_∼_ a b → _∶_∼_ b c → _∶_∼_ a c 

open Setoid

ab∘ba : {Γ : Setoid}{a b : ∣ Γ ∣} → (p : Γ ∶ a ∼ b) → (q : Γ ∶ b ∼ a) → transₛ Γ p q ≡ reflₛ Γ
ab∘ba {Γ}{a}{b} p q = (Γ ∼prp) (transₛ Γ p q) (reflₛ Γ) 

{-record FamSetoid (Γ : Setoid) : Set₁ where
  constructor cf
  field
    ∣_∣_ : ∣ Γ ∣ → Set
    _∶F_∼_ : {δ₁ δ₂ : ∣ Γ ∣} → ∣_∣_ δ₁ → ∣_∣_ δ₂ → Set
  -------
    reflF : {γ : ∣ Γ ∣}{a : ∣_∣_ γ } → _∶F_∼_ a a
    symF : {γ : ∣ Γ ∣}{a b : ∣_∣_ γ} → _∶F_∼_ a b → _∶F_∼_ b a
    transF : {γ : ∣ Γ ∣}{a b c : ∣_∣_ γ} → _∶F_∼_ a b → _∶F_∼_ b c → _∶F_∼_ a c
    -- NEED TO CHECK
 -}

record _⇒ₛ_ (Γ : Setoid)(Δ : Setoid) : Set where  -- was : Set₁
  constructor cₘ
  field
    f : ∣ Γ ∣ → ∣ Δ ∣
    resp : {γ γ' : ∣ Γ ∣} → Γ ∶ γ ∼ γ' → Δ ∶  f γ ∼ f γ'
    -- A ∶ a ~ a'     is the same as   _∶_~_ A a a'
    -- e.g. sym A is the sym-component of the setoid A
    -- trans A a b c

    
open _⇒ₛ_ 

_∘ₛ_ : {Γ Δ Θ : Setoid} → Δ ⇒ₛ Θ → Γ ⇒ₛ Δ → Γ ⇒ₛ Θ
_∘ₛ_ {Γ}{Δ}{Θ} ΔΘ ΓΔ = cₘ (λ γ → f ΔΘ (f ΓΔ γ)) (λ γγ' → resp ΔΘ (resp ΓΔ γγ'))

idₛ : {Γ : Setoid} → Γ ⇒ₛ Γ
idₛ {Γ} = cₘ (λ x → x) (λ x → x)


-- ⇒ₛ= : {Γ : Setoid}{Δ : Setoid}(σ τ : Γ ⇒ₛ Δ) → (p : f σ ≡ f τ) → ((resp σ) ≡[ ap {!λ (k : ∣ Γ ∣ → ∣ Δ ∣) → {γ γ' : ∣ Γ ∣} → Γ ∶ γ ∼ γ' → Δ ∶  k γ ∼ k γ'!} p ]≡ (resp τ)) → σ ≡ τ
-- ⇒ₛ= = {!!}



record FamSetoid (Γ : Setoid) : Set where  -- was : Set₁
  constructor cf
  field
    F₀ : ∣ Γ ∣ → Setoid
    F₁ : {x₁ x₂ : ∣ Γ ∣} → Γ ∶ x₁ ∼ x₂ → F₀ x₁ ⇒ₛ F₀ x₂
    F-id : {x : ∣ Γ ∣} → F₁ {x} (reflₛ Γ) ≡ idₛ
    F-hom : {x₁ x₂ x₃ : ∣ Γ ∣} (p : Γ ∶ x₁ ∼ x₂) (q : Γ ∶ x₂ ∼ x₃) → F₁ q ∘ₛ F₁ p ≡ F₁ (transₛ Γ p q)


open FamSetoid

record SectSetoid (Γ : Setoid) (A : FamSetoid Γ) :  Set where  -- was : Set₁
  constructor cs
  field
    s : (γ : ∣ Γ ∣) → ∣ F₀ A γ ∣
    p : (γ₁ γ₂ : ∣ Γ ∣) → (r : Γ ∶ γ₁ ∼ γ₂) → f (F₁ A r) (s γ₁) ≡ s γ₂





∶∼≡₁ : {Γ : Setoid}{γ₁ γ₂ γ₃ : ∣ Γ ∣} → γ₁ ≡ γ₂ → Γ ∶ γ₁ ∼ γ₃ ≡ Γ ∶ γ₂ ∼ γ₃
∶∼≡₁ refl = refl

∶∼≡₂ : {Γ : Setoid}{γ₁ γ₂ γ₃ : ∣ Γ ∣} → γ₂ ≡ γ₃ → Γ ∶ γ₁ ∼ γ₂ ≡ Γ ∶ γ₁ ∼ γ₃
∶∼≡₂ refl = refl

f≡₁ : {Γ Δ : Setoid}{δ σ : Γ ⇒ₛ Δ}{a : ∣ Γ ∣} → δ ≡ σ → f δ a ≡ f σ a
f≡₁ refl = refl

f≡₂ : {Γ Δ : Setoid}{δ : Γ ⇒ₛ Δ}{a₁ a₂ : ∣ Γ ∣} → a₁ ≡ a₂ → f δ a₁ ≡ f δ a₂
f≡₂ refl = refl

F₁≡ : {Γ : Setoid}{A : FamSetoid Γ}{γ₁ γ₂ : ∣ Γ ∣}{r₁ r₂ : Γ ∶ γ₁ ∼ γ₂} → r₁ ≡ r₂ → F₁ A r₁ ≡ F₁ A r₂
F₁≡ refl = refl

F₀≡ : {Γ : Setoid}{A : FamSetoid Γ}{γ₁ γ₂ : ∣ Γ ∣} → γ₁ ≡ γ₂ → F₀ A γ₁ ≡ F₀ A γ₂
F₀≡ refl = refl

≡∼ : {Γ : Setoid}{γ₁ γ₂ : ∣ Γ ∣} → γ₁ ≡ γ₂ → Γ ∶ γ₁ ∼ γ₂
≡∼ {Γ} refl = reflₛ Γ

⇒≡₁ : {Γ Δ Θ : Setoid} → Γ ≡ Δ → Γ ⇒ₛ Θ ≡ Δ ⇒ₛ Θ
⇒≡₁ refl = refl

⇒≡₂ : {Γ Δ Θ : Setoid} → Δ ≡ Θ → Γ ⇒ₛ Δ ≡ Γ ⇒ₛ Θ
⇒≡₂ refl = refl 



_,c_ : (Γ : Setoid) → (A : FamSetoid Γ) → Setoid
Γ ,c A = cₛ (Σ ∣ Γ ∣ (λ γ → ∣ F₀ A γ ∣))
                  (λ {(γ₁ , a₁) (γ₂ , a₂) → Σ (Γ ∶ γ₁ ∼ γ₂) (λ p → (F₀ A γ₂) ∶ f (F₁ A p) a₁ ∼ a₂)})
                  (λ { {(γ₁ , a₁)} {(γ₂ , a₂)} (p₁ , p₂) (q₁ , q₂) →  Σ= ((Γ ∼prp) p₁ q₁) 
                                                                                               (((F₀ A γ₂) ∼prp) _ q₂) })
                  (λ { {γ , a} → reflₛ Γ , coe (∶∼≡₁ {F₀ A γ} (f≡₁ {δ = idₛ}{σ = F₁ A (reflₛ Γ)} (F-id A ⁻¹))) (reflₛ (F₀ A γ) {a}) })
                                                  --coe (ap (λ h → F₀ A γ ∶ f h a ∼ a) ( F-id A ⁻¹)) (reflₛ (F₀ A γ))} )
                  (λ { {γ₁ , a₁}{γ₂ , a₂} (p₁ , p₂) → symₛ Γ p₁ , symₛ (F₀ A γ₁)
                                                                                                (coe (∶∼≡₁ {F₀ A γ₁}{f (F₁ A (symₛ Γ p₁)) (f (F₁ A p₁) a₁)}{a₁}{ f (F₁ A (symₛ Γ p₁)) a₂}
                                                                                                                  (f≡₁ (F-hom A p₁ (symₛ Γ p₁) ◾ ap (F₁ A) (ab∘ba {Γ}{γ₁}{γ₂}p₁ (symₛ Γ p₁)) ◾ F-id A)))
                                                                                                        (resp (F₁ A (symₛ Γ p₁)) p₂))})
                  (λ { {γ₁ , a₁}{γ₂ , a₂}{γ₃ , a₃} (p₁ , p₂) (q₁ , q₂) → transₛ Γ p₁ q₁ , coe ( ∶∼≡₁ {F₀ A γ₃}{ (f (F₁ A q₁)) (f (F₁ A p₁) a₁)}{f (F₁ A (transₛ Γ p₁ q₁)) a₁}{a₃}
                                                                                                                                         (f≡₁ (F-hom A p₁ q₁)) )
                                                                                                                              (transₛ (F₀ A γ₃) (resp (F₁ A q₁) p₂) q₂)}) 




tr-sy-re₁ : {Γ : Setoid}{γ₁ γ₂ : ∣ Γ ∣}{r : Γ ∶ γ₁ ∼ γ₂} → transₛ Γ r (symₛ Γ r) ≡ reflₛ Γ
tr-sy-re₁ {Γ}{γ₁}{γ₂}{r} = (Γ ∼prp) (transₛ Γ r (symₛ Γ r)) (reflₛ Γ)

tr-sy-re₂ : {Γ : Setoid}{γ₁ γ₂ : ∣ Γ ∣}{r : Γ ∶ γ₁ ∼ γ₂} → transₛ Γ (symₛ Γ r) r ≡ reflₛ Γ
tr-sy-re₂ {Γ}{γ₁}{γ₂}{r} = (Γ ∼prp) (transₛ Γ (symₛ Γ r) r) (reflₛ Γ)


is-isoₛ : {Γ Δ : Setoid} → (Γ ⇒ₛ Δ) → Set
is-isoₛ {Γ} {Δ} h = Σ (Δ ⇒ₛ Γ) 
                               λ g → ((γ : ∣ Γ ∣) → f (g ∘ₛ h) γ ≡ γ)
                                     × ((δ : ∣ Δ ∣) → f (h ∘ₛ g) δ ≡ δ)

famsetoid-isos : {Γ : Setoid} {A : FamSetoid Γ} {γ₁ γ₂ : ∣ Γ ∣} → (r : Γ ∶ γ₁ ∼ γ₂) → is-isoₛ (F₁ A  r)
famsetoid-isos {Γ}{A}{γ₁}{γ₂} r = (F₁ A (symₛ Γ r))
                                                          , ((λ γ → f≡₁ (F-hom A r (symₛ Γ r) ◾ ((F₁≡ {Γ}{A} (tr-sy-re₁ {Γ})) ◾ (F-id A)))) 
                                                              , (λ δ → f≡₁ (F-hom A (symₛ Γ r) r ◾ ((F₁≡ {Γ}{A} (tr-sy-re₂ {Γ})) ◾ (F-id A)))))

      


{-
Setoid⇒ : {Γ Δ : Setoid} → Setoid
Setoid⇒ {Γ}{Δ} = cₛ (Γ ⇒ₛ Δ) 
                                   (λ ( g g' : Γ ⇒ₛ Δ ) →  { a a' : ∣ Γ ∣ } → (Γ ∶ a ∼ a' → Δ ∶ f g a ∼ f g' a'))
                                   (λ {x}{y} p q →  funexti (λ z → funexti p q (λ w → funext p q (λ r → (Δ ∼prp) (p r) (q r)))))
                                   (λ {g} → λ a → resp g a)
                                   (λ {g}{h} → λ gh a → symₛ Δ (gh (symₛ Γ a)))
                                   (λ {p}{q}{r} → λ pq qr a → transₛ Δ (pq a) (qr (reflₛ Γ))) 
-}

-- uses K, but this can be fixed very easily
⊤ₛ : Setoid
⊤ₛ = cₛ ⊤  _≡_  irr refl _⁻¹ _◾_
          where
            irr : {a b : ⊤} → (x y : a ≡ b) → x ≡ y
            irr {tt} {tt} refl refl = refl

×≡ : {A B : Set}{a₁ a₂ : A}{b₁ b₂ : B} → a₁ ≡ a₂ → b₁ ≡ b₂ → (a₁ , b₁) ≡ (a₂ , b₂)
×≡ refl refl = refl

prop-×-closed : {A B : Set} → ((a₁ a₂ : A) → a₁ ≡ a₂) → ((b₁ b₂ : B) → b₁ ≡ b₂) → (x y : A × B) → x ≡ y
prop-×-closed {A}{B} p q (proj₁ , proj₂) (proj₃ , proj₄) = ×≡ (p proj₁ proj₃) (q proj₂ proj₄)


_×ₛ_ : (Γ : Setoid) → (Δ : Setoid) → Setoid
(cₛ ∣Γ∣  _~Γ_ pΓ rΓ sΓ tΓ) ×ₛ (cₛ ∣Δ∣ _~Δ_ pΔ rΔ sΔ tΔ) = cₛ (∣Γ∣ × ∣Δ∣) 
                                                                                           (λ { (a₁ , b₁) (a₂ , b₂) → (a₁ ~Γ a₂) × (b₁ ~Δ b₂) } ) 
                                                                                           (prop-×-closed pΓ pΔ)
                                                                                           (λ {ab} → rΓ , rΔ) 
                                                                                           (λ {ab1}{ab2} x → (sΓ (proj₁ x)) , (sΔ (proj₂ x))) 
                                                                                           (λ {x}{y}{z} xy yz → (tΓ (proj₁ xy) (proj₁ yz)) , (tΔ (proj₂ xy) (proj₂ yz)))



proj₁ₛ : {Γ Δ : Setoid} → (Γ ×ₛ Δ) ⇒ₛ Γ
proj₁ₛ {Γ}{Δ} = cₘ (λ ab → proj₁ ab) (λ {(a₁ , b₁) → a₁}) 

proj₂ₛ : {Γ Δ : Setoid} → (Γ ×ₛ Δ) ⇒ₛ Δ
proj₂ₛ {Γ}{Δ} = cₘ (λ ab → proj₂ ab) (λ {(a₁ , b₁) → b₁})


{-
appₛ : {Γ Δ Θ : Setoid} → Γ ⇒ₛ Setoid⇒ {Δ}{Θ} → (Γ ×ₛ Δ) ⇒ₛ Θ
appₛ (cₘ f resp) = cₘ (λ γa →  _⇒ₛ_.f (f (proj₁ γa)) (proj₂ γa))
                                   (λ {γa₁}{γa₂} γa₁∼γa₂ → resp (proj₁ γa₁∼γa₂) (proj₂ γa₁∼γa₂))

λtₛ : {Γ Δ Θ : Setoid} → (Γ ×ₛ Δ) ⇒ₛ Θ → Γ ⇒ₛ Setoid⇒ {Δ}{Θ}
λtₛ {cₛ γ ∼γ pγ rγ sγ tγ} (cₘ f resp) = cₘ (λ γ₁ → cₘ (λ a₁ → f (γ₁ , a₁))
                                                                              (λ {a₁}{a₂} a₁∼a₂ → resp ( rγ , a₁∼a₂)))
                                                                  (λ {γ₁}{γ₂} γ₁∼γ₂ {a₁}{a₂} a₁∼a₂ → resp (γ₁∼γ₂ , a₁∼a₂))
-}

_,sₛ_ : {Γ Δ Θ : Setoid} → Γ ⇒ₛ Δ → Γ ⇒ₛ Θ → Γ ⇒ₛ (Δ ×ₛ Θ)
_,sₛ_  (cₘ fΓΔ respΓΔ) (cₘ fΓΘ respΓΘ) = cₘ (λ γ₁ → fΓΔ γ₁ , fΓΘ γ₁)
                                                                  (λ {γ₁}{γ₂} γ₁∼γ₂ →  respΓΔ γ₁∼γ₂ , respΓΘ γ₁∼γ₂)


