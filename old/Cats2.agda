module Cats2 where

record Cat : Set₁ where
  constructor c
  field
    Obj    : Set
    Obj=   : Obj → Obj → Set
    coeObj : Obj → Obj
    refObj : {I : Obj} → Obj= I I
    _⇒_    : Obj → Obj → Set
    ⇒=     : {I₀ I₁ : Obj}(I₂ : Obj= I₀ I₁)
             {J₀ J₁ : Obj}(J₂ : Obj= J₀ J₁)
           → I₀ ⇒ J₀ → I₁ ⇒ J₁ → Set
    ref⇒   : {I J : Obj}{f : I ⇒ J}
           → ⇒= (refObj {I}) (refObj {J}) f f
    coe⇒   : {I₀ I₁ : Obj}(I₂ : Obj= I₀ I₁)
             {J₀ J₁ : Obj}(J₂ : Obj= J₀ J₁)
           → I₀ ⇒ J₀ → I₁ ⇒ J₁
    idc    : {I : Obj} → I ⇒ I
    idc=   : {I₀ I₁ : Obj}{I₂ : Obj= I₀ I₁}
           → ⇒= I₂ I₂ (idc {I₀}) (idc {I₁})
    _∘c_   : {I J K : Obj} → J ⇒ K → I ⇒ J → I ⇒ K
    ∘c=    : {I₀ I₁ : Obj}(I₂ : Obj= I₀ I₁)
             {J₀ J₁ : Obj}(J₂ : Obj= J₀ J₁)
             {K₀ K₁ : Obj}(K₂ : Obj= K₀ K₁)
             {g₀ : J₀ ⇒ K₀}{g₁ : J₁ ⇒ K₁}(g₂ : ⇒= J₂ K₂ g₀ g₁)
             {f₀ : I₀ ⇒ J₀}{f₁ : I₁ ⇒ J₁}(f₂ : ⇒= I₂ J₂ f₀ f₁)
           → ⇒= I₂ K₂ (g₀ ∘c f₀) (g₁ ∘c f₁)
    idl    : {I J : Obj}{f : I ⇒ J}
           → ⇒= refObj refObj (idc ∘c f) f
    idr    : {I J : Obj}{f : I ⇒ J}
           → ⇒= refObj refObj (f ∘c idc) f
    ass    : {I J K L : Obj}{f : I ⇒ J}{g : J ⇒ K}{h : K ⇒ L}
           → ⇒= refObj refObj ((h ∘c g) ∘c f) (h ∘c (g ∘c f))
         
  infix 6 _∘c_
  infix 4 _⇒_
