{-# OPTIONS --without-K #-}

-- categorical application rule and conventional π₁ and π₂

module SyntaxOld where

open import Relation.Binary.PropositionalEquality

coe : {A B : Set} → A ≡ B → A → B
coe refl a = a

-- this is the projection ~ in the cubical syntax
_≡[_]≡_ : {A B : Set} → A → A ≡ B → B → Set
--x ≡[ e ]≡ y = coe e x ≡ y
x ≡[ refl ]≡ y = x ≡ y

data Con : Set
data Ty : Con → Set
data Tms : Con → Con → Set
data Tm : ∀ Γ → Ty Γ → Set

data Con where
  • : Con  -- \bub
  _,_ : (Γ : Con) → Ty Γ → Con

data Ty where
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
  U     : ∀{Γ} → Ty Γ
  Π     : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ

data Tms where
  ε   : ∀{Γ} → Tms Γ • 
  _,_ : ∀{Γ Δ}(δ : Tms Γ Δ) → {A : Ty Δ} → Tm Γ (A [ δ ]T) → Tms Γ (Δ , A)
  id  : ∀{Γ} → Tms Γ Γ
  _∘_ : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  π₁  : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ

data Tm where
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (δ : Tms Γ Δ) → Tm Γ (A [ δ ]T) 
  π₂    : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ π₁ ]T)
  app   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm Γ (Π A B) → Tm (Γ , A) B
  lam   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm (Γ , A) B → Tm Γ (Π A B)

Ty= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → Ty Γ₀ ≡ Ty Γ₁
Ty= refl = refl

Tms= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
       {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
     → Tms Γ₀ Δ₀ ≡ Tms Γ₁ Δ₁
Tms= refl refl = refl

_[_]T= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
         {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
         {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
         {δ₀ : Tms Γ₀ Δ₀}{δ₁ : Tms Γ₁ Δ₁}(δ₂ : δ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ δ₁)
       → A₀ [ δ₀ ]T ≡[ Ty= Γ₂ ]≡ A₁ [ δ₁ ]T
_[_]T= refl refl refl refl = refl


Tm= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
    → Tm Γ₀ A₀ ≡ Tm Γ₁ A₁
Tm= refl refl = refl

TmΓ= : {Γ : Con}
       {A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
     → Tm Γ A₀ ≡ Tm Γ A₁
TmΓ= α = Tm= refl α

postulate
   -- higher constructors for Ty
   [id]T  : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
   [][]T  : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → (A [ δ ]T) [ σ ]T ≡ A [ δ ∘ σ ]T
   U[]    : ∀{Γ Δ}{δ : Tms Γ Δ} → U [ δ ]T ≡ U

_^_ : ∀ {Γ Δ}(δ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ δ ]T) (Δ , A)
δ ^ A = (δ ∘ π₁) , coe (Tm= refl [][]T) π₂ 

postulate
   Π[]    : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
          → (Π A B) [ δ ]T ≡ Π (A [ δ ]T) (B [ δ ^ A ]T)
   setT   : ∀{Γ}{A B : Ty Γ}{e0 e1 : A ≡ B} → e0 ≡ e1

   -- higher constructors for Tms
   idl  : ∀{Γ Δ}{δ : Tms Γ Δ} → id ∘ δ ≡ δ 
   idr  : ∀{Γ Δ}{δ : Tms Γ Δ} → δ ∘ id ≡ δ 
   ass  : ∀{Δ Γ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Γ Σ}{ν : Tms Δ Γ} → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
   π₁β  : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₁ ∘ (δ , a) ≡ δ
   πid  : ∀{Γ}{A : Ty Γ} → (π₁ , π₂) ≡ (id {Γ , A})
   εη   : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε
   sets : ∀{Γ Δ}{δ σ : Tms Γ Δ}{e0 e1 : δ ≡ σ} → e0 ≡ e1

   -- higher constructors for Tm
   [id]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≡[ TmΓ= [id]T ]≡ t
   [][]t : ∀{Γ Δ Σ}{A : Ty Σ}{t : Tm Σ A}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
         → (t [ δ ]t) [ σ ]t ≡[ Tm= refl [][]T ]≡  t [ δ ∘ σ ]t
   π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
         → π₂ [ δ , a ]t ≡[ TmΓ= (trans [][]T (_[_]T= refl refl refl π₁β)) ]≡ a
   app[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm Δ (Π A B)}
         → app (coe (Tm= refl Π[]) (t [ δ ]t)) ≡ (app t) [ δ ^ A ]t
   lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B}                       
         → (lam t) [ δ ]t ≡[ Tm= refl Π[] ]≡ lam (t [ δ ^ A ]t)
   Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
         →  app (lam t) ≡ t
   Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
         → lam (app t) ≡ t
   sett  : ∀{Γ}{A : Ty Γ}{u v : Tm Γ A}{e0 e1 : u ≡ v} → e0 ≡ e1

-- _≡_ is ~Con=
~Con== : {Γ₀₀ Γ₀₁ : Con}(Γ₀₂ : Γ₀₀ ≡ Γ₀₁)
         {Γ₁₀ Γ₁₁ : Con}(Γ₁₂ : Γ₁₀ ≡ Γ₁₁)
       → (Γ₀₀ ≡ Γ₁₀) ≡ (Γ₀₁ ≡ Γ₁₁)
~Con== refl refl = refl

~Ty== : {Γ₀₀ Γ₀₁ : Con}(Γ₀₂ : Γ₀₀ ≡ Γ₀₁)
        {Γ₁₀ Γ₁₁ : Con}(Γ₁₂ : Γ₁₀ ≡ Γ₁₁)
        (Γ₂₀ : Γ₀₀ ≡ Γ₁₀)(Γ₂₁ : Γ₀₁ ≡ Γ₁₁)(Γ₂₂ : Γ₂₀ ≡[ ~Con== Γ₀₂ Γ₁₂ ]≡ Γ₂₁)
        {A₀₀ : Ty Γ₀₀}{A₀₁ : Ty Γ₀₁}(A₀₂ : A₀₀ ≡[ Ty= Γ₀₂ ]≡ A₀₁)
        {A₁₀ : Ty Γ₁₀}{A₁₁ : Ty Γ₁₁}(A₁₂ : A₁₀ ≡[ Ty= Γ₁₂ ]≡ A₁₁)
      → (A₀₀ ≡[ Ty= Γ₂₀ ]≡ A₁₀) ≡ (A₀₁ ≡[ Ty= Γ₂₁ ]≡ A₁₁)
~Ty== refl refl p (.p) refl refl refl = refl

~~Con== : {Γ₀₀ Γ₀₁ : Con}(Γ₀₂ : Γ₀₀ ≡ Γ₀₁)
          {Γ₁₀ Γ₁₁ : Con}(Γ₁₂ : Γ₁₀ ≡ Γ₁₁)
        → Γ₀₀ ≡ Γ₁₀ → Γ₀₁ ≡ Γ₁₁ → Set
~~Con== refl refl p q = p ≡ q

~~Con==' : {Γ₀₀ Γ₀₁ : Con}(Γ₀₂ : Γ₀₀ ≡ Γ₀₁)
           {Γ₁₀ Γ₁₁ : Con}(Γ₁₂ : Γ₁₀ ≡ Γ₁₁)
         → Γ₀₀ ≡ Γ₁₀ → Γ₀₁ ≡ Γ₁₁ → Set
~~Con==' Γ₀₂ Γ₁₂ Γ₂₀ Γ₂₁ = Γ₂₀ ≡[ ~Con== Γ₀₂ Γ₁₂ ]≡ Γ₂₁

~Tms== : {Γ₀₀ Γ₀₁ : Con}(Γ₀₂ : Γ₀₀ ≡ Γ₀₁)
         {Γ₁₀ Γ₁₁ : Con}(Γ₁₂ : Γ₁₀ ≡ Γ₁₁)
         (Γ₂₀ : Γ₀₀ ≡ Γ₁₀)(Γ₂₁ : Γ₀₁ ≡ Γ₁₁)(Γ₂₂ : Γ₂₀ ≡[ ~Con== Γ₀₂ Γ₁₂ ]≡ Γ₂₁)
         {Δ₀₀ Δ₀₁ : Con}(Δ₀₂ : Δ₀₀ ≡ Δ₀₁)
         {Δ₁₀ Δ₁₁ : Con}(Δ₁₂ : Δ₁₀ ≡ Δ₁₁)
         (Δ₂₀ : Δ₀₀ ≡ Δ₁₀)(Δ₂₁ : Δ₀₁ ≡ Δ₁₁)(Δ₂₂ : Δ₂₀ ≡[ ~Con== Δ₀₂ Δ₁₂ ]≡ Δ₂₁)
         {δ₀₀ : Tms Γ₀₀ Δ₀₀}{δ₀₁ : Tms Γ₀₁ Δ₀₁}(δ₀₂ : δ₀₀ ≡[ Tms= Γ₀₂ Δ₀₂ ]≡ δ₀₁)
         {δ₁₀ : Tms Γ₁₀ Δ₁₀}{δ₁₁ : Tms Γ₁₁ Δ₁₁}(δ₁₂ : δ₁₀ ≡[ Tms= Γ₁₂ Δ₁₂ ]≡ δ₁₁)
       → (δ₀₀ ≡[ Tms= Γ₂₀ Δ₂₀ ]≡ δ₁₀) ≡ (δ₀₁ ≡[ Tms= Γ₂₁ Δ₂₁ ]≡ δ₁₁)
~Tms== refl refl p (.p) refl
       refl refl q (.q) refl
       refl refl             = refl

