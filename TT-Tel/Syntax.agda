{-# OPTIONS --without-K #-}

module TT-Tel.Syntax where

open import lib

--open Relation.Binary.PropositionalEqualitylib

-- types of the syntax

data Con : Set
data Ty : Con → Set
data Tm : ∀ Γ → Ty Γ → Set
data Tel : Con → Set
data Tms : ∀ Γ → Tel Γ → Set

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con) → Ty Γ → Con
  _++_  : (Γ : Con) → Tel Γ → Con

data Ty where
  _[_] : ∀{Γ}{Δ : Tel Γ} → Ty (Γ ++ Δ) → Tms Γ Δ → Ty Γ

data Tel where
  •      : ∀ {Γ} → Tel Γ
  _,_    : ∀ {Γ} → (Δ : Tel Γ)(A : Ty (Γ ++ Δ)) → Tel Γ
  _++_   : ∀ {Γ} → (Δ : Tel Γ)(Θ : Tel (Γ ++ Δ)) → Tel Γ

data Tms where
  ε      : ∀ {Γ} → Tms Γ •
  _,_    : ∀ {Γ}{Δ : Tel Γ}(δ : Tms Γ Δ){A : Ty (Γ ++ Δ)}
           (a : Tm Γ (A [ δ ])) → Tms Γ (Δ , A)
  _∘_    : ∀ {Γ}{Δ : Tel Γ}{Θ : Tel (Γ ++ Δ)}
           (δ : Tms Γ Δ)(θ : Tms (Γ ++ Δ) Θ) → Tms Γ (Δ ++ Θ)
  π₁     : ∀{Γ Δ A} → Tms Γ (Δ , A) → Tms Γ Δ

data Tm where
  _[_] : ∀{Γ}{Δ : Tel Γ}{A : Ty (Γ ++ Δ)}
       → Tm (Γ ++ Δ) A → (ts : Tms Γ Δ) → Tm Γ (A [ ts ])
  π₂ : ∀{Γ Δ A}(δ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ δ ])

postulate
  •Con : ∀ {Γ : Con} → Γ ++ • ≡ Γ
  ++Con : ∀{Γ}{Δ : Tel Γ}{A : Ty (Γ ++ Δ)} → _≡_ {A = Con} (Γ ++ (Δ , A)) ((Γ ++ Δ) , A)
  assCon : ∀ {Γ : Con}{Δ : Tel Γ}{Θ : Tel (Γ ++ Δ)} → _≡_ {A = Con} (Γ ++ (Δ ++ Θ)) ((Γ ++ Δ) ++ Θ)

zero : ∀ {Γ A} → Tm (Γ , A) {!A!}
zero = {!!}

{-
  εTms : γ ∘ ε ≡ γ
  ,Tms : γ ∘ (δ , a) ≡ (γ ∘ δ , a)
  ∘Tms : γ ∘ (δ ∘ θ) ≡ (γ ∘ δ) ∘ θ
  εTy : A [ ε ] ≡ A
  ∘Ty : A [ δ ∘ γ ] ≡ A [ δ ] [ γ ]
  εTm : a [ ε ] ≡ a
  ∘Ty : a [ δ ∘ γ ] ≡ a [ δ ] [ γ ]
-}


{-
-- a congruence rule (corresponds to refl(TmΓ) in the cubical syntax:
-- Γ is in the context, we degenerate it)
TmΓ= : {Γ : Con}{A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
     → Tm Γ A₀ ≡ Tm Γ A₁
TmΓ= {Γ} p = ap (Tm Γ) p

-- constructors
-- note: we are using the categorical application rule and π₁, π₂

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con) → Ty Γ → Con

data Ty where
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
  U     : ∀{Γ} → Ty Γ
  Π     : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
  El    : ∀{Γ}(A : Tm Γ U) → Ty Γ

data Tms where
  ε     : ∀{Γ} → Tms Γ •
  _,_   : ∀{Γ Δ}(δ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ δ ]T) → Tms Γ (Δ , A)
  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ

data Tm where
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (δ : Tms Γ Δ) → Tm Γ (A [ δ ]T) 
  π₂    : ∀{Γ Δ}{A : Ty Δ}(δ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ δ ]T)
  app   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm Γ (Π A B) → Tm (Γ , A) B
  lam   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)} → Tm (Γ , A) B → Tm Γ (Π A B)

-- higher contructors are postulated

postulate
   -- higher constructors for Ty
   [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
   [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → (A [ δ ]T) [ σ ]T ≡ A [ δ ∘ σ ]T
   U[]   : ∀{Γ Δ}{δ : Tms Γ Δ} → U [ δ ]T ≡ U
   El[]  : ∀{Γ Δ}{δ : Tms Γ Δ}{Â : Tm Δ U} → El Â [ δ ]T ≡ El (coe (TmΓ= U[]) (Â [ δ ]t))

_^_ : ∀{Γ Δ}(δ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ δ ]T) (Δ , A)
δ ^ A = (δ ∘ π₁ id) , coe (TmΓ= [][]T) (π₂ id)

postulate
   Π[]   : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
         → (Π A B) [ δ ]T ≡ Π (A [ δ ]T) (B [ δ ^ A ]T)
   setT  : ∀{Γ}{A B : Ty Γ}{e0 e1 : A ≡ B} → e0 ≡ e1

   -- higher constructors for Tms
   idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → id ∘ δ ≡ δ 
   idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → δ ∘ id ≡ δ 
   ass   : ∀{Δ Γ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Γ Σ}{ν : Tms Δ Γ}
         → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
   ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
         → (δ , a) ∘ σ ≡ (δ ∘ σ) , coe (TmΓ= [][]T) (a [ σ ]t)
   π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₁ (δ , a) ≡ δ
   πη    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)} → (π₁ δ , π₂ δ) ≡ δ
   εη    : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε
   sets  : ∀{Γ Δ}{δ σ : Tms Γ Δ}{e0 e1 : δ ≡ σ} → e0 ≡ e1

   -- higher constructors for Tm
   [id]t : ∀{Γ}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≡[ TmΓ= [id]T ]≡ t
   [][]t : ∀{Γ Δ Σ}{A : Ty Σ}{t : Tm Σ A}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
         → (t [ δ ]t) [ σ ]t ≡[ TmΓ= [][]T ]≡  t [ δ ∘ σ ]t
   π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
         → π₂ (δ , a) ≡[ TmΓ= (ap (λ z → A [ z ]T) π₁β) ]≡ a
   lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B}                       
         → (lam t) [ δ ]t ≡[ TmΓ= Π[] ]≡ lam (t [ δ ^ A ]t)
   Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
         →  app (lam t) ≡ t
   Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
         → lam (app t) ≡ t
   sett  : ∀{Γ}{A : Ty Γ}{u v : Tm Γ A}{e0 e1 : u ≡ v} → e0 ≡ e1

-- some defined conversion rules

app= : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t₀ t₁ : Tm Γ (Π A B)} → t₀ ≡ t₁ → app t₀ ≡ app t₁
app= refl = refl

app[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm Δ (Π A B)}
      → app (coe (TmΓ= Π[]) (t [ δ ]t)) ≡ (app t) [ δ ^ A ]t
app[] {Γ}{Δ}{δ}{A}{B}{t}
  = app (coe (TmΓ= Π[]) (t [ δ ]t))
                                                ≡⟨ Πβ ⁻¹ ⟩
    app (lam (app (coe (TmΓ= Π[]) (t [ δ ]t))))
                                                ≡⟨ app= Πη ⟩
    app (coe (TmΓ= Π[]) (t [ δ ]t))
                                                ≡⟨ ap (λ z → app (coe (TmΓ= Π[]) (z [ δ ]t)))
                                                      (Πη ⁻¹) ⟩
    app (coe (TmΓ= Π[]) (lam (app t) [ δ ]t))
                                                ≡⟨ app= lam[] ⟩
    app (lam (app t [ δ ^ A ]t))
                                                ≡⟨ Πβ ⟩
    app t [ δ ^ A ]t
                                                ∎

π₁∘ : ∀{Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
     → π₁ δ ∘ ρ ≡ π₁ (δ ∘ ρ)
π₁∘ {Γ}{Δ}{Θ}{A}{δ}{ρ}
  = π₁ δ ∘ ρ
                                                ≡⟨ π₁β ⁻¹ ⟩
    π₁ ((π₁ δ ∘ ρ) , coe (TmΓ= [][]T) (π₂ δ [ ρ ]t))
                                                ≡⟨ ap π₁ (,∘ ⁻¹) ⟩
    π₁ ((π₁ δ , π₂ δ) ∘ ρ )
                                                ≡⟨ ap (λ σ → π₁ (σ ∘ ρ)) πη ⟩
    π₁ (δ ∘ ρ)
                                                ∎

π₂[]tot : ∀{Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
      → _≡_ {A = Σ Set λ X → X}
            (Tm Θ (A [ π₁ δ ]T [ ρ ]T) , π₂ δ [ ρ ]t)
            (Tm Θ (A [ π₁ (δ ∘ ρ) ]T)  , π₂ (δ ∘ ρ))
π₂[]tot {Γ}{Δ}{Θ}{A}{δ}{ρ}

  =       Tm Θ (A [ π₁ δ ]T [ ρ ]T)                                    , π₂ δ [ ρ ]t
  
  ≡⟨ Σ=' (TmΓ= [][]T)                                                    refl ⟩
        
          Tm Θ (A [ π₁ δ ∘ ρ ]T)                                       , coe (TmΓ= [][]T) (π₂ δ [ ρ ]t)
        
  ≡⟨ Σ=' (TmΓ= (ap (λ z → A [ z ]T) (π₁β ⁻¹)))                           [ π₁β , π₂β ]⁻¹ ⟩
        
          Tm Θ (A [ π₁ (π₁ δ ∘ ρ , coe (TmΓ= [][]T) (π₂ δ [ ρ ]t)) ]T) , π₂ (π₁ δ ∘ ρ , coe (TmΓ= [][]T) (π₂ δ [ ρ ]t))

  ≡⟨ Σ=' (ap (λ z → Tm Θ (A [ π₁ z ]T)) (,∘ ⁻¹))                        (apd π₂ (,∘ ⁻¹)) ⟩
  
          Tm Θ (A [ π₁ ((π₁ δ , π₂ δ) ∘ ρ) ]T)                         , π₂ ((π₁ δ , π₂ δ) ∘ ρ)
  
  ≡⟨ Σ=' (ap (λ z → Tm Θ (A [ π₁ (z ∘ ρ) ]T)) πη)                       (apd (λ z → π₂ (z ∘ ρ)) πη) ⟩
  
          Tm Θ (A [ π₁ (δ ∘ ρ) ]T)                                     , π₂ (δ ∘ ρ)
  
  ∎
  where
    [_,_]⁻¹ : ∀{Γ Δ}{A : Ty Δ}{ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
              {t₀ : Tm Γ (A [ ρ₀ ]T)}{t₁ : Tm Γ (A [ ρ₁ ]T)}
              (p : t₀ ≡[ TmΓ= (ap (λ z → A [ z ]T) ρ₂) ]≡ t₁)
            → t₁ ≡[ TmΓ= (ap (λ z → A [ z ]T) (ρ₂ ⁻¹)) ]≡ t₀
    [ refl , refl ]⁻¹ = refl
  

π₂[] : ∀{Γ Δ Θ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ρ : Tms Θ Γ}
    → π₂ δ [ ρ ]t ≡[ Σ=0 π₂[]tot ]≡ π₂ (δ ∘ ρ)
π₂[] = Σ=1' π₂[]tot

{-
app' : ∀ {Γ}{A : Ty Γ}{B : Ty (Γ , A)}
     → Tm ((Γ , Π A B) , A [ π₁ id ]T) (B [ (π₁ id) ^ A ]T)
app' {Γ}{A}{B}
  = coe (TmΓ= ([][]T ◾ ap (λ z → B [ π₁ id ^ A ]T [ z ]T) α ◾ [id]T))
        (app (coe (TmΓ= Π[])
                  (coe (TmΓ= Π[])
                       (π₂ {A = Π A B} id) [ π₁ {A = A [ π₁ id ]T} id ]t))
         [ id , coe (TmΓ= ([id]T ⁻¹)) (π₂ id) ]t)
  where
  
    ,= : ∀{Γ Δ}{ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
         {A : Ty Δ}{t₀ : Tm Γ (A [ ρ₀ ]T)}{t₁ : Tm Γ (A [ ρ₁ ]T)}
         (t₂ : t₀ ≡[ ap (λ z → Tm Γ (A [ z ]T)) ρ₂ ]≡ t₁)
       → _≡_ {A = Tms Γ (Δ , A)} (ρ₀ , t₀) (ρ₁ , t₁)
    ,= refl refl = refl

    α : (π₁ id ^ (A [ π₁ id ]T)) ∘ (id , coe (TmΓ= ([id]T ⁻¹)) (π₂ id)) ≡ id
    α =   
       ((π₁ id ∘ π₁ id) , m) ∘ (id , n)
                                                ≡⟨ ap (λ z → z ∘ (id , n)) (,= (π₁∘ ◾ ap π₁ idl) refl) ⟩
       ((π₁ (π₁ id)) , coe (ap (λ z → Tm (((Γ , Π A B) , (A [ π₁ id ]T)) , ((A [ π₁ id ]T) [ π₁ id ]T)) ((A [ π₁ id ]T) [ z ]T)) (π₁∘ ◾ ap π₁ idl)) m) ∘ (id , n)
                                                ≡⟨ {!!} ⟩
       id
                                                ∎
      where

        m = coe (TmΓ= [][]T) (π₂ id)
        n = coe (TmΓ= ([id]T ⁻¹)) (π₂ id)
-}



-}
