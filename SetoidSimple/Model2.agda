{-# OPTIONS --no-eta #-}

-- setoids are decorated with a predicate saying that it is decidable
-- whether the equality is reflexivity. evverything preserves
-- reflexivity, because we put in this property

module SetoidSimple.Model2 where

open import Agda.Primitive

open import lib

open import SetoidSimple.Syntax
open import SetoidSimple.Rec

data Dec {i} (A : Set i) : Set i where
  yes : A → Dec A
  no  : (A → ⊥) → Dec A

----------------------------------------------------------------------
-- Definition of setoids
----------------------------------------------------------------------

record Presetoid : Set₁ where
  field
    ∣_∣p : Set
    _∶p_~_ : ∣_∣p → ∣_∣p → Set
    refp : (a : ∣_∣p) → _∶p_~_ a a

  infix 4 ∣_∣p
  infix 5 _∶p_~_

open Presetoid

data isReflp (A : Presetoid) : {a a' : ∣ A ∣p} → A ∶p a ~ a' → Set where
  refl : (a : ∣ A ∣p) → isReflp A (refp A a)

isReflJ : {A : Presetoid}{a : ∣ A ∣p}(P : {a' : ∣ A ∣p} → A ∶p a ~ a' → Set)
        → P (refp A a) → {a' : ∣ A ∣p}{w : A ∶p a ~ a'} → isReflp A w → P w
isReflJ P p (refl a) = p

record Setoid : Set₁ where
  field
    ps : Presetoid
    refd : {a₀ a₁ : ∣ ps ∣p}(p : ps ∶p a₀ ~ a₁) → Dec (isReflp ps p)

open Setoid

∣_∣ : Setoid → Set
∣ A ∣ = ∣ ps A ∣p

_∶_~_ : (A : Setoid) → ∣ A ∣ → ∣ A ∣ → Set
_∶_~_ A = _∶p_~_ (ps A)

infix 5 _∶_~_

ref : (A : Setoid) → (a : ∣ A ∣) → A ∶ a ~ a
ref A = refp (ps A)

isRefl : (A : Setoid) {a a' : ∣ A ∣} → A ∶ a ~ a' → Set
isRefl A = isReflp (ps A)

record _→s_ (A B : Setoid) : Set where
  field
    map  : ∣ A ∣ → ∣ B ∣
    resp : {a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (map a₀) ~ (map a₁)
--    refs : (a : ∣ A ∣) → isRefl B (resp (ref A a))

infix 3 _→s_
open _→s_

=resp : (A B : Setoid)
        {map₀ map₁ : ∣ A ∣ → ∣ B ∣}(map₂ : map₀ ≡ map₁)
      → ({a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (map₀ a₀) ~ (map₀ a₁))
      ≡ ({a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (map₁ a₀) ~ (map₁ a₁))
=resp _ _ refl = refl

=refs : (A B : Setoid)
        {map₀ map₁ : ∣ A ∣ → ∣ B ∣}(map₂ : map₀ ≡ map₁)
        {resp₀ : {a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (map₀ a₀) ~ (map₀ a₁)}
        {resp₁ : {a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (map₁ a₀) ~ (map₁ a₁)}
        (resp₂ : (λ {a₀}{a₁} → resp₀ {a₀}{a₁}) ≡[ =resp A B map₂ ]≡ resp₁)
      → ((a : ∣ A ∣) → isRefl B (resp₀ (ref A a)))
      ≡ ((a : ∣ A ∣) → isRefl B (resp₁ (ref A a)))
=refs _ _ refl refl = refl

→s= : {A B : Setoid}      {map₀ map₁ : ∣ A ∣ → ∣ B ∣}(map₂ : map₀ ≡ map₁)
      {resp₀ : {a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (map₀ a₀) ~ (map₀ a₁)}
      {resp₁ : {a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (map₁ a₀) ~ (map₁ a₁)}
      (resp₂ : (λ {a₀}{a₁} → resp₀ {a₀}{a₁}) ≡[ =resp A B map₂ ]≡ resp₁)
--      {refs₀ : (a : ∣ A ∣) → isRefl B (resp₀ (ref A a))}
--      {refs₁ : (a : ∣ A ∣) → isRefl B (resp₁ (ref A a))}
--      (refs₂ : refs₀ ≡[ =refs A B map₂ resp₂ ]≡ refs₁)
    → _≡_ {A = A →s B}
          (record { map = map₀ ; resp = resp₀ })
          (record { map = map₁ ; resp = resp₁ })
→s= refl refl = refl

infix 4 [_∶_~*_]

data [_∶_~*_] {A : Set}(R : A → A → Set) : A → A → Set where
  refl : ∀ a → [ R ∶ a ~* a ]
  η : ∀ {a b} → R a b → [ R ∶ a ~* b ]

[_⇒_∶_~_] : (a b : Setoid) → (a →s b) → (a →s b) → Set
[_⇒_∶_~_] a b f g = [ (λ f₀ f₁ → {a₀ a₁ : ∣ a ∣} → a ∶ a₀ ~ a₁ → b ∶ map f₀ a₀ ~ map f₁ a₁) ∶ f ~* g ]

{-
data [_⇒_∶_~_] (A B : Setoid) : (A →s B) → (A →s B) → Set where
  refl : (f : A →s B) → [ A ⇒ B ∶ f ~ f ]
  ext  : {f₀ f₁ : A →s B} → ({a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ map f₀ a₀ ~ map f₁ a₁)
       → [ A ⇒ B ∶ f₀ ~ f₁ ]
-}

infix 4 [_⇒_∶_~_]

----------------------------------------------------------------------
-- Types
----------------------------------------------------------------------

postulate
  ιᴹ : Setoid

_⇒ᴹ_ : Setoid → Setoid → Setoid
A ⇒ᴹ B = record
  { ps = PS
  ; refd = REFD
  }
  where
    PS : Presetoid
    PS = record
      { ∣_∣p = A →s B
      ; _∶p_~_ = λ t₀ t₁ → [ A ⇒ B ∶ t₀ ~ t₁ ]
      ; refp = refl
      } 
   
    REFD : {a₀ a₁ : A →s B}(p : [ A ⇒ B ∶ a₀ ~ a₁ ])
         → Dec (isReflp PS p)
    REFD (refl f) = yes (refl f)
    REFD (η x) = no λ ()


----------------------------------------------------------------------
-- Contexts
----------------------------------------------------------------------

•ᴹ : Setoid
•ᴹ = record
  { ps = record
    { ∣_∣p = ⊤
    ; _∶p_~_ = λ _ _ → ⊤
    ; refp = λ _ → tt
    }
  ; refd = λ { tt → yes (refl _) }
  }

_,ᴹ_ : Setoid → Setoid → Setoid
Γ ,ᴹ A = record
  { ps = PS
  ; refd = REFD
  }
  where
    PS : Presetoid
    PS = record
      { ∣_∣p = ∣ Γ ∣ × ∣ A ∣
      ; _∶p_~_ = λ { (γ₀ , a₀) (γ₁ , a₁) → Γ ∶ γ₀ ~ γ₁ × A ∶ a₀ ~ a₁ }
      ; refp = λ { (γ , a) → ref Γ γ , ref A a }
      }

    projisReflp₁ : {γ₀ γ₁ : ∣ Γ ∣}{γ₂ : Γ ∶ γ₀ ~ γ₁}{a₀ a₁ : ∣ A ∣}{a₂ : A ∶ a₀ ~ a₁}
                → isReflp PS (γ₂ , a₂) → isReflp (ps Γ) γ₂
    projisReflp₁ (refl _) = refl _

    projisReflp₂ : {γ₀ γ₁ : ∣ Γ ∣}{γ₂ : Γ ∶ γ₀ ~ γ₁}{a₀ a₁ : ∣ A ∣}{a₂ : A ∶ a₀ ~ a₁}
                → isReflp PS (γ₂ , a₂) → isReflp (ps A) a₂
    projisReflp₂ (refl _) = refl _

    REFD : {a₀ a₁ : ∣ PS ∣p} (p : PS ∶p a₀ ~ a₁) → Dec (isReflp PS p)
    REFD (γ₂ , a₂) with refd Γ γ₂ | refd A a₂
    REFD (_ , _) | yes (refl γ) | yes (refl a) = yes (refl (γ , a))
    REFD (γ₂ , a₂) | yes _ | no w = no λ p → w (projisReflp₂ p)
    REFD (γ₂ , a₂) | no  w | _ = no λ p → w (projisReflp₁ p )

,isRefl : (Γ : Setoid){γ₀ γ₁ : ∣ Γ ∣}{γ₂ : Γ ∶ γ₀ ~ γ₁} → isRefl Γ γ₂
        → (A : Setoid){a₀ a₁ : ∣ A ∣}{a₂ : A ∶ a₀ ~ a₁} → isRefl A a₂
        → isRefl (Γ ,ᴹ A) (γ₂ , a₂)
,isRefl _ (refl γ) _ (refl a) = refl (γ , a)

projisRefl₁ : (Γ : Setoid){γ₀ γ₁ : ∣ Γ ∣}{γ₂ : Γ ∶ γ₀ ~ γ₁}
              (A : Setoid){a₀ a₁ : ∣ A ∣}{a₂ : A ∶ a₀ ~ a₁}
            → isRefl (Γ ,ᴹ A) (γ₂ , a₂) → isRefl Γ γ₂
projisRefl₁ _ _ (refl _) = refl _

projisRefl₂ : (Γ : Setoid){γ₀ γ₁ : ∣ Γ ∣}{γ₂ : Γ ∶ γ₀ ~ γ₁}
              (A : Setoid){a₀ a₁ : ∣ A ∣}{a₂ : A ∶ a₀ ~ a₁}
            → isRefl (Γ ,ᴹ A) (γ₂ , a₂) → isRefl A a₂
projisRefl₂ _ _ (refl _) = refl _

----------------------------------------------------------------------
-- Substitution calculus
----------------------------------------------------------------------

idᴹ : {Γ : Setoid} → Γ →s Γ
idᴹ = record
  { map = λ γ → γ
  ; resp = λ γ₂ → γ₂
--  ; refs = λ γ → refl γ
  }

_∘ᴹ_ : {Γ Θ Δ : Setoid} → Θ →s Δ → Γ →s Θ → Γ →s Δ
_∘ᴹ_ {Γ}{Θ}{Δ} ν σ = record
  { map = λ γ → map ν (map σ γ)
  ; resp = λ γ₂ → resp ν (resp σ γ₂)
--  ; refs = λ γ → isReflJ (λ w → isRefl Δ (resp ν w)) (refs ν (map σ γ)) (refs σ γ)
  }

εᴹ : {Γ : Setoid} → Γ →s •ᴹ
εᴹ = record
  { map = λ _ → tt
  ; resp = λ _ → tt
--  ; refs = λ _ → refl _
  }

_,sᴹ_ : {Γ Δ : Setoid} → Γ →s Δ → {A : Setoid} → Γ →s A → Γ →s (Δ ,ᴹ A)
_,sᴹ_ {Γ}{Δ} σ {A} t = record
  { map = λ γ → map σ γ , map t γ
  ; resp = λ γ₂ → resp σ γ₂ , resp t γ₂
--  ; refs = λ γ → ,isRefl Δ (refs σ γ) A (refs t γ)
  }

π₁ᴹ : {Γ Δ A : Setoid} → Γ →s (Δ ,ᴹ A) → Γ →s Δ
π₁ᴹ {Γ}{Δ}{A} σ = record
  { map = λ γ → proj₁ (map σ γ)
  ; resp = λ γ₂ → proj₁ (resp σ γ₂)
--  ; refs = λ γ → projisRefl₁ Δ A (refs σ γ)
  }

_[_]tᴹ : {Γ Δ A : Setoid} → Δ →s A → Γ →s Δ → Γ →s A
_[_]tᴹ {Γ}{Δ}{A} t σ = record
  { map = λ γ → map t (map σ γ)
  ; resp = λ γ₂ → resp t (resp σ γ₂)
--  ; refs = λ γ → isReflJ (λ w → isRefl A (resp t w)) (refs t (map σ γ)) (refs σ γ)
  }

π₂ᴹ : {Γ Δ A : Setoid} → Γ →s (Δ ,ᴹ A) → Γ →s A
π₂ᴹ {Γ}{Δ}{A} σ = record
  { map  = λ γ → proj₂ (map σ γ)
  ; resp = λ γ₂ → proj₂ (resp σ γ₂)
--  ; refs = λ γ → projisRefl₂ Δ A (refs σ γ)
  }

idlᴹ : {Γ Δ : Setoid} {δ : Γ →s Δ} → (idᴹ ∘ᴹ δ) ≡ δ
idlᴹ {Γ}{Δ}{δ = record { map = map ; resp = resp }}
  = →s= refl refl 

idrᴹ : {Γ Δ : Setoid} {δ : Γ →s Δ} → (δ ∘ᴹ idᴹ) ≡ δ
idrᴹ {Γ}{Δ}{δ = record { map = map ; resp = resp }}
  = →s= refl refl 

assᴹ : {Δ Γ Σ Ω : Setoid}{σ : Σ →s Ω}{δ : Γ →s Σ}{ν : Δ →s Γ}
     → ((σ ∘ᴹ δ) ∘ᴹ ν) ≡ (σ ∘ᴹ (δ ∘ᴹ ν))
assᴹ {Δ}{Γ}{Σ}{Ω}
     {σ = record { map = mapσ ; resp = respσ }}
     {δ = record { map = mapδ ; resp = respδ }}
     {ν = record { map = mapν ; resp = respν }}
  = →s= refl refl

,∘ᴹ : {Γ Δ Σ : Setoid}{δ : Γ →s Δ}{σ : Σ →s Γ}
      {A : Setoid}{a : Γ →s A}
    → ((δ ,sᴹ a) ∘ᴹ σ) ≡ ((δ ∘ᴹ σ) ,sᴹ (a [ σ ]tᴹ))
,∘ᴹ {Γ}{Δ}{Σ}
    {record { map = mapδ ; resp = respδ }}
    {record { map = mapσ ; resp = respσ }}
    {A}
    {record { map = mapa ; resp = respa }}
  = →s= refl refl

,β₁ᴹ : {Γ Δ A : Setoid}{δ : Γ →s Δ}{a : Γ →s A} → π₁ᴹ {A = A} (δ ,sᴹ a) ≡ δ
,β₁ᴹ {Γ}{Δ}{A}
     {record { map = mapδ ; resp = respδ }}
     {record { map = mapa ; resp = respa }}
  = →s= refl refl

,ηᴹ : {Γ Δ A : Setoid}{δ : Γ →s (Δ ,ᴹ A)} → (π₁ᴹ {Γ}{Δ}{A} δ ,sᴹ π₂ᴹ {Γ}{Δ}{A} δ) ≡ δ
,ηᴹ {Γ}{Δ}{A}
    {record { map = mapδ ; resp = respδ }}
  = →s= refl refl

•ηᴹ : {Γ : Setoid}{σ : Γ →s •ᴹ} → σ ≡ εᴹ
•ηᴹ {Γ}{record { map = mapσ ; resp = respσ }}
  = →s= refl refl

,β₂ᴹ : {Γ Δ A : Setoid}{δ : Γ →s Δ}{a : Γ →s A} → π₂ᴹ {Δ = Δ} (δ ,sᴹ a) ≡ a
,β₂ᴹ {Γ}{Δ}{A}
     {record { map = mapδ ; resp = respδ }}
     {record { map = mapa ; resp = respa }}
  = →s= refl refl

----------------------------------------------------------------------
-- Function space
----------------------------------------------------------------------

lamᴹ : {Γ A B : Setoid} → (Γ ,ᴹ A) →s B → Γ →s (A ⇒ᴹ B)
lamᴹ {Γ}{A}{B} t =
 record
  { map = λ γ → record
     { map  = λ a → map t (γ , a)
     ; resp = λ a₂ → resp t (ref Γ γ , a₂)
--     ; refs = λ a → refs t (γ , a)
     }
  ; resp = {!!} -- λ γ₂ a₂ → resp t (γ₂ , a₂)
--  ; refs = {!!}
  }

appᴹ : {Γ A B : Setoid} → Γ →s (A ⇒ᴹ B) → (Γ ,ᴹ A) →s B
appᴹ {Γ}{A}{B} t = record
  { map  = λ { (γ , a) → map (map t γ) a }
  ; resp = λ { (γ₂ , a₂) → {!!} } -- resp t γ₂ a₂
--  ; refs = {!!}
  }

lam[]ᴹ : {Γ Δ : Setoid}{δ : Γ →s Δ}{A B : Setoid}{t : (Δ ,ᴹ A) →s B}
       → (lamᴹ {Δ}{A}{B} t [ δ ]tᴹ)
       ≡ lamᴹ (t [ (δ ∘ᴹ π₁ᴹ {Γ ,ᴹ A}{Γ}{A} idᴹ) ,sᴹ π₂ᴹ {Γ ,ᴹ A}{Γ}{A} idᴹ ]tᴹ)
lam[]ᴹ {Γ}{Δ}
       {record { map = mapδ ; resp = respδ }}
       {A}{B}
       {record { map = mapt ; resp = respt }}
  = →s= {!!}
        {!!}

⇒βᴹ : {Γ A B : Setoid}{t : (Γ ,ᴹ A) →s B} → appᴹ (lamᴹ {Γ}{A}{B} t) ≡ t
⇒βᴹ {Γ}{A}{B}{record { map = mapt ; resp = respt }}
  = →s= {!!} {!!} 

⇒ηᴹ : {Γ A B : Setoid}{t : Γ →s (A ⇒ᴹ B)} → lamᴹ (appᴹ {Γ}{A}{B} t) ≡ t
⇒ηᴹ {Γ}{A}{B}{record { map = mapt ; resp = respt }}
  = →s= {!!}
        {!!}

----------------------------------------------------------------------
-- The model
----------------------------------------------------------------------

M : Model
M = record
  { Tyᴹ    = Setoid
  ; Conᴹ   = Setoid
  ; Tmsᴹ   = _→s_
  ; Tmᴹ    = _→s_
  ; ιᴹ     = ιᴹ
  ; _⇒ᴹ_   = _⇒ᴹ_
  ; •ᴹ     = •ᴹ
  ; _,ᴹ_   = _,ᴹ_
  ; idᴹ    = idᴹ
  ; _∘ᴹ_   = _∘ᴹ_
  ; εᴹ     = εᴹ
  ; _,ₛᴹ_  = _,sᴹ_
  ; π₁ᴹ    = λ {_}{_}{A} → π₁ᴹ {_}{_}{A}
  ; _[_]ᴹ  = _[_]tᴹ
  ; π₂ᴹ    = λ {_}{Δ} → π₂ᴹ {_}{Δ}
  ; idlᴹ   = idlᴹ
  ; idrᴹ   = idrᴹ
  ; assᴹ   = λ {_}{_}{_}{_}{σ}{δ}{ν} → assᴹ {σ = σ}{δ}{ν}
  ; ,∘ᴹ    = λ {_}{_}{_}{δ}{σ}{A}{a} → ,∘ᴹ {δ = δ}{σ}{A}{a}
  ; ,β₁ᴹ   = λ {_}{_}{_}{δ}{a} → ,β₁ᴹ {δ = δ}{a}
  ; ,ηᴹ    = λ {Γ}{Δ}{A}{δ} → ,ηᴹ {Γ}{Δ}{A}{δ}
  ; •ηᴹ    = •ηᴹ
  ; ,β₂ᴹ   = λ {_}{_}{_}{δ}{a} → ,β₂ᴹ {δ = δ}{a}
  ; lamᴹ   = lamᴹ
  ; appᴹ   = appᴹ
  ; lam[]ᴹ = λ {Γ}{Δ}{δ}{A}{B}{t} → lam[]ᴹ {Γ}{Δ}{δ}{A}{B}{t}
  ; ⇒βᴹ    = λ {Γ}{A}{B}{t} → ⇒βᴹ {Γ}{A}{B}{t}
  ; ⇒ηᴹ    = λ {Γ}{A}{B}{t} → ⇒ηᴹ {Γ}{A}{B}{t}
  }

