{-# OPTIONS --without-K --no-eta --rewriting #-}

module SetoidSimple.Model where

open import Agda.Primitive

open import lib

open import SetoidSimple.Syntax
open import SetoidSimple.Rec

----------------------------------------------------------------------
-- Definition of setoids
----------------------------------------------------------------------

record Setoid : Set₁ where
  field
    ∣_∣ : Set
    _∶_~_ : ∣_∣ → ∣_∣ → Set
    ref : (a : ∣_∣) → _∶_~_ a a

  infix 4 ∣_∣
  infix 5 _∶_~_

open Setoid

record _→s_ (A B : Setoid) : Set where
  field
    map  : ∣ A ∣ → ∣ B ∣
    resp : {a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (map a₀) ~ (map a₁)
    nat : (a : ∣ A ∣) → resp (ref A a) ≡ ref B (map a)

open _→s_

natT= : {A B : Setoid}
        {map₀ map₁ : ∣ A ∣ → ∣ B ∣}(map₂ : map₀ ≡ map₁)
        {resp₀ : {a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (map₀ a₀) ~ (map₀ a₁)}
        {resp₁ : {a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (map₁ a₀) ~ (map₁ a₁)}
        (resp₂ : (λ {a₀}{a₁} → resp₀ {a₀}{a₁}) ≡[ ap (λ z → {a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (z a₀) ~ (z a₁)) map₂ ]≡ resp₁)
      → ((a : ∣ A ∣) → resp₀ (ref A a) ≡ ref B (map₀ a)) ≡ ((a : ∣ A ∣) → resp₁ (ref A a) ≡ ref B (map₁ a))
natT= refl refl = refl

→s= : {A B : Setoid}
      {map₀ map₁ : ∣ A ∣ → ∣ B ∣}(map₂ : map₀ ≡ map₁)
      {resp₀ : {a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (map₀ a₀) ~ (map₀ a₁)}
      {resp₁ : {a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (map₁ a₀) ~ (map₁ a₁)}
      (resp₂ : (λ {a₀}{a₁} → resp₀ {a₀}{a₁}) ≡[ ap (λ z → {a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ (z a₀) ~ (z a₁)) map₂ ]≡ resp₁)
      {nat₀ : (a : ∣ A ∣) → resp₀ (ref A a) ≡ ref B (map₀ a)}
      {nat₁ : (a : ∣ A ∣) → resp₁ (ref A a) ≡ ref B (map₁ a)}
      (nat₂ : nat₀ ≡[ natT= {A}{B} map₂ resp₂ ]≡ nat₁)
    → _≡_ {A = A →s B}
          (record { map = map₀ ; resp = resp₀ ; nat = nat₀ })
          (record { map = map₁ ; resp = resp₁ ; nat = nat₁ })
→s= refl refl refl = refl

----------------------------------------------------------------------
-- Types
----------------------------------------------------------------------

postulate
  ιᴹ : Setoid

_⇒ᴹ_ : Setoid → Setoid → Setoid
A ⇒ᴹ B = record
  { ∣_∣   = A →s B
  ; _∶_~_ = λ t₀ t₁ → ∀{a₀ a₁ : ∣ A ∣} → A ∶ a₀ ~ a₁ → B ∶ map t₀ a₀ ~ map t₁ a₁
  ; ref   = λ t {a₀}{a₁} a₂ → resp t a₂
  }

----------------------------------------------------------------------
-- Contexts
----------------------------------------------------------------------

•ᴹ : Setoid
•ᴹ = record
  { ∣_∣   = ⊤
  ; _∶_~_ = λ _ _ → ⊤
  ; ref   = λ _ → tt
  }

_,ᴹ_ : Setoid → Setoid → Setoid
Γ ,ᴹ A = record
  { ∣_∣   = ∣ Γ ∣ × ∣ A ∣
  ; _∶_~_ = λ { (γ₀ , a₀) (γ₁ , a₁) → Γ ∶ γ₀ ~ γ₁ × A ∶ a₀ ~ a₁ }
  ; ref   = λ { (γ , a) → ref Γ γ , ref A a }
  }

----------------------------------------------------------------------
-- Substitution calculus
----------------------------------------------------------------------

idᴹ : {Γ : Setoid} → Γ →s Γ
idᴹ = record
  { map  = λ γ → γ
  ; resp = λ γ₂ → γ₂
  ; nat  = λ _ → refl
  }

_∘ᴹ_ : {Γ Θ Δ : Setoid} → Θ →s Δ → Γ →s Θ → Γ →s Δ
ν ∘ᴹ σ = record
  { map  = λ γ → map ν (map σ γ)
  ; resp = λ γ₂ → resp ν (resp σ γ₂)
  ; nat  = λ γ → ap (resp ν) (nat σ γ) ◾ nat ν (map σ γ)
  }

εᴹ : {Γ : Setoid} → Γ →s •ᴹ
εᴹ = record
  { map  = λ _ → tt
  ; resp = λ _ → tt
  ; nat  = λ _ → refl
  }

_,sᴹ_ : {Γ Δ : Setoid} → Γ →s Δ → {A : Setoid} → Γ →s A → Γ →s (Δ ,ᴹ A)
_,sᴹ_ {Γ}{Δ} σ {A} t = record
  { map  = λ γ → map σ γ , map t γ 
  ; resp = λ γ₂ → resp σ γ₂ , resp t γ₂
  ; nat  = λ γ → ,×= (nat σ γ) (nat t γ)
  }

π₁ᴹ : {Γ Δ A : Setoid} → Γ →s (Δ ,ᴹ A) → Γ →s Δ
π₁ᴹ {Γ}{Δ}{A} σ = record
  { map  = λ γ → proj₁ (map σ γ)
  ; resp = λ γ₂ → proj₁ (resp σ γ₂)
  ; nat  = λ γ → ap proj₁ (nat σ γ)
  }

_[_]tᴹ : {Γ Δ A : Setoid} → Δ →s A → Γ →s Δ → Γ →s A
_[_]tᴹ {Γ}{Δ}{A} t σ = record
  { map  = λ γ → map t (map σ γ)
  ; resp = λ γ₂ → resp t (resp σ γ₂)
  ; nat  = λ γ → ap (resp t) (nat σ γ) ◾ nat t (map σ γ)
  }

π₂ᴹ : {Γ Δ A : Setoid} → Γ →s (Δ ,ᴹ A) → Γ →s A
π₂ᴹ {Γ}{Δ}{A} σ = record
  { map  = λ γ → proj₂ (map σ γ)
  ; resp = λ γ₂ → proj₂ (resp σ γ₂)
  ; nat  = λ γ → ap proj₂ (nat σ γ)
  }

idlᴹ : {Γ Δ : Setoid} {δ : Γ →s Δ} → (idᴹ ∘ᴹ δ) ≡ δ
idlᴹ {Γ}{Δ}{δ = record { map = map ; resp = resp ; nat = nat }}
  = →s= refl
        refl
        {!!}

assᴹ : {Δ Γ Σ Ω : Setoid}{σ : Σ →s Ω}{δ : Γ →s Σ}{ν : Δ →s Γ}
     → ((σ ∘ᴹ δ) ∘ᴹ ν) ≡ (σ ∘ᴹ (δ ∘ᴹ ν))
assᴹ {Δ}{Γ}{Σ}{Ω}
     {σ = record { map = mapσ ; resp = respσ ; nat = natσ }}
     {δ = record { map = mapδ ; resp = respδ ; nat = natδ }}
     {ν = record { map = mapν ; resp = respν ; nat = natν }}
  = →s= refl
        refl
        {!!}

lamᴹ : {Γ A B : Setoid} → (Γ ,ᴹ A) →s B → Γ →s (A ⇒ᴹ B)
lamᴹ {Γ}{A}{B} t =
 record
  { map = λ γ → record
     { map  = λ a → map t (γ , a)
     ; resp = λ a₂ → resp t (ref Γ γ , a₂)
     ; nat  = λ a → nat t (γ , a)
     }
  ; resp = λ γ₂ a₂ → resp t (γ₂ , a₂)
  ; nat  = λ _ → refl
  }

appᴹ : {Γ A B : Setoid} → Γ →s (A ⇒ᴹ B) → (Γ ,ᴹ A) →s B
appᴹ {Γ}{A}{B} t = record
  { map  = λ { (γ , a) → map (map t γ) a }
  ; resp = λ { (γ₂ , a₂) → resp t γ₂ a₂ }
  ; nat  = λ { (γ , a) → ap (λ z → z (ref A a)) (nat t γ) ◾ nat (map t γ) a }
  }

M : Model
M = record
  { Tyᴹ    = Setoid
  ; Conᴹ   = Setoid
  ; Tmsᴹ   = _→s_
  ; Tmᴹ    = _→s_
  ; ιᴹ     = ιᴹ
  ; _⇒ᴹ_   = _⇒ᴹ_
  ; •ᴹ     = •ᴹ
  ; _,ᴹ_   = _,ᴹ_
  ; idᴹ    = idᴹ
  ; _∘ᴹ_   = _∘ᴹ_
  ; εᴹ     = εᴹ
  ; _,sᴹ_  = _,sᴹ_
  ; π₁ᴹ    = λ {_}{_}{A} → π₁ᴹ {_}{_}{A}
  ; _[_]tᴹ = _[_]tᴹ
  ; π₂ᴹ    = λ {_}{Δ} → π₂ᴹ {_}{Δ}
  ; idlᴹ   = idlᴹ
  ; idrᴹ   = {!!}
  ; assᴹ   = {!!}
  ; ,∘ᴹ    = {!!}
  ; ,β₁ᴹ   = {!!}
  ; ,ηᴹ    = {!!}
  ; •ηᴹ    = {!!}
  ; ,β₂ᴹ   = {!!}
  ; lamᴹ   = lamᴹ
  ; appᴹ   = appᴹ
  ; lam[]ᴹ = {!!}
  ; ⇒βᴹ    = {!!}
  ; ⇒ηᴹ    = {!!}
  }
