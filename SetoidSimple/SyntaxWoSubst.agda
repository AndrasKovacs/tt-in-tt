{-# OPTIONS --without-K --no-eta #-}

module SetoidSimple.SyntaxWoSubst where

open import lib using (_≡_; refl)

-- declaration of the syntax

infixl 5 _,_
infixl 4 _⇒_

data Ty : Set where
  ι : Ty
  _⇒_ : Ty → Ty → Ty

data Con : Set where
  • : Con
  _,_ : Con → Ty → Con

postulate
  Var : Con → Ty → Set
  Tm  : Con → Ty → Set


----------------------------------------------------------------------
-- Core substitution calculus
----------------------------------------------------------------------

postulate
  vze : {Γ : Con}{A : Ty} → Var (Γ , A) A
  vsu : {Γ : Con}{A B : Ty} → Var Γ A → Var (Γ , B) A
  var : {Γ : Con}{A : Ty} → Var Γ A → Tm Γ A



----------------------------------------------------------------------
-- Function space
----------------------------------------------------------------------

postulate
  lam : ∀{Γ A B} → Tm (Γ , A) B → Tm Γ (A ⇒ B)
  app : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ , A) B

postulate
   ⇒β    : ∀{Γ A B}{t : Tm (Γ , A) B}
         → app (lam t) ≡ t
   ⇒η    : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}
         → lam (app t) ≡ t

-- we can't define this
_$_ : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
t $ u = {!app t!}
