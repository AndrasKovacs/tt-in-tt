module SetoidSimple.Standard where

open import Agda.Primitive

open import lib

open import SetoidSimple.Syntax
open import SetoidSimple.Rec

M : Model
M = record
      { Tyᴹ = Set
      ; Conᴹ = Set
      ; Tmsᴹ = λ ⟦Γ⟧ ⟦Δ⟧ → ⟦Γ⟧ → ⟦Δ⟧
      ; Tmᴹ = λ ⟦Γ⟧ ⟦A⟧ → ⟦Γ⟧ → ⟦A⟧
      ; ιᴹ = {!!}
      ; _⇒ᴹ_ = λ ⟦A⟧ ⟦B⟧ → ⟦A⟧ → ⟦B⟧
      ; •ᴹ = ⊤
      ; _,ᴹ_ = _×_
      ; idᴹ = λ γ → γ
      ; _∘ᴹ_ = λ ⟦σ⟧ ⟦ν⟧ γ → ⟦σ⟧ (⟦ν⟧ γ)
      ; εᴹ = λ _ → tt
      ; _,ₛᴹ_ = λ ⟦σ⟧ ⟦t⟧ γ → (⟦σ⟧ γ , ⟦t⟧ γ)
      ; π₁ᴹ = λ ⟦σ⟧ γ → proj₁ (⟦σ⟧ γ)
      ; _[_]ᴹ = λ ⟦t⟧ ⟦σ⟧ γ → ⟦t⟧ (⟦σ⟧ γ)
      ; π₂ᴹ = λ ⟦σ⟧ γ → proj₂ (⟦σ⟧ γ)
      ; idlᴹ = refl
      ; idrᴹ = refl
      ; assᴹ = refl
      ; ,∘ᴹ = refl
      ; ,β₁ᴹ = refl
      ; ,ηᴹ = refl
      ; •ηᴹ = refl
      ; ,β₂ᴹ = refl
      ; lamᴹ = λ ⟦t⟧ γ a → ⟦t⟧ (γ , a)
      ; appᴹ = λ ⟦t⟧ γa → ⟦t⟧ (proj₁ γa) (proj₂ γa)
      ; lam[]ᴹ = refl
      ; ⇒βᴹ = refl
      ; ⇒ηᴹ = refl
      }
