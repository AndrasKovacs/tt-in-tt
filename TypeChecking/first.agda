module TypeChecking.first where

open import lib

data Con : ℕ → Set
data Tm : ℕ → Set

data Con where
  •   : Con zero
  _,_ : {n : ℕ} → Con n → Tm n → Con (suc n)

data Tm where
